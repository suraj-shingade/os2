package com.crisil.onesource;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.crisil.onesource");

        noClasses()
            .that()
            .resideInAnyPackage("com.crisil.onesource.service..")
            .or()
            .resideInAnyPackage("com.crisil.onesource.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..com.crisil.onesource.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
