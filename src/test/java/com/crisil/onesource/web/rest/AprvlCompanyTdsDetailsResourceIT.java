package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.AprvlCompanyTdsDetails;
import com.crisil.onesource.repository.AprvlCompanyTdsDetailsRepository;
import com.crisil.onesource.service.AprvlCompanyTdsDetailsService;
import com.crisil.onesource.service.dto.AprvlCompanyTdsDetailsCriteria;
import com.crisil.onesource.service.AprvlCompanyTdsDetailsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AprvlCompanyTdsDetailsResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AprvlCompanyTdsDetailsResourceIT {

    private static final Integer DEFAULT_CTLG_SRNO = 1;
    private static final Integer UPDATED_CTLG_SRNO = 2;
    private static final Integer SMALLER_CTLG_SRNO = 1 - 1;

    private static final String DEFAULT_RECORDID = "AAAAAAAAAA";
    private static final String UPDATED_RECORDID = "BBBBBBBBBB";

    private static final String DEFAULT_LINEOFBUSINESS = "AAAAAAAAAA";
    private static final String UPDATED_LINEOFBUSINESS = "BBBBBBBBBB";

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_GSTNUMBER = "AAAAAAAAAA";
    private static final String UPDATED_GSTNUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_ISSEZGST = "AAAAAAAAAA";
    private static final String UPDATED_ISSEZGST = "BBBBBBBBBB";

    private static final String DEFAULT_PREVIOUSTDS = "AAAAAAAAAA";
    private static final String UPDATED_PREVIOUSTDS = "BBBBBBBBBB";

    private static final String DEFAULT_TDSNUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TDSNUMBER = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATECODE = 1;
    private static final Integer UPDATED_STATECODE = 2;
    private static final Integer SMALLER_STATECODE = 1 - 1;

    private static final String DEFAULT_OPERATION = "A";
    private static final String UPDATED_OPERATION = "B";

    private static final String DEFAULT_HDELETED = "AAAAAAAAAA";
    private static final String UPDATED_HDELETED = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CTLG_IS_DELETED = "A";
    private static final String UPDATED_CTLG_IS_DELETED = "B";

    private static final Integer DEFAULT_JSON_STORE_ID = 1;
    private static final Integer UPDATED_JSON_STORE_ID = 2;
    private static final Integer SMALLER_JSON_STORE_ID = 1 - 1;

    private static final String DEFAULT_IS_DATA_PROCESSED = "A";
    private static final String UPDATED_IS_DATA_PROCESSED = "B";

    @Autowired
    private AprvlCompanyTdsDetailsRepository aprvlCompanyTdsDetailsRepository;

    @Autowired
    private AprvlCompanyTdsDetailsService aprvlCompanyTdsDetailsService;

    @Autowired
    private AprvlCompanyTdsDetailsQueryService aprvlCompanyTdsDetailsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAprvlCompanyTdsDetailsMockMvc;

    private AprvlCompanyTdsDetails aprvlCompanyTdsDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AprvlCompanyTdsDetails createEntity(EntityManager em) {
        AprvlCompanyTdsDetails aprvlCompanyTdsDetails = new AprvlCompanyTdsDetails()
            .ctlgSrno(DEFAULT_CTLG_SRNO)
            .recordid(DEFAULT_RECORDID)
            .lineofbusiness(DEFAULT_LINEOFBUSINESS)
            .filename(DEFAULT_FILENAME)
            .gstnumber(DEFAULT_GSTNUMBER)
            .comments(DEFAULT_COMMENTS)
            .issezgst(DEFAULT_ISSEZGST)
            .previoustds(DEFAULT_PREVIOUSTDS)
            .tdsnumber(DEFAULT_TDSNUMBER)
            .statecode(DEFAULT_STATECODE)
            .operation(DEFAULT_OPERATION)
            .hdeleted(DEFAULT_HDELETED)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .ctlgIsDeleted(DEFAULT_CTLG_IS_DELETED)
            .jsonStoreId(DEFAULT_JSON_STORE_ID)
            .isDataProcessed(DEFAULT_IS_DATA_PROCESSED);
        return aprvlCompanyTdsDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AprvlCompanyTdsDetails createUpdatedEntity(EntityManager em) {
        AprvlCompanyTdsDetails aprvlCompanyTdsDetails = new AprvlCompanyTdsDetails()
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .recordid(UPDATED_RECORDID)
            .lineofbusiness(UPDATED_LINEOFBUSINESS)
            .filename(UPDATED_FILENAME)
            .gstnumber(UPDATED_GSTNUMBER)
            .comments(UPDATED_COMMENTS)
            .issezgst(UPDATED_ISSEZGST)
            .previoustds(UPDATED_PREVIOUSTDS)
            .tdsnumber(UPDATED_TDSNUMBER)
            .statecode(UPDATED_STATECODE)
            .operation(UPDATED_OPERATION)
            .hdeleted(UPDATED_HDELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED);
        return aprvlCompanyTdsDetails;
    }

    @BeforeEach
    public void initTest() {
        aprvlCompanyTdsDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createAprvlCompanyTdsDetails() throws Exception {
        int databaseSizeBeforeCreate = aprvlCompanyTdsDetailsRepository.findAll().size();
        // Create the AprvlCompanyTdsDetails
        restAprvlCompanyTdsDetailsMockMvc.perform(post("/api/aprvl-company-tds-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyTdsDetails)))
            .andExpect(status().isCreated());

        // Validate the AprvlCompanyTdsDetails in the database
        List<AprvlCompanyTdsDetails> aprvlCompanyTdsDetailsList = aprvlCompanyTdsDetailsRepository.findAll();
        assertThat(aprvlCompanyTdsDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        AprvlCompanyTdsDetails testAprvlCompanyTdsDetails = aprvlCompanyTdsDetailsList.get(aprvlCompanyTdsDetailsList.size() - 1);
        assertThat(testAprvlCompanyTdsDetails.getCtlgSrno()).isEqualTo(DEFAULT_CTLG_SRNO);
        assertThat(testAprvlCompanyTdsDetails.getRecordid()).isEqualTo(DEFAULT_RECORDID);
        assertThat(testAprvlCompanyTdsDetails.getLineofbusiness()).isEqualTo(DEFAULT_LINEOFBUSINESS);
        assertThat(testAprvlCompanyTdsDetails.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testAprvlCompanyTdsDetails.getGstnumber()).isEqualTo(DEFAULT_GSTNUMBER);
        assertThat(testAprvlCompanyTdsDetails.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAprvlCompanyTdsDetails.getIssezgst()).isEqualTo(DEFAULT_ISSEZGST);
        assertThat(testAprvlCompanyTdsDetails.getPrevioustds()).isEqualTo(DEFAULT_PREVIOUSTDS);
        assertThat(testAprvlCompanyTdsDetails.getTdsnumber()).isEqualTo(DEFAULT_TDSNUMBER);
        assertThat(testAprvlCompanyTdsDetails.getStatecode()).isEqualTo(DEFAULT_STATECODE);
        assertThat(testAprvlCompanyTdsDetails.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testAprvlCompanyTdsDetails.getHdeleted()).isEqualTo(DEFAULT_HDELETED);
        assertThat(testAprvlCompanyTdsDetails.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAprvlCompanyTdsDetails.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testAprvlCompanyTdsDetails.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAprvlCompanyTdsDetails.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testAprvlCompanyTdsDetails.getCtlgIsDeleted()).isEqualTo(DEFAULT_CTLG_IS_DELETED);
        assertThat(testAprvlCompanyTdsDetails.getJsonStoreId()).isEqualTo(DEFAULT_JSON_STORE_ID);
        assertThat(testAprvlCompanyTdsDetails.getIsDataProcessed()).isEqualTo(DEFAULT_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void createAprvlCompanyTdsDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aprvlCompanyTdsDetailsRepository.findAll().size();

        // Create the AprvlCompanyTdsDetails with an existing ID
        aprvlCompanyTdsDetails.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAprvlCompanyTdsDetailsMockMvc.perform(post("/api/aprvl-company-tds-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyTdsDetails)))
            .andExpect(status().isBadRequest());

        // Validate the AprvlCompanyTdsDetails in the database
        List<AprvlCompanyTdsDetails> aprvlCompanyTdsDetailsList = aprvlCompanyTdsDetailsRepository.findAll();
        assertThat(aprvlCompanyTdsDetailsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCtlgSrnoIsRequired() throws Exception {
        int databaseSizeBeforeTest = aprvlCompanyTdsDetailsRepository.findAll().size();
        // set the field null
        aprvlCompanyTdsDetails.setCtlgSrno(null);

        // Create the AprvlCompanyTdsDetails, which fails.


        restAprvlCompanyTdsDetailsMockMvc.perform(post("/api/aprvl-company-tds-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyTdsDetails)))
            .andExpect(status().isBadRequest());

        List<AprvlCompanyTdsDetails> aprvlCompanyTdsDetailsList = aprvlCompanyTdsDetailsRepository.findAll();
        assertThat(aprvlCompanyTdsDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetails() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aprvlCompanyTdsDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].recordid").value(hasItem(DEFAULT_RECORDID)))
            .andExpect(jsonPath("$.[*].lineofbusiness").value(hasItem(DEFAULT_LINEOFBUSINESS)))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME)))
            .andExpect(jsonPath("$.[*].gstnumber").value(hasItem(DEFAULT_GSTNUMBER)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].issezgst").value(hasItem(DEFAULT_ISSEZGST)))
            .andExpect(jsonPath("$.[*].previoustds").value(hasItem(DEFAULT_PREVIOUSTDS)))
            .andExpect(jsonPath("$.[*].tdsnumber").value(hasItem(DEFAULT_TDSNUMBER)))
            .andExpect(jsonPath("$.[*].statecode").value(hasItem(DEFAULT_STATECODE)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].hdeleted").value(hasItem(DEFAULT_HDELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)));
    }
    
    @Test
    @Transactional
    public void getAprvlCompanyTdsDetails() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get the aprvlCompanyTdsDetails
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details/{id}", aprvlCompanyTdsDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(aprvlCompanyTdsDetails.getId().intValue()))
            .andExpect(jsonPath("$.ctlgSrno").value(DEFAULT_CTLG_SRNO))
            .andExpect(jsonPath("$.recordid").value(DEFAULT_RECORDID))
            .andExpect(jsonPath("$.lineofbusiness").value(DEFAULT_LINEOFBUSINESS))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME))
            .andExpect(jsonPath("$.gstnumber").value(DEFAULT_GSTNUMBER))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()))
            .andExpect(jsonPath("$.issezgst").value(DEFAULT_ISSEZGST))
            .andExpect(jsonPath("$.previoustds").value(DEFAULT_PREVIOUSTDS))
            .andExpect(jsonPath("$.tdsnumber").value(DEFAULT_TDSNUMBER))
            .andExpect(jsonPath("$.statecode").value(DEFAULT_STATECODE))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION))
            .andExpect(jsonPath("$.hdeleted").value(DEFAULT_HDELETED))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.ctlgIsDeleted").value(DEFAULT_CTLG_IS_DELETED))
            .andExpect(jsonPath("$.jsonStoreId").value(DEFAULT_JSON_STORE_ID))
            .andExpect(jsonPath("$.isDataProcessed").value(DEFAULT_IS_DATA_PROCESSED));
    }


    @Test
    @Transactional
    public void getAprvlCompanyTdsDetailsByIdFiltering() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        Long id = aprvlCompanyTdsDetails.getId();

        defaultAprvlCompanyTdsDetailsShouldBeFound("id.equals=" + id);
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultAprvlCompanyTdsDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultAprvlCompanyTdsDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno equals to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.equals=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.equals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno not equals to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.notEquals=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno not equals to UPDATED_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.notEquals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno in DEFAULT_CTLG_SRNO or UPDATED_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.in=" + DEFAULT_CTLG_SRNO + "," + UPDATED_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.in=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is greater than or equal to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.greaterThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is greater than or equal to UPDATED_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.greaterThanOrEqual=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is less than or equal to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.lessThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is less than or equal to SMALLER_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.lessThanOrEqual=" + SMALLER_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is less than DEFAULT_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.lessThan=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is less than UPDATED_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.lessThan=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgSrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is greater than DEFAULT_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgSrno.greaterThan=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyTdsDetailsList where ctlgSrno is greater than SMALLER_CTLG_SRNO
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgSrno.greaterThan=" + SMALLER_CTLG_SRNO);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByRecordidIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where recordid equals to DEFAULT_RECORDID
        defaultAprvlCompanyTdsDetailsShouldBeFound("recordid.equals=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyTdsDetailsList where recordid equals to UPDATED_RECORDID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("recordid.equals=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByRecordidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where recordid not equals to DEFAULT_RECORDID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("recordid.notEquals=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyTdsDetailsList where recordid not equals to UPDATED_RECORDID
        defaultAprvlCompanyTdsDetailsShouldBeFound("recordid.notEquals=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByRecordidIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where recordid in DEFAULT_RECORDID or UPDATED_RECORDID
        defaultAprvlCompanyTdsDetailsShouldBeFound("recordid.in=" + DEFAULT_RECORDID + "," + UPDATED_RECORDID);

        // Get all the aprvlCompanyTdsDetailsList where recordid equals to UPDATED_RECORDID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("recordid.in=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByRecordidIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where recordid is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("recordid.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where recordid is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("recordid.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByRecordidContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where recordid contains DEFAULT_RECORDID
        defaultAprvlCompanyTdsDetailsShouldBeFound("recordid.contains=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyTdsDetailsList where recordid contains UPDATED_RECORDID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("recordid.contains=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByRecordidNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where recordid does not contain DEFAULT_RECORDID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("recordid.doesNotContain=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyTdsDetailsList where recordid does not contain UPDATED_RECORDID
        defaultAprvlCompanyTdsDetailsShouldBeFound("recordid.doesNotContain=" + UPDATED_RECORDID);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLineofbusinessIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness equals to DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldBeFound("lineofbusiness.equals=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness equals to UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lineofbusiness.equals=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLineofbusinessIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness not equals to DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lineofbusiness.notEquals=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness not equals to UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldBeFound("lineofbusiness.notEquals=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLineofbusinessIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness in DEFAULT_LINEOFBUSINESS or UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldBeFound("lineofbusiness.in=" + DEFAULT_LINEOFBUSINESS + "," + UPDATED_LINEOFBUSINESS);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness equals to UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lineofbusiness.in=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLineofbusinessIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("lineofbusiness.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lineofbusiness.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLineofbusinessContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness contains DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldBeFound("lineofbusiness.contains=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness contains UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lineofbusiness.contains=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLineofbusinessNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness does not contain DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lineofbusiness.doesNotContain=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyTdsDetailsList where lineofbusiness does not contain UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyTdsDetailsShouldBeFound("lineofbusiness.doesNotContain=" + UPDATED_LINEOFBUSINESS);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where filename equals to DEFAULT_FILENAME
        defaultAprvlCompanyTdsDetailsShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyTdsDetailsList where filename equals to UPDATED_FILENAME
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where filename not equals to DEFAULT_FILENAME
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("filename.notEquals=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyTdsDetailsList where filename not equals to UPDATED_FILENAME
        defaultAprvlCompanyTdsDetailsShouldBeFound("filename.notEquals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultAprvlCompanyTdsDetailsShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the aprvlCompanyTdsDetailsList where filename equals to UPDATED_FILENAME
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where filename is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("filename.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where filename is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("filename.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByFilenameContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where filename contains DEFAULT_FILENAME
        defaultAprvlCompanyTdsDetailsShouldBeFound("filename.contains=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyTdsDetailsList where filename contains UPDATED_FILENAME
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("filename.contains=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where filename does not contain DEFAULT_FILENAME
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("filename.doesNotContain=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyTdsDetailsList where filename does not contain UPDATED_FILENAME
        defaultAprvlCompanyTdsDetailsShouldBeFound("filename.doesNotContain=" + UPDATED_FILENAME);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByGstnumberIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber equals to DEFAULT_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("gstnumber.equals=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber equals to UPDATED_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("gstnumber.equals=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByGstnumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber not equals to DEFAULT_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("gstnumber.notEquals=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber not equals to UPDATED_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("gstnumber.notEquals=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByGstnumberIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber in DEFAULT_GSTNUMBER or UPDATED_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("gstnumber.in=" + DEFAULT_GSTNUMBER + "," + UPDATED_GSTNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber equals to UPDATED_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("gstnumber.in=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByGstnumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("gstnumber.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where gstnumber is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("gstnumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByGstnumberContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber contains DEFAULT_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("gstnumber.contains=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber contains UPDATED_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("gstnumber.contains=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByGstnumberNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber does not contain DEFAULT_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("gstnumber.doesNotContain=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where gstnumber does not contain UPDATED_GSTNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("gstnumber.doesNotContain=" + UPDATED_GSTNUMBER);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIssezgstIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where issezgst equals to DEFAULT_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldBeFound("issezgst.equals=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyTdsDetailsList where issezgst equals to UPDATED_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("issezgst.equals=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIssezgstIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where issezgst not equals to DEFAULT_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("issezgst.notEquals=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyTdsDetailsList where issezgst not equals to UPDATED_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldBeFound("issezgst.notEquals=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIssezgstIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where issezgst in DEFAULT_ISSEZGST or UPDATED_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldBeFound("issezgst.in=" + DEFAULT_ISSEZGST + "," + UPDATED_ISSEZGST);

        // Get all the aprvlCompanyTdsDetailsList where issezgst equals to UPDATED_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("issezgst.in=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIssezgstIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where issezgst is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("issezgst.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where issezgst is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("issezgst.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIssezgstContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where issezgst contains DEFAULT_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldBeFound("issezgst.contains=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyTdsDetailsList where issezgst contains UPDATED_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("issezgst.contains=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIssezgstNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where issezgst does not contain DEFAULT_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("issezgst.doesNotContain=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyTdsDetailsList where issezgst does not contain UPDATED_ISSEZGST
        defaultAprvlCompanyTdsDetailsShouldBeFound("issezgst.doesNotContain=" + UPDATED_ISSEZGST);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByPrevioustdsIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where previoustds equals to DEFAULT_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldBeFound("previoustds.equals=" + DEFAULT_PREVIOUSTDS);

        // Get all the aprvlCompanyTdsDetailsList where previoustds equals to UPDATED_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("previoustds.equals=" + UPDATED_PREVIOUSTDS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByPrevioustdsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where previoustds not equals to DEFAULT_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("previoustds.notEquals=" + DEFAULT_PREVIOUSTDS);

        // Get all the aprvlCompanyTdsDetailsList where previoustds not equals to UPDATED_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldBeFound("previoustds.notEquals=" + UPDATED_PREVIOUSTDS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByPrevioustdsIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where previoustds in DEFAULT_PREVIOUSTDS or UPDATED_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldBeFound("previoustds.in=" + DEFAULT_PREVIOUSTDS + "," + UPDATED_PREVIOUSTDS);

        // Get all the aprvlCompanyTdsDetailsList where previoustds equals to UPDATED_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("previoustds.in=" + UPDATED_PREVIOUSTDS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByPrevioustdsIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where previoustds is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("previoustds.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where previoustds is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("previoustds.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByPrevioustdsContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where previoustds contains DEFAULT_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldBeFound("previoustds.contains=" + DEFAULT_PREVIOUSTDS);

        // Get all the aprvlCompanyTdsDetailsList where previoustds contains UPDATED_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("previoustds.contains=" + UPDATED_PREVIOUSTDS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByPrevioustdsNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where previoustds does not contain DEFAULT_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("previoustds.doesNotContain=" + DEFAULT_PREVIOUSTDS);

        // Get all the aprvlCompanyTdsDetailsList where previoustds does not contain UPDATED_PREVIOUSTDS
        defaultAprvlCompanyTdsDetailsShouldBeFound("previoustds.doesNotContain=" + UPDATED_PREVIOUSTDS);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByTdsnumberIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber equals to DEFAULT_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("tdsnumber.equals=" + DEFAULT_TDSNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber equals to UPDATED_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("tdsnumber.equals=" + UPDATED_TDSNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByTdsnumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber not equals to DEFAULT_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("tdsnumber.notEquals=" + DEFAULT_TDSNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber not equals to UPDATED_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("tdsnumber.notEquals=" + UPDATED_TDSNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByTdsnumberIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber in DEFAULT_TDSNUMBER or UPDATED_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("tdsnumber.in=" + DEFAULT_TDSNUMBER + "," + UPDATED_TDSNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber equals to UPDATED_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("tdsnumber.in=" + UPDATED_TDSNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByTdsnumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("tdsnumber.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("tdsnumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByTdsnumberContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber contains DEFAULT_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("tdsnumber.contains=" + DEFAULT_TDSNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber contains UPDATED_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("tdsnumber.contains=" + UPDATED_TDSNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByTdsnumberNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber does not contain DEFAULT_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("tdsnumber.doesNotContain=" + DEFAULT_TDSNUMBER);

        // Get all the aprvlCompanyTdsDetailsList where tdsnumber does not contain UPDATED_TDSNUMBER
        defaultAprvlCompanyTdsDetailsShouldBeFound("tdsnumber.doesNotContain=" + UPDATED_TDSNUMBER);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode equals to DEFAULT_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.equals=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode equals to UPDATED_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.equals=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode not equals to DEFAULT_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.notEquals=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode not equals to UPDATED_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.notEquals=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode in DEFAULT_STATECODE or UPDATED_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.in=" + DEFAULT_STATECODE + "," + UPDATED_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode equals to UPDATED_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.in=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where statecode is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode is greater than or equal to DEFAULT_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.greaterThanOrEqual=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode is greater than or equal to UPDATED_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.greaterThanOrEqual=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode is less than or equal to DEFAULT_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.lessThanOrEqual=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode is less than or equal to SMALLER_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.lessThanOrEqual=" + SMALLER_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode is less than DEFAULT_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.lessThan=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode is less than UPDATED_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.lessThan=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByStatecodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where statecode is greater than DEFAULT_STATECODE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("statecode.greaterThan=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyTdsDetailsList where statecode is greater than SMALLER_STATECODE
        defaultAprvlCompanyTdsDetailsShouldBeFound("statecode.greaterThan=" + SMALLER_STATECODE);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where operation equals to DEFAULT_OPERATION
        defaultAprvlCompanyTdsDetailsShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyTdsDetailsList where operation equals to UPDATED_OPERATION
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where operation not equals to DEFAULT_OPERATION
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyTdsDetailsList where operation not equals to UPDATED_OPERATION
        defaultAprvlCompanyTdsDetailsShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultAprvlCompanyTdsDetailsShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the aprvlCompanyTdsDetailsList where operation equals to UPDATED_OPERATION
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where operation is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("operation.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where operation is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("operation.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByOperationContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where operation contains DEFAULT_OPERATION
        defaultAprvlCompanyTdsDetailsShouldBeFound("operation.contains=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyTdsDetailsList where operation contains UPDATED_OPERATION
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("operation.contains=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByOperationNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where operation does not contain DEFAULT_OPERATION
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("operation.doesNotContain=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyTdsDetailsList where operation does not contain UPDATED_OPERATION
        defaultAprvlCompanyTdsDetailsShouldBeFound("operation.doesNotContain=" + UPDATED_OPERATION);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByHdeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted equals to DEFAULT_HDELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("hdeleted.equals=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted equals to UPDATED_HDELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("hdeleted.equals=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByHdeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted not equals to DEFAULT_HDELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("hdeleted.notEquals=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted not equals to UPDATED_HDELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("hdeleted.notEquals=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByHdeletedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted in DEFAULT_HDELETED or UPDATED_HDELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("hdeleted.in=" + DEFAULT_HDELETED + "," + UPDATED_HDELETED);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted equals to UPDATED_HDELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("hdeleted.in=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByHdeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("hdeleted.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where hdeleted is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("hdeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByHdeletedContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted contains DEFAULT_HDELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("hdeleted.contains=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted contains UPDATED_HDELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("hdeleted.contains=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByHdeletedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted does not contain DEFAULT_HDELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("hdeleted.doesNotContain=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyTdsDetailsList where hdeleted does not contain UPDATED_HDELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("hdeleted.doesNotContain=" + UPDATED_HDELETED);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdDate equals to DEFAULT_CREATED_DATE
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the aprvlCompanyTdsDetailsList where createdDate equals to UPDATED_CREATED_DATE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the aprvlCompanyTdsDetailsList where createdDate not equals to UPDATED_CREATED_DATE
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the aprvlCompanyTdsDetailsList where createdDate equals to UPDATED_CREATED_DATE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdDate is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdDate.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where createdDate is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedDate.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedDate is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdBy equals to DEFAULT_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyTdsDetailsList where createdBy equals to UPDATED_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdBy not equals to DEFAULT_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyTdsDetailsList where createdBy not equals to UPDATED_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the aprvlCompanyTdsDetailsList where createdBy equals to UPDATED_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdBy is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdBy.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where createdBy is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdBy contains DEFAULT_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyTdsDetailsList where createdBy contains UPDATED_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where createdBy does not contain DEFAULT_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyTdsDetailsList where createdBy does not contain UPDATED_CREATED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedBy.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyTdsDetailsList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyTdsDetailsShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted equals to DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgIsDeleted.equals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgIsDeleted.equals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted not equals to DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgIsDeleted.notEquals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted not equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgIsDeleted.notEquals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted in DEFAULT_CTLG_IS_DELETED or UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgIsDeleted.in=" + DEFAULT_CTLG_IS_DELETED + "," + UPDATED_CTLG_IS_DELETED);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgIsDeleted.in=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgIsDeleted.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgIsDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted contains DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgIsDeleted.contains=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted contains UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgIsDeleted.contains=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByCtlgIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted does not contain DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("ctlgIsDeleted.doesNotContain=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyTdsDetailsList where ctlgIsDeleted does not contain UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyTdsDetailsShouldBeFound("ctlgIsDeleted.doesNotContain=" + UPDATED_CTLG_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId equals to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.equals=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.equals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId not equals to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.notEquals=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId not equals to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.notEquals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId in DEFAULT_JSON_STORE_ID or UPDATED_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.in=" + DEFAULT_JSON_STORE_ID + "," + UPDATED_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.in=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is greater than or equal to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.greaterThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is greater than or equal to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.greaterThanOrEqual=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is less than or equal to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.lessThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is less than or equal to SMALLER_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.lessThanOrEqual=" + SMALLER_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is less than DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.lessThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is less than UPDATED_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.lessThan=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByJsonStoreIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is greater than DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("jsonStoreId.greaterThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyTdsDetailsList where jsonStoreId is greater than SMALLER_JSON_STORE_ID
        defaultAprvlCompanyTdsDetailsShouldBeFound("jsonStoreId.greaterThan=" + SMALLER_JSON_STORE_ID);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIsDataProcessedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed equals to DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldBeFound("isDataProcessed.equals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("isDataProcessed.equals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIsDataProcessedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed not equals to DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("isDataProcessed.notEquals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed not equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldBeFound("isDataProcessed.notEquals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIsDataProcessedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed in DEFAULT_IS_DATA_PROCESSED or UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldBeFound("isDataProcessed.in=" + DEFAULT_IS_DATA_PROCESSED + "," + UPDATED_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("isDataProcessed.in=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIsDataProcessedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed is not null
        defaultAprvlCompanyTdsDetailsShouldBeFound("isDataProcessed.specified=true");

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed is null
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("isDataProcessed.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIsDataProcessedContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed contains DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldBeFound("isDataProcessed.contains=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed contains UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("isDataProcessed.contains=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyTdsDetailsByIsDataProcessedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsRepository.saveAndFlush(aprvlCompanyTdsDetails);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed does not contain DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldNotBeFound("isDataProcessed.doesNotContain=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyTdsDetailsList where isDataProcessed does not contain UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyTdsDetailsShouldBeFound("isDataProcessed.doesNotContain=" + UPDATED_IS_DATA_PROCESSED);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAprvlCompanyTdsDetailsShouldBeFound(String filter) throws Exception {
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aprvlCompanyTdsDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].recordid").value(hasItem(DEFAULT_RECORDID)))
            .andExpect(jsonPath("$.[*].lineofbusiness").value(hasItem(DEFAULT_LINEOFBUSINESS)))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME)))
            .andExpect(jsonPath("$.[*].gstnumber").value(hasItem(DEFAULT_GSTNUMBER)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].issezgst").value(hasItem(DEFAULT_ISSEZGST)))
            .andExpect(jsonPath("$.[*].previoustds").value(hasItem(DEFAULT_PREVIOUSTDS)))
            .andExpect(jsonPath("$.[*].tdsnumber").value(hasItem(DEFAULT_TDSNUMBER)))
            .andExpect(jsonPath("$.[*].statecode").value(hasItem(DEFAULT_STATECODE)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].hdeleted").value(hasItem(DEFAULT_HDELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)));

        // Check, that the count call also returns 1
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAprvlCompanyTdsDetailsShouldNotBeFound(String filter) throws Exception {
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAprvlCompanyTdsDetails() throws Exception {
        // Get the aprvlCompanyTdsDetails
        restAprvlCompanyTdsDetailsMockMvc.perform(get("/api/aprvl-company-tds-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAprvlCompanyTdsDetails() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsService.save(aprvlCompanyTdsDetails);

        int databaseSizeBeforeUpdate = aprvlCompanyTdsDetailsRepository.findAll().size();

        // Update the aprvlCompanyTdsDetails
        AprvlCompanyTdsDetails updatedAprvlCompanyTdsDetails = aprvlCompanyTdsDetailsRepository.findById(aprvlCompanyTdsDetails.getId()).get();
        // Disconnect from session so that the updates on updatedAprvlCompanyTdsDetails are not directly saved in db
        em.detach(updatedAprvlCompanyTdsDetails);
        updatedAprvlCompanyTdsDetails
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .recordid(UPDATED_RECORDID)
            .lineofbusiness(UPDATED_LINEOFBUSINESS)
            .filename(UPDATED_FILENAME)
            .gstnumber(UPDATED_GSTNUMBER)
            .comments(UPDATED_COMMENTS)
            .issezgst(UPDATED_ISSEZGST)
            .previoustds(UPDATED_PREVIOUSTDS)
            .tdsnumber(UPDATED_TDSNUMBER)
            .statecode(UPDATED_STATECODE)
            .operation(UPDATED_OPERATION)
            .hdeleted(UPDATED_HDELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED);

        restAprvlCompanyTdsDetailsMockMvc.perform(put("/api/aprvl-company-tds-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAprvlCompanyTdsDetails)))
            .andExpect(status().isOk());

        // Validate the AprvlCompanyTdsDetails in the database
        List<AprvlCompanyTdsDetails> aprvlCompanyTdsDetailsList = aprvlCompanyTdsDetailsRepository.findAll();
        assertThat(aprvlCompanyTdsDetailsList).hasSize(databaseSizeBeforeUpdate);
        AprvlCompanyTdsDetails testAprvlCompanyTdsDetails = aprvlCompanyTdsDetailsList.get(aprvlCompanyTdsDetailsList.size() - 1);
        assertThat(testAprvlCompanyTdsDetails.getCtlgSrno()).isEqualTo(UPDATED_CTLG_SRNO);
        assertThat(testAprvlCompanyTdsDetails.getRecordid()).isEqualTo(UPDATED_RECORDID);
        assertThat(testAprvlCompanyTdsDetails.getLineofbusiness()).isEqualTo(UPDATED_LINEOFBUSINESS);
        assertThat(testAprvlCompanyTdsDetails.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testAprvlCompanyTdsDetails.getGstnumber()).isEqualTo(UPDATED_GSTNUMBER);
        assertThat(testAprvlCompanyTdsDetails.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAprvlCompanyTdsDetails.getIssezgst()).isEqualTo(UPDATED_ISSEZGST);
        assertThat(testAprvlCompanyTdsDetails.getPrevioustds()).isEqualTo(UPDATED_PREVIOUSTDS);
        assertThat(testAprvlCompanyTdsDetails.getTdsnumber()).isEqualTo(UPDATED_TDSNUMBER);
        assertThat(testAprvlCompanyTdsDetails.getStatecode()).isEqualTo(UPDATED_STATECODE);
        assertThat(testAprvlCompanyTdsDetails.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testAprvlCompanyTdsDetails.getHdeleted()).isEqualTo(UPDATED_HDELETED);
        assertThat(testAprvlCompanyTdsDetails.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAprvlCompanyTdsDetails.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testAprvlCompanyTdsDetails.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAprvlCompanyTdsDetails.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testAprvlCompanyTdsDetails.getCtlgIsDeleted()).isEqualTo(UPDATED_CTLG_IS_DELETED);
        assertThat(testAprvlCompanyTdsDetails.getJsonStoreId()).isEqualTo(UPDATED_JSON_STORE_ID);
        assertThat(testAprvlCompanyTdsDetails.getIsDataProcessed()).isEqualTo(UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void updateNonExistingAprvlCompanyTdsDetails() throws Exception {
        int databaseSizeBeforeUpdate = aprvlCompanyTdsDetailsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAprvlCompanyTdsDetailsMockMvc.perform(put("/api/aprvl-company-tds-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyTdsDetails)))
            .andExpect(status().isBadRequest());

        // Validate the AprvlCompanyTdsDetails in the database
        List<AprvlCompanyTdsDetails> aprvlCompanyTdsDetailsList = aprvlCompanyTdsDetailsRepository.findAll();
        assertThat(aprvlCompanyTdsDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAprvlCompanyTdsDetails() throws Exception {
        // Initialize the database
        aprvlCompanyTdsDetailsService.save(aprvlCompanyTdsDetails);

        int databaseSizeBeforeDelete = aprvlCompanyTdsDetailsRepository.findAll().size();

        // Delete the aprvlCompanyTdsDetails
        restAprvlCompanyTdsDetailsMockMvc.perform(delete("/api/aprvl-company-tds-details/{id}", aprvlCompanyTdsDetails.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AprvlCompanyTdsDetails> aprvlCompanyTdsDetailsList = aprvlCompanyTdsDetailsRepository.findAll();
        assertThat(aprvlCompanyTdsDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
