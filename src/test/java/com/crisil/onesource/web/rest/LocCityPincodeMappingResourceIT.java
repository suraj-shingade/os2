package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocCityPincodeMapping;
import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.repository.LocCityPincodeMappingRepository;
import com.crisil.onesource.service.LocCityPincodeMappingService;
import com.crisil.onesource.service.dto.LocCityPincodeMappingCriteria;
import com.crisil.onesource.service.LocCityPincodeMappingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocCityPincodeMappingResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocCityPincodeMappingResourceIT {

    private static final Integer DEFAULT_CITY_PIN_MAPPING_ID = 1;
    private static final Integer UPDATED_CITY_PIN_MAPPING_ID = 2;
    private static final Integer SMALLER_CITY_PIN_MAPPING_ID = 1 - 1;

    private static final String DEFAULT_PINCODE = "AAAAAAAAAA";
    private static final String UPDATED_PINCODE = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private LocCityPincodeMappingRepository locCityPincodeMappingRepository;

    @Autowired
    private LocCityPincodeMappingService locCityPincodeMappingService;

    @Autowired
    private LocCityPincodeMappingQueryService locCityPincodeMappingQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocCityPincodeMappingMockMvc;

    private LocCityPincodeMapping locCityPincodeMapping;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocCityPincodeMapping createEntity(EntityManager em) {
        LocCityPincodeMapping locCityPincodeMapping = new LocCityPincodeMapping()
            .cityPinMappingId(DEFAULT_CITY_PIN_MAPPING_ID)
            .pincode(DEFAULT_PINCODE)
            .isDeleted(DEFAULT_IS_DELETED)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY);
        return locCityPincodeMapping;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocCityPincodeMapping createUpdatedEntity(EntityManager em) {
        LocCityPincodeMapping locCityPincodeMapping = new LocCityPincodeMapping()
            .cityPinMappingId(UPDATED_CITY_PIN_MAPPING_ID)
            .pincode(UPDATED_PINCODE)
            .isDeleted(UPDATED_IS_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        return locCityPincodeMapping;
    }

    @BeforeEach
    public void initTest() {
        locCityPincodeMapping = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocCityPincodeMapping() throws Exception {
        int databaseSizeBeforeCreate = locCityPincodeMappingRepository.findAll().size();
        // Create the LocCityPincodeMapping
        restLocCityPincodeMappingMockMvc.perform(post("/api/loc-city-pincode-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityPincodeMapping)))
            .andExpect(status().isCreated());

        // Validate the LocCityPincodeMapping in the database
        List<LocCityPincodeMapping> locCityPincodeMappingList = locCityPincodeMappingRepository.findAll();
        assertThat(locCityPincodeMappingList).hasSize(databaseSizeBeforeCreate + 1);
        LocCityPincodeMapping testLocCityPincodeMapping = locCityPincodeMappingList.get(locCityPincodeMappingList.size() - 1);
        assertThat(testLocCityPincodeMapping.getCityPinMappingId()).isEqualTo(DEFAULT_CITY_PIN_MAPPING_ID);
        assertThat(testLocCityPincodeMapping.getPincode()).isEqualTo(DEFAULT_PINCODE);
        assertThat(testLocCityPincodeMapping.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocCityPincodeMapping.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocCityPincodeMapping.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocCityPincodeMapping.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocCityPincodeMapping.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createLocCityPincodeMappingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locCityPincodeMappingRepository.findAll().size();

        // Create the LocCityPincodeMapping with an existing ID
        locCityPincodeMapping.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocCityPincodeMappingMockMvc.perform(post("/api/loc-city-pincode-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityPincodeMapping)))
            .andExpect(status().isBadRequest());

        // Validate the LocCityPincodeMapping in the database
        List<LocCityPincodeMapping> locCityPincodeMappingList = locCityPincodeMappingRepository.findAll();
        assertThat(locCityPincodeMappingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCityPinMappingIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = locCityPincodeMappingRepository.findAll().size();
        // set the field null
        locCityPincodeMapping.setCityPinMappingId(null);

        // Create the LocCityPincodeMapping, which fails.


        restLocCityPincodeMappingMockMvc.perform(post("/api/loc-city-pincode-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityPincodeMapping)))
            .andExpect(status().isBadRequest());

        List<LocCityPincodeMapping> locCityPincodeMappingList = locCityPincodeMappingRepository.findAll();
        assertThat(locCityPincodeMappingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappings() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locCityPincodeMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityPinMappingId").value(hasItem(DEFAULT_CITY_PIN_MAPPING_ID)))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }
    
    @Test
    @Transactional
    public void getLocCityPincodeMapping() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get the locCityPincodeMapping
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings/{id}", locCityPincodeMapping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locCityPincodeMapping.getId().intValue()))
            .andExpect(jsonPath("$.cityPinMappingId").value(DEFAULT_CITY_PIN_MAPPING_ID))
            .andExpect(jsonPath("$.pincode").value(DEFAULT_PINCODE))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY));
    }


    @Test
    @Transactional
    public void getLocCityPincodeMappingsByIdFiltering() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        Long id = locCityPincodeMapping.getId();

        defaultLocCityPincodeMappingShouldBeFound("id.equals=" + id);
        defaultLocCityPincodeMappingShouldNotBeFound("id.notEquals=" + id);

        defaultLocCityPincodeMappingShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocCityPincodeMappingShouldNotBeFound("id.greaterThan=" + id);

        defaultLocCityPincodeMappingShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocCityPincodeMappingShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId equals to DEFAULT_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.equals=" + DEFAULT_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId equals to UPDATED_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.equals=" + UPDATED_CITY_PIN_MAPPING_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId not equals to DEFAULT_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.notEquals=" + DEFAULT_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId not equals to UPDATED_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.notEquals=" + UPDATED_CITY_PIN_MAPPING_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId in DEFAULT_CITY_PIN_MAPPING_ID or UPDATED_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.in=" + DEFAULT_CITY_PIN_MAPPING_ID + "," + UPDATED_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId equals to UPDATED_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.in=" + UPDATED_CITY_PIN_MAPPING_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId is not null
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.specified=true");

        // Get all the locCityPincodeMappingList where cityPinMappingId is null
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId is greater than or equal to DEFAULT_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.greaterThanOrEqual=" + DEFAULT_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId is greater than or equal to UPDATED_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.greaterThanOrEqual=" + UPDATED_CITY_PIN_MAPPING_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId is less than or equal to DEFAULT_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.lessThanOrEqual=" + DEFAULT_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId is less than or equal to SMALLER_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.lessThanOrEqual=" + SMALLER_CITY_PIN_MAPPING_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId is less than DEFAULT_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.lessThan=" + DEFAULT_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId is less than UPDATED_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.lessThan=" + UPDATED_CITY_PIN_MAPPING_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityPinMappingIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where cityPinMappingId is greater than DEFAULT_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldNotBeFound("cityPinMappingId.greaterThan=" + DEFAULT_CITY_PIN_MAPPING_ID);

        // Get all the locCityPincodeMappingList where cityPinMappingId is greater than SMALLER_CITY_PIN_MAPPING_ID
        defaultLocCityPincodeMappingShouldBeFound("cityPinMappingId.greaterThan=" + SMALLER_CITY_PIN_MAPPING_ID);
    }


    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByPincodeIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where pincode equals to DEFAULT_PINCODE
        defaultLocCityPincodeMappingShouldBeFound("pincode.equals=" + DEFAULT_PINCODE);

        // Get all the locCityPincodeMappingList where pincode equals to UPDATED_PINCODE
        defaultLocCityPincodeMappingShouldNotBeFound("pincode.equals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByPincodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where pincode not equals to DEFAULT_PINCODE
        defaultLocCityPincodeMappingShouldNotBeFound("pincode.notEquals=" + DEFAULT_PINCODE);

        // Get all the locCityPincodeMappingList where pincode not equals to UPDATED_PINCODE
        defaultLocCityPincodeMappingShouldBeFound("pincode.notEquals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByPincodeIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where pincode in DEFAULT_PINCODE or UPDATED_PINCODE
        defaultLocCityPincodeMappingShouldBeFound("pincode.in=" + DEFAULT_PINCODE + "," + UPDATED_PINCODE);

        // Get all the locCityPincodeMappingList where pincode equals to UPDATED_PINCODE
        defaultLocCityPincodeMappingShouldNotBeFound("pincode.in=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByPincodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where pincode is not null
        defaultLocCityPincodeMappingShouldBeFound("pincode.specified=true");

        // Get all the locCityPincodeMappingList where pincode is null
        defaultLocCityPincodeMappingShouldNotBeFound("pincode.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByPincodeContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where pincode contains DEFAULT_PINCODE
        defaultLocCityPincodeMappingShouldBeFound("pincode.contains=" + DEFAULT_PINCODE);

        // Get all the locCityPincodeMappingList where pincode contains UPDATED_PINCODE
        defaultLocCityPincodeMappingShouldNotBeFound("pincode.contains=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByPincodeNotContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where pincode does not contain DEFAULT_PINCODE
        defaultLocCityPincodeMappingShouldNotBeFound("pincode.doesNotContain=" + DEFAULT_PINCODE);

        // Get all the locCityPincodeMappingList where pincode does not contain UPDATED_PINCODE
        defaultLocCityPincodeMappingShouldBeFound("pincode.doesNotContain=" + UPDATED_PINCODE);
    }


    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocCityPincodeMappingShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locCityPincodeMappingList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocCityPincodeMappingShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocCityPincodeMappingShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locCityPincodeMappingList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocCityPincodeMappingShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocCityPincodeMappingShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locCityPincodeMappingList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocCityPincodeMappingShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where isDeleted is not null
        defaultLocCityPincodeMappingShouldBeFound("isDeleted.specified=true");

        // Get all the locCityPincodeMappingList where isDeleted is null
        defaultLocCityPincodeMappingShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocCityPincodeMappingShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locCityPincodeMappingList where isDeleted contains UPDATED_IS_DELETED
        defaultLocCityPincodeMappingShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocCityPincodeMappingShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locCityPincodeMappingList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocCityPincodeMappingShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocCityPincodeMappingShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locCityPincodeMappingList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocCityPincodeMappingShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocCityPincodeMappingShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locCityPincodeMappingList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocCityPincodeMappingShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocCityPincodeMappingShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locCityPincodeMappingList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocCityPincodeMappingShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdDate is not null
        defaultLocCityPincodeMappingShouldBeFound("createdDate.specified=true");

        // Get all the locCityPincodeMappingList where createdDate is null
        defaultLocCityPincodeMappingShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocCityPincodeMappingShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locCityPincodeMappingList where createdBy equals to UPDATED_CREATED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locCityPincodeMappingList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocCityPincodeMappingShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocCityPincodeMappingShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locCityPincodeMappingList where createdBy equals to UPDATED_CREATED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdBy is not null
        defaultLocCityPincodeMappingShouldBeFound("createdBy.specified=true");

        // Get all the locCityPincodeMappingList where createdBy is null
        defaultLocCityPincodeMappingShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdBy contains DEFAULT_CREATED_BY
        defaultLocCityPincodeMappingShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locCityPincodeMappingList where createdBy contains UPDATED_CREATED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locCityPincodeMappingList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocCityPincodeMappingShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locCityPincodeMappingList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locCityPincodeMappingList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locCityPincodeMappingList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedDate is not null
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locCityPincodeMappingList where lastModifiedDate is null
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityPincodeMappingList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityPincodeMappingList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locCityPincodeMappingList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedBy is not null
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locCityPincodeMappingList where lastModifiedBy is null
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityPincodeMappingList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);

        // Get all the locCityPincodeMappingList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityPincodeMappingList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocCityPincodeMappingShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocCityPincodeMappingsByCityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);
        LocCityMaster cityId = LocCityMasterResourceIT.createEntity(em);
        em.persist(cityId);
        em.flush();
        locCityPincodeMapping.setCityId(cityId);
        locCityPincodeMappingRepository.saveAndFlush(locCityPincodeMapping);
        Long cityIdId = cityId.getId();

        // Get all the locCityPincodeMappingList where cityId equals to cityIdId
        defaultLocCityPincodeMappingShouldBeFound("cityIdId.equals=" + cityIdId);

        // Get all the locCityPincodeMappingList where cityId equals to cityIdId + 1
        defaultLocCityPincodeMappingShouldNotBeFound("cityIdId.equals=" + (cityIdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocCityPincodeMappingShouldBeFound(String filter) throws Exception {
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locCityPincodeMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityPinMappingId").value(hasItem(DEFAULT_CITY_PIN_MAPPING_ID)))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));

        // Check, that the count call also returns 1
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocCityPincodeMappingShouldNotBeFound(String filter) throws Exception {
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocCityPincodeMapping() throws Exception {
        // Get the locCityPincodeMapping
        restLocCityPincodeMappingMockMvc.perform(get("/api/loc-city-pincode-mappings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocCityPincodeMapping() throws Exception {
        // Initialize the database
        locCityPincodeMappingService.save(locCityPincodeMapping);

        int databaseSizeBeforeUpdate = locCityPincodeMappingRepository.findAll().size();

        // Update the locCityPincodeMapping
        LocCityPincodeMapping updatedLocCityPincodeMapping = locCityPincodeMappingRepository.findById(locCityPincodeMapping.getId()).get();
        // Disconnect from session so that the updates on updatedLocCityPincodeMapping are not directly saved in db
        em.detach(updatedLocCityPincodeMapping);
        updatedLocCityPincodeMapping
            .cityPinMappingId(UPDATED_CITY_PIN_MAPPING_ID)
            .pincode(UPDATED_PINCODE)
            .isDeleted(UPDATED_IS_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);

        restLocCityPincodeMappingMockMvc.perform(put("/api/loc-city-pincode-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocCityPincodeMapping)))
            .andExpect(status().isOk());

        // Validate the LocCityPincodeMapping in the database
        List<LocCityPincodeMapping> locCityPincodeMappingList = locCityPincodeMappingRepository.findAll();
        assertThat(locCityPincodeMappingList).hasSize(databaseSizeBeforeUpdate);
        LocCityPincodeMapping testLocCityPincodeMapping = locCityPincodeMappingList.get(locCityPincodeMappingList.size() - 1);
        assertThat(testLocCityPincodeMapping.getCityPinMappingId()).isEqualTo(UPDATED_CITY_PIN_MAPPING_ID);
        assertThat(testLocCityPincodeMapping.getPincode()).isEqualTo(UPDATED_PINCODE);
        assertThat(testLocCityPincodeMapping.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocCityPincodeMapping.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocCityPincodeMapping.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocCityPincodeMapping.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocCityPincodeMapping.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingLocCityPincodeMapping() throws Exception {
        int databaseSizeBeforeUpdate = locCityPincodeMappingRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocCityPincodeMappingMockMvc.perform(put("/api/loc-city-pincode-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityPincodeMapping)))
            .andExpect(status().isBadRequest());

        // Validate the LocCityPincodeMapping in the database
        List<LocCityPincodeMapping> locCityPincodeMappingList = locCityPincodeMappingRepository.findAll();
        assertThat(locCityPincodeMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocCityPincodeMapping() throws Exception {
        // Initialize the database
        locCityPincodeMappingService.save(locCityPincodeMapping);

        int databaseSizeBeforeDelete = locCityPincodeMappingRepository.findAll().size();

        // Delete the locCityPincodeMapping
        restLocCityPincodeMappingMockMvc.perform(delete("/api/loc-city-pincode-mappings/{id}", locCityPincodeMapping.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocCityPincodeMapping> locCityPincodeMappingList = locCityPincodeMappingRepository.findAll();
        assertThat(locCityPincodeMappingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
