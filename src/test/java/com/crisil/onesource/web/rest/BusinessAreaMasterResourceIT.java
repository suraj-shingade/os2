package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.BusinessAreaMaster;
import com.crisil.onesource.repository.BusinessAreaMasterRepository;
import com.crisil.onesource.service.BusinessAreaMasterService;
import com.crisil.onesource.service.dto.BusinessAreaMasterCriteria;
import com.crisil.onesource.service.BusinessAreaMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BusinessAreaMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BusinessAreaMasterResourceIT {

    private static final Integer DEFAULT_BUSINESS_AREA_ID = 1;
    private static final Integer UPDATED_BUSINESS_AREA_ID = 2;
    private static final Integer SMALLER_BUSINESS_AREA_ID = 1 - 1;

    private static final String DEFAULT_BUSINESS_AREA_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BUSINESS_AREA_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private BusinessAreaMasterRepository businessAreaMasterRepository;

    @Autowired
    private BusinessAreaMasterService businessAreaMasterService;

    @Autowired
    private BusinessAreaMasterQueryService businessAreaMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBusinessAreaMasterMockMvc;

    private BusinessAreaMaster businessAreaMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BusinessAreaMaster createEntity(EntityManager em) {
        BusinessAreaMaster businessAreaMaster = new BusinessAreaMaster()
            .businessAreaId(DEFAULT_BUSINESS_AREA_ID)
            .businessAreaName(DEFAULT_BUSINESS_AREA_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isDeleted(DEFAULT_IS_DELETED);
        return businessAreaMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BusinessAreaMaster createUpdatedEntity(EntityManager em) {
        BusinessAreaMaster businessAreaMaster = new BusinessAreaMaster()
            .businessAreaId(UPDATED_BUSINESS_AREA_ID)
            .businessAreaName(UPDATED_BUSINESS_AREA_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);
        return businessAreaMaster;
    }

    @BeforeEach
    public void initTest() {
        businessAreaMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createBusinessAreaMaster() throws Exception {
        int databaseSizeBeforeCreate = businessAreaMasterRepository.findAll().size();
        // Create the BusinessAreaMaster
        restBusinessAreaMasterMockMvc.perform(post("/api/business-area-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(businessAreaMaster)))
            .andExpect(status().isCreated());

        // Validate the BusinessAreaMaster in the database
        List<BusinessAreaMaster> businessAreaMasterList = businessAreaMasterRepository.findAll();
        assertThat(businessAreaMasterList).hasSize(databaseSizeBeforeCreate + 1);
        BusinessAreaMaster testBusinessAreaMaster = businessAreaMasterList.get(businessAreaMasterList.size() - 1);
        assertThat(testBusinessAreaMaster.getBusinessAreaId()).isEqualTo(DEFAULT_BUSINESS_AREA_ID);
        assertThat(testBusinessAreaMaster.getBusinessAreaName()).isEqualTo(DEFAULT_BUSINESS_AREA_NAME);
        assertThat(testBusinessAreaMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testBusinessAreaMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testBusinessAreaMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testBusinessAreaMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testBusinessAreaMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createBusinessAreaMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = businessAreaMasterRepository.findAll().size();

        // Create the BusinessAreaMaster with an existing ID
        businessAreaMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBusinessAreaMasterMockMvc.perform(post("/api/business-area-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(businessAreaMaster)))
            .andExpect(status().isBadRequest());

        // Validate the BusinessAreaMaster in the database
        List<BusinessAreaMaster> businessAreaMasterList = businessAreaMasterRepository.findAll();
        assertThat(businessAreaMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkBusinessAreaIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = businessAreaMasterRepository.findAll().size();
        // set the field null
        businessAreaMaster.setBusinessAreaId(null);

        // Create the BusinessAreaMaster, which fails.


        restBusinessAreaMasterMockMvc.perform(post("/api/business-area-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(businessAreaMaster)))
            .andExpect(status().isBadRequest());

        List<BusinessAreaMaster> businessAreaMasterList = businessAreaMasterRepository.findAll();
        assertThat(businessAreaMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMasters() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(businessAreaMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].businessAreaId").value(hasItem(DEFAULT_BUSINESS_AREA_ID)))
            .andExpect(jsonPath("$.[*].businessAreaName").value(hasItem(DEFAULT_BUSINESS_AREA_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getBusinessAreaMaster() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get the businessAreaMaster
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters/{id}", businessAreaMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(businessAreaMaster.getId().intValue()))
            .andExpect(jsonPath("$.businessAreaId").value(DEFAULT_BUSINESS_AREA_ID))
            .andExpect(jsonPath("$.businessAreaName").value(DEFAULT_BUSINESS_AREA_NAME))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getBusinessAreaMastersByIdFiltering() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        Long id = businessAreaMaster.getId();

        defaultBusinessAreaMasterShouldBeFound("id.equals=" + id);
        defaultBusinessAreaMasterShouldNotBeFound("id.notEquals=" + id);

        defaultBusinessAreaMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBusinessAreaMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultBusinessAreaMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBusinessAreaMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId equals to DEFAULT_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.equals=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId equals to UPDATED_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.equals=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId not equals to DEFAULT_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.notEquals=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId not equals to UPDATED_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.notEquals=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId in DEFAULT_BUSINESS_AREA_ID or UPDATED_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.in=" + DEFAULT_BUSINESS_AREA_ID + "," + UPDATED_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId equals to UPDATED_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.in=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId is not null
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.specified=true");

        // Get all the businessAreaMasterList where businessAreaId is null
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.specified=false");
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId is greater than or equal to DEFAULT_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.greaterThanOrEqual=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId is greater than or equal to UPDATED_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.greaterThanOrEqual=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId is less than or equal to DEFAULT_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.lessThanOrEqual=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId is less than or equal to SMALLER_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.lessThanOrEqual=" + SMALLER_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsLessThanSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId is less than DEFAULT_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.lessThan=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId is less than UPDATED_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.lessThan=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaId is greater than DEFAULT_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaId.greaterThan=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the businessAreaMasterList where businessAreaId is greater than SMALLER_BUSINESS_AREA_ID
        defaultBusinessAreaMasterShouldBeFound("businessAreaId.greaterThan=" + SMALLER_BUSINESS_AREA_ID);
    }


    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaNameIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaName equals to DEFAULT_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldBeFound("businessAreaName.equals=" + DEFAULT_BUSINESS_AREA_NAME);

        // Get all the businessAreaMasterList where businessAreaName equals to UPDATED_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaName.equals=" + UPDATED_BUSINESS_AREA_NAME);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaName not equals to DEFAULT_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaName.notEquals=" + DEFAULT_BUSINESS_AREA_NAME);

        // Get all the businessAreaMasterList where businessAreaName not equals to UPDATED_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldBeFound("businessAreaName.notEquals=" + UPDATED_BUSINESS_AREA_NAME);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaNameIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaName in DEFAULT_BUSINESS_AREA_NAME or UPDATED_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldBeFound("businessAreaName.in=" + DEFAULT_BUSINESS_AREA_NAME + "," + UPDATED_BUSINESS_AREA_NAME);

        // Get all the businessAreaMasterList where businessAreaName equals to UPDATED_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaName.in=" + UPDATED_BUSINESS_AREA_NAME);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaName is not null
        defaultBusinessAreaMasterShouldBeFound("businessAreaName.specified=true");

        // Get all the businessAreaMasterList where businessAreaName is null
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaName.specified=false");
    }
                @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaNameContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaName contains DEFAULT_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldBeFound("businessAreaName.contains=" + DEFAULT_BUSINESS_AREA_NAME);

        // Get all the businessAreaMasterList where businessAreaName contains UPDATED_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaName.contains=" + UPDATED_BUSINESS_AREA_NAME);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByBusinessAreaNameNotContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where businessAreaName does not contain DEFAULT_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldNotBeFound("businessAreaName.doesNotContain=" + DEFAULT_BUSINESS_AREA_NAME);

        // Get all the businessAreaMasterList where businessAreaName does not contain UPDATED_BUSINESS_AREA_NAME
        defaultBusinessAreaMasterShouldBeFound("businessAreaName.doesNotContain=" + UPDATED_BUSINESS_AREA_NAME);
    }


    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultBusinessAreaMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the businessAreaMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultBusinessAreaMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultBusinessAreaMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the businessAreaMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultBusinessAreaMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultBusinessAreaMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the businessAreaMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultBusinessAreaMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdDate is not null
        defaultBusinessAreaMasterShouldBeFound("createdDate.specified=true");

        // Get all the businessAreaMasterList where createdDate is null
        defaultBusinessAreaMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultBusinessAreaMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the businessAreaMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultBusinessAreaMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultBusinessAreaMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the businessAreaMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultBusinessAreaMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultBusinessAreaMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the businessAreaMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultBusinessAreaMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdBy is not null
        defaultBusinessAreaMasterShouldBeFound("createdBy.specified=true");

        // Get all the businessAreaMasterList where createdBy is null
        defaultBusinessAreaMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultBusinessAreaMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the businessAreaMasterList where createdBy contains UPDATED_CREATED_BY
        defaultBusinessAreaMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultBusinessAreaMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the businessAreaMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultBusinessAreaMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultBusinessAreaMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the businessAreaMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the businessAreaMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultBusinessAreaMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultBusinessAreaMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the businessAreaMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedDate is not null
        defaultBusinessAreaMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the businessAreaMasterList where lastModifiedDate is null
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the businessAreaMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the businessAreaMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the businessAreaMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedBy is not null
        defaultBusinessAreaMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the businessAreaMasterList where lastModifiedBy is null
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the businessAreaMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the businessAreaMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultBusinessAreaMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllBusinessAreaMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultBusinessAreaMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the businessAreaMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultBusinessAreaMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultBusinessAreaMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the businessAreaMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultBusinessAreaMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultBusinessAreaMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the businessAreaMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultBusinessAreaMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where isDeleted is not null
        defaultBusinessAreaMasterShouldBeFound("isDeleted.specified=true");

        // Get all the businessAreaMasterList where isDeleted is null
        defaultBusinessAreaMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllBusinessAreaMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultBusinessAreaMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the businessAreaMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultBusinessAreaMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllBusinessAreaMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        businessAreaMasterRepository.saveAndFlush(businessAreaMaster);

        // Get all the businessAreaMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultBusinessAreaMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the businessAreaMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultBusinessAreaMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBusinessAreaMasterShouldBeFound(String filter) throws Exception {
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(businessAreaMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].businessAreaId").value(hasItem(DEFAULT_BUSINESS_AREA_ID)))
            .andExpect(jsonPath("$.[*].businessAreaName").value(hasItem(DEFAULT_BUSINESS_AREA_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBusinessAreaMasterShouldNotBeFound(String filter) throws Exception {
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingBusinessAreaMaster() throws Exception {
        // Get the businessAreaMaster
        restBusinessAreaMasterMockMvc.perform(get("/api/business-area-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBusinessAreaMaster() throws Exception {
        // Initialize the database
        businessAreaMasterService.save(businessAreaMaster);

        int databaseSizeBeforeUpdate = businessAreaMasterRepository.findAll().size();

        // Update the businessAreaMaster
        BusinessAreaMaster updatedBusinessAreaMaster = businessAreaMasterRepository.findById(businessAreaMaster.getId()).get();
        // Disconnect from session so that the updates on updatedBusinessAreaMaster are not directly saved in db
        em.detach(updatedBusinessAreaMaster);
        updatedBusinessAreaMaster
            .businessAreaId(UPDATED_BUSINESS_AREA_ID)
            .businessAreaName(UPDATED_BUSINESS_AREA_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);

        restBusinessAreaMasterMockMvc.perform(put("/api/business-area-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBusinessAreaMaster)))
            .andExpect(status().isOk());

        // Validate the BusinessAreaMaster in the database
        List<BusinessAreaMaster> businessAreaMasterList = businessAreaMasterRepository.findAll();
        assertThat(businessAreaMasterList).hasSize(databaseSizeBeforeUpdate);
        BusinessAreaMaster testBusinessAreaMaster = businessAreaMasterList.get(businessAreaMasterList.size() - 1);
        assertThat(testBusinessAreaMaster.getBusinessAreaId()).isEqualTo(UPDATED_BUSINESS_AREA_ID);
        assertThat(testBusinessAreaMaster.getBusinessAreaName()).isEqualTo(UPDATED_BUSINESS_AREA_NAME);
        assertThat(testBusinessAreaMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testBusinessAreaMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testBusinessAreaMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testBusinessAreaMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testBusinessAreaMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingBusinessAreaMaster() throws Exception {
        int databaseSizeBeforeUpdate = businessAreaMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBusinessAreaMasterMockMvc.perform(put("/api/business-area-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(businessAreaMaster)))
            .andExpect(status().isBadRequest());

        // Validate the BusinessAreaMaster in the database
        List<BusinessAreaMaster> businessAreaMasterList = businessAreaMasterRepository.findAll();
        assertThat(businessAreaMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBusinessAreaMaster() throws Exception {
        // Initialize the database
        businessAreaMasterService.save(businessAreaMaster);

        int databaseSizeBeforeDelete = businessAreaMasterRepository.findAll().size();

        // Delete the businessAreaMaster
        restBusinessAreaMasterMockMvc.perform(delete("/api/business-area-masters/{id}", businessAreaMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BusinessAreaMaster> businessAreaMasterList = businessAreaMasterRepository.findAll();
        assertThat(businessAreaMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
