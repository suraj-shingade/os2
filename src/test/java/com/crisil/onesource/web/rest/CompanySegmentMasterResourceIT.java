package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanySegmentMaster;
import com.crisil.onesource.repository.CompanySegmentMasterRepository;
import com.crisil.onesource.service.CompanySegmentMasterService;
import com.crisil.onesource.service.dto.CompanySegmentMasterCriteria;
import com.crisil.onesource.service.CompanySegmentMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanySegmentMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanySegmentMasterResourceIT {

    private static final Integer DEFAULT_SEGMENT_ID = 1;
    private static final Integer UPDATED_SEGMENT_ID = 2;
    private static final Integer SMALLER_SEGMENT_ID = 1 - 1;

    private static final String DEFAULT_SEGMENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SEGMENT_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private CompanySegmentMasterRepository companySegmentMasterRepository;

    @Autowired
    private CompanySegmentMasterService companySegmentMasterService;

    @Autowired
    private CompanySegmentMasterQueryService companySegmentMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanySegmentMasterMockMvc;

    private CompanySegmentMaster companySegmentMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanySegmentMaster createEntity(EntityManager em) {
        CompanySegmentMaster companySegmentMaster = new CompanySegmentMaster()
            .segmentId(DEFAULT_SEGMENT_ID)
            .segmentName(DEFAULT_SEGMENT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isDeleted(DEFAULT_IS_DELETED);
        return companySegmentMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanySegmentMaster createUpdatedEntity(EntityManager em) {
        CompanySegmentMaster companySegmentMaster = new CompanySegmentMaster()
            .segmentId(UPDATED_SEGMENT_ID)
            .segmentName(UPDATED_SEGMENT_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);
        return companySegmentMaster;
    }

    @BeforeEach
    public void initTest() {
        companySegmentMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanySegmentMaster() throws Exception {
        int databaseSizeBeforeCreate = companySegmentMasterRepository.findAll().size();
        // Create the CompanySegmentMaster
        restCompanySegmentMasterMockMvc.perform(post("/api/company-segment-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySegmentMaster)))
            .andExpect(status().isCreated());

        // Validate the CompanySegmentMaster in the database
        List<CompanySegmentMaster> companySegmentMasterList = companySegmentMasterRepository.findAll();
        assertThat(companySegmentMasterList).hasSize(databaseSizeBeforeCreate + 1);
        CompanySegmentMaster testCompanySegmentMaster = companySegmentMasterList.get(companySegmentMasterList.size() - 1);
        assertThat(testCompanySegmentMaster.getSegmentId()).isEqualTo(DEFAULT_SEGMENT_ID);
        assertThat(testCompanySegmentMaster.getSegmentName()).isEqualTo(DEFAULT_SEGMENT_NAME);
        assertThat(testCompanySegmentMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanySegmentMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanySegmentMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanySegmentMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanySegmentMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createCompanySegmentMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companySegmentMasterRepository.findAll().size();

        // Create the CompanySegmentMaster with an existing ID
        companySegmentMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanySegmentMasterMockMvc.perform(post("/api/company-segment-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySegmentMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanySegmentMaster in the database
        List<CompanySegmentMaster> companySegmentMasterList = companySegmentMasterRepository.findAll();
        assertThat(companySegmentMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSegmentIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = companySegmentMasterRepository.findAll().size();
        // set the field null
        companySegmentMaster.setSegmentId(null);

        // Create the CompanySegmentMaster, which fails.


        restCompanySegmentMasterMockMvc.perform(post("/api/company-segment-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySegmentMaster)))
            .andExpect(status().isBadRequest());

        List<CompanySegmentMaster> companySegmentMasterList = companySegmentMasterRepository.findAll();
        assertThat(companySegmentMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMasters() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companySegmentMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].segmentId").value(hasItem(DEFAULT_SEGMENT_ID)))
            .andExpect(jsonPath("$.[*].segmentName").value(hasItem(DEFAULT_SEGMENT_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getCompanySegmentMaster() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get the companySegmentMaster
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters/{id}", companySegmentMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companySegmentMaster.getId().intValue()))
            .andExpect(jsonPath("$.segmentId").value(DEFAULT_SEGMENT_ID))
            .andExpect(jsonPath("$.segmentName").value(DEFAULT_SEGMENT_NAME))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getCompanySegmentMastersByIdFiltering() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        Long id = companySegmentMaster.getId();

        defaultCompanySegmentMasterShouldBeFound("id.equals=" + id);
        defaultCompanySegmentMasterShouldNotBeFound("id.notEquals=" + id);

        defaultCompanySegmentMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanySegmentMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanySegmentMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanySegmentMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId equals to DEFAULT_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.equals=" + DEFAULT_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId equals to UPDATED_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.equals=" + UPDATED_SEGMENT_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId not equals to DEFAULT_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.notEquals=" + DEFAULT_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId not equals to UPDATED_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.notEquals=" + UPDATED_SEGMENT_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId in DEFAULT_SEGMENT_ID or UPDATED_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.in=" + DEFAULT_SEGMENT_ID + "," + UPDATED_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId equals to UPDATED_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.in=" + UPDATED_SEGMENT_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId is not null
        defaultCompanySegmentMasterShouldBeFound("segmentId.specified=true");

        // Get all the companySegmentMasterList where segmentId is null
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId is greater than or equal to DEFAULT_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.greaterThanOrEqual=" + DEFAULT_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId is greater than or equal to UPDATED_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.greaterThanOrEqual=" + UPDATED_SEGMENT_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId is less than or equal to DEFAULT_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.lessThanOrEqual=" + DEFAULT_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId is less than or equal to SMALLER_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.lessThanOrEqual=" + SMALLER_SEGMENT_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId is less than DEFAULT_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.lessThan=" + DEFAULT_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId is less than UPDATED_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.lessThan=" + UPDATED_SEGMENT_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentId is greater than DEFAULT_SEGMENT_ID
        defaultCompanySegmentMasterShouldNotBeFound("segmentId.greaterThan=" + DEFAULT_SEGMENT_ID);

        // Get all the companySegmentMasterList where segmentId is greater than SMALLER_SEGMENT_ID
        defaultCompanySegmentMasterShouldBeFound("segmentId.greaterThan=" + SMALLER_SEGMENT_ID);
    }


    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentName equals to DEFAULT_SEGMENT_NAME
        defaultCompanySegmentMasterShouldBeFound("segmentName.equals=" + DEFAULT_SEGMENT_NAME);

        // Get all the companySegmentMasterList where segmentName equals to UPDATED_SEGMENT_NAME
        defaultCompanySegmentMasterShouldNotBeFound("segmentName.equals=" + UPDATED_SEGMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentName not equals to DEFAULT_SEGMENT_NAME
        defaultCompanySegmentMasterShouldNotBeFound("segmentName.notEquals=" + DEFAULT_SEGMENT_NAME);

        // Get all the companySegmentMasterList where segmentName not equals to UPDATED_SEGMENT_NAME
        defaultCompanySegmentMasterShouldBeFound("segmentName.notEquals=" + UPDATED_SEGMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentNameIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentName in DEFAULT_SEGMENT_NAME or UPDATED_SEGMENT_NAME
        defaultCompanySegmentMasterShouldBeFound("segmentName.in=" + DEFAULT_SEGMENT_NAME + "," + UPDATED_SEGMENT_NAME);

        // Get all the companySegmentMasterList where segmentName equals to UPDATED_SEGMENT_NAME
        defaultCompanySegmentMasterShouldNotBeFound("segmentName.in=" + UPDATED_SEGMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentName is not null
        defaultCompanySegmentMasterShouldBeFound("segmentName.specified=true");

        // Get all the companySegmentMasterList where segmentName is null
        defaultCompanySegmentMasterShouldNotBeFound("segmentName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentNameContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentName contains DEFAULT_SEGMENT_NAME
        defaultCompanySegmentMasterShouldBeFound("segmentName.contains=" + DEFAULT_SEGMENT_NAME);

        // Get all the companySegmentMasterList where segmentName contains UPDATED_SEGMENT_NAME
        defaultCompanySegmentMasterShouldNotBeFound("segmentName.contains=" + UPDATED_SEGMENT_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersBySegmentNameNotContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where segmentName does not contain DEFAULT_SEGMENT_NAME
        defaultCompanySegmentMasterShouldNotBeFound("segmentName.doesNotContain=" + DEFAULT_SEGMENT_NAME);

        // Get all the companySegmentMasterList where segmentName does not contain UPDATED_SEGMENT_NAME
        defaultCompanySegmentMasterShouldBeFound("segmentName.doesNotContain=" + UPDATED_SEGMENT_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanySegmentMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companySegmentMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanySegmentMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanySegmentMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companySegmentMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanySegmentMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanySegmentMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companySegmentMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanySegmentMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdDate is not null
        defaultCompanySegmentMasterShouldBeFound("createdDate.specified=true");

        // Get all the companySegmentMasterList where createdDate is null
        defaultCompanySegmentMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanySegmentMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companySegmentMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanySegmentMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanySegmentMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companySegmentMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanySegmentMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanySegmentMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companySegmentMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanySegmentMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdBy is not null
        defaultCompanySegmentMasterShouldBeFound("createdBy.specified=true");

        // Get all the companySegmentMasterList where createdBy is null
        defaultCompanySegmentMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanySegmentMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companySegmentMasterList where createdBy contains UPDATED_CREATED_BY
        defaultCompanySegmentMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanySegmentMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companySegmentMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanySegmentMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySegmentMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySegmentMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySegmentMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySegmentMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanySegmentMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companySegmentMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedDate is not null
        defaultCompanySegmentMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companySegmentMasterList where lastModifiedDate is null
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySegmentMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySegmentMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companySegmentMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedBy is not null
        defaultCompanySegmentMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companySegmentMasterList where lastModifiedBy is null
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySegmentMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySegmentMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanySegmentMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanySegmentMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanySegmentMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companySegmentMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanySegmentMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanySegmentMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companySegmentMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanySegmentMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanySegmentMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companySegmentMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanySegmentMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where isDeleted is not null
        defaultCompanySegmentMasterShouldBeFound("isDeleted.specified=true");

        // Get all the companySegmentMasterList where isDeleted is null
        defaultCompanySegmentMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySegmentMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanySegmentMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companySegmentMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanySegmentMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySegmentMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companySegmentMasterRepository.saveAndFlush(companySegmentMaster);

        // Get all the companySegmentMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanySegmentMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companySegmentMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanySegmentMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanySegmentMasterShouldBeFound(String filter) throws Exception {
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companySegmentMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].segmentId").value(hasItem(DEFAULT_SEGMENT_ID)))
            .andExpect(jsonPath("$.[*].segmentName").value(hasItem(DEFAULT_SEGMENT_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanySegmentMasterShouldNotBeFound(String filter) throws Exception {
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanySegmentMaster() throws Exception {
        // Get the companySegmentMaster
        restCompanySegmentMasterMockMvc.perform(get("/api/company-segment-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanySegmentMaster() throws Exception {
        // Initialize the database
        companySegmentMasterService.save(companySegmentMaster);

        int databaseSizeBeforeUpdate = companySegmentMasterRepository.findAll().size();

        // Update the companySegmentMaster
        CompanySegmentMaster updatedCompanySegmentMaster = companySegmentMasterRepository.findById(companySegmentMaster.getId()).get();
        // Disconnect from session so that the updates on updatedCompanySegmentMaster are not directly saved in db
        em.detach(updatedCompanySegmentMaster);
        updatedCompanySegmentMaster
            .segmentId(UPDATED_SEGMENT_ID)
            .segmentName(UPDATED_SEGMENT_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);

        restCompanySegmentMasterMockMvc.perform(put("/api/company-segment-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanySegmentMaster)))
            .andExpect(status().isOk());

        // Validate the CompanySegmentMaster in the database
        List<CompanySegmentMaster> companySegmentMasterList = companySegmentMasterRepository.findAll();
        assertThat(companySegmentMasterList).hasSize(databaseSizeBeforeUpdate);
        CompanySegmentMaster testCompanySegmentMaster = companySegmentMasterList.get(companySegmentMasterList.size() - 1);
        assertThat(testCompanySegmentMaster.getSegmentId()).isEqualTo(UPDATED_SEGMENT_ID);
        assertThat(testCompanySegmentMaster.getSegmentName()).isEqualTo(UPDATED_SEGMENT_NAME);
        assertThat(testCompanySegmentMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanySegmentMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanySegmentMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanySegmentMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanySegmentMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanySegmentMaster() throws Exception {
        int databaseSizeBeforeUpdate = companySegmentMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanySegmentMasterMockMvc.perform(put("/api/company-segment-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySegmentMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanySegmentMaster in the database
        List<CompanySegmentMaster> companySegmentMasterList = companySegmentMasterRepository.findAll();
        assertThat(companySegmentMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanySegmentMaster() throws Exception {
        // Initialize the database
        companySegmentMasterService.save(companySegmentMaster);

        int databaseSizeBeforeDelete = companySegmentMasterRepository.findAll().size();

        // Delete the companySegmentMaster
        restCompanySegmentMasterMockMvc.perform(delete("/api/company-segment-masters/{id}", companySegmentMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanySegmentMaster> companySegmentMasterList = companySegmentMasterRepository.findAll();
        assertThat(companySegmentMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
