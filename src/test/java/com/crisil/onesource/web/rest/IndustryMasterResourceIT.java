package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.IndustryMaster;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.IndustryMasterRepository;
import com.crisil.onesource.service.IndustryMasterService;
import com.crisil.onesource.service.dto.IndustryMasterCriteria;
import com.crisil.onesource.service.IndustryMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link IndustryMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class IndustryMasterResourceIT {

    private static final Integer DEFAULT_INDUSTRY_ID = 1;
    private static final Integer UPDATED_INDUSTRY_ID = 2;
    private static final Integer SMALLER_INDUSTRY_ID = 1 - 1;

    private static final String DEFAULT_INDUSTRY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_INDUSTRY_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private IndustryMasterRepository industryMasterRepository;

    @Autowired
    private IndustryMasterService industryMasterService;

    @Autowired
    private IndustryMasterQueryService industryMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restIndustryMasterMockMvc;

    private IndustryMaster industryMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndustryMaster createEntity(EntityManager em) {
        IndustryMaster industryMaster = new IndustryMaster()
            .industryId(DEFAULT_INDUSTRY_ID)
            .industryName(DEFAULT_INDUSTRY_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isDeleted(DEFAULT_IS_DELETED);
        return industryMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IndustryMaster createUpdatedEntity(EntityManager em) {
        IndustryMaster industryMaster = new IndustryMaster()
            .industryId(UPDATED_INDUSTRY_ID)
            .industryName(UPDATED_INDUSTRY_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);
        return industryMaster;
    }

    @BeforeEach
    public void initTest() {
        industryMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createIndustryMaster() throws Exception {
        int databaseSizeBeforeCreate = industryMasterRepository.findAll().size();
        // Create the IndustryMaster
        restIndustryMasterMockMvc.perform(post("/api/industry-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(industryMaster)))
            .andExpect(status().isCreated());

        // Validate the IndustryMaster in the database
        List<IndustryMaster> industryMasterList = industryMasterRepository.findAll();
        assertThat(industryMasterList).hasSize(databaseSizeBeforeCreate + 1);
        IndustryMaster testIndustryMaster = industryMasterList.get(industryMasterList.size() - 1);
        assertThat(testIndustryMaster.getIndustryId()).isEqualTo(DEFAULT_INDUSTRY_ID);
        assertThat(testIndustryMaster.getIndustryName()).isEqualTo(DEFAULT_INDUSTRY_NAME);
        assertThat(testIndustryMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testIndustryMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testIndustryMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testIndustryMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testIndustryMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createIndustryMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = industryMasterRepository.findAll().size();

        // Create the IndustryMaster with an existing ID
        industryMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIndustryMasterMockMvc.perform(post("/api/industry-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(industryMaster)))
            .andExpect(status().isBadRequest());

        // Validate the IndustryMaster in the database
        List<IndustryMaster> industryMasterList = industryMasterRepository.findAll();
        assertThat(industryMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIndustryIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = industryMasterRepository.findAll().size();
        // set the field null
        industryMaster.setIndustryId(null);

        // Create the IndustryMaster, which fails.


        restIndustryMasterMockMvc.perform(post("/api/industry-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(industryMaster)))
            .andExpect(status().isBadRequest());

        List<IndustryMaster> industryMasterList = industryMasterRepository.findAll();
        assertThat(industryMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIndustryMasters() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList
        restIndustryMasterMockMvc.perform(get("/api/industry-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(industryMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].industryId").value(hasItem(DEFAULT_INDUSTRY_ID)))
            .andExpect(jsonPath("$.[*].industryName").value(hasItem(DEFAULT_INDUSTRY_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getIndustryMaster() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get the industryMaster
        restIndustryMasterMockMvc.perform(get("/api/industry-masters/{id}", industryMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(industryMaster.getId().intValue()))
            .andExpect(jsonPath("$.industryId").value(DEFAULT_INDUSTRY_ID))
            .andExpect(jsonPath("$.industryName").value(DEFAULT_INDUSTRY_NAME))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getIndustryMastersByIdFiltering() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        Long id = industryMaster.getId();

        defaultIndustryMasterShouldBeFound("id.equals=" + id);
        defaultIndustryMasterShouldNotBeFound("id.notEquals=" + id);

        defaultIndustryMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultIndustryMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultIndustryMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultIndustryMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId equals to DEFAULT_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.equals=" + DEFAULT_INDUSTRY_ID);

        // Get all the industryMasterList where industryId equals to UPDATED_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.equals=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId not equals to DEFAULT_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.notEquals=" + DEFAULT_INDUSTRY_ID);

        // Get all the industryMasterList where industryId not equals to UPDATED_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.notEquals=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId in DEFAULT_INDUSTRY_ID or UPDATED_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.in=" + DEFAULT_INDUSTRY_ID + "," + UPDATED_INDUSTRY_ID);

        // Get all the industryMasterList where industryId equals to UPDATED_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.in=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId is not null
        defaultIndustryMasterShouldBeFound("industryId.specified=true");

        // Get all the industryMasterList where industryId is null
        defaultIndustryMasterShouldNotBeFound("industryId.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId is greater than or equal to DEFAULT_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.greaterThanOrEqual=" + DEFAULT_INDUSTRY_ID);

        // Get all the industryMasterList where industryId is greater than or equal to UPDATED_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.greaterThanOrEqual=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId is less than or equal to DEFAULT_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.lessThanOrEqual=" + DEFAULT_INDUSTRY_ID);

        // Get all the industryMasterList where industryId is less than or equal to SMALLER_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.lessThanOrEqual=" + SMALLER_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsLessThanSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId is less than DEFAULT_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.lessThan=" + DEFAULT_INDUSTRY_ID);

        // Get all the industryMasterList where industryId is less than UPDATED_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.lessThan=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryId is greater than DEFAULT_INDUSTRY_ID
        defaultIndustryMasterShouldNotBeFound("industryId.greaterThan=" + DEFAULT_INDUSTRY_ID);

        // Get all the industryMasterList where industryId is greater than SMALLER_INDUSTRY_ID
        defaultIndustryMasterShouldBeFound("industryId.greaterThan=" + SMALLER_INDUSTRY_ID);
    }


    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryNameIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryName equals to DEFAULT_INDUSTRY_NAME
        defaultIndustryMasterShouldBeFound("industryName.equals=" + DEFAULT_INDUSTRY_NAME);

        // Get all the industryMasterList where industryName equals to UPDATED_INDUSTRY_NAME
        defaultIndustryMasterShouldNotBeFound("industryName.equals=" + UPDATED_INDUSTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryName not equals to DEFAULT_INDUSTRY_NAME
        defaultIndustryMasterShouldNotBeFound("industryName.notEquals=" + DEFAULT_INDUSTRY_NAME);

        // Get all the industryMasterList where industryName not equals to UPDATED_INDUSTRY_NAME
        defaultIndustryMasterShouldBeFound("industryName.notEquals=" + UPDATED_INDUSTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryNameIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryName in DEFAULT_INDUSTRY_NAME or UPDATED_INDUSTRY_NAME
        defaultIndustryMasterShouldBeFound("industryName.in=" + DEFAULT_INDUSTRY_NAME + "," + UPDATED_INDUSTRY_NAME);

        // Get all the industryMasterList where industryName equals to UPDATED_INDUSTRY_NAME
        defaultIndustryMasterShouldNotBeFound("industryName.in=" + UPDATED_INDUSTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryName is not null
        defaultIndustryMasterShouldBeFound("industryName.specified=true");

        // Get all the industryMasterList where industryName is null
        defaultIndustryMasterShouldNotBeFound("industryName.specified=false");
    }
                @Test
    @Transactional
    public void getAllIndustryMastersByIndustryNameContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryName contains DEFAULT_INDUSTRY_NAME
        defaultIndustryMasterShouldBeFound("industryName.contains=" + DEFAULT_INDUSTRY_NAME);

        // Get all the industryMasterList where industryName contains UPDATED_INDUSTRY_NAME
        defaultIndustryMasterShouldNotBeFound("industryName.contains=" + UPDATED_INDUSTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIndustryNameNotContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where industryName does not contain DEFAULT_INDUSTRY_NAME
        defaultIndustryMasterShouldNotBeFound("industryName.doesNotContain=" + DEFAULT_INDUSTRY_NAME);

        // Get all the industryMasterList where industryName does not contain UPDATED_INDUSTRY_NAME
        defaultIndustryMasterShouldBeFound("industryName.doesNotContain=" + UPDATED_INDUSTRY_NAME);
    }


    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultIndustryMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the industryMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultIndustryMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultIndustryMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the industryMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultIndustryMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultIndustryMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the industryMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultIndustryMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdDate is not null
        defaultIndustryMasterShouldBeFound("createdDate.specified=true");

        // Get all the industryMasterList where createdDate is null
        defaultIndustryMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultIndustryMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the industryMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultIndustryMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultIndustryMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the industryMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultIndustryMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultIndustryMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the industryMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultIndustryMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdBy is not null
        defaultIndustryMasterShouldBeFound("createdBy.specified=true");

        // Get all the industryMasterList where createdBy is null
        defaultIndustryMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllIndustryMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultIndustryMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the industryMasterList where createdBy contains UPDATED_CREATED_BY
        defaultIndustryMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultIndustryMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the industryMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultIndustryMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultIndustryMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the industryMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultIndustryMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultIndustryMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the industryMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultIndustryMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultIndustryMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the industryMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultIndustryMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedDate is not null
        defaultIndustryMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the industryMasterList where lastModifiedDate is null
        defaultIndustryMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultIndustryMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the industryMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultIndustryMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultIndustryMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the industryMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultIndustryMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultIndustryMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the industryMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultIndustryMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedBy is not null
        defaultIndustryMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the industryMasterList where lastModifiedBy is null
        defaultIndustryMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultIndustryMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the industryMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultIndustryMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultIndustryMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the industryMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultIndustryMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllIndustryMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultIndustryMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the industryMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultIndustryMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultIndustryMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the industryMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultIndustryMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultIndustryMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the industryMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultIndustryMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where isDeleted is not null
        defaultIndustryMasterShouldBeFound("isDeleted.specified=true");

        // Get all the industryMasterList where isDeleted is null
        defaultIndustryMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllIndustryMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultIndustryMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the industryMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultIndustryMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllIndustryMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);

        // Get all the industryMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultIndustryMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the industryMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultIndustryMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllIndustryMastersByOnesourceCompanyMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        industryMasterRepository.saveAndFlush(industryMaster);
        OnesourceCompanyMaster onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(onesourceCompanyMaster);
        em.flush();
        industryMaster.addOnesourceCompanyMaster(onesourceCompanyMaster);
        industryMasterRepository.saveAndFlush(industryMaster);
        Long onesourceCompanyMasterId = onesourceCompanyMaster.getId();

        // Get all the industryMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId
        defaultIndustryMasterShouldBeFound("onesourceCompanyMasterId.equals=" + onesourceCompanyMasterId);

        // Get all the industryMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId + 1
        defaultIndustryMasterShouldNotBeFound("onesourceCompanyMasterId.equals=" + (onesourceCompanyMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultIndustryMasterShouldBeFound(String filter) throws Exception {
        restIndustryMasterMockMvc.perform(get("/api/industry-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(industryMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].industryId").value(hasItem(DEFAULT_INDUSTRY_ID)))
            .andExpect(jsonPath("$.[*].industryName").value(hasItem(DEFAULT_INDUSTRY_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restIndustryMasterMockMvc.perform(get("/api/industry-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultIndustryMasterShouldNotBeFound(String filter) throws Exception {
        restIndustryMasterMockMvc.perform(get("/api/industry-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restIndustryMasterMockMvc.perform(get("/api/industry-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingIndustryMaster() throws Exception {
        // Get the industryMaster
        restIndustryMasterMockMvc.perform(get("/api/industry-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIndustryMaster() throws Exception {
        // Initialize the database
        industryMasterService.save(industryMaster);

        int databaseSizeBeforeUpdate = industryMasterRepository.findAll().size();

        // Update the industryMaster
        IndustryMaster updatedIndustryMaster = industryMasterRepository.findById(industryMaster.getId()).get();
        // Disconnect from session so that the updates on updatedIndustryMaster are not directly saved in db
        em.detach(updatedIndustryMaster);
        updatedIndustryMaster
            .industryId(UPDATED_INDUSTRY_ID)
            .industryName(UPDATED_INDUSTRY_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);

        restIndustryMasterMockMvc.perform(put("/api/industry-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedIndustryMaster)))
            .andExpect(status().isOk());

        // Validate the IndustryMaster in the database
        List<IndustryMaster> industryMasterList = industryMasterRepository.findAll();
        assertThat(industryMasterList).hasSize(databaseSizeBeforeUpdate);
        IndustryMaster testIndustryMaster = industryMasterList.get(industryMasterList.size() - 1);
        assertThat(testIndustryMaster.getIndustryId()).isEqualTo(UPDATED_INDUSTRY_ID);
        assertThat(testIndustryMaster.getIndustryName()).isEqualTo(UPDATED_INDUSTRY_NAME);
        assertThat(testIndustryMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testIndustryMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testIndustryMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testIndustryMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testIndustryMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingIndustryMaster() throws Exception {
        int databaseSizeBeforeUpdate = industryMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIndustryMasterMockMvc.perform(put("/api/industry-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(industryMaster)))
            .andExpect(status().isBadRequest());

        // Validate the IndustryMaster in the database
        List<IndustryMaster> industryMasterList = industryMasterRepository.findAll();
        assertThat(industryMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIndustryMaster() throws Exception {
        // Initialize the database
        industryMasterService.save(industryMaster);

        int databaseSizeBeforeDelete = industryMasterRepository.findAll().size();

        // Delete the industryMaster
        restIndustryMasterMockMvc.perform(delete("/api/industry-masters/{id}", industryMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IndustryMaster> industryMasterList = industryMasterRepository.findAll();
        assertThat(industryMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
