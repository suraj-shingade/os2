package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyTypeMaster;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.CompanyTypeMasterRepository;
import com.crisil.onesource.service.CompanyTypeMasterService;
import com.crisil.onesource.service.dto.CompanyTypeMasterCriteria;
import com.crisil.onesource.service.CompanyTypeMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyTypeMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyTypeMasterResourceIT {

    private static final Integer DEFAULT_COMPANY_TYPE_ID = 1;
    private static final Integer UPDATED_COMPANY_TYPE_ID = 2;
    private static final Integer SMALLER_COMPANY_TYPE_ID = 1 - 1;

    private static final String DEFAULT_COMPANY_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private CompanyTypeMasterRepository companyTypeMasterRepository;

    @Autowired
    private CompanyTypeMasterService companyTypeMasterService;

    @Autowired
    private CompanyTypeMasterQueryService companyTypeMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyTypeMasterMockMvc;

    private CompanyTypeMaster companyTypeMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyTypeMaster createEntity(EntityManager em) {
        CompanyTypeMaster companyTypeMaster = new CompanyTypeMaster()
            .companyTypeId(DEFAULT_COMPANY_TYPE_ID)
            .companyType(DEFAULT_COMPANY_TYPE)
            .isDeleted(DEFAULT_IS_DELETED)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY);
        return companyTypeMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyTypeMaster createUpdatedEntity(EntityManager em) {
        CompanyTypeMaster companyTypeMaster = new CompanyTypeMaster()
            .companyTypeId(UPDATED_COMPANY_TYPE_ID)
            .companyType(UPDATED_COMPANY_TYPE)
            .isDeleted(UPDATED_IS_DELETED)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);
        return companyTypeMaster;
    }

    @BeforeEach
    public void initTest() {
        companyTypeMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyTypeMaster() throws Exception {
        int databaseSizeBeforeCreate = companyTypeMasterRepository.findAll().size();
        // Create the CompanyTypeMaster
        restCompanyTypeMasterMockMvc.perform(post("/api/company-type-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTypeMaster)))
            .andExpect(status().isCreated());

        // Validate the CompanyTypeMaster in the database
        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyTypeMaster testCompanyTypeMaster = companyTypeMasterList.get(companyTypeMasterList.size() - 1);
        assertThat(testCompanyTypeMaster.getCompanyTypeId()).isEqualTo(DEFAULT_COMPANY_TYPE_ID);
        assertThat(testCompanyTypeMaster.getCompanyType()).isEqualTo(DEFAULT_COMPANY_TYPE);
        assertThat(testCompanyTypeMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testCompanyTypeMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyTypeMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyTypeMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyTypeMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createCompanyTypeMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyTypeMasterRepository.findAll().size();

        // Create the CompanyTypeMaster with an existing ID
        companyTypeMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyTypeMasterMockMvc.perform(post("/api/company-type-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTypeMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyTypeMaster in the database
        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCompanyTypeIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyTypeMasterRepository.findAll().size();
        // set the field null
        companyTypeMaster.setCompanyTypeId(null);

        // Create the CompanyTypeMaster, which fails.


        restCompanyTypeMasterMockMvc.perform(post("/api/company-type-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTypeMaster)))
            .andExpect(status().isBadRequest());

        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCompanyTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyTypeMasterRepository.findAll().size();
        // set the field null
        companyTypeMaster.setCompanyType(null);

        // Create the CompanyTypeMaster, which fails.


        restCompanyTypeMasterMockMvc.perform(post("/api/company-type-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTypeMaster)))
            .andExpect(status().isBadRequest());

        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMasters() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyTypeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyTypeId").value(hasItem(DEFAULT_COMPANY_TYPE_ID)))
            .andExpect(jsonPath("$.[*].companyType").value(hasItem(DEFAULT_COMPANY_TYPE)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));
    }
    
    @Test
    @Transactional
    public void getCompanyTypeMaster() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get the companyTypeMaster
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters/{id}", companyTypeMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyTypeMaster.getId().intValue()))
            .andExpect(jsonPath("$.companyTypeId").value(DEFAULT_COMPANY_TYPE_ID))
            .andExpect(jsonPath("$.companyType").value(DEFAULT_COMPANY_TYPE))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY));
    }


    @Test
    @Transactional
    public void getCompanyTypeMastersByIdFiltering() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        Long id = companyTypeMaster.getId();

        defaultCompanyTypeMasterShouldBeFound("id.equals=" + id);
        defaultCompanyTypeMasterShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyTypeMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyTypeMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyTypeMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyTypeMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId equals to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.equals=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId equals to UPDATED_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.equals=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId not equals to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.notEquals=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId not equals to UPDATED_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.notEquals=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId in DEFAULT_COMPANY_TYPE_ID or UPDATED_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.in=" + DEFAULT_COMPANY_TYPE_ID + "," + UPDATED_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId equals to UPDATED_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.in=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId is not null
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.specified=true");

        // Get all the companyTypeMasterList where companyTypeId is null
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId is greater than or equal to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.greaterThanOrEqual=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId is greater than or equal to UPDATED_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.greaterThanOrEqual=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId is less than or equal to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.lessThanOrEqual=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId is less than or equal to SMALLER_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.lessThanOrEqual=" + SMALLER_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId is less than DEFAULT_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.lessThan=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId is less than UPDATED_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.lessThan=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyTypeId is greater than DEFAULT_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldNotBeFound("companyTypeId.greaterThan=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyTypeMasterList where companyTypeId is greater than SMALLER_COMPANY_TYPE_ID
        defaultCompanyTypeMasterShouldBeFound("companyTypeId.greaterThan=" + SMALLER_COMPANY_TYPE_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyType equals to DEFAULT_COMPANY_TYPE
        defaultCompanyTypeMasterShouldBeFound("companyType.equals=" + DEFAULT_COMPANY_TYPE);

        // Get all the companyTypeMasterList where companyType equals to UPDATED_COMPANY_TYPE
        defaultCompanyTypeMasterShouldNotBeFound("companyType.equals=" + UPDATED_COMPANY_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyType not equals to DEFAULT_COMPANY_TYPE
        defaultCompanyTypeMasterShouldNotBeFound("companyType.notEquals=" + DEFAULT_COMPANY_TYPE);

        // Get all the companyTypeMasterList where companyType not equals to UPDATED_COMPANY_TYPE
        defaultCompanyTypeMasterShouldBeFound("companyType.notEquals=" + UPDATED_COMPANY_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyType in DEFAULT_COMPANY_TYPE or UPDATED_COMPANY_TYPE
        defaultCompanyTypeMasterShouldBeFound("companyType.in=" + DEFAULT_COMPANY_TYPE + "," + UPDATED_COMPANY_TYPE);

        // Get all the companyTypeMasterList where companyType equals to UPDATED_COMPANY_TYPE
        defaultCompanyTypeMasterShouldNotBeFound("companyType.in=" + UPDATED_COMPANY_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyType is not null
        defaultCompanyTypeMasterShouldBeFound("companyType.specified=true");

        // Get all the companyTypeMasterList where companyType is null
        defaultCompanyTypeMasterShouldNotBeFound("companyType.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyType contains DEFAULT_COMPANY_TYPE
        defaultCompanyTypeMasterShouldBeFound("companyType.contains=" + DEFAULT_COMPANY_TYPE);

        // Get all the companyTypeMasterList where companyType contains UPDATED_COMPANY_TYPE
        defaultCompanyTypeMasterShouldNotBeFound("companyType.contains=" + UPDATED_COMPANY_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCompanyTypeNotContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where companyType does not contain DEFAULT_COMPANY_TYPE
        defaultCompanyTypeMasterShouldNotBeFound("companyType.doesNotContain=" + DEFAULT_COMPANY_TYPE);

        // Get all the companyTypeMasterList where companyType does not contain UPDATED_COMPANY_TYPE
        defaultCompanyTypeMasterShouldBeFound("companyType.doesNotContain=" + UPDATED_COMPANY_TYPE);
    }


    @Test
    @Transactional
    public void getAllCompanyTypeMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyTypeMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyTypeMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyTypeMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyTypeMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyTypeMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyTypeMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyTypeMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyTypeMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyTypeMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where isDeleted is not null
        defaultCompanyTypeMasterShouldBeFound("isDeleted.specified=true");

        // Get all the companyTypeMasterList where isDeleted is null
        defaultCompanyTypeMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTypeMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyTypeMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyTypeMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyTypeMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyTypeMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyTypeMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyTypeMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTypeMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTypeMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTypeMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTypeMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTypeMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyTypeMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedDate is not null
        defaultCompanyTypeMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyTypeMasterList where lastModifiedDate is null
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyTypeMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyTypeMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyTypeMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyTypeMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyTypeMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyTypeMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyTypeMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyTypeMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyTypeMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdDate is not null
        defaultCompanyTypeMasterShouldBeFound("createdDate.specified=true");

        // Get all the companyTypeMasterList where createdDate is null
        defaultCompanyTypeMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyTypeMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyTypeMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyTypeMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyTypeMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyTypeMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyTypeMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyTypeMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyTypeMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyTypeMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdBy is not null
        defaultCompanyTypeMasterShouldBeFound("createdBy.specified=true");

        // Get all the companyTypeMasterList where createdBy is null
        defaultCompanyTypeMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyTypeMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyTypeMasterList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyTypeMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyTypeMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyTypeMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyTypeMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTypeMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTypeMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyTypeMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedBy is not null
        defaultCompanyTypeMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyTypeMasterList where lastModifiedBy is null
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTypeMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTypeMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);

        // Get all the companyTypeMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTypeMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyTypeMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyTypeMastersByOnesourceCompanyMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);
        OnesourceCompanyMaster onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(onesourceCompanyMaster);
        em.flush();
        companyTypeMaster.addOnesourceCompanyMaster(onesourceCompanyMaster);
        companyTypeMasterRepository.saveAndFlush(companyTypeMaster);
        Long onesourceCompanyMasterId = onesourceCompanyMaster.getId();

        // Get all the companyTypeMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId
        defaultCompanyTypeMasterShouldBeFound("onesourceCompanyMasterId.equals=" + onesourceCompanyMasterId);

        // Get all the companyTypeMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId + 1
        defaultCompanyTypeMasterShouldNotBeFound("onesourceCompanyMasterId.equals=" + (onesourceCompanyMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyTypeMasterShouldBeFound(String filter) throws Exception {
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyTypeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyTypeId").value(hasItem(DEFAULT_COMPANY_TYPE_ID)))
            .andExpect(jsonPath("$.[*].companyType").value(hasItem(DEFAULT_COMPANY_TYPE)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)));

        // Check, that the count call also returns 1
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyTypeMasterShouldNotBeFound(String filter) throws Exception {
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyTypeMaster() throws Exception {
        // Get the companyTypeMaster
        restCompanyTypeMasterMockMvc.perform(get("/api/company-type-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyTypeMaster() throws Exception {
        // Initialize the database
        companyTypeMasterService.save(companyTypeMaster);

        int databaseSizeBeforeUpdate = companyTypeMasterRepository.findAll().size();

        // Update the companyTypeMaster
        CompanyTypeMaster updatedCompanyTypeMaster = companyTypeMasterRepository.findById(companyTypeMaster.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyTypeMaster are not directly saved in db
        em.detach(updatedCompanyTypeMaster);
        updatedCompanyTypeMaster
            .companyTypeId(UPDATED_COMPANY_TYPE_ID)
            .companyType(UPDATED_COMPANY_TYPE)
            .isDeleted(UPDATED_IS_DELETED)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY);

        restCompanyTypeMasterMockMvc.perform(put("/api/company-type-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyTypeMaster)))
            .andExpect(status().isOk());

        // Validate the CompanyTypeMaster in the database
        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeUpdate);
        CompanyTypeMaster testCompanyTypeMaster = companyTypeMasterList.get(companyTypeMasterList.size() - 1);
        assertThat(testCompanyTypeMaster.getCompanyTypeId()).isEqualTo(UPDATED_COMPANY_TYPE_ID);
        assertThat(testCompanyTypeMaster.getCompanyType()).isEqualTo(UPDATED_COMPANY_TYPE);
        assertThat(testCompanyTypeMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testCompanyTypeMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyTypeMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyTypeMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyTypeMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyTypeMaster() throws Exception {
        int databaseSizeBeforeUpdate = companyTypeMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyTypeMasterMockMvc.perform(put("/api/company-type-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTypeMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyTypeMaster in the database
        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyTypeMaster() throws Exception {
        // Initialize the database
        companyTypeMasterService.save(companyTypeMaster);

        int databaseSizeBeforeDelete = companyTypeMasterRepository.findAll().size();

        // Delete the companyTypeMaster
        restCompanyTypeMasterMockMvc.perform(delete("/api/company-type-masters/{id}", companyTypeMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyTypeMaster> companyTypeMasterList = companyTypeMasterRepository.findAll();
        assertThat(companyTypeMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
