package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.AprvlContactMaster;
import com.crisil.onesource.repository.AprvlContactMasterRepository;
import com.crisil.onesource.service.AprvlContactMasterService;
import com.crisil.onesource.service.dto.AprvlContactMasterCriteria;
import com.crisil.onesource.service.AprvlContactMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AprvlContactMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AprvlContactMasterResourceIT {

    private static final String DEFAULT_LASTNAME = "AAAAAAAAAA";
    private static final String UPDATED_LASTNAME = "BBBBBBBBBB";

    private static final String DEFAULT_JOINEDNEWCOMPANY = "AAAAAAAAAA";
    private static final String UPDATED_JOINEDNEWCOMPANY = "BBBBBBBBBB";

    private static final Integer DEFAULT_CITYID = 1;
    private static final Integer UPDATED_CITYID = 2;
    private static final Integer SMALLER_CITYID = 1 - 1;

    private static final Integer DEFAULT_HDELETED = 1;
    private static final Integer UPDATED_HDELETED = 2;
    private static final Integer SMALLER_HDELETED = 1 - 1;

    private static final String DEFAULT_RECORDID = "AAAAAAAAAA";
    private static final String UPDATED_RECORDID = "BBBBBBBBBB";

    private static final String DEFAULT_LEVELIMPORTANCE = "AAAAAAAAAA";
    private static final String UPDATED_LEVELIMPORTANCE = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_KEYPERSON = "AAAAA";
    private static final String UPDATED_KEYPERSON = "BBBBB";

    private static final String DEFAULT_COMPANYCODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANYCODE = "BBBBBBBBBB";

    private static final String DEFAULT_PINCODE = "AAAAAAAAAA";
    private static final String UPDATED_PINCODE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_3 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_CITYNAME = "AAAAAAAAAA";
    private static final String UPDATED_CITYNAME = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRYNAME = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRYNAME = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE = "BBBBBBBBBB";

    private static final String DEFAULT_STATENAME = "AAAAAAAAAA";
    private static final String UPDATED_STATENAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_MODIFY_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_MODIFY_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_MODIFY_DATE = LocalDate.ofEpochDay(-1L);

    private static final Integer DEFAULT_OFAFLAG = 1;
    private static final Integer UPDATED_OFAFLAG = 2;
    private static final Integer SMALLER_OFAFLAG = 1 - 1;

    private static final String DEFAULT_FIRSTNAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRSTNAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLENAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLENAME = "BBBBBBBBBB";

    private static final String DEFAULT_REMOVEREASON = "AAAAAAAAAA";
    private static final String UPDATED_REMOVEREASON = "BBBBBBBBBB";

    private static final String DEFAULT_SALUTATION = "AAAAA";
    private static final String UPDATED_SALUTATION = "BBBBB";

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Integer DEFAULT_CTLG_SRNO = 1;
    private static final Integer UPDATED_CTLG_SRNO = 2;
    private static final Integer SMALLER_CTLG_SRNO = 1 - 1;

    private static final String DEFAULT_CTLG_IS_DELETED = "A";
    private static final String UPDATED_CTLG_IS_DELETED = "B";

    private static final Long DEFAULT_JSON_STORE_ID = 1L;
    private static final Long UPDATED_JSON_STORE_ID = 2L;
    private static final Long SMALLER_JSON_STORE_ID = 1L - 1L;

    private static final String DEFAULT_IS_DATA_PROCESSED = "A";
    private static final String UPDATED_IS_DATA_PROCESSED = "B";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_OPERATION = "A";
    private static final String UPDATED_OPERATION = "B";

    private static final String DEFAULT_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    @Autowired
    private AprvlContactMasterRepository aprvlContactMasterRepository;

    @Autowired
    private AprvlContactMasterService aprvlContactMasterService;

    @Autowired
    private AprvlContactMasterQueryService aprvlContactMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAprvlContactMasterMockMvc;

    private AprvlContactMaster aprvlContactMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AprvlContactMaster createEntity(EntityManager em) {
        AprvlContactMaster aprvlContactMaster = new AprvlContactMaster()
            .lastname(DEFAULT_LASTNAME)
            .joinednewcompany(DEFAULT_JOINEDNEWCOMPANY)
            .cityid(DEFAULT_CITYID)
            .hdeleted(DEFAULT_HDELETED)
            .recordid(DEFAULT_RECORDID)
            .levelimportance(DEFAULT_LEVELIMPORTANCE)
            .department(DEFAULT_DEPARTMENT)
            .fax(DEFAULT_FAX)
            .email(DEFAULT_EMAIL)
            .keyperson(DEFAULT_KEYPERSON)
            .companycode(DEFAULT_COMPANYCODE)
            .pincode(DEFAULT_PINCODE)
            .comments(DEFAULT_COMMENTS)
            .address3(DEFAULT_ADDRESS_3)
            .address2(DEFAULT_ADDRESS_2)
            .contactFullName(DEFAULT_CONTACT_FULL_NAME)
            .address1(DEFAULT_ADDRESS_1)
            .cityname(DEFAULT_CITYNAME)
            .countryname(DEFAULT_COUNTRYNAME)
            .mobile(DEFAULT_MOBILE)
            .statename(DEFAULT_STATENAME)
            .modifyDate(DEFAULT_MODIFY_DATE)
            .ofaflag(DEFAULT_OFAFLAG)
            .firstname(DEFAULT_FIRSTNAME)
            .middlename(DEFAULT_MIDDLENAME)
            .removereason(DEFAULT_REMOVEREASON)
            .salutation(DEFAULT_SALUTATION)
            .designation(DEFAULT_DESIGNATION)
            .status(DEFAULT_STATUS)
            .ctlgSrno(DEFAULT_CTLG_SRNO)
            .ctlgIsDeleted(DEFAULT_CTLG_IS_DELETED)
            .jsonStoreId(DEFAULT_JSON_STORE_ID)
            .isDataProcessed(DEFAULT_IS_DATA_PROCESSED)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .operation(DEFAULT_OPERATION)
            .contactId(DEFAULT_CONTACT_ID)
            .phone(DEFAULT_PHONE);
        return aprvlContactMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AprvlContactMaster createUpdatedEntity(EntityManager em) {
        AprvlContactMaster aprvlContactMaster = new AprvlContactMaster()
            .lastname(UPDATED_LASTNAME)
            .joinednewcompany(UPDATED_JOINEDNEWCOMPANY)
            .cityid(UPDATED_CITYID)
            .hdeleted(UPDATED_HDELETED)
            .recordid(UPDATED_RECORDID)
            .levelimportance(UPDATED_LEVELIMPORTANCE)
            .department(UPDATED_DEPARTMENT)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .keyperson(UPDATED_KEYPERSON)
            .companycode(UPDATED_COMPANYCODE)
            .pincode(UPDATED_PINCODE)
            .comments(UPDATED_COMMENTS)
            .address3(UPDATED_ADDRESS_3)
            .address2(UPDATED_ADDRESS_2)
            .contactFullName(UPDATED_CONTACT_FULL_NAME)
            .address1(UPDATED_ADDRESS_1)
            .cityname(UPDATED_CITYNAME)
            .countryname(UPDATED_COUNTRYNAME)
            .mobile(UPDATED_MOBILE)
            .statename(UPDATED_STATENAME)
            .modifyDate(UPDATED_MODIFY_DATE)
            .ofaflag(UPDATED_OFAFLAG)
            .firstname(UPDATED_FIRSTNAME)
            .middlename(UPDATED_MIDDLENAME)
            .removereason(UPDATED_REMOVEREASON)
            .salutation(UPDATED_SALUTATION)
            .designation(UPDATED_DESIGNATION)
            .status(UPDATED_STATUS)
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .operation(UPDATED_OPERATION)
            .contactId(UPDATED_CONTACT_ID)
            .phone(UPDATED_PHONE);
        return aprvlContactMaster;
    }

    @BeforeEach
    public void initTest() {
        aprvlContactMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createAprvlContactMaster() throws Exception {
        int databaseSizeBeforeCreate = aprvlContactMasterRepository.findAll().size();
        // Create the AprvlContactMaster
        restAprvlContactMasterMockMvc.perform(post("/api/aprvl-contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlContactMaster)))
            .andExpect(status().isCreated());

        // Validate the AprvlContactMaster in the database
        List<AprvlContactMaster> aprvlContactMasterList = aprvlContactMasterRepository.findAll();
        assertThat(aprvlContactMasterList).hasSize(databaseSizeBeforeCreate + 1);
        AprvlContactMaster testAprvlContactMaster = aprvlContactMasterList.get(aprvlContactMasterList.size() - 1);
        assertThat(testAprvlContactMaster.getLastname()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(testAprvlContactMaster.getJoinednewcompany()).isEqualTo(DEFAULT_JOINEDNEWCOMPANY);
        assertThat(testAprvlContactMaster.getCityid()).isEqualTo(DEFAULT_CITYID);
        assertThat(testAprvlContactMaster.getHdeleted()).isEqualTo(DEFAULT_HDELETED);
        assertThat(testAprvlContactMaster.getRecordid()).isEqualTo(DEFAULT_RECORDID);
        assertThat(testAprvlContactMaster.getLevelimportance()).isEqualTo(DEFAULT_LEVELIMPORTANCE);
        assertThat(testAprvlContactMaster.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testAprvlContactMaster.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testAprvlContactMaster.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAprvlContactMaster.getKeyperson()).isEqualTo(DEFAULT_KEYPERSON);
        assertThat(testAprvlContactMaster.getCompanycode()).isEqualTo(DEFAULT_COMPANYCODE);
        assertThat(testAprvlContactMaster.getPincode()).isEqualTo(DEFAULT_PINCODE);
        assertThat(testAprvlContactMaster.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAprvlContactMaster.getAddress3()).isEqualTo(DEFAULT_ADDRESS_3);
        assertThat(testAprvlContactMaster.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testAprvlContactMaster.getContactFullName()).isEqualTo(DEFAULT_CONTACT_FULL_NAME);
        assertThat(testAprvlContactMaster.getAddress1()).isEqualTo(DEFAULT_ADDRESS_1);
        assertThat(testAprvlContactMaster.getCityname()).isEqualTo(DEFAULT_CITYNAME);
        assertThat(testAprvlContactMaster.getCountryname()).isEqualTo(DEFAULT_COUNTRYNAME);
        assertThat(testAprvlContactMaster.getMobile()).isEqualTo(DEFAULT_MOBILE);
        assertThat(testAprvlContactMaster.getStatename()).isEqualTo(DEFAULT_STATENAME);
        assertThat(testAprvlContactMaster.getModifyDate()).isEqualTo(DEFAULT_MODIFY_DATE);
        assertThat(testAprvlContactMaster.getOfaflag()).isEqualTo(DEFAULT_OFAFLAG);
        assertThat(testAprvlContactMaster.getFirstname()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(testAprvlContactMaster.getMiddlename()).isEqualTo(DEFAULT_MIDDLENAME);
        assertThat(testAprvlContactMaster.getRemovereason()).isEqualTo(DEFAULT_REMOVEREASON);
        assertThat(testAprvlContactMaster.getSalutation()).isEqualTo(DEFAULT_SALUTATION);
        assertThat(testAprvlContactMaster.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testAprvlContactMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAprvlContactMaster.getCtlgSrno()).isEqualTo(DEFAULT_CTLG_SRNO);
        assertThat(testAprvlContactMaster.getCtlgIsDeleted()).isEqualTo(DEFAULT_CTLG_IS_DELETED);
        assertThat(testAprvlContactMaster.getJsonStoreId()).isEqualTo(DEFAULT_JSON_STORE_ID);
        assertThat(testAprvlContactMaster.getIsDataProcessed()).isEqualTo(DEFAULT_IS_DATA_PROCESSED);
        assertThat(testAprvlContactMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testAprvlContactMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testAprvlContactMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAprvlContactMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAprvlContactMaster.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testAprvlContactMaster.getContactId()).isEqualTo(DEFAULT_CONTACT_ID);
        assertThat(testAprvlContactMaster.getPhone()).isEqualTo(DEFAULT_PHONE);
    }

    @Test
    @Transactional
    public void createAprvlContactMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aprvlContactMasterRepository.findAll().size();

        // Create the AprvlContactMaster with an existing ID
        aprvlContactMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAprvlContactMasterMockMvc.perform(post("/api/aprvl-contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlContactMaster)))
            .andExpect(status().isBadRequest());

        // Validate the AprvlContactMaster in the database
        List<AprvlContactMaster> aprvlContactMasterList = aprvlContactMasterRepository.findAll();
        assertThat(aprvlContactMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCtlgSrnoIsRequired() throws Exception {
        int databaseSizeBeforeTest = aprvlContactMasterRepository.findAll().size();
        // set the field null
        aprvlContactMaster.setCtlgSrno(null);

        // Create the AprvlContactMaster, which fails.


        restAprvlContactMasterMockMvc.perform(post("/api/aprvl-contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlContactMaster)))
            .andExpect(status().isBadRequest());

        List<AprvlContactMaster> aprvlContactMasterList = aprvlContactMasterRepository.findAll();
        assertThat(aprvlContactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMasters() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aprvlContactMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].joinednewcompany").value(hasItem(DEFAULT_JOINEDNEWCOMPANY)))
            .andExpect(jsonPath("$.[*].cityid").value(hasItem(DEFAULT_CITYID)))
            .andExpect(jsonPath("$.[*].hdeleted").value(hasItem(DEFAULT_HDELETED)))
            .andExpect(jsonPath("$.[*].recordid").value(hasItem(DEFAULT_RECORDID)))
            .andExpect(jsonPath("$.[*].levelimportance").value(hasItem(DEFAULT_LEVELIMPORTANCE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].keyperson").value(hasItem(DEFAULT_KEYPERSON)))
            .andExpect(jsonPath("$.[*].companycode").value(hasItem(DEFAULT_COMPANYCODE)))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3.toString())))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2.toString())))
            .andExpect(jsonPath("$.[*].contactFullName").value(hasItem(DEFAULT_CONTACT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].cityname").value(hasItem(DEFAULT_CITYNAME)))
            .andExpect(jsonPath("$.[*].countryname").value(hasItem(DEFAULT_COUNTRYNAME)))
            .andExpect(jsonPath("$.[*].mobile").value(hasItem(DEFAULT_MOBILE)))
            .andExpect(jsonPath("$.[*].statename").value(hasItem(DEFAULT_STATENAME)))
            .andExpect(jsonPath("$.[*].modifyDate").value(hasItem(DEFAULT_MODIFY_DATE.toString())))
            .andExpect(jsonPath("$.[*].ofaflag").value(hasItem(DEFAULT_OFAFLAG)))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].middlename").value(hasItem(DEFAULT_MIDDLENAME)))
            .andExpect(jsonPath("$.[*].removereason").value(hasItem(DEFAULT_REMOVEREASON)))
            .andExpect(jsonPath("$.[*].salutation").value(hasItem(DEFAULT_SALUTATION)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID.intValue())))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())));
    }
    
    @Test
    @Transactional
    public void getAprvlContactMaster() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get the aprvlContactMaster
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters/{id}", aprvlContactMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(aprvlContactMaster.getId().intValue()))
            .andExpect(jsonPath("$.lastname").value(DEFAULT_LASTNAME))
            .andExpect(jsonPath("$.joinednewcompany").value(DEFAULT_JOINEDNEWCOMPANY))
            .andExpect(jsonPath("$.cityid").value(DEFAULT_CITYID))
            .andExpect(jsonPath("$.hdeleted").value(DEFAULT_HDELETED))
            .andExpect(jsonPath("$.recordid").value(DEFAULT_RECORDID))
            .andExpect(jsonPath("$.levelimportance").value(DEFAULT_LEVELIMPORTANCE))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.keyperson").value(DEFAULT_KEYPERSON))
            .andExpect(jsonPath("$.companycode").value(DEFAULT_COMPANYCODE))
            .andExpect(jsonPath("$.pincode").value(DEFAULT_PINCODE))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()))
            .andExpect(jsonPath("$.address3").value(DEFAULT_ADDRESS_3.toString()))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2.toString()))
            .andExpect(jsonPath("$.contactFullName").value(DEFAULT_CONTACT_FULL_NAME))
            .andExpect(jsonPath("$.address1").value(DEFAULT_ADDRESS_1))
            .andExpect(jsonPath("$.cityname").value(DEFAULT_CITYNAME))
            .andExpect(jsonPath("$.countryname").value(DEFAULT_COUNTRYNAME))
            .andExpect(jsonPath("$.mobile").value(DEFAULT_MOBILE))
            .andExpect(jsonPath("$.statename").value(DEFAULT_STATENAME))
            .andExpect(jsonPath("$.modifyDate").value(DEFAULT_MODIFY_DATE.toString()))
            .andExpect(jsonPath("$.ofaflag").value(DEFAULT_OFAFLAG))
            .andExpect(jsonPath("$.firstname").value(DEFAULT_FIRSTNAME))
            .andExpect(jsonPath("$.middlename").value(DEFAULT_MIDDLENAME))
            .andExpect(jsonPath("$.removereason").value(DEFAULT_REMOVEREASON))
            .andExpect(jsonPath("$.salutation").value(DEFAULT_SALUTATION))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.ctlgSrno").value(DEFAULT_CTLG_SRNO))
            .andExpect(jsonPath("$.ctlgIsDeleted").value(DEFAULT_CTLG_IS_DELETED))
            .andExpect(jsonPath("$.jsonStoreId").value(DEFAULT_JSON_STORE_ID.intValue()))
            .andExpect(jsonPath("$.isDataProcessed").value(DEFAULT_IS_DATA_PROCESSED))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION))
            .andExpect(jsonPath("$.contactId").value(DEFAULT_CONTACT_ID))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()));
    }


    @Test
    @Transactional
    public void getAprvlContactMastersByIdFiltering() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        Long id = aprvlContactMaster.getId();

        defaultAprvlContactMasterShouldBeFound("id.equals=" + id);
        defaultAprvlContactMasterShouldNotBeFound("id.notEquals=" + id);

        defaultAprvlContactMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAprvlContactMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultAprvlContactMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAprvlContactMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastnameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastname equals to DEFAULT_LASTNAME
        defaultAprvlContactMasterShouldBeFound("lastname.equals=" + DEFAULT_LASTNAME);

        // Get all the aprvlContactMasterList where lastname equals to UPDATED_LASTNAME
        defaultAprvlContactMasterShouldNotBeFound("lastname.equals=" + UPDATED_LASTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastnameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastname not equals to DEFAULT_LASTNAME
        defaultAprvlContactMasterShouldNotBeFound("lastname.notEquals=" + DEFAULT_LASTNAME);

        // Get all the aprvlContactMasterList where lastname not equals to UPDATED_LASTNAME
        defaultAprvlContactMasterShouldBeFound("lastname.notEquals=" + UPDATED_LASTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastnameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastname in DEFAULT_LASTNAME or UPDATED_LASTNAME
        defaultAprvlContactMasterShouldBeFound("lastname.in=" + DEFAULT_LASTNAME + "," + UPDATED_LASTNAME);

        // Get all the aprvlContactMasterList where lastname equals to UPDATED_LASTNAME
        defaultAprvlContactMasterShouldNotBeFound("lastname.in=" + UPDATED_LASTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastnameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastname is not null
        defaultAprvlContactMasterShouldBeFound("lastname.specified=true");

        // Get all the aprvlContactMasterList where lastname is null
        defaultAprvlContactMasterShouldNotBeFound("lastname.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByLastnameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastname contains DEFAULT_LASTNAME
        defaultAprvlContactMasterShouldBeFound("lastname.contains=" + DEFAULT_LASTNAME);

        // Get all the aprvlContactMasterList where lastname contains UPDATED_LASTNAME
        defaultAprvlContactMasterShouldNotBeFound("lastname.contains=" + UPDATED_LASTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastnameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastname does not contain DEFAULT_LASTNAME
        defaultAprvlContactMasterShouldNotBeFound("lastname.doesNotContain=" + DEFAULT_LASTNAME);

        // Get all the aprvlContactMasterList where lastname does not contain UPDATED_LASTNAME
        defaultAprvlContactMasterShouldBeFound("lastname.doesNotContain=" + UPDATED_LASTNAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByJoinednewcompanyIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where joinednewcompany equals to DEFAULT_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldBeFound("joinednewcompany.equals=" + DEFAULT_JOINEDNEWCOMPANY);

        // Get all the aprvlContactMasterList where joinednewcompany equals to UPDATED_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldNotBeFound("joinednewcompany.equals=" + UPDATED_JOINEDNEWCOMPANY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJoinednewcompanyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where joinednewcompany not equals to DEFAULT_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldNotBeFound("joinednewcompany.notEquals=" + DEFAULT_JOINEDNEWCOMPANY);

        // Get all the aprvlContactMasterList where joinednewcompany not equals to UPDATED_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldBeFound("joinednewcompany.notEquals=" + UPDATED_JOINEDNEWCOMPANY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJoinednewcompanyIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where joinednewcompany in DEFAULT_JOINEDNEWCOMPANY or UPDATED_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldBeFound("joinednewcompany.in=" + DEFAULT_JOINEDNEWCOMPANY + "," + UPDATED_JOINEDNEWCOMPANY);

        // Get all the aprvlContactMasterList where joinednewcompany equals to UPDATED_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldNotBeFound("joinednewcompany.in=" + UPDATED_JOINEDNEWCOMPANY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJoinednewcompanyIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where joinednewcompany is not null
        defaultAprvlContactMasterShouldBeFound("joinednewcompany.specified=true");

        // Get all the aprvlContactMasterList where joinednewcompany is null
        defaultAprvlContactMasterShouldNotBeFound("joinednewcompany.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByJoinednewcompanyContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where joinednewcompany contains DEFAULT_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldBeFound("joinednewcompany.contains=" + DEFAULT_JOINEDNEWCOMPANY);

        // Get all the aprvlContactMasterList where joinednewcompany contains UPDATED_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldNotBeFound("joinednewcompany.contains=" + UPDATED_JOINEDNEWCOMPANY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJoinednewcompanyNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where joinednewcompany does not contain DEFAULT_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldNotBeFound("joinednewcompany.doesNotContain=" + DEFAULT_JOINEDNEWCOMPANY);

        // Get all the aprvlContactMasterList where joinednewcompany does not contain UPDATED_JOINEDNEWCOMPANY
        defaultAprvlContactMasterShouldBeFound("joinednewcompany.doesNotContain=" + UPDATED_JOINEDNEWCOMPANY);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid equals to DEFAULT_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.equals=" + DEFAULT_CITYID);

        // Get all the aprvlContactMasterList where cityid equals to UPDATED_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.equals=" + UPDATED_CITYID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid not equals to DEFAULT_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.notEquals=" + DEFAULT_CITYID);

        // Get all the aprvlContactMasterList where cityid not equals to UPDATED_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.notEquals=" + UPDATED_CITYID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid in DEFAULT_CITYID or UPDATED_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.in=" + DEFAULT_CITYID + "," + UPDATED_CITYID);

        // Get all the aprvlContactMasterList where cityid equals to UPDATED_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.in=" + UPDATED_CITYID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid is not null
        defaultAprvlContactMasterShouldBeFound("cityid.specified=true");

        // Get all the aprvlContactMasterList where cityid is null
        defaultAprvlContactMasterShouldNotBeFound("cityid.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid is greater than or equal to DEFAULT_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.greaterThanOrEqual=" + DEFAULT_CITYID);

        // Get all the aprvlContactMasterList where cityid is greater than or equal to UPDATED_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.greaterThanOrEqual=" + UPDATED_CITYID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid is less than or equal to DEFAULT_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.lessThanOrEqual=" + DEFAULT_CITYID);

        // Get all the aprvlContactMasterList where cityid is less than or equal to SMALLER_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.lessThanOrEqual=" + SMALLER_CITYID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid is less than DEFAULT_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.lessThan=" + DEFAULT_CITYID);

        // Get all the aprvlContactMasterList where cityid is less than UPDATED_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.lessThan=" + UPDATED_CITYID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCityidIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityid is greater than DEFAULT_CITYID
        defaultAprvlContactMasterShouldNotBeFound("cityid.greaterThan=" + DEFAULT_CITYID);

        // Get all the aprvlContactMasterList where cityid is greater than SMALLER_CITYID
        defaultAprvlContactMasterShouldBeFound("cityid.greaterThan=" + SMALLER_CITYID);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted equals to DEFAULT_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.equals=" + DEFAULT_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted equals to UPDATED_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.equals=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted not equals to DEFAULT_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.notEquals=" + DEFAULT_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted not equals to UPDATED_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.notEquals=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted in DEFAULT_HDELETED or UPDATED_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.in=" + DEFAULT_HDELETED + "," + UPDATED_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted equals to UPDATED_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.in=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted is not null
        defaultAprvlContactMasterShouldBeFound("hdeleted.specified=true");

        // Get all the aprvlContactMasterList where hdeleted is null
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted is greater than or equal to DEFAULT_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.greaterThanOrEqual=" + DEFAULT_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted is greater than or equal to UPDATED_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.greaterThanOrEqual=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted is less than or equal to DEFAULT_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.lessThanOrEqual=" + DEFAULT_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted is less than or equal to SMALLER_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.lessThanOrEqual=" + SMALLER_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted is less than DEFAULT_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.lessThan=" + DEFAULT_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted is less than UPDATED_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.lessThan=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByHdeletedIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where hdeleted is greater than DEFAULT_HDELETED
        defaultAprvlContactMasterShouldNotBeFound("hdeleted.greaterThan=" + DEFAULT_HDELETED);

        // Get all the aprvlContactMasterList where hdeleted is greater than SMALLER_HDELETED
        defaultAprvlContactMasterShouldBeFound("hdeleted.greaterThan=" + SMALLER_HDELETED);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByRecordidIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where recordid equals to DEFAULT_RECORDID
        defaultAprvlContactMasterShouldBeFound("recordid.equals=" + DEFAULT_RECORDID);

        // Get all the aprvlContactMasterList where recordid equals to UPDATED_RECORDID
        defaultAprvlContactMasterShouldNotBeFound("recordid.equals=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRecordidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where recordid not equals to DEFAULT_RECORDID
        defaultAprvlContactMasterShouldNotBeFound("recordid.notEquals=" + DEFAULT_RECORDID);

        // Get all the aprvlContactMasterList where recordid not equals to UPDATED_RECORDID
        defaultAprvlContactMasterShouldBeFound("recordid.notEquals=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRecordidIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where recordid in DEFAULT_RECORDID or UPDATED_RECORDID
        defaultAprvlContactMasterShouldBeFound("recordid.in=" + DEFAULT_RECORDID + "," + UPDATED_RECORDID);

        // Get all the aprvlContactMasterList where recordid equals to UPDATED_RECORDID
        defaultAprvlContactMasterShouldNotBeFound("recordid.in=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRecordidIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where recordid is not null
        defaultAprvlContactMasterShouldBeFound("recordid.specified=true");

        // Get all the aprvlContactMasterList where recordid is null
        defaultAprvlContactMasterShouldNotBeFound("recordid.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByRecordidContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where recordid contains DEFAULT_RECORDID
        defaultAprvlContactMasterShouldBeFound("recordid.contains=" + DEFAULT_RECORDID);

        // Get all the aprvlContactMasterList where recordid contains UPDATED_RECORDID
        defaultAprvlContactMasterShouldNotBeFound("recordid.contains=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRecordidNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where recordid does not contain DEFAULT_RECORDID
        defaultAprvlContactMasterShouldNotBeFound("recordid.doesNotContain=" + DEFAULT_RECORDID);

        // Get all the aprvlContactMasterList where recordid does not contain UPDATED_RECORDID
        defaultAprvlContactMasterShouldBeFound("recordid.doesNotContain=" + UPDATED_RECORDID);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByLevelimportanceIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where levelimportance equals to DEFAULT_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldBeFound("levelimportance.equals=" + DEFAULT_LEVELIMPORTANCE);

        // Get all the aprvlContactMasterList where levelimportance equals to UPDATED_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldNotBeFound("levelimportance.equals=" + UPDATED_LEVELIMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLevelimportanceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where levelimportance not equals to DEFAULT_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldNotBeFound("levelimportance.notEquals=" + DEFAULT_LEVELIMPORTANCE);

        // Get all the aprvlContactMasterList where levelimportance not equals to UPDATED_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldBeFound("levelimportance.notEquals=" + UPDATED_LEVELIMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLevelimportanceIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where levelimportance in DEFAULT_LEVELIMPORTANCE or UPDATED_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldBeFound("levelimportance.in=" + DEFAULT_LEVELIMPORTANCE + "," + UPDATED_LEVELIMPORTANCE);

        // Get all the aprvlContactMasterList where levelimportance equals to UPDATED_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldNotBeFound("levelimportance.in=" + UPDATED_LEVELIMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLevelimportanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where levelimportance is not null
        defaultAprvlContactMasterShouldBeFound("levelimportance.specified=true");

        // Get all the aprvlContactMasterList where levelimportance is null
        defaultAprvlContactMasterShouldNotBeFound("levelimportance.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByLevelimportanceContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where levelimportance contains DEFAULT_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldBeFound("levelimportance.contains=" + DEFAULT_LEVELIMPORTANCE);

        // Get all the aprvlContactMasterList where levelimportance contains UPDATED_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldNotBeFound("levelimportance.contains=" + UPDATED_LEVELIMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLevelimportanceNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where levelimportance does not contain DEFAULT_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldNotBeFound("levelimportance.doesNotContain=" + DEFAULT_LEVELIMPORTANCE);

        // Get all the aprvlContactMasterList where levelimportance does not contain UPDATED_LEVELIMPORTANCE
        defaultAprvlContactMasterShouldBeFound("levelimportance.doesNotContain=" + UPDATED_LEVELIMPORTANCE);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByDepartmentIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where department equals to DEFAULT_DEPARTMENT
        defaultAprvlContactMasterShouldBeFound("department.equals=" + DEFAULT_DEPARTMENT);

        // Get all the aprvlContactMasterList where department equals to UPDATED_DEPARTMENT
        defaultAprvlContactMasterShouldNotBeFound("department.equals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDepartmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where department not equals to DEFAULT_DEPARTMENT
        defaultAprvlContactMasterShouldNotBeFound("department.notEquals=" + DEFAULT_DEPARTMENT);

        // Get all the aprvlContactMasterList where department not equals to UPDATED_DEPARTMENT
        defaultAprvlContactMasterShouldBeFound("department.notEquals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDepartmentIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where department in DEFAULT_DEPARTMENT or UPDATED_DEPARTMENT
        defaultAprvlContactMasterShouldBeFound("department.in=" + DEFAULT_DEPARTMENT + "," + UPDATED_DEPARTMENT);

        // Get all the aprvlContactMasterList where department equals to UPDATED_DEPARTMENT
        defaultAprvlContactMasterShouldNotBeFound("department.in=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDepartmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where department is not null
        defaultAprvlContactMasterShouldBeFound("department.specified=true");

        // Get all the aprvlContactMasterList where department is null
        defaultAprvlContactMasterShouldNotBeFound("department.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByDepartmentContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where department contains DEFAULT_DEPARTMENT
        defaultAprvlContactMasterShouldBeFound("department.contains=" + DEFAULT_DEPARTMENT);

        // Get all the aprvlContactMasterList where department contains UPDATED_DEPARTMENT
        defaultAprvlContactMasterShouldNotBeFound("department.contains=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDepartmentNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where department does not contain DEFAULT_DEPARTMENT
        defaultAprvlContactMasterShouldNotBeFound("department.doesNotContain=" + DEFAULT_DEPARTMENT);

        // Get all the aprvlContactMasterList where department does not contain UPDATED_DEPARTMENT
        defaultAprvlContactMasterShouldBeFound("department.doesNotContain=" + UPDATED_DEPARTMENT);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where fax equals to DEFAULT_FAX
        defaultAprvlContactMasterShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the aprvlContactMasterList where fax equals to UPDATED_FAX
        defaultAprvlContactMasterShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFaxIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where fax not equals to DEFAULT_FAX
        defaultAprvlContactMasterShouldNotBeFound("fax.notEquals=" + DEFAULT_FAX);

        // Get all the aprvlContactMasterList where fax not equals to UPDATED_FAX
        defaultAprvlContactMasterShouldBeFound("fax.notEquals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultAprvlContactMasterShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the aprvlContactMasterList where fax equals to UPDATED_FAX
        defaultAprvlContactMasterShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where fax is not null
        defaultAprvlContactMasterShouldBeFound("fax.specified=true");

        // Get all the aprvlContactMasterList where fax is null
        defaultAprvlContactMasterShouldNotBeFound("fax.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByFaxContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where fax contains DEFAULT_FAX
        defaultAprvlContactMasterShouldBeFound("fax.contains=" + DEFAULT_FAX);

        // Get all the aprvlContactMasterList where fax contains UPDATED_FAX
        defaultAprvlContactMasterShouldNotBeFound("fax.contains=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFaxNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where fax does not contain DEFAULT_FAX
        defaultAprvlContactMasterShouldNotBeFound("fax.doesNotContain=" + DEFAULT_FAX);

        // Get all the aprvlContactMasterList where fax does not contain UPDATED_FAX
        defaultAprvlContactMasterShouldBeFound("fax.doesNotContain=" + UPDATED_FAX);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where email equals to DEFAULT_EMAIL
        defaultAprvlContactMasterShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the aprvlContactMasterList where email equals to UPDATED_EMAIL
        defaultAprvlContactMasterShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where email not equals to DEFAULT_EMAIL
        defaultAprvlContactMasterShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the aprvlContactMasterList where email not equals to UPDATED_EMAIL
        defaultAprvlContactMasterShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultAprvlContactMasterShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the aprvlContactMasterList where email equals to UPDATED_EMAIL
        defaultAprvlContactMasterShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where email is not null
        defaultAprvlContactMasterShouldBeFound("email.specified=true");

        // Get all the aprvlContactMasterList where email is null
        defaultAprvlContactMasterShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByEmailContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where email contains DEFAULT_EMAIL
        defaultAprvlContactMasterShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the aprvlContactMasterList where email contains UPDATED_EMAIL
        defaultAprvlContactMasterShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where email does not contain DEFAULT_EMAIL
        defaultAprvlContactMasterShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the aprvlContactMasterList where email does not contain UPDATED_EMAIL
        defaultAprvlContactMasterShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByKeypersonIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where keyperson equals to DEFAULT_KEYPERSON
        defaultAprvlContactMasterShouldBeFound("keyperson.equals=" + DEFAULT_KEYPERSON);

        // Get all the aprvlContactMasterList where keyperson equals to UPDATED_KEYPERSON
        defaultAprvlContactMasterShouldNotBeFound("keyperson.equals=" + UPDATED_KEYPERSON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByKeypersonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where keyperson not equals to DEFAULT_KEYPERSON
        defaultAprvlContactMasterShouldNotBeFound("keyperson.notEquals=" + DEFAULT_KEYPERSON);

        // Get all the aprvlContactMasterList where keyperson not equals to UPDATED_KEYPERSON
        defaultAprvlContactMasterShouldBeFound("keyperson.notEquals=" + UPDATED_KEYPERSON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByKeypersonIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where keyperson in DEFAULT_KEYPERSON or UPDATED_KEYPERSON
        defaultAprvlContactMasterShouldBeFound("keyperson.in=" + DEFAULT_KEYPERSON + "," + UPDATED_KEYPERSON);

        // Get all the aprvlContactMasterList where keyperson equals to UPDATED_KEYPERSON
        defaultAprvlContactMasterShouldNotBeFound("keyperson.in=" + UPDATED_KEYPERSON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByKeypersonIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where keyperson is not null
        defaultAprvlContactMasterShouldBeFound("keyperson.specified=true");

        // Get all the aprvlContactMasterList where keyperson is null
        defaultAprvlContactMasterShouldNotBeFound("keyperson.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByKeypersonContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where keyperson contains DEFAULT_KEYPERSON
        defaultAprvlContactMasterShouldBeFound("keyperson.contains=" + DEFAULT_KEYPERSON);

        // Get all the aprvlContactMasterList where keyperson contains UPDATED_KEYPERSON
        defaultAprvlContactMasterShouldNotBeFound("keyperson.contains=" + UPDATED_KEYPERSON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByKeypersonNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where keyperson does not contain DEFAULT_KEYPERSON
        defaultAprvlContactMasterShouldNotBeFound("keyperson.doesNotContain=" + DEFAULT_KEYPERSON);

        // Get all the aprvlContactMasterList where keyperson does not contain UPDATED_KEYPERSON
        defaultAprvlContactMasterShouldBeFound("keyperson.doesNotContain=" + UPDATED_KEYPERSON);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCompanycodeIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where companycode equals to DEFAULT_COMPANYCODE
        defaultAprvlContactMasterShouldBeFound("companycode.equals=" + DEFAULT_COMPANYCODE);

        // Get all the aprvlContactMasterList where companycode equals to UPDATED_COMPANYCODE
        defaultAprvlContactMasterShouldNotBeFound("companycode.equals=" + UPDATED_COMPANYCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCompanycodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where companycode not equals to DEFAULT_COMPANYCODE
        defaultAprvlContactMasterShouldNotBeFound("companycode.notEquals=" + DEFAULT_COMPANYCODE);

        // Get all the aprvlContactMasterList where companycode not equals to UPDATED_COMPANYCODE
        defaultAprvlContactMasterShouldBeFound("companycode.notEquals=" + UPDATED_COMPANYCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCompanycodeIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where companycode in DEFAULT_COMPANYCODE or UPDATED_COMPANYCODE
        defaultAprvlContactMasterShouldBeFound("companycode.in=" + DEFAULT_COMPANYCODE + "," + UPDATED_COMPANYCODE);

        // Get all the aprvlContactMasterList where companycode equals to UPDATED_COMPANYCODE
        defaultAprvlContactMasterShouldNotBeFound("companycode.in=" + UPDATED_COMPANYCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCompanycodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where companycode is not null
        defaultAprvlContactMasterShouldBeFound("companycode.specified=true");

        // Get all the aprvlContactMasterList where companycode is null
        defaultAprvlContactMasterShouldNotBeFound("companycode.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByCompanycodeContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where companycode contains DEFAULT_COMPANYCODE
        defaultAprvlContactMasterShouldBeFound("companycode.contains=" + DEFAULT_COMPANYCODE);

        // Get all the aprvlContactMasterList where companycode contains UPDATED_COMPANYCODE
        defaultAprvlContactMasterShouldNotBeFound("companycode.contains=" + UPDATED_COMPANYCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCompanycodeNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where companycode does not contain DEFAULT_COMPANYCODE
        defaultAprvlContactMasterShouldNotBeFound("companycode.doesNotContain=" + DEFAULT_COMPANYCODE);

        // Get all the aprvlContactMasterList where companycode does not contain UPDATED_COMPANYCODE
        defaultAprvlContactMasterShouldBeFound("companycode.doesNotContain=" + UPDATED_COMPANYCODE);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByPincodeIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where pincode equals to DEFAULT_PINCODE
        defaultAprvlContactMasterShouldBeFound("pincode.equals=" + DEFAULT_PINCODE);

        // Get all the aprvlContactMasterList where pincode equals to UPDATED_PINCODE
        defaultAprvlContactMasterShouldNotBeFound("pincode.equals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByPincodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where pincode not equals to DEFAULT_PINCODE
        defaultAprvlContactMasterShouldNotBeFound("pincode.notEquals=" + DEFAULT_PINCODE);

        // Get all the aprvlContactMasterList where pincode not equals to UPDATED_PINCODE
        defaultAprvlContactMasterShouldBeFound("pincode.notEquals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByPincodeIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where pincode in DEFAULT_PINCODE or UPDATED_PINCODE
        defaultAprvlContactMasterShouldBeFound("pincode.in=" + DEFAULT_PINCODE + "," + UPDATED_PINCODE);

        // Get all the aprvlContactMasterList where pincode equals to UPDATED_PINCODE
        defaultAprvlContactMasterShouldNotBeFound("pincode.in=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByPincodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where pincode is not null
        defaultAprvlContactMasterShouldBeFound("pincode.specified=true");

        // Get all the aprvlContactMasterList where pincode is null
        defaultAprvlContactMasterShouldNotBeFound("pincode.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByPincodeContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where pincode contains DEFAULT_PINCODE
        defaultAprvlContactMasterShouldBeFound("pincode.contains=" + DEFAULT_PINCODE);

        // Get all the aprvlContactMasterList where pincode contains UPDATED_PINCODE
        defaultAprvlContactMasterShouldNotBeFound("pincode.contains=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByPincodeNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where pincode does not contain DEFAULT_PINCODE
        defaultAprvlContactMasterShouldNotBeFound("pincode.doesNotContain=" + DEFAULT_PINCODE);

        // Get all the aprvlContactMasterList where pincode does not contain UPDATED_PINCODE
        defaultAprvlContactMasterShouldBeFound("pincode.doesNotContain=" + UPDATED_PINCODE);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactFullNameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactFullName equals to DEFAULT_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldBeFound("contactFullName.equals=" + DEFAULT_CONTACT_FULL_NAME);

        // Get all the aprvlContactMasterList where contactFullName equals to UPDATED_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldNotBeFound("contactFullName.equals=" + UPDATED_CONTACT_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactFullNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactFullName not equals to DEFAULT_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldNotBeFound("contactFullName.notEquals=" + DEFAULT_CONTACT_FULL_NAME);

        // Get all the aprvlContactMasterList where contactFullName not equals to UPDATED_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldBeFound("contactFullName.notEquals=" + UPDATED_CONTACT_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactFullNameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactFullName in DEFAULT_CONTACT_FULL_NAME or UPDATED_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldBeFound("contactFullName.in=" + DEFAULT_CONTACT_FULL_NAME + "," + UPDATED_CONTACT_FULL_NAME);

        // Get all the aprvlContactMasterList where contactFullName equals to UPDATED_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldNotBeFound("contactFullName.in=" + UPDATED_CONTACT_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactFullNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactFullName is not null
        defaultAprvlContactMasterShouldBeFound("contactFullName.specified=true");

        // Get all the aprvlContactMasterList where contactFullName is null
        defaultAprvlContactMasterShouldNotBeFound("contactFullName.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByContactFullNameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactFullName contains DEFAULT_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldBeFound("contactFullName.contains=" + DEFAULT_CONTACT_FULL_NAME);

        // Get all the aprvlContactMasterList where contactFullName contains UPDATED_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldNotBeFound("contactFullName.contains=" + UPDATED_CONTACT_FULL_NAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactFullNameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactFullName does not contain DEFAULT_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldNotBeFound("contactFullName.doesNotContain=" + DEFAULT_CONTACT_FULL_NAME);

        // Get all the aprvlContactMasterList where contactFullName does not contain UPDATED_CONTACT_FULL_NAME
        defaultAprvlContactMasterShouldBeFound("contactFullName.doesNotContain=" + UPDATED_CONTACT_FULL_NAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByAddress1IsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where address1 equals to DEFAULT_ADDRESS_1
        defaultAprvlContactMasterShouldBeFound("address1.equals=" + DEFAULT_ADDRESS_1);

        // Get all the aprvlContactMasterList where address1 equals to UPDATED_ADDRESS_1
        defaultAprvlContactMasterShouldNotBeFound("address1.equals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByAddress1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where address1 not equals to DEFAULT_ADDRESS_1
        defaultAprvlContactMasterShouldNotBeFound("address1.notEquals=" + DEFAULT_ADDRESS_1);

        // Get all the aprvlContactMasterList where address1 not equals to UPDATED_ADDRESS_1
        defaultAprvlContactMasterShouldBeFound("address1.notEquals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByAddress1IsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where address1 in DEFAULT_ADDRESS_1 or UPDATED_ADDRESS_1
        defaultAprvlContactMasterShouldBeFound("address1.in=" + DEFAULT_ADDRESS_1 + "," + UPDATED_ADDRESS_1);

        // Get all the aprvlContactMasterList where address1 equals to UPDATED_ADDRESS_1
        defaultAprvlContactMasterShouldNotBeFound("address1.in=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByAddress1IsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where address1 is not null
        defaultAprvlContactMasterShouldBeFound("address1.specified=true");

        // Get all the aprvlContactMasterList where address1 is null
        defaultAprvlContactMasterShouldNotBeFound("address1.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByAddress1ContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where address1 contains DEFAULT_ADDRESS_1
        defaultAprvlContactMasterShouldBeFound("address1.contains=" + DEFAULT_ADDRESS_1);

        // Get all the aprvlContactMasterList where address1 contains UPDATED_ADDRESS_1
        defaultAprvlContactMasterShouldNotBeFound("address1.contains=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByAddress1NotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where address1 does not contain DEFAULT_ADDRESS_1
        defaultAprvlContactMasterShouldNotBeFound("address1.doesNotContain=" + DEFAULT_ADDRESS_1);

        // Get all the aprvlContactMasterList where address1 does not contain UPDATED_ADDRESS_1
        defaultAprvlContactMasterShouldBeFound("address1.doesNotContain=" + UPDATED_ADDRESS_1);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCitynameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityname equals to DEFAULT_CITYNAME
        defaultAprvlContactMasterShouldBeFound("cityname.equals=" + DEFAULT_CITYNAME);

        // Get all the aprvlContactMasterList where cityname equals to UPDATED_CITYNAME
        defaultAprvlContactMasterShouldNotBeFound("cityname.equals=" + UPDATED_CITYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCitynameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityname not equals to DEFAULT_CITYNAME
        defaultAprvlContactMasterShouldNotBeFound("cityname.notEquals=" + DEFAULT_CITYNAME);

        // Get all the aprvlContactMasterList where cityname not equals to UPDATED_CITYNAME
        defaultAprvlContactMasterShouldBeFound("cityname.notEquals=" + UPDATED_CITYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCitynameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityname in DEFAULT_CITYNAME or UPDATED_CITYNAME
        defaultAprvlContactMasterShouldBeFound("cityname.in=" + DEFAULT_CITYNAME + "," + UPDATED_CITYNAME);

        // Get all the aprvlContactMasterList where cityname equals to UPDATED_CITYNAME
        defaultAprvlContactMasterShouldNotBeFound("cityname.in=" + UPDATED_CITYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCitynameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityname is not null
        defaultAprvlContactMasterShouldBeFound("cityname.specified=true");

        // Get all the aprvlContactMasterList where cityname is null
        defaultAprvlContactMasterShouldNotBeFound("cityname.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByCitynameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityname contains DEFAULT_CITYNAME
        defaultAprvlContactMasterShouldBeFound("cityname.contains=" + DEFAULT_CITYNAME);

        // Get all the aprvlContactMasterList where cityname contains UPDATED_CITYNAME
        defaultAprvlContactMasterShouldNotBeFound("cityname.contains=" + UPDATED_CITYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCitynameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where cityname does not contain DEFAULT_CITYNAME
        defaultAprvlContactMasterShouldNotBeFound("cityname.doesNotContain=" + DEFAULT_CITYNAME);

        // Get all the aprvlContactMasterList where cityname does not contain UPDATED_CITYNAME
        defaultAprvlContactMasterShouldBeFound("cityname.doesNotContain=" + UPDATED_CITYNAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCountrynameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where countryname equals to DEFAULT_COUNTRYNAME
        defaultAprvlContactMasterShouldBeFound("countryname.equals=" + DEFAULT_COUNTRYNAME);

        // Get all the aprvlContactMasterList where countryname equals to UPDATED_COUNTRYNAME
        defaultAprvlContactMasterShouldNotBeFound("countryname.equals=" + UPDATED_COUNTRYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCountrynameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where countryname not equals to DEFAULT_COUNTRYNAME
        defaultAprvlContactMasterShouldNotBeFound("countryname.notEquals=" + DEFAULT_COUNTRYNAME);

        // Get all the aprvlContactMasterList where countryname not equals to UPDATED_COUNTRYNAME
        defaultAprvlContactMasterShouldBeFound("countryname.notEquals=" + UPDATED_COUNTRYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCountrynameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where countryname in DEFAULT_COUNTRYNAME or UPDATED_COUNTRYNAME
        defaultAprvlContactMasterShouldBeFound("countryname.in=" + DEFAULT_COUNTRYNAME + "," + UPDATED_COUNTRYNAME);

        // Get all the aprvlContactMasterList where countryname equals to UPDATED_COUNTRYNAME
        defaultAprvlContactMasterShouldNotBeFound("countryname.in=" + UPDATED_COUNTRYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCountrynameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where countryname is not null
        defaultAprvlContactMasterShouldBeFound("countryname.specified=true");

        // Get all the aprvlContactMasterList where countryname is null
        defaultAprvlContactMasterShouldNotBeFound("countryname.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByCountrynameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where countryname contains DEFAULT_COUNTRYNAME
        defaultAprvlContactMasterShouldBeFound("countryname.contains=" + DEFAULT_COUNTRYNAME);

        // Get all the aprvlContactMasterList where countryname contains UPDATED_COUNTRYNAME
        defaultAprvlContactMasterShouldNotBeFound("countryname.contains=" + UPDATED_COUNTRYNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCountrynameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where countryname does not contain DEFAULT_COUNTRYNAME
        defaultAprvlContactMasterShouldNotBeFound("countryname.doesNotContain=" + DEFAULT_COUNTRYNAME);

        // Get all the aprvlContactMasterList where countryname does not contain UPDATED_COUNTRYNAME
        defaultAprvlContactMasterShouldBeFound("countryname.doesNotContain=" + UPDATED_COUNTRYNAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByMobileIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where mobile equals to DEFAULT_MOBILE
        defaultAprvlContactMasterShouldBeFound("mobile.equals=" + DEFAULT_MOBILE);

        // Get all the aprvlContactMasterList where mobile equals to UPDATED_MOBILE
        defaultAprvlContactMasterShouldNotBeFound("mobile.equals=" + UPDATED_MOBILE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMobileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where mobile not equals to DEFAULT_MOBILE
        defaultAprvlContactMasterShouldNotBeFound("mobile.notEquals=" + DEFAULT_MOBILE);

        // Get all the aprvlContactMasterList where mobile not equals to UPDATED_MOBILE
        defaultAprvlContactMasterShouldBeFound("mobile.notEquals=" + UPDATED_MOBILE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMobileIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where mobile in DEFAULT_MOBILE or UPDATED_MOBILE
        defaultAprvlContactMasterShouldBeFound("mobile.in=" + DEFAULT_MOBILE + "," + UPDATED_MOBILE);

        // Get all the aprvlContactMasterList where mobile equals to UPDATED_MOBILE
        defaultAprvlContactMasterShouldNotBeFound("mobile.in=" + UPDATED_MOBILE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMobileIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where mobile is not null
        defaultAprvlContactMasterShouldBeFound("mobile.specified=true");

        // Get all the aprvlContactMasterList where mobile is null
        defaultAprvlContactMasterShouldNotBeFound("mobile.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByMobileContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where mobile contains DEFAULT_MOBILE
        defaultAprvlContactMasterShouldBeFound("mobile.contains=" + DEFAULT_MOBILE);

        // Get all the aprvlContactMasterList where mobile contains UPDATED_MOBILE
        defaultAprvlContactMasterShouldNotBeFound("mobile.contains=" + UPDATED_MOBILE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMobileNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where mobile does not contain DEFAULT_MOBILE
        defaultAprvlContactMasterShouldNotBeFound("mobile.doesNotContain=" + DEFAULT_MOBILE);

        // Get all the aprvlContactMasterList where mobile does not contain UPDATED_MOBILE
        defaultAprvlContactMasterShouldBeFound("mobile.doesNotContain=" + UPDATED_MOBILE);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatenameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where statename equals to DEFAULT_STATENAME
        defaultAprvlContactMasterShouldBeFound("statename.equals=" + DEFAULT_STATENAME);

        // Get all the aprvlContactMasterList where statename equals to UPDATED_STATENAME
        defaultAprvlContactMasterShouldNotBeFound("statename.equals=" + UPDATED_STATENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where statename not equals to DEFAULT_STATENAME
        defaultAprvlContactMasterShouldNotBeFound("statename.notEquals=" + DEFAULT_STATENAME);

        // Get all the aprvlContactMasterList where statename not equals to UPDATED_STATENAME
        defaultAprvlContactMasterShouldBeFound("statename.notEquals=" + UPDATED_STATENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatenameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where statename in DEFAULT_STATENAME or UPDATED_STATENAME
        defaultAprvlContactMasterShouldBeFound("statename.in=" + DEFAULT_STATENAME + "," + UPDATED_STATENAME);

        // Get all the aprvlContactMasterList where statename equals to UPDATED_STATENAME
        defaultAprvlContactMasterShouldNotBeFound("statename.in=" + UPDATED_STATENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where statename is not null
        defaultAprvlContactMasterShouldBeFound("statename.specified=true");

        // Get all the aprvlContactMasterList where statename is null
        defaultAprvlContactMasterShouldNotBeFound("statename.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByStatenameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where statename contains DEFAULT_STATENAME
        defaultAprvlContactMasterShouldBeFound("statename.contains=" + DEFAULT_STATENAME);

        // Get all the aprvlContactMasterList where statename contains UPDATED_STATENAME
        defaultAprvlContactMasterShouldNotBeFound("statename.contains=" + UPDATED_STATENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatenameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where statename does not contain DEFAULT_STATENAME
        defaultAprvlContactMasterShouldNotBeFound("statename.doesNotContain=" + DEFAULT_STATENAME);

        // Get all the aprvlContactMasterList where statename does not contain UPDATED_STATENAME
        defaultAprvlContactMasterShouldBeFound("statename.doesNotContain=" + UPDATED_STATENAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate equals to DEFAULT_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.equals=" + DEFAULT_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate equals to UPDATED_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.equals=" + UPDATED_MODIFY_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate not equals to DEFAULT_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.notEquals=" + DEFAULT_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate not equals to UPDATED_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.notEquals=" + UPDATED_MODIFY_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate in DEFAULT_MODIFY_DATE or UPDATED_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.in=" + DEFAULT_MODIFY_DATE + "," + UPDATED_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate equals to UPDATED_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.in=" + UPDATED_MODIFY_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate is not null
        defaultAprvlContactMasterShouldBeFound("modifyDate.specified=true");

        // Get all the aprvlContactMasterList where modifyDate is null
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate is greater than or equal to DEFAULT_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.greaterThanOrEqual=" + DEFAULT_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate is greater than or equal to UPDATED_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.greaterThanOrEqual=" + UPDATED_MODIFY_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate is less than or equal to DEFAULT_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.lessThanOrEqual=" + DEFAULT_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate is less than or equal to SMALLER_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.lessThanOrEqual=" + SMALLER_MODIFY_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate is less than DEFAULT_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.lessThan=" + DEFAULT_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate is less than UPDATED_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.lessThan=" + UPDATED_MODIFY_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByModifyDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where modifyDate is greater than DEFAULT_MODIFY_DATE
        defaultAprvlContactMasterShouldNotBeFound("modifyDate.greaterThan=" + DEFAULT_MODIFY_DATE);

        // Get all the aprvlContactMasterList where modifyDate is greater than SMALLER_MODIFY_DATE
        defaultAprvlContactMasterShouldBeFound("modifyDate.greaterThan=" + SMALLER_MODIFY_DATE);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag equals to DEFAULT_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.equals=" + DEFAULT_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag equals to UPDATED_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.equals=" + UPDATED_OFAFLAG);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag not equals to DEFAULT_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.notEquals=" + DEFAULT_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag not equals to UPDATED_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.notEquals=" + UPDATED_OFAFLAG);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag in DEFAULT_OFAFLAG or UPDATED_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.in=" + DEFAULT_OFAFLAG + "," + UPDATED_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag equals to UPDATED_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.in=" + UPDATED_OFAFLAG);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag is not null
        defaultAprvlContactMasterShouldBeFound("ofaflag.specified=true");

        // Get all the aprvlContactMasterList where ofaflag is null
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag is greater than or equal to DEFAULT_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.greaterThanOrEqual=" + DEFAULT_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag is greater than or equal to UPDATED_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.greaterThanOrEqual=" + UPDATED_OFAFLAG);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag is less than or equal to DEFAULT_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.lessThanOrEqual=" + DEFAULT_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag is less than or equal to SMALLER_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.lessThanOrEqual=" + SMALLER_OFAFLAG);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag is less than DEFAULT_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.lessThan=" + DEFAULT_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag is less than UPDATED_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.lessThan=" + UPDATED_OFAFLAG);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOfaflagIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ofaflag is greater than DEFAULT_OFAFLAG
        defaultAprvlContactMasterShouldNotBeFound("ofaflag.greaterThan=" + DEFAULT_OFAFLAG);

        // Get all the aprvlContactMasterList where ofaflag is greater than SMALLER_OFAFLAG
        defaultAprvlContactMasterShouldBeFound("ofaflag.greaterThan=" + SMALLER_OFAFLAG);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByFirstnameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where firstname equals to DEFAULT_FIRSTNAME
        defaultAprvlContactMasterShouldBeFound("firstname.equals=" + DEFAULT_FIRSTNAME);

        // Get all the aprvlContactMasterList where firstname equals to UPDATED_FIRSTNAME
        defaultAprvlContactMasterShouldNotBeFound("firstname.equals=" + UPDATED_FIRSTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFirstnameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where firstname not equals to DEFAULT_FIRSTNAME
        defaultAprvlContactMasterShouldNotBeFound("firstname.notEquals=" + DEFAULT_FIRSTNAME);

        // Get all the aprvlContactMasterList where firstname not equals to UPDATED_FIRSTNAME
        defaultAprvlContactMasterShouldBeFound("firstname.notEquals=" + UPDATED_FIRSTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFirstnameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where firstname in DEFAULT_FIRSTNAME or UPDATED_FIRSTNAME
        defaultAprvlContactMasterShouldBeFound("firstname.in=" + DEFAULT_FIRSTNAME + "," + UPDATED_FIRSTNAME);

        // Get all the aprvlContactMasterList where firstname equals to UPDATED_FIRSTNAME
        defaultAprvlContactMasterShouldNotBeFound("firstname.in=" + UPDATED_FIRSTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFirstnameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where firstname is not null
        defaultAprvlContactMasterShouldBeFound("firstname.specified=true");

        // Get all the aprvlContactMasterList where firstname is null
        defaultAprvlContactMasterShouldNotBeFound("firstname.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByFirstnameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where firstname contains DEFAULT_FIRSTNAME
        defaultAprvlContactMasterShouldBeFound("firstname.contains=" + DEFAULT_FIRSTNAME);

        // Get all the aprvlContactMasterList where firstname contains UPDATED_FIRSTNAME
        defaultAprvlContactMasterShouldNotBeFound("firstname.contains=" + UPDATED_FIRSTNAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByFirstnameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where firstname does not contain DEFAULT_FIRSTNAME
        defaultAprvlContactMasterShouldNotBeFound("firstname.doesNotContain=" + DEFAULT_FIRSTNAME);

        // Get all the aprvlContactMasterList where firstname does not contain UPDATED_FIRSTNAME
        defaultAprvlContactMasterShouldBeFound("firstname.doesNotContain=" + UPDATED_FIRSTNAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByMiddlenameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where middlename equals to DEFAULT_MIDDLENAME
        defaultAprvlContactMasterShouldBeFound("middlename.equals=" + DEFAULT_MIDDLENAME);

        // Get all the aprvlContactMasterList where middlename equals to UPDATED_MIDDLENAME
        defaultAprvlContactMasterShouldNotBeFound("middlename.equals=" + UPDATED_MIDDLENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMiddlenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where middlename not equals to DEFAULT_MIDDLENAME
        defaultAprvlContactMasterShouldNotBeFound("middlename.notEquals=" + DEFAULT_MIDDLENAME);

        // Get all the aprvlContactMasterList where middlename not equals to UPDATED_MIDDLENAME
        defaultAprvlContactMasterShouldBeFound("middlename.notEquals=" + UPDATED_MIDDLENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMiddlenameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where middlename in DEFAULT_MIDDLENAME or UPDATED_MIDDLENAME
        defaultAprvlContactMasterShouldBeFound("middlename.in=" + DEFAULT_MIDDLENAME + "," + UPDATED_MIDDLENAME);

        // Get all the aprvlContactMasterList where middlename equals to UPDATED_MIDDLENAME
        defaultAprvlContactMasterShouldNotBeFound("middlename.in=" + UPDATED_MIDDLENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMiddlenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where middlename is not null
        defaultAprvlContactMasterShouldBeFound("middlename.specified=true");

        // Get all the aprvlContactMasterList where middlename is null
        defaultAprvlContactMasterShouldNotBeFound("middlename.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByMiddlenameContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where middlename contains DEFAULT_MIDDLENAME
        defaultAprvlContactMasterShouldBeFound("middlename.contains=" + DEFAULT_MIDDLENAME);

        // Get all the aprvlContactMasterList where middlename contains UPDATED_MIDDLENAME
        defaultAprvlContactMasterShouldNotBeFound("middlename.contains=" + UPDATED_MIDDLENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByMiddlenameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where middlename does not contain DEFAULT_MIDDLENAME
        defaultAprvlContactMasterShouldNotBeFound("middlename.doesNotContain=" + DEFAULT_MIDDLENAME);

        // Get all the aprvlContactMasterList where middlename does not contain UPDATED_MIDDLENAME
        defaultAprvlContactMasterShouldBeFound("middlename.doesNotContain=" + UPDATED_MIDDLENAME);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByRemovereasonIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where removereason equals to DEFAULT_REMOVEREASON
        defaultAprvlContactMasterShouldBeFound("removereason.equals=" + DEFAULT_REMOVEREASON);

        // Get all the aprvlContactMasterList where removereason equals to UPDATED_REMOVEREASON
        defaultAprvlContactMasterShouldNotBeFound("removereason.equals=" + UPDATED_REMOVEREASON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRemovereasonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where removereason not equals to DEFAULT_REMOVEREASON
        defaultAprvlContactMasterShouldNotBeFound("removereason.notEquals=" + DEFAULT_REMOVEREASON);

        // Get all the aprvlContactMasterList where removereason not equals to UPDATED_REMOVEREASON
        defaultAprvlContactMasterShouldBeFound("removereason.notEquals=" + UPDATED_REMOVEREASON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRemovereasonIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where removereason in DEFAULT_REMOVEREASON or UPDATED_REMOVEREASON
        defaultAprvlContactMasterShouldBeFound("removereason.in=" + DEFAULT_REMOVEREASON + "," + UPDATED_REMOVEREASON);

        // Get all the aprvlContactMasterList where removereason equals to UPDATED_REMOVEREASON
        defaultAprvlContactMasterShouldNotBeFound("removereason.in=" + UPDATED_REMOVEREASON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRemovereasonIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where removereason is not null
        defaultAprvlContactMasterShouldBeFound("removereason.specified=true");

        // Get all the aprvlContactMasterList where removereason is null
        defaultAprvlContactMasterShouldNotBeFound("removereason.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByRemovereasonContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where removereason contains DEFAULT_REMOVEREASON
        defaultAprvlContactMasterShouldBeFound("removereason.contains=" + DEFAULT_REMOVEREASON);

        // Get all the aprvlContactMasterList where removereason contains UPDATED_REMOVEREASON
        defaultAprvlContactMasterShouldNotBeFound("removereason.contains=" + UPDATED_REMOVEREASON);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByRemovereasonNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where removereason does not contain DEFAULT_REMOVEREASON
        defaultAprvlContactMasterShouldNotBeFound("removereason.doesNotContain=" + DEFAULT_REMOVEREASON);

        // Get all the aprvlContactMasterList where removereason does not contain UPDATED_REMOVEREASON
        defaultAprvlContactMasterShouldBeFound("removereason.doesNotContain=" + UPDATED_REMOVEREASON);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersBySalutationIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where salutation equals to DEFAULT_SALUTATION
        defaultAprvlContactMasterShouldBeFound("salutation.equals=" + DEFAULT_SALUTATION);

        // Get all the aprvlContactMasterList where salutation equals to UPDATED_SALUTATION
        defaultAprvlContactMasterShouldNotBeFound("salutation.equals=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersBySalutationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where salutation not equals to DEFAULT_SALUTATION
        defaultAprvlContactMasterShouldNotBeFound("salutation.notEquals=" + DEFAULT_SALUTATION);

        // Get all the aprvlContactMasterList where salutation not equals to UPDATED_SALUTATION
        defaultAprvlContactMasterShouldBeFound("salutation.notEquals=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersBySalutationIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where salutation in DEFAULT_SALUTATION or UPDATED_SALUTATION
        defaultAprvlContactMasterShouldBeFound("salutation.in=" + DEFAULT_SALUTATION + "," + UPDATED_SALUTATION);

        // Get all the aprvlContactMasterList where salutation equals to UPDATED_SALUTATION
        defaultAprvlContactMasterShouldNotBeFound("salutation.in=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersBySalutationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where salutation is not null
        defaultAprvlContactMasterShouldBeFound("salutation.specified=true");

        // Get all the aprvlContactMasterList where salutation is null
        defaultAprvlContactMasterShouldNotBeFound("salutation.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersBySalutationContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where salutation contains DEFAULT_SALUTATION
        defaultAprvlContactMasterShouldBeFound("salutation.contains=" + DEFAULT_SALUTATION);

        // Get all the aprvlContactMasterList where salutation contains UPDATED_SALUTATION
        defaultAprvlContactMasterShouldNotBeFound("salutation.contains=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersBySalutationNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where salutation does not contain DEFAULT_SALUTATION
        defaultAprvlContactMasterShouldNotBeFound("salutation.doesNotContain=" + DEFAULT_SALUTATION);

        // Get all the aprvlContactMasterList where salutation does not contain UPDATED_SALUTATION
        defaultAprvlContactMasterShouldBeFound("salutation.doesNotContain=" + UPDATED_SALUTATION);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where designation equals to DEFAULT_DESIGNATION
        defaultAprvlContactMasterShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the aprvlContactMasterList where designation equals to UPDATED_DESIGNATION
        defaultAprvlContactMasterShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDesignationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where designation not equals to DEFAULT_DESIGNATION
        defaultAprvlContactMasterShouldNotBeFound("designation.notEquals=" + DEFAULT_DESIGNATION);

        // Get all the aprvlContactMasterList where designation not equals to UPDATED_DESIGNATION
        defaultAprvlContactMasterShouldBeFound("designation.notEquals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultAprvlContactMasterShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the aprvlContactMasterList where designation equals to UPDATED_DESIGNATION
        defaultAprvlContactMasterShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where designation is not null
        defaultAprvlContactMasterShouldBeFound("designation.specified=true");

        // Get all the aprvlContactMasterList where designation is null
        defaultAprvlContactMasterShouldNotBeFound("designation.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByDesignationContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where designation contains DEFAULT_DESIGNATION
        defaultAprvlContactMasterShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the aprvlContactMasterList where designation contains UPDATED_DESIGNATION
        defaultAprvlContactMasterShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where designation does not contain DEFAULT_DESIGNATION
        defaultAprvlContactMasterShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the aprvlContactMasterList where designation does not contain UPDATED_DESIGNATION
        defaultAprvlContactMasterShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where status equals to DEFAULT_STATUS
        defaultAprvlContactMasterShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the aprvlContactMasterList where status equals to UPDATED_STATUS
        defaultAprvlContactMasterShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where status not equals to DEFAULT_STATUS
        defaultAprvlContactMasterShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the aprvlContactMasterList where status not equals to UPDATED_STATUS
        defaultAprvlContactMasterShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultAprvlContactMasterShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the aprvlContactMasterList where status equals to UPDATED_STATUS
        defaultAprvlContactMasterShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where status is not null
        defaultAprvlContactMasterShouldBeFound("status.specified=true");

        // Get all the aprvlContactMasterList where status is null
        defaultAprvlContactMasterShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByStatusContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where status contains DEFAULT_STATUS
        defaultAprvlContactMasterShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the aprvlContactMasterList where status contains UPDATED_STATUS
        defaultAprvlContactMasterShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where status does not contain DEFAULT_STATUS
        defaultAprvlContactMasterShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the aprvlContactMasterList where status does not contain UPDATED_STATUS
        defaultAprvlContactMasterShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno equals to DEFAULT_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.equals=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.equals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno not equals to DEFAULT_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.notEquals=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno not equals to UPDATED_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.notEquals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno in DEFAULT_CTLG_SRNO or UPDATED_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.in=" + DEFAULT_CTLG_SRNO + "," + UPDATED_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.in=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno is not null
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.specified=true");

        // Get all the aprvlContactMasterList where ctlgSrno is null
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno is greater than or equal to DEFAULT_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.greaterThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno is greater than or equal to UPDATED_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.greaterThanOrEqual=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno is less than or equal to DEFAULT_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.lessThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno is less than or equal to SMALLER_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.lessThanOrEqual=" + SMALLER_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno is less than DEFAULT_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.lessThan=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno is less than UPDATED_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.lessThan=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgSrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgSrno is greater than DEFAULT_CTLG_SRNO
        defaultAprvlContactMasterShouldNotBeFound("ctlgSrno.greaterThan=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlContactMasterList where ctlgSrno is greater than SMALLER_CTLG_SRNO
        defaultAprvlContactMasterShouldBeFound("ctlgSrno.greaterThan=" + SMALLER_CTLG_SRNO);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgIsDeleted equals to DEFAULT_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldBeFound("ctlgIsDeleted.equals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlContactMasterList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldNotBeFound("ctlgIsDeleted.equals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgIsDeleted not equals to DEFAULT_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldNotBeFound("ctlgIsDeleted.notEquals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlContactMasterList where ctlgIsDeleted not equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldBeFound("ctlgIsDeleted.notEquals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgIsDeleted in DEFAULT_CTLG_IS_DELETED or UPDATED_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldBeFound("ctlgIsDeleted.in=" + DEFAULT_CTLG_IS_DELETED + "," + UPDATED_CTLG_IS_DELETED);

        // Get all the aprvlContactMasterList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldNotBeFound("ctlgIsDeleted.in=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgIsDeleted is not null
        defaultAprvlContactMasterShouldBeFound("ctlgIsDeleted.specified=true");

        // Get all the aprvlContactMasterList where ctlgIsDeleted is null
        defaultAprvlContactMasterShouldNotBeFound("ctlgIsDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgIsDeleted contains DEFAULT_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldBeFound("ctlgIsDeleted.contains=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlContactMasterList where ctlgIsDeleted contains UPDATED_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldNotBeFound("ctlgIsDeleted.contains=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCtlgIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where ctlgIsDeleted does not contain DEFAULT_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldNotBeFound("ctlgIsDeleted.doesNotContain=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlContactMasterList where ctlgIsDeleted does not contain UPDATED_CTLG_IS_DELETED
        defaultAprvlContactMasterShouldBeFound("ctlgIsDeleted.doesNotContain=" + UPDATED_CTLG_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId equals to DEFAULT_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.equals=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.equals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId not equals to DEFAULT_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.notEquals=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId not equals to UPDATED_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.notEquals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId in DEFAULT_JSON_STORE_ID or UPDATED_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.in=" + DEFAULT_JSON_STORE_ID + "," + UPDATED_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.in=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId is not null
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.specified=true");

        // Get all the aprvlContactMasterList where jsonStoreId is null
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId is greater than or equal to DEFAULT_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.greaterThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId is greater than or equal to UPDATED_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.greaterThanOrEqual=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId is less than or equal to DEFAULT_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.lessThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId is less than or equal to SMALLER_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.lessThanOrEqual=" + SMALLER_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId is less than DEFAULT_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.lessThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId is less than UPDATED_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.lessThan=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByJsonStoreIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where jsonStoreId is greater than DEFAULT_JSON_STORE_ID
        defaultAprvlContactMasterShouldNotBeFound("jsonStoreId.greaterThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlContactMasterList where jsonStoreId is greater than SMALLER_JSON_STORE_ID
        defaultAprvlContactMasterShouldBeFound("jsonStoreId.greaterThan=" + SMALLER_JSON_STORE_ID);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByIsDataProcessedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where isDataProcessed equals to DEFAULT_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldBeFound("isDataProcessed.equals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlContactMasterList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldNotBeFound("isDataProcessed.equals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByIsDataProcessedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where isDataProcessed not equals to DEFAULT_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldNotBeFound("isDataProcessed.notEquals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlContactMasterList where isDataProcessed not equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldBeFound("isDataProcessed.notEquals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByIsDataProcessedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where isDataProcessed in DEFAULT_IS_DATA_PROCESSED or UPDATED_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldBeFound("isDataProcessed.in=" + DEFAULT_IS_DATA_PROCESSED + "," + UPDATED_IS_DATA_PROCESSED);

        // Get all the aprvlContactMasterList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldNotBeFound("isDataProcessed.in=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByIsDataProcessedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where isDataProcessed is not null
        defaultAprvlContactMasterShouldBeFound("isDataProcessed.specified=true");

        // Get all the aprvlContactMasterList where isDataProcessed is null
        defaultAprvlContactMasterShouldNotBeFound("isDataProcessed.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByIsDataProcessedContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where isDataProcessed contains DEFAULT_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldBeFound("isDataProcessed.contains=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlContactMasterList where isDataProcessed contains UPDATED_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldNotBeFound("isDataProcessed.contains=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByIsDataProcessedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where isDataProcessed does not contain DEFAULT_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldNotBeFound("isDataProcessed.doesNotContain=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlContactMasterList where isDataProcessed does not contain UPDATED_IS_DATA_PROCESSED
        defaultAprvlContactMasterShouldBeFound("isDataProcessed.doesNotContain=" + UPDATED_IS_DATA_PROCESSED);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultAprvlContactMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the aprvlContactMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the aprvlContactMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlContactMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultAprvlContactMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the aprvlContactMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedDate is not null
        defaultAprvlContactMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the aprvlContactMasterList where lastModifiedDate is null
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlContactMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlContactMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the aprvlContactMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedBy is not null
        defaultAprvlContactMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the aprvlContactMasterList where lastModifiedBy is null
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlContactMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlContactMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultAprvlContactMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultAprvlContactMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the aprvlContactMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultAprvlContactMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultAprvlContactMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the aprvlContactMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultAprvlContactMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultAprvlContactMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the aprvlContactMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultAprvlContactMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdBy is not null
        defaultAprvlContactMasterShouldBeFound("createdBy.specified=true");

        // Get all the aprvlContactMasterList where createdBy is null
        defaultAprvlContactMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultAprvlContactMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the aprvlContactMasterList where createdBy contains UPDATED_CREATED_BY
        defaultAprvlContactMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultAprvlContactMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the aprvlContactMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultAprvlContactMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultAprvlContactMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the aprvlContactMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultAprvlContactMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultAprvlContactMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the aprvlContactMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultAprvlContactMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultAprvlContactMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the aprvlContactMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultAprvlContactMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where createdDate is not null
        defaultAprvlContactMasterShouldBeFound("createdDate.specified=true");

        // Get all the aprvlContactMasterList where createdDate is null
        defaultAprvlContactMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where operation equals to DEFAULT_OPERATION
        defaultAprvlContactMasterShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the aprvlContactMasterList where operation equals to UPDATED_OPERATION
        defaultAprvlContactMasterShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where operation not equals to DEFAULT_OPERATION
        defaultAprvlContactMasterShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the aprvlContactMasterList where operation not equals to UPDATED_OPERATION
        defaultAprvlContactMasterShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultAprvlContactMasterShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the aprvlContactMasterList where operation equals to UPDATED_OPERATION
        defaultAprvlContactMasterShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where operation is not null
        defaultAprvlContactMasterShouldBeFound("operation.specified=true");

        // Get all the aprvlContactMasterList where operation is null
        defaultAprvlContactMasterShouldNotBeFound("operation.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByOperationContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where operation contains DEFAULT_OPERATION
        defaultAprvlContactMasterShouldBeFound("operation.contains=" + DEFAULT_OPERATION);

        // Get all the aprvlContactMasterList where operation contains UPDATED_OPERATION
        defaultAprvlContactMasterShouldNotBeFound("operation.contains=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByOperationNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where operation does not contain DEFAULT_OPERATION
        defaultAprvlContactMasterShouldNotBeFound("operation.doesNotContain=" + DEFAULT_OPERATION);

        // Get all the aprvlContactMasterList where operation does not contain UPDATED_OPERATION
        defaultAprvlContactMasterShouldBeFound("operation.doesNotContain=" + UPDATED_OPERATION);
    }


    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactId equals to DEFAULT_CONTACT_ID
        defaultAprvlContactMasterShouldBeFound("contactId.equals=" + DEFAULT_CONTACT_ID);

        // Get all the aprvlContactMasterList where contactId equals to UPDATED_CONTACT_ID
        defaultAprvlContactMasterShouldNotBeFound("contactId.equals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactId not equals to DEFAULT_CONTACT_ID
        defaultAprvlContactMasterShouldNotBeFound("contactId.notEquals=" + DEFAULT_CONTACT_ID);

        // Get all the aprvlContactMasterList where contactId not equals to UPDATED_CONTACT_ID
        defaultAprvlContactMasterShouldBeFound("contactId.notEquals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactId in DEFAULT_CONTACT_ID or UPDATED_CONTACT_ID
        defaultAprvlContactMasterShouldBeFound("contactId.in=" + DEFAULT_CONTACT_ID + "," + UPDATED_CONTACT_ID);

        // Get all the aprvlContactMasterList where contactId equals to UPDATED_CONTACT_ID
        defaultAprvlContactMasterShouldNotBeFound("contactId.in=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactId is not null
        defaultAprvlContactMasterShouldBeFound("contactId.specified=true");

        // Get all the aprvlContactMasterList where contactId is null
        defaultAprvlContactMasterShouldNotBeFound("contactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlContactMastersByContactIdContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactId contains DEFAULT_CONTACT_ID
        defaultAprvlContactMasterShouldBeFound("contactId.contains=" + DEFAULT_CONTACT_ID);

        // Get all the aprvlContactMasterList where contactId contains UPDATED_CONTACT_ID
        defaultAprvlContactMasterShouldNotBeFound("contactId.contains=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlContactMastersByContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlContactMasterRepository.saveAndFlush(aprvlContactMaster);

        // Get all the aprvlContactMasterList where contactId does not contain DEFAULT_CONTACT_ID
        defaultAprvlContactMasterShouldNotBeFound("contactId.doesNotContain=" + DEFAULT_CONTACT_ID);

        // Get all the aprvlContactMasterList where contactId does not contain UPDATED_CONTACT_ID
        defaultAprvlContactMasterShouldBeFound("contactId.doesNotContain=" + UPDATED_CONTACT_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAprvlContactMasterShouldBeFound(String filter) throws Exception {
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aprvlContactMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].lastname").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].joinednewcompany").value(hasItem(DEFAULT_JOINEDNEWCOMPANY)))
            .andExpect(jsonPath("$.[*].cityid").value(hasItem(DEFAULT_CITYID)))
            .andExpect(jsonPath("$.[*].hdeleted").value(hasItem(DEFAULT_HDELETED)))
            .andExpect(jsonPath("$.[*].recordid").value(hasItem(DEFAULT_RECORDID)))
            .andExpect(jsonPath("$.[*].levelimportance").value(hasItem(DEFAULT_LEVELIMPORTANCE)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].keyperson").value(hasItem(DEFAULT_KEYPERSON)))
            .andExpect(jsonPath("$.[*].companycode").value(hasItem(DEFAULT_COMPANYCODE)))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3.toString())))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2.toString())))
            .andExpect(jsonPath("$.[*].contactFullName").value(hasItem(DEFAULT_CONTACT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].cityname").value(hasItem(DEFAULT_CITYNAME)))
            .andExpect(jsonPath("$.[*].countryname").value(hasItem(DEFAULT_COUNTRYNAME)))
            .andExpect(jsonPath("$.[*].mobile").value(hasItem(DEFAULT_MOBILE)))
            .andExpect(jsonPath("$.[*].statename").value(hasItem(DEFAULT_STATENAME)))
            .andExpect(jsonPath("$.[*].modifyDate").value(hasItem(DEFAULT_MODIFY_DATE.toString())))
            .andExpect(jsonPath("$.[*].ofaflag").value(hasItem(DEFAULT_OFAFLAG)))
            .andExpect(jsonPath("$.[*].firstname").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].middlename").value(hasItem(DEFAULT_MIDDLENAME)))
            .andExpect(jsonPath("$.[*].removereason").value(hasItem(DEFAULT_REMOVEREASON)))
            .andExpect(jsonPath("$.[*].salutation").value(hasItem(DEFAULT_SALUTATION)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID.intValue())))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())));

        // Check, that the count call also returns 1
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAprvlContactMasterShouldNotBeFound(String filter) throws Exception {
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAprvlContactMaster() throws Exception {
        // Get the aprvlContactMaster
        restAprvlContactMasterMockMvc.perform(get("/api/aprvl-contact-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAprvlContactMaster() throws Exception {
        // Initialize the database
        aprvlContactMasterService.save(aprvlContactMaster);

        int databaseSizeBeforeUpdate = aprvlContactMasterRepository.findAll().size();

        // Update the aprvlContactMaster
        AprvlContactMaster updatedAprvlContactMaster = aprvlContactMasterRepository.findById(aprvlContactMaster.getId()).get();
        // Disconnect from session so that the updates on updatedAprvlContactMaster are not directly saved in db
        em.detach(updatedAprvlContactMaster);
        updatedAprvlContactMaster
            .lastname(UPDATED_LASTNAME)
            .joinednewcompany(UPDATED_JOINEDNEWCOMPANY)
            .cityid(UPDATED_CITYID)
            .hdeleted(UPDATED_HDELETED)
            .recordid(UPDATED_RECORDID)
            .levelimportance(UPDATED_LEVELIMPORTANCE)
            .department(UPDATED_DEPARTMENT)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .keyperson(UPDATED_KEYPERSON)
            .companycode(UPDATED_COMPANYCODE)
            .pincode(UPDATED_PINCODE)
            .comments(UPDATED_COMMENTS)
            .address3(UPDATED_ADDRESS_3)
            .address2(UPDATED_ADDRESS_2)
            .contactFullName(UPDATED_CONTACT_FULL_NAME)
            .address1(UPDATED_ADDRESS_1)
            .cityname(UPDATED_CITYNAME)
            .countryname(UPDATED_COUNTRYNAME)
            .mobile(UPDATED_MOBILE)
            .statename(UPDATED_STATENAME)
            .modifyDate(UPDATED_MODIFY_DATE)
            .ofaflag(UPDATED_OFAFLAG)
            .firstname(UPDATED_FIRSTNAME)
            .middlename(UPDATED_MIDDLENAME)
            .removereason(UPDATED_REMOVEREASON)
            .salutation(UPDATED_SALUTATION)
            .designation(UPDATED_DESIGNATION)
            .status(UPDATED_STATUS)
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .operation(UPDATED_OPERATION)
            .contactId(UPDATED_CONTACT_ID)
            .phone(UPDATED_PHONE);

        restAprvlContactMasterMockMvc.perform(put("/api/aprvl-contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAprvlContactMaster)))
            .andExpect(status().isOk());

        // Validate the AprvlContactMaster in the database
        List<AprvlContactMaster> aprvlContactMasterList = aprvlContactMasterRepository.findAll();
        assertThat(aprvlContactMasterList).hasSize(databaseSizeBeforeUpdate);
        AprvlContactMaster testAprvlContactMaster = aprvlContactMasterList.get(aprvlContactMasterList.size() - 1);
        assertThat(testAprvlContactMaster.getLastname()).isEqualTo(UPDATED_LASTNAME);
        assertThat(testAprvlContactMaster.getJoinednewcompany()).isEqualTo(UPDATED_JOINEDNEWCOMPANY);
        assertThat(testAprvlContactMaster.getCityid()).isEqualTo(UPDATED_CITYID);
        assertThat(testAprvlContactMaster.getHdeleted()).isEqualTo(UPDATED_HDELETED);
        assertThat(testAprvlContactMaster.getRecordid()).isEqualTo(UPDATED_RECORDID);
        assertThat(testAprvlContactMaster.getLevelimportance()).isEqualTo(UPDATED_LEVELIMPORTANCE);
        assertThat(testAprvlContactMaster.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testAprvlContactMaster.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testAprvlContactMaster.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAprvlContactMaster.getKeyperson()).isEqualTo(UPDATED_KEYPERSON);
        assertThat(testAprvlContactMaster.getCompanycode()).isEqualTo(UPDATED_COMPANYCODE);
        assertThat(testAprvlContactMaster.getPincode()).isEqualTo(UPDATED_PINCODE);
        assertThat(testAprvlContactMaster.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAprvlContactMaster.getAddress3()).isEqualTo(UPDATED_ADDRESS_3);
        assertThat(testAprvlContactMaster.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testAprvlContactMaster.getContactFullName()).isEqualTo(UPDATED_CONTACT_FULL_NAME);
        assertThat(testAprvlContactMaster.getAddress1()).isEqualTo(UPDATED_ADDRESS_1);
        assertThat(testAprvlContactMaster.getCityname()).isEqualTo(UPDATED_CITYNAME);
        assertThat(testAprvlContactMaster.getCountryname()).isEqualTo(UPDATED_COUNTRYNAME);
        assertThat(testAprvlContactMaster.getMobile()).isEqualTo(UPDATED_MOBILE);
        assertThat(testAprvlContactMaster.getStatename()).isEqualTo(UPDATED_STATENAME);
        assertThat(testAprvlContactMaster.getModifyDate()).isEqualTo(UPDATED_MODIFY_DATE);
        assertThat(testAprvlContactMaster.getOfaflag()).isEqualTo(UPDATED_OFAFLAG);
        assertThat(testAprvlContactMaster.getFirstname()).isEqualTo(UPDATED_FIRSTNAME);
        assertThat(testAprvlContactMaster.getMiddlename()).isEqualTo(UPDATED_MIDDLENAME);
        assertThat(testAprvlContactMaster.getRemovereason()).isEqualTo(UPDATED_REMOVEREASON);
        assertThat(testAprvlContactMaster.getSalutation()).isEqualTo(UPDATED_SALUTATION);
        assertThat(testAprvlContactMaster.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testAprvlContactMaster.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAprvlContactMaster.getCtlgSrno()).isEqualTo(UPDATED_CTLG_SRNO);
        assertThat(testAprvlContactMaster.getCtlgIsDeleted()).isEqualTo(UPDATED_CTLG_IS_DELETED);
        assertThat(testAprvlContactMaster.getJsonStoreId()).isEqualTo(UPDATED_JSON_STORE_ID);
        assertThat(testAprvlContactMaster.getIsDataProcessed()).isEqualTo(UPDATED_IS_DATA_PROCESSED);
        assertThat(testAprvlContactMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testAprvlContactMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testAprvlContactMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAprvlContactMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAprvlContactMaster.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testAprvlContactMaster.getContactId()).isEqualTo(UPDATED_CONTACT_ID);
        assertThat(testAprvlContactMaster.getPhone()).isEqualTo(UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void updateNonExistingAprvlContactMaster() throws Exception {
        int databaseSizeBeforeUpdate = aprvlContactMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAprvlContactMasterMockMvc.perform(put("/api/aprvl-contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlContactMaster)))
            .andExpect(status().isBadRequest());

        // Validate the AprvlContactMaster in the database
        List<AprvlContactMaster> aprvlContactMasterList = aprvlContactMasterRepository.findAll();
        assertThat(aprvlContactMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAprvlContactMaster() throws Exception {
        // Initialize the database
        aprvlContactMasterService.save(aprvlContactMaster);

        int databaseSizeBeforeDelete = aprvlContactMasterRepository.findAll().size();

        // Delete the aprvlContactMaster
        restAprvlContactMasterMockMvc.perform(delete("/api/aprvl-contact-masters/{id}", aprvlContactMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AprvlContactMaster> aprvlContactMasterList = aprvlContactMasterRepository.findAll();
        assertThat(aprvlContactMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
