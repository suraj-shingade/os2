package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompGstNotApplicable;
import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.repository.CompGstNotApplicableRepository;
import com.crisil.onesource.service.CompGstNotApplicableService;
import com.crisil.onesource.service.dto.CompGstNotApplicableCriteria;
import com.crisil.onesource.service.CompGstNotApplicableQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompGstNotApplicableResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompGstNotApplicableResourceIT {

    private static final String DEFAULT_REQUEST_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REQUESTER_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTER_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_REQUESTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_REQUESTED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REQUESTED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_APPROVER_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_APPROVER_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_APPROVED_BY = "AAAAAAAAAA";
    private static final String UPDATED_APPROVED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_APPROVED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_APPROVED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_GST_NA_FILE = "AAAAAAAAAA";
    private static final String UPDATED_GST_NA_FILE = "BBBBBBBBBB";

    private static final String DEFAULT_IS_REVERSE_REQUEST = "A";
    private static final String UPDATED_IS_REVERSE_REQUEST = "B";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private CompGstNotApplicableRepository compGstNotApplicableRepository;

    @Autowired
    private CompGstNotApplicableService compGstNotApplicableService;

    @Autowired
    private CompGstNotApplicableQueryService compGstNotApplicableQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompGstNotApplicableMockMvc;

    private CompGstNotApplicable compGstNotApplicable;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompGstNotApplicable createEntity(EntityManager em) {
        CompGstNotApplicable compGstNotApplicable = new CompGstNotApplicable()
            .requestStatus(DEFAULT_REQUEST_STATUS)
            .requesterComments(DEFAULT_REQUESTER_COMMENTS)
            .requestedBy(DEFAULT_REQUESTED_BY)
            .requestedDate(DEFAULT_REQUESTED_DATE)
            .approverComments(DEFAULT_APPROVER_COMMENTS)
            .approvedBy(DEFAULT_APPROVED_BY)
            .approvedDate(DEFAULT_APPROVED_DATE)
            .gstNaFile(DEFAULT_GST_NA_FILE)
            .isReverseRequest(DEFAULT_IS_REVERSE_REQUEST)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED);
        return compGstNotApplicable;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompGstNotApplicable createUpdatedEntity(EntityManager em) {
        CompGstNotApplicable compGstNotApplicable = new CompGstNotApplicable()
            .requestStatus(UPDATED_REQUEST_STATUS)
            .requesterComments(UPDATED_REQUESTER_COMMENTS)
            .requestedBy(UPDATED_REQUESTED_BY)
            .requestedDate(UPDATED_REQUESTED_DATE)
            .approverComments(UPDATED_APPROVER_COMMENTS)
            .approvedBy(UPDATED_APPROVED_BY)
            .approvedDate(UPDATED_APPROVED_DATE)
            .gstNaFile(UPDATED_GST_NA_FILE)
            .isReverseRequest(UPDATED_IS_REVERSE_REQUEST)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);
        return compGstNotApplicable;
    }

    @BeforeEach
    public void initTest() {
        compGstNotApplicable = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompGstNotApplicable() throws Exception {
        int databaseSizeBeforeCreate = compGstNotApplicableRepository.findAll().size();
        // Create the CompGstNotApplicable
        restCompGstNotApplicableMockMvc.perform(post("/api/comp-gst-not-applicables")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compGstNotApplicable)))
            .andExpect(status().isCreated());

        // Validate the CompGstNotApplicable in the database
        List<CompGstNotApplicable> compGstNotApplicableList = compGstNotApplicableRepository.findAll();
        assertThat(compGstNotApplicableList).hasSize(databaseSizeBeforeCreate + 1);
        CompGstNotApplicable testCompGstNotApplicable = compGstNotApplicableList.get(compGstNotApplicableList.size() - 1);
        assertThat(testCompGstNotApplicable.getRequestStatus()).isEqualTo(DEFAULT_REQUEST_STATUS);
        assertThat(testCompGstNotApplicable.getRequesterComments()).isEqualTo(DEFAULT_REQUESTER_COMMENTS);
        assertThat(testCompGstNotApplicable.getRequestedBy()).isEqualTo(DEFAULT_REQUESTED_BY);
        assertThat(testCompGstNotApplicable.getRequestedDate()).isEqualTo(DEFAULT_REQUESTED_DATE);
        assertThat(testCompGstNotApplicable.getApproverComments()).isEqualTo(DEFAULT_APPROVER_COMMENTS);
        assertThat(testCompGstNotApplicable.getApprovedBy()).isEqualTo(DEFAULT_APPROVED_BY);
        assertThat(testCompGstNotApplicable.getApprovedDate()).isEqualTo(DEFAULT_APPROVED_DATE);
        assertThat(testCompGstNotApplicable.getGstNaFile()).isEqualTo(DEFAULT_GST_NA_FILE);
        assertThat(testCompGstNotApplicable.getIsReverseRequest()).isEqualTo(DEFAULT_IS_REVERSE_REQUEST);
        assertThat(testCompGstNotApplicable.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompGstNotApplicable.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompGstNotApplicable.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompGstNotApplicable.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createCompGstNotApplicableWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = compGstNotApplicableRepository.findAll().size();

        // Create the CompGstNotApplicable with an existing ID
        compGstNotApplicable.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompGstNotApplicableMockMvc.perform(post("/api/comp-gst-not-applicables")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compGstNotApplicable)))
            .andExpect(status().isBadRequest());

        // Validate the CompGstNotApplicable in the database
        List<CompGstNotApplicable> compGstNotApplicableList = compGstNotApplicableRepository.findAll();
        assertThat(compGstNotApplicableList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicables() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compGstNotApplicable.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].requesterComments").value(hasItem(DEFAULT_REQUESTER_COMMENTS)))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].requestedDate").value(hasItem(DEFAULT_REQUESTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].approverComments").value(hasItem(DEFAULT_APPROVER_COMMENTS)))
            .andExpect(jsonPath("$.[*].approvedBy").value(hasItem(DEFAULT_APPROVED_BY)))
            .andExpect(jsonPath("$.[*].approvedDate").value(hasItem(DEFAULT_APPROVED_DATE.toString())))
            .andExpect(jsonPath("$.[*].gstNaFile").value(hasItem(DEFAULT_GST_NA_FILE)))
            .andExpect(jsonPath("$.[*].isReverseRequest").value(hasItem(DEFAULT_IS_REVERSE_REQUEST)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getCompGstNotApplicable() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get the compGstNotApplicable
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables/{id}", compGstNotApplicable.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(compGstNotApplicable.getId().intValue()))
            .andExpect(jsonPath("$.requestStatus").value(DEFAULT_REQUEST_STATUS))
            .andExpect(jsonPath("$.requesterComments").value(DEFAULT_REQUESTER_COMMENTS))
            .andExpect(jsonPath("$.requestedBy").value(DEFAULT_REQUESTED_BY))
            .andExpect(jsonPath("$.requestedDate").value(DEFAULT_REQUESTED_DATE.toString()))
            .andExpect(jsonPath("$.approverComments").value(DEFAULT_APPROVER_COMMENTS))
            .andExpect(jsonPath("$.approvedBy").value(DEFAULT_APPROVED_BY))
            .andExpect(jsonPath("$.approvedDate").value(DEFAULT_APPROVED_DATE.toString()))
            .andExpect(jsonPath("$.gstNaFile").value(DEFAULT_GST_NA_FILE))
            .andExpect(jsonPath("$.isReverseRequest").value(DEFAULT_IS_REVERSE_REQUEST))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getCompGstNotApplicablesByIdFiltering() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        Long id = compGstNotApplicable.getId();

        defaultCompGstNotApplicableShouldBeFound("id.equals=" + id);
        defaultCompGstNotApplicableShouldNotBeFound("id.notEquals=" + id);

        defaultCompGstNotApplicableShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompGstNotApplicableShouldNotBeFound("id.greaterThan=" + id);

        defaultCompGstNotApplicableShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompGstNotApplicableShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestStatus equals to DEFAULT_REQUEST_STATUS
        defaultCompGstNotApplicableShouldBeFound("requestStatus.equals=" + DEFAULT_REQUEST_STATUS);

        // Get all the compGstNotApplicableList where requestStatus equals to UPDATED_REQUEST_STATUS
        defaultCompGstNotApplicableShouldNotBeFound("requestStatus.equals=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestStatus not equals to DEFAULT_REQUEST_STATUS
        defaultCompGstNotApplicableShouldNotBeFound("requestStatus.notEquals=" + DEFAULT_REQUEST_STATUS);

        // Get all the compGstNotApplicableList where requestStatus not equals to UPDATED_REQUEST_STATUS
        defaultCompGstNotApplicableShouldBeFound("requestStatus.notEquals=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestStatusIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestStatus in DEFAULT_REQUEST_STATUS or UPDATED_REQUEST_STATUS
        defaultCompGstNotApplicableShouldBeFound("requestStatus.in=" + DEFAULT_REQUEST_STATUS + "," + UPDATED_REQUEST_STATUS);

        // Get all the compGstNotApplicableList where requestStatus equals to UPDATED_REQUEST_STATUS
        defaultCompGstNotApplicableShouldNotBeFound("requestStatus.in=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestStatus is not null
        defaultCompGstNotApplicableShouldBeFound("requestStatus.specified=true");

        // Get all the compGstNotApplicableList where requestStatus is null
        defaultCompGstNotApplicableShouldNotBeFound("requestStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestStatusContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestStatus contains DEFAULT_REQUEST_STATUS
        defaultCompGstNotApplicableShouldBeFound("requestStatus.contains=" + DEFAULT_REQUEST_STATUS);

        // Get all the compGstNotApplicableList where requestStatus contains UPDATED_REQUEST_STATUS
        defaultCompGstNotApplicableShouldNotBeFound("requestStatus.contains=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestStatusNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestStatus does not contain DEFAULT_REQUEST_STATUS
        defaultCompGstNotApplicableShouldNotBeFound("requestStatus.doesNotContain=" + DEFAULT_REQUEST_STATUS);

        // Get all the compGstNotApplicableList where requestStatus does not contain UPDATED_REQUEST_STATUS
        defaultCompGstNotApplicableShouldBeFound("requestStatus.doesNotContain=" + UPDATED_REQUEST_STATUS);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequesterCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requesterComments equals to DEFAULT_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("requesterComments.equals=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the compGstNotApplicableList where requesterComments equals to UPDATED_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("requesterComments.equals=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequesterCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requesterComments not equals to DEFAULT_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("requesterComments.notEquals=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the compGstNotApplicableList where requesterComments not equals to UPDATED_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("requesterComments.notEquals=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequesterCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requesterComments in DEFAULT_REQUESTER_COMMENTS or UPDATED_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("requesterComments.in=" + DEFAULT_REQUESTER_COMMENTS + "," + UPDATED_REQUESTER_COMMENTS);

        // Get all the compGstNotApplicableList where requesterComments equals to UPDATED_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("requesterComments.in=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequesterCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requesterComments is not null
        defaultCompGstNotApplicableShouldBeFound("requesterComments.specified=true");

        // Get all the compGstNotApplicableList where requesterComments is null
        defaultCompGstNotApplicableShouldNotBeFound("requesterComments.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequesterCommentsContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requesterComments contains DEFAULT_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("requesterComments.contains=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the compGstNotApplicableList where requesterComments contains UPDATED_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("requesterComments.contains=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequesterCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requesterComments does not contain DEFAULT_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("requesterComments.doesNotContain=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the compGstNotApplicableList where requesterComments does not contain UPDATED_REQUESTER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("requesterComments.doesNotContain=" + UPDATED_REQUESTER_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedByIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedBy equals to DEFAULT_REQUESTED_BY
        defaultCompGstNotApplicableShouldBeFound("requestedBy.equals=" + DEFAULT_REQUESTED_BY);

        // Get all the compGstNotApplicableList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultCompGstNotApplicableShouldNotBeFound("requestedBy.equals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedBy not equals to DEFAULT_REQUESTED_BY
        defaultCompGstNotApplicableShouldNotBeFound("requestedBy.notEquals=" + DEFAULT_REQUESTED_BY);

        // Get all the compGstNotApplicableList where requestedBy not equals to UPDATED_REQUESTED_BY
        defaultCompGstNotApplicableShouldBeFound("requestedBy.notEquals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedByIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedBy in DEFAULT_REQUESTED_BY or UPDATED_REQUESTED_BY
        defaultCompGstNotApplicableShouldBeFound("requestedBy.in=" + DEFAULT_REQUESTED_BY + "," + UPDATED_REQUESTED_BY);

        // Get all the compGstNotApplicableList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultCompGstNotApplicableShouldNotBeFound("requestedBy.in=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedBy is not null
        defaultCompGstNotApplicableShouldBeFound("requestedBy.specified=true");

        // Get all the compGstNotApplicableList where requestedBy is null
        defaultCompGstNotApplicableShouldNotBeFound("requestedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedByContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedBy contains DEFAULT_REQUESTED_BY
        defaultCompGstNotApplicableShouldBeFound("requestedBy.contains=" + DEFAULT_REQUESTED_BY);

        // Get all the compGstNotApplicableList where requestedBy contains UPDATED_REQUESTED_BY
        defaultCompGstNotApplicableShouldNotBeFound("requestedBy.contains=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedByNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedBy does not contain DEFAULT_REQUESTED_BY
        defaultCompGstNotApplicableShouldNotBeFound("requestedBy.doesNotContain=" + DEFAULT_REQUESTED_BY);

        // Get all the compGstNotApplicableList where requestedBy does not contain UPDATED_REQUESTED_BY
        defaultCompGstNotApplicableShouldBeFound("requestedBy.doesNotContain=" + UPDATED_REQUESTED_BY);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedDate equals to DEFAULT_REQUESTED_DATE
        defaultCompGstNotApplicableShouldBeFound("requestedDate.equals=" + DEFAULT_REQUESTED_DATE);

        // Get all the compGstNotApplicableList where requestedDate equals to UPDATED_REQUESTED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("requestedDate.equals=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedDate not equals to DEFAULT_REQUESTED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("requestedDate.notEquals=" + DEFAULT_REQUESTED_DATE);

        // Get all the compGstNotApplicableList where requestedDate not equals to UPDATED_REQUESTED_DATE
        defaultCompGstNotApplicableShouldBeFound("requestedDate.notEquals=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedDateIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedDate in DEFAULT_REQUESTED_DATE or UPDATED_REQUESTED_DATE
        defaultCompGstNotApplicableShouldBeFound("requestedDate.in=" + DEFAULT_REQUESTED_DATE + "," + UPDATED_REQUESTED_DATE);

        // Get all the compGstNotApplicableList where requestedDate equals to UPDATED_REQUESTED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("requestedDate.in=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByRequestedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where requestedDate is not null
        defaultCompGstNotApplicableShouldBeFound("requestedDate.specified=true");

        // Get all the compGstNotApplicableList where requestedDate is null
        defaultCompGstNotApplicableShouldNotBeFound("requestedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApproverCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approverComments equals to DEFAULT_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("approverComments.equals=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the compGstNotApplicableList where approverComments equals to UPDATED_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("approverComments.equals=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApproverCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approverComments not equals to DEFAULT_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("approverComments.notEquals=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the compGstNotApplicableList where approverComments not equals to UPDATED_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("approverComments.notEquals=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApproverCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approverComments in DEFAULT_APPROVER_COMMENTS or UPDATED_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("approverComments.in=" + DEFAULT_APPROVER_COMMENTS + "," + UPDATED_APPROVER_COMMENTS);

        // Get all the compGstNotApplicableList where approverComments equals to UPDATED_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("approverComments.in=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApproverCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approverComments is not null
        defaultCompGstNotApplicableShouldBeFound("approverComments.specified=true");

        // Get all the compGstNotApplicableList where approverComments is null
        defaultCompGstNotApplicableShouldNotBeFound("approverComments.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApproverCommentsContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approverComments contains DEFAULT_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("approverComments.contains=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the compGstNotApplicableList where approverComments contains UPDATED_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("approverComments.contains=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApproverCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approverComments does not contain DEFAULT_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldNotBeFound("approverComments.doesNotContain=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the compGstNotApplicableList where approverComments does not contain UPDATED_APPROVER_COMMENTS
        defaultCompGstNotApplicableShouldBeFound("approverComments.doesNotContain=" + UPDATED_APPROVER_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedByIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedBy equals to DEFAULT_APPROVED_BY
        defaultCompGstNotApplicableShouldBeFound("approvedBy.equals=" + DEFAULT_APPROVED_BY);

        // Get all the compGstNotApplicableList where approvedBy equals to UPDATED_APPROVED_BY
        defaultCompGstNotApplicableShouldNotBeFound("approvedBy.equals=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedBy not equals to DEFAULT_APPROVED_BY
        defaultCompGstNotApplicableShouldNotBeFound("approvedBy.notEquals=" + DEFAULT_APPROVED_BY);

        // Get all the compGstNotApplicableList where approvedBy not equals to UPDATED_APPROVED_BY
        defaultCompGstNotApplicableShouldBeFound("approvedBy.notEquals=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedByIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedBy in DEFAULT_APPROVED_BY or UPDATED_APPROVED_BY
        defaultCompGstNotApplicableShouldBeFound("approvedBy.in=" + DEFAULT_APPROVED_BY + "," + UPDATED_APPROVED_BY);

        // Get all the compGstNotApplicableList where approvedBy equals to UPDATED_APPROVED_BY
        defaultCompGstNotApplicableShouldNotBeFound("approvedBy.in=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedBy is not null
        defaultCompGstNotApplicableShouldBeFound("approvedBy.specified=true");

        // Get all the compGstNotApplicableList where approvedBy is null
        defaultCompGstNotApplicableShouldNotBeFound("approvedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedByContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedBy contains DEFAULT_APPROVED_BY
        defaultCompGstNotApplicableShouldBeFound("approvedBy.contains=" + DEFAULT_APPROVED_BY);

        // Get all the compGstNotApplicableList where approvedBy contains UPDATED_APPROVED_BY
        defaultCompGstNotApplicableShouldNotBeFound("approvedBy.contains=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedByNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedBy does not contain DEFAULT_APPROVED_BY
        defaultCompGstNotApplicableShouldNotBeFound("approvedBy.doesNotContain=" + DEFAULT_APPROVED_BY);

        // Get all the compGstNotApplicableList where approvedBy does not contain UPDATED_APPROVED_BY
        defaultCompGstNotApplicableShouldBeFound("approvedBy.doesNotContain=" + UPDATED_APPROVED_BY);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedDate equals to DEFAULT_APPROVED_DATE
        defaultCompGstNotApplicableShouldBeFound("approvedDate.equals=" + DEFAULT_APPROVED_DATE);

        // Get all the compGstNotApplicableList where approvedDate equals to UPDATED_APPROVED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("approvedDate.equals=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedDate not equals to DEFAULT_APPROVED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("approvedDate.notEquals=" + DEFAULT_APPROVED_DATE);

        // Get all the compGstNotApplicableList where approvedDate not equals to UPDATED_APPROVED_DATE
        defaultCompGstNotApplicableShouldBeFound("approvedDate.notEquals=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedDateIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedDate in DEFAULT_APPROVED_DATE or UPDATED_APPROVED_DATE
        defaultCompGstNotApplicableShouldBeFound("approvedDate.in=" + DEFAULT_APPROVED_DATE + "," + UPDATED_APPROVED_DATE);

        // Get all the compGstNotApplicableList where approvedDate equals to UPDATED_APPROVED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("approvedDate.in=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByApprovedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where approvedDate is not null
        defaultCompGstNotApplicableShouldBeFound("approvedDate.specified=true");

        // Get all the compGstNotApplicableList where approvedDate is null
        defaultCompGstNotApplicableShouldNotBeFound("approvedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByGstNaFileIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where gstNaFile equals to DEFAULT_GST_NA_FILE
        defaultCompGstNotApplicableShouldBeFound("gstNaFile.equals=" + DEFAULT_GST_NA_FILE);

        // Get all the compGstNotApplicableList where gstNaFile equals to UPDATED_GST_NA_FILE
        defaultCompGstNotApplicableShouldNotBeFound("gstNaFile.equals=" + UPDATED_GST_NA_FILE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByGstNaFileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where gstNaFile not equals to DEFAULT_GST_NA_FILE
        defaultCompGstNotApplicableShouldNotBeFound("gstNaFile.notEquals=" + DEFAULT_GST_NA_FILE);

        // Get all the compGstNotApplicableList where gstNaFile not equals to UPDATED_GST_NA_FILE
        defaultCompGstNotApplicableShouldBeFound("gstNaFile.notEquals=" + UPDATED_GST_NA_FILE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByGstNaFileIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where gstNaFile in DEFAULT_GST_NA_FILE or UPDATED_GST_NA_FILE
        defaultCompGstNotApplicableShouldBeFound("gstNaFile.in=" + DEFAULT_GST_NA_FILE + "," + UPDATED_GST_NA_FILE);

        // Get all the compGstNotApplicableList where gstNaFile equals to UPDATED_GST_NA_FILE
        defaultCompGstNotApplicableShouldNotBeFound("gstNaFile.in=" + UPDATED_GST_NA_FILE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByGstNaFileIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where gstNaFile is not null
        defaultCompGstNotApplicableShouldBeFound("gstNaFile.specified=true");

        // Get all the compGstNotApplicableList where gstNaFile is null
        defaultCompGstNotApplicableShouldNotBeFound("gstNaFile.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByGstNaFileContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where gstNaFile contains DEFAULT_GST_NA_FILE
        defaultCompGstNotApplicableShouldBeFound("gstNaFile.contains=" + DEFAULT_GST_NA_FILE);

        // Get all the compGstNotApplicableList where gstNaFile contains UPDATED_GST_NA_FILE
        defaultCompGstNotApplicableShouldNotBeFound("gstNaFile.contains=" + UPDATED_GST_NA_FILE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByGstNaFileNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where gstNaFile does not contain DEFAULT_GST_NA_FILE
        defaultCompGstNotApplicableShouldNotBeFound("gstNaFile.doesNotContain=" + DEFAULT_GST_NA_FILE);

        // Get all the compGstNotApplicableList where gstNaFile does not contain UPDATED_GST_NA_FILE
        defaultCompGstNotApplicableShouldBeFound("gstNaFile.doesNotContain=" + UPDATED_GST_NA_FILE);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsReverseRequestIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isReverseRequest equals to DEFAULT_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldBeFound("isReverseRequest.equals=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the compGstNotApplicableList where isReverseRequest equals to UPDATED_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldNotBeFound("isReverseRequest.equals=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsReverseRequestIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isReverseRequest not equals to DEFAULT_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldNotBeFound("isReverseRequest.notEquals=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the compGstNotApplicableList where isReverseRequest not equals to UPDATED_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldBeFound("isReverseRequest.notEquals=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsReverseRequestIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isReverseRequest in DEFAULT_IS_REVERSE_REQUEST or UPDATED_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldBeFound("isReverseRequest.in=" + DEFAULT_IS_REVERSE_REQUEST + "," + UPDATED_IS_REVERSE_REQUEST);

        // Get all the compGstNotApplicableList where isReverseRequest equals to UPDATED_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldNotBeFound("isReverseRequest.in=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsReverseRequestIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isReverseRequest is not null
        defaultCompGstNotApplicableShouldBeFound("isReverseRequest.specified=true");

        // Get all the compGstNotApplicableList where isReverseRequest is null
        defaultCompGstNotApplicableShouldNotBeFound("isReverseRequest.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsReverseRequestContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isReverseRequest contains DEFAULT_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldBeFound("isReverseRequest.contains=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the compGstNotApplicableList where isReverseRequest contains UPDATED_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldNotBeFound("isReverseRequest.contains=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsReverseRequestNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isReverseRequest does not contain DEFAULT_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldNotBeFound("isReverseRequest.doesNotContain=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the compGstNotApplicableList where isReverseRequest does not contain UPDATED_IS_REVERSE_REQUEST
        defaultCompGstNotApplicableShouldBeFound("isReverseRequest.doesNotContain=" + UPDATED_IS_REVERSE_REQUEST);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate is not null
        defaultCompGstNotApplicableShouldBeFound("createdDate.specified=true");

        // Get all the compGstNotApplicableList where createdDate is null
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the compGstNotApplicableList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompGstNotApplicableShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the compGstNotApplicableList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the compGstNotApplicableList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the compGstNotApplicableList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedBy is not null
        defaultCompGstNotApplicableShouldBeFound("lastModifiedBy.specified=true");

        // Get all the compGstNotApplicableList where lastModifiedBy is null
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the compGstNotApplicableList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the compGstNotApplicableList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompGstNotApplicableShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate is not null
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.specified=true");

        // Get all the compGstNotApplicableList where lastModifiedDate is null
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the compGstNotApplicableList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompGstNotApplicableShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompGstNotApplicableShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the compGstNotApplicableList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompGstNotApplicableShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompGstNotApplicableShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the compGstNotApplicableList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompGstNotApplicableShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompGstNotApplicableShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the compGstNotApplicableList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompGstNotApplicableShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isDeleted is not null
        defaultCompGstNotApplicableShouldBeFound("isDeleted.specified=true");

        // Get all the compGstNotApplicableList where isDeleted is null
        defaultCompGstNotApplicableShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompGstNotApplicableShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the compGstNotApplicableList where isDeleted contains UPDATED_IS_DELETED
        defaultCompGstNotApplicableShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);

        // Get all the compGstNotApplicableList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompGstNotApplicableShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the compGstNotApplicableList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompGstNotApplicableShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompGstNotApplicablesByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);
        ContactMaster contactId = ContactMasterResourceIT.createEntity(em);
        em.persist(contactId);
        em.flush();
        compGstNotApplicable.setContactId(contactId);
        compGstNotApplicableRepository.saveAndFlush(compGstNotApplicable);
        Long contactIdId = contactId.getId();

        // Get all the compGstNotApplicableList where contactId equals to contactIdId
        defaultCompGstNotApplicableShouldBeFound("contactIdId.equals=" + contactIdId);

        // Get all the compGstNotApplicableList where contactId equals to contactIdId + 1
        defaultCompGstNotApplicableShouldNotBeFound("contactIdId.equals=" + (contactIdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompGstNotApplicableShouldBeFound(String filter) throws Exception {
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compGstNotApplicable.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].requesterComments").value(hasItem(DEFAULT_REQUESTER_COMMENTS)))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].requestedDate").value(hasItem(DEFAULT_REQUESTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].approverComments").value(hasItem(DEFAULT_APPROVER_COMMENTS)))
            .andExpect(jsonPath("$.[*].approvedBy").value(hasItem(DEFAULT_APPROVED_BY)))
            .andExpect(jsonPath("$.[*].approvedDate").value(hasItem(DEFAULT_APPROVED_DATE.toString())))
            .andExpect(jsonPath("$.[*].gstNaFile").value(hasItem(DEFAULT_GST_NA_FILE)))
            .andExpect(jsonPath("$.[*].isReverseRequest").value(hasItem(DEFAULT_IS_REVERSE_REQUEST)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompGstNotApplicableShouldNotBeFound(String filter) throws Exception {
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompGstNotApplicable() throws Exception {
        // Get the compGstNotApplicable
        restCompGstNotApplicableMockMvc.perform(get("/api/comp-gst-not-applicables/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompGstNotApplicable() throws Exception {
        // Initialize the database
        compGstNotApplicableService.save(compGstNotApplicable);

        int databaseSizeBeforeUpdate = compGstNotApplicableRepository.findAll().size();

        // Update the compGstNotApplicable
        CompGstNotApplicable updatedCompGstNotApplicable = compGstNotApplicableRepository.findById(compGstNotApplicable.getId()).get();
        // Disconnect from session so that the updates on updatedCompGstNotApplicable are not directly saved in db
        em.detach(updatedCompGstNotApplicable);
        updatedCompGstNotApplicable
            .requestStatus(UPDATED_REQUEST_STATUS)
            .requesterComments(UPDATED_REQUESTER_COMMENTS)
            .requestedBy(UPDATED_REQUESTED_BY)
            .requestedDate(UPDATED_REQUESTED_DATE)
            .approverComments(UPDATED_APPROVER_COMMENTS)
            .approvedBy(UPDATED_APPROVED_BY)
            .approvedDate(UPDATED_APPROVED_DATE)
            .gstNaFile(UPDATED_GST_NA_FILE)
            .isReverseRequest(UPDATED_IS_REVERSE_REQUEST)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);

        restCompGstNotApplicableMockMvc.perform(put("/api/comp-gst-not-applicables")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompGstNotApplicable)))
            .andExpect(status().isOk());

        // Validate the CompGstNotApplicable in the database
        List<CompGstNotApplicable> compGstNotApplicableList = compGstNotApplicableRepository.findAll();
        assertThat(compGstNotApplicableList).hasSize(databaseSizeBeforeUpdate);
        CompGstNotApplicable testCompGstNotApplicable = compGstNotApplicableList.get(compGstNotApplicableList.size() - 1);
        assertThat(testCompGstNotApplicable.getRequestStatus()).isEqualTo(UPDATED_REQUEST_STATUS);
        assertThat(testCompGstNotApplicable.getRequesterComments()).isEqualTo(UPDATED_REQUESTER_COMMENTS);
        assertThat(testCompGstNotApplicable.getRequestedBy()).isEqualTo(UPDATED_REQUESTED_BY);
        assertThat(testCompGstNotApplicable.getRequestedDate()).isEqualTo(UPDATED_REQUESTED_DATE);
        assertThat(testCompGstNotApplicable.getApproverComments()).isEqualTo(UPDATED_APPROVER_COMMENTS);
        assertThat(testCompGstNotApplicable.getApprovedBy()).isEqualTo(UPDATED_APPROVED_BY);
        assertThat(testCompGstNotApplicable.getApprovedDate()).isEqualTo(UPDATED_APPROVED_DATE);
        assertThat(testCompGstNotApplicable.getGstNaFile()).isEqualTo(UPDATED_GST_NA_FILE);
        assertThat(testCompGstNotApplicable.getIsReverseRequest()).isEqualTo(UPDATED_IS_REVERSE_REQUEST);
        assertThat(testCompGstNotApplicable.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompGstNotApplicable.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompGstNotApplicable.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompGstNotApplicable.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingCompGstNotApplicable() throws Exception {
        int databaseSizeBeforeUpdate = compGstNotApplicableRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompGstNotApplicableMockMvc.perform(put("/api/comp-gst-not-applicables")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compGstNotApplicable)))
            .andExpect(status().isBadRequest());

        // Validate the CompGstNotApplicable in the database
        List<CompGstNotApplicable> compGstNotApplicableList = compGstNotApplicableRepository.findAll();
        assertThat(compGstNotApplicableList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompGstNotApplicable() throws Exception {
        // Initialize the database
        compGstNotApplicableService.save(compGstNotApplicable);

        int databaseSizeBeforeDelete = compGstNotApplicableRepository.findAll().size();

        // Delete the compGstNotApplicable
        restCompGstNotApplicableMockMvc.perform(delete("/api/comp-gst-not-applicables/{id}", compGstNotApplicable.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompGstNotApplicable> compGstNotApplicableList = compGstNotApplicableRepository.findAll();
        assertThat(compGstNotApplicableList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
