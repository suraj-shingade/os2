package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyAddressMaster;
import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.domain.CompanyRequest;
import com.crisil.onesource.repository.CompanyAddressMasterRepository;
import com.crisil.onesource.service.CompanyAddressMasterService;
import com.crisil.onesource.service.dto.CompanyAddressMasterCriteria;
import com.crisil.onesource.service.CompanyAddressMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyAddressMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyAddressMasterResourceIT {

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    private static final Integer DEFAULT_ADDRESS_TYPE_ID = 1;
    private static final Integer UPDATED_ADDRESS_TYPE_ID = 2;
    private static final Integer SMALLER_ADDRESS_TYPE_ID = 1 - 1;

    private static final String DEFAULT_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_3 = "BBBBBBBBBB";

    private static final Integer DEFAULT_CITY_ID = 1;
    private static final Integer UPDATED_CITY_ID = 2;
    private static final Integer SMALLER_CITY_ID = 1 - 1;

    private static final String DEFAULT_PHONE_NO = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NO = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_FAX = "AAAAAAAAAA";
    private static final String UPDATED_FAX = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_WEBPAGE = "AAAAAAAAAA";
    private static final String UPDATED_WEBPAGE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final Integer DEFAULT_COMPANY_ADDRESS_ID = 1;
    private static final Integer UPDATED_COMPANY_ADDRESS_ID = 2;
    private static final Integer SMALLER_COMPANY_ADDRESS_ID = 1 - 1;

    private static final String DEFAULT_ADDRESS_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_SOURCE = "BBBBBBBBBB";

    @Autowired
    private CompanyAddressMasterRepository companyAddressMasterRepository;

    @Autowired
    private CompanyAddressMasterService companyAddressMasterService;

    @Autowired
    private CompanyAddressMasterQueryService companyAddressMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyAddressMasterMockMvc;

    private CompanyAddressMaster companyAddressMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyAddressMaster createEntity(EntityManager em) {
        CompanyAddressMaster companyAddressMaster = new CompanyAddressMaster()
            .companyCode(DEFAULT_COMPANY_CODE)
            .addressTypeId(DEFAULT_ADDRESS_TYPE_ID)
            .address1(DEFAULT_ADDRESS_1)
            .address2(DEFAULT_ADDRESS_2)
            .address3(DEFAULT_ADDRESS_3)
            .cityId(DEFAULT_CITY_ID)
            .phoneNo(DEFAULT_PHONE_NO)
            .mobileNo(DEFAULT_MOBILE_NO)
            .fax(DEFAULT_FAX)
            .email(DEFAULT_EMAIL)
            .webpage(DEFAULT_WEBPAGE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED)
            .companyAddressId(DEFAULT_COMPANY_ADDRESS_ID)
            .addressSource(DEFAULT_ADDRESS_SOURCE);
        return companyAddressMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyAddressMaster createUpdatedEntity(EntityManager em) {
        CompanyAddressMaster companyAddressMaster = new CompanyAddressMaster()
            .companyCode(UPDATED_COMPANY_CODE)
            .addressTypeId(UPDATED_ADDRESS_TYPE_ID)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .cityId(UPDATED_CITY_ID)
            .phoneNo(UPDATED_PHONE_NO)
            .mobileNo(UPDATED_MOBILE_NO)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .webpage(UPDATED_WEBPAGE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED)
            .companyAddressId(UPDATED_COMPANY_ADDRESS_ID)
            .addressSource(UPDATED_ADDRESS_SOURCE);
        return companyAddressMaster;
    }

    @BeforeEach
    public void initTest() {
        companyAddressMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyAddressMaster() throws Exception {
        int databaseSizeBeforeCreate = companyAddressMasterRepository.findAll().size();
        // Create the CompanyAddressMaster
        restCompanyAddressMasterMockMvc.perform(post("/api/company-address-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyAddressMaster)))
            .andExpect(status().isCreated());

        // Validate the CompanyAddressMaster in the database
        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyAddressMaster testCompanyAddressMaster = companyAddressMasterList.get(companyAddressMasterList.size() - 1);
        assertThat(testCompanyAddressMaster.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
        assertThat(testCompanyAddressMaster.getAddressTypeId()).isEqualTo(DEFAULT_ADDRESS_TYPE_ID);
        assertThat(testCompanyAddressMaster.getAddress1()).isEqualTo(DEFAULT_ADDRESS_1);
        assertThat(testCompanyAddressMaster.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testCompanyAddressMaster.getAddress3()).isEqualTo(DEFAULT_ADDRESS_3);
        assertThat(testCompanyAddressMaster.getCityId()).isEqualTo(DEFAULT_CITY_ID);
        assertThat(testCompanyAddressMaster.getPhoneNo()).isEqualTo(DEFAULT_PHONE_NO);
        assertThat(testCompanyAddressMaster.getMobileNo()).isEqualTo(DEFAULT_MOBILE_NO);
        assertThat(testCompanyAddressMaster.getFax()).isEqualTo(DEFAULT_FAX);
        assertThat(testCompanyAddressMaster.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCompanyAddressMaster.getWebpage()).isEqualTo(DEFAULT_WEBPAGE);
        assertThat(testCompanyAddressMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyAddressMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyAddressMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanyAddressMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyAddressMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testCompanyAddressMaster.getCompanyAddressId()).isEqualTo(DEFAULT_COMPANY_ADDRESS_ID);
        assertThat(testCompanyAddressMaster.getAddressSource()).isEqualTo(DEFAULT_ADDRESS_SOURCE);
    }

    @Test
    @Transactional
    public void createCompanyAddressMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyAddressMasterRepository.findAll().size();

        // Create the CompanyAddressMaster with an existing ID
        companyAddressMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyAddressMasterMockMvc.perform(post("/api/company-address-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyAddressMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyAddressMaster in the database
        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAddressTypeIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyAddressMasterRepository.findAll().size();
        // set the field null
        companyAddressMaster.setAddressTypeId(null);

        // Create the CompanyAddressMaster, which fails.


        restCompanyAddressMasterMockMvc.perform(post("/api/company-address-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyAddressMaster)))
            .andExpect(status().isBadRequest());

        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCompanyAddressIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyAddressMasterRepository.findAll().size();
        // set the field null
        companyAddressMaster.setCompanyAddressId(null);

        // Create the CompanyAddressMaster, which fails.


        restCompanyAddressMasterMockMvc.perform(post("/api/company-address-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyAddressMaster)))
            .andExpect(status().isBadRequest());

        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMasters() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyAddressMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].addressTypeId").value(hasItem(DEFAULT_ADDRESS_TYPE_ID)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].phoneNo").value(hasItem(DEFAULT_PHONE_NO)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].webpage").value(hasItem(DEFAULT_WEBPAGE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].companyAddressId").value(hasItem(DEFAULT_COMPANY_ADDRESS_ID)))
            .andExpect(jsonPath("$.[*].addressSource").value(hasItem(DEFAULT_ADDRESS_SOURCE)));
    }
    
    @Test
    @Transactional
    public void getCompanyAddressMaster() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get the companyAddressMaster
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters/{id}", companyAddressMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyAddressMaster.getId().intValue()))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE))
            .andExpect(jsonPath("$.addressTypeId").value(DEFAULT_ADDRESS_TYPE_ID))
            .andExpect(jsonPath("$.address1").value(DEFAULT_ADDRESS_1))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2))
            .andExpect(jsonPath("$.address3").value(DEFAULT_ADDRESS_3))
            .andExpect(jsonPath("$.cityId").value(DEFAULT_CITY_ID))
            .andExpect(jsonPath("$.phoneNo").value(DEFAULT_PHONE_NO))
            .andExpect(jsonPath("$.mobileNo").value(DEFAULT_MOBILE_NO))
            .andExpect(jsonPath("$.fax").value(DEFAULT_FAX))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.webpage").value(DEFAULT_WEBPAGE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.companyAddressId").value(DEFAULT_COMPANY_ADDRESS_ID))
            .andExpect(jsonPath("$.addressSource").value(DEFAULT_ADDRESS_SOURCE));
    }


    @Test
    @Transactional
    public void getCompanyAddressMastersByIdFiltering() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        Long id = companyAddressMaster.getId();

        defaultCompanyAddressMasterShouldBeFound("id.equals=" + id);
        defaultCompanyAddressMasterShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyAddressMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyAddressMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyAddressMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyAddressMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultCompanyAddressMasterShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the companyAddressMasterList where companyCode equals to UPDATED_COMPANY_CODE
        defaultCompanyAddressMasterShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultCompanyAddressMasterShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the companyAddressMasterList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultCompanyAddressMasterShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultCompanyAddressMasterShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the companyAddressMasterList where companyCode equals to UPDATED_COMPANY_CODE
        defaultCompanyAddressMasterShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyCode is not null
        defaultCompanyAddressMasterShouldBeFound("companyCode.specified=true");

        // Get all the companyAddressMasterList where companyCode is null
        defaultCompanyAddressMasterShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyCode contains DEFAULT_COMPANY_CODE
        defaultCompanyAddressMasterShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the companyAddressMasterList where companyCode contains UPDATED_COMPANY_CODE
        defaultCompanyAddressMasterShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultCompanyAddressMasterShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the companyAddressMasterList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultCompanyAddressMasterShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId equals to DEFAULT_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.equals=" + DEFAULT_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId equals to UPDATED_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.equals=" + UPDATED_ADDRESS_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId not equals to DEFAULT_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.notEquals=" + DEFAULT_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId not equals to UPDATED_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.notEquals=" + UPDATED_ADDRESS_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId in DEFAULT_ADDRESS_TYPE_ID or UPDATED_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.in=" + DEFAULT_ADDRESS_TYPE_ID + "," + UPDATED_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId equals to UPDATED_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.in=" + UPDATED_ADDRESS_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId is not null
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.specified=true");

        // Get all the companyAddressMasterList where addressTypeId is null
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId is greater than or equal to DEFAULT_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.greaterThanOrEqual=" + DEFAULT_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId is greater than or equal to UPDATED_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.greaterThanOrEqual=" + UPDATED_ADDRESS_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId is less than or equal to DEFAULT_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.lessThanOrEqual=" + DEFAULT_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId is less than or equal to SMALLER_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.lessThanOrEqual=" + SMALLER_ADDRESS_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId is less than DEFAULT_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.lessThan=" + DEFAULT_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId is less than UPDATED_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.lessThan=" + UPDATED_ADDRESS_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressTypeIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressTypeId is greater than DEFAULT_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldNotBeFound("addressTypeId.greaterThan=" + DEFAULT_ADDRESS_TYPE_ID);

        // Get all the companyAddressMasterList where addressTypeId is greater than SMALLER_ADDRESS_TYPE_ID
        defaultCompanyAddressMasterShouldBeFound("addressTypeId.greaterThan=" + SMALLER_ADDRESS_TYPE_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress1IsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address1 equals to DEFAULT_ADDRESS_1
        defaultCompanyAddressMasterShouldBeFound("address1.equals=" + DEFAULT_ADDRESS_1);

        // Get all the companyAddressMasterList where address1 equals to UPDATED_ADDRESS_1
        defaultCompanyAddressMasterShouldNotBeFound("address1.equals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address1 not equals to DEFAULT_ADDRESS_1
        defaultCompanyAddressMasterShouldNotBeFound("address1.notEquals=" + DEFAULT_ADDRESS_1);

        // Get all the companyAddressMasterList where address1 not equals to UPDATED_ADDRESS_1
        defaultCompanyAddressMasterShouldBeFound("address1.notEquals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress1IsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address1 in DEFAULT_ADDRESS_1 or UPDATED_ADDRESS_1
        defaultCompanyAddressMasterShouldBeFound("address1.in=" + DEFAULT_ADDRESS_1 + "," + UPDATED_ADDRESS_1);

        // Get all the companyAddressMasterList where address1 equals to UPDATED_ADDRESS_1
        defaultCompanyAddressMasterShouldNotBeFound("address1.in=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress1IsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address1 is not null
        defaultCompanyAddressMasterShouldBeFound("address1.specified=true");

        // Get all the companyAddressMasterList where address1 is null
        defaultCompanyAddressMasterShouldNotBeFound("address1.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress1ContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address1 contains DEFAULT_ADDRESS_1
        defaultCompanyAddressMasterShouldBeFound("address1.contains=" + DEFAULT_ADDRESS_1);

        // Get all the companyAddressMasterList where address1 contains UPDATED_ADDRESS_1
        defaultCompanyAddressMasterShouldNotBeFound("address1.contains=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress1NotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address1 does not contain DEFAULT_ADDRESS_1
        defaultCompanyAddressMasterShouldNotBeFound("address1.doesNotContain=" + DEFAULT_ADDRESS_1);

        // Get all the companyAddressMasterList where address1 does not contain UPDATED_ADDRESS_1
        defaultCompanyAddressMasterShouldBeFound("address1.doesNotContain=" + UPDATED_ADDRESS_1);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress2IsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address2 equals to DEFAULT_ADDRESS_2
        defaultCompanyAddressMasterShouldBeFound("address2.equals=" + DEFAULT_ADDRESS_2);

        // Get all the companyAddressMasterList where address2 equals to UPDATED_ADDRESS_2
        defaultCompanyAddressMasterShouldNotBeFound("address2.equals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address2 not equals to DEFAULT_ADDRESS_2
        defaultCompanyAddressMasterShouldNotBeFound("address2.notEquals=" + DEFAULT_ADDRESS_2);

        // Get all the companyAddressMasterList where address2 not equals to UPDATED_ADDRESS_2
        defaultCompanyAddressMasterShouldBeFound("address2.notEquals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress2IsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address2 in DEFAULT_ADDRESS_2 or UPDATED_ADDRESS_2
        defaultCompanyAddressMasterShouldBeFound("address2.in=" + DEFAULT_ADDRESS_2 + "," + UPDATED_ADDRESS_2);

        // Get all the companyAddressMasterList where address2 equals to UPDATED_ADDRESS_2
        defaultCompanyAddressMasterShouldNotBeFound("address2.in=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress2IsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address2 is not null
        defaultCompanyAddressMasterShouldBeFound("address2.specified=true");

        // Get all the companyAddressMasterList where address2 is null
        defaultCompanyAddressMasterShouldNotBeFound("address2.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress2ContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address2 contains DEFAULT_ADDRESS_2
        defaultCompanyAddressMasterShouldBeFound("address2.contains=" + DEFAULT_ADDRESS_2);

        // Get all the companyAddressMasterList where address2 contains UPDATED_ADDRESS_2
        defaultCompanyAddressMasterShouldNotBeFound("address2.contains=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress2NotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address2 does not contain DEFAULT_ADDRESS_2
        defaultCompanyAddressMasterShouldNotBeFound("address2.doesNotContain=" + DEFAULT_ADDRESS_2);

        // Get all the companyAddressMasterList where address2 does not contain UPDATED_ADDRESS_2
        defaultCompanyAddressMasterShouldBeFound("address2.doesNotContain=" + UPDATED_ADDRESS_2);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress3IsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address3 equals to DEFAULT_ADDRESS_3
        defaultCompanyAddressMasterShouldBeFound("address3.equals=" + DEFAULT_ADDRESS_3);

        // Get all the companyAddressMasterList where address3 equals to UPDATED_ADDRESS_3
        defaultCompanyAddressMasterShouldNotBeFound("address3.equals=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address3 not equals to DEFAULT_ADDRESS_3
        defaultCompanyAddressMasterShouldNotBeFound("address3.notEquals=" + DEFAULT_ADDRESS_3);

        // Get all the companyAddressMasterList where address3 not equals to UPDATED_ADDRESS_3
        defaultCompanyAddressMasterShouldBeFound("address3.notEquals=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress3IsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address3 in DEFAULT_ADDRESS_3 or UPDATED_ADDRESS_3
        defaultCompanyAddressMasterShouldBeFound("address3.in=" + DEFAULT_ADDRESS_3 + "," + UPDATED_ADDRESS_3);

        // Get all the companyAddressMasterList where address3 equals to UPDATED_ADDRESS_3
        defaultCompanyAddressMasterShouldNotBeFound("address3.in=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress3IsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address3 is not null
        defaultCompanyAddressMasterShouldBeFound("address3.specified=true");

        // Get all the companyAddressMasterList where address3 is null
        defaultCompanyAddressMasterShouldNotBeFound("address3.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress3ContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address3 contains DEFAULT_ADDRESS_3
        defaultCompanyAddressMasterShouldBeFound("address3.contains=" + DEFAULT_ADDRESS_3);

        // Get all the companyAddressMasterList where address3 contains UPDATED_ADDRESS_3
        defaultCompanyAddressMasterShouldNotBeFound("address3.contains=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddress3NotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where address3 does not contain DEFAULT_ADDRESS_3
        defaultCompanyAddressMasterShouldNotBeFound("address3.doesNotContain=" + DEFAULT_ADDRESS_3);

        // Get all the companyAddressMasterList where address3 does not contain UPDATED_ADDRESS_3
        defaultCompanyAddressMasterShouldBeFound("address3.doesNotContain=" + UPDATED_ADDRESS_3);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId equals to DEFAULT_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.equals=" + DEFAULT_CITY_ID);

        // Get all the companyAddressMasterList where cityId equals to UPDATED_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.equals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId not equals to DEFAULT_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.notEquals=" + DEFAULT_CITY_ID);

        // Get all the companyAddressMasterList where cityId not equals to UPDATED_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.notEquals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId in DEFAULT_CITY_ID or UPDATED_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.in=" + DEFAULT_CITY_ID + "," + UPDATED_CITY_ID);

        // Get all the companyAddressMasterList where cityId equals to UPDATED_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.in=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId is not null
        defaultCompanyAddressMasterShouldBeFound("cityId.specified=true");

        // Get all the companyAddressMasterList where cityId is null
        defaultCompanyAddressMasterShouldNotBeFound("cityId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId is greater than or equal to DEFAULT_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.greaterThanOrEqual=" + DEFAULT_CITY_ID);

        // Get all the companyAddressMasterList where cityId is greater than or equal to UPDATED_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.greaterThanOrEqual=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId is less than or equal to DEFAULT_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.lessThanOrEqual=" + DEFAULT_CITY_ID);

        // Get all the companyAddressMasterList where cityId is less than or equal to SMALLER_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.lessThanOrEqual=" + SMALLER_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId is less than DEFAULT_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.lessThan=" + DEFAULT_CITY_ID);

        // Get all the companyAddressMasterList where cityId is less than UPDATED_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.lessThan=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCityIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where cityId is greater than DEFAULT_CITY_ID
        defaultCompanyAddressMasterShouldNotBeFound("cityId.greaterThan=" + DEFAULT_CITY_ID);

        // Get all the companyAddressMasterList where cityId is greater than SMALLER_CITY_ID
        defaultCompanyAddressMasterShouldBeFound("cityId.greaterThan=" + SMALLER_CITY_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByPhoneNoIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where phoneNo equals to DEFAULT_PHONE_NO
        defaultCompanyAddressMasterShouldBeFound("phoneNo.equals=" + DEFAULT_PHONE_NO);

        // Get all the companyAddressMasterList where phoneNo equals to UPDATED_PHONE_NO
        defaultCompanyAddressMasterShouldNotBeFound("phoneNo.equals=" + UPDATED_PHONE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByPhoneNoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where phoneNo not equals to DEFAULT_PHONE_NO
        defaultCompanyAddressMasterShouldNotBeFound("phoneNo.notEquals=" + DEFAULT_PHONE_NO);

        // Get all the companyAddressMasterList where phoneNo not equals to UPDATED_PHONE_NO
        defaultCompanyAddressMasterShouldBeFound("phoneNo.notEquals=" + UPDATED_PHONE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByPhoneNoIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where phoneNo in DEFAULT_PHONE_NO or UPDATED_PHONE_NO
        defaultCompanyAddressMasterShouldBeFound("phoneNo.in=" + DEFAULT_PHONE_NO + "," + UPDATED_PHONE_NO);

        // Get all the companyAddressMasterList where phoneNo equals to UPDATED_PHONE_NO
        defaultCompanyAddressMasterShouldNotBeFound("phoneNo.in=" + UPDATED_PHONE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByPhoneNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where phoneNo is not null
        defaultCompanyAddressMasterShouldBeFound("phoneNo.specified=true");

        // Get all the companyAddressMasterList where phoneNo is null
        defaultCompanyAddressMasterShouldNotBeFound("phoneNo.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByPhoneNoContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where phoneNo contains DEFAULT_PHONE_NO
        defaultCompanyAddressMasterShouldBeFound("phoneNo.contains=" + DEFAULT_PHONE_NO);

        // Get all the companyAddressMasterList where phoneNo contains UPDATED_PHONE_NO
        defaultCompanyAddressMasterShouldNotBeFound("phoneNo.contains=" + UPDATED_PHONE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByPhoneNoNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where phoneNo does not contain DEFAULT_PHONE_NO
        defaultCompanyAddressMasterShouldNotBeFound("phoneNo.doesNotContain=" + DEFAULT_PHONE_NO);

        // Get all the companyAddressMasterList where phoneNo does not contain UPDATED_PHONE_NO
        defaultCompanyAddressMasterShouldBeFound("phoneNo.doesNotContain=" + UPDATED_PHONE_NO);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByMobileNoIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where mobileNo equals to DEFAULT_MOBILE_NO
        defaultCompanyAddressMasterShouldBeFound("mobileNo.equals=" + DEFAULT_MOBILE_NO);

        // Get all the companyAddressMasterList where mobileNo equals to UPDATED_MOBILE_NO
        defaultCompanyAddressMasterShouldNotBeFound("mobileNo.equals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByMobileNoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where mobileNo not equals to DEFAULT_MOBILE_NO
        defaultCompanyAddressMasterShouldNotBeFound("mobileNo.notEquals=" + DEFAULT_MOBILE_NO);

        // Get all the companyAddressMasterList where mobileNo not equals to UPDATED_MOBILE_NO
        defaultCompanyAddressMasterShouldBeFound("mobileNo.notEquals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByMobileNoIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where mobileNo in DEFAULT_MOBILE_NO or UPDATED_MOBILE_NO
        defaultCompanyAddressMasterShouldBeFound("mobileNo.in=" + DEFAULT_MOBILE_NO + "," + UPDATED_MOBILE_NO);

        // Get all the companyAddressMasterList where mobileNo equals to UPDATED_MOBILE_NO
        defaultCompanyAddressMasterShouldNotBeFound("mobileNo.in=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByMobileNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where mobileNo is not null
        defaultCompanyAddressMasterShouldBeFound("mobileNo.specified=true");

        // Get all the companyAddressMasterList where mobileNo is null
        defaultCompanyAddressMasterShouldNotBeFound("mobileNo.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByMobileNoContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where mobileNo contains DEFAULT_MOBILE_NO
        defaultCompanyAddressMasterShouldBeFound("mobileNo.contains=" + DEFAULT_MOBILE_NO);

        // Get all the companyAddressMasterList where mobileNo contains UPDATED_MOBILE_NO
        defaultCompanyAddressMasterShouldNotBeFound("mobileNo.contains=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByMobileNoNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where mobileNo does not contain DEFAULT_MOBILE_NO
        defaultCompanyAddressMasterShouldNotBeFound("mobileNo.doesNotContain=" + DEFAULT_MOBILE_NO);

        // Get all the companyAddressMasterList where mobileNo does not contain UPDATED_MOBILE_NO
        defaultCompanyAddressMasterShouldBeFound("mobileNo.doesNotContain=" + UPDATED_MOBILE_NO);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByFaxIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where fax equals to DEFAULT_FAX
        defaultCompanyAddressMasterShouldBeFound("fax.equals=" + DEFAULT_FAX);

        // Get all the companyAddressMasterList where fax equals to UPDATED_FAX
        defaultCompanyAddressMasterShouldNotBeFound("fax.equals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByFaxIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where fax not equals to DEFAULT_FAX
        defaultCompanyAddressMasterShouldNotBeFound("fax.notEquals=" + DEFAULT_FAX);

        // Get all the companyAddressMasterList where fax not equals to UPDATED_FAX
        defaultCompanyAddressMasterShouldBeFound("fax.notEquals=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByFaxIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where fax in DEFAULT_FAX or UPDATED_FAX
        defaultCompanyAddressMasterShouldBeFound("fax.in=" + DEFAULT_FAX + "," + UPDATED_FAX);

        // Get all the companyAddressMasterList where fax equals to UPDATED_FAX
        defaultCompanyAddressMasterShouldNotBeFound("fax.in=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByFaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where fax is not null
        defaultCompanyAddressMasterShouldBeFound("fax.specified=true");

        // Get all the companyAddressMasterList where fax is null
        defaultCompanyAddressMasterShouldNotBeFound("fax.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByFaxContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where fax contains DEFAULT_FAX
        defaultCompanyAddressMasterShouldBeFound("fax.contains=" + DEFAULT_FAX);

        // Get all the companyAddressMasterList where fax contains UPDATED_FAX
        defaultCompanyAddressMasterShouldNotBeFound("fax.contains=" + UPDATED_FAX);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByFaxNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where fax does not contain DEFAULT_FAX
        defaultCompanyAddressMasterShouldNotBeFound("fax.doesNotContain=" + DEFAULT_FAX);

        // Get all the companyAddressMasterList where fax does not contain UPDATED_FAX
        defaultCompanyAddressMasterShouldBeFound("fax.doesNotContain=" + UPDATED_FAX);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where email equals to DEFAULT_EMAIL
        defaultCompanyAddressMasterShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the companyAddressMasterList where email equals to UPDATED_EMAIL
        defaultCompanyAddressMasterShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where email not equals to DEFAULT_EMAIL
        defaultCompanyAddressMasterShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the companyAddressMasterList where email not equals to UPDATED_EMAIL
        defaultCompanyAddressMasterShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultCompanyAddressMasterShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the companyAddressMasterList where email equals to UPDATED_EMAIL
        defaultCompanyAddressMasterShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where email is not null
        defaultCompanyAddressMasterShouldBeFound("email.specified=true");

        // Get all the companyAddressMasterList where email is null
        defaultCompanyAddressMasterShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByEmailContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where email contains DEFAULT_EMAIL
        defaultCompanyAddressMasterShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the companyAddressMasterList where email contains UPDATED_EMAIL
        defaultCompanyAddressMasterShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where email does not contain DEFAULT_EMAIL
        defaultCompanyAddressMasterShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the companyAddressMasterList where email does not contain UPDATED_EMAIL
        defaultCompanyAddressMasterShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByWebpageIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where webpage equals to DEFAULT_WEBPAGE
        defaultCompanyAddressMasterShouldBeFound("webpage.equals=" + DEFAULT_WEBPAGE);

        // Get all the companyAddressMasterList where webpage equals to UPDATED_WEBPAGE
        defaultCompanyAddressMasterShouldNotBeFound("webpage.equals=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByWebpageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where webpage not equals to DEFAULT_WEBPAGE
        defaultCompanyAddressMasterShouldNotBeFound("webpage.notEquals=" + DEFAULT_WEBPAGE);

        // Get all the companyAddressMasterList where webpage not equals to UPDATED_WEBPAGE
        defaultCompanyAddressMasterShouldBeFound("webpage.notEquals=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByWebpageIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where webpage in DEFAULT_WEBPAGE or UPDATED_WEBPAGE
        defaultCompanyAddressMasterShouldBeFound("webpage.in=" + DEFAULT_WEBPAGE + "," + UPDATED_WEBPAGE);

        // Get all the companyAddressMasterList where webpage equals to UPDATED_WEBPAGE
        defaultCompanyAddressMasterShouldNotBeFound("webpage.in=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByWebpageIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where webpage is not null
        defaultCompanyAddressMasterShouldBeFound("webpage.specified=true");

        // Get all the companyAddressMasterList where webpage is null
        defaultCompanyAddressMasterShouldNotBeFound("webpage.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByWebpageContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where webpage contains DEFAULT_WEBPAGE
        defaultCompanyAddressMasterShouldBeFound("webpage.contains=" + DEFAULT_WEBPAGE);

        // Get all the companyAddressMasterList where webpage contains UPDATED_WEBPAGE
        defaultCompanyAddressMasterShouldNotBeFound("webpage.contains=" + UPDATED_WEBPAGE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByWebpageNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where webpage does not contain DEFAULT_WEBPAGE
        defaultCompanyAddressMasterShouldNotBeFound("webpage.doesNotContain=" + DEFAULT_WEBPAGE);

        // Get all the companyAddressMasterList where webpage does not contain UPDATED_WEBPAGE
        defaultCompanyAddressMasterShouldBeFound("webpage.doesNotContain=" + UPDATED_WEBPAGE);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyAddressMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyAddressMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyAddressMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyAddressMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyAddressMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyAddressMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyAddressMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyAddressMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyAddressMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdBy is not null
        defaultCompanyAddressMasterShouldBeFound("createdBy.specified=true");

        // Get all the companyAddressMasterList where createdBy is null
        defaultCompanyAddressMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyAddressMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyAddressMasterList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyAddressMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyAddressMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyAddressMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyAddressMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate is not null
        defaultCompanyAddressMasterShouldBeFound("createdDate.specified=true");

        // Get all the companyAddressMasterList where createdDate is null
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyAddressMasterList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompanyAddressMasterShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyAddressMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyAddressMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyAddressMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedBy is not null
        defaultCompanyAddressMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyAddressMasterList where lastModifiedBy is null
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyAddressMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyAddressMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyAddressMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate is not null
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyAddressMasterList where lastModifiedDate is null
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyAddressMasterList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompanyAddressMasterShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyAddressMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyAddressMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyAddressMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyAddressMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyAddressMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyAddressMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyAddressMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyAddressMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyAddressMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where isDeleted is not null
        defaultCompanyAddressMasterShouldBeFound("isDeleted.specified=true");

        // Get all the companyAddressMasterList where isDeleted is null
        defaultCompanyAddressMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyAddressMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyAddressMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyAddressMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyAddressMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyAddressMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyAddressMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId equals to DEFAULT_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.equals=" + DEFAULT_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId equals to UPDATED_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.equals=" + UPDATED_COMPANY_ADDRESS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId not equals to DEFAULT_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.notEquals=" + DEFAULT_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId not equals to UPDATED_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.notEquals=" + UPDATED_COMPANY_ADDRESS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId in DEFAULT_COMPANY_ADDRESS_ID or UPDATED_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.in=" + DEFAULT_COMPANY_ADDRESS_ID + "," + UPDATED_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId equals to UPDATED_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.in=" + UPDATED_COMPANY_ADDRESS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId is not null
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.specified=true");

        // Get all the companyAddressMasterList where companyAddressId is null
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId is greater than or equal to DEFAULT_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.greaterThanOrEqual=" + DEFAULT_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId is greater than or equal to UPDATED_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.greaterThanOrEqual=" + UPDATED_COMPANY_ADDRESS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId is less than or equal to DEFAULT_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.lessThanOrEqual=" + DEFAULT_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId is less than or equal to SMALLER_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.lessThanOrEqual=" + SMALLER_COMPANY_ADDRESS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId is less than DEFAULT_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.lessThan=" + DEFAULT_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId is less than UPDATED_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.lessThan=" + UPDATED_COMPANY_ADDRESS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByCompanyAddressIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where companyAddressId is greater than DEFAULT_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldNotBeFound("companyAddressId.greaterThan=" + DEFAULT_COMPANY_ADDRESS_ID);

        // Get all the companyAddressMasterList where companyAddressId is greater than SMALLER_COMPANY_ADDRESS_ID
        defaultCompanyAddressMasterShouldBeFound("companyAddressId.greaterThan=" + SMALLER_COMPANY_ADDRESS_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressSourceIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressSource equals to DEFAULT_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldBeFound("addressSource.equals=" + DEFAULT_ADDRESS_SOURCE);

        // Get all the companyAddressMasterList where addressSource equals to UPDATED_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldNotBeFound("addressSource.equals=" + UPDATED_ADDRESS_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressSourceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressSource not equals to DEFAULT_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldNotBeFound("addressSource.notEquals=" + DEFAULT_ADDRESS_SOURCE);

        // Get all the companyAddressMasterList where addressSource not equals to UPDATED_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldBeFound("addressSource.notEquals=" + UPDATED_ADDRESS_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressSourceIsInShouldWork() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressSource in DEFAULT_ADDRESS_SOURCE or UPDATED_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldBeFound("addressSource.in=" + DEFAULT_ADDRESS_SOURCE + "," + UPDATED_ADDRESS_SOURCE);

        // Get all the companyAddressMasterList where addressSource equals to UPDATED_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldNotBeFound("addressSource.in=" + UPDATED_ADDRESS_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressSourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressSource is not null
        defaultCompanyAddressMasterShouldBeFound("addressSource.specified=true");

        // Get all the companyAddressMasterList where addressSource is null
        defaultCompanyAddressMasterShouldNotBeFound("addressSource.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressSourceContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressSource contains DEFAULT_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldBeFound("addressSource.contains=" + DEFAULT_ADDRESS_SOURCE);

        // Get all the companyAddressMasterList where addressSource contains UPDATED_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldNotBeFound("addressSource.contains=" + UPDATED_ADDRESS_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyAddressMastersByAddressSourceNotContainsSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);

        // Get all the companyAddressMasterList where addressSource does not contain DEFAULT_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldNotBeFound("addressSource.doesNotContain=" + DEFAULT_ADDRESS_SOURCE);

        // Get all the companyAddressMasterList where addressSource does not contain UPDATED_ADDRESS_SOURCE
        defaultCompanyAddressMasterShouldBeFound("addressSource.doesNotContain=" + UPDATED_ADDRESS_SOURCE);
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByPincodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);
        LocPincodeMaster pincode = LocPincodeMasterResourceIT.createEntity(em);
        em.persist(pincode);
        em.flush();
        companyAddressMaster.setPincode(pincode);
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);
        Long pincodeId = pincode.getId();

        // Get all the companyAddressMasterList where pincode equals to pincodeId
        defaultCompanyAddressMasterShouldBeFound("pincodeId.equals=" + pincodeId);

        // Get all the companyAddressMasterList where pincode equals to pincodeId + 1
        defaultCompanyAddressMasterShouldNotBeFound("pincodeId.equals=" + (pincodeId + 1));
    }


    @Test
    @Transactional
    public void getAllCompanyAddressMastersByRecordIsEqualToSomething() throws Exception {
        // Initialize the database
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);
        CompanyRequest record = CompanyRequestResourceIT.createEntity(em);
        em.persist(record);
        em.flush();
        companyAddressMaster.setRecord(record);
        companyAddressMasterRepository.saveAndFlush(companyAddressMaster);
        Long recordId = record.getId();

        // Get all the companyAddressMasterList where record equals to recordId
        defaultCompanyAddressMasterShouldBeFound("recordId.equals=" + recordId);

        // Get all the companyAddressMasterList where record equals to recordId + 1
        defaultCompanyAddressMasterShouldNotBeFound("recordId.equals=" + (recordId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyAddressMasterShouldBeFound(String filter) throws Exception {
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyAddressMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].addressTypeId").value(hasItem(DEFAULT_ADDRESS_TYPE_ID)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].phoneNo").value(hasItem(DEFAULT_PHONE_NO)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].fax").value(hasItem(DEFAULT_FAX)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].webpage").value(hasItem(DEFAULT_WEBPAGE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].companyAddressId").value(hasItem(DEFAULT_COMPANY_ADDRESS_ID)))
            .andExpect(jsonPath("$.[*].addressSource").value(hasItem(DEFAULT_ADDRESS_SOURCE)));

        // Check, that the count call also returns 1
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyAddressMasterShouldNotBeFound(String filter) throws Exception {
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyAddressMaster() throws Exception {
        // Get the companyAddressMaster
        restCompanyAddressMasterMockMvc.perform(get("/api/company-address-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyAddressMaster() throws Exception {
        // Initialize the database
        companyAddressMasterService.save(companyAddressMaster);

        int databaseSizeBeforeUpdate = companyAddressMasterRepository.findAll().size();

        // Update the companyAddressMaster
        CompanyAddressMaster updatedCompanyAddressMaster = companyAddressMasterRepository.findById(companyAddressMaster.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyAddressMaster are not directly saved in db
        em.detach(updatedCompanyAddressMaster);
        updatedCompanyAddressMaster
            .companyCode(UPDATED_COMPANY_CODE)
            .addressTypeId(UPDATED_ADDRESS_TYPE_ID)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .cityId(UPDATED_CITY_ID)
            .phoneNo(UPDATED_PHONE_NO)
            .mobileNo(UPDATED_MOBILE_NO)
            .fax(UPDATED_FAX)
            .email(UPDATED_EMAIL)
            .webpage(UPDATED_WEBPAGE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED)
            .companyAddressId(UPDATED_COMPANY_ADDRESS_ID)
            .addressSource(UPDATED_ADDRESS_SOURCE);

        restCompanyAddressMasterMockMvc.perform(put("/api/company-address-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyAddressMaster)))
            .andExpect(status().isOk());

        // Validate the CompanyAddressMaster in the database
        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeUpdate);
        CompanyAddressMaster testCompanyAddressMaster = companyAddressMasterList.get(companyAddressMasterList.size() - 1);
        assertThat(testCompanyAddressMaster.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
        assertThat(testCompanyAddressMaster.getAddressTypeId()).isEqualTo(UPDATED_ADDRESS_TYPE_ID);
        assertThat(testCompanyAddressMaster.getAddress1()).isEqualTo(UPDATED_ADDRESS_1);
        assertThat(testCompanyAddressMaster.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testCompanyAddressMaster.getAddress3()).isEqualTo(UPDATED_ADDRESS_3);
        assertThat(testCompanyAddressMaster.getCityId()).isEqualTo(UPDATED_CITY_ID);
        assertThat(testCompanyAddressMaster.getPhoneNo()).isEqualTo(UPDATED_PHONE_NO);
        assertThat(testCompanyAddressMaster.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testCompanyAddressMaster.getFax()).isEqualTo(UPDATED_FAX);
        assertThat(testCompanyAddressMaster.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCompanyAddressMaster.getWebpage()).isEqualTo(UPDATED_WEBPAGE);
        assertThat(testCompanyAddressMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyAddressMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyAddressMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanyAddressMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyAddressMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testCompanyAddressMaster.getCompanyAddressId()).isEqualTo(UPDATED_COMPANY_ADDRESS_ID);
        assertThat(testCompanyAddressMaster.getAddressSource()).isEqualTo(UPDATED_ADDRESS_SOURCE);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyAddressMaster() throws Exception {
        int databaseSizeBeforeUpdate = companyAddressMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyAddressMasterMockMvc.perform(put("/api/company-address-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyAddressMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyAddressMaster in the database
        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyAddressMaster() throws Exception {
        // Initialize the database
        companyAddressMasterService.save(companyAddressMaster);

        int databaseSizeBeforeDelete = companyAddressMasterRepository.findAll().size();

        // Delete the companyAddressMaster
        restCompanyAddressMasterMockMvc.perform(delete("/api/company-address-masters/{id}", companyAddressMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyAddressMaster> companyAddressMasterList = companyAddressMasterRepository.findAll();
        assertThat(companyAddressMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
