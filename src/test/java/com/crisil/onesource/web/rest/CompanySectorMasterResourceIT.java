package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanySectorMaster;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.CompanySectorMasterRepository;
import com.crisil.onesource.service.CompanySectorMasterService;
import com.crisil.onesource.service.dto.CompanySectorMasterCriteria;
import com.crisil.onesource.service.CompanySectorMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanySectorMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanySectorMasterResourceIT {

    private static final Integer DEFAULT_SECTOR_ID = 1;
    private static final Integer UPDATED_SECTOR_ID = 2;
    private static final Integer SMALLER_SECTOR_ID = 1 - 1;

    private static final String DEFAULT_SECTOR_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SECTOR_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_OPERATION = "AAAAAAAAAA";
    private static final String UPDATED_OPERATION = "BBBBBBBBBB";

    @Autowired
    private CompanySectorMasterRepository companySectorMasterRepository;

    @Autowired
    private CompanySectorMasterService companySectorMasterService;

    @Autowired
    private CompanySectorMasterQueryService companySectorMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanySectorMasterMockMvc;

    private CompanySectorMaster companySectorMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanySectorMaster createEntity(EntityManager em) {
        CompanySectorMaster companySectorMaster = new CompanySectorMaster()
            .sectorId(DEFAULT_SECTOR_ID)
            .sectorName(DEFAULT_SECTOR_NAME)
            .isDeleted(DEFAULT_IS_DELETED)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .operation(DEFAULT_OPERATION);
        return companySectorMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanySectorMaster createUpdatedEntity(EntityManager em) {
        CompanySectorMaster companySectorMaster = new CompanySectorMaster()
            .sectorId(UPDATED_SECTOR_ID)
            .sectorName(UPDATED_SECTOR_NAME)
            .isDeleted(UPDATED_IS_DELETED)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .operation(UPDATED_OPERATION);
        return companySectorMaster;
    }

    @BeforeEach
    public void initTest() {
        companySectorMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanySectorMaster() throws Exception {
        int databaseSizeBeforeCreate = companySectorMasterRepository.findAll().size();
        // Create the CompanySectorMaster
        restCompanySectorMasterMockMvc.perform(post("/api/company-sector-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySectorMaster)))
            .andExpect(status().isCreated());

        // Validate the CompanySectorMaster in the database
        List<CompanySectorMaster> companySectorMasterList = companySectorMasterRepository.findAll();
        assertThat(companySectorMasterList).hasSize(databaseSizeBeforeCreate + 1);
        CompanySectorMaster testCompanySectorMaster = companySectorMasterList.get(companySectorMasterList.size() - 1);
        assertThat(testCompanySectorMaster.getSectorId()).isEqualTo(DEFAULT_SECTOR_ID);
        assertThat(testCompanySectorMaster.getSectorName()).isEqualTo(DEFAULT_SECTOR_NAME);
        assertThat(testCompanySectorMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testCompanySectorMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanySectorMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanySectorMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanySectorMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanySectorMaster.getOperation()).isEqualTo(DEFAULT_OPERATION);
    }

    @Test
    @Transactional
    public void createCompanySectorMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companySectorMasterRepository.findAll().size();

        // Create the CompanySectorMaster with an existing ID
        companySectorMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanySectorMasterMockMvc.perform(post("/api/company-sector-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySectorMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanySectorMaster in the database
        List<CompanySectorMaster> companySectorMasterList = companySectorMasterRepository.findAll();
        assertThat(companySectorMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSectorIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = companySectorMasterRepository.findAll().size();
        // set the field null
        companySectorMaster.setSectorId(null);

        // Create the CompanySectorMaster, which fails.


        restCompanySectorMasterMockMvc.perform(post("/api/company-sector-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySectorMaster)))
            .andExpect(status().isBadRequest());

        List<CompanySectorMaster> companySectorMasterList = companySectorMasterRepository.findAll();
        assertThat(companySectorMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMasters() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companySectorMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].sectorId").value(hasItem(DEFAULT_SECTOR_ID)))
            .andExpect(jsonPath("$.[*].sectorName").value(hasItem(DEFAULT_SECTOR_NAME)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)));
    }
    
    @Test
    @Transactional
    public void getCompanySectorMaster() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get the companySectorMaster
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters/{id}", companySectorMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companySectorMaster.getId().intValue()))
            .andExpect(jsonPath("$.sectorId").value(DEFAULT_SECTOR_ID))
            .andExpect(jsonPath("$.sectorName").value(DEFAULT_SECTOR_NAME))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION));
    }


    @Test
    @Transactional
    public void getCompanySectorMastersByIdFiltering() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        Long id = companySectorMaster.getId();

        defaultCompanySectorMasterShouldBeFound("id.equals=" + id);
        defaultCompanySectorMasterShouldNotBeFound("id.notEquals=" + id);

        defaultCompanySectorMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanySectorMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanySectorMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanySectorMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId equals to DEFAULT_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.equals=" + DEFAULT_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId equals to UPDATED_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.equals=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId not equals to DEFAULT_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.notEquals=" + DEFAULT_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId not equals to UPDATED_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.notEquals=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId in DEFAULT_SECTOR_ID or UPDATED_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.in=" + DEFAULT_SECTOR_ID + "," + UPDATED_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId equals to UPDATED_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.in=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId is not null
        defaultCompanySectorMasterShouldBeFound("sectorId.specified=true");

        // Get all the companySectorMasterList where sectorId is null
        defaultCompanySectorMasterShouldNotBeFound("sectorId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId is greater than or equal to DEFAULT_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.greaterThanOrEqual=" + DEFAULT_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId is greater than or equal to UPDATED_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.greaterThanOrEqual=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId is less than or equal to DEFAULT_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.lessThanOrEqual=" + DEFAULT_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId is less than or equal to SMALLER_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.lessThanOrEqual=" + SMALLER_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId is less than DEFAULT_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.lessThan=" + DEFAULT_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId is less than UPDATED_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.lessThan=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorId is greater than DEFAULT_SECTOR_ID
        defaultCompanySectorMasterShouldNotBeFound("sectorId.greaterThan=" + DEFAULT_SECTOR_ID);

        // Get all the companySectorMasterList where sectorId is greater than SMALLER_SECTOR_ID
        defaultCompanySectorMasterShouldBeFound("sectorId.greaterThan=" + SMALLER_SECTOR_ID);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorName equals to DEFAULT_SECTOR_NAME
        defaultCompanySectorMasterShouldBeFound("sectorName.equals=" + DEFAULT_SECTOR_NAME);

        // Get all the companySectorMasterList where sectorName equals to UPDATED_SECTOR_NAME
        defaultCompanySectorMasterShouldNotBeFound("sectorName.equals=" + UPDATED_SECTOR_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorName not equals to DEFAULT_SECTOR_NAME
        defaultCompanySectorMasterShouldNotBeFound("sectorName.notEquals=" + DEFAULT_SECTOR_NAME);

        // Get all the companySectorMasterList where sectorName not equals to UPDATED_SECTOR_NAME
        defaultCompanySectorMasterShouldBeFound("sectorName.notEquals=" + UPDATED_SECTOR_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorNameIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorName in DEFAULT_SECTOR_NAME or UPDATED_SECTOR_NAME
        defaultCompanySectorMasterShouldBeFound("sectorName.in=" + DEFAULT_SECTOR_NAME + "," + UPDATED_SECTOR_NAME);

        // Get all the companySectorMasterList where sectorName equals to UPDATED_SECTOR_NAME
        defaultCompanySectorMasterShouldNotBeFound("sectorName.in=" + UPDATED_SECTOR_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorName is not null
        defaultCompanySectorMasterShouldBeFound("sectorName.specified=true");

        // Get all the companySectorMasterList where sectorName is null
        defaultCompanySectorMasterShouldNotBeFound("sectorName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorNameContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorName contains DEFAULT_SECTOR_NAME
        defaultCompanySectorMasterShouldBeFound("sectorName.contains=" + DEFAULT_SECTOR_NAME);

        // Get all the companySectorMasterList where sectorName contains UPDATED_SECTOR_NAME
        defaultCompanySectorMasterShouldNotBeFound("sectorName.contains=" + UPDATED_SECTOR_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersBySectorNameNotContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where sectorName does not contain DEFAULT_SECTOR_NAME
        defaultCompanySectorMasterShouldNotBeFound("sectorName.doesNotContain=" + DEFAULT_SECTOR_NAME);

        // Get all the companySectorMasterList where sectorName does not contain UPDATED_SECTOR_NAME
        defaultCompanySectorMasterShouldBeFound("sectorName.doesNotContain=" + UPDATED_SECTOR_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanySectorMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companySectorMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanySectorMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanySectorMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companySectorMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanySectorMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanySectorMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companySectorMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanySectorMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where isDeleted is not null
        defaultCompanySectorMasterShouldBeFound("isDeleted.specified=true");

        // Get all the companySectorMasterList where isDeleted is null
        defaultCompanySectorMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySectorMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanySectorMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companySectorMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanySectorMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanySectorMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companySectorMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanySectorMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate is not null
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companySectorMasterList where lastModifiedDate is null
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companySectorMasterList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompanySectorMasterShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySectorMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySectorMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companySectorMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedBy is not null
        defaultCompanySectorMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companySectorMasterList where lastModifiedBy is null
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySectorMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companySectorMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanySectorMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanySectorMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companySectorMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanySectorMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanySectorMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companySectorMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanySectorMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanySectorMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companySectorMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanySectorMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdBy is not null
        defaultCompanySectorMasterShouldBeFound("createdBy.specified=true");

        // Get all the companySectorMasterList where createdBy is null
        defaultCompanySectorMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanySectorMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companySectorMasterList where createdBy contains UPDATED_CREATED_BY
        defaultCompanySectorMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanySectorMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companySectorMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanySectorMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate is not null
        defaultCompanySectorMasterShouldBeFound("createdDate.specified=true");

        // Get all the companySectorMasterList where createdDate is null
        defaultCompanySectorMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompanySectorMasterShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the companySectorMasterList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompanySectorMasterShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where operation equals to DEFAULT_OPERATION
        defaultCompanySectorMasterShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the companySectorMasterList where operation equals to UPDATED_OPERATION
        defaultCompanySectorMasterShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where operation not equals to DEFAULT_OPERATION
        defaultCompanySectorMasterShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the companySectorMasterList where operation not equals to UPDATED_OPERATION
        defaultCompanySectorMasterShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultCompanySectorMasterShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the companySectorMasterList where operation equals to UPDATED_OPERATION
        defaultCompanySectorMasterShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where operation is not null
        defaultCompanySectorMasterShouldBeFound("operation.specified=true");

        // Get all the companySectorMasterList where operation is null
        defaultCompanySectorMasterShouldNotBeFound("operation.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanySectorMastersByOperationContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where operation contains DEFAULT_OPERATION
        defaultCompanySectorMasterShouldBeFound("operation.contains=" + DEFAULT_OPERATION);

        // Get all the companySectorMasterList where operation contains UPDATED_OPERATION
        defaultCompanySectorMasterShouldNotBeFound("operation.contains=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanySectorMastersByOperationNotContainsSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);

        // Get all the companySectorMasterList where operation does not contain DEFAULT_OPERATION
        defaultCompanySectorMasterShouldNotBeFound("operation.doesNotContain=" + DEFAULT_OPERATION);

        // Get all the companySectorMasterList where operation does not contain UPDATED_OPERATION
        defaultCompanySectorMasterShouldBeFound("operation.doesNotContain=" + UPDATED_OPERATION);
    }


    @Test
    @Transactional
    public void getAllCompanySectorMastersByOnesourceCompanyMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        companySectorMasterRepository.saveAndFlush(companySectorMaster);
        OnesourceCompanyMaster onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(onesourceCompanyMaster);
        em.flush();
        companySectorMaster.addOnesourceCompanyMaster(onesourceCompanyMaster);
        companySectorMasterRepository.saveAndFlush(companySectorMaster);
        Long onesourceCompanyMasterId = onesourceCompanyMaster.getId();

        // Get all the companySectorMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId
        defaultCompanySectorMasterShouldBeFound("onesourceCompanyMasterId.equals=" + onesourceCompanyMasterId);

        // Get all the companySectorMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId + 1
        defaultCompanySectorMasterShouldNotBeFound("onesourceCompanyMasterId.equals=" + (onesourceCompanyMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanySectorMasterShouldBeFound(String filter) throws Exception {
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companySectorMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].sectorId").value(hasItem(DEFAULT_SECTOR_ID)))
            .andExpect(jsonPath("$.[*].sectorName").value(hasItem(DEFAULT_SECTOR_NAME)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)));

        // Check, that the count call also returns 1
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanySectorMasterShouldNotBeFound(String filter) throws Exception {
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanySectorMaster() throws Exception {
        // Get the companySectorMaster
        restCompanySectorMasterMockMvc.perform(get("/api/company-sector-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanySectorMaster() throws Exception {
        // Initialize the database
        companySectorMasterService.save(companySectorMaster);

        int databaseSizeBeforeUpdate = companySectorMasterRepository.findAll().size();

        // Update the companySectorMaster
        CompanySectorMaster updatedCompanySectorMaster = companySectorMasterRepository.findById(companySectorMaster.getId()).get();
        // Disconnect from session so that the updates on updatedCompanySectorMaster are not directly saved in db
        em.detach(updatedCompanySectorMaster);
        updatedCompanySectorMaster
            .sectorId(UPDATED_SECTOR_ID)
            .sectorName(UPDATED_SECTOR_NAME)
            .isDeleted(UPDATED_IS_DELETED)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .operation(UPDATED_OPERATION);

        restCompanySectorMasterMockMvc.perform(put("/api/company-sector-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanySectorMaster)))
            .andExpect(status().isOk());

        // Validate the CompanySectorMaster in the database
        List<CompanySectorMaster> companySectorMasterList = companySectorMasterRepository.findAll();
        assertThat(companySectorMasterList).hasSize(databaseSizeBeforeUpdate);
        CompanySectorMaster testCompanySectorMaster = companySectorMasterList.get(companySectorMasterList.size() - 1);
        assertThat(testCompanySectorMaster.getSectorId()).isEqualTo(UPDATED_SECTOR_ID);
        assertThat(testCompanySectorMaster.getSectorName()).isEqualTo(UPDATED_SECTOR_NAME);
        assertThat(testCompanySectorMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testCompanySectorMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanySectorMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanySectorMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanySectorMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanySectorMaster.getOperation()).isEqualTo(UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanySectorMaster() throws Exception {
        int databaseSizeBeforeUpdate = companySectorMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanySectorMasterMockMvc.perform(put("/api/company-sector-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companySectorMaster)))
            .andExpect(status().isBadRequest());

        // Validate the CompanySectorMaster in the database
        List<CompanySectorMaster> companySectorMasterList = companySectorMasterRepository.findAll();
        assertThat(companySectorMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanySectorMaster() throws Exception {
        // Initialize the database
        companySectorMasterService.save(companySectorMaster);

        int databaseSizeBeforeDelete = companySectorMasterRepository.findAll().size();

        // Delete the companySectorMaster
        restCompanySectorMasterMockMvc.perform(delete("/api/company-sector-masters/{id}", companySectorMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanySectorMaster> companySectorMasterList = companySectorMasterRepository.findAll();
        assertThat(companySectorMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
