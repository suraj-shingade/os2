package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.ContactChangeRequest;
import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.repository.ContactChangeRequestRepository;
import com.crisil.onesource.service.ContactChangeRequestService;
import com.crisil.onesource.service.dto.ContactChangeRequestCriteria;
import com.crisil.onesource.service.ContactChangeRequestQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactChangeRequestResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactChangeRequestResourceIT {

    private static final String DEFAULT_REQUEST_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REQUESTER_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTER_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_REQUESTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_REQUESTED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REQUESTED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_APPROVER_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_APPROVER_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_APPROVED_BY = "AAAAAAAAAA";
    private static final String UPDATED_APPROVED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_APPROVED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_APPROVED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_IS_REVERSE_REQUEST = "A";
    private static final String UPDATED_IS_REVERSE_REQUEST = "B";

    private static final String DEFAULT_REQUEST_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_TYPE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private ContactChangeRequestRepository contactChangeRequestRepository;

    @Autowired
    private ContactChangeRequestService contactChangeRequestService;

    @Autowired
    private ContactChangeRequestQueryService contactChangeRequestQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactChangeRequestMockMvc;

    private ContactChangeRequest contactChangeRequest;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactChangeRequest createEntity(EntityManager em) {
        ContactChangeRequest contactChangeRequest = new ContactChangeRequest()
            .requestStatus(DEFAULT_REQUEST_STATUS)
            .requesterComments(DEFAULT_REQUESTER_COMMENTS)
            .requestedBy(DEFAULT_REQUESTED_BY)
            .requestedDate(DEFAULT_REQUESTED_DATE)
            .approverComments(DEFAULT_APPROVER_COMMENTS)
            .approvedBy(DEFAULT_APPROVED_BY)
            .approvedDate(DEFAULT_APPROVED_DATE)
            .fileName(DEFAULT_FILE_NAME)
            .isReverseRequest(DEFAULT_IS_REVERSE_REQUEST)
            .requestType(DEFAULT_REQUEST_TYPE)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED);
        return contactChangeRequest;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactChangeRequest createUpdatedEntity(EntityManager em) {
        ContactChangeRequest contactChangeRequest = new ContactChangeRequest()
            .requestStatus(UPDATED_REQUEST_STATUS)
            .requesterComments(UPDATED_REQUESTER_COMMENTS)
            .requestedBy(UPDATED_REQUESTED_BY)
            .requestedDate(UPDATED_REQUESTED_DATE)
            .approverComments(UPDATED_APPROVER_COMMENTS)
            .approvedBy(UPDATED_APPROVED_BY)
            .approvedDate(UPDATED_APPROVED_DATE)
            .fileName(UPDATED_FILE_NAME)
            .isReverseRequest(UPDATED_IS_REVERSE_REQUEST)
            .requestType(UPDATED_REQUEST_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);
        return contactChangeRequest;
    }

    @BeforeEach
    public void initTest() {
        contactChangeRequest = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactChangeRequest() throws Exception {
        int databaseSizeBeforeCreate = contactChangeRequestRepository.findAll().size();
        // Create the ContactChangeRequest
        restContactChangeRequestMockMvc.perform(post("/api/contact-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactChangeRequest)))
            .andExpect(status().isCreated());

        // Validate the ContactChangeRequest in the database
        List<ContactChangeRequest> contactChangeRequestList = contactChangeRequestRepository.findAll();
        assertThat(contactChangeRequestList).hasSize(databaseSizeBeforeCreate + 1);
        ContactChangeRequest testContactChangeRequest = contactChangeRequestList.get(contactChangeRequestList.size() - 1);
        assertThat(testContactChangeRequest.getRequestStatus()).isEqualTo(DEFAULT_REQUEST_STATUS);
        assertThat(testContactChangeRequest.getRequesterComments()).isEqualTo(DEFAULT_REQUESTER_COMMENTS);
        assertThat(testContactChangeRequest.getRequestedBy()).isEqualTo(DEFAULT_REQUESTED_BY);
        assertThat(testContactChangeRequest.getRequestedDate()).isEqualTo(DEFAULT_REQUESTED_DATE);
        assertThat(testContactChangeRequest.getApproverComments()).isEqualTo(DEFAULT_APPROVER_COMMENTS);
        assertThat(testContactChangeRequest.getApprovedBy()).isEqualTo(DEFAULT_APPROVED_BY);
        assertThat(testContactChangeRequest.getApprovedDate()).isEqualTo(DEFAULT_APPROVED_DATE);
        assertThat(testContactChangeRequest.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testContactChangeRequest.getIsReverseRequest()).isEqualTo(DEFAULT_IS_REVERSE_REQUEST);
        assertThat(testContactChangeRequest.getRequestType()).isEqualTo(DEFAULT_REQUEST_TYPE);
        assertThat(testContactChangeRequest.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testContactChangeRequest.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testContactChangeRequest.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testContactChangeRequest.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createContactChangeRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactChangeRequestRepository.findAll().size();

        // Create the ContactChangeRequest with an existing ID
        contactChangeRequest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactChangeRequestMockMvc.perform(post("/api/contact-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactChangeRequest)))
            .andExpect(status().isBadRequest());

        // Validate the ContactChangeRequest in the database
        List<ContactChangeRequest> contactChangeRequestList = contactChangeRequestRepository.findAll();
        assertThat(contactChangeRequestList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequests() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactChangeRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].requesterComments").value(hasItem(DEFAULT_REQUESTER_COMMENTS)))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].requestedDate").value(hasItem(DEFAULT_REQUESTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].approverComments").value(hasItem(DEFAULT_APPROVER_COMMENTS)))
            .andExpect(jsonPath("$.[*].approvedBy").value(hasItem(DEFAULT_APPROVED_BY)))
            .andExpect(jsonPath("$.[*].approvedDate").value(hasItem(DEFAULT_APPROVED_DATE.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].isReverseRequest").value(hasItem(DEFAULT_IS_REVERSE_REQUEST)))
            .andExpect(jsonPath("$.[*].requestType").value(hasItem(DEFAULT_REQUEST_TYPE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getContactChangeRequest() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get the contactChangeRequest
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests/{id}", contactChangeRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactChangeRequest.getId().intValue()))
            .andExpect(jsonPath("$.requestStatus").value(DEFAULT_REQUEST_STATUS))
            .andExpect(jsonPath("$.requesterComments").value(DEFAULT_REQUESTER_COMMENTS))
            .andExpect(jsonPath("$.requestedBy").value(DEFAULT_REQUESTED_BY))
            .andExpect(jsonPath("$.requestedDate").value(DEFAULT_REQUESTED_DATE.toString()))
            .andExpect(jsonPath("$.approverComments").value(DEFAULT_APPROVER_COMMENTS))
            .andExpect(jsonPath("$.approvedBy").value(DEFAULT_APPROVED_BY))
            .andExpect(jsonPath("$.approvedDate").value(DEFAULT_APPROVED_DATE.toString()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME))
            .andExpect(jsonPath("$.isReverseRequest").value(DEFAULT_IS_REVERSE_REQUEST))
            .andExpect(jsonPath("$.requestType").value(DEFAULT_REQUEST_TYPE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getContactChangeRequestsByIdFiltering() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        Long id = contactChangeRequest.getId();

        defaultContactChangeRequestShouldBeFound("id.equals=" + id);
        defaultContactChangeRequestShouldNotBeFound("id.notEquals=" + id);

        defaultContactChangeRequestShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactChangeRequestShouldNotBeFound("id.greaterThan=" + id);

        defaultContactChangeRequestShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactChangeRequestShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestStatus equals to DEFAULT_REQUEST_STATUS
        defaultContactChangeRequestShouldBeFound("requestStatus.equals=" + DEFAULT_REQUEST_STATUS);

        // Get all the contactChangeRequestList where requestStatus equals to UPDATED_REQUEST_STATUS
        defaultContactChangeRequestShouldNotBeFound("requestStatus.equals=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestStatus not equals to DEFAULT_REQUEST_STATUS
        defaultContactChangeRequestShouldNotBeFound("requestStatus.notEquals=" + DEFAULT_REQUEST_STATUS);

        // Get all the contactChangeRequestList where requestStatus not equals to UPDATED_REQUEST_STATUS
        defaultContactChangeRequestShouldBeFound("requestStatus.notEquals=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestStatusIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestStatus in DEFAULT_REQUEST_STATUS or UPDATED_REQUEST_STATUS
        defaultContactChangeRequestShouldBeFound("requestStatus.in=" + DEFAULT_REQUEST_STATUS + "," + UPDATED_REQUEST_STATUS);

        // Get all the contactChangeRequestList where requestStatus equals to UPDATED_REQUEST_STATUS
        defaultContactChangeRequestShouldNotBeFound("requestStatus.in=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestStatus is not null
        defaultContactChangeRequestShouldBeFound("requestStatus.specified=true");

        // Get all the contactChangeRequestList where requestStatus is null
        defaultContactChangeRequestShouldNotBeFound("requestStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestStatusContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestStatus contains DEFAULT_REQUEST_STATUS
        defaultContactChangeRequestShouldBeFound("requestStatus.contains=" + DEFAULT_REQUEST_STATUS);

        // Get all the contactChangeRequestList where requestStatus contains UPDATED_REQUEST_STATUS
        defaultContactChangeRequestShouldNotBeFound("requestStatus.contains=" + UPDATED_REQUEST_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestStatusNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestStatus does not contain DEFAULT_REQUEST_STATUS
        defaultContactChangeRequestShouldNotBeFound("requestStatus.doesNotContain=" + DEFAULT_REQUEST_STATUS);

        // Get all the contactChangeRequestList where requestStatus does not contain UPDATED_REQUEST_STATUS
        defaultContactChangeRequestShouldBeFound("requestStatus.doesNotContain=" + UPDATED_REQUEST_STATUS);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequesterCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requesterComments equals to DEFAULT_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldBeFound("requesterComments.equals=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the contactChangeRequestList where requesterComments equals to UPDATED_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("requesterComments.equals=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequesterCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requesterComments not equals to DEFAULT_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("requesterComments.notEquals=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the contactChangeRequestList where requesterComments not equals to UPDATED_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldBeFound("requesterComments.notEquals=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequesterCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requesterComments in DEFAULT_REQUESTER_COMMENTS or UPDATED_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldBeFound("requesterComments.in=" + DEFAULT_REQUESTER_COMMENTS + "," + UPDATED_REQUESTER_COMMENTS);

        // Get all the contactChangeRequestList where requesterComments equals to UPDATED_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("requesterComments.in=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequesterCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requesterComments is not null
        defaultContactChangeRequestShouldBeFound("requesterComments.specified=true");

        // Get all the contactChangeRequestList where requesterComments is null
        defaultContactChangeRequestShouldNotBeFound("requesterComments.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByRequesterCommentsContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requesterComments contains DEFAULT_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldBeFound("requesterComments.contains=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the contactChangeRequestList where requesterComments contains UPDATED_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("requesterComments.contains=" + UPDATED_REQUESTER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequesterCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requesterComments does not contain DEFAULT_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("requesterComments.doesNotContain=" + DEFAULT_REQUESTER_COMMENTS);

        // Get all the contactChangeRequestList where requesterComments does not contain UPDATED_REQUESTER_COMMENTS
        defaultContactChangeRequestShouldBeFound("requesterComments.doesNotContain=" + UPDATED_REQUESTER_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedBy equals to DEFAULT_REQUESTED_BY
        defaultContactChangeRequestShouldBeFound("requestedBy.equals=" + DEFAULT_REQUESTED_BY);

        // Get all the contactChangeRequestList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultContactChangeRequestShouldNotBeFound("requestedBy.equals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedBy not equals to DEFAULT_REQUESTED_BY
        defaultContactChangeRequestShouldNotBeFound("requestedBy.notEquals=" + DEFAULT_REQUESTED_BY);

        // Get all the contactChangeRequestList where requestedBy not equals to UPDATED_REQUESTED_BY
        defaultContactChangeRequestShouldBeFound("requestedBy.notEquals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedBy in DEFAULT_REQUESTED_BY or UPDATED_REQUESTED_BY
        defaultContactChangeRequestShouldBeFound("requestedBy.in=" + DEFAULT_REQUESTED_BY + "," + UPDATED_REQUESTED_BY);

        // Get all the contactChangeRequestList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultContactChangeRequestShouldNotBeFound("requestedBy.in=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedBy is not null
        defaultContactChangeRequestShouldBeFound("requestedBy.specified=true");

        // Get all the contactChangeRequestList where requestedBy is null
        defaultContactChangeRequestShouldNotBeFound("requestedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedByContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedBy contains DEFAULT_REQUESTED_BY
        defaultContactChangeRequestShouldBeFound("requestedBy.contains=" + DEFAULT_REQUESTED_BY);

        // Get all the contactChangeRequestList where requestedBy contains UPDATED_REQUESTED_BY
        defaultContactChangeRequestShouldNotBeFound("requestedBy.contains=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedBy does not contain DEFAULT_REQUESTED_BY
        defaultContactChangeRequestShouldNotBeFound("requestedBy.doesNotContain=" + DEFAULT_REQUESTED_BY);

        // Get all the contactChangeRequestList where requestedBy does not contain UPDATED_REQUESTED_BY
        defaultContactChangeRequestShouldBeFound("requestedBy.doesNotContain=" + UPDATED_REQUESTED_BY);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedDate equals to DEFAULT_REQUESTED_DATE
        defaultContactChangeRequestShouldBeFound("requestedDate.equals=" + DEFAULT_REQUESTED_DATE);

        // Get all the contactChangeRequestList where requestedDate equals to UPDATED_REQUESTED_DATE
        defaultContactChangeRequestShouldNotBeFound("requestedDate.equals=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedDate not equals to DEFAULT_REQUESTED_DATE
        defaultContactChangeRequestShouldNotBeFound("requestedDate.notEquals=" + DEFAULT_REQUESTED_DATE);

        // Get all the contactChangeRequestList where requestedDate not equals to UPDATED_REQUESTED_DATE
        defaultContactChangeRequestShouldBeFound("requestedDate.notEquals=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedDate in DEFAULT_REQUESTED_DATE or UPDATED_REQUESTED_DATE
        defaultContactChangeRequestShouldBeFound("requestedDate.in=" + DEFAULT_REQUESTED_DATE + "," + UPDATED_REQUESTED_DATE);

        // Get all the contactChangeRequestList where requestedDate equals to UPDATED_REQUESTED_DATE
        defaultContactChangeRequestShouldNotBeFound("requestedDate.in=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestedDate is not null
        defaultContactChangeRequestShouldBeFound("requestedDate.specified=true");

        // Get all the contactChangeRequestList where requestedDate is null
        defaultContactChangeRequestShouldNotBeFound("requestedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApproverCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approverComments equals to DEFAULT_APPROVER_COMMENTS
        defaultContactChangeRequestShouldBeFound("approverComments.equals=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the contactChangeRequestList where approverComments equals to UPDATED_APPROVER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("approverComments.equals=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApproverCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approverComments not equals to DEFAULT_APPROVER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("approverComments.notEquals=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the contactChangeRequestList where approverComments not equals to UPDATED_APPROVER_COMMENTS
        defaultContactChangeRequestShouldBeFound("approverComments.notEquals=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApproverCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approverComments in DEFAULT_APPROVER_COMMENTS or UPDATED_APPROVER_COMMENTS
        defaultContactChangeRequestShouldBeFound("approverComments.in=" + DEFAULT_APPROVER_COMMENTS + "," + UPDATED_APPROVER_COMMENTS);

        // Get all the contactChangeRequestList where approverComments equals to UPDATED_APPROVER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("approverComments.in=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApproverCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approverComments is not null
        defaultContactChangeRequestShouldBeFound("approverComments.specified=true");

        // Get all the contactChangeRequestList where approverComments is null
        defaultContactChangeRequestShouldNotBeFound("approverComments.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByApproverCommentsContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approverComments contains DEFAULT_APPROVER_COMMENTS
        defaultContactChangeRequestShouldBeFound("approverComments.contains=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the contactChangeRequestList where approverComments contains UPDATED_APPROVER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("approverComments.contains=" + UPDATED_APPROVER_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApproverCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approverComments does not contain DEFAULT_APPROVER_COMMENTS
        defaultContactChangeRequestShouldNotBeFound("approverComments.doesNotContain=" + DEFAULT_APPROVER_COMMENTS);

        // Get all the contactChangeRequestList where approverComments does not contain UPDATED_APPROVER_COMMENTS
        defaultContactChangeRequestShouldBeFound("approverComments.doesNotContain=" + UPDATED_APPROVER_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedBy equals to DEFAULT_APPROVED_BY
        defaultContactChangeRequestShouldBeFound("approvedBy.equals=" + DEFAULT_APPROVED_BY);

        // Get all the contactChangeRequestList where approvedBy equals to UPDATED_APPROVED_BY
        defaultContactChangeRequestShouldNotBeFound("approvedBy.equals=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedBy not equals to DEFAULT_APPROVED_BY
        defaultContactChangeRequestShouldNotBeFound("approvedBy.notEquals=" + DEFAULT_APPROVED_BY);

        // Get all the contactChangeRequestList where approvedBy not equals to UPDATED_APPROVED_BY
        defaultContactChangeRequestShouldBeFound("approvedBy.notEquals=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedBy in DEFAULT_APPROVED_BY or UPDATED_APPROVED_BY
        defaultContactChangeRequestShouldBeFound("approvedBy.in=" + DEFAULT_APPROVED_BY + "," + UPDATED_APPROVED_BY);

        // Get all the contactChangeRequestList where approvedBy equals to UPDATED_APPROVED_BY
        defaultContactChangeRequestShouldNotBeFound("approvedBy.in=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedBy is not null
        defaultContactChangeRequestShouldBeFound("approvedBy.specified=true");

        // Get all the contactChangeRequestList where approvedBy is null
        defaultContactChangeRequestShouldNotBeFound("approvedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedByContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedBy contains DEFAULT_APPROVED_BY
        defaultContactChangeRequestShouldBeFound("approvedBy.contains=" + DEFAULT_APPROVED_BY);

        // Get all the contactChangeRequestList where approvedBy contains UPDATED_APPROVED_BY
        defaultContactChangeRequestShouldNotBeFound("approvedBy.contains=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedBy does not contain DEFAULT_APPROVED_BY
        defaultContactChangeRequestShouldNotBeFound("approvedBy.doesNotContain=" + DEFAULT_APPROVED_BY);

        // Get all the contactChangeRequestList where approvedBy does not contain UPDATED_APPROVED_BY
        defaultContactChangeRequestShouldBeFound("approvedBy.doesNotContain=" + UPDATED_APPROVED_BY);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedDate equals to DEFAULT_APPROVED_DATE
        defaultContactChangeRequestShouldBeFound("approvedDate.equals=" + DEFAULT_APPROVED_DATE);

        // Get all the contactChangeRequestList where approvedDate equals to UPDATED_APPROVED_DATE
        defaultContactChangeRequestShouldNotBeFound("approvedDate.equals=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedDate not equals to DEFAULT_APPROVED_DATE
        defaultContactChangeRequestShouldNotBeFound("approvedDate.notEquals=" + DEFAULT_APPROVED_DATE);

        // Get all the contactChangeRequestList where approvedDate not equals to UPDATED_APPROVED_DATE
        defaultContactChangeRequestShouldBeFound("approvedDate.notEquals=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedDate in DEFAULT_APPROVED_DATE or UPDATED_APPROVED_DATE
        defaultContactChangeRequestShouldBeFound("approvedDate.in=" + DEFAULT_APPROVED_DATE + "," + UPDATED_APPROVED_DATE);

        // Get all the contactChangeRequestList where approvedDate equals to UPDATED_APPROVED_DATE
        defaultContactChangeRequestShouldNotBeFound("approvedDate.in=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByApprovedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where approvedDate is not null
        defaultContactChangeRequestShouldBeFound("approvedDate.specified=true");

        // Get all the contactChangeRequestList where approvedDate is null
        defaultContactChangeRequestShouldNotBeFound("approvedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByFileNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where fileName equals to DEFAULT_FILE_NAME
        defaultContactChangeRequestShouldBeFound("fileName.equals=" + DEFAULT_FILE_NAME);

        // Get all the contactChangeRequestList where fileName equals to UPDATED_FILE_NAME
        defaultContactChangeRequestShouldNotBeFound("fileName.equals=" + UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByFileNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where fileName not equals to DEFAULT_FILE_NAME
        defaultContactChangeRequestShouldNotBeFound("fileName.notEquals=" + DEFAULT_FILE_NAME);

        // Get all the contactChangeRequestList where fileName not equals to UPDATED_FILE_NAME
        defaultContactChangeRequestShouldBeFound("fileName.notEquals=" + UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByFileNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where fileName in DEFAULT_FILE_NAME or UPDATED_FILE_NAME
        defaultContactChangeRequestShouldBeFound("fileName.in=" + DEFAULT_FILE_NAME + "," + UPDATED_FILE_NAME);

        // Get all the contactChangeRequestList where fileName equals to UPDATED_FILE_NAME
        defaultContactChangeRequestShouldNotBeFound("fileName.in=" + UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByFileNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where fileName is not null
        defaultContactChangeRequestShouldBeFound("fileName.specified=true");

        // Get all the contactChangeRequestList where fileName is null
        defaultContactChangeRequestShouldNotBeFound("fileName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByFileNameContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where fileName contains DEFAULT_FILE_NAME
        defaultContactChangeRequestShouldBeFound("fileName.contains=" + DEFAULT_FILE_NAME);

        // Get all the contactChangeRequestList where fileName contains UPDATED_FILE_NAME
        defaultContactChangeRequestShouldNotBeFound("fileName.contains=" + UPDATED_FILE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByFileNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where fileName does not contain DEFAULT_FILE_NAME
        defaultContactChangeRequestShouldNotBeFound("fileName.doesNotContain=" + DEFAULT_FILE_NAME);

        // Get all the contactChangeRequestList where fileName does not contain UPDATED_FILE_NAME
        defaultContactChangeRequestShouldBeFound("fileName.doesNotContain=" + UPDATED_FILE_NAME);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsReverseRequestIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isReverseRequest equals to DEFAULT_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldBeFound("isReverseRequest.equals=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the contactChangeRequestList where isReverseRequest equals to UPDATED_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldNotBeFound("isReverseRequest.equals=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsReverseRequestIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isReverseRequest not equals to DEFAULT_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldNotBeFound("isReverseRequest.notEquals=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the contactChangeRequestList where isReverseRequest not equals to UPDATED_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldBeFound("isReverseRequest.notEquals=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsReverseRequestIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isReverseRequest in DEFAULT_IS_REVERSE_REQUEST or UPDATED_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldBeFound("isReverseRequest.in=" + DEFAULT_IS_REVERSE_REQUEST + "," + UPDATED_IS_REVERSE_REQUEST);

        // Get all the contactChangeRequestList where isReverseRequest equals to UPDATED_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldNotBeFound("isReverseRequest.in=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsReverseRequestIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isReverseRequest is not null
        defaultContactChangeRequestShouldBeFound("isReverseRequest.specified=true");

        // Get all the contactChangeRequestList where isReverseRequest is null
        defaultContactChangeRequestShouldNotBeFound("isReverseRequest.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByIsReverseRequestContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isReverseRequest contains DEFAULT_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldBeFound("isReverseRequest.contains=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the contactChangeRequestList where isReverseRequest contains UPDATED_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldNotBeFound("isReverseRequest.contains=" + UPDATED_IS_REVERSE_REQUEST);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsReverseRequestNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isReverseRequest does not contain DEFAULT_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldNotBeFound("isReverseRequest.doesNotContain=" + DEFAULT_IS_REVERSE_REQUEST);

        // Get all the contactChangeRequestList where isReverseRequest does not contain UPDATED_IS_REVERSE_REQUEST
        defaultContactChangeRequestShouldBeFound("isReverseRequest.doesNotContain=" + UPDATED_IS_REVERSE_REQUEST);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestType equals to DEFAULT_REQUEST_TYPE
        defaultContactChangeRequestShouldBeFound("requestType.equals=" + DEFAULT_REQUEST_TYPE);

        // Get all the contactChangeRequestList where requestType equals to UPDATED_REQUEST_TYPE
        defaultContactChangeRequestShouldNotBeFound("requestType.equals=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestType not equals to DEFAULT_REQUEST_TYPE
        defaultContactChangeRequestShouldNotBeFound("requestType.notEquals=" + DEFAULT_REQUEST_TYPE);

        // Get all the contactChangeRequestList where requestType not equals to UPDATED_REQUEST_TYPE
        defaultContactChangeRequestShouldBeFound("requestType.notEquals=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestType in DEFAULT_REQUEST_TYPE or UPDATED_REQUEST_TYPE
        defaultContactChangeRequestShouldBeFound("requestType.in=" + DEFAULT_REQUEST_TYPE + "," + UPDATED_REQUEST_TYPE);

        // Get all the contactChangeRequestList where requestType equals to UPDATED_REQUEST_TYPE
        defaultContactChangeRequestShouldNotBeFound("requestType.in=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestType is not null
        defaultContactChangeRequestShouldBeFound("requestType.specified=true");

        // Get all the contactChangeRequestList where requestType is null
        defaultContactChangeRequestShouldNotBeFound("requestType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestTypeContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestType contains DEFAULT_REQUEST_TYPE
        defaultContactChangeRequestShouldBeFound("requestType.contains=" + DEFAULT_REQUEST_TYPE);

        // Get all the contactChangeRequestList where requestType contains UPDATED_REQUEST_TYPE
        defaultContactChangeRequestShouldNotBeFound("requestType.contains=" + UPDATED_REQUEST_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByRequestTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where requestType does not contain DEFAULT_REQUEST_TYPE
        defaultContactChangeRequestShouldNotBeFound("requestType.doesNotContain=" + DEFAULT_REQUEST_TYPE);

        // Get all the contactChangeRequestList where requestType does not contain UPDATED_REQUEST_TYPE
        defaultContactChangeRequestShouldBeFound("requestType.doesNotContain=" + UPDATED_REQUEST_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate equals to DEFAULT_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate equals to UPDATED_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate not equals to UPDATED_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate equals to UPDATED_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate is not null
        defaultContactChangeRequestShouldBeFound("createdDate.specified=true");

        // Get all the contactChangeRequestList where createdDate is null
        defaultContactChangeRequestShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate is less than DEFAULT_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate is less than UPDATED_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultContactChangeRequestShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the contactChangeRequestList where createdDate is greater than SMALLER_CREATED_DATE
        defaultContactChangeRequestShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactChangeRequestList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactChangeRequestList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the contactChangeRequestList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedBy is not null
        defaultContactChangeRequestShouldBeFound("lastModifiedBy.specified=true");

        // Get all the contactChangeRequestList where lastModifiedBy is null
        defaultContactChangeRequestShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactChangeRequestList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactChangeRequestList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultContactChangeRequestShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate is not null
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.specified=true");

        // Get all the contactChangeRequestList where lastModifiedDate is null
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactChangeRequestList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultContactChangeRequestShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isDeleted equals to DEFAULT_IS_DELETED
        defaultContactChangeRequestShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the contactChangeRequestList where isDeleted equals to UPDATED_IS_DELETED
        defaultContactChangeRequestShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultContactChangeRequestShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the contactChangeRequestList where isDeleted not equals to UPDATED_IS_DELETED
        defaultContactChangeRequestShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultContactChangeRequestShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the contactChangeRequestList where isDeleted equals to UPDATED_IS_DELETED
        defaultContactChangeRequestShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isDeleted is not null
        defaultContactChangeRequestShouldBeFound("isDeleted.specified=true");

        // Get all the contactChangeRequestList where isDeleted is null
        defaultContactChangeRequestShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactChangeRequestsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isDeleted contains DEFAULT_IS_DELETED
        defaultContactChangeRequestShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the contactChangeRequestList where isDeleted contains UPDATED_IS_DELETED
        defaultContactChangeRequestShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactChangeRequestsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);

        // Get all the contactChangeRequestList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultContactChangeRequestShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the contactChangeRequestList where isDeleted does not contain UPDATED_IS_DELETED
        defaultContactChangeRequestShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllContactChangeRequestsByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);
        ContactMaster contactId = ContactMasterResourceIT.createEntity(em);
        em.persist(contactId);
        em.flush();
        contactChangeRequest.setContactId(contactId);
        contactChangeRequestRepository.saveAndFlush(contactChangeRequest);
        Long contactIdId = contactId.getId();

        // Get all the contactChangeRequestList where contactId equals to contactIdId
        defaultContactChangeRequestShouldBeFound("contactIdId.equals=" + contactIdId);

        // Get all the contactChangeRequestList where contactId equals to contactIdId + 1
        defaultContactChangeRequestShouldNotBeFound("contactIdId.equals=" + (contactIdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactChangeRequestShouldBeFound(String filter) throws Exception {
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactChangeRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestStatus").value(hasItem(DEFAULT_REQUEST_STATUS)))
            .andExpect(jsonPath("$.[*].requesterComments").value(hasItem(DEFAULT_REQUESTER_COMMENTS)))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].requestedDate").value(hasItem(DEFAULT_REQUESTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].approverComments").value(hasItem(DEFAULT_APPROVER_COMMENTS)))
            .andExpect(jsonPath("$.[*].approvedBy").value(hasItem(DEFAULT_APPROVED_BY)))
            .andExpect(jsonPath("$.[*].approvedDate").value(hasItem(DEFAULT_APPROVED_DATE.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].isReverseRequest").value(hasItem(DEFAULT_IS_REVERSE_REQUEST)))
            .andExpect(jsonPath("$.[*].requestType").value(hasItem(DEFAULT_REQUEST_TYPE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactChangeRequestShouldNotBeFound(String filter) throws Exception {
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactChangeRequest() throws Exception {
        // Get the contactChangeRequest
        restContactChangeRequestMockMvc.perform(get("/api/contact-change-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactChangeRequest() throws Exception {
        // Initialize the database
        contactChangeRequestService.save(contactChangeRequest);

        int databaseSizeBeforeUpdate = contactChangeRequestRepository.findAll().size();

        // Update the contactChangeRequest
        ContactChangeRequest updatedContactChangeRequest = contactChangeRequestRepository.findById(contactChangeRequest.getId()).get();
        // Disconnect from session so that the updates on updatedContactChangeRequest are not directly saved in db
        em.detach(updatedContactChangeRequest);
        updatedContactChangeRequest
            .requestStatus(UPDATED_REQUEST_STATUS)
            .requesterComments(UPDATED_REQUESTER_COMMENTS)
            .requestedBy(UPDATED_REQUESTED_BY)
            .requestedDate(UPDATED_REQUESTED_DATE)
            .approverComments(UPDATED_APPROVER_COMMENTS)
            .approvedBy(UPDATED_APPROVED_BY)
            .approvedDate(UPDATED_APPROVED_DATE)
            .fileName(UPDATED_FILE_NAME)
            .isReverseRequest(UPDATED_IS_REVERSE_REQUEST)
            .requestType(UPDATED_REQUEST_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);

        restContactChangeRequestMockMvc.perform(put("/api/contact-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedContactChangeRequest)))
            .andExpect(status().isOk());

        // Validate the ContactChangeRequest in the database
        List<ContactChangeRequest> contactChangeRequestList = contactChangeRequestRepository.findAll();
        assertThat(contactChangeRequestList).hasSize(databaseSizeBeforeUpdate);
        ContactChangeRequest testContactChangeRequest = contactChangeRequestList.get(contactChangeRequestList.size() - 1);
        assertThat(testContactChangeRequest.getRequestStatus()).isEqualTo(UPDATED_REQUEST_STATUS);
        assertThat(testContactChangeRequest.getRequesterComments()).isEqualTo(UPDATED_REQUESTER_COMMENTS);
        assertThat(testContactChangeRequest.getRequestedBy()).isEqualTo(UPDATED_REQUESTED_BY);
        assertThat(testContactChangeRequest.getRequestedDate()).isEqualTo(UPDATED_REQUESTED_DATE);
        assertThat(testContactChangeRequest.getApproverComments()).isEqualTo(UPDATED_APPROVER_COMMENTS);
        assertThat(testContactChangeRequest.getApprovedBy()).isEqualTo(UPDATED_APPROVED_BY);
        assertThat(testContactChangeRequest.getApprovedDate()).isEqualTo(UPDATED_APPROVED_DATE);
        assertThat(testContactChangeRequest.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testContactChangeRequest.getIsReverseRequest()).isEqualTo(UPDATED_IS_REVERSE_REQUEST);
        assertThat(testContactChangeRequest.getRequestType()).isEqualTo(UPDATED_REQUEST_TYPE);
        assertThat(testContactChangeRequest.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testContactChangeRequest.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testContactChangeRequest.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testContactChangeRequest.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingContactChangeRequest() throws Exception {
        int databaseSizeBeforeUpdate = contactChangeRequestRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactChangeRequestMockMvc.perform(put("/api/contact-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactChangeRequest)))
            .andExpect(status().isBadRequest());

        // Validate the ContactChangeRequest in the database
        List<ContactChangeRequest> contactChangeRequestList = contactChangeRequestRepository.findAll();
        assertThat(contactChangeRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactChangeRequest() throws Exception {
        // Initialize the database
        contactChangeRequestService.save(contactChangeRequest);

        int databaseSizeBeforeDelete = contactChangeRequestRepository.findAll().size();

        // Delete the contactChangeRequest
        restContactChangeRequestMockMvc.perform(delete("/api/contact-change-requests/{id}", contactChangeRequest.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactChangeRequest> contactChangeRequestList = contactChangeRequestRepository.findAll();
        assertThat(contactChangeRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
