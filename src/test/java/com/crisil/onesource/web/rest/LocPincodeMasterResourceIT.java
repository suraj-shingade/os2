package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.domain.CompanyAddressMaster;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.repository.LocPincodeMasterRepository;
import com.crisil.onesource.service.LocPincodeMasterService;
import com.crisil.onesource.service.dto.LocPincodeMasterCriteria;
import com.crisil.onesource.service.LocPincodeMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocPincodeMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocPincodeMasterResourceIT {

    private static final Integer DEFAULT_PINCODE_SRNO = 1;
    private static final Integer UPDATED_PINCODE_SRNO = 2;
    private static final Integer SMALLER_PINCODE_SRNO = 1 - 1;

    private static final String DEFAULT_OFFICE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_OFFICE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PINCODE = "AAAAAAAAAA";
    private static final String UPDATED_PINCODE = "BBBBBBBBBB";

    private static final String DEFAULT_OFFICE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_OFFICE_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_DIVISION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DIVISION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REGION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_REGION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CIRCLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CIRCLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TALUKA = "AAAAAAAAAA";
    private static final String UPDATED_TALUKA = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STATE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_APPROVED = "A";
    private static final String UPDATED_IS_APPROVED = "B";

    private static final Integer DEFAULT_SEQ_ORDER = 1;
    private static final Integer UPDATED_SEQ_ORDER = 2;
    private static final Integer SMALLER_SEQ_ORDER = 1 - 1;

    private static final String DEFAULT_IS_DATA_PROCESSED = "A";
    private static final String UPDATED_IS_DATA_PROCESSED = "B";

    private static final Long DEFAULT_JSON_STORE_ID = 1L;
    private static final Long UPDATED_JSON_STORE_ID = 2L;
    private static final Long SMALLER_JSON_STORE_ID = 1L - 1L;

    private static final String DEFAULT_CTLG_IS_DELETED = "A";
    private static final String UPDATED_CTLG_IS_DELETED = "B";

    private static final String DEFAULT_OPERATION = "A";
    private static final String UPDATED_OPERATION = "B";

    private static final Integer DEFAULT_CTLG_SRNO = 1;
    private static final Integer UPDATED_CTLG_SRNO = 2;
    private static final Integer SMALLER_CTLG_SRNO = 1 - 1;

    @Autowired
    private LocPincodeMasterRepository locPincodeMasterRepository;

    @Autowired
    private LocPincodeMasterService locPincodeMasterService;

    @Autowired
    private LocPincodeMasterQueryService locPincodeMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocPincodeMasterMockMvc;

    private LocPincodeMaster locPincodeMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocPincodeMaster createEntity(EntityManager em) {
        LocPincodeMaster locPincodeMaster = new LocPincodeMaster()
            .pincodeSrno(DEFAULT_PINCODE_SRNO)
            .officeName(DEFAULT_OFFICE_NAME)
            .pincode(DEFAULT_PINCODE)
            .officeType(DEFAULT_OFFICE_TYPE)
            .deliveryStatus(DEFAULT_DELIVERY_STATUS)
            .divisionName(DEFAULT_DIVISION_NAME)
            .regionName(DEFAULT_REGION_NAME)
            .circleName(DEFAULT_CIRCLE_NAME)
            .taluka(DEFAULT_TALUKA)
            .districtName(DEFAULT_DISTRICT_NAME)
            .stateName(DEFAULT_STATE_NAME)
            .country(DEFAULT_COUNTRY)
            .isDeleted(DEFAULT_IS_DELETED)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isApproved(DEFAULT_IS_APPROVED)
            .seqOrder(DEFAULT_SEQ_ORDER)
            .isDataProcessed(DEFAULT_IS_DATA_PROCESSED)
            .jsonStoreId(DEFAULT_JSON_STORE_ID)
            .ctlgIsDeleted(DEFAULT_CTLG_IS_DELETED)
            .operation(DEFAULT_OPERATION)
            .ctlgSrno(DEFAULT_CTLG_SRNO);
        return locPincodeMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocPincodeMaster createUpdatedEntity(EntityManager em) {
        LocPincodeMaster locPincodeMaster = new LocPincodeMaster()
            .pincodeSrno(UPDATED_PINCODE_SRNO)
            .officeName(UPDATED_OFFICE_NAME)
            .pincode(UPDATED_PINCODE)
            .officeType(UPDATED_OFFICE_TYPE)
            .deliveryStatus(UPDATED_DELIVERY_STATUS)
            .divisionName(UPDATED_DIVISION_NAME)
            .regionName(UPDATED_REGION_NAME)
            .circleName(UPDATED_CIRCLE_NAME)
            .taluka(UPDATED_TALUKA)
            .districtName(UPDATED_DISTRICT_NAME)
            .stateName(UPDATED_STATE_NAME)
            .country(UPDATED_COUNTRY)
            .isDeleted(UPDATED_IS_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .operation(UPDATED_OPERATION)
            .ctlgSrno(UPDATED_CTLG_SRNO);
        return locPincodeMaster;
    }

    @BeforeEach
    public void initTest() {
        locPincodeMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocPincodeMaster() throws Exception {
        int databaseSizeBeforeCreate = locPincodeMasterRepository.findAll().size();
        // Create the LocPincodeMaster
        restLocPincodeMasterMockMvc.perform(post("/api/loc-pincode-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locPincodeMaster)))
            .andExpect(status().isCreated());

        // Validate the LocPincodeMaster in the database
        List<LocPincodeMaster> locPincodeMasterList = locPincodeMasterRepository.findAll();
        assertThat(locPincodeMasterList).hasSize(databaseSizeBeforeCreate + 1);
        LocPincodeMaster testLocPincodeMaster = locPincodeMasterList.get(locPincodeMasterList.size() - 1);
        assertThat(testLocPincodeMaster.getPincodeSrno()).isEqualTo(DEFAULT_PINCODE_SRNO);
        assertThat(testLocPincodeMaster.getOfficeName()).isEqualTo(DEFAULT_OFFICE_NAME);
        assertThat(testLocPincodeMaster.getPincode()).isEqualTo(DEFAULT_PINCODE);
        assertThat(testLocPincodeMaster.getOfficeType()).isEqualTo(DEFAULT_OFFICE_TYPE);
        assertThat(testLocPincodeMaster.getDeliveryStatus()).isEqualTo(DEFAULT_DELIVERY_STATUS);
        assertThat(testLocPincodeMaster.getDivisionName()).isEqualTo(DEFAULT_DIVISION_NAME);
        assertThat(testLocPincodeMaster.getRegionName()).isEqualTo(DEFAULT_REGION_NAME);
        assertThat(testLocPincodeMaster.getCircleName()).isEqualTo(DEFAULT_CIRCLE_NAME);
        assertThat(testLocPincodeMaster.getTaluka()).isEqualTo(DEFAULT_TALUKA);
        assertThat(testLocPincodeMaster.getDistrictName()).isEqualTo(DEFAULT_DISTRICT_NAME);
        assertThat(testLocPincodeMaster.getStateName()).isEqualTo(DEFAULT_STATE_NAME);
        assertThat(testLocPincodeMaster.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testLocPincodeMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocPincodeMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocPincodeMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocPincodeMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocPincodeMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testLocPincodeMaster.getIsApproved()).isEqualTo(DEFAULT_IS_APPROVED);
        assertThat(testLocPincodeMaster.getSeqOrder()).isEqualTo(DEFAULT_SEQ_ORDER);
        assertThat(testLocPincodeMaster.getIsDataProcessed()).isEqualTo(DEFAULT_IS_DATA_PROCESSED);
        assertThat(testLocPincodeMaster.getJsonStoreId()).isEqualTo(DEFAULT_JSON_STORE_ID);
        assertThat(testLocPincodeMaster.getCtlgIsDeleted()).isEqualTo(DEFAULT_CTLG_IS_DELETED);
        assertThat(testLocPincodeMaster.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testLocPincodeMaster.getCtlgSrno()).isEqualTo(DEFAULT_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void createLocPincodeMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locPincodeMasterRepository.findAll().size();

        // Create the LocPincodeMaster with an existing ID
        locPincodeMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocPincodeMasterMockMvc.perform(post("/api/loc-pincode-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locPincodeMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocPincodeMaster in the database
        List<LocPincodeMaster> locPincodeMasterList = locPincodeMasterRepository.findAll();
        assertThat(locPincodeMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCtlgSrnoIsRequired() throws Exception {
        int databaseSizeBeforeTest = locPincodeMasterRepository.findAll().size();
        // set the field null
        locPincodeMaster.setCtlgSrno(null);

        // Create the LocPincodeMaster, which fails.


        restLocPincodeMasterMockMvc.perform(post("/api/loc-pincode-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locPincodeMaster)))
            .andExpect(status().isBadRequest());

        List<LocPincodeMaster> locPincodeMasterList = locPincodeMasterRepository.findAll();
        assertThat(locPincodeMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMasters() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locPincodeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].pincodeSrno").value(hasItem(DEFAULT_PINCODE_SRNO)))
            .andExpect(jsonPath("$.[*].officeName").value(hasItem(DEFAULT_OFFICE_NAME)))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)))
            .andExpect(jsonPath("$.[*].officeType").value(hasItem(DEFAULT_OFFICE_TYPE)))
            .andExpect(jsonPath("$.[*].deliveryStatus").value(hasItem(DEFAULT_DELIVERY_STATUS)))
            .andExpect(jsonPath("$.[*].divisionName").value(hasItem(DEFAULT_DIVISION_NAME)))
            .andExpect(jsonPath("$.[*].regionName").value(hasItem(DEFAULT_REGION_NAME)))
            .andExpect(jsonPath("$.[*].circleName").value(hasItem(DEFAULT_CIRCLE_NAME)))
            .andExpect(jsonPath("$.[*].taluka").value(hasItem(DEFAULT_TALUKA)))
            .andExpect(jsonPath("$.[*].districtName").value(hasItem(DEFAULT_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID.intValue())))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)));
    }
    
    @Test
    @Transactional
    public void getLocPincodeMaster() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get the locPincodeMaster
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters/{id}", locPincodeMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locPincodeMaster.getId().intValue()))
            .andExpect(jsonPath("$.pincodeSrno").value(DEFAULT_PINCODE_SRNO))
            .andExpect(jsonPath("$.officeName").value(DEFAULT_OFFICE_NAME))
            .andExpect(jsonPath("$.pincode").value(DEFAULT_PINCODE))
            .andExpect(jsonPath("$.officeType").value(DEFAULT_OFFICE_TYPE))
            .andExpect(jsonPath("$.deliveryStatus").value(DEFAULT_DELIVERY_STATUS))
            .andExpect(jsonPath("$.divisionName").value(DEFAULT_DIVISION_NAME))
            .andExpect(jsonPath("$.regionName").value(DEFAULT_REGION_NAME))
            .andExpect(jsonPath("$.circleName").value(DEFAULT_CIRCLE_NAME))
            .andExpect(jsonPath("$.taluka").value(DEFAULT_TALUKA))
            .andExpect(jsonPath("$.districtName").value(DEFAULT_DISTRICT_NAME))
            .andExpect(jsonPath("$.stateName").value(DEFAULT_STATE_NAME))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isApproved").value(DEFAULT_IS_APPROVED))
            .andExpect(jsonPath("$.seqOrder").value(DEFAULT_SEQ_ORDER))
            .andExpect(jsonPath("$.isDataProcessed").value(DEFAULT_IS_DATA_PROCESSED))
            .andExpect(jsonPath("$.jsonStoreId").value(DEFAULT_JSON_STORE_ID.intValue()))
            .andExpect(jsonPath("$.ctlgIsDeleted").value(DEFAULT_CTLG_IS_DELETED))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION))
            .andExpect(jsonPath("$.ctlgSrno").value(DEFAULT_CTLG_SRNO));
    }


    @Test
    @Transactional
    public void getLocPincodeMastersByIdFiltering() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        Long id = locPincodeMaster.getId();

        defaultLocPincodeMasterShouldBeFound("id.equals=" + id);
        defaultLocPincodeMasterShouldNotBeFound("id.notEquals=" + id);

        defaultLocPincodeMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocPincodeMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultLocPincodeMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocPincodeMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno equals to DEFAULT_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.equals=" + DEFAULT_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno equals to UPDATED_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.equals=" + UPDATED_PINCODE_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno not equals to DEFAULT_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.notEquals=" + DEFAULT_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno not equals to UPDATED_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.notEquals=" + UPDATED_PINCODE_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno in DEFAULT_PINCODE_SRNO or UPDATED_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.in=" + DEFAULT_PINCODE_SRNO + "," + UPDATED_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno equals to UPDATED_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.in=" + UPDATED_PINCODE_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno is not null
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.specified=true");

        // Get all the locPincodeMasterList where pincodeSrno is null
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno is greater than or equal to DEFAULT_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.greaterThanOrEqual=" + DEFAULT_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno is greater than or equal to UPDATED_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.greaterThanOrEqual=" + UPDATED_PINCODE_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno is less than or equal to DEFAULT_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.lessThanOrEqual=" + DEFAULT_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno is less than or equal to SMALLER_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.lessThanOrEqual=" + SMALLER_PINCODE_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno is less than DEFAULT_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.lessThan=" + DEFAULT_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno is less than UPDATED_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.lessThan=" + UPDATED_PINCODE_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeSrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincodeSrno is greater than DEFAULT_PINCODE_SRNO
        defaultLocPincodeMasterShouldNotBeFound("pincodeSrno.greaterThan=" + DEFAULT_PINCODE_SRNO);

        // Get all the locPincodeMasterList where pincodeSrno is greater than SMALLER_PINCODE_SRNO
        defaultLocPincodeMasterShouldBeFound("pincodeSrno.greaterThan=" + SMALLER_PINCODE_SRNO);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeName equals to DEFAULT_OFFICE_NAME
        defaultLocPincodeMasterShouldBeFound("officeName.equals=" + DEFAULT_OFFICE_NAME);

        // Get all the locPincodeMasterList where officeName equals to UPDATED_OFFICE_NAME
        defaultLocPincodeMasterShouldNotBeFound("officeName.equals=" + UPDATED_OFFICE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeName not equals to DEFAULT_OFFICE_NAME
        defaultLocPincodeMasterShouldNotBeFound("officeName.notEquals=" + DEFAULT_OFFICE_NAME);

        // Get all the locPincodeMasterList where officeName not equals to UPDATED_OFFICE_NAME
        defaultLocPincodeMasterShouldBeFound("officeName.notEquals=" + UPDATED_OFFICE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeNameIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeName in DEFAULT_OFFICE_NAME or UPDATED_OFFICE_NAME
        defaultLocPincodeMasterShouldBeFound("officeName.in=" + DEFAULT_OFFICE_NAME + "," + UPDATED_OFFICE_NAME);

        // Get all the locPincodeMasterList where officeName equals to UPDATED_OFFICE_NAME
        defaultLocPincodeMasterShouldNotBeFound("officeName.in=" + UPDATED_OFFICE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeName is not null
        defaultLocPincodeMasterShouldBeFound("officeName.specified=true");

        // Get all the locPincodeMasterList where officeName is null
        defaultLocPincodeMasterShouldNotBeFound("officeName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeNameContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeName contains DEFAULT_OFFICE_NAME
        defaultLocPincodeMasterShouldBeFound("officeName.contains=" + DEFAULT_OFFICE_NAME);

        // Get all the locPincodeMasterList where officeName contains UPDATED_OFFICE_NAME
        defaultLocPincodeMasterShouldNotBeFound("officeName.contains=" + UPDATED_OFFICE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeNameNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeName does not contain DEFAULT_OFFICE_NAME
        defaultLocPincodeMasterShouldNotBeFound("officeName.doesNotContain=" + DEFAULT_OFFICE_NAME);

        // Get all the locPincodeMasterList where officeName does not contain UPDATED_OFFICE_NAME
        defaultLocPincodeMasterShouldBeFound("officeName.doesNotContain=" + UPDATED_OFFICE_NAME);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincode equals to DEFAULT_PINCODE
        defaultLocPincodeMasterShouldBeFound("pincode.equals=" + DEFAULT_PINCODE);

        // Get all the locPincodeMasterList where pincode equals to UPDATED_PINCODE
        defaultLocPincodeMasterShouldNotBeFound("pincode.equals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincode not equals to DEFAULT_PINCODE
        defaultLocPincodeMasterShouldNotBeFound("pincode.notEquals=" + DEFAULT_PINCODE);

        // Get all the locPincodeMasterList where pincode not equals to UPDATED_PINCODE
        defaultLocPincodeMasterShouldBeFound("pincode.notEquals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincode in DEFAULT_PINCODE or UPDATED_PINCODE
        defaultLocPincodeMasterShouldBeFound("pincode.in=" + DEFAULT_PINCODE + "," + UPDATED_PINCODE);

        // Get all the locPincodeMasterList where pincode equals to UPDATED_PINCODE
        defaultLocPincodeMasterShouldNotBeFound("pincode.in=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincode is not null
        defaultLocPincodeMasterShouldBeFound("pincode.specified=true");

        // Get all the locPincodeMasterList where pincode is null
        defaultLocPincodeMasterShouldNotBeFound("pincode.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincode contains DEFAULT_PINCODE
        defaultLocPincodeMasterShouldBeFound("pincode.contains=" + DEFAULT_PINCODE);

        // Get all the locPincodeMasterList where pincode contains UPDATED_PINCODE
        defaultLocPincodeMasterShouldNotBeFound("pincode.contains=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByPincodeNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where pincode does not contain DEFAULT_PINCODE
        defaultLocPincodeMasterShouldNotBeFound("pincode.doesNotContain=" + DEFAULT_PINCODE);

        // Get all the locPincodeMasterList where pincode does not contain UPDATED_PINCODE
        defaultLocPincodeMasterShouldBeFound("pincode.doesNotContain=" + UPDATED_PINCODE);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeType equals to DEFAULT_OFFICE_TYPE
        defaultLocPincodeMasterShouldBeFound("officeType.equals=" + DEFAULT_OFFICE_TYPE);

        // Get all the locPincodeMasterList where officeType equals to UPDATED_OFFICE_TYPE
        defaultLocPincodeMasterShouldNotBeFound("officeType.equals=" + UPDATED_OFFICE_TYPE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeType not equals to DEFAULT_OFFICE_TYPE
        defaultLocPincodeMasterShouldNotBeFound("officeType.notEquals=" + DEFAULT_OFFICE_TYPE);

        // Get all the locPincodeMasterList where officeType not equals to UPDATED_OFFICE_TYPE
        defaultLocPincodeMasterShouldBeFound("officeType.notEquals=" + UPDATED_OFFICE_TYPE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeType in DEFAULT_OFFICE_TYPE or UPDATED_OFFICE_TYPE
        defaultLocPincodeMasterShouldBeFound("officeType.in=" + DEFAULT_OFFICE_TYPE + "," + UPDATED_OFFICE_TYPE);

        // Get all the locPincodeMasterList where officeType equals to UPDATED_OFFICE_TYPE
        defaultLocPincodeMasterShouldNotBeFound("officeType.in=" + UPDATED_OFFICE_TYPE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeType is not null
        defaultLocPincodeMasterShouldBeFound("officeType.specified=true");

        // Get all the locPincodeMasterList where officeType is null
        defaultLocPincodeMasterShouldNotBeFound("officeType.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeTypeContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeType contains DEFAULT_OFFICE_TYPE
        defaultLocPincodeMasterShouldBeFound("officeType.contains=" + DEFAULT_OFFICE_TYPE);

        // Get all the locPincodeMasterList where officeType contains UPDATED_OFFICE_TYPE
        defaultLocPincodeMasterShouldNotBeFound("officeType.contains=" + UPDATED_OFFICE_TYPE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOfficeTypeNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where officeType does not contain DEFAULT_OFFICE_TYPE
        defaultLocPincodeMasterShouldNotBeFound("officeType.doesNotContain=" + DEFAULT_OFFICE_TYPE);

        // Get all the locPincodeMasterList where officeType does not contain UPDATED_OFFICE_TYPE
        defaultLocPincodeMasterShouldBeFound("officeType.doesNotContain=" + UPDATED_OFFICE_TYPE);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByDeliveryStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where deliveryStatus equals to DEFAULT_DELIVERY_STATUS
        defaultLocPincodeMasterShouldBeFound("deliveryStatus.equals=" + DEFAULT_DELIVERY_STATUS);

        // Get all the locPincodeMasterList where deliveryStatus equals to UPDATED_DELIVERY_STATUS
        defaultLocPincodeMasterShouldNotBeFound("deliveryStatus.equals=" + UPDATED_DELIVERY_STATUS);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDeliveryStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where deliveryStatus not equals to DEFAULT_DELIVERY_STATUS
        defaultLocPincodeMasterShouldNotBeFound("deliveryStatus.notEquals=" + DEFAULT_DELIVERY_STATUS);

        // Get all the locPincodeMasterList where deliveryStatus not equals to UPDATED_DELIVERY_STATUS
        defaultLocPincodeMasterShouldBeFound("deliveryStatus.notEquals=" + UPDATED_DELIVERY_STATUS);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDeliveryStatusIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where deliveryStatus in DEFAULT_DELIVERY_STATUS or UPDATED_DELIVERY_STATUS
        defaultLocPincodeMasterShouldBeFound("deliveryStatus.in=" + DEFAULT_DELIVERY_STATUS + "," + UPDATED_DELIVERY_STATUS);

        // Get all the locPincodeMasterList where deliveryStatus equals to UPDATED_DELIVERY_STATUS
        defaultLocPincodeMasterShouldNotBeFound("deliveryStatus.in=" + UPDATED_DELIVERY_STATUS);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDeliveryStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where deliveryStatus is not null
        defaultLocPincodeMasterShouldBeFound("deliveryStatus.specified=true");

        // Get all the locPincodeMasterList where deliveryStatus is null
        defaultLocPincodeMasterShouldNotBeFound("deliveryStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByDeliveryStatusContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where deliveryStatus contains DEFAULT_DELIVERY_STATUS
        defaultLocPincodeMasterShouldBeFound("deliveryStatus.contains=" + DEFAULT_DELIVERY_STATUS);

        // Get all the locPincodeMasterList where deliveryStatus contains UPDATED_DELIVERY_STATUS
        defaultLocPincodeMasterShouldNotBeFound("deliveryStatus.contains=" + UPDATED_DELIVERY_STATUS);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDeliveryStatusNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where deliveryStatus does not contain DEFAULT_DELIVERY_STATUS
        defaultLocPincodeMasterShouldNotBeFound("deliveryStatus.doesNotContain=" + DEFAULT_DELIVERY_STATUS);

        // Get all the locPincodeMasterList where deliveryStatus does not contain UPDATED_DELIVERY_STATUS
        defaultLocPincodeMasterShouldBeFound("deliveryStatus.doesNotContain=" + UPDATED_DELIVERY_STATUS);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByDivisionNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where divisionName equals to DEFAULT_DIVISION_NAME
        defaultLocPincodeMasterShouldBeFound("divisionName.equals=" + DEFAULT_DIVISION_NAME);

        // Get all the locPincodeMasterList where divisionName equals to UPDATED_DIVISION_NAME
        defaultLocPincodeMasterShouldNotBeFound("divisionName.equals=" + UPDATED_DIVISION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDivisionNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where divisionName not equals to DEFAULT_DIVISION_NAME
        defaultLocPincodeMasterShouldNotBeFound("divisionName.notEquals=" + DEFAULT_DIVISION_NAME);

        // Get all the locPincodeMasterList where divisionName not equals to UPDATED_DIVISION_NAME
        defaultLocPincodeMasterShouldBeFound("divisionName.notEquals=" + UPDATED_DIVISION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDivisionNameIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where divisionName in DEFAULT_DIVISION_NAME or UPDATED_DIVISION_NAME
        defaultLocPincodeMasterShouldBeFound("divisionName.in=" + DEFAULT_DIVISION_NAME + "," + UPDATED_DIVISION_NAME);

        // Get all the locPincodeMasterList where divisionName equals to UPDATED_DIVISION_NAME
        defaultLocPincodeMasterShouldNotBeFound("divisionName.in=" + UPDATED_DIVISION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDivisionNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where divisionName is not null
        defaultLocPincodeMasterShouldBeFound("divisionName.specified=true");

        // Get all the locPincodeMasterList where divisionName is null
        defaultLocPincodeMasterShouldNotBeFound("divisionName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByDivisionNameContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where divisionName contains DEFAULT_DIVISION_NAME
        defaultLocPincodeMasterShouldBeFound("divisionName.contains=" + DEFAULT_DIVISION_NAME);

        // Get all the locPincodeMasterList where divisionName contains UPDATED_DIVISION_NAME
        defaultLocPincodeMasterShouldNotBeFound("divisionName.contains=" + UPDATED_DIVISION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDivisionNameNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where divisionName does not contain DEFAULT_DIVISION_NAME
        defaultLocPincodeMasterShouldNotBeFound("divisionName.doesNotContain=" + DEFAULT_DIVISION_NAME);

        // Get all the locPincodeMasterList where divisionName does not contain UPDATED_DIVISION_NAME
        defaultLocPincodeMasterShouldBeFound("divisionName.doesNotContain=" + UPDATED_DIVISION_NAME);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByRegionNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where regionName equals to DEFAULT_REGION_NAME
        defaultLocPincodeMasterShouldBeFound("regionName.equals=" + DEFAULT_REGION_NAME);

        // Get all the locPincodeMasterList where regionName equals to UPDATED_REGION_NAME
        defaultLocPincodeMasterShouldNotBeFound("regionName.equals=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByRegionNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where regionName not equals to DEFAULT_REGION_NAME
        defaultLocPincodeMasterShouldNotBeFound("regionName.notEquals=" + DEFAULT_REGION_NAME);

        // Get all the locPincodeMasterList where regionName not equals to UPDATED_REGION_NAME
        defaultLocPincodeMasterShouldBeFound("regionName.notEquals=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByRegionNameIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where regionName in DEFAULT_REGION_NAME or UPDATED_REGION_NAME
        defaultLocPincodeMasterShouldBeFound("regionName.in=" + DEFAULT_REGION_NAME + "," + UPDATED_REGION_NAME);

        // Get all the locPincodeMasterList where regionName equals to UPDATED_REGION_NAME
        defaultLocPincodeMasterShouldNotBeFound("regionName.in=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByRegionNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where regionName is not null
        defaultLocPincodeMasterShouldBeFound("regionName.specified=true");

        // Get all the locPincodeMasterList where regionName is null
        defaultLocPincodeMasterShouldNotBeFound("regionName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByRegionNameContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where regionName contains DEFAULT_REGION_NAME
        defaultLocPincodeMasterShouldBeFound("regionName.contains=" + DEFAULT_REGION_NAME);

        // Get all the locPincodeMasterList where regionName contains UPDATED_REGION_NAME
        defaultLocPincodeMasterShouldNotBeFound("regionName.contains=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByRegionNameNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where regionName does not contain DEFAULT_REGION_NAME
        defaultLocPincodeMasterShouldNotBeFound("regionName.doesNotContain=" + DEFAULT_REGION_NAME);

        // Get all the locPincodeMasterList where regionName does not contain UPDATED_REGION_NAME
        defaultLocPincodeMasterShouldBeFound("regionName.doesNotContain=" + UPDATED_REGION_NAME);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByCircleNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where circleName equals to DEFAULT_CIRCLE_NAME
        defaultLocPincodeMasterShouldBeFound("circleName.equals=" + DEFAULT_CIRCLE_NAME);

        // Get all the locPincodeMasterList where circleName equals to UPDATED_CIRCLE_NAME
        defaultLocPincodeMasterShouldNotBeFound("circleName.equals=" + UPDATED_CIRCLE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCircleNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where circleName not equals to DEFAULT_CIRCLE_NAME
        defaultLocPincodeMasterShouldNotBeFound("circleName.notEquals=" + DEFAULT_CIRCLE_NAME);

        // Get all the locPincodeMasterList where circleName not equals to UPDATED_CIRCLE_NAME
        defaultLocPincodeMasterShouldBeFound("circleName.notEquals=" + UPDATED_CIRCLE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCircleNameIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where circleName in DEFAULT_CIRCLE_NAME or UPDATED_CIRCLE_NAME
        defaultLocPincodeMasterShouldBeFound("circleName.in=" + DEFAULT_CIRCLE_NAME + "," + UPDATED_CIRCLE_NAME);

        // Get all the locPincodeMasterList where circleName equals to UPDATED_CIRCLE_NAME
        defaultLocPincodeMasterShouldNotBeFound("circleName.in=" + UPDATED_CIRCLE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCircleNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where circleName is not null
        defaultLocPincodeMasterShouldBeFound("circleName.specified=true");

        // Get all the locPincodeMasterList where circleName is null
        defaultLocPincodeMasterShouldNotBeFound("circleName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByCircleNameContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where circleName contains DEFAULT_CIRCLE_NAME
        defaultLocPincodeMasterShouldBeFound("circleName.contains=" + DEFAULT_CIRCLE_NAME);

        // Get all the locPincodeMasterList where circleName contains UPDATED_CIRCLE_NAME
        defaultLocPincodeMasterShouldNotBeFound("circleName.contains=" + UPDATED_CIRCLE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCircleNameNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where circleName does not contain DEFAULT_CIRCLE_NAME
        defaultLocPincodeMasterShouldNotBeFound("circleName.doesNotContain=" + DEFAULT_CIRCLE_NAME);

        // Get all the locPincodeMasterList where circleName does not contain UPDATED_CIRCLE_NAME
        defaultLocPincodeMasterShouldBeFound("circleName.doesNotContain=" + UPDATED_CIRCLE_NAME);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByTalukaIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where taluka equals to DEFAULT_TALUKA
        defaultLocPincodeMasterShouldBeFound("taluka.equals=" + DEFAULT_TALUKA);

        // Get all the locPincodeMasterList where taluka equals to UPDATED_TALUKA
        defaultLocPincodeMasterShouldNotBeFound("taluka.equals=" + UPDATED_TALUKA);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByTalukaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where taluka not equals to DEFAULT_TALUKA
        defaultLocPincodeMasterShouldNotBeFound("taluka.notEquals=" + DEFAULT_TALUKA);

        // Get all the locPincodeMasterList where taluka not equals to UPDATED_TALUKA
        defaultLocPincodeMasterShouldBeFound("taluka.notEquals=" + UPDATED_TALUKA);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByTalukaIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where taluka in DEFAULT_TALUKA or UPDATED_TALUKA
        defaultLocPincodeMasterShouldBeFound("taluka.in=" + DEFAULT_TALUKA + "," + UPDATED_TALUKA);

        // Get all the locPincodeMasterList where taluka equals to UPDATED_TALUKA
        defaultLocPincodeMasterShouldNotBeFound("taluka.in=" + UPDATED_TALUKA);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByTalukaIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where taluka is not null
        defaultLocPincodeMasterShouldBeFound("taluka.specified=true");

        // Get all the locPincodeMasterList where taluka is null
        defaultLocPincodeMasterShouldNotBeFound("taluka.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByTalukaContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where taluka contains DEFAULT_TALUKA
        defaultLocPincodeMasterShouldBeFound("taluka.contains=" + DEFAULT_TALUKA);

        // Get all the locPincodeMasterList where taluka contains UPDATED_TALUKA
        defaultLocPincodeMasterShouldNotBeFound("taluka.contains=" + UPDATED_TALUKA);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByTalukaNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where taluka does not contain DEFAULT_TALUKA
        defaultLocPincodeMasterShouldNotBeFound("taluka.doesNotContain=" + DEFAULT_TALUKA);

        // Get all the locPincodeMasterList where taluka does not contain UPDATED_TALUKA
        defaultLocPincodeMasterShouldBeFound("taluka.doesNotContain=" + UPDATED_TALUKA);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where districtName equals to DEFAULT_DISTRICT_NAME
        defaultLocPincodeMasterShouldBeFound("districtName.equals=" + DEFAULT_DISTRICT_NAME);

        // Get all the locPincodeMasterList where districtName equals to UPDATED_DISTRICT_NAME
        defaultLocPincodeMasterShouldNotBeFound("districtName.equals=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where districtName not equals to DEFAULT_DISTRICT_NAME
        defaultLocPincodeMasterShouldNotBeFound("districtName.notEquals=" + DEFAULT_DISTRICT_NAME);

        // Get all the locPincodeMasterList where districtName not equals to UPDATED_DISTRICT_NAME
        defaultLocPincodeMasterShouldBeFound("districtName.notEquals=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictNameIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where districtName in DEFAULT_DISTRICT_NAME or UPDATED_DISTRICT_NAME
        defaultLocPincodeMasterShouldBeFound("districtName.in=" + DEFAULT_DISTRICT_NAME + "," + UPDATED_DISTRICT_NAME);

        // Get all the locPincodeMasterList where districtName equals to UPDATED_DISTRICT_NAME
        defaultLocPincodeMasterShouldNotBeFound("districtName.in=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where districtName is not null
        defaultLocPincodeMasterShouldBeFound("districtName.specified=true");

        // Get all the locPincodeMasterList where districtName is null
        defaultLocPincodeMasterShouldNotBeFound("districtName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictNameContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where districtName contains DEFAULT_DISTRICT_NAME
        defaultLocPincodeMasterShouldBeFound("districtName.contains=" + DEFAULT_DISTRICT_NAME);

        // Get all the locPincodeMasterList where districtName contains UPDATED_DISTRICT_NAME
        defaultLocPincodeMasterShouldNotBeFound("districtName.contains=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictNameNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where districtName does not contain DEFAULT_DISTRICT_NAME
        defaultLocPincodeMasterShouldNotBeFound("districtName.doesNotContain=" + DEFAULT_DISTRICT_NAME);

        // Get all the locPincodeMasterList where districtName does not contain UPDATED_DISTRICT_NAME
        defaultLocPincodeMasterShouldBeFound("districtName.doesNotContain=" + UPDATED_DISTRICT_NAME);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByStateNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where stateName equals to DEFAULT_STATE_NAME
        defaultLocPincodeMasterShouldBeFound("stateName.equals=" + DEFAULT_STATE_NAME);

        // Get all the locPincodeMasterList where stateName equals to UPDATED_STATE_NAME
        defaultLocPincodeMasterShouldNotBeFound("stateName.equals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByStateNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where stateName not equals to DEFAULT_STATE_NAME
        defaultLocPincodeMasterShouldNotBeFound("stateName.notEquals=" + DEFAULT_STATE_NAME);

        // Get all the locPincodeMasterList where stateName not equals to UPDATED_STATE_NAME
        defaultLocPincodeMasterShouldBeFound("stateName.notEquals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByStateNameIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where stateName in DEFAULT_STATE_NAME or UPDATED_STATE_NAME
        defaultLocPincodeMasterShouldBeFound("stateName.in=" + DEFAULT_STATE_NAME + "," + UPDATED_STATE_NAME);

        // Get all the locPincodeMasterList where stateName equals to UPDATED_STATE_NAME
        defaultLocPincodeMasterShouldNotBeFound("stateName.in=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByStateNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where stateName is not null
        defaultLocPincodeMasterShouldBeFound("stateName.specified=true");

        // Get all the locPincodeMasterList where stateName is null
        defaultLocPincodeMasterShouldNotBeFound("stateName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByStateNameContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where stateName contains DEFAULT_STATE_NAME
        defaultLocPincodeMasterShouldBeFound("stateName.contains=" + DEFAULT_STATE_NAME);

        // Get all the locPincodeMasterList where stateName contains UPDATED_STATE_NAME
        defaultLocPincodeMasterShouldNotBeFound("stateName.contains=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByStateNameNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where stateName does not contain DEFAULT_STATE_NAME
        defaultLocPincodeMasterShouldNotBeFound("stateName.doesNotContain=" + DEFAULT_STATE_NAME);

        // Get all the locPincodeMasterList where stateName does not contain UPDATED_STATE_NAME
        defaultLocPincodeMasterShouldBeFound("stateName.doesNotContain=" + UPDATED_STATE_NAME);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where country equals to DEFAULT_COUNTRY
        defaultLocPincodeMasterShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the locPincodeMasterList where country equals to UPDATED_COUNTRY
        defaultLocPincodeMasterShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCountryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where country not equals to DEFAULT_COUNTRY
        defaultLocPincodeMasterShouldNotBeFound("country.notEquals=" + DEFAULT_COUNTRY);

        // Get all the locPincodeMasterList where country not equals to UPDATED_COUNTRY
        defaultLocPincodeMasterShouldBeFound("country.notEquals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultLocPincodeMasterShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the locPincodeMasterList where country equals to UPDATED_COUNTRY
        defaultLocPincodeMasterShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where country is not null
        defaultLocPincodeMasterShouldBeFound("country.specified=true");

        // Get all the locPincodeMasterList where country is null
        defaultLocPincodeMasterShouldNotBeFound("country.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByCountryContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where country contains DEFAULT_COUNTRY
        defaultLocPincodeMasterShouldBeFound("country.contains=" + DEFAULT_COUNTRY);

        // Get all the locPincodeMasterList where country contains UPDATED_COUNTRY
        defaultLocPincodeMasterShouldNotBeFound("country.contains=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCountryNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where country does not contain DEFAULT_COUNTRY
        defaultLocPincodeMasterShouldNotBeFound("country.doesNotContain=" + DEFAULT_COUNTRY);

        // Get all the locPincodeMasterList where country does not contain UPDATED_COUNTRY
        defaultLocPincodeMasterShouldBeFound("country.doesNotContain=" + UPDATED_COUNTRY);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locPincodeMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locPincodeMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locPincodeMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDeleted is not null
        defaultLocPincodeMasterShouldBeFound("isDeleted.specified=true");

        // Get all the locPincodeMasterList where isDeleted is null
        defaultLocPincodeMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locPincodeMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locPincodeMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocPincodeMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locPincodeMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocPincodeMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocPincodeMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locPincodeMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocPincodeMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocPincodeMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locPincodeMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocPincodeMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdDate is not null
        defaultLocPincodeMasterShouldBeFound("createdDate.specified=true");

        // Get all the locPincodeMasterList where createdDate is null
        defaultLocPincodeMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocPincodeMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locPincodeMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocPincodeMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocPincodeMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locPincodeMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocPincodeMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocPincodeMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locPincodeMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocPincodeMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdBy is not null
        defaultLocPincodeMasterShouldBeFound("createdBy.specified=true");

        // Get all the locPincodeMasterList where createdBy is null
        defaultLocPincodeMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultLocPincodeMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locPincodeMasterList where createdBy contains UPDATED_CREATED_BY
        defaultLocPincodeMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocPincodeMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locPincodeMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocPincodeMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocPincodeMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locPincodeMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locPincodeMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocPincodeMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocPincodeMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locPincodeMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedDate is not null
        defaultLocPincodeMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locPincodeMasterList where lastModifiedDate is null
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locPincodeMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locPincodeMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locPincodeMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedBy is not null
        defaultLocPincodeMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locPincodeMasterList where lastModifiedBy is null
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locPincodeMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locPincodeMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocPincodeMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsApprovedIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isApproved equals to DEFAULT_IS_APPROVED
        defaultLocPincodeMasterShouldBeFound("isApproved.equals=" + DEFAULT_IS_APPROVED);

        // Get all the locPincodeMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocPincodeMasterShouldNotBeFound("isApproved.equals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsApprovedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isApproved not equals to DEFAULT_IS_APPROVED
        defaultLocPincodeMasterShouldNotBeFound("isApproved.notEquals=" + DEFAULT_IS_APPROVED);

        // Get all the locPincodeMasterList where isApproved not equals to UPDATED_IS_APPROVED
        defaultLocPincodeMasterShouldBeFound("isApproved.notEquals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsApprovedIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isApproved in DEFAULT_IS_APPROVED or UPDATED_IS_APPROVED
        defaultLocPincodeMasterShouldBeFound("isApproved.in=" + DEFAULT_IS_APPROVED + "," + UPDATED_IS_APPROVED);

        // Get all the locPincodeMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocPincodeMasterShouldNotBeFound("isApproved.in=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsApprovedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isApproved is not null
        defaultLocPincodeMasterShouldBeFound("isApproved.specified=true");

        // Get all the locPincodeMasterList where isApproved is null
        defaultLocPincodeMasterShouldNotBeFound("isApproved.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByIsApprovedContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isApproved contains DEFAULT_IS_APPROVED
        defaultLocPincodeMasterShouldBeFound("isApproved.contains=" + DEFAULT_IS_APPROVED);

        // Get all the locPincodeMasterList where isApproved contains UPDATED_IS_APPROVED
        defaultLocPincodeMasterShouldNotBeFound("isApproved.contains=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsApprovedNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isApproved does not contain DEFAULT_IS_APPROVED
        defaultLocPincodeMasterShouldNotBeFound("isApproved.doesNotContain=" + DEFAULT_IS_APPROVED);

        // Get all the locPincodeMasterList where isApproved does not contain UPDATED_IS_APPROVED
        defaultLocPincodeMasterShouldBeFound("isApproved.doesNotContain=" + UPDATED_IS_APPROVED);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder equals to DEFAULT_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.equals=" + DEFAULT_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.equals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder not equals to DEFAULT_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.notEquals=" + DEFAULT_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder not equals to UPDATED_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.notEquals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder in DEFAULT_SEQ_ORDER or UPDATED_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.in=" + DEFAULT_SEQ_ORDER + "," + UPDATED_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.in=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder is not null
        defaultLocPincodeMasterShouldBeFound("seqOrder.specified=true");

        // Get all the locPincodeMasterList where seqOrder is null
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder is greater than or equal to DEFAULT_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.greaterThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder is greater than or equal to UPDATED_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.greaterThanOrEqual=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder is less than or equal to DEFAULT_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.lessThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder is less than or equal to SMALLER_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.lessThanOrEqual=" + SMALLER_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder is less than DEFAULT_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.lessThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder is less than UPDATED_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.lessThan=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersBySeqOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where seqOrder is greater than DEFAULT_SEQ_ORDER
        defaultLocPincodeMasterShouldNotBeFound("seqOrder.greaterThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locPincodeMasterList where seqOrder is greater than SMALLER_SEQ_ORDER
        defaultLocPincodeMasterShouldBeFound("seqOrder.greaterThan=" + SMALLER_SEQ_ORDER);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDataProcessedIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDataProcessed equals to DEFAULT_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldBeFound("isDataProcessed.equals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the locPincodeMasterList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldNotBeFound("isDataProcessed.equals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDataProcessedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDataProcessed not equals to DEFAULT_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldNotBeFound("isDataProcessed.notEquals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the locPincodeMasterList where isDataProcessed not equals to UPDATED_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldBeFound("isDataProcessed.notEquals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDataProcessedIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDataProcessed in DEFAULT_IS_DATA_PROCESSED or UPDATED_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldBeFound("isDataProcessed.in=" + DEFAULT_IS_DATA_PROCESSED + "," + UPDATED_IS_DATA_PROCESSED);

        // Get all the locPincodeMasterList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldNotBeFound("isDataProcessed.in=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDataProcessedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDataProcessed is not null
        defaultLocPincodeMasterShouldBeFound("isDataProcessed.specified=true");

        // Get all the locPincodeMasterList where isDataProcessed is null
        defaultLocPincodeMasterShouldNotBeFound("isDataProcessed.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDataProcessedContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDataProcessed contains DEFAULT_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldBeFound("isDataProcessed.contains=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the locPincodeMasterList where isDataProcessed contains UPDATED_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldNotBeFound("isDataProcessed.contains=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByIsDataProcessedNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where isDataProcessed does not contain DEFAULT_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldNotBeFound("isDataProcessed.doesNotContain=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the locPincodeMasterList where isDataProcessed does not contain UPDATED_IS_DATA_PROCESSED
        defaultLocPincodeMasterShouldBeFound("isDataProcessed.doesNotContain=" + UPDATED_IS_DATA_PROCESSED);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId equals to DEFAULT_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.equals=" + DEFAULT_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.equals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId not equals to DEFAULT_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.notEquals=" + DEFAULT_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId not equals to UPDATED_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.notEquals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId in DEFAULT_JSON_STORE_ID or UPDATED_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.in=" + DEFAULT_JSON_STORE_ID + "," + UPDATED_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.in=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId is not null
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.specified=true");

        // Get all the locPincodeMasterList where jsonStoreId is null
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId is greater than or equal to DEFAULT_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.greaterThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId is greater than or equal to UPDATED_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.greaterThanOrEqual=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId is less than or equal to DEFAULT_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.lessThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId is less than or equal to SMALLER_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.lessThanOrEqual=" + SMALLER_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId is less than DEFAULT_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.lessThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId is less than UPDATED_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.lessThan=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByJsonStoreIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where jsonStoreId is greater than DEFAULT_JSON_STORE_ID
        defaultLocPincodeMasterShouldNotBeFound("jsonStoreId.greaterThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the locPincodeMasterList where jsonStoreId is greater than SMALLER_JSON_STORE_ID
        defaultLocPincodeMasterShouldBeFound("jsonStoreId.greaterThan=" + SMALLER_JSON_STORE_ID);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgIsDeleted equals to DEFAULT_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("ctlgIsDeleted.equals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the locPincodeMasterList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("ctlgIsDeleted.equals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgIsDeleted not equals to DEFAULT_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("ctlgIsDeleted.notEquals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the locPincodeMasterList where ctlgIsDeleted not equals to UPDATED_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("ctlgIsDeleted.notEquals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgIsDeleted in DEFAULT_CTLG_IS_DELETED or UPDATED_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("ctlgIsDeleted.in=" + DEFAULT_CTLG_IS_DELETED + "," + UPDATED_CTLG_IS_DELETED);

        // Get all the locPincodeMasterList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("ctlgIsDeleted.in=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgIsDeleted is not null
        defaultLocPincodeMasterShouldBeFound("ctlgIsDeleted.specified=true");

        // Get all the locPincodeMasterList where ctlgIsDeleted is null
        defaultLocPincodeMasterShouldNotBeFound("ctlgIsDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgIsDeleted contains DEFAULT_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("ctlgIsDeleted.contains=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the locPincodeMasterList where ctlgIsDeleted contains UPDATED_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("ctlgIsDeleted.contains=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgIsDeleted does not contain DEFAULT_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldNotBeFound("ctlgIsDeleted.doesNotContain=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the locPincodeMasterList where ctlgIsDeleted does not contain UPDATED_CTLG_IS_DELETED
        defaultLocPincodeMasterShouldBeFound("ctlgIsDeleted.doesNotContain=" + UPDATED_CTLG_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where operation equals to DEFAULT_OPERATION
        defaultLocPincodeMasterShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the locPincodeMasterList where operation equals to UPDATED_OPERATION
        defaultLocPincodeMasterShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where operation not equals to DEFAULT_OPERATION
        defaultLocPincodeMasterShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the locPincodeMasterList where operation not equals to UPDATED_OPERATION
        defaultLocPincodeMasterShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultLocPincodeMasterShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the locPincodeMasterList where operation equals to UPDATED_OPERATION
        defaultLocPincodeMasterShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where operation is not null
        defaultLocPincodeMasterShouldBeFound("operation.specified=true");

        // Get all the locPincodeMasterList where operation is null
        defaultLocPincodeMasterShouldNotBeFound("operation.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocPincodeMastersByOperationContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where operation contains DEFAULT_OPERATION
        defaultLocPincodeMasterShouldBeFound("operation.contains=" + DEFAULT_OPERATION);

        // Get all the locPincodeMasterList where operation contains UPDATED_OPERATION
        defaultLocPincodeMasterShouldNotBeFound("operation.contains=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByOperationNotContainsSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where operation does not contain DEFAULT_OPERATION
        defaultLocPincodeMasterShouldNotBeFound("operation.doesNotContain=" + DEFAULT_OPERATION);

        // Get all the locPincodeMasterList where operation does not contain UPDATED_OPERATION
        defaultLocPincodeMasterShouldBeFound("operation.doesNotContain=" + UPDATED_OPERATION);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno equals to DEFAULT_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.equals=" + DEFAULT_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.equals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno not equals to DEFAULT_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.notEquals=" + DEFAULT_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno not equals to UPDATED_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.notEquals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsInShouldWork() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno in DEFAULT_CTLG_SRNO or UPDATED_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.in=" + DEFAULT_CTLG_SRNO + "," + UPDATED_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.in=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno is not null
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.specified=true");

        // Get all the locPincodeMasterList where ctlgSrno is null
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno is greater than or equal to DEFAULT_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.greaterThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno is greater than or equal to UPDATED_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.greaterThanOrEqual=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno is less than or equal to DEFAULT_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.lessThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno is less than or equal to SMALLER_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.lessThanOrEqual=" + SMALLER_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno is less than DEFAULT_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.lessThan=" + DEFAULT_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno is less than UPDATED_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.lessThan=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocPincodeMastersByCtlgSrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);

        // Get all the locPincodeMasterList where ctlgSrno is greater than DEFAULT_CTLG_SRNO
        defaultLocPincodeMasterShouldNotBeFound("ctlgSrno.greaterThan=" + DEFAULT_CTLG_SRNO);

        // Get all the locPincodeMasterList where ctlgSrno is greater than SMALLER_CTLG_SRNO
        defaultLocPincodeMasterShouldBeFound("ctlgSrno.greaterThan=" + SMALLER_CTLG_SRNO);
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByCompanyAddressMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        CompanyAddressMaster companyAddressMaster = CompanyAddressMasterResourceIT.createEntity(em);
        em.persist(companyAddressMaster);
        em.flush();
        locPincodeMaster.addCompanyAddressMaster(companyAddressMaster);
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        Long companyAddressMasterId = companyAddressMaster.getId();

        // Get all the locPincodeMasterList where companyAddressMaster equals to companyAddressMasterId
        defaultLocPincodeMasterShouldBeFound("companyAddressMasterId.equals=" + companyAddressMasterId);

        // Get all the locPincodeMasterList where companyAddressMaster equals to companyAddressMasterId + 1
        defaultLocPincodeMasterShouldNotBeFound("companyAddressMasterId.equals=" + (companyAddressMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByOnesourceCompanyMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        OnesourceCompanyMaster onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(onesourceCompanyMaster);
        em.flush();
        locPincodeMaster.addOnesourceCompanyMaster(onesourceCompanyMaster);
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        Long onesourceCompanyMasterId = onesourceCompanyMaster.getId();

        // Get all the locPincodeMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId
        defaultLocPincodeMasterShouldBeFound("onesourceCompanyMasterId.equals=" + onesourceCompanyMasterId);

        // Get all the locPincodeMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId + 1
        defaultLocPincodeMasterShouldNotBeFound("onesourceCompanyMasterId.equals=" + (onesourceCompanyMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByStateIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        LocStateMaster stateId = LocStateMasterResourceIT.createEntity(em);
        em.persist(stateId);
        em.flush();
        locPincodeMaster.setStateId(stateId);
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        Long stateIdId = stateId.getId();

        // Get all the locPincodeMasterList where stateId equals to stateIdId
        defaultLocPincodeMasterShouldBeFound("stateIdId.equals=" + stateIdId);

        // Get all the locPincodeMasterList where stateId equals to stateIdId + 1
        defaultLocPincodeMasterShouldNotBeFound("stateIdId.equals=" + (stateIdId + 1));
    }


    @Test
    @Transactional
    public void getAllLocPincodeMastersByDistrictIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        LocDistrictMaster districtId = LocDistrictMasterResourceIT.createEntity(em);
        em.persist(districtId);
        em.flush();
        locPincodeMaster.setDistrictId(districtId);
        locPincodeMasterRepository.saveAndFlush(locPincodeMaster);
        Long districtIdId = districtId.getId();

        // Get all the locPincodeMasterList where districtId equals to districtIdId
        defaultLocPincodeMasterShouldBeFound("districtIdId.equals=" + districtIdId);

        // Get all the locPincodeMasterList where districtId equals to districtIdId + 1
        defaultLocPincodeMasterShouldNotBeFound("districtIdId.equals=" + (districtIdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocPincodeMasterShouldBeFound(String filter) throws Exception {
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locPincodeMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].pincodeSrno").value(hasItem(DEFAULT_PINCODE_SRNO)))
            .andExpect(jsonPath("$.[*].officeName").value(hasItem(DEFAULT_OFFICE_NAME)))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)))
            .andExpect(jsonPath("$.[*].officeType").value(hasItem(DEFAULT_OFFICE_TYPE)))
            .andExpect(jsonPath("$.[*].deliveryStatus").value(hasItem(DEFAULT_DELIVERY_STATUS)))
            .andExpect(jsonPath("$.[*].divisionName").value(hasItem(DEFAULT_DIVISION_NAME)))
            .andExpect(jsonPath("$.[*].regionName").value(hasItem(DEFAULT_REGION_NAME)))
            .andExpect(jsonPath("$.[*].circleName").value(hasItem(DEFAULT_CIRCLE_NAME)))
            .andExpect(jsonPath("$.[*].taluka").value(hasItem(DEFAULT_TALUKA)))
            .andExpect(jsonPath("$.[*].districtName").value(hasItem(DEFAULT_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID.intValue())))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)));

        // Check, that the count call also returns 1
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocPincodeMasterShouldNotBeFound(String filter) throws Exception {
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocPincodeMaster() throws Exception {
        // Get the locPincodeMaster
        restLocPincodeMasterMockMvc.perform(get("/api/loc-pincode-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocPincodeMaster() throws Exception {
        // Initialize the database
        locPincodeMasterService.save(locPincodeMaster);

        int databaseSizeBeforeUpdate = locPincodeMasterRepository.findAll().size();

        // Update the locPincodeMaster
        LocPincodeMaster updatedLocPincodeMaster = locPincodeMasterRepository.findById(locPincodeMaster.getId()).get();
        // Disconnect from session so that the updates on updatedLocPincodeMaster are not directly saved in db
        em.detach(updatedLocPincodeMaster);
        updatedLocPincodeMaster
            .pincodeSrno(UPDATED_PINCODE_SRNO)
            .officeName(UPDATED_OFFICE_NAME)
            .pincode(UPDATED_PINCODE)
            .officeType(UPDATED_OFFICE_TYPE)
            .deliveryStatus(UPDATED_DELIVERY_STATUS)
            .divisionName(UPDATED_DIVISION_NAME)
            .regionName(UPDATED_REGION_NAME)
            .circleName(UPDATED_CIRCLE_NAME)
            .taluka(UPDATED_TALUKA)
            .districtName(UPDATED_DISTRICT_NAME)
            .stateName(UPDATED_STATE_NAME)
            .country(UPDATED_COUNTRY)
            .isDeleted(UPDATED_IS_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .operation(UPDATED_OPERATION)
            .ctlgSrno(UPDATED_CTLG_SRNO);

        restLocPincodeMasterMockMvc.perform(put("/api/loc-pincode-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocPincodeMaster)))
            .andExpect(status().isOk());

        // Validate the LocPincodeMaster in the database
        List<LocPincodeMaster> locPincodeMasterList = locPincodeMasterRepository.findAll();
        assertThat(locPincodeMasterList).hasSize(databaseSizeBeforeUpdate);
        LocPincodeMaster testLocPincodeMaster = locPincodeMasterList.get(locPincodeMasterList.size() - 1);
        assertThat(testLocPincodeMaster.getPincodeSrno()).isEqualTo(UPDATED_PINCODE_SRNO);
        assertThat(testLocPincodeMaster.getOfficeName()).isEqualTo(UPDATED_OFFICE_NAME);
        assertThat(testLocPincodeMaster.getPincode()).isEqualTo(UPDATED_PINCODE);
        assertThat(testLocPincodeMaster.getOfficeType()).isEqualTo(UPDATED_OFFICE_TYPE);
        assertThat(testLocPincodeMaster.getDeliveryStatus()).isEqualTo(UPDATED_DELIVERY_STATUS);
        assertThat(testLocPincodeMaster.getDivisionName()).isEqualTo(UPDATED_DIVISION_NAME);
        assertThat(testLocPincodeMaster.getRegionName()).isEqualTo(UPDATED_REGION_NAME);
        assertThat(testLocPincodeMaster.getCircleName()).isEqualTo(UPDATED_CIRCLE_NAME);
        assertThat(testLocPincodeMaster.getTaluka()).isEqualTo(UPDATED_TALUKA);
        assertThat(testLocPincodeMaster.getDistrictName()).isEqualTo(UPDATED_DISTRICT_NAME);
        assertThat(testLocPincodeMaster.getStateName()).isEqualTo(UPDATED_STATE_NAME);
        assertThat(testLocPincodeMaster.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testLocPincodeMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocPincodeMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocPincodeMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocPincodeMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocPincodeMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testLocPincodeMaster.getIsApproved()).isEqualTo(UPDATED_IS_APPROVED);
        assertThat(testLocPincodeMaster.getSeqOrder()).isEqualTo(UPDATED_SEQ_ORDER);
        assertThat(testLocPincodeMaster.getIsDataProcessed()).isEqualTo(UPDATED_IS_DATA_PROCESSED);
        assertThat(testLocPincodeMaster.getJsonStoreId()).isEqualTo(UPDATED_JSON_STORE_ID);
        assertThat(testLocPincodeMaster.getCtlgIsDeleted()).isEqualTo(UPDATED_CTLG_IS_DELETED);
        assertThat(testLocPincodeMaster.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testLocPincodeMaster.getCtlgSrno()).isEqualTo(UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void updateNonExistingLocPincodeMaster() throws Exception {
        int databaseSizeBeforeUpdate = locPincodeMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocPincodeMasterMockMvc.perform(put("/api/loc-pincode-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locPincodeMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocPincodeMaster in the database
        List<LocPincodeMaster> locPincodeMasterList = locPincodeMasterRepository.findAll();
        assertThat(locPincodeMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocPincodeMaster() throws Exception {
        // Initialize the database
        locPincodeMasterService.save(locPincodeMaster);

        int databaseSizeBeforeDelete = locPincodeMasterRepository.findAll().size();

        // Delete the locPincodeMaster
        restLocPincodeMasterMockMvc.perform(delete("/api/loc-pincode-masters/{id}", locPincodeMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocPincodeMaster> locPincodeMasterList = locPincodeMasterRepository.findAll();
        assertThat(locPincodeMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
