package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyListingStatus;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.CompanyListingStatusRepository;
import com.crisil.onesource.service.CompanyListingStatusService;
import com.crisil.onesource.service.dto.CompanyListingStatusCriteria;
import com.crisil.onesource.service.CompanyListingStatusQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyListingStatusResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyListingStatusResourceIT {

    private static final Integer DEFAULT_LISTING_ID = 1;
    private static final Integer UPDATED_LISTING_ID = 2;
    private static final Integer SMALLER_LISTING_ID = 1 - 1;

    private static final String DEFAULT_SCRIPT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SCRIPT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EXCHANGE = "AAAAAAAAAA";
    private static final String UPDATED_EXCHANGE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_STATUS_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_STATUS_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_STATUS_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_OPERATION = "AAAAAAAAAA";
    private static final String UPDATED_OPERATION = "BBBBBBBBBB";

    private static final Integer DEFAULT_CTLG_SRNO = 1;
    private static final Integer UPDATED_CTLG_SRNO = 2;
    private static final Integer SMALLER_CTLG_SRNO = 1 - 1;

    private static final Integer DEFAULT_JSON_STORE_ID = 1;
    private static final Integer UPDATED_JSON_STORE_ID = 2;
    private static final Integer SMALLER_JSON_STORE_ID = 1 - 1;

    private static final String DEFAULT_IS_DATA_PROCESSED = "A";
    private static final String UPDATED_IS_DATA_PROCESSED = "B";

    private static final String DEFAULT_CTLG_IS_DELETED = "A";
    private static final String UPDATED_CTLG_IS_DELETED = "B";

    @Autowired
    private CompanyListingStatusRepository companyListingStatusRepository;

    @Autowired
    private CompanyListingStatusService companyListingStatusService;

    @Autowired
    private CompanyListingStatusQueryService companyListingStatusQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyListingStatusMockMvc;

    private CompanyListingStatus companyListingStatus;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyListingStatus createEntity(EntityManager em) {
        CompanyListingStatus companyListingStatus = new CompanyListingStatus()
            .listingId(DEFAULT_LISTING_ID)
            .scriptCode(DEFAULT_SCRIPT_CODE)
            .exchange(DEFAULT_EXCHANGE)
            .status(DEFAULT_STATUS)
            .statusDate(DEFAULT_STATUS_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED)
            .operation(DEFAULT_OPERATION)
            .ctlgSrno(DEFAULT_CTLG_SRNO)
            .jsonStoreId(DEFAULT_JSON_STORE_ID)
            .isDataProcessed(DEFAULT_IS_DATA_PROCESSED)
            .ctlgIsDeleted(DEFAULT_CTLG_IS_DELETED);
        return companyListingStatus;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyListingStatus createUpdatedEntity(EntityManager em) {
        CompanyListingStatus companyListingStatus = new CompanyListingStatus()
            .listingId(UPDATED_LISTING_ID)
            .scriptCode(UPDATED_SCRIPT_CODE)
            .exchange(UPDATED_EXCHANGE)
            .status(UPDATED_STATUS)
            .statusDate(UPDATED_STATUS_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED)
            .operation(UPDATED_OPERATION)
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED);
        return companyListingStatus;
    }

    @BeforeEach
    public void initTest() {
        companyListingStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyListingStatus() throws Exception {
        int databaseSizeBeforeCreate = companyListingStatusRepository.findAll().size();
        // Create the CompanyListingStatus
        restCompanyListingStatusMockMvc.perform(post("/api/company-listing-statuses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyListingStatus)))
            .andExpect(status().isCreated());

        // Validate the CompanyListingStatus in the database
        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyListingStatus testCompanyListingStatus = companyListingStatusList.get(companyListingStatusList.size() - 1);
        assertThat(testCompanyListingStatus.getListingId()).isEqualTo(DEFAULT_LISTING_ID);
        assertThat(testCompanyListingStatus.getScriptCode()).isEqualTo(DEFAULT_SCRIPT_CODE);
        assertThat(testCompanyListingStatus.getExchange()).isEqualTo(DEFAULT_EXCHANGE);
        assertThat(testCompanyListingStatus.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCompanyListingStatus.getStatusDate()).isEqualTo(DEFAULT_STATUS_DATE);
        assertThat(testCompanyListingStatus.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyListingStatus.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyListingStatus.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanyListingStatus.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyListingStatus.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testCompanyListingStatus.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testCompanyListingStatus.getCtlgSrno()).isEqualTo(DEFAULT_CTLG_SRNO);
        assertThat(testCompanyListingStatus.getJsonStoreId()).isEqualTo(DEFAULT_JSON_STORE_ID);
        assertThat(testCompanyListingStatus.getIsDataProcessed()).isEqualTo(DEFAULT_IS_DATA_PROCESSED);
        assertThat(testCompanyListingStatus.getCtlgIsDeleted()).isEqualTo(DEFAULT_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void createCompanyListingStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyListingStatusRepository.findAll().size();

        // Create the CompanyListingStatus with an existing ID
        companyListingStatus.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyListingStatusMockMvc.perform(post("/api/company-listing-statuses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyListingStatus)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyListingStatus in the database
        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCtlgSrnoIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyListingStatusRepository.findAll().size();
        // set the field null
        companyListingStatus.setCtlgSrno(null);

        // Create the CompanyListingStatus, which fails.


        restCompanyListingStatusMockMvc.perform(post("/api/company-listing-statuses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyListingStatus)))
            .andExpect(status().isBadRequest());

        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCtlgIsDeletedIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyListingStatusRepository.findAll().size();
        // set the field null
        companyListingStatus.setCtlgIsDeleted(null);

        // Create the CompanyListingStatus, which fails.


        restCompanyListingStatusMockMvc.perform(post("/api/company-listing-statuses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyListingStatus)))
            .andExpect(status().isBadRequest());

        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatuses() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyListingStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].listingId").value(hasItem(DEFAULT_LISTING_ID)))
            .andExpect(jsonPath("$.[*].scriptCode").value(hasItem(DEFAULT_SCRIPT_CODE)))
            .andExpect(jsonPath("$.[*].exchange").value(hasItem(DEFAULT_EXCHANGE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].statusDate").value(hasItem(DEFAULT_STATUS_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getCompanyListingStatus() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get the companyListingStatus
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses/{id}", companyListingStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyListingStatus.getId().intValue()))
            .andExpect(jsonPath("$.listingId").value(DEFAULT_LISTING_ID))
            .andExpect(jsonPath("$.scriptCode").value(DEFAULT_SCRIPT_CODE))
            .andExpect(jsonPath("$.exchange").value(DEFAULT_EXCHANGE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.statusDate").value(DEFAULT_STATUS_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION))
            .andExpect(jsonPath("$.ctlgSrno").value(DEFAULT_CTLG_SRNO))
            .andExpect(jsonPath("$.jsonStoreId").value(DEFAULT_JSON_STORE_ID))
            .andExpect(jsonPath("$.isDataProcessed").value(DEFAULT_IS_DATA_PROCESSED))
            .andExpect(jsonPath("$.ctlgIsDeleted").value(DEFAULT_CTLG_IS_DELETED));
    }


    @Test
    @Transactional
    public void getCompanyListingStatusesByIdFiltering() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        Long id = companyListingStatus.getId();

        defaultCompanyListingStatusShouldBeFound("id.equals=" + id);
        defaultCompanyListingStatusShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyListingStatusShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyListingStatusShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyListingStatusShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyListingStatusShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId equals to DEFAULT_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.equals=" + DEFAULT_LISTING_ID);

        // Get all the companyListingStatusList where listingId equals to UPDATED_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.equals=" + UPDATED_LISTING_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId not equals to DEFAULT_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.notEquals=" + DEFAULT_LISTING_ID);

        // Get all the companyListingStatusList where listingId not equals to UPDATED_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.notEquals=" + UPDATED_LISTING_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId in DEFAULT_LISTING_ID or UPDATED_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.in=" + DEFAULT_LISTING_ID + "," + UPDATED_LISTING_ID);

        // Get all the companyListingStatusList where listingId equals to UPDATED_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.in=" + UPDATED_LISTING_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId is not null
        defaultCompanyListingStatusShouldBeFound("listingId.specified=true");

        // Get all the companyListingStatusList where listingId is null
        defaultCompanyListingStatusShouldNotBeFound("listingId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId is greater than or equal to DEFAULT_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.greaterThanOrEqual=" + DEFAULT_LISTING_ID);

        // Get all the companyListingStatusList where listingId is greater than or equal to UPDATED_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.greaterThanOrEqual=" + UPDATED_LISTING_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId is less than or equal to DEFAULT_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.lessThanOrEqual=" + DEFAULT_LISTING_ID);

        // Get all the companyListingStatusList where listingId is less than or equal to SMALLER_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.lessThanOrEqual=" + SMALLER_LISTING_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId is less than DEFAULT_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.lessThan=" + DEFAULT_LISTING_ID);

        // Get all the companyListingStatusList where listingId is less than UPDATED_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.lessThan=" + UPDATED_LISTING_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByListingIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where listingId is greater than DEFAULT_LISTING_ID
        defaultCompanyListingStatusShouldNotBeFound("listingId.greaterThan=" + DEFAULT_LISTING_ID);

        // Get all the companyListingStatusList where listingId is greater than SMALLER_LISTING_ID
        defaultCompanyListingStatusShouldBeFound("listingId.greaterThan=" + SMALLER_LISTING_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByScriptCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where scriptCode equals to DEFAULT_SCRIPT_CODE
        defaultCompanyListingStatusShouldBeFound("scriptCode.equals=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyListingStatusList where scriptCode equals to UPDATED_SCRIPT_CODE
        defaultCompanyListingStatusShouldNotBeFound("scriptCode.equals=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByScriptCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where scriptCode not equals to DEFAULT_SCRIPT_CODE
        defaultCompanyListingStatusShouldNotBeFound("scriptCode.notEquals=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyListingStatusList where scriptCode not equals to UPDATED_SCRIPT_CODE
        defaultCompanyListingStatusShouldBeFound("scriptCode.notEquals=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByScriptCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where scriptCode in DEFAULT_SCRIPT_CODE or UPDATED_SCRIPT_CODE
        defaultCompanyListingStatusShouldBeFound("scriptCode.in=" + DEFAULT_SCRIPT_CODE + "," + UPDATED_SCRIPT_CODE);

        // Get all the companyListingStatusList where scriptCode equals to UPDATED_SCRIPT_CODE
        defaultCompanyListingStatusShouldNotBeFound("scriptCode.in=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByScriptCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where scriptCode is not null
        defaultCompanyListingStatusShouldBeFound("scriptCode.specified=true");

        // Get all the companyListingStatusList where scriptCode is null
        defaultCompanyListingStatusShouldNotBeFound("scriptCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByScriptCodeContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where scriptCode contains DEFAULT_SCRIPT_CODE
        defaultCompanyListingStatusShouldBeFound("scriptCode.contains=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyListingStatusList where scriptCode contains UPDATED_SCRIPT_CODE
        defaultCompanyListingStatusShouldNotBeFound("scriptCode.contains=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByScriptCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where scriptCode does not contain DEFAULT_SCRIPT_CODE
        defaultCompanyListingStatusShouldNotBeFound("scriptCode.doesNotContain=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyListingStatusList where scriptCode does not contain UPDATED_SCRIPT_CODE
        defaultCompanyListingStatusShouldBeFound("scriptCode.doesNotContain=" + UPDATED_SCRIPT_CODE);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByExchangeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where exchange equals to DEFAULT_EXCHANGE
        defaultCompanyListingStatusShouldBeFound("exchange.equals=" + DEFAULT_EXCHANGE);

        // Get all the companyListingStatusList where exchange equals to UPDATED_EXCHANGE
        defaultCompanyListingStatusShouldNotBeFound("exchange.equals=" + UPDATED_EXCHANGE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByExchangeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where exchange not equals to DEFAULT_EXCHANGE
        defaultCompanyListingStatusShouldNotBeFound("exchange.notEquals=" + DEFAULT_EXCHANGE);

        // Get all the companyListingStatusList where exchange not equals to UPDATED_EXCHANGE
        defaultCompanyListingStatusShouldBeFound("exchange.notEquals=" + UPDATED_EXCHANGE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByExchangeIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where exchange in DEFAULT_EXCHANGE or UPDATED_EXCHANGE
        defaultCompanyListingStatusShouldBeFound("exchange.in=" + DEFAULT_EXCHANGE + "," + UPDATED_EXCHANGE);

        // Get all the companyListingStatusList where exchange equals to UPDATED_EXCHANGE
        defaultCompanyListingStatusShouldNotBeFound("exchange.in=" + UPDATED_EXCHANGE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByExchangeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where exchange is not null
        defaultCompanyListingStatusShouldBeFound("exchange.specified=true");

        // Get all the companyListingStatusList where exchange is null
        defaultCompanyListingStatusShouldNotBeFound("exchange.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByExchangeContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where exchange contains DEFAULT_EXCHANGE
        defaultCompanyListingStatusShouldBeFound("exchange.contains=" + DEFAULT_EXCHANGE);

        // Get all the companyListingStatusList where exchange contains UPDATED_EXCHANGE
        defaultCompanyListingStatusShouldNotBeFound("exchange.contains=" + UPDATED_EXCHANGE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByExchangeNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where exchange does not contain DEFAULT_EXCHANGE
        defaultCompanyListingStatusShouldNotBeFound("exchange.doesNotContain=" + DEFAULT_EXCHANGE);

        // Get all the companyListingStatusList where exchange does not contain UPDATED_EXCHANGE
        defaultCompanyListingStatusShouldBeFound("exchange.doesNotContain=" + UPDATED_EXCHANGE);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where status equals to DEFAULT_STATUS
        defaultCompanyListingStatusShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the companyListingStatusList where status equals to UPDATED_STATUS
        defaultCompanyListingStatusShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where status not equals to DEFAULT_STATUS
        defaultCompanyListingStatusShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the companyListingStatusList where status not equals to UPDATED_STATUS
        defaultCompanyListingStatusShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCompanyListingStatusShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the companyListingStatusList where status equals to UPDATED_STATUS
        defaultCompanyListingStatusShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where status is not null
        defaultCompanyListingStatusShouldBeFound("status.specified=true");

        // Get all the companyListingStatusList where status is null
        defaultCompanyListingStatusShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where status contains DEFAULT_STATUS
        defaultCompanyListingStatusShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the companyListingStatusList where status contains UPDATED_STATUS
        defaultCompanyListingStatusShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where status does not contain DEFAULT_STATUS
        defaultCompanyListingStatusShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the companyListingStatusList where status does not contain UPDATED_STATUS
        defaultCompanyListingStatusShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate equals to DEFAULT_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.equals=" + DEFAULT_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate equals to UPDATED_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.equals=" + UPDATED_STATUS_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate not equals to DEFAULT_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.notEquals=" + DEFAULT_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate not equals to UPDATED_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.notEquals=" + UPDATED_STATUS_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate in DEFAULT_STATUS_DATE or UPDATED_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.in=" + DEFAULT_STATUS_DATE + "," + UPDATED_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate equals to UPDATED_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.in=" + UPDATED_STATUS_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate is not null
        defaultCompanyListingStatusShouldBeFound("statusDate.specified=true");

        // Get all the companyListingStatusList where statusDate is null
        defaultCompanyListingStatusShouldNotBeFound("statusDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate is greater than or equal to DEFAULT_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.greaterThanOrEqual=" + DEFAULT_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate is greater than or equal to UPDATED_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.greaterThanOrEqual=" + UPDATED_STATUS_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate is less than or equal to DEFAULT_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.lessThanOrEqual=" + DEFAULT_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate is less than or equal to SMALLER_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.lessThanOrEqual=" + SMALLER_STATUS_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate is less than DEFAULT_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.lessThan=" + DEFAULT_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate is less than UPDATED_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.lessThan=" + UPDATED_STATUS_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByStatusDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where statusDate is greater than DEFAULT_STATUS_DATE
        defaultCompanyListingStatusShouldNotBeFound("statusDate.greaterThan=" + DEFAULT_STATUS_DATE);

        // Get all the companyListingStatusList where statusDate is greater than SMALLER_STATUS_DATE
        defaultCompanyListingStatusShouldBeFound("statusDate.greaterThan=" + SMALLER_STATUS_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyListingStatusShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyListingStatusList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyListingStatusShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyListingStatusShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyListingStatusList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyListingStatusShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyListingStatusShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyListingStatusList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyListingStatusShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdBy is not null
        defaultCompanyListingStatusShouldBeFound("createdBy.specified=true");

        // Get all the companyListingStatusList where createdBy is null
        defaultCompanyListingStatusShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyListingStatusShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyListingStatusList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyListingStatusShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyListingStatusShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyListingStatusList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyListingStatusShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate is not null
        defaultCompanyListingStatusShouldBeFound("createdDate.specified=true");

        // Get all the companyListingStatusList where createdDate is null
        defaultCompanyListingStatusShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompanyListingStatusShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyListingStatusList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompanyListingStatusShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyListingStatusList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyListingStatusList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyListingStatusList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedBy is not null
        defaultCompanyListingStatusShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyListingStatusList where lastModifiedBy is null
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyListingStatusList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyListingStatusList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyListingStatusShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate is not null
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyListingStatusList where lastModifiedDate is null
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyListingStatusList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompanyListingStatusShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyListingStatusList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyListingStatusList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyListingStatusList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDeleted is not null
        defaultCompanyListingStatusShouldBeFound("isDeleted.specified=true");

        // Get all the companyListingStatusList where isDeleted is null
        defaultCompanyListingStatusShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyListingStatusList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyListingStatusList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where operation equals to DEFAULT_OPERATION
        defaultCompanyListingStatusShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the companyListingStatusList where operation equals to UPDATED_OPERATION
        defaultCompanyListingStatusShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where operation not equals to DEFAULT_OPERATION
        defaultCompanyListingStatusShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the companyListingStatusList where operation not equals to UPDATED_OPERATION
        defaultCompanyListingStatusShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultCompanyListingStatusShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the companyListingStatusList where operation equals to UPDATED_OPERATION
        defaultCompanyListingStatusShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where operation is not null
        defaultCompanyListingStatusShouldBeFound("operation.specified=true");

        // Get all the companyListingStatusList where operation is null
        defaultCompanyListingStatusShouldNotBeFound("operation.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByOperationContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where operation contains DEFAULT_OPERATION
        defaultCompanyListingStatusShouldBeFound("operation.contains=" + DEFAULT_OPERATION);

        // Get all the companyListingStatusList where operation contains UPDATED_OPERATION
        defaultCompanyListingStatusShouldNotBeFound("operation.contains=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByOperationNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where operation does not contain DEFAULT_OPERATION
        defaultCompanyListingStatusShouldNotBeFound("operation.doesNotContain=" + DEFAULT_OPERATION);

        // Get all the companyListingStatusList where operation does not contain UPDATED_OPERATION
        defaultCompanyListingStatusShouldBeFound("operation.doesNotContain=" + UPDATED_OPERATION);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno equals to DEFAULT_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.equals=" + DEFAULT_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.equals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno not equals to DEFAULT_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.notEquals=" + DEFAULT_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno not equals to UPDATED_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.notEquals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno in DEFAULT_CTLG_SRNO or UPDATED_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.in=" + DEFAULT_CTLG_SRNO + "," + UPDATED_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.in=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno is not null
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.specified=true");

        // Get all the companyListingStatusList where ctlgSrno is null
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno is greater than or equal to DEFAULT_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.greaterThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno is greater than or equal to UPDATED_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.greaterThanOrEqual=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno is less than or equal to DEFAULT_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.lessThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno is less than or equal to SMALLER_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.lessThanOrEqual=" + SMALLER_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno is less than DEFAULT_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.lessThan=" + DEFAULT_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno is less than UPDATED_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.lessThan=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgSrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgSrno is greater than DEFAULT_CTLG_SRNO
        defaultCompanyListingStatusShouldNotBeFound("ctlgSrno.greaterThan=" + DEFAULT_CTLG_SRNO);

        // Get all the companyListingStatusList where ctlgSrno is greater than SMALLER_CTLG_SRNO
        defaultCompanyListingStatusShouldBeFound("ctlgSrno.greaterThan=" + SMALLER_CTLG_SRNO);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId equals to DEFAULT_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.equals=" + DEFAULT_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.equals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId not equals to DEFAULT_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.notEquals=" + DEFAULT_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId not equals to UPDATED_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.notEquals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId in DEFAULT_JSON_STORE_ID or UPDATED_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.in=" + DEFAULT_JSON_STORE_ID + "," + UPDATED_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.in=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId is not null
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.specified=true");

        // Get all the companyListingStatusList where jsonStoreId is null
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId is greater than or equal to DEFAULT_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.greaterThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId is greater than or equal to UPDATED_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.greaterThanOrEqual=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId is less than or equal to DEFAULT_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.lessThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId is less than or equal to SMALLER_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.lessThanOrEqual=" + SMALLER_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId is less than DEFAULT_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.lessThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId is less than UPDATED_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.lessThan=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByJsonStoreIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where jsonStoreId is greater than DEFAULT_JSON_STORE_ID
        defaultCompanyListingStatusShouldNotBeFound("jsonStoreId.greaterThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the companyListingStatusList where jsonStoreId is greater than SMALLER_JSON_STORE_ID
        defaultCompanyListingStatusShouldBeFound("jsonStoreId.greaterThan=" + SMALLER_JSON_STORE_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDataProcessedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDataProcessed equals to DEFAULT_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldBeFound("isDataProcessed.equals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the companyListingStatusList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldNotBeFound("isDataProcessed.equals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDataProcessedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDataProcessed not equals to DEFAULT_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldNotBeFound("isDataProcessed.notEquals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the companyListingStatusList where isDataProcessed not equals to UPDATED_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldBeFound("isDataProcessed.notEquals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDataProcessedIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDataProcessed in DEFAULT_IS_DATA_PROCESSED or UPDATED_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldBeFound("isDataProcessed.in=" + DEFAULT_IS_DATA_PROCESSED + "," + UPDATED_IS_DATA_PROCESSED);

        // Get all the companyListingStatusList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldNotBeFound("isDataProcessed.in=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDataProcessedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDataProcessed is not null
        defaultCompanyListingStatusShouldBeFound("isDataProcessed.specified=true");

        // Get all the companyListingStatusList where isDataProcessed is null
        defaultCompanyListingStatusShouldNotBeFound("isDataProcessed.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDataProcessedContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDataProcessed contains DEFAULT_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldBeFound("isDataProcessed.contains=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the companyListingStatusList where isDataProcessed contains UPDATED_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldNotBeFound("isDataProcessed.contains=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByIsDataProcessedNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where isDataProcessed does not contain DEFAULT_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldNotBeFound("isDataProcessed.doesNotContain=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the companyListingStatusList where isDataProcessed does not contain UPDATED_IS_DATA_PROCESSED
        defaultCompanyListingStatusShouldBeFound("isDataProcessed.doesNotContain=" + UPDATED_IS_DATA_PROCESSED);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgIsDeleted equals to DEFAULT_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("ctlgIsDeleted.equals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the companyListingStatusList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("ctlgIsDeleted.equals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgIsDeleted not equals to DEFAULT_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("ctlgIsDeleted.notEquals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the companyListingStatusList where ctlgIsDeleted not equals to UPDATED_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("ctlgIsDeleted.notEquals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgIsDeleted in DEFAULT_CTLG_IS_DELETED or UPDATED_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("ctlgIsDeleted.in=" + DEFAULT_CTLG_IS_DELETED + "," + UPDATED_CTLG_IS_DELETED);

        // Get all the companyListingStatusList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("ctlgIsDeleted.in=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgIsDeleted is not null
        defaultCompanyListingStatusShouldBeFound("ctlgIsDeleted.specified=true");

        // Get all the companyListingStatusList where ctlgIsDeleted is null
        defaultCompanyListingStatusShouldNotBeFound("ctlgIsDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgIsDeleted contains DEFAULT_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("ctlgIsDeleted.contains=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the companyListingStatusList where ctlgIsDeleted contains UPDATED_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("ctlgIsDeleted.contains=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCtlgIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);

        // Get all the companyListingStatusList where ctlgIsDeleted does not contain DEFAULT_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldNotBeFound("ctlgIsDeleted.doesNotContain=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the companyListingStatusList where ctlgIsDeleted does not contain UPDATED_CTLG_IS_DELETED
        defaultCompanyListingStatusShouldBeFound("ctlgIsDeleted.doesNotContain=" + UPDATED_CTLG_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyListingStatusesByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyListingStatusRepository.saveAndFlush(companyListingStatus);
        OnesourceCompanyMaster companyCode = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(companyCode);
        em.flush();
        companyListingStatus.setCompanyCode(companyCode);
        companyListingStatusRepository.saveAndFlush(companyListingStatus);
        Long companyCodeId = companyCode.getId();

        // Get all the companyListingStatusList where companyCode equals to companyCodeId
        defaultCompanyListingStatusShouldBeFound("companyCodeId.equals=" + companyCodeId);

        // Get all the companyListingStatusList where companyCode equals to companyCodeId + 1
        defaultCompanyListingStatusShouldNotBeFound("companyCodeId.equals=" + (companyCodeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyListingStatusShouldBeFound(String filter) throws Exception {
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyListingStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].listingId").value(hasItem(DEFAULT_LISTING_ID)))
            .andExpect(jsonPath("$.[*].scriptCode").value(hasItem(DEFAULT_SCRIPT_CODE)))
            .andExpect(jsonPath("$.[*].exchange").value(hasItem(DEFAULT_EXCHANGE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].statusDate").value(hasItem(DEFAULT_STATUS_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)));

        // Check, that the count call also returns 1
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyListingStatusShouldNotBeFound(String filter) throws Exception {
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyListingStatus() throws Exception {
        // Get the companyListingStatus
        restCompanyListingStatusMockMvc.perform(get("/api/company-listing-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyListingStatus() throws Exception {
        // Initialize the database
        companyListingStatusService.save(companyListingStatus);

        int databaseSizeBeforeUpdate = companyListingStatusRepository.findAll().size();

        // Update the companyListingStatus
        CompanyListingStatus updatedCompanyListingStatus = companyListingStatusRepository.findById(companyListingStatus.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyListingStatus are not directly saved in db
        em.detach(updatedCompanyListingStatus);
        updatedCompanyListingStatus
            .listingId(UPDATED_LISTING_ID)
            .scriptCode(UPDATED_SCRIPT_CODE)
            .exchange(UPDATED_EXCHANGE)
            .status(UPDATED_STATUS)
            .statusDate(UPDATED_STATUS_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED)
            .operation(UPDATED_OPERATION)
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED);

        restCompanyListingStatusMockMvc.perform(put("/api/company-listing-statuses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyListingStatus)))
            .andExpect(status().isOk());

        // Validate the CompanyListingStatus in the database
        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeUpdate);
        CompanyListingStatus testCompanyListingStatus = companyListingStatusList.get(companyListingStatusList.size() - 1);
        assertThat(testCompanyListingStatus.getListingId()).isEqualTo(UPDATED_LISTING_ID);
        assertThat(testCompanyListingStatus.getScriptCode()).isEqualTo(UPDATED_SCRIPT_CODE);
        assertThat(testCompanyListingStatus.getExchange()).isEqualTo(UPDATED_EXCHANGE);
        assertThat(testCompanyListingStatus.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCompanyListingStatus.getStatusDate()).isEqualTo(UPDATED_STATUS_DATE);
        assertThat(testCompanyListingStatus.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyListingStatus.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyListingStatus.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanyListingStatus.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyListingStatus.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testCompanyListingStatus.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testCompanyListingStatus.getCtlgSrno()).isEqualTo(UPDATED_CTLG_SRNO);
        assertThat(testCompanyListingStatus.getJsonStoreId()).isEqualTo(UPDATED_JSON_STORE_ID);
        assertThat(testCompanyListingStatus.getIsDataProcessed()).isEqualTo(UPDATED_IS_DATA_PROCESSED);
        assertThat(testCompanyListingStatus.getCtlgIsDeleted()).isEqualTo(UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyListingStatus() throws Exception {
        int databaseSizeBeforeUpdate = companyListingStatusRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyListingStatusMockMvc.perform(put("/api/company-listing-statuses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyListingStatus)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyListingStatus in the database
        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyListingStatus() throws Exception {
        // Initialize the database
        companyListingStatusService.save(companyListingStatus);

        int databaseSizeBeforeDelete = companyListingStatusRepository.findAll().size();

        // Delete the companyListingStatus
        restCompanyListingStatusMockMvc.perform(delete("/api/company-listing-statuses/{id}", companyListingStatus.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyListingStatus> companyListingStatusList = companyListingStatusRepository.findAll();
        assertThat(companyListingStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
