package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyTanPanRocDtls;
import com.crisil.onesource.domain.CompanyRequest;
import com.crisil.onesource.repository.CompanyTanPanRocDtlsRepository;
import com.crisil.onesource.service.CompanyTanPanRocDtlsService;
import com.crisil.onesource.service.dto.CompanyTanPanRocDtlsCriteria;
import com.crisil.onesource.service.CompanyTanPanRocDtlsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyTanPanRocDtlsResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyTanPanRocDtlsResourceIT {

    private static final String DEFAULT_TAN = "AAAAAAAAAA";
    private static final String UPDATED_TAN = "BBBBBBBBBB";

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final String DEFAULT_ROC = "AAAAAAAAAA";
    private static final String UPDATED_ROC = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Integer DEFAULT_COMPANY_RISK_STATUS_ID = 1;
    private static final Integer UPDATED_COMPANY_RISK_STATUS_ID = 2;
    private static final Integer SMALLER_COMPANY_RISK_STATUS_ID = 1 - 1;

    private static final String DEFAULT_COMPANY_RIST_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_RIST_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_BACKGROUND = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_BACKGROUND = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_RPT_FLAG = "AAAAAAAAAA";
    private static final String UPDATED_RPT_FLAG = "BBBBBBBBBB";

    private static final String DEFAULT_GST_EXEMPT = "A";
    private static final String UPDATED_GST_EXEMPT = "B";

    private static final String DEFAULT_GST_NUMBER_NOT_APPLICABLE = "A";
    private static final String UPDATED_GST_NUMBER_NOT_APPLICABLE = "B";

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    @Autowired
    private CompanyTanPanRocDtlsRepository companyTanPanRocDtlsRepository;

    @Autowired
    private CompanyTanPanRocDtlsService companyTanPanRocDtlsService;

    @Autowired
    private CompanyTanPanRocDtlsQueryService companyTanPanRocDtlsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyTanPanRocDtlsMockMvc;

    private CompanyTanPanRocDtls companyTanPanRocDtls;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyTanPanRocDtls createEntity(EntityManager em) {
        CompanyTanPanRocDtls companyTanPanRocDtls = new CompanyTanPanRocDtls()
            .tan(DEFAULT_TAN)
            .pan(DEFAULT_PAN)
            .roc(DEFAULT_ROC)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .companyRiskStatusId(DEFAULT_COMPANY_RISK_STATUS_ID)
            .companyRistComments(DEFAULT_COMPANY_RIST_COMMENTS)
            .companyBackground(DEFAULT_COMPANY_BACKGROUND)
            .isDeleted(DEFAULT_IS_DELETED)
            .rptFlag(DEFAULT_RPT_FLAG)
            .gstExempt(DEFAULT_GST_EXEMPT)
            .gstNumberNotApplicable(DEFAULT_GST_NUMBER_NOT_APPLICABLE)
            .companyCode(DEFAULT_COMPANY_CODE);
        return companyTanPanRocDtls;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyTanPanRocDtls createUpdatedEntity(EntityManager em) {
        CompanyTanPanRocDtls companyTanPanRocDtls = new CompanyTanPanRocDtls()
            .tan(UPDATED_TAN)
            .pan(UPDATED_PAN)
            .roc(UPDATED_ROC)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .companyRiskStatusId(UPDATED_COMPANY_RISK_STATUS_ID)
            .companyRistComments(UPDATED_COMPANY_RIST_COMMENTS)
            .companyBackground(UPDATED_COMPANY_BACKGROUND)
            .isDeleted(UPDATED_IS_DELETED)
            .rptFlag(UPDATED_RPT_FLAG)
            .gstExempt(UPDATED_GST_EXEMPT)
            .gstNumberNotApplicable(UPDATED_GST_NUMBER_NOT_APPLICABLE)
            .companyCode(UPDATED_COMPANY_CODE);
        return companyTanPanRocDtls;
    }

    @BeforeEach
    public void initTest() {
        companyTanPanRocDtls = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyTanPanRocDtls() throws Exception {
        int databaseSizeBeforeCreate = companyTanPanRocDtlsRepository.findAll().size();
        // Create the CompanyTanPanRocDtls
        restCompanyTanPanRocDtlsMockMvc.perform(post("/api/company-tan-pan-roc-dtls")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTanPanRocDtls)))
            .andExpect(status().isCreated());

        // Validate the CompanyTanPanRocDtls in the database
        List<CompanyTanPanRocDtls> companyTanPanRocDtlsList = companyTanPanRocDtlsRepository.findAll();
        assertThat(companyTanPanRocDtlsList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyTanPanRocDtls testCompanyTanPanRocDtls = companyTanPanRocDtlsList.get(companyTanPanRocDtlsList.size() - 1);
        assertThat(testCompanyTanPanRocDtls.getTan()).isEqualTo(DEFAULT_TAN);
        assertThat(testCompanyTanPanRocDtls.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testCompanyTanPanRocDtls.getRoc()).isEqualTo(DEFAULT_ROC);
        assertThat(testCompanyTanPanRocDtls.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyTanPanRocDtls.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyTanPanRocDtls.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyTanPanRocDtls.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanyTanPanRocDtls.getCompanyRiskStatusId()).isEqualTo(DEFAULT_COMPANY_RISK_STATUS_ID);
        assertThat(testCompanyTanPanRocDtls.getCompanyRistComments()).isEqualTo(DEFAULT_COMPANY_RIST_COMMENTS);
        assertThat(testCompanyTanPanRocDtls.getCompanyBackground()).isEqualTo(DEFAULT_COMPANY_BACKGROUND);
        assertThat(testCompanyTanPanRocDtls.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testCompanyTanPanRocDtls.getRptFlag()).isEqualTo(DEFAULT_RPT_FLAG);
        assertThat(testCompanyTanPanRocDtls.getGstExempt()).isEqualTo(DEFAULT_GST_EXEMPT);
        assertThat(testCompanyTanPanRocDtls.getGstNumberNotApplicable()).isEqualTo(DEFAULT_GST_NUMBER_NOT_APPLICABLE);
        assertThat(testCompanyTanPanRocDtls.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void createCompanyTanPanRocDtlsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyTanPanRocDtlsRepository.findAll().size();

        // Create the CompanyTanPanRocDtls with an existing ID
        companyTanPanRocDtls.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyTanPanRocDtlsMockMvc.perform(post("/api/company-tan-pan-roc-dtls")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTanPanRocDtls)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyTanPanRocDtls in the database
        List<CompanyTanPanRocDtls> companyTanPanRocDtlsList = companyTanPanRocDtlsRepository.findAll();
        assertThat(companyTanPanRocDtlsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtls() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyTanPanRocDtls.getId().intValue())))
            .andExpect(jsonPath("$.[*].tan").value(hasItem(DEFAULT_TAN)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].roc").value(hasItem(DEFAULT_ROC)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].companyRiskStatusId").value(hasItem(DEFAULT_COMPANY_RISK_STATUS_ID)))
            .andExpect(jsonPath("$.[*].companyRistComments").value(hasItem(DEFAULT_COMPANY_RIST_COMMENTS)))
            .andExpect(jsonPath("$.[*].companyBackground").value(hasItem(DEFAULT_COMPANY_BACKGROUND)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].rptFlag").value(hasItem(DEFAULT_RPT_FLAG)))
            .andExpect(jsonPath("$.[*].gstExempt").value(hasItem(DEFAULT_GST_EXEMPT)))
            .andExpect(jsonPath("$.[*].gstNumberNotApplicable").value(hasItem(DEFAULT_GST_NUMBER_NOT_APPLICABLE)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)));
    }
    
    @Test
    @Transactional
    public void getCompanyTanPanRocDtls() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get the companyTanPanRocDtls
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls/{id}", companyTanPanRocDtls.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyTanPanRocDtls.getId().intValue()))
            .andExpect(jsonPath("$.tan").value(DEFAULT_TAN))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.roc").value(DEFAULT_ROC))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.companyRiskStatusId").value(DEFAULT_COMPANY_RISK_STATUS_ID))
            .andExpect(jsonPath("$.companyRistComments").value(DEFAULT_COMPANY_RIST_COMMENTS))
            .andExpect(jsonPath("$.companyBackground").value(DEFAULT_COMPANY_BACKGROUND))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.rptFlag").value(DEFAULT_RPT_FLAG))
            .andExpect(jsonPath("$.gstExempt").value(DEFAULT_GST_EXEMPT))
            .andExpect(jsonPath("$.gstNumberNotApplicable").value(DEFAULT_GST_NUMBER_NOT_APPLICABLE))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE));
    }


    @Test
    @Transactional
    public void getCompanyTanPanRocDtlsByIdFiltering() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        Long id = companyTanPanRocDtls.getId();

        defaultCompanyTanPanRocDtlsShouldBeFound("id.equals=" + id);
        defaultCompanyTanPanRocDtlsShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyTanPanRocDtlsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyTanPanRocDtlsShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyTanPanRocDtlsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyTanPanRocDtlsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByTanIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where tan equals to DEFAULT_TAN
        defaultCompanyTanPanRocDtlsShouldBeFound("tan.equals=" + DEFAULT_TAN);

        // Get all the companyTanPanRocDtlsList where tan equals to UPDATED_TAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("tan.equals=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByTanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where tan not equals to DEFAULT_TAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("tan.notEquals=" + DEFAULT_TAN);

        // Get all the companyTanPanRocDtlsList where tan not equals to UPDATED_TAN
        defaultCompanyTanPanRocDtlsShouldBeFound("tan.notEquals=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByTanIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where tan in DEFAULT_TAN or UPDATED_TAN
        defaultCompanyTanPanRocDtlsShouldBeFound("tan.in=" + DEFAULT_TAN + "," + UPDATED_TAN);

        // Get all the companyTanPanRocDtlsList where tan equals to UPDATED_TAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("tan.in=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByTanIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where tan is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("tan.specified=true");

        // Get all the companyTanPanRocDtlsList where tan is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("tan.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByTanContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where tan contains DEFAULT_TAN
        defaultCompanyTanPanRocDtlsShouldBeFound("tan.contains=" + DEFAULT_TAN);

        // Get all the companyTanPanRocDtlsList where tan contains UPDATED_TAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("tan.contains=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByTanNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where tan does not contain DEFAULT_TAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("tan.doesNotContain=" + DEFAULT_TAN);

        // Get all the companyTanPanRocDtlsList where tan does not contain UPDATED_TAN
        defaultCompanyTanPanRocDtlsShouldBeFound("tan.doesNotContain=" + UPDATED_TAN);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByPanIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where pan equals to DEFAULT_PAN
        defaultCompanyTanPanRocDtlsShouldBeFound("pan.equals=" + DEFAULT_PAN);

        // Get all the companyTanPanRocDtlsList where pan equals to UPDATED_PAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("pan.equals=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByPanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where pan not equals to DEFAULT_PAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("pan.notEquals=" + DEFAULT_PAN);

        // Get all the companyTanPanRocDtlsList where pan not equals to UPDATED_PAN
        defaultCompanyTanPanRocDtlsShouldBeFound("pan.notEquals=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByPanIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where pan in DEFAULT_PAN or UPDATED_PAN
        defaultCompanyTanPanRocDtlsShouldBeFound("pan.in=" + DEFAULT_PAN + "," + UPDATED_PAN);

        // Get all the companyTanPanRocDtlsList where pan equals to UPDATED_PAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("pan.in=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByPanIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where pan is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("pan.specified=true");

        // Get all the companyTanPanRocDtlsList where pan is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("pan.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByPanContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where pan contains DEFAULT_PAN
        defaultCompanyTanPanRocDtlsShouldBeFound("pan.contains=" + DEFAULT_PAN);

        // Get all the companyTanPanRocDtlsList where pan contains UPDATED_PAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("pan.contains=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByPanNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where pan does not contain DEFAULT_PAN
        defaultCompanyTanPanRocDtlsShouldNotBeFound("pan.doesNotContain=" + DEFAULT_PAN);

        // Get all the companyTanPanRocDtlsList where pan does not contain UPDATED_PAN
        defaultCompanyTanPanRocDtlsShouldBeFound("pan.doesNotContain=" + UPDATED_PAN);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRocIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where roc equals to DEFAULT_ROC
        defaultCompanyTanPanRocDtlsShouldBeFound("roc.equals=" + DEFAULT_ROC);

        // Get all the companyTanPanRocDtlsList where roc equals to UPDATED_ROC
        defaultCompanyTanPanRocDtlsShouldNotBeFound("roc.equals=" + UPDATED_ROC);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRocIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where roc not equals to DEFAULT_ROC
        defaultCompanyTanPanRocDtlsShouldNotBeFound("roc.notEquals=" + DEFAULT_ROC);

        // Get all the companyTanPanRocDtlsList where roc not equals to UPDATED_ROC
        defaultCompanyTanPanRocDtlsShouldBeFound("roc.notEquals=" + UPDATED_ROC);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRocIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where roc in DEFAULT_ROC or UPDATED_ROC
        defaultCompanyTanPanRocDtlsShouldBeFound("roc.in=" + DEFAULT_ROC + "," + UPDATED_ROC);

        // Get all the companyTanPanRocDtlsList where roc equals to UPDATED_ROC
        defaultCompanyTanPanRocDtlsShouldNotBeFound("roc.in=" + UPDATED_ROC);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRocIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where roc is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("roc.specified=true");

        // Get all the companyTanPanRocDtlsList where roc is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("roc.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRocContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where roc contains DEFAULT_ROC
        defaultCompanyTanPanRocDtlsShouldBeFound("roc.contains=" + DEFAULT_ROC);

        // Get all the companyTanPanRocDtlsList where roc contains UPDATED_ROC
        defaultCompanyTanPanRocDtlsShouldNotBeFound("roc.contains=" + UPDATED_ROC);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRocNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where roc does not contain DEFAULT_ROC
        defaultCompanyTanPanRocDtlsShouldNotBeFound("roc.doesNotContain=" + DEFAULT_ROC);

        // Get all the companyTanPanRocDtlsList where roc does not contain UPDATED_ROC
        defaultCompanyTanPanRocDtlsShouldBeFound("roc.doesNotContain=" + UPDATED_ROC);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.specified=true");

        // Get all the companyTanPanRocDtlsList where createdDate is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyTanPanRocDtlsList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyTanPanRocDtlsList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyTanPanRocDtlsList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyTanPanRocDtlsList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdBy is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("createdBy.specified=true");

        // Get all the companyTanPanRocDtlsList where createdBy is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyTanPanRocDtlsList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyTanPanRocDtlsList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyTanPanRocDtlsList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyTanPanRocDtlsList where lastModifiedBy is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyTanPanRocDtlsList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyTanPanRocDtlsShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId equals to DEFAULT_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.equals=" + DEFAULT_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId equals to UPDATED_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.equals=" + UPDATED_COMPANY_RISK_STATUS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId not equals to DEFAULT_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.notEquals=" + DEFAULT_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId not equals to UPDATED_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.notEquals=" + UPDATED_COMPANY_RISK_STATUS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId in DEFAULT_COMPANY_RISK_STATUS_ID or UPDATED_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.in=" + DEFAULT_COMPANY_RISK_STATUS_ID + "," + UPDATED_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId equals to UPDATED_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.in=" + UPDATED_COMPANY_RISK_STATUS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.specified=true");

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is greater than or equal to DEFAULT_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.greaterThanOrEqual=" + DEFAULT_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is greater than or equal to UPDATED_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.greaterThanOrEqual=" + UPDATED_COMPANY_RISK_STATUS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is less than or equal to DEFAULT_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.lessThanOrEqual=" + DEFAULT_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is less than or equal to SMALLER_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.lessThanOrEqual=" + SMALLER_COMPANY_RISK_STATUS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is less than DEFAULT_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.lessThan=" + DEFAULT_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is less than UPDATED_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.lessThan=" + UPDATED_COMPANY_RISK_STATUS_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRiskStatusIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is greater than DEFAULT_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRiskStatusId.greaterThan=" + DEFAULT_COMPANY_RISK_STATUS_ID);

        // Get all the companyTanPanRocDtlsList where companyRiskStatusId is greater than SMALLER_COMPANY_RISK_STATUS_ID
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRiskStatusId.greaterThan=" + SMALLER_COMPANY_RISK_STATUS_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRistCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRistComments equals to DEFAULT_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRistComments.equals=" + DEFAULT_COMPANY_RIST_COMMENTS);

        // Get all the companyTanPanRocDtlsList where companyRistComments equals to UPDATED_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRistComments.equals=" + UPDATED_COMPANY_RIST_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRistCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRistComments not equals to DEFAULT_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRistComments.notEquals=" + DEFAULT_COMPANY_RIST_COMMENTS);

        // Get all the companyTanPanRocDtlsList where companyRistComments not equals to UPDATED_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRistComments.notEquals=" + UPDATED_COMPANY_RIST_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRistCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRistComments in DEFAULT_COMPANY_RIST_COMMENTS or UPDATED_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRistComments.in=" + DEFAULT_COMPANY_RIST_COMMENTS + "," + UPDATED_COMPANY_RIST_COMMENTS);

        // Get all the companyTanPanRocDtlsList where companyRistComments equals to UPDATED_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRistComments.in=" + UPDATED_COMPANY_RIST_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRistCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRistComments is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRistComments.specified=true");

        // Get all the companyTanPanRocDtlsList where companyRistComments is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRistComments.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRistCommentsContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRistComments contains DEFAULT_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRistComments.contains=" + DEFAULT_COMPANY_RIST_COMMENTS);

        // Get all the companyTanPanRocDtlsList where companyRistComments contains UPDATED_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRistComments.contains=" + UPDATED_COMPANY_RIST_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyRistCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyRistComments does not contain DEFAULT_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyRistComments.doesNotContain=" + DEFAULT_COMPANY_RIST_COMMENTS);

        // Get all the companyTanPanRocDtlsList where companyRistComments does not contain UPDATED_COMPANY_RIST_COMMENTS
        defaultCompanyTanPanRocDtlsShouldBeFound("companyRistComments.doesNotContain=" + UPDATED_COMPANY_RIST_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyBackgroundIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyBackground equals to DEFAULT_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldBeFound("companyBackground.equals=" + DEFAULT_COMPANY_BACKGROUND);

        // Get all the companyTanPanRocDtlsList where companyBackground equals to UPDATED_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyBackground.equals=" + UPDATED_COMPANY_BACKGROUND);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyBackgroundIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyBackground not equals to DEFAULT_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyBackground.notEquals=" + DEFAULT_COMPANY_BACKGROUND);

        // Get all the companyTanPanRocDtlsList where companyBackground not equals to UPDATED_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldBeFound("companyBackground.notEquals=" + UPDATED_COMPANY_BACKGROUND);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyBackgroundIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyBackground in DEFAULT_COMPANY_BACKGROUND or UPDATED_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldBeFound("companyBackground.in=" + DEFAULT_COMPANY_BACKGROUND + "," + UPDATED_COMPANY_BACKGROUND);

        // Get all the companyTanPanRocDtlsList where companyBackground equals to UPDATED_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyBackground.in=" + UPDATED_COMPANY_BACKGROUND);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyBackgroundIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyBackground is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("companyBackground.specified=true");

        // Get all the companyTanPanRocDtlsList where companyBackground is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyBackground.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyBackgroundContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyBackground contains DEFAULT_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldBeFound("companyBackground.contains=" + DEFAULT_COMPANY_BACKGROUND);

        // Get all the companyTanPanRocDtlsList where companyBackground contains UPDATED_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyBackground.contains=" + UPDATED_COMPANY_BACKGROUND);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyBackgroundNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyBackground does not contain DEFAULT_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyBackground.doesNotContain=" + DEFAULT_COMPANY_BACKGROUND);

        // Get all the companyTanPanRocDtlsList where companyBackground does not contain UPDATED_COMPANY_BACKGROUND
        defaultCompanyTanPanRocDtlsShouldBeFound("companyBackground.doesNotContain=" + UPDATED_COMPANY_BACKGROUND);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyTanPanRocDtlsList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyTanPanRocDtlsList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyTanPanRocDtlsList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where isDeleted is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("isDeleted.specified=true");

        // Get all the companyTanPanRocDtlsList where isDeleted is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyTanPanRocDtlsList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyTanPanRocDtlsList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyTanPanRocDtlsShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRptFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where rptFlag equals to DEFAULT_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldBeFound("rptFlag.equals=" + DEFAULT_RPT_FLAG);

        // Get all the companyTanPanRocDtlsList where rptFlag equals to UPDATED_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldNotBeFound("rptFlag.equals=" + UPDATED_RPT_FLAG);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRptFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where rptFlag not equals to DEFAULT_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldNotBeFound("rptFlag.notEquals=" + DEFAULT_RPT_FLAG);

        // Get all the companyTanPanRocDtlsList where rptFlag not equals to UPDATED_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldBeFound("rptFlag.notEquals=" + UPDATED_RPT_FLAG);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRptFlagIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where rptFlag in DEFAULT_RPT_FLAG or UPDATED_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldBeFound("rptFlag.in=" + DEFAULT_RPT_FLAG + "," + UPDATED_RPT_FLAG);

        // Get all the companyTanPanRocDtlsList where rptFlag equals to UPDATED_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldNotBeFound("rptFlag.in=" + UPDATED_RPT_FLAG);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRptFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where rptFlag is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("rptFlag.specified=true");

        // Get all the companyTanPanRocDtlsList where rptFlag is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("rptFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRptFlagContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where rptFlag contains DEFAULT_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldBeFound("rptFlag.contains=" + DEFAULT_RPT_FLAG);

        // Get all the companyTanPanRocDtlsList where rptFlag contains UPDATED_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldNotBeFound("rptFlag.contains=" + UPDATED_RPT_FLAG);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRptFlagNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where rptFlag does not contain DEFAULT_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldNotBeFound("rptFlag.doesNotContain=" + DEFAULT_RPT_FLAG);

        // Get all the companyTanPanRocDtlsList where rptFlag does not contain UPDATED_RPT_FLAG
        defaultCompanyTanPanRocDtlsShouldBeFound("rptFlag.doesNotContain=" + UPDATED_RPT_FLAG);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstExemptIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstExempt equals to DEFAULT_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldBeFound("gstExempt.equals=" + DEFAULT_GST_EXEMPT);

        // Get all the companyTanPanRocDtlsList where gstExempt equals to UPDATED_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstExempt.equals=" + UPDATED_GST_EXEMPT);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstExemptIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstExempt not equals to DEFAULT_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstExempt.notEquals=" + DEFAULT_GST_EXEMPT);

        // Get all the companyTanPanRocDtlsList where gstExempt not equals to UPDATED_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldBeFound("gstExempt.notEquals=" + UPDATED_GST_EXEMPT);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstExemptIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstExempt in DEFAULT_GST_EXEMPT or UPDATED_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldBeFound("gstExempt.in=" + DEFAULT_GST_EXEMPT + "," + UPDATED_GST_EXEMPT);

        // Get all the companyTanPanRocDtlsList where gstExempt equals to UPDATED_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstExempt.in=" + UPDATED_GST_EXEMPT);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstExemptIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstExempt is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("gstExempt.specified=true");

        // Get all the companyTanPanRocDtlsList where gstExempt is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstExempt.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstExemptContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstExempt contains DEFAULT_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldBeFound("gstExempt.contains=" + DEFAULT_GST_EXEMPT);

        // Get all the companyTanPanRocDtlsList where gstExempt contains UPDATED_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstExempt.contains=" + UPDATED_GST_EXEMPT);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstExemptNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstExempt does not contain DEFAULT_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstExempt.doesNotContain=" + DEFAULT_GST_EXEMPT);

        // Get all the companyTanPanRocDtlsList where gstExempt does not contain UPDATED_GST_EXEMPT
        defaultCompanyTanPanRocDtlsShouldBeFound("gstExempt.doesNotContain=" + UPDATED_GST_EXEMPT);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstNumberNotApplicableIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable equals to DEFAULT_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldBeFound("gstNumberNotApplicable.equals=" + DEFAULT_GST_NUMBER_NOT_APPLICABLE);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable equals to UPDATED_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstNumberNotApplicable.equals=" + UPDATED_GST_NUMBER_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstNumberNotApplicableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable not equals to DEFAULT_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstNumberNotApplicable.notEquals=" + DEFAULT_GST_NUMBER_NOT_APPLICABLE);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable not equals to UPDATED_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldBeFound("gstNumberNotApplicable.notEquals=" + UPDATED_GST_NUMBER_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstNumberNotApplicableIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable in DEFAULT_GST_NUMBER_NOT_APPLICABLE or UPDATED_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldBeFound("gstNumberNotApplicable.in=" + DEFAULT_GST_NUMBER_NOT_APPLICABLE + "," + UPDATED_GST_NUMBER_NOT_APPLICABLE);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable equals to UPDATED_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstNumberNotApplicable.in=" + UPDATED_GST_NUMBER_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstNumberNotApplicableIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("gstNumberNotApplicable.specified=true");

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstNumberNotApplicable.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstNumberNotApplicableContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable contains DEFAULT_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldBeFound("gstNumberNotApplicable.contains=" + DEFAULT_GST_NUMBER_NOT_APPLICABLE);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable contains UPDATED_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstNumberNotApplicable.contains=" + UPDATED_GST_NUMBER_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByGstNumberNotApplicableNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable does not contain DEFAULT_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("gstNumberNotApplicable.doesNotContain=" + DEFAULT_GST_NUMBER_NOT_APPLICABLE);

        // Get all the companyTanPanRocDtlsList where gstNumberNotApplicable does not contain UPDATED_GST_NUMBER_NOT_APPLICABLE
        defaultCompanyTanPanRocDtlsShouldBeFound("gstNumberNotApplicable.doesNotContain=" + UPDATED_GST_NUMBER_NOT_APPLICABLE);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the companyTanPanRocDtlsList where companyCode equals to UPDATED_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the companyTanPanRocDtlsList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the companyTanPanRocDtlsList where companyCode equals to UPDATED_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyCode is not null
        defaultCompanyTanPanRocDtlsShouldBeFound("companyCode.specified=true");

        // Get all the companyTanPanRocDtlsList where companyCode is null
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyCode contains DEFAULT_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the companyTanPanRocDtlsList where companyCode contains UPDATED_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);

        // Get all the companyTanPanRocDtlsList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the companyTanPanRocDtlsList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultCompanyTanPanRocDtlsShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllCompanyTanPanRocDtlsByRecordIsEqualToSomething() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);
        CompanyRequest record = CompanyRequestResourceIT.createEntity(em);
        em.persist(record);
        em.flush();
        companyTanPanRocDtls.setRecord(record);
        companyTanPanRocDtlsRepository.saveAndFlush(companyTanPanRocDtls);
        Long recordId = record.getId();

        // Get all the companyTanPanRocDtlsList where record equals to recordId
        defaultCompanyTanPanRocDtlsShouldBeFound("recordId.equals=" + recordId);

        // Get all the companyTanPanRocDtlsList where record equals to recordId + 1
        defaultCompanyTanPanRocDtlsShouldNotBeFound("recordId.equals=" + (recordId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyTanPanRocDtlsShouldBeFound(String filter) throws Exception {
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyTanPanRocDtls.getId().intValue())))
            .andExpect(jsonPath("$.[*].tan").value(hasItem(DEFAULT_TAN)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].roc").value(hasItem(DEFAULT_ROC)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].companyRiskStatusId").value(hasItem(DEFAULT_COMPANY_RISK_STATUS_ID)))
            .andExpect(jsonPath("$.[*].companyRistComments").value(hasItem(DEFAULT_COMPANY_RIST_COMMENTS)))
            .andExpect(jsonPath("$.[*].companyBackground").value(hasItem(DEFAULT_COMPANY_BACKGROUND)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].rptFlag").value(hasItem(DEFAULT_RPT_FLAG)))
            .andExpect(jsonPath("$.[*].gstExempt").value(hasItem(DEFAULT_GST_EXEMPT)))
            .andExpect(jsonPath("$.[*].gstNumberNotApplicable").value(hasItem(DEFAULT_GST_NUMBER_NOT_APPLICABLE)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)));

        // Check, that the count call also returns 1
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyTanPanRocDtlsShouldNotBeFound(String filter) throws Exception {
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyTanPanRocDtls() throws Exception {
        // Get the companyTanPanRocDtls
        restCompanyTanPanRocDtlsMockMvc.perform(get("/api/company-tan-pan-roc-dtls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyTanPanRocDtls() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsService.save(companyTanPanRocDtls);

        int databaseSizeBeforeUpdate = companyTanPanRocDtlsRepository.findAll().size();

        // Update the companyTanPanRocDtls
        CompanyTanPanRocDtls updatedCompanyTanPanRocDtls = companyTanPanRocDtlsRepository.findById(companyTanPanRocDtls.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyTanPanRocDtls are not directly saved in db
        em.detach(updatedCompanyTanPanRocDtls);
        updatedCompanyTanPanRocDtls
            .tan(UPDATED_TAN)
            .pan(UPDATED_PAN)
            .roc(UPDATED_ROC)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .companyRiskStatusId(UPDATED_COMPANY_RISK_STATUS_ID)
            .companyRistComments(UPDATED_COMPANY_RIST_COMMENTS)
            .companyBackground(UPDATED_COMPANY_BACKGROUND)
            .isDeleted(UPDATED_IS_DELETED)
            .rptFlag(UPDATED_RPT_FLAG)
            .gstExempt(UPDATED_GST_EXEMPT)
            .gstNumberNotApplicable(UPDATED_GST_NUMBER_NOT_APPLICABLE)
            .companyCode(UPDATED_COMPANY_CODE);

        restCompanyTanPanRocDtlsMockMvc.perform(put("/api/company-tan-pan-roc-dtls")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyTanPanRocDtls)))
            .andExpect(status().isOk());

        // Validate the CompanyTanPanRocDtls in the database
        List<CompanyTanPanRocDtls> companyTanPanRocDtlsList = companyTanPanRocDtlsRepository.findAll();
        assertThat(companyTanPanRocDtlsList).hasSize(databaseSizeBeforeUpdate);
        CompanyTanPanRocDtls testCompanyTanPanRocDtls = companyTanPanRocDtlsList.get(companyTanPanRocDtlsList.size() - 1);
        assertThat(testCompanyTanPanRocDtls.getTan()).isEqualTo(UPDATED_TAN);
        assertThat(testCompanyTanPanRocDtls.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testCompanyTanPanRocDtls.getRoc()).isEqualTo(UPDATED_ROC);
        assertThat(testCompanyTanPanRocDtls.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyTanPanRocDtls.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyTanPanRocDtls.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyTanPanRocDtls.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanyTanPanRocDtls.getCompanyRiskStatusId()).isEqualTo(UPDATED_COMPANY_RISK_STATUS_ID);
        assertThat(testCompanyTanPanRocDtls.getCompanyRistComments()).isEqualTo(UPDATED_COMPANY_RIST_COMMENTS);
        assertThat(testCompanyTanPanRocDtls.getCompanyBackground()).isEqualTo(UPDATED_COMPANY_BACKGROUND);
        assertThat(testCompanyTanPanRocDtls.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testCompanyTanPanRocDtls.getRptFlag()).isEqualTo(UPDATED_RPT_FLAG);
        assertThat(testCompanyTanPanRocDtls.getGstExempt()).isEqualTo(UPDATED_GST_EXEMPT);
        assertThat(testCompanyTanPanRocDtls.getGstNumberNotApplicable()).isEqualTo(UPDATED_GST_NUMBER_NOT_APPLICABLE);
        assertThat(testCompanyTanPanRocDtls.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyTanPanRocDtls() throws Exception {
        int databaseSizeBeforeUpdate = companyTanPanRocDtlsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyTanPanRocDtlsMockMvc.perform(put("/api/company-tan-pan-roc-dtls")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyTanPanRocDtls)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyTanPanRocDtls in the database
        List<CompanyTanPanRocDtls> companyTanPanRocDtlsList = companyTanPanRocDtlsRepository.findAll();
        assertThat(companyTanPanRocDtlsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyTanPanRocDtls() throws Exception {
        // Initialize the database
        companyTanPanRocDtlsService.save(companyTanPanRocDtls);

        int databaseSizeBeforeDelete = companyTanPanRocDtlsRepository.findAll().size();

        // Delete the companyTanPanRocDtls
        restCompanyTanPanRocDtlsMockMvc.perform(delete("/api/company-tan-pan-roc-dtls/{id}", companyTanPanRocDtls.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyTanPanRocDtls> companyTanPanRocDtlsList = companyTanPanRocDtlsRepository.findAll();
        assertThat(companyTanPanRocDtlsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
