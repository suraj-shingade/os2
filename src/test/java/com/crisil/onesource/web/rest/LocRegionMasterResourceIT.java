package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocRegionMaster;
import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.repository.LocRegionMasterRepository;
import com.crisil.onesource.service.LocRegionMasterService;
import com.crisil.onesource.service.dto.LocRegionMasterCriteria;
import com.crisil.onesource.service.LocRegionMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocRegionMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocRegionMasterResourceIT {

    private static final Integer DEFAULT_REGION_ID = 1;
    private static final Integer UPDATED_REGION_ID = 2;
    private static final Integer SMALLER_REGION_ID = 1 - 1;

    private static final String DEFAULT_REGION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_REGION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_IS_APPROVED = "A";
    private static final String UPDATED_IS_APPROVED = "B";

    private static final Integer DEFAULT_SEQ_ORDER = 1;
    private static final Integer UPDATED_SEQ_ORDER = 2;
    private static final Integer SMALLER_SEQ_ORDER = 1 - 1;

    @Autowired
    private LocRegionMasterRepository locRegionMasterRepository;

    @Autowired
    private LocRegionMasterService locRegionMasterService;

    @Autowired
    private LocRegionMasterQueryService locRegionMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocRegionMasterMockMvc;

    private LocRegionMaster locRegionMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocRegionMaster createEntity(EntityManager em) {
        LocRegionMaster locRegionMaster = new LocRegionMaster()
            .regionId(DEFAULT_REGION_ID)
            .regionName(DEFAULT_REGION_NAME)
            .description(DEFAULT_DESCRIPTION)
            .isDeleted(DEFAULT_IS_DELETED)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isApproved(DEFAULT_IS_APPROVED)
            .seqOrder(DEFAULT_SEQ_ORDER);
        return locRegionMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocRegionMaster createUpdatedEntity(EntityManager em) {
        LocRegionMaster locRegionMaster = new LocRegionMaster()
            .regionId(UPDATED_REGION_ID)
            .regionName(UPDATED_REGION_NAME)
            .description(UPDATED_DESCRIPTION)
            .isDeleted(UPDATED_IS_DELETED)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);
        return locRegionMaster;
    }

    @BeforeEach
    public void initTest() {
        locRegionMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocRegionMaster() throws Exception {
        int databaseSizeBeforeCreate = locRegionMasterRepository.findAll().size();
        // Create the LocRegionMaster
        restLocRegionMasterMockMvc.perform(post("/api/loc-region-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locRegionMaster)))
            .andExpect(status().isCreated());

        // Validate the LocRegionMaster in the database
        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeCreate + 1);
        LocRegionMaster testLocRegionMaster = locRegionMasterList.get(locRegionMasterList.size() - 1);
        assertThat(testLocRegionMaster.getRegionId()).isEqualTo(DEFAULT_REGION_ID);
        assertThat(testLocRegionMaster.getRegionName()).isEqualTo(DEFAULT_REGION_NAME);
        assertThat(testLocRegionMaster.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testLocRegionMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocRegionMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocRegionMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocRegionMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testLocRegionMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocRegionMaster.getIsApproved()).isEqualTo(DEFAULT_IS_APPROVED);
        assertThat(testLocRegionMaster.getSeqOrder()).isEqualTo(DEFAULT_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void createLocRegionMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locRegionMasterRepository.findAll().size();

        // Create the LocRegionMaster with an existing ID
        locRegionMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocRegionMasterMockMvc.perform(post("/api/loc-region-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locRegionMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocRegionMaster in the database
        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkRegionIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = locRegionMasterRepository.findAll().size();
        // set the field null
        locRegionMaster.setRegionId(null);

        // Create the LocRegionMaster, which fails.


        restLocRegionMasterMockMvc.perform(post("/api/loc-region-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locRegionMaster)))
            .andExpect(status().isBadRequest());

        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRegionNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = locRegionMasterRepository.findAll().size();
        // set the field null
        locRegionMaster.setRegionName(null);

        // Create the LocRegionMaster, which fails.


        restLocRegionMasterMockMvc.perform(post("/api/loc-region-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locRegionMaster)))
            .andExpect(status().isBadRequest());

        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocRegionMasters() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locRegionMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].regionId").value(hasItem(DEFAULT_REGION_ID)))
            .andExpect(jsonPath("$.[*].regionName").value(hasItem(DEFAULT_REGION_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));
    }
    
    @Test
    @Transactional
    public void getLocRegionMaster() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get the locRegionMaster
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters/{id}", locRegionMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locRegionMaster.getId().intValue()))
            .andExpect(jsonPath("$.regionId").value(DEFAULT_REGION_ID))
            .andExpect(jsonPath("$.regionName").value(DEFAULT_REGION_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isApproved").value(DEFAULT_IS_APPROVED))
            .andExpect(jsonPath("$.seqOrder").value(DEFAULT_SEQ_ORDER));
    }


    @Test
    @Transactional
    public void getLocRegionMastersByIdFiltering() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        Long id = locRegionMaster.getId();

        defaultLocRegionMasterShouldBeFound("id.equals=" + id);
        defaultLocRegionMasterShouldNotBeFound("id.notEquals=" + id);

        defaultLocRegionMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocRegionMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultLocRegionMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocRegionMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId equals to DEFAULT_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.equals=" + DEFAULT_REGION_ID);

        // Get all the locRegionMasterList where regionId equals to UPDATED_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.equals=" + UPDATED_REGION_ID);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId not equals to DEFAULT_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.notEquals=" + DEFAULT_REGION_ID);

        // Get all the locRegionMasterList where regionId not equals to UPDATED_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.notEquals=" + UPDATED_REGION_ID);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId in DEFAULT_REGION_ID or UPDATED_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.in=" + DEFAULT_REGION_ID + "," + UPDATED_REGION_ID);

        // Get all the locRegionMasterList where regionId equals to UPDATED_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.in=" + UPDATED_REGION_ID);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId is not null
        defaultLocRegionMasterShouldBeFound("regionId.specified=true");

        // Get all the locRegionMasterList where regionId is null
        defaultLocRegionMasterShouldNotBeFound("regionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId is greater than or equal to DEFAULT_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.greaterThanOrEqual=" + DEFAULT_REGION_ID);

        // Get all the locRegionMasterList where regionId is greater than or equal to UPDATED_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.greaterThanOrEqual=" + UPDATED_REGION_ID);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId is less than or equal to DEFAULT_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.lessThanOrEqual=" + DEFAULT_REGION_ID);

        // Get all the locRegionMasterList where regionId is less than or equal to SMALLER_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.lessThanOrEqual=" + SMALLER_REGION_ID);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId is less than DEFAULT_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.lessThan=" + DEFAULT_REGION_ID);

        // Get all the locRegionMasterList where regionId is less than UPDATED_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.lessThan=" + UPDATED_REGION_ID);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionId is greater than DEFAULT_REGION_ID
        defaultLocRegionMasterShouldNotBeFound("regionId.greaterThan=" + DEFAULT_REGION_ID);

        // Get all the locRegionMasterList where regionId is greater than SMALLER_REGION_ID
        defaultLocRegionMasterShouldBeFound("regionId.greaterThan=" + SMALLER_REGION_ID);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionName equals to DEFAULT_REGION_NAME
        defaultLocRegionMasterShouldBeFound("regionName.equals=" + DEFAULT_REGION_NAME);

        // Get all the locRegionMasterList where regionName equals to UPDATED_REGION_NAME
        defaultLocRegionMasterShouldNotBeFound("regionName.equals=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionName not equals to DEFAULT_REGION_NAME
        defaultLocRegionMasterShouldNotBeFound("regionName.notEquals=" + DEFAULT_REGION_NAME);

        // Get all the locRegionMasterList where regionName not equals to UPDATED_REGION_NAME
        defaultLocRegionMasterShouldBeFound("regionName.notEquals=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionNameIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionName in DEFAULT_REGION_NAME or UPDATED_REGION_NAME
        defaultLocRegionMasterShouldBeFound("regionName.in=" + DEFAULT_REGION_NAME + "," + UPDATED_REGION_NAME);

        // Get all the locRegionMasterList where regionName equals to UPDATED_REGION_NAME
        defaultLocRegionMasterShouldNotBeFound("regionName.in=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionName is not null
        defaultLocRegionMasterShouldBeFound("regionName.specified=true");

        // Get all the locRegionMasterList where regionName is null
        defaultLocRegionMasterShouldNotBeFound("regionName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocRegionMastersByRegionNameContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionName contains DEFAULT_REGION_NAME
        defaultLocRegionMasterShouldBeFound("regionName.contains=" + DEFAULT_REGION_NAME);

        // Get all the locRegionMasterList where regionName contains UPDATED_REGION_NAME
        defaultLocRegionMasterShouldNotBeFound("regionName.contains=" + UPDATED_REGION_NAME);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByRegionNameNotContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where regionName does not contain DEFAULT_REGION_NAME
        defaultLocRegionMasterShouldNotBeFound("regionName.doesNotContain=" + DEFAULT_REGION_NAME);

        // Get all the locRegionMasterList where regionName does not contain UPDATED_REGION_NAME
        defaultLocRegionMasterShouldBeFound("regionName.doesNotContain=" + UPDATED_REGION_NAME);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where description equals to DEFAULT_DESCRIPTION
        defaultLocRegionMasterShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the locRegionMasterList where description equals to UPDATED_DESCRIPTION
        defaultLocRegionMasterShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where description not equals to DEFAULT_DESCRIPTION
        defaultLocRegionMasterShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the locRegionMasterList where description not equals to UPDATED_DESCRIPTION
        defaultLocRegionMasterShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultLocRegionMasterShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the locRegionMasterList where description equals to UPDATED_DESCRIPTION
        defaultLocRegionMasterShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where description is not null
        defaultLocRegionMasterShouldBeFound("description.specified=true");

        // Get all the locRegionMasterList where description is null
        defaultLocRegionMasterShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocRegionMastersByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where description contains DEFAULT_DESCRIPTION
        defaultLocRegionMasterShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the locRegionMasterList where description contains UPDATED_DESCRIPTION
        defaultLocRegionMasterShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where description does not contain DEFAULT_DESCRIPTION
        defaultLocRegionMasterShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the locRegionMasterList where description does not contain UPDATED_DESCRIPTION
        defaultLocRegionMasterShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocRegionMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locRegionMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocRegionMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocRegionMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locRegionMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocRegionMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocRegionMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locRegionMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocRegionMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isDeleted is not null
        defaultLocRegionMasterShouldBeFound("isDeleted.specified=true");

        // Get all the locRegionMasterList where isDeleted is null
        defaultLocRegionMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocRegionMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocRegionMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locRegionMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultLocRegionMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocRegionMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locRegionMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocRegionMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocRegionMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locRegionMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocRegionMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocRegionMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locRegionMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocRegionMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocRegionMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locRegionMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocRegionMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdBy is not null
        defaultLocRegionMasterShouldBeFound("createdBy.specified=true");

        // Get all the locRegionMasterList where createdBy is null
        defaultLocRegionMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultLocRegionMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locRegionMasterList where createdBy contains UPDATED_CREATED_BY
        defaultLocRegionMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocRegionMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locRegionMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocRegionMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocRegionMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locRegionMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocRegionMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocRegionMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locRegionMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocRegionMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocRegionMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locRegionMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocRegionMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where createdDate is not null
        defaultLocRegionMasterShouldBeFound("createdDate.specified=true");

        // Get all the locRegionMasterList where createdDate is null
        defaultLocRegionMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locRegionMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locRegionMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locRegionMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedBy is not null
        defaultLocRegionMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locRegionMasterList where lastModifiedBy is null
        defaultLocRegionMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locRegionMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locRegionMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocRegionMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocRegionMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locRegionMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocRegionMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocRegionMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locRegionMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocRegionMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocRegionMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locRegionMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocRegionMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where lastModifiedDate is not null
        defaultLocRegionMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locRegionMasterList where lastModifiedDate is null
        defaultLocRegionMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsApprovedIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isApproved equals to DEFAULT_IS_APPROVED
        defaultLocRegionMasterShouldBeFound("isApproved.equals=" + DEFAULT_IS_APPROVED);

        // Get all the locRegionMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocRegionMasterShouldNotBeFound("isApproved.equals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsApprovedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isApproved not equals to DEFAULT_IS_APPROVED
        defaultLocRegionMasterShouldNotBeFound("isApproved.notEquals=" + DEFAULT_IS_APPROVED);

        // Get all the locRegionMasterList where isApproved not equals to UPDATED_IS_APPROVED
        defaultLocRegionMasterShouldBeFound("isApproved.notEquals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsApprovedIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isApproved in DEFAULT_IS_APPROVED or UPDATED_IS_APPROVED
        defaultLocRegionMasterShouldBeFound("isApproved.in=" + DEFAULT_IS_APPROVED + "," + UPDATED_IS_APPROVED);

        // Get all the locRegionMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocRegionMasterShouldNotBeFound("isApproved.in=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsApprovedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isApproved is not null
        defaultLocRegionMasterShouldBeFound("isApproved.specified=true");

        // Get all the locRegionMasterList where isApproved is null
        defaultLocRegionMasterShouldNotBeFound("isApproved.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocRegionMastersByIsApprovedContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isApproved contains DEFAULT_IS_APPROVED
        defaultLocRegionMasterShouldBeFound("isApproved.contains=" + DEFAULT_IS_APPROVED);

        // Get all the locRegionMasterList where isApproved contains UPDATED_IS_APPROVED
        defaultLocRegionMasterShouldNotBeFound("isApproved.contains=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersByIsApprovedNotContainsSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where isApproved does not contain DEFAULT_IS_APPROVED
        defaultLocRegionMasterShouldNotBeFound("isApproved.doesNotContain=" + DEFAULT_IS_APPROVED);

        // Get all the locRegionMasterList where isApproved does not contain UPDATED_IS_APPROVED
        defaultLocRegionMasterShouldBeFound("isApproved.doesNotContain=" + UPDATED_IS_APPROVED);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder equals to DEFAULT_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.equals=" + DEFAULT_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.equals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder not equals to DEFAULT_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.notEquals=" + DEFAULT_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder not equals to UPDATED_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.notEquals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsInShouldWork() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder in DEFAULT_SEQ_ORDER or UPDATED_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.in=" + DEFAULT_SEQ_ORDER + "," + UPDATED_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.in=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder is not null
        defaultLocRegionMasterShouldBeFound("seqOrder.specified=true");

        // Get all the locRegionMasterList where seqOrder is null
        defaultLocRegionMasterShouldNotBeFound("seqOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder is greater than or equal to DEFAULT_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.greaterThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder is greater than or equal to UPDATED_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.greaterThanOrEqual=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder is less than or equal to DEFAULT_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.lessThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder is less than or equal to SMALLER_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.lessThanOrEqual=" + SMALLER_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder is less than DEFAULT_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.lessThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder is less than UPDATED_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.lessThan=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocRegionMastersBySeqOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);

        // Get all the locRegionMasterList where seqOrder is greater than DEFAULT_SEQ_ORDER
        defaultLocRegionMasterShouldNotBeFound("seqOrder.greaterThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locRegionMasterList where seqOrder is greater than SMALLER_SEQ_ORDER
        defaultLocRegionMasterShouldBeFound("seqOrder.greaterThan=" + SMALLER_SEQ_ORDER);
    }


    @Test
    @Transactional
    public void getAllLocRegionMastersByLocStateMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locRegionMasterRepository.saveAndFlush(locRegionMaster);
        LocStateMaster locStateMaster = LocStateMasterResourceIT.createEntity(em);
        em.persist(locStateMaster);
        em.flush();
        locRegionMaster.addLocStateMaster(locStateMaster);
        locRegionMasterRepository.saveAndFlush(locRegionMaster);
        Long locStateMasterId = locStateMaster.getId();

        // Get all the locRegionMasterList where locStateMaster equals to locStateMasterId
        defaultLocRegionMasterShouldBeFound("locStateMasterId.equals=" + locStateMasterId);

        // Get all the locRegionMasterList where locStateMaster equals to locStateMasterId + 1
        defaultLocRegionMasterShouldNotBeFound("locStateMasterId.equals=" + (locStateMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocRegionMasterShouldBeFound(String filter) throws Exception {
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locRegionMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].regionId").value(hasItem(DEFAULT_REGION_ID)))
            .andExpect(jsonPath("$.[*].regionName").value(hasItem(DEFAULT_REGION_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));

        // Check, that the count call also returns 1
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocRegionMasterShouldNotBeFound(String filter) throws Exception {
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocRegionMaster() throws Exception {
        // Get the locRegionMaster
        restLocRegionMasterMockMvc.perform(get("/api/loc-region-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocRegionMaster() throws Exception {
        // Initialize the database
        locRegionMasterService.save(locRegionMaster);

        int databaseSizeBeforeUpdate = locRegionMasterRepository.findAll().size();

        // Update the locRegionMaster
        LocRegionMaster updatedLocRegionMaster = locRegionMasterRepository.findById(locRegionMaster.getId()).get();
        // Disconnect from session so that the updates on updatedLocRegionMaster are not directly saved in db
        em.detach(updatedLocRegionMaster);
        updatedLocRegionMaster
            .regionId(UPDATED_REGION_ID)
            .regionName(UPDATED_REGION_NAME)
            .description(UPDATED_DESCRIPTION)
            .isDeleted(UPDATED_IS_DELETED)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);

        restLocRegionMasterMockMvc.perform(put("/api/loc-region-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocRegionMaster)))
            .andExpect(status().isOk());

        // Validate the LocRegionMaster in the database
        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeUpdate);
        LocRegionMaster testLocRegionMaster = locRegionMasterList.get(locRegionMasterList.size() - 1);
        assertThat(testLocRegionMaster.getRegionId()).isEqualTo(UPDATED_REGION_ID);
        assertThat(testLocRegionMaster.getRegionName()).isEqualTo(UPDATED_REGION_NAME);
        assertThat(testLocRegionMaster.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testLocRegionMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocRegionMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocRegionMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocRegionMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testLocRegionMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocRegionMaster.getIsApproved()).isEqualTo(UPDATED_IS_APPROVED);
        assertThat(testLocRegionMaster.getSeqOrder()).isEqualTo(UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void updateNonExistingLocRegionMaster() throws Exception {
        int databaseSizeBeforeUpdate = locRegionMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocRegionMasterMockMvc.perform(put("/api/loc-region-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locRegionMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocRegionMaster in the database
        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocRegionMaster() throws Exception {
        // Initialize the database
        locRegionMasterService.save(locRegionMaster);

        int databaseSizeBeforeDelete = locRegionMasterRepository.findAll().size();

        // Delete the locRegionMaster
        restLocRegionMasterMockMvc.perform(delete("/api/loc-region-masters/{id}", locRegionMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocRegionMaster> locRegionMasterList = locRegionMasterRepository.findAll();
        assertThat(locRegionMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
