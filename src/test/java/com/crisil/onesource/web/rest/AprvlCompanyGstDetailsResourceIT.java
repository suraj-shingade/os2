package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.AprvlCompanyGstDetails;
import com.crisil.onesource.repository.AprvlCompanyGstDetailsRepository;
import com.crisil.onesource.service.AprvlCompanyGstDetailsService;
import com.crisil.onesource.service.dto.AprvlCompanyGstDetailsCriteria;
import com.crisil.onesource.service.AprvlCompanyGstDetailsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AprvlCompanyGstDetailsResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AprvlCompanyGstDetailsResourceIT {

    private static final Integer DEFAULT_CTLG_SRNO = 1;
    private static final Integer UPDATED_CTLG_SRNO = 2;
    private static final Integer SMALLER_CTLG_SRNO = 1 - 1;

    private static final String DEFAULT_RECORDID = "AAAAAAAAAA";
    private static final String UPDATED_RECORDID = "BBBBBBBBBB";

    private static final String DEFAULT_LINEOFBUSINESS = "AAAAAAAAAA";
    private static final String UPDATED_LINEOFBUSINESS = "BBBBBBBBBB";

    private static final String DEFAULT_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_GSTNUMBER = "AAAAAAAAAA";
    private static final String UPDATED_GSTNUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_ISSEZGST = "AAAAAAAAAA";
    private static final String UPDATED_ISSEZGST = "BBBBBBBBBB";

    private static final String DEFAULT_PREVIOUSGST = "AAAAAAAAAA";
    private static final String UPDATED_PREVIOUSGST = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATECODE = 1;
    private static final Integer UPDATED_STATECODE = 2;
    private static final Integer SMALLER_STATECODE = 1 - 1;

    private static final String DEFAULT_OPERATION = "A";
    private static final String UPDATED_OPERATION = "B";

    private static final String DEFAULT_HDELETED = "AAAAAAAAAA";
    private static final String UPDATED_HDELETED = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CTLG_IS_DELETED = "A";
    private static final String UPDATED_CTLG_IS_DELETED = "B";

    private static final Integer DEFAULT_JSON_STORE_ID = 1;
    private static final Integer UPDATED_JSON_STORE_ID = 2;
    private static final Integer SMALLER_JSON_STORE_ID = 1 - 1;

    private static final String DEFAULT_IS_DATA_PROCESSED = "A";
    private static final String UPDATED_IS_DATA_PROCESSED = "B";

    @Autowired
    private AprvlCompanyGstDetailsRepository aprvlCompanyGstDetailsRepository;

    @Autowired
    private AprvlCompanyGstDetailsService aprvlCompanyGstDetailsService;

    @Autowired
    private AprvlCompanyGstDetailsQueryService aprvlCompanyGstDetailsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAprvlCompanyGstDetailsMockMvc;

    private AprvlCompanyGstDetails aprvlCompanyGstDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AprvlCompanyGstDetails createEntity(EntityManager em) {
        AprvlCompanyGstDetails aprvlCompanyGstDetails = new AprvlCompanyGstDetails()
            .ctlgSrno(DEFAULT_CTLG_SRNO)
            .recordid(DEFAULT_RECORDID)
            .lineofbusiness(DEFAULT_LINEOFBUSINESS)
            .filename(DEFAULT_FILENAME)
            .gstnumber(DEFAULT_GSTNUMBER)
            .comments(DEFAULT_COMMENTS)
            .issezgst(DEFAULT_ISSEZGST)
            .previousgst(DEFAULT_PREVIOUSGST)
            .statecode(DEFAULT_STATECODE)
            .operation(DEFAULT_OPERATION)
            .hdeleted(DEFAULT_HDELETED)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .ctlgIsDeleted(DEFAULT_CTLG_IS_DELETED)
            .jsonStoreId(DEFAULT_JSON_STORE_ID)
            .isDataProcessed(DEFAULT_IS_DATA_PROCESSED);
        return aprvlCompanyGstDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AprvlCompanyGstDetails createUpdatedEntity(EntityManager em) {
        AprvlCompanyGstDetails aprvlCompanyGstDetails = new AprvlCompanyGstDetails()
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .recordid(UPDATED_RECORDID)
            .lineofbusiness(UPDATED_LINEOFBUSINESS)
            .filename(UPDATED_FILENAME)
            .gstnumber(UPDATED_GSTNUMBER)
            .comments(UPDATED_COMMENTS)
            .issezgst(UPDATED_ISSEZGST)
            .previousgst(UPDATED_PREVIOUSGST)
            .statecode(UPDATED_STATECODE)
            .operation(UPDATED_OPERATION)
            .hdeleted(UPDATED_HDELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED);
        return aprvlCompanyGstDetails;
    }

    @BeforeEach
    public void initTest() {
        aprvlCompanyGstDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createAprvlCompanyGstDetails() throws Exception {
        int databaseSizeBeforeCreate = aprvlCompanyGstDetailsRepository.findAll().size();
        // Create the AprvlCompanyGstDetails
        restAprvlCompanyGstDetailsMockMvc.perform(post("/api/aprvl-company-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyGstDetails)))
            .andExpect(status().isCreated());

        // Validate the AprvlCompanyGstDetails in the database
        List<AprvlCompanyGstDetails> aprvlCompanyGstDetailsList = aprvlCompanyGstDetailsRepository.findAll();
        assertThat(aprvlCompanyGstDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        AprvlCompanyGstDetails testAprvlCompanyGstDetails = aprvlCompanyGstDetailsList.get(aprvlCompanyGstDetailsList.size() - 1);
        assertThat(testAprvlCompanyGstDetails.getCtlgSrno()).isEqualTo(DEFAULT_CTLG_SRNO);
        assertThat(testAprvlCompanyGstDetails.getRecordid()).isEqualTo(DEFAULT_RECORDID);
        assertThat(testAprvlCompanyGstDetails.getLineofbusiness()).isEqualTo(DEFAULT_LINEOFBUSINESS);
        assertThat(testAprvlCompanyGstDetails.getFilename()).isEqualTo(DEFAULT_FILENAME);
        assertThat(testAprvlCompanyGstDetails.getGstnumber()).isEqualTo(DEFAULT_GSTNUMBER);
        assertThat(testAprvlCompanyGstDetails.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testAprvlCompanyGstDetails.getIssezgst()).isEqualTo(DEFAULT_ISSEZGST);
        assertThat(testAprvlCompanyGstDetails.getPreviousgst()).isEqualTo(DEFAULT_PREVIOUSGST);
        assertThat(testAprvlCompanyGstDetails.getStatecode()).isEqualTo(DEFAULT_STATECODE);
        assertThat(testAprvlCompanyGstDetails.getOperation()).isEqualTo(DEFAULT_OPERATION);
        assertThat(testAprvlCompanyGstDetails.getHdeleted()).isEqualTo(DEFAULT_HDELETED);
        assertThat(testAprvlCompanyGstDetails.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testAprvlCompanyGstDetails.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testAprvlCompanyGstDetails.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testAprvlCompanyGstDetails.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testAprvlCompanyGstDetails.getCtlgIsDeleted()).isEqualTo(DEFAULT_CTLG_IS_DELETED);
        assertThat(testAprvlCompanyGstDetails.getJsonStoreId()).isEqualTo(DEFAULT_JSON_STORE_ID);
        assertThat(testAprvlCompanyGstDetails.getIsDataProcessed()).isEqualTo(DEFAULT_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void createAprvlCompanyGstDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = aprvlCompanyGstDetailsRepository.findAll().size();

        // Create the AprvlCompanyGstDetails with an existing ID
        aprvlCompanyGstDetails.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAprvlCompanyGstDetailsMockMvc.perform(post("/api/aprvl-company-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyGstDetails)))
            .andExpect(status().isBadRequest());

        // Validate the AprvlCompanyGstDetails in the database
        List<AprvlCompanyGstDetails> aprvlCompanyGstDetailsList = aprvlCompanyGstDetailsRepository.findAll();
        assertThat(aprvlCompanyGstDetailsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCtlgSrnoIsRequired() throws Exception {
        int databaseSizeBeforeTest = aprvlCompanyGstDetailsRepository.findAll().size();
        // set the field null
        aprvlCompanyGstDetails.setCtlgSrno(null);

        // Create the AprvlCompanyGstDetails, which fails.


        restAprvlCompanyGstDetailsMockMvc.perform(post("/api/aprvl-company-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyGstDetails)))
            .andExpect(status().isBadRequest());

        List<AprvlCompanyGstDetails> aprvlCompanyGstDetailsList = aprvlCompanyGstDetailsRepository.findAll();
        assertThat(aprvlCompanyGstDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetails() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aprvlCompanyGstDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].recordid").value(hasItem(DEFAULT_RECORDID)))
            .andExpect(jsonPath("$.[*].lineofbusiness").value(hasItem(DEFAULT_LINEOFBUSINESS)))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME)))
            .andExpect(jsonPath("$.[*].gstnumber").value(hasItem(DEFAULT_GSTNUMBER)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].issezgst").value(hasItem(DEFAULT_ISSEZGST)))
            .andExpect(jsonPath("$.[*].previousgst").value(hasItem(DEFAULT_PREVIOUSGST)))
            .andExpect(jsonPath("$.[*].statecode").value(hasItem(DEFAULT_STATECODE)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].hdeleted").value(hasItem(DEFAULT_HDELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)));
    }
    
    @Test
    @Transactional
    public void getAprvlCompanyGstDetails() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get the aprvlCompanyGstDetails
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details/{id}", aprvlCompanyGstDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(aprvlCompanyGstDetails.getId().intValue()))
            .andExpect(jsonPath("$.ctlgSrno").value(DEFAULT_CTLG_SRNO))
            .andExpect(jsonPath("$.recordid").value(DEFAULT_RECORDID))
            .andExpect(jsonPath("$.lineofbusiness").value(DEFAULT_LINEOFBUSINESS))
            .andExpect(jsonPath("$.filename").value(DEFAULT_FILENAME))
            .andExpect(jsonPath("$.gstnumber").value(DEFAULT_GSTNUMBER))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.toString()))
            .andExpect(jsonPath("$.issezgst").value(DEFAULT_ISSEZGST))
            .andExpect(jsonPath("$.previousgst").value(DEFAULT_PREVIOUSGST))
            .andExpect(jsonPath("$.statecode").value(DEFAULT_STATECODE))
            .andExpect(jsonPath("$.operation").value(DEFAULT_OPERATION))
            .andExpect(jsonPath("$.hdeleted").value(DEFAULT_HDELETED))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.ctlgIsDeleted").value(DEFAULT_CTLG_IS_DELETED))
            .andExpect(jsonPath("$.jsonStoreId").value(DEFAULT_JSON_STORE_ID))
            .andExpect(jsonPath("$.isDataProcessed").value(DEFAULT_IS_DATA_PROCESSED));
    }


    @Test
    @Transactional
    public void getAprvlCompanyGstDetailsByIdFiltering() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        Long id = aprvlCompanyGstDetails.getId();

        defaultAprvlCompanyGstDetailsShouldBeFound("id.equals=" + id);
        defaultAprvlCompanyGstDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultAprvlCompanyGstDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAprvlCompanyGstDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultAprvlCompanyGstDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAprvlCompanyGstDetailsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno equals to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.equals=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.equals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno not equals to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.notEquals=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno not equals to UPDATED_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.notEquals=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno in DEFAULT_CTLG_SRNO or UPDATED_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.in=" + DEFAULT_CTLG_SRNO + "," + UPDATED_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno equals to UPDATED_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.in=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.specified=true");

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is greater than or equal to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.greaterThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is greater than or equal to UPDATED_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.greaterThanOrEqual=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is less than or equal to DEFAULT_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.lessThanOrEqual=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is less than or equal to SMALLER_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.lessThanOrEqual=" + SMALLER_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is less than DEFAULT_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.lessThan=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is less than UPDATED_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.lessThan=" + UPDATED_CTLG_SRNO);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgSrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is greater than DEFAULT_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgSrno.greaterThan=" + DEFAULT_CTLG_SRNO);

        // Get all the aprvlCompanyGstDetailsList where ctlgSrno is greater than SMALLER_CTLG_SRNO
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgSrno.greaterThan=" + SMALLER_CTLG_SRNO);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByRecordidIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where recordid equals to DEFAULT_RECORDID
        defaultAprvlCompanyGstDetailsShouldBeFound("recordid.equals=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyGstDetailsList where recordid equals to UPDATED_RECORDID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("recordid.equals=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByRecordidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where recordid not equals to DEFAULT_RECORDID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("recordid.notEquals=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyGstDetailsList where recordid not equals to UPDATED_RECORDID
        defaultAprvlCompanyGstDetailsShouldBeFound("recordid.notEquals=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByRecordidIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where recordid in DEFAULT_RECORDID or UPDATED_RECORDID
        defaultAprvlCompanyGstDetailsShouldBeFound("recordid.in=" + DEFAULT_RECORDID + "," + UPDATED_RECORDID);

        // Get all the aprvlCompanyGstDetailsList where recordid equals to UPDATED_RECORDID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("recordid.in=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByRecordidIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where recordid is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("recordid.specified=true");

        // Get all the aprvlCompanyGstDetailsList where recordid is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("recordid.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByRecordidContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where recordid contains DEFAULT_RECORDID
        defaultAprvlCompanyGstDetailsShouldBeFound("recordid.contains=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyGstDetailsList where recordid contains UPDATED_RECORDID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("recordid.contains=" + UPDATED_RECORDID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByRecordidNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where recordid does not contain DEFAULT_RECORDID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("recordid.doesNotContain=" + DEFAULT_RECORDID);

        // Get all the aprvlCompanyGstDetailsList where recordid does not contain UPDATED_RECORDID
        defaultAprvlCompanyGstDetailsShouldBeFound("recordid.doesNotContain=" + UPDATED_RECORDID);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLineofbusinessIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness equals to DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldBeFound("lineofbusiness.equals=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness equals to UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lineofbusiness.equals=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLineofbusinessIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness not equals to DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lineofbusiness.notEquals=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness not equals to UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldBeFound("lineofbusiness.notEquals=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLineofbusinessIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness in DEFAULT_LINEOFBUSINESS or UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldBeFound("lineofbusiness.in=" + DEFAULT_LINEOFBUSINESS + "," + UPDATED_LINEOFBUSINESS);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness equals to UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lineofbusiness.in=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLineofbusinessIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("lineofbusiness.specified=true");

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lineofbusiness.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLineofbusinessContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness contains DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldBeFound("lineofbusiness.contains=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness contains UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lineofbusiness.contains=" + UPDATED_LINEOFBUSINESS);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLineofbusinessNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness does not contain DEFAULT_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lineofbusiness.doesNotContain=" + DEFAULT_LINEOFBUSINESS);

        // Get all the aprvlCompanyGstDetailsList where lineofbusiness does not contain UPDATED_LINEOFBUSINESS
        defaultAprvlCompanyGstDetailsShouldBeFound("lineofbusiness.doesNotContain=" + UPDATED_LINEOFBUSINESS);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where filename equals to DEFAULT_FILENAME
        defaultAprvlCompanyGstDetailsShouldBeFound("filename.equals=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyGstDetailsList where filename equals to UPDATED_FILENAME
        defaultAprvlCompanyGstDetailsShouldNotBeFound("filename.equals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where filename not equals to DEFAULT_FILENAME
        defaultAprvlCompanyGstDetailsShouldNotBeFound("filename.notEquals=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyGstDetailsList where filename not equals to UPDATED_FILENAME
        defaultAprvlCompanyGstDetailsShouldBeFound("filename.notEquals=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where filename in DEFAULT_FILENAME or UPDATED_FILENAME
        defaultAprvlCompanyGstDetailsShouldBeFound("filename.in=" + DEFAULT_FILENAME + "," + UPDATED_FILENAME);

        // Get all the aprvlCompanyGstDetailsList where filename equals to UPDATED_FILENAME
        defaultAprvlCompanyGstDetailsShouldNotBeFound("filename.in=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where filename is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("filename.specified=true");

        // Get all the aprvlCompanyGstDetailsList where filename is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("filename.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByFilenameContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where filename contains DEFAULT_FILENAME
        defaultAprvlCompanyGstDetailsShouldBeFound("filename.contains=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyGstDetailsList where filename contains UPDATED_FILENAME
        defaultAprvlCompanyGstDetailsShouldNotBeFound("filename.contains=" + UPDATED_FILENAME);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where filename does not contain DEFAULT_FILENAME
        defaultAprvlCompanyGstDetailsShouldNotBeFound("filename.doesNotContain=" + DEFAULT_FILENAME);

        // Get all the aprvlCompanyGstDetailsList where filename does not contain UPDATED_FILENAME
        defaultAprvlCompanyGstDetailsShouldBeFound("filename.doesNotContain=" + UPDATED_FILENAME);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByGstnumberIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where gstnumber equals to DEFAULT_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldBeFound("gstnumber.equals=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyGstDetailsList where gstnumber equals to UPDATED_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldNotBeFound("gstnumber.equals=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByGstnumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where gstnumber not equals to DEFAULT_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldNotBeFound("gstnumber.notEquals=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyGstDetailsList where gstnumber not equals to UPDATED_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldBeFound("gstnumber.notEquals=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByGstnumberIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where gstnumber in DEFAULT_GSTNUMBER or UPDATED_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldBeFound("gstnumber.in=" + DEFAULT_GSTNUMBER + "," + UPDATED_GSTNUMBER);

        // Get all the aprvlCompanyGstDetailsList where gstnumber equals to UPDATED_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldNotBeFound("gstnumber.in=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByGstnumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where gstnumber is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("gstnumber.specified=true");

        // Get all the aprvlCompanyGstDetailsList where gstnumber is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("gstnumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByGstnumberContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where gstnumber contains DEFAULT_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldBeFound("gstnumber.contains=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyGstDetailsList where gstnumber contains UPDATED_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldNotBeFound("gstnumber.contains=" + UPDATED_GSTNUMBER);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByGstnumberNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where gstnumber does not contain DEFAULT_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldNotBeFound("gstnumber.doesNotContain=" + DEFAULT_GSTNUMBER);

        // Get all the aprvlCompanyGstDetailsList where gstnumber does not contain UPDATED_GSTNUMBER
        defaultAprvlCompanyGstDetailsShouldBeFound("gstnumber.doesNotContain=" + UPDATED_GSTNUMBER);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIssezgstIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where issezgst equals to DEFAULT_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldBeFound("issezgst.equals=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyGstDetailsList where issezgst equals to UPDATED_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("issezgst.equals=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIssezgstIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where issezgst not equals to DEFAULT_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("issezgst.notEquals=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyGstDetailsList where issezgst not equals to UPDATED_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldBeFound("issezgst.notEquals=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIssezgstIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where issezgst in DEFAULT_ISSEZGST or UPDATED_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldBeFound("issezgst.in=" + DEFAULT_ISSEZGST + "," + UPDATED_ISSEZGST);

        // Get all the aprvlCompanyGstDetailsList where issezgst equals to UPDATED_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("issezgst.in=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIssezgstIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where issezgst is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("issezgst.specified=true");

        // Get all the aprvlCompanyGstDetailsList where issezgst is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("issezgst.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIssezgstContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where issezgst contains DEFAULT_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldBeFound("issezgst.contains=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyGstDetailsList where issezgst contains UPDATED_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("issezgst.contains=" + UPDATED_ISSEZGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIssezgstNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where issezgst does not contain DEFAULT_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("issezgst.doesNotContain=" + DEFAULT_ISSEZGST);

        // Get all the aprvlCompanyGstDetailsList where issezgst does not contain UPDATED_ISSEZGST
        defaultAprvlCompanyGstDetailsShouldBeFound("issezgst.doesNotContain=" + UPDATED_ISSEZGST);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByPreviousgstIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where previousgst equals to DEFAULT_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldBeFound("previousgst.equals=" + DEFAULT_PREVIOUSGST);

        // Get all the aprvlCompanyGstDetailsList where previousgst equals to UPDATED_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("previousgst.equals=" + UPDATED_PREVIOUSGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByPreviousgstIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where previousgst not equals to DEFAULT_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("previousgst.notEquals=" + DEFAULT_PREVIOUSGST);

        // Get all the aprvlCompanyGstDetailsList where previousgst not equals to UPDATED_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldBeFound("previousgst.notEquals=" + UPDATED_PREVIOUSGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByPreviousgstIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where previousgst in DEFAULT_PREVIOUSGST or UPDATED_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldBeFound("previousgst.in=" + DEFAULT_PREVIOUSGST + "," + UPDATED_PREVIOUSGST);

        // Get all the aprvlCompanyGstDetailsList where previousgst equals to UPDATED_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("previousgst.in=" + UPDATED_PREVIOUSGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByPreviousgstIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where previousgst is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("previousgst.specified=true");

        // Get all the aprvlCompanyGstDetailsList where previousgst is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("previousgst.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByPreviousgstContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where previousgst contains DEFAULT_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldBeFound("previousgst.contains=" + DEFAULT_PREVIOUSGST);

        // Get all the aprvlCompanyGstDetailsList where previousgst contains UPDATED_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("previousgst.contains=" + UPDATED_PREVIOUSGST);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByPreviousgstNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where previousgst does not contain DEFAULT_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldNotBeFound("previousgst.doesNotContain=" + DEFAULT_PREVIOUSGST);

        // Get all the aprvlCompanyGstDetailsList where previousgst does not contain UPDATED_PREVIOUSGST
        defaultAprvlCompanyGstDetailsShouldBeFound("previousgst.doesNotContain=" + UPDATED_PREVIOUSGST);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode equals to DEFAULT_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.equals=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode equals to UPDATED_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.equals=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode not equals to DEFAULT_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.notEquals=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode not equals to UPDATED_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.notEquals=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode in DEFAULT_STATECODE or UPDATED_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.in=" + DEFAULT_STATECODE + "," + UPDATED_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode equals to UPDATED_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.in=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.specified=true");

        // Get all the aprvlCompanyGstDetailsList where statecode is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode is greater than or equal to DEFAULT_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.greaterThanOrEqual=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode is greater than or equal to UPDATED_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.greaterThanOrEqual=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode is less than or equal to DEFAULT_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.lessThanOrEqual=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode is less than or equal to SMALLER_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.lessThanOrEqual=" + SMALLER_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode is less than DEFAULT_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.lessThan=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode is less than UPDATED_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.lessThan=" + UPDATED_STATECODE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByStatecodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where statecode is greater than DEFAULT_STATECODE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("statecode.greaterThan=" + DEFAULT_STATECODE);

        // Get all the aprvlCompanyGstDetailsList where statecode is greater than SMALLER_STATECODE
        defaultAprvlCompanyGstDetailsShouldBeFound("statecode.greaterThan=" + SMALLER_STATECODE);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where operation equals to DEFAULT_OPERATION
        defaultAprvlCompanyGstDetailsShouldBeFound("operation.equals=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyGstDetailsList where operation equals to UPDATED_OPERATION
        defaultAprvlCompanyGstDetailsShouldNotBeFound("operation.equals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByOperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where operation not equals to DEFAULT_OPERATION
        defaultAprvlCompanyGstDetailsShouldNotBeFound("operation.notEquals=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyGstDetailsList where operation not equals to UPDATED_OPERATION
        defaultAprvlCompanyGstDetailsShouldBeFound("operation.notEquals=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByOperationIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where operation in DEFAULT_OPERATION or UPDATED_OPERATION
        defaultAprvlCompanyGstDetailsShouldBeFound("operation.in=" + DEFAULT_OPERATION + "," + UPDATED_OPERATION);

        // Get all the aprvlCompanyGstDetailsList where operation equals to UPDATED_OPERATION
        defaultAprvlCompanyGstDetailsShouldNotBeFound("operation.in=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByOperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where operation is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("operation.specified=true");

        // Get all the aprvlCompanyGstDetailsList where operation is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("operation.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByOperationContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where operation contains DEFAULT_OPERATION
        defaultAprvlCompanyGstDetailsShouldBeFound("operation.contains=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyGstDetailsList where operation contains UPDATED_OPERATION
        defaultAprvlCompanyGstDetailsShouldNotBeFound("operation.contains=" + UPDATED_OPERATION);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByOperationNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where operation does not contain DEFAULT_OPERATION
        defaultAprvlCompanyGstDetailsShouldNotBeFound("operation.doesNotContain=" + DEFAULT_OPERATION);

        // Get all the aprvlCompanyGstDetailsList where operation does not contain UPDATED_OPERATION
        defaultAprvlCompanyGstDetailsShouldBeFound("operation.doesNotContain=" + UPDATED_OPERATION);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByHdeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where hdeleted equals to DEFAULT_HDELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("hdeleted.equals=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyGstDetailsList where hdeleted equals to UPDATED_HDELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("hdeleted.equals=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByHdeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where hdeleted not equals to DEFAULT_HDELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("hdeleted.notEquals=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyGstDetailsList where hdeleted not equals to UPDATED_HDELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("hdeleted.notEquals=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByHdeletedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where hdeleted in DEFAULT_HDELETED or UPDATED_HDELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("hdeleted.in=" + DEFAULT_HDELETED + "," + UPDATED_HDELETED);

        // Get all the aprvlCompanyGstDetailsList where hdeleted equals to UPDATED_HDELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("hdeleted.in=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByHdeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where hdeleted is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("hdeleted.specified=true");

        // Get all the aprvlCompanyGstDetailsList where hdeleted is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("hdeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByHdeletedContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where hdeleted contains DEFAULT_HDELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("hdeleted.contains=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyGstDetailsList where hdeleted contains UPDATED_HDELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("hdeleted.contains=" + UPDATED_HDELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByHdeletedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where hdeleted does not contain DEFAULT_HDELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("hdeleted.doesNotContain=" + DEFAULT_HDELETED);

        // Get all the aprvlCompanyGstDetailsList where hdeleted does not contain UPDATED_HDELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("hdeleted.doesNotContain=" + UPDATED_HDELETED);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdDate equals to DEFAULT_CREATED_DATE
        defaultAprvlCompanyGstDetailsShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the aprvlCompanyGstDetailsList where createdDate equals to UPDATED_CREATED_DATE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the aprvlCompanyGstDetailsList where createdDate not equals to UPDATED_CREATED_DATE
        defaultAprvlCompanyGstDetailsShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultAprvlCompanyGstDetailsShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the aprvlCompanyGstDetailsList where createdDate equals to UPDATED_CREATED_DATE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdDate is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("createdDate.specified=true");

        // Get all the aprvlCompanyGstDetailsList where createdDate is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedDate.specified=true");

        // Get all the aprvlCompanyGstDetailsList where lastModifiedDate is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdBy equals to DEFAULT_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyGstDetailsList where createdBy equals to UPDATED_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdBy not equals to DEFAULT_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyGstDetailsList where createdBy not equals to UPDATED_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the aprvlCompanyGstDetailsList where createdBy equals to UPDATED_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdBy is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("createdBy.specified=true");

        // Get all the aprvlCompanyGstDetailsList where createdBy is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdBy contains DEFAULT_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyGstDetailsList where createdBy contains UPDATED_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where createdBy does not contain DEFAULT_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the aprvlCompanyGstDetailsList where createdBy does not contain UPDATED_CREATED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedBy.specified=true");

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the aprvlCompanyGstDetailsList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultAprvlCompanyGstDetailsShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted equals to DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgIsDeleted.equals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgIsDeleted.equals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted not equals to DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgIsDeleted.notEquals=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted not equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgIsDeleted.notEquals=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted in DEFAULT_CTLG_IS_DELETED or UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgIsDeleted.in=" + DEFAULT_CTLG_IS_DELETED + "," + UPDATED_CTLG_IS_DELETED);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted equals to UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgIsDeleted.in=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgIsDeleted.specified=true");

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgIsDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted contains DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgIsDeleted.contains=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted contains UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgIsDeleted.contains=" + UPDATED_CTLG_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByCtlgIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted does not contain DEFAULT_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("ctlgIsDeleted.doesNotContain=" + DEFAULT_CTLG_IS_DELETED);

        // Get all the aprvlCompanyGstDetailsList where ctlgIsDeleted does not contain UPDATED_CTLG_IS_DELETED
        defaultAprvlCompanyGstDetailsShouldBeFound("ctlgIsDeleted.doesNotContain=" + UPDATED_CTLG_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId equals to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.equals=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.equals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId not equals to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.notEquals=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId not equals to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.notEquals=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId in DEFAULT_JSON_STORE_ID or UPDATED_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.in=" + DEFAULT_JSON_STORE_ID + "," + UPDATED_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId equals to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.in=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.specified=true");

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.specified=false");
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is greater than or equal to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.greaterThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is greater than or equal to UPDATED_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.greaterThanOrEqual=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is less than or equal to DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.lessThanOrEqual=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is less than or equal to SMALLER_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.lessThanOrEqual=" + SMALLER_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsLessThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is less than DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.lessThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is less than UPDATED_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.lessThan=" + UPDATED_JSON_STORE_ID);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByJsonStoreIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is greater than DEFAULT_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldNotBeFound("jsonStoreId.greaterThan=" + DEFAULT_JSON_STORE_ID);

        // Get all the aprvlCompanyGstDetailsList where jsonStoreId is greater than SMALLER_JSON_STORE_ID
        defaultAprvlCompanyGstDetailsShouldBeFound("jsonStoreId.greaterThan=" + SMALLER_JSON_STORE_ID);
    }


    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIsDataProcessedIsEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed equals to DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldBeFound("isDataProcessed.equals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("isDataProcessed.equals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIsDataProcessedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed not equals to DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("isDataProcessed.notEquals=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed not equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldBeFound("isDataProcessed.notEquals=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIsDataProcessedIsInShouldWork() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed in DEFAULT_IS_DATA_PROCESSED or UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldBeFound("isDataProcessed.in=" + DEFAULT_IS_DATA_PROCESSED + "," + UPDATED_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed equals to UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("isDataProcessed.in=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIsDataProcessedIsNullOrNotNull() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed is not null
        defaultAprvlCompanyGstDetailsShouldBeFound("isDataProcessed.specified=true");

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed is null
        defaultAprvlCompanyGstDetailsShouldNotBeFound("isDataProcessed.specified=false");
    }
                @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIsDataProcessedContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed contains DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldBeFound("isDataProcessed.contains=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed contains UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("isDataProcessed.contains=" + UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void getAllAprvlCompanyGstDetailsByIsDataProcessedNotContainsSomething() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsRepository.saveAndFlush(aprvlCompanyGstDetails);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed does not contain DEFAULT_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldNotBeFound("isDataProcessed.doesNotContain=" + DEFAULT_IS_DATA_PROCESSED);

        // Get all the aprvlCompanyGstDetailsList where isDataProcessed does not contain UPDATED_IS_DATA_PROCESSED
        defaultAprvlCompanyGstDetailsShouldBeFound("isDataProcessed.doesNotContain=" + UPDATED_IS_DATA_PROCESSED);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAprvlCompanyGstDetailsShouldBeFound(String filter) throws Exception {
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(aprvlCompanyGstDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].ctlgSrno").value(hasItem(DEFAULT_CTLG_SRNO)))
            .andExpect(jsonPath("$.[*].recordid").value(hasItem(DEFAULT_RECORDID)))
            .andExpect(jsonPath("$.[*].lineofbusiness").value(hasItem(DEFAULT_LINEOFBUSINESS)))
            .andExpect(jsonPath("$.[*].filename").value(hasItem(DEFAULT_FILENAME)))
            .andExpect(jsonPath("$.[*].gstnumber").value(hasItem(DEFAULT_GSTNUMBER)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.toString())))
            .andExpect(jsonPath("$.[*].issezgst").value(hasItem(DEFAULT_ISSEZGST)))
            .andExpect(jsonPath("$.[*].previousgst").value(hasItem(DEFAULT_PREVIOUSGST)))
            .andExpect(jsonPath("$.[*].statecode").value(hasItem(DEFAULT_STATECODE)))
            .andExpect(jsonPath("$.[*].operation").value(hasItem(DEFAULT_OPERATION)))
            .andExpect(jsonPath("$.[*].hdeleted").value(hasItem(DEFAULT_HDELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].ctlgIsDeleted").value(hasItem(DEFAULT_CTLG_IS_DELETED)))
            .andExpect(jsonPath("$.[*].jsonStoreId").value(hasItem(DEFAULT_JSON_STORE_ID)))
            .andExpect(jsonPath("$.[*].isDataProcessed").value(hasItem(DEFAULT_IS_DATA_PROCESSED)));

        // Check, that the count call also returns 1
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAprvlCompanyGstDetailsShouldNotBeFound(String filter) throws Exception {
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingAprvlCompanyGstDetails() throws Exception {
        // Get the aprvlCompanyGstDetails
        restAprvlCompanyGstDetailsMockMvc.perform(get("/api/aprvl-company-gst-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAprvlCompanyGstDetails() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsService.save(aprvlCompanyGstDetails);

        int databaseSizeBeforeUpdate = aprvlCompanyGstDetailsRepository.findAll().size();

        // Update the aprvlCompanyGstDetails
        AprvlCompanyGstDetails updatedAprvlCompanyGstDetails = aprvlCompanyGstDetailsRepository.findById(aprvlCompanyGstDetails.getId()).get();
        // Disconnect from session so that the updates on updatedAprvlCompanyGstDetails are not directly saved in db
        em.detach(updatedAprvlCompanyGstDetails);
        updatedAprvlCompanyGstDetails
            .ctlgSrno(UPDATED_CTLG_SRNO)
            .recordid(UPDATED_RECORDID)
            .lineofbusiness(UPDATED_LINEOFBUSINESS)
            .filename(UPDATED_FILENAME)
            .gstnumber(UPDATED_GSTNUMBER)
            .comments(UPDATED_COMMENTS)
            .issezgst(UPDATED_ISSEZGST)
            .previousgst(UPDATED_PREVIOUSGST)
            .statecode(UPDATED_STATECODE)
            .operation(UPDATED_OPERATION)
            .hdeleted(UPDATED_HDELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .ctlgIsDeleted(UPDATED_CTLG_IS_DELETED)
            .jsonStoreId(UPDATED_JSON_STORE_ID)
            .isDataProcessed(UPDATED_IS_DATA_PROCESSED);

        restAprvlCompanyGstDetailsMockMvc.perform(put("/api/aprvl-company-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAprvlCompanyGstDetails)))
            .andExpect(status().isOk());

        // Validate the AprvlCompanyGstDetails in the database
        List<AprvlCompanyGstDetails> aprvlCompanyGstDetailsList = aprvlCompanyGstDetailsRepository.findAll();
        assertThat(aprvlCompanyGstDetailsList).hasSize(databaseSizeBeforeUpdate);
        AprvlCompanyGstDetails testAprvlCompanyGstDetails = aprvlCompanyGstDetailsList.get(aprvlCompanyGstDetailsList.size() - 1);
        assertThat(testAprvlCompanyGstDetails.getCtlgSrno()).isEqualTo(UPDATED_CTLG_SRNO);
        assertThat(testAprvlCompanyGstDetails.getRecordid()).isEqualTo(UPDATED_RECORDID);
        assertThat(testAprvlCompanyGstDetails.getLineofbusiness()).isEqualTo(UPDATED_LINEOFBUSINESS);
        assertThat(testAprvlCompanyGstDetails.getFilename()).isEqualTo(UPDATED_FILENAME);
        assertThat(testAprvlCompanyGstDetails.getGstnumber()).isEqualTo(UPDATED_GSTNUMBER);
        assertThat(testAprvlCompanyGstDetails.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testAprvlCompanyGstDetails.getIssezgst()).isEqualTo(UPDATED_ISSEZGST);
        assertThat(testAprvlCompanyGstDetails.getPreviousgst()).isEqualTo(UPDATED_PREVIOUSGST);
        assertThat(testAprvlCompanyGstDetails.getStatecode()).isEqualTo(UPDATED_STATECODE);
        assertThat(testAprvlCompanyGstDetails.getOperation()).isEqualTo(UPDATED_OPERATION);
        assertThat(testAprvlCompanyGstDetails.getHdeleted()).isEqualTo(UPDATED_HDELETED);
        assertThat(testAprvlCompanyGstDetails.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testAprvlCompanyGstDetails.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testAprvlCompanyGstDetails.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testAprvlCompanyGstDetails.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testAprvlCompanyGstDetails.getCtlgIsDeleted()).isEqualTo(UPDATED_CTLG_IS_DELETED);
        assertThat(testAprvlCompanyGstDetails.getJsonStoreId()).isEqualTo(UPDATED_JSON_STORE_ID);
        assertThat(testAprvlCompanyGstDetails.getIsDataProcessed()).isEqualTo(UPDATED_IS_DATA_PROCESSED);
    }

    @Test
    @Transactional
    public void updateNonExistingAprvlCompanyGstDetails() throws Exception {
        int databaseSizeBeforeUpdate = aprvlCompanyGstDetailsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAprvlCompanyGstDetailsMockMvc.perform(put("/api/aprvl-company-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(aprvlCompanyGstDetails)))
            .andExpect(status().isBadRequest());

        // Validate the AprvlCompanyGstDetails in the database
        List<AprvlCompanyGstDetails> aprvlCompanyGstDetailsList = aprvlCompanyGstDetailsRepository.findAll();
        assertThat(aprvlCompanyGstDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAprvlCompanyGstDetails() throws Exception {
        // Initialize the database
        aprvlCompanyGstDetailsService.save(aprvlCompanyGstDetails);

        int databaseSizeBeforeDelete = aprvlCompanyGstDetailsRepository.findAll().size();

        // Delete the aprvlCompanyGstDetails
        restAprvlCompanyGstDetailsMockMvc.perform(delete("/api/aprvl-company-gst-details/{id}", aprvlCompanyGstDetails.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AprvlCompanyGstDetails> aprvlCompanyGstDetailsList = aprvlCompanyGstDetailsRepository.findAll();
        assertThat(aprvlCompanyGstDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
