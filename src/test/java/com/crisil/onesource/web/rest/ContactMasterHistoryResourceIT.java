package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.ContactMasterHistory;
import com.crisil.onesource.repository.ContactMasterHistoryRepository;
import com.crisil.onesource.service.ContactMasterHistoryService;
import com.crisil.onesource.service.dto.ContactMasterHistoryCriteria;
import com.crisil.onesource.service.ContactMasterHistoryQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactMasterHistoryResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactMasterHistoryResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SALUTATION = "AAAAAAAAAA";
    private static final String UPDATED_SALUTATION = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_3 = "BBBBBBBBBB";

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_PIN = "AAAAAAAAAA";
    private static final String UPDATED_PIN = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUM = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_FAX_NUM = "AAAAAAAAAA";
    private static final String UPDATED_FAX_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NUM = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_KEY_PERSON = "AAAAAAAAAA";
    private static final String UPDATED_KEY_PERSON = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LEVEL_IMPORTANCE = "AAAAAAAAAA";
    private static final String UPDATED_LEVEL_IMPORTANCE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REMOVE_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REMOVE_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_CITY_ID = "AAAAAAAAAA";
    private static final String UPDATED_CITY_ID = "BBBBBBBBBB";

    private static final String DEFAULT_O_FLAG = "AAA";
    private static final String UPDATED_O_FLAG = "BBB";

    private static final String DEFAULT_OPT_OUT = "AAAAAAAAAA";
    private static final String UPDATED_OPT_OUT = "BBBBBBBBBB";

    private static final String DEFAULT_OFA_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_OFA_CONTACT_ID = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_OPT_OUT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_OPT_OUT_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_OPT_OUT_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_FAX_OPTOUT = "AAAAAAAAAA";
    private static final String UPDATED_FAX_OPTOUT = "BBBBBBBBBB";

    private static final String DEFAULT_DONOTCALL = "AAAAAAAAAA";
    private static final String UPDATED_DONOTCALL = "BBBBBBBBBB";

    private static final String DEFAULT_DNSEND_DIRECT_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_DNSEND_DIRECT_MAIL = "BBBBBBBBBB";

    private static final String DEFAULT_DNSHARE_OUTSIDE_MHP = "AAAAAAAAAA";
    private static final String UPDATED_DNSHARE_OUTSIDE_MHP = "BBBBBBBBBB";

    private static final String DEFAULT_OUTSIDE_OF_CONTACT = "AAAAAAAAAA";
    private static final String UPDATED_OUTSIDE_OF_CONTACT = "BBBBBBBBBB";

    private static final String DEFAULT_DNSHARE_OUTSIDE_SNP = "AAAAAAAAAA";
    private static final String UPDATED_DNSHARE_OUTSIDE_SNP = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_OPTOUT = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_OPTOUT = "BBBBBBBBBB";

    private static final String DEFAULT_SMS_OPTOUT = "A";
    private static final String UPDATED_SMS_OPTOUT = "B";

    private static final String DEFAULT_IS_SEZ_CONTACT_FLAG = "A";
    private static final String UPDATED_IS_SEZ_CONTACT_FLAG = "B";

    private static final String DEFAULT_IS_EXEMPT_CONTACT_FLAG = "AAAAAAAAAA";
    private static final String UPDATED_IS_EXEMPT_CONTACT_FLAG = "BBBBBBBBBB";

    private static final String DEFAULT_GST_NOT_APPLICABLE = "A";
    private static final String UPDATED_GST_NOT_APPLICABLE = "B";

    private static final String DEFAULT_BILLING_CONTACT = "A";
    private static final String UPDATED_BILLING_CONTACT = "B";

    private static final String DEFAULT_GST_PARTY_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_GST_PARTY_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_TAN = "AAAAAAAAAA";
    private static final String UPDATED_TAN = "BBBBBBBBBB";

    private static final String DEFAULT_GST_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_GST_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TDS_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TDS_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private ContactMasterHistoryRepository contactMasterHistoryRepository;

    @Autowired
    private ContactMasterHistoryService contactMasterHistoryService;

    @Autowired
    private ContactMasterHistoryQueryService contactMasterHistoryQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactMasterHistoryMockMvc;

    private ContactMasterHistory contactMasterHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMasterHistory createEntity(EntityManager em) {
        ContactMasterHistory contactMasterHistory = new ContactMasterHistory()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .salutation(DEFAULT_SALUTATION)
            .address1(DEFAULT_ADDRESS_1)
            .address2(DEFAULT_ADDRESS_2)
            .address3(DEFAULT_ADDRESS_3)
            .designation(DEFAULT_DESIGNATION)
            .department(DEFAULT_DEPARTMENT)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .pin(DEFAULT_PIN)
            .phoneNum(DEFAULT_PHONE_NUM)
            .faxNum(DEFAULT_FAX_NUM)
            .email(DEFAULT_EMAIL)
            .mobileNum(DEFAULT_MOBILE_NUM)
            .recordId(DEFAULT_RECORD_ID)
            .companyCode(DEFAULT_COMPANY_CODE)
            .comments(DEFAULT_COMMENTS)
            .keyPerson(DEFAULT_KEY_PERSON)
            .contactId(DEFAULT_CONTACT_ID)
            .levelImportance(DEFAULT_LEVEL_IMPORTANCE)
            .status(DEFAULT_STATUS)
            .removeReason(DEFAULT_REMOVE_REASON)
            .cityId(DEFAULT_CITY_ID)
            .oFlag(DEFAULT_O_FLAG)
            .optOut(DEFAULT_OPT_OUT)
            .ofaContactId(DEFAULT_OFA_CONTACT_ID)
            .optOutDate(DEFAULT_OPT_OUT_DATE)
            .faxOptout(DEFAULT_FAX_OPTOUT)
            .donotcall(DEFAULT_DONOTCALL)
            .dnsendDirectMail(DEFAULT_DNSEND_DIRECT_MAIL)
            .dnshareOutsideMhp(DEFAULT_DNSHARE_OUTSIDE_MHP)
            .outsideOfContact(DEFAULT_OUTSIDE_OF_CONTACT)
            .dnshareOutsideSnp(DEFAULT_DNSHARE_OUTSIDE_SNP)
            .emailOptout(DEFAULT_EMAIL_OPTOUT)
            .smsOptout(DEFAULT_SMS_OPTOUT)
            .isSezContactFlag(DEFAULT_IS_SEZ_CONTACT_FLAG)
            .isExemptContactFlag(DEFAULT_IS_EXEMPT_CONTACT_FLAG)
            .gstNotApplicable(DEFAULT_GST_NOT_APPLICABLE)
            .billingContact(DEFAULT_BILLING_CONTACT)
            .gstPartyType(DEFAULT_GST_PARTY_TYPE)
            .tan(DEFAULT_TAN)
            .gstNumber(DEFAULT_GST_NUMBER)
            .tdsNumber(DEFAULT_TDS_NUMBER)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED);
        return contactMasterHistory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMasterHistory createUpdatedEntity(EntityManager em) {
        ContactMasterHistory contactMasterHistory = new ContactMasterHistory()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .salutation(UPDATED_SALUTATION)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .designation(UPDATED_DESIGNATION)
            .department(UPDATED_DEPARTMENT)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .pin(UPDATED_PIN)
            .phoneNum(UPDATED_PHONE_NUM)
            .faxNum(UPDATED_FAX_NUM)
            .email(UPDATED_EMAIL)
            .mobileNum(UPDATED_MOBILE_NUM)
            .recordId(UPDATED_RECORD_ID)
            .companyCode(UPDATED_COMPANY_CODE)
            .comments(UPDATED_COMMENTS)
            .keyPerson(UPDATED_KEY_PERSON)
            .contactId(UPDATED_CONTACT_ID)
            .levelImportance(UPDATED_LEVEL_IMPORTANCE)
            .status(UPDATED_STATUS)
            .removeReason(UPDATED_REMOVE_REASON)
            .cityId(UPDATED_CITY_ID)
            .oFlag(UPDATED_O_FLAG)
            .optOut(UPDATED_OPT_OUT)
            .ofaContactId(UPDATED_OFA_CONTACT_ID)
            .optOutDate(UPDATED_OPT_OUT_DATE)
            .faxOptout(UPDATED_FAX_OPTOUT)
            .donotcall(UPDATED_DONOTCALL)
            .dnsendDirectMail(UPDATED_DNSEND_DIRECT_MAIL)
            .dnshareOutsideMhp(UPDATED_DNSHARE_OUTSIDE_MHP)
            .outsideOfContact(UPDATED_OUTSIDE_OF_CONTACT)
            .dnshareOutsideSnp(UPDATED_DNSHARE_OUTSIDE_SNP)
            .emailOptout(UPDATED_EMAIL_OPTOUT)
            .smsOptout(UPDATED_SMS_OPTOUT)
            .isSezContactFlag(UPDATED_IS_SEZ_CONTACT_FLAG)
            .isExemptContactFlag(UPDATED_IS_EXEMPT_CONTACT_FLAG)
            .gstNotApplicable(UPDATED_GST_NOT_APPLICABLE)
            .billingContact(UPDATED_BILLING_CONTACT)
            .gstPartyType(UPDATED_GST_PARTY_TYPE)
            .tan(UPDATED_TAN)
            .gstNumber(UPDATED_GST_NUMBER)
            .tdsNumber(UPDATED_TDS_NUMBER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);
        return contactMasterHistory;
    }

    @BeforeEach
    public void initTest() {
        contactMasterHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactMasterHistory() throws Exception {
        int databaseSizeBeforeCreate = contactMasterHistoryRepository.findAll().size();
        // Create the ContactMasterHistory
        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isCreated());

        // Validate the ContactMasterHistory in the database
        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        ContactMasterHistory testContactMasterHistory = contactMasterHistoryList.get(contactMasterHistoryList.size() - 1);
        assertThat(testContactMasterHistory.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testContactMasterHistory.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testContactMasterHistory.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testContactMasterHistory.getSalutation()).isEqualTo(DEFAULT_SALUTATION);
        assertThat(testContactMasterHistory.getAddress1()).isEqualTo(DEFAULT_ADDRESS_1);
        assertThat(testContactMasterHistory.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testContactMasterHistory.getAddress3()).isEqualTo(DEFAULT_ADDRESS_3);
        assertThat(testContactMasterHistory.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testContactMasterHistory.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testContactMasterHistory.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testContactMasterHistory.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testContactMasterHistory.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testContactMasterHistory.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testContactMasterHistory.getPhoneNum()).isEqualTo(DEFAULT_PHONE_NUM);
        assertThat(testContactMasterHistory.getFaxNum()).isEqualTo(DEFAULT_FAX_NUM);
        assertThat(testContactMasterHistory.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testContactMasterHistory.getMobileNum()).isEqualTo(DEFAULT_MOBILE_NUM);
        assertThat(testContactMasterHistory.getRecordId()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testContactMasterHistory.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
        assertThat(testContactMasterHistory.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testContactMasterHistory.getKeyPerson()).isEqualTo(DEFAULT_KEY_PERSON);
        assertThat(testContactMasterHistory.getContactId()).isEqualTo(DEFAULT_CONTACT_ID);
        assertThat(testContactMasterHistory.getLevelImportance()).isEqualTo(DEFAULT_LEVEL_IMPORTANCE);
        assertThat(testContactMasterHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testContactMasterHistory.getRemoveReason()).isEqualTo(DEFAULT_REMOVE_REASON);
        assertThat(testContactMasterHistory.getCityId()).isEqualTo(DEFAULT_CITY_ID);
        assertThat(testContactMasterHistory.getoFlag()).isEqualTo(DEFAULT_O_FLAG);
        assertThat(testContactMasterHistory.getOptOut()).isEqualTo(DEFAULT_OPT_OUT);
        assertThat(testContactMasterHistory.getOfaContactId()).isEqualTo(DEFAULT_OFA_CONTACT_ID);
        assertThat(testContactMasterHistory.getOptOutDate()).isEqualTo(DEFAULT_OPT_OUT_DATE);
        assertThat(testContactMasterHistory.getFaxOptout()).isEqualTo(DEFAULT_FAX_OPTOUT);
        assertThat(testContactMasterHistory.getDonotcall()).isEqualTo(DEFAULT_DONOTCALL);
        assertThat(testContactMasterHistory.getDnsendDirectMail()).isEqualTo(DEFAULT_DNSEND_DIRECT_MAIL);
        assertThat(testContactMasterHistory.getDnshareOutsideMhp()).isEqualTo(DEFAULT_DNSHARE_OUTSIDE_MHP);
        assertThat(testContactMasterHistory.getOutsideOfContact()).isEqualTo(DEFAULT_OUTSIDE_OF_CONTACT);
        assertThat(testContactMasterHistory.getDnshareOutsideSnp()).isEqualTo(DEFAULT_DNSHARE_OUTSIDE_SNP);
        assertThat(testContactMasterHistory.getEmailOptout()).isEqualTo(DEFAULT_EMAIL_OPTOUT);
        assertThat(testContactMasterHistory.getSmsOptout()).isEqualTo(DEFAULT_SMS_OPTOUT);
        assertThat(testContactMasterHistory.getIsSezContactFlag()).isEqualTo(DEFAULT_IS_SEZ_CONTACT_FLAG);
        assertThat(testContactMasterHistory.getIsExemptContactFlag()).isEqualTo(DEFAULT_IS_EXEMPT_CONTACT_FLAG);
        assertThat(testContactMasterHistory.getGstNotApplicable()).isEqualTo(DEFAULT_GST_NOT_APPLICABLE);
        assertThat(testContactMasterHistory.getBillingContact()).isEqualTo(DEFAULT_BILLING_CONTACT);
        assertThat(testContactMasterHistory.getGstPartyType()).isEqualTo(DEFAULT_GST_PARTY_TYPE);
        assertThat(testContactMasterHistory.getTan()).isEqualTo(DEFAULT_TAN);
        assertThat(testContactMasterHistory.getGstNumber()).isEqualTo(DEFAULT_GST_NUMBER);
        assertThat(testContactMasterHistory.getTdsNumber()).isEqualTo(DEFAULT_TDS_NUMBER);
        assertThat(testContactMasterHistory.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testContactMasterHistory.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testContactMasterHistory.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testContactMasterHistory.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testContactMasterHistory.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createContactMasterHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactMasterHistoryRepository.findAll().size();

        // Create the ContactMasterHistory with an existing ID
        contactMasterHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMasterHistory in the database
        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setLastName(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSalutationIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setSalutation(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddress1IsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setAddress1(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDesignationIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setDesignation(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRecordIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setRecordId(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCompanyCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setCompanyCode(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterHistoryRepository.findAll().size();
        // set the field null
        contactMasterHistory.setContactId(null);

        // Create the ContactMasterHistory, which fails.


        restContactMasterHistoryMockMvc.perform(post("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistories() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMasterHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].salutation").value(hasItem(DEFAULT_SALUTATION)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].phoneNum").value(hasItem(DEFAULT_PHONE_NUM)))
            .andExpect(jsonPath("$.[*].faxNum").value(hasItem(DEFAULT_FAX_NUM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].mobileNum").value(hasItem(DEFAULT_MOBILE_NUM)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].keyPerson").value(hasItem(DEFAULT_KEY_PERSON)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].levelImportance").value(hasItem(DEFAULT_LEVEL_IMPORTANCE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].removeReason").value(hasItem(DEFAULT_REMOVE_REASON)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].oFlag").value(hasItem(DEFAULT_O_FLAG)))
            .andExpect(jsonPath("$.[*].optOut").value(hasItem(DEFAULT_OPT_OUT)))
            .andExpect(jsonPath("$.[*].ofaContactId").value(hasItem(DEFAULT_OFA_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].optOutDate").value(hasItem(DEFAULT_OPT_OUT_DATE.toString())))
            .andExpect(jsonPath("$.[*].faxOptout").value(hasItem(DEFAULT_FAX_OPTOUT)))
            .andExpect(jsonPath("$.[*].donotcall").value(hasItem(DEFAULT_DONOTCALL)))
            .andExpect(jsonPath("$.[*].dnsendDirectMail").value(hasItem(DEFAULT_DNSEND_DIRECT_MAIL)))
            .andExpect(jsonPath("$.[*].dnshareOutsideMhp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_MHP)))
            .andExpect(jsonPath("$.[*].outsideOfContact").value(hasItem(DEFAULT_OUTSIDE_OF_CONTACT)))
            .andExpect(jsonPath("$.[*].dnshareOutsideSnp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_SNP)))
            .andExpect(jsonPath("$.[*].emailOptout").value(hasItem(DEFAULT_EMAIL_OPTOUT)))
            .andExpect(jsonPath("$.[*].smsOptout").value(hasItem(DEFAULT_SMS_OPTOUT)))
            .andExpect(jsonPath("$.[*].isSezContactFlag").value(hasItem(DEFAULT_IS_SEZ_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].isExemptContactFlag").value(hasItem(DEFAULT_IS_EXEMPT_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].gstNotApplicable").value(hasItem(DEFAULT_GST_NOT_APPLICABLE)))
            .andExpect(jsonPath("$.[*].billingContact").value(hasItem(DEFAULT_BILLING_CONTACT)))
            .andExpect(jsonPath("$.[*].gstPartyType").value(hasItem(DEFAULT_GST_PARTY_TYPE)))
            .andExpect(jsonPath("$.[*].tan").value(hasItem(DEFAULT_TAN)))
            .andExpect(jsonPath("$.[*].gstNumber").value(hasItem(DEFAULT_GST_NUMBER)))
            .andExpect(jsonPath("$.[*].tdsNumber").value(hasItem(DEFAULT_TDS_NUMBER)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getContactMasterHistory() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get the contactMasterHistory
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories/{id}", contactMasterHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactMasterHistory.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME))
            .andExpect(jsonPath("$.salutation").value(DEFAULT_SALUTATION))
            .andExpect(jsonPath("$.address1").value(DEFAULT_ADDRESS_1))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2))
            .andExpect(jsonPath("$.address3").value(DEFAULT_ADDRESS_3))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN))
            .andExpect(jsonPath("$.phoneNum").value(DEFAULT_PHONE_NUM))
            .andExpect(jsonPath("$.faxNum").value(DEFAULT_FAX_NUM))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.mobileNum").value(DEFAULT_MOBILE_NUM))
            .andExpect(jsonPath("$.recordId").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.keyPerson").value(DEFAULT_KEY_PERSON))
            .andExpect(jsonPath("$.contactId").value(DEFAULT_CONTACT_ID))
            .andExpect(jsonPath("$.levelImportance").value(DEFAULT_LEVEL_IMPORTANCE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.removeReason").value(DEFAULT_REMOVE_REASON))
            .andExpect(jsonPath("$.cityId").value(DEFAULT_CITY_ID))
            .andExpect(jsonPath("$.oFlag").value(DEFAULT_O_FLAG))
            .andExpect(jsonPath("$.optOut").value(DEFAULT_OPT_OUT))
            .andExpect(jsonPath("$.ofaContactId").value(DEFAULT_OFA_CONTACT_ID))
            .andExpect(jsonPath("$.optOutDate").value(DEFAULT_OPT_OUT_DATE.toString()))
            .andExpect(jsonPath("$.faxOptout").value(DEFAULT_FAX_OPTOUT))
            .andExpect(jsonPath("$.donotcall").value(DEFAULT_DONOTCALL))
            .andExpect(jsonPath("$.dnsendDirectMail").value(DEFAULT_DNSEND_DIRECT_MAIL))
            .andExpect(jsonPath("$.dnshareOutsideMhp").value(DEFAULT_DNSHARE_OUTSIDE_MHP))
            .andExpect(jsonPath("$.outsideOfContact").value(DEFAULT_OUTSIDE_OF_CONTACT))
            .andExpect(jsonPath("$.dnshareOutsideSnp").value(DEFAULT_DNSHARE_OUTSIDE_SNP))
            .andExpect(jsonPath("$.emailOptout").value(DEFAULT_EMAIL_OPTOUT))
            .andExpect(jsonPath("$.smsOptout").value(DEFAULT_SMS_OPTOUT))
            .andExpect(jsonPath("$.isSezContactFlag").value(DEFAULT_IS_SEZ_CONTACT_FLAG))
            .andExpect(jsonPath("$.isExemptContactFlag").value(DEFAULT_IS_EXEMPT_CONTACT_FLAG))
            .andExpect(jsonPath("$.gstNotApplicable").value(DEFAULT_GST_NOT_APPLICABLE))
            .andExpect(jsonPath("$.billingContact").value(DEFAULT_BILLING_CONTACT))
            .andExpect(jsonPath("$.gstPartyType").value(DEFAULT_GST_PARTY_TYPE))
            .andExpect(jsonPath("$.tan").value(DEFAULT_TAN))
            .andExpect(jsonPath("$.gstNumber").value(DEFAULT_GST_NUMBER))
            .andExpect(jsonPath("$.tdsNumber").value(DEFAULT_TDS_NUMBER))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getContactMasterHistoriesByIdFiltering() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        Long id = contactMasterHistory.getId();

        defaultContactMasterHistoryShouldBeFound("id.equals=" + id);
        defaultContactMasterHistoryShouldNotBeFound("id.notEquals=" + id);

        defaultContactMasterHistoryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactMasterHistoryShouldNotBeFound("id.greaterThan=" + id);

        defaultContactMasterHistoryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactMasterHistoryShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where firstName equals to DEFAULT_FIRST_NAME
        defaultContactMasterHistoryShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterHistoryList where firstName equals to UPDATED_FIRST_NAME
        defaultContactMasterHistoryShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFirstNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where firstName not equals to DEFAULT_FIRST_NAME
        defaultContactMasterHistoryShouldNotBeFound("firstName.notEquals=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterHistoryList where firstName not equals to UPDATED_FIRST_NAME
        defaultContactMasterHistoryShouldBeFound("firstName.notEquals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultContactMasterHistoryShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the contactMasterHistoryList where firstName equals to UPDATED_FIRST_NAME
        defaultContactMasterHistoryShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where firstName is not null
        defaultContactMasterHistoryShouldBeFound("firstName.specified=true");

        // Get all the contactMasterHistoryList where firstName is null
        defaultContactMasterHistoryShouldNotBeFound("firstName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByFirstNameContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where firstName contains DEFAULT_FIRST_NAME
        defaultContactMasterHistoryShouldBeFound("firstName.contains=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterHistoryList where firstName contains UPDATED_FIRST_NAME
        defaultContactMasterHistoryShouldNotBeFound("firstName.contains=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFirstNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where firstName does not contain DEFAULT_FIRST_NAME
        defaultContactMasterHistoryShouldNotBeFound("firstName.doesNotContain=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterHistoryList where firstName does not contain UPDATED_FIRST_NAME
        defaultContactMasterHistoryShouldBeFound("firstName.doesNotContain=" + UPDATED_FIRST_NAME);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastName equals to DEFAULT_LAST_NAME
        defaultContactMasterHistoryShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterHistoryList where lastName equals to UPDATED_LAST_NAME
        defaultContactMasterHistoryShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastName not equals to DEFAULT_LAST_NAME
        defaultContactMasterHistoryShouldNotBeFound("lastName.notEquals=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterHistoryList where lastName not equals to UPDATED_LAST_NAME
        defaultContactMasterHistoryShouldBeFound("lastName.notEquals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultContactMasterHistoryShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the contactMasterHistoryList where lastName equals to UPDATED_LAST_NAME
        defaultContactMasterHistoryShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastName is not null
        defaultContactMasterHistoryShouldBeFound("lastName.specified=true");

        // Get all the contactMasterHistoryList where lastName is null
        defaultContactMasterHistoryShouldNotBeFound("lastName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastNameContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastName contains DEFAULT_LAST_NAME
        defaultContactMasterHistoryShouldBeFound("lastName.contains=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterHistoryList where lastName contains UPDATED_LAST_NAME
        defaultContactMasterHistoryShouldNotBeFound("lastName.contains=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastName does not contain DEFAULT_LAST_NAME
        defaultContactMasterHistoryShouldNotBeFound("lastName.doesNotContain=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterHistoryList where lastName does not contain UPDATED_LAST_NAME
        defaultContactMasterHistoryShouldBeFound("lastName.doesNotContain=" + UPDATED_LAST_NAME);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMiddleNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where middleName equals to DEFAULT_MIDDLE_NAME
        defaultContactMasterHistoryShouldBeFound("middleName.equals=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterHistoryList where middleName equals to UPDATED_MIDDLE_NAME
        defaultContactMasterHistoryShouldNotBeFound("middleName.equals=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMiddleNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where middleName not equals to DEFAULT_MIDDLE_NAME
        defaultContactMasterHistoryShouldNotBeFound("middleName.notEquals=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterHistoryList where middleName not equals to UPDATED_MIDDLE_NAME
        defaultContactMasterHistoryShouldBeFound("middleName.notEquals=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMiddleNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where middleName in DEFAULT_MIDDLE_NAME or UPDATED_MIDDLE_NAME
        defaultContactMasterHistoryShouldBeFound("middleName.in=" + DEFAULT_MIDDLE_NAME + "," + UPDATED_MIDDLE_NAME);

        // Get all the contactMasterHistoryList where middleName equals to UPDATED_MIDDLE_NAME
        defaultContactMasterHistoryShouldNotBeFound("middleName.in=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMiddleNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where middleName is not null
        defaultContactMasterHistoryShouldBeFound("middleName.specified=true");

        // Get all the contactMasterHistoryList where middleName is null
        defaultContactMasterHistoryShouldNotBeFound("middleName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByMiddleNameContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where middleName contains DEFAULT_MIDDLE_NAME
        defaultContactMasterHistoryShouldBeFound("middleName.contains=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterHistoryList where middleName contains UPDATED_MIDDLE_NAME
        defaultContactMasterHistoryShouldNotBeFound("middleName.contains=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMiddleNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where middleName does not contain DEFAULT_MIDDLE_NAME
        defaultContactMasterHistoryShouldNotBeFound("middleName.doesNotContain=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterHistoryList where middleName does not contain UPDATED_MIDDLE_NAME
        defaultContactMasterHistoryShouldBeFound("middleName.doesNotContain=" + UPDATED_MIDDLE_NAME);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySalutationIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where salutation equals to DEFAULT_SALUTATION
        defaultContactMasterHistoryShouldBeFound("salutation.equals=" + DEFAULT_SALUTATION);

        // Get all the contactMasterHistoryList where salutation equals to UPDATED_SALUTATION
        defaultContactMasterHistoryShouldNotBeFound("salutation.equals=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySalutationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where salutation not equals to DEFAULT_SALUTATION
        defaultContactMasterHistoryShouldNotBeFound("salutation.notEquals=" + DEFAULT_SALUTATION);

        // Get all the contactMasterHistoryList where salutation not equals to UPDATED_SALUTATION
        defaultContactMasterHistoryShouldBeFound("salutation.notEquals=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySalutationIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where salutation in DEFAULT_SALUTATION or UPDATED_SALUTATION
        defaultContactMasterHistoryShouldBeFound("salutation.in=" + DEFAULT_SALUTATION + "," + UPDATED_SALUTATION);

        // Get all the contactMasterHistoryList where salutation equals to UPDATED_SALUTATION
        defaultContactMasterHistoryShouldNotBeFound("salutation.in=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySalutationIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where salutation is not null
        defaultContactMasterHistoryShouldBeFound("salutation.specified=true");

        // Get all the contactMasterHistoryList where salutation is null
        defaultContactMasterHistoryShouldNotBeFound("salutation.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesBySalutationContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where salutation contains DEFAULT_SALUTATION
        defaultContactMasterHistoryShouldBeFound("salutation.contains=" + DEFAULT_SALUTATION);

        // Get all the contactMasterHistoryList where salutation contains UPDATED_SALUTATION
        defaultContactMasterHistoryShouldNotBeFound("salutation.contains=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySalutationNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where salutation does not contain DEFAULT_SALUTATION
        defaultContactMasterHistoryShouldNotBeFound("salutation.doesNotContain=" + DEFAULT_SALUTATION);

        // Get all the contactMasterHistoryList where salutation does not contain UPDATED_SALUTATION
        defaultContactMasterHistoryShouldBeFound("salutation.doesNotContain=" + UPDATED_SALUTATION);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress1IsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address1 equals to DEFAULT_ADDRESS_1
        defaultContactMasterHistoryShouldBeFound("address1.equals=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterHistoryList where address1 equals to UPDATED_ADDRESS_1
        defaultContactMasterHistoryShouldNotBeFound("address1.equals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address1 not equals to DEFAULT_ADDRESS_1
        defaultContactMasterHistoryShouldNotBeFound("address1.notEquals=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterHistoryList where address1 not equals to UPDATED_ADDRESS_1
        defaultContactMasterHistoryShouldBeFound("address1.notEquals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress1IsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address1 in DEFAULT_ADDRESS_1 or UPDATED_ADDRESS_1
        defaultContactMasterHistoryShouldBeFound("address1.in=" + DEFAULT_ADDRESS_1 + "," + UPDATED_ADDRESS_1);

        // Get all the contactMasterHistoryList where address1 equals to UPDATED_ADDRESS_1
        defaultContactMasterHistoryShouldNotBeFound("address1.in=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress1IsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address1 is not null
        defaultContactMasterHistoryShouldBeFound("address1.specified=true");

        // Get all the contactMasterHistoryList where address1 is null
        defaultContactMasterHistoryShouldNotBeFound("address1.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress1ContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address1 contains DEFAULT_ADDRESS_1
        defaultContactMasterHistoryShouldBeFound("address1.contains=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterHistoryList where address1 contains UPDATED_ADDRESS_1
        defaultContactMasterHistoryShouldNotBeFound("address1.contains=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress1NotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address1 does not contain DEFAULT_ADDRESS_1
        defaultContactMasterHistoryShouldNotBeFound("address1.doesNotContain=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterHistoryList where address1 does not contain UPDATED_ADDRESS_1
        defaultContactMasterHistoryShouldBeFound("address1.doesNotContain=" + UPDATED_ADDRESS_1);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress2IsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address2 equals to DEFAULT_ADDRESS_2
        defaultContactMasterHistoryShouldBeFound("address2.equals=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterHistoryList where address2 equals to UPDATED_ADDRESS_2
        defaultContactMasterHistoryShouldNotBeFound("address2.equals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address2 not equals to DEFAULT_ADDRESS_2
        defaultContactMasterHistoryShouldNotBeFound("address2.notEquals=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterHistoryList where address2 not equals to UPDATED_ADDRESS_2
        defaultContactMasterHistoryShouldBeFound("address2.notEquals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress2IsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address2 in DEFAULT_ADDRESS_2 or UPDATED_ADDRESS_2
        defaultContactMasterHistoryShouldBeFound("address2.in=" + DEFAULT_ADDRESS_2 + "," + UPDATED_ADDRESS_2);

        // Get all the contactMasterHistoryList where address2 equals to UPDATED_ADDRESS_2
        defaultContactMasterHistoryShouldNotBeFound("address2.in=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress2IsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address2 is not null
        defaultContactMasterHistoryShouldBeFound("address2.specified=true");

        // Get all the contactMasterHistoryList where address2 is null
        defaultContactMasterHistoryShouldNotBeFound("address2.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress2ContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address2 contains DEFAULT_ADDRESS_2
        defaultContactMasterHistoryShouldBeFound("address2.contains=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterHistoryList where address2 contains UPDATED_ADDRESS_2
        defaultContactMasterHistoryShouldNotBeFound("address2.contains=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress2NotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address2 does not contain DEFAULT_ADDRESS_2
        defaultContactMasterHistoryShouldNotBeFound("address2.doesNotContain=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterHistoryList where address2 does not contain UPDATED_ADDRESS_2
        defaultContactMasterHistoryShouldBeFound("address2.doesNotContain=" + UPDATED_ADDRESS_2);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress3IsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address3 equals to DEFAULT_ADDRESS_3
        defaultContactMasterHistoryShouldBeFound("address3.equals=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterHistoryList where address3 equals to UPDATED_ADDRESS_3
        defaultContactMasterHistoryShouldNotBeFound("address3.equals=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address3 not equals to DEFAULT_ADDRESS_3
        defaultContactMasterHistoryShouldNotBeFound("address3.notEquals=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterHistoryList where address3 not equals to UPDATED_ADDRESS_3
        defaultContactMasterHistoryShouldBeFound("address3.notEquals=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress3IsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address3 in DEFAULT_ADDRESS_3 or UPDATED_ADDRESS_3
        defaultContactMasterHistoryShouldBeFound("address3.in=" + DEFAULT_ADDRESS_3 + "," + UPDATED_ADDRESS_3);

        // Get all the contactMasterHistoryList where address3 equals to UPDATED_ADDRESS_3
        defaultContactMasterHistoryShouldNotBeFound("address3.in=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress3IsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address3 is not null
        defaultContactMasterHistoryShouldBeFound("address3.specified=true");

        // Get all the contactMasterHistoryList where address3 is null
        defaultContactMasterHistoryShouldNotBeFound("address3.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress3ContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address3 contains DEFAULT_ADDRESS_3
        defaultContactMasterHistoryShouldBeFound("address3.contains=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterHistoryList where address3 contains UPDATED_ADDRESS_3
        defaultContactMasterHistoryShouldNotBeFound("address3.contains=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByAddress3NotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where address3 does not contain DEFAULT_ADDRESS_3
        defaultContactMasterHistoryShouldNotBeFound("address3.doesNotContain=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterHistoryList where address3 does not contain UPDATED_ADDRESS_3
        defaultContactMasterHistoryShouldBeFound("address3.doesNotContain=" + UPDATED_ADDRESS_3);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where designation equals to DEFAULT_DESIGNATION
        defaultContactMasterHistoryShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterHistoryList where designation equals to UPDATED_DESIGNATION
        defaultContactMasterHistoryShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDesignationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where designation not equals to DEFAULT_DESIGNATION
        defaultContactMasterHistoryShouldNotBeFound("designation.notEquals=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterHistoryList where designation not equals to UPDATED_DESIGNATION
        defaultContactMasterHistoryShouldBeFound("designation.notEquals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultContactMasterHistoryShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the contactMasterHistoryList where designation equals to UPDATED_DESIGNATION
        defaultContactMasterHistoryShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where designation is not null
        defaultContactMasterHistoryShouldBeFound("designation.specified=true");

        // Get all the contactMasterHistoryList where designation is null
        defaultContactMasterHistoryShouldNotBeFound("designation.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByDesignationContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where designation contains DEFAULT_DESIGNATION
        defaultContactMasterHistoryShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterHistoryList where designation contains UPDATED_DESIGNATION
        defaultContactMasterHistoryShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where designation does not contain DEFAULT_DESIGNATION
        defaultContactMasterHistoryShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterHistoryList where designation does not contain UPDATED_DESIGNATION
        defaultContactMasterHistoryShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDepartmentIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where department equals to DEFAULT_DEPARTMENT
        defaultContactMasterHistoryShouldBeFound("department.equals=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterHistoryList where department equals to UPDATED_DEPARTMENT
        defaultContactMasterHistoryShouldNotBeFound("department.equals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDepartmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where department not equals to DEFAULT_DEPARTMENT
        defaultContactMasterHistoryShouldNotBeFound("department.notEquals=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterHistoryList where department not equals to UPDATED_DEPARTMENT
        defaultContactMasterHistoryShouldBeFound("department.notEquals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDepartmentIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where department in DEFAULT_DEPARTMENT or UPDATED_DEPARTMENT
        defaultContactMasterHistoryShouldBeFound("department.in=" + DEFAULT_DEPARTMENT + "," + UPDATED_DEPARTMENT);

        // Get all the contactMasterHistoryList where department equals to UPDATED_DEPARTMENT
        defaultContactMasterHistoryShouldNotBeFound("department.in=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDepartmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where department is not null
        defaultContactMasterHistoryShouldBeFound("department.specified=true");

        // Get all the contactMasterHistoryList where department is null
        defaultContactMasterHistoryShouldNotBeFound("department.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByDepartmentContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where department contains DEFAULT_DEPARTMENT
        defaultContactMasterHistoryShouldBeFound("department.contains=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterHistoryList where department contains UPDATED_DEPARTMENT
        defaultContactMasterHistoryShouldNotBeFound("department.contains=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDepartmentNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where department does not contain DEFAULT_DEPARTMENT
        defaultContactMasterHistoryShouldNotBeFound("department.doesNotContain=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterHistoryList where department does not contain UPDATED_DEPARTMENT
        defaultContactMasterHistoryShouldBeFound("department.doesNotContain=" + UPDATED_DEPARTMENT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where city equals to DEFAULT_CITY
        defaultContactMasterHistoryShouldBeFound("city.equals=" + DEFAULT_CITY);

        // Get all the contactMasterHistoryList where city equals to UPDATED_CITY
        defaultContactMasterHistoryShouldNotBeFound("city.equals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where city not equals to DEFAULT_CITY
        defaultContactMasterHistoryShouldNotBeFound("city.notEquals=" + DEFAULT_CITY);

        // Get all the contactMasterHistoryList where city not equals to UPDATED_CITY
        defaultContactMasterHistoryShouldBeFound("city.notEquals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where city in DEFAULT_CITY or UPDATED_CITY
        defaultContactMasterHistoryShouldBeFound("city.in=" + DEFAULT_CITY + "," + UPDATED_CITY);

        // Get all the contactMasterHistoryList where city equals to UPDATED_CITY
        defaultContactMasterHistoryShouldNotBeFound("city.in=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where city is not null
        defaultContactMasterHistoryShouldBeFound("city.specified=true");

        // Get all the contactMasterHistoryList where city is null
        defaultContactMasterHistoryShouldNotBeFound("city.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where city contains DEFAULT_CITY
        defaultContactMasterHistoryShouldBeFound("city.contains=" + DEFAULT_CITY);

        // Get all the contactMasterHistoryList where city contains UPDATED_CITY
        defaultContactMasterHistoryShouldNotBeFound("city.contains=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where city does not contain DEFAULT_CITY
        defaultContactMasterHistoryShouldNotBeFound("city.doesNotContain=" + DEFAULT_CITY);

        // Get all the contactMasterHistoryList where city does not contain UPDATED_CITY
        defaultContactMasterHistoryShouldBeFound("city.doesNotContain=" + UPDATED_CITY);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where state equals to DEFAULT_STATE
        defaultContactMasterHistoryShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the contactMasterHistoryList where state equals to UPDATED_STATE
        defaultContactMasterHistoryShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where state not equals to DEFAULT_STATE
        defaultContactMasterHistoryShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the contactMasterHistoryList where state not equals to UPDATED_STATE
        defaultContactMasterHistoryShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where state in DEFAULT_STATE or UPDATED_STATE
        defaultContactMasterHistoryShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the contactMasterHistoryList where state equals to UPDATED_STATE
        defaultContactMasterHistoryShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where state is not null
        defaultContactMasterHistoryShouldBeFound("state.specified=true");

        // Get all the contactMasterHistoryList where state is null
        defaultContactMasterHistoryShouldNotBeFound("state.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByStateContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where state contains DEFAULT_STATE
        defaultContactMasterHistoryShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the contactMasterHistoryList where state contains UPDATED_STATE
        defaultContactMasterHistoryShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStateNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where state does not contain DEFAULT_STATE
        defaultContactMasterHistoryShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the contactMasterHistoryList where state does not contain UPDATED_STATE
        defaultContactMasterHistoryShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where country equals to DEFAULT_COUNTRY
        defaultContactMasterHistoryShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the contactMasterHistoryList where country equals to UPDATED_COUNTRY
        defaultContactMasterHistoryShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCountryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where country not equals to DEFAULT_COUNTRY
        defaultContactMasterHistoryShouldNotBeFound("country.notEquals=" + DEFAULT_COUNTRY);

        // Get all the contactMasterHistoryList where country not equals to UPDATED_COUNTRY
        defaultContactMasterHistoryShouldBeFound("country.notEquals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultContactMasterHistoryShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the contactMasterHistoryList where country equals to UPDATED_COUNTRY
        defaultContactMasterHistoryShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where country is not null
        defaultContactMasterHistoryShouldBeFound("country.specified=true");

        // Get all the contactMasterHistoryList where country is null
        defaultContactMasterHistoryShouldNotBeFound("country.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByCountryContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where country contains DEFAULT_COUNTRY
        defaultContactMasterHistoryShouldBeFound("country.contains=" + DEFAULT_COUNTRY);

        // Get all the contactMasterHistoryList where country contains UPDATED_COUNTRY
        defaultContactMasterHistoryShouldNotBeFound("country.contains=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCountryNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where country does not contain DEFAULT_COUNTRY
        defaultContactMasterHistoryShouldNotBeFound("country.doesNotContain=" + DEFAULT_COUNTRY);

        // Get all the contactMasterHistoryList where country does not contain UPDATED_COUNTRY
        defaultContactMasterHistoryShouldBeFound("country.doesNotContain=" + UPDATED_COUNTRY);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPinIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where pin equals to DEFAULT_PIN
        defaultContactMasterHistoryShouldBeFound("pin.equals=" + DEFAULT_PIN);

        // Get all the contactMasterHistoryList where pin equals to UPDATED_PIN
        defaultContactMasterHistoryShouldNotBeFound("pin.equals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where pin not equals to DEFAULT_PIN
        defaultContactMasterHistoryShouldNotBeFound("pin.notEquals=" + DEFAULT_PIN);

        // Get all the contactMasterHistoryList where pin not equals to UPDATED_PIN
        defaultContactMasterHistoryShouldBeFound("pin.notEquals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPinIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where pin in DEFAULT_PIN or UPDATED_PIN
        defaultContactMasterHistoryShouldBeFound("pin.in=" + DEFAULT_PIN + "," + UPDATED_PIN);

        // Get all the contactMasterHistoryList where pin equals to UPDATED_PIN
        defaultContactMasterHistoryShouldNotBeFound("pin.in=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPinIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where pin is not null
        defaultContactMasterHistoryShouldBeFound("pin.specified=true");

        // Get all the contactMasterHistoryList where pin is null
        defaultContactMasterHistoryShouldNotBeFound("pin.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByPinContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where pin contains DEFAULT_PIN
        defaultContactMasterHistoryShouldBeFound("pin.contains=" + DEFAULT_PIN);

        // Get all the contactMasterHistoryList where pin contains UPDATED_PIN
        defaultContactMasterHistoryShouldNotBeFound("pin.contains=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPinNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where pin does not contain DEFAULT_PIN
        defaultContactMasterHistoryShouldNotBeFound("pin.doesNotContain=" + DEFAULT_PIN);

        // Get all the contactMasterHistoryList where pin does not contain UPDATED_PIN
        defaultContactMasterHistoryShouldBeFound("pin.doesNotContain=" + UPDATED_PIN);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPhoneNumIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where phoneNum equals to DEFAULT_PHONE_NUM
        defaultContactMasterHistoryShouldBeFound("phoneNum.equals=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterHistoryList where phoneNum equals to UPDATED_PHONE_NUM
        defaultContactMasterHistoryShouldNotBeFound("phoneNum.equals=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPhoneNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where phoneNum not equals to DEFAULT_PHONE_NUM
        defaultContactMasterHistoryShouldNotBeFound("phoneNum.notEquals=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterHistoryList where phoneNum not equals to UPDATED_PHONE_NUM
        defaultContactMasterHistoryShouldBeFound("phoneNum.notEquals=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPhoneNumIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where phoneNum in DEFAULT_PHONE_NUM or UPDATED_PHONE_NUM
        defaultContactMasterHistoryShouldBeFound("phoneNum.in=" + DEFAULT_PHONE_NUM + "," + UPDATED_PHONE_NUM);

        // Get all the contactMasterHistoryList where phoneNum equals to UPDATED_PHONE_NUM
        defaultContactMasterHistoryShouldNotBeFound("phoneNum.in=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPhoneNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where phoneNum is not null
        defaultContactMasterHistoryShouldBeFound("phoneNum.specified=true");

        // Get all the contactMasterHistoryList where phoneNum is null
        defaultContactMasterHistoryShouldNotBeFound("phoneNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByPhoneNumContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where phoneNum contains DEFAULT_PHONE_NUM
        defaultContactMasterHistoryShouldBeFound("phoneNum.contains=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterHistoryList where phoneNum contains UPDATED_PHONE_NUM
        defaultContactMasterHistoryShouldNotBeFound("phoneNum.contains=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByPhoneNumNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where phoneNum does not contain DEFAULT_PHONE_NUM
        defaultContactMasterHistoryShouldNotBeFound("phoneNum.doesNotContain=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterHistoryList where phoneNum does not contain UPDATED_PHONE_NUM
        defaultContactMasterHistoryShouldBeFound("phoneNum.doesNotContain=" + UPDATED_PHONE_NUM);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxNumIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxNum equals to DEFAULT_FAX_NUM
        defaultContactMasterHistoryShouldBeFound("faxNum.equals=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterHistoryList where faxNum equals to UPDATED_FAX_NUM
        defaultContactMasterHistoryShouldNotBeFound("faxNum.equals=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxNum not equals to DEFAULT_FAX_NUM
        defaultContactMasterHistoryShouldNotBeFound("faxNum.notEquals=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterHistoryList where faxNum not equals to UPDATED_FAX_NUM
        defaultContactMasterHistoryShouldBeFound("faxNum.notEquals=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxNumIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxNum in DEFAULT_FAX_NUM or UPDATED_FAX_NUM
        defaultContactMasterHistoryShouldBeFound("faxNum.in=" + DEFAULT_FAX_NUM + "," + UPDATED_FAX_NUM);

        // Get all the contactMasterHistoryList where faxNum equals to UPDATED_FAX_NUM
        defaultContactMasterHistoryShouldNotBeFound("faxNum.in=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxNum is not null
        defaultContactMasterHistoryShouldBeFound("faxNum.specified=true");

        // Get all the contactMasterHistoryList where faxNum is null
        defaultContactMasterHistoryShouldNotBeFound("faxNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxNumContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxNum contains DEFAULT_FAX_NUM
        defaultContactMasterHistoryShouldBeFound("faxNum.contains=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterHistoryList where faxNum contains UPDATED_FAX_NUM
        defaultContactMasterHistoryShouldNotBeFound("faxNum.contains=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxNumNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxNum does not contain DEFAULT_FAX_NUM
        defaultContactMasterHistoryShouldNotBeFound("faxNum.doesNotContain=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterHistoryList where faxNum does not contain UPDATED_FAX_NUM
        defaultContactMasterHistoryShouldBeFound("faxNum.doesNotContain=" + UPDATED_FAX_NUM);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where email equals to DEFAULT_EMAIL
        defaultContactMasterHistoryShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the contactMasterHistoryList where email equals to UPDATED_EMAIL
        defaultContactMasterHistoryShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where email not equals to DEFAULT_EMAIL
        defaultContactMasterHistoryShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the contactMasterHistoryList where email not equals to UPDATED_EMAIL
        defaultContactMasterHistoryShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultContactMasterHistoryShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the contactMasterHistoryList where email equals to UPDATED_EMAIL
        defaultContactMasterHistoryShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where email is not null
        defaultContactMasterHistoryShouldBeFound("email.specified=true");

        // Get all the contactMasterHistoryList where email is null
        defaultContactMasterHistoryShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where email contains DEFAULT_EMAIL
        defaultContactMasterHistoryShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the contactMasterHistoryList where email contains UPDATED_EMAIL
        defaultContactMasterHistoryShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where email does not contain DEFAULT_EMAIL
        defaultContactMasterHistoryShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the contactMasterHistoryList where email does not contain UPDATED_EMAIL
        defaultContactMasterHistoryShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMobileNumIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where mobileNum equals to DEFAULT_MOBILE_NUM
        defaultContactMasterHistoryShouldBeFound("mobileNum.equals=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterHistoryList where mobileNum equals to UPDATED_MOBILE_NUM
        defaultContactMasterHistoryShouldNotBeFound("mobileNum.equals=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMobileNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where mobileNum not equals to DEFAULT_MOBILE_NUM
        defaultContactMasterHistoryShouldNotBeFound("mobileNum.notEquals=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterHistoryList where mobileNum not equals to UPDATED_MOBILE_NUM
        defaultContactMasterHistoryShouldBeFound("mobileNum.notEquals=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMobileNumIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where mobileNum in DEFAULT_MOBILE_NUM or UPDATED_MOBILE_NUM
        defaultContactMasterHistoryShouldBeFound("mobileNum.in=" + DEFAULT_MOBILE_NUM + "," + UPDATED_MOBILE_NUM);

        // Get all the contactMasterHistoryList where mobileNum equals to UPDATED_MOBILE_NUM
        defaultContactMasterHistoryShouldNotBeFound("mobileNum.in=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMobileNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where mobileNum is not null
        defaultContactMasterHistoryShouldBeFound("mobileNum.specified=true");

        // Get all the contactMasterHistoryList where mobileNum is null
        defaultContactMasterHistoryShouldNotBeFound("mobileNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByMobileNumContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where mobileNum contains DEFAULT_MOBILE_NUM
        defaultContactMasterHistoryShouldBeFound("mobileNum.contains=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterHistoryList where mobileNum contains UPDATED_MOBILE_NUM
        defaultContactMasterHistoryShouldNotBeFound("mobileNum.contains=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByMobileNumNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where mobileNum does not contain DEFAULT_MOBILE_NUM
        defaultContactMasterHistoryShouldNotBeFound("mobileNum.doesNotContain=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterHistoryList where mobileNum does not contain UPDATED_MOBILE_NUM
        defaultContactMasterHistoryShouldBeFound("mobileNum.doesNotContain=" + UPDATED_MOBILE_NUM);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where recordId equals to DEFAULT_RECORD_ID
        defaultContactMasterHistoryShouldBeFound("recordId.equals=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterHistoryList where recordId equals to UPDATED_RECORD_ID
        defaultContactMasterHistoryShouldNotBeFound("recordId.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where recordId not equals to DEFAULT_RECORD_ID
        defaultContactMasterHistoryShouldNotBeFound("recordId.notEquals=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterHistoryList where recordId not equals to UPDATED_RECORD_ID
        defaultContactMasterHistoryShouldBeFound("recordId.notEquals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where recordId in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultContactMasterHistoryShouldBeFound("recordId.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the contactMasterHistoryList where recordId equals to UPDATED_RECORD_ID
        defaultContactMasterHistoryShouldNotBeFound("recordId.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where recordId is not null
        defaultContactMasterHistoryShouldBeFound("recordId.specified=true");

        // Get all the contactMasterHistoryList where recordId is null
        defaultContactMasterHistoryShouldNotBeFound("recordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByRecordIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where recordId contains DEFAULT_RECORD_ID
        defaultContactMasterHistoryShouldBeFound("recordId.contains=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterHistoryList where recordId contains UPDATED_RECORD_ID
        defaultContactMasterHistoryShouldNotBeFound("recordId.contains=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where recordId does not contain DEFAULT_RECORD_ID
        defaultContactMasterHistoryShouldNotBeFound("recordId.doesNotContain=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterHistoryList where recordId does not contain UPDATED_RECORD_ID
        defaultContactMasterHistoryShouldBeFound("recordId.doesNotContain=" + UPDATED_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultContactMasterHistoryShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the contactMasterHistoryList where companyCode equals to UPDATED_COMPANY_CODE
        defaultContactMasterHistoryShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultContactMasterHistoryShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the contactMasterHistoryList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultContactMasterHistoryShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultContactMasterHistoryShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the contactMasterHistoryList where companyCode equals to UPDATED_COMPANY_CODE
        defaultContactMasterHistoryShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where companyCode is not null
        defaultContactMasterHistoryShouldBeFound("companyCode.specified=true");

        // Get all the contactMasterHistoryList where companyCode is null
        defaultContactMasterHistoryShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where companyCode contains DEFAULT_COMPANY_CODE
        defaultContactMasterHistoryShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the contactMasterHistoryList where companyCode contains UPDATED_COMPANY_CODE
        defaultContactMasterHistoryShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultContactMasterHistoryShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the contactMasterHistoryList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultContactMasterHistoryShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where comments equals to DEFAULT_COMMENTS
        defaultContactMasterHistoryShouldBeFound("comments.equals=" + DEFAULT_COMMENTS);

        // Get all the contactMasterHistoryList where comments equals to UPDATED_COMMENTS
        defaultContactMasterHistoryShouldNotBeFound("comments.equals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where comments not equals to DEFAULT_COMMENTS
        defaultContactMasterHistoryShouldNotBeFound("comments.notEquals=" + DEFAULT_COMMENTS);

        // Get all the contactMasterHistoryList where comments not equals to UPDATED_COMMENTS
        defaultContactMasterHistoryShouldBeFound("comments.notEquals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where comments in DEFAULT_COMMENTS or UPDATED_COMMENTS
        defaultContactMasterHistoryShouldBeFound("comments.in=" + DEFAULT_COMMENTS + "," + UPDATED_COMMENTS);

        // Get all the contactMasterHistoryList where comments equals to UPDATED_COMMENTS
        defaultContactMasterHistoryShouldNotBeFound("comments.in=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where comments is not null
        defaultContactMasterHistoryShouldBeFound("comments.specified=true");

        // Get all the contactMasterHistoryList where comments is null
        defaultContactMasterHistoryShouldNotBeFound("comments.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByCommentsContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where comments contains DEFAULT_COMMENTS
        defaultContactMasterHistoryShouldBeFound("comments.contains=" + DEFAULT_COMMENTS);

        // Get all the contactMasterHistoryList where comments contains UPDATED_COMMENTS
        defaultContactMasterHistoryShouldNotBeFound("comments.contains=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where comments does not contain DEFAULT_COMMENTS
        defaultContactMasterHistoryShouldNotBeFound("comments.doesNotContain=" + DEFAULT_COMMENTS);

        // Get all the contactMasterHistoryList where comments does not contain UPDATED_COMMENTS
        defaultContactMasterHistoryShouldBeFound("comments.doesNotContain=" + UPDATED_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByKeyPersonIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where keyPerson equals to DEFAULT_KEY_PERSON
        defaultContactMasterHistoryShouldBeFound("keyPerson.equals=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterHistoryList where keyPerson equals to UPDATED_KEY_PERSON
        defaultContactMasterHistoryShouldNotBeFound("keyPerson.equals=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByKeyPersonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where keyPerson not equals to DEFAULT_KEY_PERSON
        defaultContactMasterHistoryShouldNotBeFound("keyPerson.notEquals=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterHistoryList where keyPerson not equals to UPDATED_KEY_PERSON
        defaultContactMasterHistoryShouldBeFound("keyPerson.notEquals=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByKeyPersonIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where keyPerson in DEFAULT_KEY_PERSON or UPDATED_KEY_PERSON
        defaultContactMasterHistoryShouldBeFound("keyPerson.in=" + DEFAULT_KEY_PERSON + "," + UPDATED_KEY_PERSON);

        // Get all the contactMasterHistoryList where keyPerson equals to UPDATED_KEY_PERSON
        defaultContactMasterHistoryShouldNotBeFound("keyPerson.in=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByKeyPersonIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where keyPerson is not null
        defaultContactMasterHistoryShouldBeFound("keyPerson.specified=true");

        // Get all the contactMasterHistoryList where keyPerson is null
        defaultContactMasterHistoryShouldNotBeFound("keyPerson.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByKeyPersonContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where keyPerson contains DEFAULT_KEY_PERSON
        defaultContactMasterHistoryShouldBeFound("keyPerson.contains=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterHistoryList where keyPerson contains UPDATED_KEY_PERSON
        defaultContactMasterHistoryShouldNotBeFound("keyPerson.contains=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByKeyPersonNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where keyPerson does not contain DEFAULT_KEY_PERSON
        defaultContactMasterHistoryShouldNotBeFound("keyPerson.doesNotContain=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterHistoryList where keyPerson does not contain UPDATED_KEY_PERSON
        defaultContactMasterHistoryShouldBeFound("keyPerson.doesNotContain=" + UPDATED_KEY_PERSON);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where contactId equals to DEFAULT_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("contactId.equals=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterHistoryList where contactId equals to UPDATED_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("contactId.equals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where contactId not equals to DEFAULT_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("contactId.notEquals=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterHistoryList where contactId not equals to UPDATED_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("contactId.notEquals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where contactId in DEFAULT_CONTACT_ID or UPDATED_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("contactId.in=" + DEFAULT_CONTACT_ID + "," + UPDATED_CONTACT_ID);

        // Get all the contactMasterHistoryList where contactId equals to UPDATED_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("contactId.in=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where contactId is not null
        defaultContactMasterHistoryShouldBeFound("contactId.specified=true");

        // Get all the contactMasterHistoryList where contactId is null
        defaultContactMasterHistoryShouldNotBeFound("contactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByContactIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where contactId contains DEFAULT_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("contactId.contains=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterHistoryList where contactId contains UPDATED_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("contactId.contains=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where contactId does not contain DEFAULT_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("contactId.doesNotContain=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterHistoryList where contactId does not contain UPDATED_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("contactId.doesNotContain=" + UPDATED_CONTACT_ID);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLevelImportanceIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where levelImportance equals to DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldBeFound("levelImportance.equals=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterHistoryList where levelImportance equals to UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldNotBeFound("levelImportance.equals=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLevelImportanceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where levelImportance not equals to DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldNotBeFound("levelImportance.notEquals=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterHistoryList where levelImportance not equals to UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldBeFound("levelImportance.notEquals=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLevelImportanceIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where levelImportance in DEFAULT_LEVEL_IMPORTANCE or UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldBeFound("levelImportance.in=" + DEFAULT_LEVEL_IMPORTANCE + "," + UPDATED_LEVEL_IMPORTANCE);

        // Get all the contactMasterHistoryList where levelImportance equals to UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldNotBeFound("levelImportance.in=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLevelImportanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where levelImportance is not null
        defaultContactMasterHistoryShouldBeFound("levelImportance.specified=true");

        // Get all the contactMasterHistoryList where levelImportance is null
        defaultContactMasterHistoryShouldNotBeFound("levelImportance.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByLevelImportanceContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where levelImportance contains DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldBeFound("levelImportance.contains=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterHistoryList where levelImportance contains UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldNotBeFound("levelImportance.contains=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLevelImportanceNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where levelImportance does not contain DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldNotBeFound("levelImportance.doesNotContain=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterHistoryList where levelImportance does not contain UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterHistoryShouldBeFound("levelImportance.doesNotContain=" + UPDATED_LEVEL_IMPORTANCE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where status equals to DEFAULT_STATUS
        defaultContactMasterHistoryShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the contactMasterHistoryList where status equals to UPDATED_STATUS
        defaultContactMasterHistoryShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where status not equals to DEFAULT_STATUS
        defaultContactMasterHistoryShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the contactMasterHistoryList where status not equals to UPDATED_STATUS
        defaultContactMasterHistoryShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultContactMasterHistoryShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the contactMasterHistoryList where status equals to UPDATED_STATUS
        defaultContactMasterHistoryShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where status is not null
        defaultContactMasterHistoryShouldBeFound("status.specified=true");

        // Get all the contactMasterHistoryList where status is null
        defaultContactMasterHistoryShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByStatusContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where status contains DEFAULT_STATUS
        defaultContactMasterHistoryShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the contactMasterHistoryList where status contains UPDATED_STATUS
        defaultContactMasterHistoryShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where status does not contain DEFAULT_STATUS
        defaultContactMasterHistoryShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the contactMasterHistoryList where status does not contain UPDATED_STATUS
        defaultContactMasterHistoryShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRemoveReasonIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where removeReason equals to DEFAULT_REMOVE_REASON
        defaultContactMasterHistoryShouldBeFound("removeReason.equals=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterHistoryList where removeReason equals to UPDATED_REMOVE_REASON
        defaultContactMasterHistoryShouldNotBeFound("removeReason.equals=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRemoveReasonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where removeReason not equals to DEFAULT_REMOVE_REASON
        defaultContactMasterHistoryShouldNotBeFound("removeReason.notEquals=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterHistoryList where removeReason not equals to UPDATED_REMOVE_REASON
        defaultContactMasterHistoryShouldBeFound("removeReason.notEquals=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRemoveReasonIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where removeReason in DEFAULT_REMOVE_REASON or UPDATED_REMOVE_REASON
        defaultContactMasterHistoryShouldBeFound("removeReason.in=" + DEFAULT_REMOVE_REASON + "," + UPDATED_REMOVE_REASON);

        // Get all the contactMasterHistoryList where removeReason equals to UPDATED_REMOVE_REASON
        defaultContactMasterHistoryShouldNotBeFound("removeReason.in=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRemoveReasonIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where removeReason is not null
        defaultContactMasterHistoryShouldBeFound("removeReason.specified=true");

        // Get all the contactMasterHistoryList where removeReason is null
        defaultContactMasterHistoryShouldNotBeFound("removeReason.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByRemoveReasonContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where removeReason contains DEFAULT_REMOVE_REASON
        defaultContactMasterHistoryShouldBeFound("removeReason.contains=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterHistoryList where removeReason contains UPDATED_REMOVE_REASON
        defaultContactMasterHistoryShouldNotBeFound("removeReason.contains=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByRemoveReasonNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where removeReason does not contain DEFAULT_REMOVE_REASON
        defaultContactMasterHistoryShouldNotBeFound("removeReason.doesNotContain=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterHistoryList where removeReason does not contain UPDATED_REMOVE_REASON
        defaultContactMasterHistoryShouldBeFound("removeReason.doesNotContain=" + UPDATED_REMOVE_REASON);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where cityId equals to DEFAULT_CITY_ID
        defaultContactMasterHistoryShouldBeFound("cityId.equals=" + DEFAULT_CITY_ID);

        // Get all the contactMasterHistoryList where cityId equals to UPDATED_CITY_ID
        defaultContactMasterHistoryShouldNotBeFound("cityId.equals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where cityId not equals to DEFAULT_CITY_ID
        defaultContactMasterHistoryShouldNotBeFound("cityId.notEquals=" + DEFAULT_CITY_ID);

        // Get all the contactMasterHistoryList where cityId not equals to UPDATED_CITY_ID
        defaultContactMasterHistoryShouldBeFound("cityId.notEquals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where cityId in DEFAULT_CITY_ID or UPDATED_CITY_ID
        defaultContactMasterHistoryShouldBeFound("cityId.in=" + DEFAULT_CITY_ID + "," + UPDATED_CITY_ID);

        // Get all the contactMasterHistoryList where cityId equals to UPDATED_CITY_ID
        defaultContactMasterHistoryShouldNotBeFound("cityId.in=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where cityId is not null
        defaultContactMasterHistoryShouldBeFound("cityId.specified=true");

        // Get all the contactMasterHistoryList where cityId is null
        defaultContactMasterHistoryShouldNotBeFound("cityId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where cityId contains DEFAULT_CITY_ID
        defaultContactMasterHistoryShouldBeFound("cityId.contains=" + DEFAULT_CITY_ID);

        // Get all the contactMasterHistoryList where cityId contains UPDATED_CITY_ID
        defaultContactMasterHistoryShouldNotBeFound("cityId.contains=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCityIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where cityId does not contain DEFAULT_CITY_ID
        defaultContactMasterHistoryShouldNotBeFound("cityId.doesNotContain=" + DEFAULT_CITY_ID);

        // Get all the contactMasterHistoryList where cityId does not contain UPDATED_CITY_ID
        defaultContactMasterHistoryShouldBeFound("cityId.doesNotContain=" + UPDATED_CITY_ID);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByoFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where oFlag equals to DEFAULT_O_FLAG
        defaultContactMasterHistoryShouldBeFound("oFlag.equals=" + DEFAULT_O_FLAG);

        // Get all the contactMasterHistoryList where oFlag equals to UPDATED_O_FLAG
        defaultContactMasterHistoryShouldNotBeFound("oFlag.equals=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByoFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where oFlag not equals to DEFAULT_O_FLAG
        defaultContactMasterHistoryShouldNotBeFound("oFlag.notEquals=" + DEFAULT_O_FLAG);

        // Get all the contactMasterHistoryList where oFlag not equals to UPDATED_O_FLAG
        defaultContactMasterHistoryShouldBeFound("oFlag.notEquals=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByoFlagIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where oFlag in DEFAULT_O_FLAG or UPDATED_O_FLAG
        defaultContactMasterHistoryShouldBeFound("oFlag.in=" + DEFAULT_O_FLAG + "," + UPDATED_O_FLAG);

        // Get all the contactMasterHistoryList where oFlag equals to UPDATED_O_FLAG
        defaultContactMasterHistoryShouldNotBeFound("oFlag.in=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByoFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where oFlag is not null
        defaultContactMasterHistoryShouldBeFound("oFlag.specified=true");

        // Get all the contactMasterHistoryList where oFlag is null
        defaultContactMasterHistoryShouldNotBeFound("oFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByoFlagContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where oFlag contains DEFAULT_O_FLAG
        defaultContactMasterHistoryShouldBeFound("oFlag.contains=" + DEFAULT_O_FLAG);

        // Get all the contactMasterHistoryList where oFlag contains UPDATED_O_FLAG
        defaultContactMasterHistoryShouldNotBeFound("oFlag.contains=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByoFlagNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where oFlag does not contain DEFAULT_O_FLAG
        defaultContactMasterHistoryShouldNotBeFound("oFlag.doesNotContain=" + DEFAULT_O_FLAG);

        // Get all the contactMasterHistoryList where oFlag does not contain UPDATED_O_FLAG
        defaultContactMasterHistoryShouldBeFound("oFlag.doesNotContain=" + UPDATED_O_FLAG);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOut equals to DEFAULT_OPT_OUT
        defaultContactMasterHistoryShouldBeFound("optOut.equals=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterHistoryList where optOut equals to UPDATED_OPT_OUT
        defaultContactMasterHistoryShouldNotBeFound("optOut.equals=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOut not equals to DEFAULT_OPT_OUT
        defaultContactMasterHistoryShouldNotBeFound("optOut.notEquals=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterHistoryList where optOut not equals to UPDATED_OPT_OUT
        defaultContactMasterHistoryShouldBeFound("optOut.notEquals=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOut in DEFAULT_OPT_OUT or UPDATED_OPT_OUT
        defaultContactMasterHistoryShouldBeFound("optOut.in=" + DEFAULT_OPT_OUT + "," + UPDATED_OPT_OUT);

        // Get all the contactMasterHistoryList where optOut equals to UPDATED_OPT_OUT
        defaultContactMasterHistoryShouldNotBeFound("optOut.in=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOut is not null
        defaultContactMasterHistoryShouldBeFound("optOut.specified=true");

        // Get all the contactMasterHistoryList where optOut is null
        defaultContactMasterHistoryShouldNotBeFound("optOut.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOut contains DEFAULT_OPT_OUT
        defaultContactMasterHistoryShouldBeFound("optOut.contains=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterHistoryList where optOut contains UPDATED_OPT_OUT
        defaultContactMasterHistoryShouldNotBeFound("optOut.contains=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOut does not contain DEFAULT_OPT_OUT
        defaultContactMasterHistoryShouldNotBeFound("optOut.doesNotContain=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterHistoryList where optOut does not contain UPDATED_OPT_OUT
        defaultContactMasterHistoryShouldBeFound("optOut.doesNotContain=" + UPDATED_OPT_OUT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOfaContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where ofaContactId equals to DEFAULT_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("ofaContactId.equals=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterHistoryList where ofaContactId equals to UPDATED_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("ofaContactId.equals=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOfaContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where ofaContactId not equals to DEFAULT_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("ofaContactId.notEquals=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterHistoryList where ofaContactId not equals to UPDATED_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("ofaContactId.notEquals=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOfaContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where ofaContactId in DEFAULT_OFA_CONTACT_ID or UPDATED_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("ofaContactId.in=" + DEFAULT_OFA_CONTACT_ID + "," + UPDATED_OFA_CONTACT_ID);

        // Get all the contactMasterHistoryList where ofaContactId equals to UPDATED_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("ofaContactId.in=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOfaContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where ofaContactId is not null
        defaultContactMasterHistoryShouldBeFound("ofaContactId.specified=true");

        // Get all the contactMasterHistoryList where ofaContactId is null
        defaultContactMasterHistoryShouldNotBeFound("ofaContactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByOfaContactIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where ofaContactId contains DEFAULT_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("ofaContactId.contains=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterHistoryList where ofaContactId contains UPDATED_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("ofaContactId.contains=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOfaContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where ofaContactId does not contain DEFAULT_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldNotBeFound("ofaContactId.doesNotContain=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterHistoryList where ofaContactId does not contain UPDATED_OFA_CONTACT_ID
        defaultContactMasterHistoryShouldBeFound("ofaContactId.doesNotContain=" + UPDATED_OFA_CONTACT_ID);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate equals to DEFAULT_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.equals=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate equals to UPDATED_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.equals=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate not equals to DEFAULT_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.notEquals=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate not equals to UPDATED_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.notEquals=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate in DEFAULT_OPT_OUT_DATE or UPDATED_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.in=" + DEFAULT_OPT_OUT_DATE + "," + UPDATED_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate equals to UPDATED_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.in=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate is not null
        defaultContactMasterHistoryShouldBeFound("optOutDate.specified=true");

        // Get all the contactMasterHistoryList where optOutDate is null
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate is greater than or equal to DEFAULT_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.greaterThanOrEqual=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate is greater than or equal to UPDATED_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.greaterThanOrEqual=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate is less than or equal to DEFAULT_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.lessThanOrEqual=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate is less than or equal to SMALLER_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.lessThanOrEqual=" + SMALLER_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate is less than DEFAULT_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.lessThan=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate is less than UPDATED_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.lessThan=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOptOutDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where optOutDate is greater than DEFAULT_OPT_OUT_DATE
        defaultContactMasterHistoryShouldNotBeFound("optOutDate.greaterThan=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterHistoryList where optOutDate is greater than SMALLER_OPT_OUT_DATE
        defaultContactMasterHistoryShouldBeFound("optOutDate.greaterThan=" + SMALLER_OPT_OUT_DATE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxOptoutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxOptout equals to DEFAULT_FAX_OPTOUT
        defaultContactMasterHistoryShouldBeFound("faxOptout.equals=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterHistoryList where faxOptout equals to UPDATED_FAX_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("faxOptout.equals=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxOptoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxOptout not equals to DEFAULT_FAX_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("faxOptout.notEquals=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterHistoryList where faxOptout not equals to UPDATED_FAX_OPTOUT
        defaultContactMasterHistoryShouldBeFound("faxOptout.notEquals=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxOptoutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxOptout in DEFAULT_FAX_OPTOUT or UPDATED_FAX_OPTOUT
        defaultContactMasterHistoryShouldBeFound("faxOptout.in=" + DEFAULT_FAX_OPTOUT + "," + UPDATED_FAX_OPTOUT);

        // Get all the contactMasterHistoryList where faxOptout equals to UPDATED_FAX_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("faxOptout.in=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxOptoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxOptout is not null
        defaultContactMasterHistoryShouldBeFound("faxOptout.specified=true");

        // Get all the contactMasterHistoryList where faxOptout is null
        defaultContactMasterHistoryShouldNotBeFound("faxOptout.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxOptoutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxOptout contains DEFAULT_FAX_OPTOUT
        defaultContactMasterHistoryShouldBeFound("faxOptout.contains=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterHistoryList where faxOptout contains UPDATED_FAX_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("faxOptout.contains=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByFaxOptoutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where faxOptout does not contain DEFAULT_FAX_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("faxOptout.doesNotContain=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterHistoryList where faxOptout does not contain UPDATED_FAX_OPTOUT
        defaultContactMasterHistoryShouldBeFound("faxOptout.doesNotContain=" + UPDATED_FAX_OPTOUT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDonotcallIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where donotcall equals to DEFAULT_DONOTCALL
        defaultContactMasterHistoryShouldBeFound("donotcall.equals=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterHistoryList where donotcall equals to UPDATED_DONOTCALL
        defaultContactMasterHistoryShouldNotBeFound("donotcall.equals=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDonotcallIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where donotcall not equals to DEFAULT_DONOTCALL
        defaultContactMasterHistoryShouldNotBeFound("donotcall.notEquals=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterHistoryList where donotcall not equals to UPDATED_DONOTCALL
        defaultContactMasterHistoryShouldBeFound("donotcall.notEquals=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDonotcallIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where donotcall in DEFAULT_DONOTCALL or UPDATED_DONOTCALL
        defaultContactMasterHistoryShouldBeFound("donotcall.in=" + DEFAULT_DONOTCALL + "," + UPDATED_DONOTCALL);

        // Get all the contactMasterHistoryList where donotcall equals to UPDATED_DONOTCALL
        defaultContactMasterHistoryShouldNotBeFound("donotcall.in=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDonotcallIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where donotcall is not null
        defaultContactMasterHistoryShouldBeFound("donotcall.specified=true");

        // Get all the contactMasterHistoryList where donotcall is null
        defaultContactMasterHistoryShouldNotBeFound("donotcall.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByDonotcallContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where donotcall contains DEFAULT_DONOTCALL
        defaultContactMasterHistoryShouldBeFound("donotcall.contains=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterHistoryList where donotcall contains UPDATED_DONOTCALL
        defaultContactMasterHistoryShouldNotBeFound("donotcall.contains=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDonotcallNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where donotcall does not contain DEFAULT_DONOTCALL
        defaultContactMasterHistoryShouldNotBeFound("donotcall.doesNotContain=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterHistoryList where donotcall does not contain UPDATED_DONOTCALL
        defaultContactMasterHistoryShouldBeFound("donotcall.doesNotContain=" + UPDATED_DONOTCALL);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnsendDirectMailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnsendDirectMail equals to DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldBeFound("dnsendDirectMail.equals=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterHistoryList where dnsendDirectMail equals to UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldNotBeFound("dnsendDirectMail.equals=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnsendDirectMailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnsendDirectMail not equals to DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldNotBeFound("dnsendDirectMail.notEquals=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterHistoryList where dnsendDirectMail not equals to UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldBeFound("dnsendDirectMail.notEquals=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnsendDirectMailIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnsendDirectMail in DEFAULT_DNSEND_DIRECT_MAIL or UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldBeFound("dnsendDirectMail.in=" + DEFAULT_DNSEND_DIRECT_MAIL + "," + UPDATED_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterHistoryList where dnsendDirectMail equals to UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldNotBeFound("dnsendDirectMail.in=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnsendDirectMailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnsendDirectMail is not null
        defaultContactMasterHistoryShouldBeFound("dnsendDirectMail.specified=true");

        // Get all the contactMasterHistoryList where dnsendDirectMail is null
        defaultContactMasterHistoryShouldNotBeFound("dnsendDirectMail.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnsendDirectMailContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnsendDirectMail contains DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldBeFound("dnsendDirectMail.contains=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterHistoryList where dnsendDirectMail contains UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldNotBeFound("dnsendDirectMail.contains=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnsendDirectMailNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnsendDirectMail does not contain DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldNotBeFound("dnsendDirectMail.doesNotContain=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterHistoryList where dnsendDirectMail does not contain UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterHistoryShouldBeFound("dnsendDirectMail.doesNotContain=" + UPDATED_DNSEND_DIRECT_MAIL);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideMhpIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp equals to DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideMhp.equals=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp equals to UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideMhp.equals=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideMhpIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp not equals to DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideMhp.notEquals=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp not equals to UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideMhp.notEquals=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideMhpIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp in DEFAULT_DNSHARE_OUTSIDE_MHP or UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideMhp.in=" + DEFAULT_DNSHARE_OUTSIDE_MHP + "," + UPDATED_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp equals to UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideMhp.in=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideMhpIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp is not null
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideMhp.specified=true");

        // Get all the contactMasterHistoryList where dnshareOutsideMhp is null
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideMhp.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideMhpContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp contains DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideMhp.contains=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp contains UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideMhp.contains=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideMhpNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp does not contain DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideMhp.doesNotContain=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterHistoryList where dnshareOutsideMhp does not contain UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideMhp.doesNotContain=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOutsideOfContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where outsideOfContact equals to DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldBeFound("outsideOfContact.equals=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterHistoryList where outsideOfContact equals to UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("outsideOfContact.equals=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOutsideOfContactIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where outsideOfContact not equals to DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("outsideOfContact.notEquals=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterHistoryList where outsideOfContact not equals to UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldBeFound("outsideOfContact.notEquals=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOutsideOfContactIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where outsideOfContact in DEFAULT_OUTSIDE_OF_CONTACT or UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldBeFound("outsideOfContact.in=" + DEFAULT_OUTSIDE_OF_CONTACT + "," + UPDATED_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterHistoryList where outsideOfContact equals to UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("outsideOfContact.in=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOutsideOfContactIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where outsideOfContact is not null
        defaultContactMasterHistoryShouldBeFound("outsideOfContact.specified=true");

        // Get all the contactMasterHistoryList where outsideOfContact is null
        defaultContactMasterHistoryShouldNotBeFound("outsideOfContact.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByOutsideOfContactContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where outsideOfContact contains DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldBeFound("outsideOfContact.contains=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterHistoryList where outsideOfContact contains UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("outsideOfContact.contains=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByOutsideOfContactNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where outsideOfContact does not contain DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("outsideOfContact.doesNotContain=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterHistoryList where outsideOfContact does not contain UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterHistoryShouldBeFound("outsideOfContact.doesNotContain=" + UPDATED_OUTSIDE_OF_CONTACT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideSnpIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp equals to DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideSnp.equals=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp equals to UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideSnp.equals=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideSnpIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp not equals to DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideSnp.notEquals=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp not equals to UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideSnp.notEquals=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideSnpIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp in DEFAULT_DNSHARE_OUTSIDE_SNP or UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideSnp.in=" + DEFAULT_DNSHARE_OUTSIDE_SNP + "," + UPDATED_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp equals to UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideSnp.in=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideSnpIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp is not null
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideSnp.specified=true");

        // Get all the contactMasterHistoryList where dnshareOutsideSnp is null
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideSnp.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideSnpContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp contains DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideSnp.contains=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp contains UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideSnp.contains=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByDnshareOutsideSnpNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp does not contain DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldNotBeFound("dnshareOutsideSnp.doesNotContain=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterHistoryList where dnshareOutsideSnp does not contain UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterHistoryShouldBeFound("dnshareOutsideSnp.doesNotContain=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailOptoutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where emailOptout equals to DEFAULT_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldBeFound("emailOptout.equals=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterHistoryList where emailOptout equals to UPDATED_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("emailOptout.equals=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailOptoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where emailOptout not equals to DEFAULT_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("emailOptout.notEquals=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterHistoryList where emailOptout not equals to UPDATED_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldBeFound("emailOptout.notEquals=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailOptoutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where emailOptout in DEFAULT_EMAIL_OPTOUT or UPDATED_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldBeFound("emailOptout.in=" + DEFAULT_EMAIL_OPTOUT + "," + UPDATED_EMAIL_OPTOUT);

        // Get all the contactMasterHistoryList where emailOptout equals to UPDATED_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("emailOptout.in=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailOptoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where emailOptout is not null
        defaultContactMasterHistoryShouldBeFound("emailOptout.specified=true");

        // Get all the contactMasterHistoryList where emailOptout is null
        defaultContactMasterHistoryShouldNotBeFound("emailOptout.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailOptoutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where emailOptout contains DEFAULT_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldBeFound("emailOptout.contains=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterHistoryList where emailOptout contains UPDATED_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("emailOptout.contains=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByEmailOptoutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where emailOptout does not contain DEFAULT_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("emailOptout.doesNotContain=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterHistoryList where emailOptout does not contain UPDATED_EMAIL_OPTOUT
        defaultContactMasterHistoryShouldBeFound("emailOptout.doesNotContain=" + UPDATED_EMAIL_OPTOUT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySmsOptoutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where smsOptout equals to DEFAULT_SMS_OPTOUT
        defaultContactMasterHistoryShouldBeFound("smsOptout.equals=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterHistoryList where smsOptout equals to UPDATED_SMS_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("smsOptout.equals=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySmsOptoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where smsOptout not equals to DEFAULT_SMS_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("smsOptout.notEquals=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterHistoryList where smsOptout not equals to UPDATED_SMS_OPTOUT
        defaultContactMasterHistoryShouldBeFound("smsOptout.notEquals=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySmsOptoutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where smsOptout in DEFAULT_SMS_OPTOUT or UPDATED_SMS_OPTOUT
        defaultContactMasterHistoryShouldBeFound("smsOptout.in=" + DEFAULT_SMS_OPTOUT + "," + UPDATED_SMS_OPTOUT);

        // Get all the contactMasterHistoryList where smsOptout equals to UPDATED_SMS_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("smsOptout.in=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySmsOptoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where smsOptout is not null
        defaultContactMasterHistoryShouldBeFound("smsOptout.specified=true");

        // Get all the contactMasterHistoryList where smsOptout is null
        defaultContactMasterHistoryShouldNotBeFound("smsOptout.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesBySmsOptoutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where smsOptout contains DEFAULT_SMS_OPTOUT
        defaultContactMasterHistoryShouldBeFound("smsOptout.contains=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterHistoryList where smsOptout contains UPDATED_SMS_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("smsOptout.contains=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesBySmsOptoutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where smsOptout does not contain DEFAULT_SMS_OPTOUT
        defaultContactMasterHistoryShouldNotBeFound("smsOptout.doesNotContain=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterHistoryList where smsOptout does not contain UPDATED_SMS_OPTOUT
        defaultContactMasterHistoryShouldBeFound("smsOptout.doesNotContain=" + UPDATED_SMS_OPTOUT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsSezContactFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isSezContactFlag equals to DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isSezContactFlag.equals=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isSezContactFlag equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isSezContactFlag.equals=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsSezContactFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isSezContactFlag not equals to DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isSezContactFlag.notEquals=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isSezContactFlag not equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isSezContactFlag.notEquals=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsSezContactFlagIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isSezContactFlag in DEFAULT_IS_SEZ_CONTACT_FLAG or UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isSezContactFlag.in=" + DEFAULT_IS_SEZ_CONTACT_FLAG + "," + UPDATED_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isSezContactFlag equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isSezContactFlag.in=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsSezContactFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isSezContactFlag is not null
        defaultContactMasterHistoryShouldBeFound("isSezContactFlag.specified=true");

        // Get all the contactMasterHistoryList where isSezContactFlag is null
        defaultContactMasterHistoryShouldNotBeFound("isSezContactFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsSezContactFlagContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isSezContactFlag contains DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isSezContactFlag.contains=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isSezContactFlag contains UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isSezContactFlag.contains=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsSezContactFlagNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isSezContactFlag does not contain DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isSezContactFlag.doesNotContain=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isSezContactFlag does not contain UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isSezContactFlag.doesNotContain=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsExemptContactFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isExemptContactFlag equals to DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isExemptContactFlag.equals=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isExemptContactFlag equals to UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isExemptContactFlag.equals=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsExemptContactFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isExemptContactFlag not equals to DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isExemptContactFlag.notEquals=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isExemptContactFlag not equals to UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isExemptContactFlag.notEquals=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsExemptContactFlagIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isExemptContactFlag in DEFAULT_IS_EXEMPT_CONTACT_FLAG or UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isExemptContactFlag.in=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG + "," + UPDATED_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isExemptContactFlag equals to UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isExemptContactFlag.in=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsExemptContactFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isExemptContactFlag is not null
        defaultContactMasterHistoryShouldBeFound("isExemptContactFlag.specified=true");

        // Get all the contactMasterHistoryList where isExemptContactFlag is null
        defaultContactMasterHistoryShouldNotBeFound("isExemptContactFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsExemptContactFlagContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isExemptContactFlag contains DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isExemptContactFlag.contains=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isExemptContactFlag contains UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isExemptContactFlag.contains=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsExemptContactFlagNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isExemptContactFlag does not contain DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldNotBeFound("isExemptContactFlag.doesNotContain=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterHistoryList where isExemptContactFlag does not contain UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterHistoryShouldBeFound("isExemptContactFlag.doesNotContain=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNotApplicableIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNotApplicable equals to DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldBeFound("gstNotApplicable.equals=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterHistoryList where gstNotApplicable equals to UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldNotBeFound("gstNotApplicable.equals=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNotApplicableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNotApplicable not equals to DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldNotBeFound("gstNotApplicable.notEquals=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterHistoryList where gstNotApplicable not equals to UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldBeFound("gstNotApplicable.notEquals=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNotApplicableIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNotApplicable in DEFAULT_GST_NOT_APPLICABLE or UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldBeFound("gstNotApplicable.in=" + DEFAULT_GST_NOT_APPLICABLE + "," + UPDATED_GST_NOT_APPLICABLE);

        // Get all the contactMasterHistoryList where gstNotApplicable equals to UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldNotBeFound("gstNotApplicable.in=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNotApplicableIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNotApplicable is not null
        defaultContactMasterHistoryShouldBeFound("gstNotApplicable.specified=true");

        // Get all the contactMasterHistoryList where gstNotApplicable is null
        defaultContactMasterHistoryShouldNotBeFound("gstNotApplicable.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNotApplicableContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNotApplicable contains DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldBeFound("gstNotApplicable.contains=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterHistoryList where gstNotApplicable contains UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldNotBeFound("gstNotApplicable.contains=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNotApplicableNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNotApplicable does not contain DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldNotBeFound("gstNotApplicable.doesNotContain=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterHistoryList where gstNotApplicable does not contain UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterHistoryShouldBeFound("gstNotApplicable.doesNotContain=" + UPDATED_GST_NOT_APPLICABLE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByBillingContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where billingContact equals to DEFAULT_BILLING_CONTACT
        defaultContactMasterHistoryShouldBeFound("billingContact.equals=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterHistoryList where billingContact equals to UPDATED_BILLING_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("billingContact.equals=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByBillingContactIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where billingContact not equals to DEFAULT_BILLING_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("billingContact.notEquals=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterHistoryList where billingContact not equals to UPDATED_BILLING_CONTACT
        defaultContactMasterHistoryShouldBeFound("billingContact.notEquals=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByBillingContactIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where billingContact in DEFAULT_BILLING_CONTACT or UPDATED_BILLING_CONTACT
        defaultContactMasterHistoryShouldBeFound("billingContact.in=" + DEFAULT_BILLING_CONTACT + "," + UPDATED_BILLING_CONTACT);

        // Get all the contactMasterHistoryList where billingContact equals to UPDATED_BILLING_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("billingContact.in=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByBillingContactIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where billingContact is not null
        defaultContactMasterHistoryShouldBeFound("billingContact.specified=true");

        // Get all the contactMasterHistoryList where billingContact is null
        defaultContactMasterHistoryShouldNotBeFound("billingContact.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByBillingContactContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where billingContact contains DEFAULT_BILLING_CONTACT
        defaultContactMasterHistoryShouldBeFound("billingContact.contains=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterHistoryList where billingContact contains UPDATED_BILLING_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("billingContact.contains=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByBillingContactNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where billingContact does not contain DEFAULT_BILLING_CONTACT
        defaultContactMasterHistoryShouldNotBeFound("billingContact.doesNotContain=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterHistoryList where billingContact does not contain UPDATED_BILLING_CONTACT
        defaultContactMasterHistoryShouldBeFound("billingContact.doesNotContain=" + UPDATED_BILLING_CONTACT);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstPartyTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstPartyType equals to DEFAULT_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldBeFound("gstPartyType.equals=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterHistoryList where gstPartyType equals to UPDATED_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldNotBeFound("gstPartyType.equals=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstPartyTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstPartyType not equals to DEFAULT_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldNotBeFound("gstPartyType.notEquals=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterHistoryList where gstPartyType not equals to UPDATED_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldBeFound("gstPartyType.notEquals=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstPartyTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstPartyType in DEFAULT_GST_PARTY_TYPE or UPDATED_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldBeFound("gstPartyType.in=" + DEFAULT_GST_PARTY_TYPE + "," + UPDATED_GST_PARTY_TYPE);

        // Get all the contactMasterHistoryList where gstPartyType equals to UPDATED_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldNotBeFound("gstPartyType.in=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstPartyTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstPartyType is not null
        defaultContactMasterHistoryShouldBeFound("gstPartyType.specified=true");

        // Get all the contactMasterHistoryList where gstPartyType is null
        defaultContactMasterHistoryShouldNotBeFound("gstPartyType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstPartyTypeContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstPartyType contains DEFAULT_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldBeFound("gstPartyType.contains=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterHistoryList where gstPartyType contains UPDATED_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldNotBeFound("gstPartyType.contains=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstPartyTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstPartyType does not contain DEFAULT_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldNotBeFound("gstPartyType.doesNotContain=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterHistoryList where gstPartyType does not contain UPDATED_GST_PARTY_TYPE
        defaultContactMasterHistoryShouldBeFound("gstPartyType.doesNotContain=" + UPDATED_GST_PARTY_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTanIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tan equals to DEFAULT_TAN
        defaultContactMasterHistoryShouldBeFound("tan.equals=" + DEFAULT_TAN);

        // Get all the contactMasterHistoryList where tan equals to UPDATED_TAN
        defaultContactMasterHistoryShouldNotBeFound("tan.equals=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tan not equals to DEFAULT_TAN
        defaultContactMasterHistoryShouldNotBeFound("tan.notEquals=" + DEFAULT_TAN);

        // Get all the contactMasterHistoryList where tan not equals to UPDATED_TAN
        defaultContactMasterHistoryShouldBeFound("tan.notEquals=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTanIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tan in DEFAULT_TAN or UPDATED_TAN
        defaultContactMasterHistoryShouldBeFound("tan.in=" + DEFAULT_TAN + "," + UPDATED_TAN);

        // Get all the contactMasterHistoryList where tan equals to UPDATED_TAN
        defaultContactMasterHistoryShouldNotBeFound("tan.in=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTanIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tan is not null
        defaultContactMasterHistoryShouldBeFound("tan.specified=true");

        // Get all the contactMasterHistoryList where tan is null
        defaultContactMasterHistoryShouldNotBeFound("tan.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByTanContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tan contains DEFAULT_TAN
        defaultContactMasterHistoryShouldBeFound("tan.contains=" + DEFAULT_TAN);

        // Get all the contactMasterHistoryList where tan contains UPDATED_TAN
        defaultContactMasterHistoryShouldNotBeFound("tan.contains=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTanNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tan does not contain DEFAULT_TAN
        defaultContactMasterHistoryShouldNotBeFound("tan.doesNotContain=" + DEFAULT_TAN);

        // Get all the contactMasterHistoryList where tan does not contain UPDATED_TAN
        defaultContactMasterHistoryShouldBeFound("tan.doesNotContain=" + UPDATED_TAN);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNumber equals to DEFAULT_GST_NUMBER
        defaultContactMasterHistoryShouldBeFound("gstNumber.equals=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterHistoryList where gstNumber equals to UPDATED_GST_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("gstNumber.equals=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNumber not equals to DEFAULT_GST_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("gstNumber.notEquals=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterHistoryList where gstNumber not equals to UPDATED_GST_NUMBER
        defaultContactMasterHistoryShouldBeFound("gstNumber.notEquals=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNumberIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNumber in DEFAULT_GST_NUMBER or UPDATED_GST_NUMBER
        defaultContactMasterHistoryShouldBeFound("gstNumber.in=" + DEFAULT_GST_NUMBER + "," + UPDATED_GST_NUMBER);

        // Get all the contactMasterHistoryList where gstNumber equals to UPDATED_GST_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("gstNumber.in=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNumber is not null
        defaultContactMasterHistoryShouldBeFound("gstNumber.specified=true");

        // Get all the contactMasterHistoryList where gstNumber is null
        defaultContactMasterHistoryShouldNotBeFound("gstNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNumberContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNumber contains DEFAULT_GST_NUMBER
        defaultContactMasterHistoryShouldBeFound("gstNumber.contains=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterHistoryList where gstNumber contains UPDATED_GST_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("gstNumber.contains=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByGstNumberNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where gstNumber does not contain DEFAULT_GST_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("gstNumber.doesNotContain=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterHistoryList where gstNumber does not contain UPDATED_GST_NUMBER
        defaultContactMasterHistoryShouldBeFound("gstNumber.doesNotContain=" + UPDATED_GST_NUMBER);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTdsNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tdsNumber equals to DEFAULT_TDS_NUMBER
        defaultContactMasterHistoryShouldBeFound("tdsNumber.equals=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterHistoryList where tdsNumber equals to UPDATED_TDS_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("tdsNumber.equals=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTdsNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tdsNumber not equals to DEFAULT_TDS_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("tdsNumber.notEquals=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterHistoryList where tdsNumber not equals to UPDATED_TDS_NUMBER
        defaultContactMasterHistoryShouldBeFound("tdsNumber.notEquals=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTdsNumberIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tdsNumber in DEFAULT_TDS_NUMBER or UPDATED_TDS_NUMBER
        defaultContactMasterHistoryShouldBeFound("tdsNumber.in=" + DEFAULT_TDS_NUMBER + "," + UPDATED_TDS_NUMBER);

        // Get all the contactMasterHistoryList where tdsNumber equals to UPDATED_TDS_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("tdsNumber.in=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTdsNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tdsNumber is not null
        defaultContactMasterHistoryShouldBeFound("tdsNumber.specified=true");

        // Get all the contactMasterHistoryList where tdsNumber is null
        defaultContactMasterHistoryShouldNotBeFound("tdsNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByTdsNumberContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tdsNumber contains DEFAULT_TDS_NUMBER
        defaultContactMasterHistoryShouldBeFound("tdsNumber.contains=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterHistoryList where tdsNumber contains UPDATED_TDS_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("tdsNumber.contains=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByTdsNumberNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where tdsNumber does not contain DEFAULT_TDS_NUMBER
        defaultContactMasterHistoryShouldNotBeFound("tdsNumber.doesNotContain=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterHistoryList where tdsNumber does not contain UPDATED_TDS_NUMBER
        defaultContactMasterHistoryShouldBeFound("tdsNumber.doesNotContain=" + UPDATED_TDS_NUMBER);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdBy equals to DEFAULT_CREATED_BY
        defaultContactMasterHistoryShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterHistoryList where createdBy equals to UPDATED_CREATED_BY
        defaultContactMasterHistoryShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdBy not equals to DEFAULT_CREATED_BY
        defaultContactMasterHistoryShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterHistoryList where createdBy not equals to UPDATED_CREATED_BY
        defaultContactMasterHistoryShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultContactMasterHistoryShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the contactMasterHistoryList where createdBy equals to UPDATED_CREATED_BY
        defaultContactMasterHistoryShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdBy is not null
        defaultContactMasterHistoryShouldBeFound("createdBy.specified=true");

        // Get all the contactMasterHistoryList where createdBy is null
        defaultContactMasterHistoryShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdBy contains DEFAULT_CREATED_BY
        defaultContactMasterHistoryShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterHistoryList where createdBy contains UPDATED_CREATED_BY
        defaultContactMasterHistoryShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdBy does not contain DEFAULT_CREATED_BY
        defaultContactMasterHistoryShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterHistoryList where createdBy does not contain UPDATED_CREATED_BY
        defaultContactMasterHistoryShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate equals to DEFAULT_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate equals to UPDATED_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate not equals to UPDATED_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate equals to UPDATED_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate is not null
        defaultContactMasterHistoryShouldBeFound("createdDate.specified=true");

        // Get all the contactMasterHistoryList where createdDate is null
        defaultContactMasterHistoryShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate is less than DEFAULT_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate is less than UPDATED_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultContactMasterHistoryShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterHistoryList where createdDate is greater than SMALLER_CREATED_DATE
        defaultContactMasterHistoryShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterHistoryList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterHistoryList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the contactMasterHistoryList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedBy is not null
        defaultContactMasterHistoryShouldBeFound("lastModifiedBy.specified=true");

        // Get all the contactMasterHistoryList where lastModifiedBy is null
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterHistoryList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterHistoryList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultContactMasterHistoryShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate is not null
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.specified=true");

        // Get all the contactMasterHistoryList where lastModifiedDate is null
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterHistoryList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultContactMasterHistoryShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isDeleted equals to DEFAULT_IS_DELETED
        defaultContactMasterHistoryShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterHistoryList where isDeleted equals to UPDATED_IS_DELETED
        defaultContactMasterHistoryShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultContactMasterHistoryShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterHistoryList where isDeleted not equals to UPDATED_IS_DELETED
        defaultContactMasterHistoryShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultContactMasterHistoryShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the contactMasterHistoryList where isDeleted equals to UPDATED_IS_DELETED
        defaultContactMasterHistoryShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isDeleted is not null
        defaultContactMasterHistoryShouldBeFound("isDeleted.specified=true");

        // Get all the contactMasterHistoryList where isDeleted is null
        defaultContactMasterHistoryShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isDeleted contains DEFAULT_IS_DELETED
        defaultContactMasterHistoryShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterHistoryList where isDeleted contains UPDATED_IS_DELETED
        defaultContactMasterHistoryShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMasterHistoriesByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterHistoryRepository.saveAndFlush(contactMasterHistory);

        // Get all the contactMasterHistoryList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultContactMasterHistoryShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterHistoryList where isDeleted does not contain UPDATED_IS_DELETED
        defaultContactMasterHistoryShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactMasterHistoryShouldBeFound(String filter) throws Exception {
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMasterHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].salutation").value(hasItem(DEFAULT_SALUTATION)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].phoneNum").value(hasItem(DEFAULT_PHONE_NUM)))
            .andExpect(jsonPath("$.[*].faxNum").value(hasItem(DEFAULT_FAX_NUM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].mobileNum").value(hasItem(DEFAULT_MOBILE_NUM)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].keyPerson").value(hasItem(DEFAULT_KEY_PERSON)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].levelImportance").value(hasItem(DEFAULT_LEVEL_IMPORTANCE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].removeReason").value(hasItem(DEFAULT_REMOVE_REASON)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].oFlag").value(hasItem(DEFAULT_O_FLAG)))
            .andExpect(jsonPath("$.[*].optOut").value(hasItem(DEFAULT_OPT_OUT)))
            .andExpect(jsonPath("$.[*].ofaContactId").value(hasItem(DEFAULT_OFA_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].optOutDate").value(hasItem(DEFAULT_OPT_OUT_DATE.toString())))
            .andExpect(jsonPath("$.[*].faxOptout").value(hasItem(DEFAULT_FAX_OPTOUT)))
            .andExpect(jsonPath("$.[*].donotcall").value(hasItem(DEFAULT_DONOTCALL)))
            .andExpect(jsonPath("$.[*].dnsendDirectMail").value(hasItem(DEFAULT_DNSEND_DIRECT_MAIL)))
            .andExpect(jsonPath("$.[*].dnshareOutsideMhp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_MHP)))
            .andExpect(jsonPath("$.[*].outsideOfContact").value(hasItem(DEFAULT_OUTSIDE_OF_CONTACT)))
            .andExpect(jsonPath("$.[*].dnshareOutsideSnp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_SNP)))
            .andExpect(jsonPath("$.[*].emailOptout").value(hasItem(DEFAULT_EMAIL_OPTOUT)))
            .andExpect(jsonPath("$.[*].smsOptout").value(hasItem(DEFAULT_SMS_OPTOUT)))
            .andExpect(jsonPath("$.[*].isSezContactFlag").value(hasItem(DEFAULT_IS_SEZ_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].isExemptContactFlag").value(hasItem(DEFAULT_IS_EXEMPT_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].gstNotApplicable").value(hasItem(DEFAULT_GST_NOT_APPLICABLE)))
            .andExpect(jsonPath("$.[*].billingContact").value(hasItem(DEFAULT_BILLING_CONTACT)))
            .andExpect(jsonPath("$.[*].gstPartyType").value(hasItem(DEFAULT_GST_PARTY_TYPE)))
            .andExpect(jsonPath("$.[*].tan").value(hasItem(DEFAULT_TAN)))
            .andExpect(jsonPath("$.[*].gstNumber").value(hasItem(DEFAULT_GST_NUMBER)))
            .andExpect(jsonPath("$.[*].tdsNumber").value(hasItem(DEFAULT_TDS_NUMBER)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactMasterHistoryShouldNotBeFound(String filter) throws Exception {
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactMasterHistory() throws Exception {
        // Get the contactMasterHistory
        restContactMasterHistoryMockMvc.perform(get("/api/contact-master-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactMasterHistory() throws Exception {
        // Initialize the database
        contactMasterHistoryService.save(contactMasterHistory);

        int databaseSizeBeforeUpdate = contactMasterHistoryRepository.findAll().size();

        // Update the contactMasterHistory
        ContactMasterHistory updatedContactMasterHistory = contactMasterHistoryRepository.findById(contactMasterHistory.getId()).get();
        // Disconnect from session so that the updates on updatedContactMasterHistory are not directly saved in db
        em.detach(updatedContactMasterHistory);
        updatedContactMasterHistory
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .salutation(UPDATED_SALUTATION)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .designation(UPDATED_DESIGNATION)
            .department(UPDATED_DEPARTMENT)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .pin(UPDATED_PIN)
            .phoneNum(UPDATED_PHONE_NUM)
            .faxNum(UPDATED_FAX_NUM)
            .email(UPDATED_EMAIL)
            .mobileNum(UPDATED_MOBILE_NUM)
            .recordId(UPDATED_RECORD_ID)
            .companyCode(UPDATED_COMPANY_CODE)
            .comments(UPDATED_COMMENTS)
            .keyPerson(UPDATED_KEY_PERSON)
            .contactId(UPDATED_CONTACT_ID)
            .levelImportance(UPDATED_LEVEL_IMPORTANCE)
            .status(UPDATED_STATUS)
            .removeReason(UPDATED_REMOVE_REASON)
            .cityId(UPDATED_CITY_ID)
            .oFlag(UPDATED_O_FLAG)
            .optOut(UPDATED_OPT_OUT)
            .ofaContactId(UPDATED_OFA_CONTACT_ID)
            .optOutDate(UPDATED_OPT_OUT_DATE)
            .faxOptout(UPDATED_FAX_OPTOUT)
            .donotcall(UPDATED_DONOTCALL)
            .dnsendDirectMail(UPDATED_DNSEND_DIRECT_MAIL)
            .dnshareOutsideMhp(UPDATED_DNSHARE_OUTSIDE_MHP)
            .outsideOfContact(UPDATED_OUTSIDE_OF_CONTACT)
            .dnshareOutsideSnp(UPDATED_DNSHARE_OUTSIDE_SNP)
            .emailOptout(UPDATED_EMAIL_OPTOUT)
            .smsOptout(UPDATED_SMS_OPTOUT)
            .isSezContactFlag(UPDATED_IS_SEZ_CONTACT_FLAG)
            .isExemptContactFlag(UPDATED_IS_EXEMPT_CONTACT_FLAG)
            .gstNotApplicable(UPDATED_GST_NOT_APPLICABLE)
            .billingContact(UPDATED_BILLING_CONTACT)
            .gstPartyType(UPDATED_GST_PARTY_TYPE)
            .tan(UPDATED_TAN)
            .gstNumber(UPDATED_GST_NUMBER)
            .tdsNumber(UPDATED_TDS_NUMBER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);

        restContactMasterHistoryMockMvc.perform(put("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedContactMasterHistory)))
            .andExpect(status().isOk());

        // Validate the ContactMasterHistory in the database
        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeUpdate);
        ContactMasterHistory testContactMasterHistory = contactMasterHistoryList.get(contactMasterHistoryList.size() - 1);
        assertThat(testContactMasterHistory.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testContactMasterHistory.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testContactMasterHistory.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testContactMasterHistory.getSalutation()).isEqualTo(UPDATED_SALUTATION);
        assertThat(testContactMasterHistory.getAddress1()).isEqualTo(UPDATED_ADDRESS_1);
        assertThat(testContactMasterHistory.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testContactMasterHistory.getAddress3()).isEqualTo(UPDATED_ADDRESS_3);
        assertThat(testContactMasterHistory.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testContactMasterHistory.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testContactMasterHistory.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testContactMasterHistory.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testContactMasterHistory.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testContactMasterHistory.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testContactMasterHistory.getPhoneNum()).isEqualTo(UPDATED_PHONE_NUM);
        assertThat(testContactMasterHistory.getFaxNum()).isEqualTo(UPDATED_FAX_NUM);
        assertThat(testContactMasterHistory.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testContactMasterHistory.getMobileNum()).isEqualTo(UPDATED_MOBILE_NUM);
        assertThat(testContactMasterHistory.getRecordId()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testContactMasterHistory.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
        assertThat(testContactMasterHistory.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testContactMasterHistory.getKeyPerson()).isEqualTo(UPDATED_KEY_PERSON);
        assertThat(testContactMasterHistory.getContactId()).isEqualTo(UPDATED_CONTACT_ID);
        assertThat(testContactMasterHistory.getLevelImportance()).isEqualTo(UPDATED_LEVEL_IMPORTANCE);
        assertThat(testContactMasterHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testContactMasterHistory.getRemoveReason()).isEqualTo(UPDATED_REMOVE_REASON);
        assertThat(testContactMasterHistory.getCityId()).isEqualTo(UPDATED_CITY_ID);
        assertThat(testContactMasterHistory.getoFlag()).isEqualTo(UPDATED_O_FLAG);
        assertThat(testContactMasterHistory.getOptOut()).isEqualTo(UPDATED_OPT_OUT);
        assertThat(testContactMasterHistory.getOfaContactId()).isEqualTo(UPDATED_OFA_CONTACT_ID);
        assertThat(testContactMasterHistory.getOptOutDate()).isEqualTo(UPDATED_OPT_OUT_DATE);
        assertThat(testContactMasterHistory.getFaxOptout()).isEqualTo(UPDATED_FAX_OPTOUT);
        assertThat(testContactMasterHistory.getDonotcall()).isEqualTo(UPDATED_DONOTCALL);
        assertThat(testContactMasterHistory.getDnsendDirectMail()).isEqualTo(UPDATED_DNSEND_DIRECT_MAIL);
        assertThat(testContactMasterHistory.getDnshareOutsideMhp()).isEqualTo(UPDATED_DNSHARE_OUTSIDE_MHP);
        assertThat(testContactMasterHistory.getOutsideOfContact()).isEqualTo(UPDATED_OUTSIDE_OF_CONTACT);
        assertThat(testContactMasterHistory.getDnshareOutsideSnp()).isEqualTo(UPDATED_DNSHARE_OUTSIDE_SNP);
        assertThat(testContactMasterHistory.getEmailOptout()).isEqualTo(UPDATED_EMAIL_OPTOUT);
        assertThat(testContactMasterHistory.getSmsOptout()).isEqualTo(UPDATED_SMS_OPTOUT);
        assertThat(testContactMasterHistory.getIsSezContactFlag()).isEqualTo(UPDATED_IS_SEZ_CONTACT_FLAG);
        assertThat(testContactMasterHistory.getIsExemptContactFlag()).isEqualTo(UPDATED_IS_EXEMPT_CONTACT_FLAG);
        assertThat(testContactMasterHistory.getGstNotApplicable()).isEqualTo(UPDATED_GST_NOT_APPLICABLE);
        assertThat(testContactMasterHistory.getBillingContact()).isEqualTo(UPDATED_BILLING_CONTACT);
        assertThat(testContactMasterHistory.getGstPartyType()).isEqualTo(UPDATED_GST_PARTY_TYPE);
        assertThat(testContactMasterHistory.getTan()).isEqualTo(UPDATED_TAN);
        assertThat(testContactMasterHistory.getGstNumber()).isEqualTo(UPDATED_GST_NUMBER);
        assertThat(testContactMasterHistory.getTdsNumber()).isEqualTo(UPDATED_TDS_NUMBER);
        assertThat(testContactMasterHistory.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testContactMasterHistory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testContactMasterHistory.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testContactMasterHistory.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testContactMasterHistory.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingContactMasterHistory() throws Exception {
        int databaseSizeBeforeUpdate = contactMasterHistoryRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactMasterHistoryMockMvc.perform(put("/api/contact-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMasterHistory)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMasterHistory in the database
        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactMasterHistory() throws Exception {
        // Initialize the database
        contactMasterHistoryService.save(contactMasterHistory);

        int databaseSizeBeforeDelete = contactMasterHistoryRepository.findAll().size();

        // Delete the contactMasterHistory
        restContactMasterHistoryMockMvc.perform(delete("/api/contact-master-histories/{id}", contactMasterHistory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactMasterHistory> contactMasterHistoryList = contactMasterHistoryRepository.findAll();
        assertThat(contactMasterHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
