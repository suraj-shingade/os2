package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.domain.CompGstNotApplicable;
import com.crisil.onesource.domain.ContactChangeRequest;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.ContactMasterRepository;
import com.crisil.onesource.service.ContactMasterService;
import com.crisil.onesource.service.dto.ContactMasterCriteria;
import com.crisil.onesource.service.ContactMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactMasterResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SALUTATION = "AAAAAAAAAA";
    private static final String UPDATED_SALUTATION = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_3 = "BBBBBBBBBB";

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_PIN = "AAAAAAAAAA";
    private static final String UPDATED_PIN = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUM = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_FAX_NUM = "AAAAAAAAAA";
    private static final String UPDATED_FAX_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NUM = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_KEY_PERSON = "AAAAAAAAAA";
    private static final String UPDATED_KEY_PERSON = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LEVEL_IMPORTANCE = "AAAAAAAAAA";
    private static final String UPDATED_LEVEL_IMPORTANCE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REMOVE_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REMOVE_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_CITY_ID = "AAAAAAAAAA";
    private static final String UPDATED_CITY_ID = "BBBBBBBBBB";

    private static final String DEFAULT_O_FLAG = "AAA";
    private static final String UPDATED_O_FLAG = "BBB";

    private static final String DEFAULT_OPT_OUT = "AAAAAAAAAA";
    private static final String UPDATED_OPT_OUT = "BBBBBBBBBB";

    private static final String DEFAULT_OFA_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_OFA_CONTACT_ID = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_OPT_OUT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_OPT_OUT_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_OPT_OUT_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_FAX_OPTOUT = "AAAAAAAAAA";
    private static final String UPDATED_FAX_OPTOUT = "BBBBBBBBBB";

    private static final String DEFAULT_DONOTCALL = "AAAAAAAAAA";
    private static final String UPDATED_DONOTCALL = "BBBBBBBBBB";

    private static final String DEFAULT_DNSEND_DIRECT_MAIL = "AAAAAAAAAA";
    private static final String UPDATED_DNSEND_DIRECT_MAIL = "BBBBBBBBBB";

    private static final String DEFAULT_DNSHARE_OUTSIDE_MHP = "AAAAAAAAAA";
    private static final String UPDATED_DNSHARE_OUTSIDE_MHP = "BBBBBBBBBB";

    private static final String DEFAULT_OUTSIDE_OF_CONTACT = "AAAAAAAAAA";
    private static final String UPDATED_OUTSIDE_OF_CONTACT = "BBBBBBBBBB";

    private static final String DEFAULT_DNSHARE_OUTSIDE_SNP = "AAAAAAAAAA";
    private static final String UPDATED_DNSHARE_OUTSIDE_SNP = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_OPTOUT = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_OPTOUT = "BBBBBBBBBB";

    private static final String DEFAULT_SMS_OPTOUT = "A";
    private static final String UPDATED_SMS_OPTOUT = "B";

    private static final String DEFAULT_IS_SEZ_CONTACT_FLAG = "A";
    private static final String UPDATED_IS_SEZ_CONTACT_FLAG = "B";

    private static final String DEFAULT_IS_EXEMPT_CONTACT_FLAG = "AAAAAAAAAA";
    private static final String UPDATED_IS_EXEMPT_CONTACT_FLAG = "BBBBBBBBBB";

    private static final String DEFAULT_GST_NOT_APPLICABLE = "A";
    private static final String UPDATED_GST_NOT_APPLICABLE = "B";

    private static final String DEFAULT_BILLING_CONTACT = "A";
    private static final String UPDATED_BILLING_CONTACT = "B";

    private static final String DEFAULT_GST_PARTY_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_GST_PARTY_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_TAN = "AAAAAAAAAA";
    private static final String UPDATED_TAN = "BBBBBBBBBB";

    private static final String DEFAULT_GST_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_GST_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TDS_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TDS_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private ContactMasterRepository contactMasterRepository;

    @Autowired
    private ContactMasterService contactMasterService;

    @Autowired
    private ContactMasterQueryService contactMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactMasterMockMvc;

    private ContactMaster contactMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMaster createEntity(EntityManager em) {
        ContactMaster contactMaster = new ContactMaster()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .salutation(DEFAULT_SALUTATION)
            .address1(DEFAULT_ADDRESS_1)
            .address2(DEFAULT_ADDRESS_2)
            .address3(DEFAULT_ADDRESS_3)
            .designation(DEFAULT_DESIGNATION)
            .department(DEFAULT_DEPARTMENT)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .pin(DEFAULT_PIN)
            .phoneNum(DEFAULT_PHONE_NUM)
            .faxNum(DEFAULT_FAX_NUM)
            .email(DEFAULT_EMAIL)
            .mobileNum(DEFAULT_MOBILE_NUM)
            .recordId(DEFAULT_RECORD_ID)
            .comments(DEFAULT_COMMENTS)
            .keyPerson(DEFAULT_KEY_PERSON)
            .contactId(DEFAULT_CONTACT_ID)
            .levelImportance(DEFAULT_LEVEL_IMPORTANCE)
            .status(DEFAULT_STATUS)
            .removeReason(DEFAULT_REMOVE_REASON)
            .cityId(DEFAULT_CITY_ID)
            .oFlag(DEFAULT_O_FLAG)
            .optOut(DEFAULT_OPT_OUT)
            .ofaContactId(DEFAULT_OFA_CONTACT_ID)
            .optOutDate(DEFAULT_OPT_OUT_DATE)
            .faxOptout(DEFAULT_FAX_OPTOUT)
            .donotcall(DEFAULT_DONOTCALL)
            .dnsendDirectMail(DEFAULT_DNSEND_DIRECT_MAIL)
            .dnshareOutsideMhp(DEFAULT_DNSHARE_OUTSIDE_MHP)
            .outsideOfContact(DEFAULT_OUTSIDE_OF_CONTACT)
            .dnshareOutsideSnp(DEFAULT_DNSHARE_OUTSIDE_SNP)
            .emailOptout(DEFAULT_EMAIL_OPTOUT)
            .smsOptout(DEFAULT_SMS_OPTOUT)
            .isSezContactFlag(DEFAULT_IS_SEZ_CONTACT_FLAG)
            .isExemptContactFlag(DEFAULT_IS_EXEMPT_CONTACT_FLAG)
            .gstNotApplicable(DEFAULT_GST_NOT_APPLICABLE)
            .billingContact(DEFAULT_BILLING_CONTACT)
            .gstPartyType(DEFAULT_GST_PARTY_TYPE)
            .tan(DEFAULT_TAN)
            .gstNumber(DEFAULT_GST_NUMBER)
            .tdsNumber(DEFAULT_TDS_NUMBER)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED);
        // Add required entity
        OnesourceCompanyMaster onesourceCompanyMaster;
        if (TestUtil.findAll(em, OnesourceCompanyMaster.class).isEmpty()) {
            onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
            em.persist(onesourceCompanyMaster);
            em.flush();
        } else {
            onesourceCompanyMaster = TestUtil.findAll(em, OnesourceCompanyMaster.class).get(0);
        }
        contactMaster.setCompanyCode(onesourceCompanyMaster);
        return contactMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMaster createUpdatedEntity(EntityManager em) {
        ContactMaster contactMaster = new ContactMaster()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .salutation(UPDATED_SALUTATION)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .designation(UPDATED_DESIGNATION)
            .department(UPDATED_DEPARTMENT)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .pin(UPDATED_PIN)
            .phoneNum(UPDATED_PHONE_NUM)
            .faxNum(UPDATED_FAX_NUM)
            .email(UPDATED_EMAIL)
            .mobileNum(UPDATED_MOBILE_NUM)
            .recordId(UPDATED_RECORD_ID)
            .comments(UPDATED_COMMENTS)
            .keyPerson(UPDATED_KEY_PERSON)
            .contactId(UPDATED_CONTACT_ID)
            .levelImportance(UPDATED_LEVEL_IMPORTANCE)
            .status(UPDATED_STATUS)
            .removeReason(UPDATED_REMOVE_REASON)
            .cityId(UPDATED_CITY_ID)
            .oFlag(UPDATED_O_FLAG)
            .optOut(UPDATED_OPT_OUT)
            .ofaContactId(UPDATED_OFA_CONTACT_ID)
            .optOutDate(UPDATED_OPT_OUT_DATE)
            .faxOptout(UPDATED_FAX_OPTOUT)
            .donotcall(UPDATED_DONOTCALL)
            .dnsendDirectMail(UPDATED_DNSEND_DIRECT_MAIL)
            .dnshareOutsideMhp(UPDATED_DNSHARE_OUTSIDE_MHP)
            .outsideOfContact(UPDATED_OUTSIDE_OF_CONTACT)
            .dnshareOutsideSnp(UPDATED_DNSHARE_OUTSIDE_SNP)
            .emailOptout(UPDATED_EMAIL_OPTOUT)
            .smsOptout(UPDATED_SMS_OPTOUT)
            .isSezContactFlag(UPDATED_IS_SEZ_CONTACT_FLAG)
            .isExemptContactFlag(UPDATED_IS_EXEMPT_CONTACT_FLAG)
            .gstNotApplicable(UPDATED_GST_NOT_APPLICABLE)
            .billingContact(UPDATED_BILLING_CONTACT)
            .gstPartyType(UPDATED_GST_PARTY_TYPE)
            .tan(UPDATED_TAN)
            .gstNumber(UPDATED_GST_NUMBER)
            .tdsNumber(UPDATED_TDS_NUMBER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);
        // Add required entity
        OnesourceCompanyMaster onesourceCompanyMaster;
        if (TestUtil.findAll(em, OnesourceCompanyMaster.class).isEmpty()) {
            onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createUpdatedEntity(em);
            em.persist(onesourceCompanyMaster);
            em.flush();
        } else {
            onesourceCompanyMaster = TestUtil.findAll(em, OnesourceCompanyMaster.class).get(0);
        }
        contactMaster.setCompanyCode(onesourceCompanyMaster);
        return contactMaster;
    }

    @BeforeEach
    public void initTest() {
        contactMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactMaster() throws Exception {
        int databaseSizeBeforeCreate = contactMasterRepository.findAll().size();
        // Create the ContactMaster
        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isCreated());

        // Validate the ContactMaster in the database
        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeCreate + 1);
        ContactMaster testContactMaster = contactMasterList.get(contactMasterList.size() - 1);
        assertThat(testContactMaster.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testContactMaster.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testContactMaster.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testContactMaster.getSalutation()).isEqualTo(DEFAULT_SALUTATION);
        assertThat(testContactMaster.getAddress1()).isEqualTo(DEFAULT_ADDRESS_1);
        assertThat(testContactMaster.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testContactMaster.getAddress3()).isEqualTo(DEFAULT_ADDRESS_3);
        assertThat(testContactMaster.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testContactMaster.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testContactMaster.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testContactMaster.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testContactMaster.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testContactMaster.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testContactMaster.getPhoneNum()).isEqualTo(DEFAULT_PHONE_NUM);
        assertThat(testContactMaster.getFaxNum()).isEqualTo(DEFAULT_FAX_NUM);
        assertThat(testContactMaster.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testContactMaster.getMobileNum()).isEqualTo(DEFAULT_MOBILE_NUM);
        assertThat(testContactMaster.getRecordId()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testContactMaster.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testContactMaster.getKeyPerson()).isEqualTo(DEFAULT_KEY_PERSON);
        assertThat(testContactMaster.getContactId()).isEqualTo(DEFAULT_CONTACT_ID);
        assertThat(testContactMaster.getLevelImportance()).isEqualTo(DEFAULT_LEVEL_IMPORTANCE);
        assertThat(testContactMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testContactMaster.getRemoveReason()).isEqualTo(DEFAULT_REMOVE_REASON);
        assertThat(testContactMaster.getCityId()).isEqualTo(DEFAULT_CITY_ID);
        assertThat(testContactMaster.getoFlag()).isEqualTo(DEFAULT_O_FLAG);
        assertThat(testContactMaster.getOptOut()).isEqualTo(DEFAULT_OPT_OUT);
        assertThat(testContactMaster.getOfaContactId()).isEqualTo(DEFAULT_OFA_CONTACT_ID);
        assertThat(testContactMaster.getOptOutDate()).isEqualTo(DEFAULT_OPT_OUT_DATE);
        assertThat(testContactMaster.getFaxOptout()).isEqualTo(DEFAULT_FAX_OPTOUT);
        assertThat(testContactMaster.getDonotcall()).isEqualTo(DEFAULT_DONOTCALL);
        assertThat(testContactMaster.getDnsendDirectMail()).isEqualTo(DEFAULT_DNSEND_DIRECT_MAIL);
        assertThat(testContactMaster.getDnshareOutsideMhp()).isEqualTo(DEFAULT_DNSHARE_OUTSIDE_MHP);
        assertThat(testContactMaster.getOutsideOfContact()).isEqualTo(DEFAULT_OUTSIDE_OF_CONTACT);
        assertThat(testContactMaster.getDnshareOutsideSnp()).isEqualTo(DEFAULT_DNSHARE_OUTSIDE_SNP);
        assertThat(testContactMaster.getEmailOptout()).isEqualTo(DEFAULT_EMAIL_OPTOUT);
        assertThat(testContactMaster.getSmsOptout()).isEqualTo(DEFAULT_SMS_OPTOUT);
        assertThat(testContactMaster.getIsSezContactFlag()).isEqualTo(DEFAULT_IS_SEZ_CONTACT_FLAG);
        assertThat(testContactMaster.getIsExemptContactFlag()).isEqualTo(DEFAULT_IS_EXEMPT_CONTACT_FLAG);
        assertThat(testContactMaster.getGstNotApplicable()).isEqualTo(DEFAULT_GST_NOT_APPLICABLE);
        assertThat(testContactMaster.getBillingContact()).isEqualTo(DEFAULT_BILLING_CONTACT);
        assertThat(testContactMaster.getGstPartyType()).isEqualTo(DEFAULT_GST_PARTY_TYPE);
        assertThat(testContactMaster.getTan()).isEqualTo(DEFAULT_TAN);
        assertThat(testContactMaster.getGstNumber()).isEqualTo(DEFAULT_GST_NUMBER);
        assertThat(testContactMaster.getTdsNumber()).isEqualTo(DEFAULT_TDS_NUMBER);
        assertThat(testContactMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testContactMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testContactMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testContactMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testContactMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createContactMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactMasterRepository.findAll().size();

        // Create the ContactMaster with an existing ID
        contactMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMaster in the database
        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterRepository.findAll().size();
        // set the field null
        contactMaster.setLastName(null);

        // Create the ContactMaster, which fails.


        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSalutationIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterRepository.findAll().size();
        // set the field null
        contactMaster.setSalutation(null);

        // Create the ContactMaster, which fails.


        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAddress1IsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterRepository.findAll().size();
        // set the field null
        contactMaster.setAddress1(null);

        // Create the ContactMaster, which fails.


        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDesignationIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterRepository.findAll().size();
        // set the field null
        contactMaster.setDesignation(null);

        // Create the ContactMaster, which fails.


        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRecordIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterRepository.findAll().size();
        // set the field null
        contactMaster.setRecordId(null);

        // Create the ContactMaster, which fails.


        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMasterRepository.findAll().size();
        // set the field null
        contactMaster.setContactId(null);

        // Create the ContactMaster, which fails.


        restContactMasterMockMvc.perform(post("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactMasters() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList
        restContactMasterMockMvc.perform(get("/api/contact-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].salutation").value(hasItem(DEFAULT_SALUTATION)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].phoneNum").value(hasItem(DEFAULT_PHONE_NUM)))
            .andExpect(jsonPath("$.[*].faxNum").value(hasItem(DEFAULT_FAX_NUM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].mobileNum").value(hasItem(DEFAULT_MOBILE_NUM)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].keyPerson").value(hasItem(DEFAULT_KEY_PERSON)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].levelImportance").value(hasItem(DEFAULT_LEVEL_IMPORTANCE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].removeReason").value(hasItem(DEFAULT_REMOVE_REASON)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].oFlag").value(hasItem(DEFAULT_O_FLAG)))
            .andExpect(jsonPath("$.[*].optOut").value(hasItem(DEFAULT_OPT_OUT)))
            .andExpect(jsonPath("$.[*].ofaContactId").value(hasItem(DEFAULT_OFA_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].optOutDate").value(hasItem(DEFAULT_OPT_OUT_DATE.toString())))
            .andExpect(jsonPath("$.[*].faxOptout").value(hasItem(DEFAULT_FAX_OPTOUT)))
            .andExpect(jsonPath("$.[*].donotcall").value(hasItem(DEFAULT_DONOTCALL)))
            .andExpect(jsonPath("$.[*].dnsendDirectMail").value(hasItem(DEFAULT_DNSEND_DIRECT_MAIL)))
            .andExpect(jsonPath("$.[*].dnshareOutsideMhp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_MHP)))
            .andExpect(jsonPath("$.[*].outsideOfContact").value(hasItem(DEFAULT_OUTSIDE_OF_CONTACT)))
            .andExpect(jsonPath("$.[*].dnshareOutsideSnp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_SNP)))
            .andExpect(jsonPath("$.[*].emailOptout").value(hasItem(DEFAULT_EMAIL_OPTOUT)))
            .andExpect(jsonPath("$.[*].smsOptout").value(hasItem(DEFAULT_SMS_OPTOUT)))
            .andExpect(jsonPath("$.[*].isSezContactFlag").value(hasItem(DEFAULT_IS_SEZ_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].isExemptContactFlag").value(hasItem(DEFAULT_IS_EXEMPT_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].gstNotApplicable").value(hasItem(DEFAULT_GST_NOT_APPLICABLE)))
            .andExpect(jsonPath("$.[*].billingContact").value(hasItem(DEFAULT_BILLING_CONTACT)))
            .andExpect(jsonPath("$.[*].gstPartyType").value(hasItem(DEFAULT_GST_PARTY_TYPE)))
            .andExpect(jsonPath("$.[*].tan").value(hasItem(DEFAULT_TAN)))
            .andExpect(jsonPath("$.[*].gstNumber").value(hasItem(DEFAULT_GST_NUMBER)))
            .andExpect(jsonPath("$.[*].tdsNumber").value(hasItem(DEFAULT_TDS_NUMBER)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getContactMaster() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get the contactMaster
        restContactMasterMockMvc.perform(get("/api/contact-masters/{id}", contactMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactMaster.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME))
            .andExpect(jsonPath("$.salutation").value(DEFAULT_SALUTATION))
            .andExpect(jsonPath("$.address1").value(DEFAULT_ADDRESS_1))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2))
            .andExpect(jsonPath("$.address3").value(DEFAULT_ADDRESS_3))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN))
            .andExpect(jsonPath("$.phoneNum").value(DEFAULT_PHONE_NUM))
            .andExpect(jsonPath("$.faxNum").value(DEFAULT_FAX_NUM))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.mobileNum").value(DEFAULT_MOBILE_NUM))
            .andExpect(jsonPath("$.recordId").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.keyPerson").value(DEFAULT_KEY_PERSON))
            .andExpect(jsonPath("$.contactId").value(DEFAULT_CONTACT_ID))
            .andExpect(jsonPath("$.levelImportance").value(DEFAULT_LEVEL_IMPORTANCE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.removeReason").value(DEFAULT_REMOVE_REASON))
            .andExpect(jsonPath("$.cityId").value(DEFAULT_CITY_ID))
            .andExpect(jsonPath("$.oFlag").value(DEFAULT_O_FLAG))
            .andExpect(jsonPath("$.optOut").value(DEFAULT_OPT_OUT))
            .andExpect(jsonPath("$.ofaContactId").value(DEFAULT_OFA_CONTACT_ID))
            .andExpect(jsonPath("$.optOutDate").value(DEFAULT_OPT_OUT_DATE.toString()))
            .andExpect(jsonPath("$.faxOptout").value(DEFAULT_FAX_OPTOUT))
            .andExpect(jsonPath("$.donotcall").value(DEFAULT_DONOTCALL))
            .andExpect(jsonPath("$.dnsendDirectMail").value(DEFAULT_DNSEND_DIRECT_MAIL))
            .andExpect(jsonPath("$.dnshareOutsideMhp").value(DEFAULT_DNSHARE_OUTSIDE_MHP))
            .andExpect(jsonPath("$.outsideOfContact").value(DEFAULT_OUTSIDE_OF_CONTACT))
            .andExpect(jsonPath("$.dnshareOutsideSnp").value(DEFAULT_DNSHARE_OUTSIDE_SNP))
            .andExpect(jsonPath("$.emailOptout").value(DEFAULT_EMAIL_OPTOUT))
            .andExpect(jsonPath("$.smsOptout").value(DEFAULT_SMS_OPTOUT))
            .andExpect(jsonPath("$.isSezContactFlag").value(DEFAULT_IS_SEZ_CONTACT_FLAG))
            .andExpect(jsonPath("$.isExemptContactFlag").value(DEFAULT_IS_EXEMPT_CONTACT_FLAG))
            .andExpect(jsonPath("$.gstNotApplicable").value(DEFAULT_GST_NOT_APPLICABLE))
            .andExpect(jsonPath("$.billingContact").value(DEFAULT_BILLING_CONTACT))
            .andExpect(jsonPath("$.gstPartyType").value(DEFAULT_GST_PARTY_TYPE))
            .andExpect(jsonPath("$.tan").value(DEFAULT_TAN))
            .andExpect(jsonPath("$.gstNumber").value(DEFAULT_GST_NUMBER))
            .andExpect(jsonPath("$.tdsNumber").value(DEFAULT_TDS_NUMBER))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getContactMastersByIdFiltering() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        Long id = contactMaster.getId();

        defaultContactMasterShouldBeFound("id.equals=" + id);
        defaultContactMasterShouldNotBeFound("id.notEquals=" + id);

        defaultContactMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultContactMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactMastersByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where firstName equals to DEFAULT_FIRST_NAME
        defaultContactMasterShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterList where firstName equals to UPDATED_FIRST_NAME
        defaultContactMasterShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFirstNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where firstName not equals to DEFAULT_FIRST_NAME
        defaultContactMasterShouldNotBeFound("firstName.notEquals=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterList where firstName not equals to UPDATED_FIRST_NAME
        defaultContactMasterShouldBeFound("firstName.notEquals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultContactMasterShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the contactMasterList where firstName equals to UPDATED_FIRST_NAME
        defaultContactMasterShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where firstName is not null
        defaultContactMasterShouldBeFound("firstName.specified=true");

        // Get all the contactMasterList where firstName is null
        defaultContactMasterShouldNotBeFound("firstName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByFirstNameContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where firstName contains DEFAULT_FIRST_NAME
        defaultContactMasterShouldBeFound("firstName.contains=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterList where firstName contains UPDATED_FIRST_NAME
        defaultContactMasterShouldNotBeFound("firstName.contains=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFirstNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where firstName does not contain DEFAULT_FIRST_NAME
        defaultContactMasterShouldNotBeFound("firstName.doesNotContain=" + DEFAULT_FIRST_NAME);

        // Get all the contactMasterList where firstName does not contain UPDATED_FIRST_NAME
        defaultContactMasterShouldBeFound("firstName.doesNotContain=" + UPDATED_FIRST_NAME);
    }


    @Test
    @Transactional
    public void getAllContactMastersByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastName equals to DEFAULT_LAST_NAME
        defaultContactMasterShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterList where lastName equals to UPDATED_LAST_NAME
        defaultContactMasterShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastName not equals to DEFAULT_LAST_NAME
        defaultContactMasterShouldNotBeFound("lastName.notEquals=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterList where lastName not equals to UPDATED_LAST_NAME
        defaultContactMasterShouldBeFound("lastName.notEquals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultContactMasterShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the contactMasterList where lastName equals to UPDATED_LAST_NAME
        defaultContactMasterShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastName is not null
        defaultContactMasterShouldBeFound("lastName.specified=true");

        // Get all the contactMasterList where lastName is null
        defaultContactMasterShouldNotBeFound("lastName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByLastNameContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastName contains DEFAULT_LAST_NAME
        defaultContactMasterShouldBeFound("lastName.contains=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterList where lastName contains UPDATED_LAST_NAME
        defaultContactMasterShouldNotBeFound("lastName.contains=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastName does not contain DEFAULT_LAST_NAME
        defaultContactMasterShouldNotBeFound("lastName.doesNotContain=" + DEFAULT_LAST_NAME);

        // Get all the contactMasterList where lastName does not contain UPDATED_LAST_NAME
        defaultContactMasterShouldBeFound("lastName.doesNotContain=" + UPDATED_LAST_NAME);
    }


    @Test
    @Transactional
    public void getAllContactMastersByMiddleNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where middleName equals to DEFAULT_MIDDLE_NAME
        defaultContactMasterShouldBeFound("middleName.equals=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterList where middleName equals to UPDATED_MIDDLE_NAME
        defaultContactMasterShouldNotBeFound("middleName.equals=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMiddleNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where middleName not equals to DEFAULT_MIDDLE_NAME
        defaultContactMasterShouldNotBeFound("middleName.notEquals=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterList where middleName not equals to UPDATED_MIDDLE_NAME
        defaultContactMasterShouldBeFound("middleName.notEquals=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMiddleNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where middleName in DEFAULT_MIDDLE_NAME or UPDATED_MIDDLE_NAME
        defaultContactMasterShouldBeFound("middleName.in=" + DEFAULT_MIDDLE_NAME + "," + UPDATED_MIDDLE_NAME);

        // Get all the contactMasterList where middleName equals to UPDATED_MIDDLE_NAME
        defaultContactMasterShouldNotBeFound("middleName.in=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMiddleNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where middleName is not null
        defaultContactMasterShouldBeFound("middleName.specified=true");

        // Get all the contactMasterList where middleName is null
        defaultContactMasterShouldNotBeFound("middleName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByMiddleNameContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where middleName contains DEFAULT_MIDDLE_NAME
        defaultContactMasterShouldBeFound("middleName.contains=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterList where middleName contains UPDATED_MIDDLE_NAME
        defaultContactMasterShouldNotBeFound("middleName.contains=" + UPDATED_MIDDLE_NAME);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMiddleNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where middleName does not contain DEFAULT_MIDDLE_NAME
        defaultContactMasterShouldNotBeFound("middleName.doesNotContain=" + DEFAULT_MIDDLE_NAME);

        // Get all the contactMasterList where middleName does not contain UPDATED_MIDDLE_NAME
        defaultContactMasterShouldBeFound("middleName.doesNotContain=" + UPDATED_MIDDLE_NAME);
    }


    @Test
    @Transactional
    public void getAllContactMastersBySalutationIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where salutation equals to DEFAULT_SALUTATION
        defaultContactMasterShouldBeFound("salutation.equals=" + DEFAULT_SALUTATION);

        // Get all the contactMasterList where salutation equals to UPDATED_SALUTATION
        defaultContactMasterShouldNotBeFound("salutation.equals=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySalutationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where salutation not equals to DEFAULT_SALUTATION
        defaultContactMasterShouldNotBeFound("salutation.notEquals=" + DEFAULT_SALUTATION);

        // Get all the contactMasterList where salutation not equals to UPDATED_SALUTATION
        defaultContactMasterShouldBeFound("salutation.notEquals=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySalutationIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where salutation in DEFAULT_SALUTATION or UPDATED_SALUTATION
        defaultContactMasterShouldBeFound("salutation.in=" + DEFAULT_SALUTATION + "," + UPDATED_SALUTATION);

        // Get all the contactMasterList where salutation equals to UPDATED_SALUTATION
        defaultContactMasterShouldNotBeFound("salutation.in=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySalutationIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where salutation is not null
        defaultContactMasterShouldBeFound("salutation.specified=true");

        // Get all the contactMasterList where salutation is null
        defaultContactMasterShouldNotBeFound("salutation.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersBySalutationContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where salutation contains DEFAULT_SALUTATION
        defaultContactMasterShouldBeFound("salutation.contains=" + DEFAULT_SALUTATION);

        // Get all the contactMasterList where salutation contains UPDATED_SALUTATION
        defaultContactMasterShouldNotBeFound("salutation.contains=" + UPDATED_SALUTATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySalutationNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where salutation does not contain DEFAULT_SALUTATION
        defaultContactMasterShouldNotBeFound("salutation.doesNotContain=" + DEFAULT_SALUTATION);

        // Get all the contactMasterList where salutation does not contain UPDATED_SALUTATION
        defaultContactMasterShouldBeFound("salutation.doesNotContain=" + UPDATED_SALUTATION);
    }


    @Test
    @Transactional
    public void getAllContactMastersByAddress1IsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address1 equals to DEFAULT_ADDRESS_1
        defaultContactMasterShouldBeFound("address1.equals=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterList where address1 equals to UPDATED_ADDRESS_1
        defaultContactMasterShouldNotBeFound("address1.equals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address1 not equals to DEFAULT_ADDRESS_1
        defaultContactMasterShouldNotBeFound("address1.notEquals=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterList where address1 not equals to UPDATED_ADDRESS_1
        defaultContactMasterShouldBeFound("address1.notEquals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress1IsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address1 in DEFAULT_ADDRESS_1 or UPDATED_ADDRESS_1
        defaultContactMasterShouldBeFound("address1.in=" + DEFAULT_ADDRESS_1 + "," + UPDATED_ADDRESS_1);

        // Get all the contactMasterList where address1 equals to UPDATED_ADDRESS_1
        defaultContactMasterShouldNotBeFound("address1.in=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress1IsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address1 is not null
        defaultContactMasterShouldBeFound("address1.specified=true");

        // Get all the contactMasterList where address1 is null
        defaultContactMasterShouldNotBeFound("address1.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByAddress1ContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address1 contains DEFAULT_ADDRESS_1
        defaultContactMasterShouldBeFound("address1.contains=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterList where address1 contains UPDATED_ADDRESS_1
        defaultContactMasterShouldNotBeFound("address1.contains=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress1NotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address1 does not contain DEFAULT_ADDRESS_1
        defaultContactMasterShouldNotBeFound("address1.doesNotContain=" + DEFAULT_ADDRESS_1);

        // Get all the contactMasterList where address1 does not contain UPDATED_ADDRESS_1
        defaultContactMasterShouldBeFound("address1.doesNotContain=" + UPDATED_ADDRESS_1);
    }


    @Test
    @Transactional
    public void getAllContactMastersByAddress2IsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address2 equals to DEFAULT_ADDRESS_2
        defaultContactMasterShouldBeFound("address2.equals=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterList where address2 equals to UPDATED_ADDRESS_2
        defaultContactMasterShouldNotBeFound("address2.equals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address2 not equals to DEFAULT_ADDRESS_2
        defaultContactMasterShouldNotBeFound("address2.notEquals=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterList where address2 not equals to UPDATED_ADDRESS_2
        defaultContactMasterShouldBeFound("address2.notEquals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress2IsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address2 in DEFAULT_ADDRESS_2 or UPDATED_ADDRESS_2
        defaultContactMasterShouldBeFound("address2.in=" + DEFAULT_ADDRESS_2 + "," + UPDATED_ADDRESS_2);

        // Get all the contactMasterList where address2 equals to UPDATED_ADDRESS_2
        defaultContactMasterShouldNotBeFound("address2.in=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress2IsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address2 is not null
        defaultContactMasterShouldBeFound("address2.specified=true");

        // Get all the contactMasterList where address2 is null
        defaultContactMasterShouldNotBeFound("address2.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByAddress2ContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address2 contains DEFAULT_ADDRESS_2
        defaultContactMasterShouldBeFound("address2.contains=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterList where address2 contains UPDATED_ADDRESS_2
        defaultContactMasterShouldNotBeFound("address2.contains=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress2NotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address2 does not contain DEFAULT_ADDRESS_2
        defaultContactMasterShouldNotBeFound("address2.doesNotContain=" + DEFAULT_ADDRESS_2);

        // Get all the contactMasterList where address2 does not contain UPDATED_ADDRESS_2
        defaultContactMasterShouldBeFound("address2.doesNotContain=" + UPDATED_ADDRESS_2);
    }


    @Test
    @Transactional
    public void getAllContactMastersByAddress3IsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address3 equals to DEFAULT_ADDRESS_3
        defaultContactMasterShouldBeFound("address3.equals=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterList where address3 equals to UPDATED_ADDRESS_3
        defaultContactMasterShouldNotBeFound("address3.equals=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address3 not equals to DEFAULT_ADDRESS_3
        defaultContactMasterShouldNotBeFound("address3.notEquals=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterList where address3 not equals to UPDATED_ADDRESS_3
        defaultContactMasterShouldBeFound("address3.notEquals=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress3IsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address3 in DEFAULT_ADDRESS_3 or UPDATED_ADDRESS_3
        defaultContactMasterShouldBeFound("address3.in=" + DEFAULT_ADDRESS_3 + "," + UPDATED_ADDRESS_3);

        // Get all the contactMasterList where address3 equals to UPDATED_ADDRESS_3
        defaultContactMasterShouldNotBeFound("address3.in=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress3IsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address3 is not null
        defaultContactMasterShouldBeFound("address3.specified=true");

        // Get all the contactMasterList where address3 is null
        defaultContactMasterShouldNotBeFound("address3.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByAddress3ContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address3 contains DEFAULT_ADDRESS_3
        defaultContactMasterShouldBeFound("address3.contains=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterList where address3 contains UPDATED_ADDRESS_3
        defaultContactMasterShouldNotBeFound("address3.contains=" + UPDATED_ADDRESS_3);
    }

    @Test
    @Transactional
    public void getAllContactMastersByAddress3NotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where address3 does not contain DEFAULT_ADDRESS_3
        defaultContactMasterShouldNotBeFound("address3.doesNotContain=" + DEFAULT_ADDRESS_3);

        // Get all the contactMasterList where address3 does not contain UPDATED_ADDRESS_3
        defaultContactMasterShouldBeFound("address3.doesNotContain=" + UPDATED_ADDRESS_3);
    }


    @Test
    @Transactional
    public void getAllContactMastersByDesignationIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where designation equals to DEFAULT_DESIGNATION
        defaultContactMasterShouldBeFound("designation.equals=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterList where designation equals to UPDATED_DESIGNATION
        defaultContactMasterShouldNotBeFound("designation.equals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDesignationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where designation not equals to DEFAULT_DESIGNATION
        defaultContactMasterShouldNotBeFound("designation.notEquals=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterList where designation not equals to UPDATED_DESIGNATION
        defaultContactMasterShouldBeFound("designation.notEquals=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDesignationIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where designation in DEFAULT_DESIGNATION or UPDATED_DESIGNATION
        defaultContactMasterShouldBeFound("designation.in=" + DEFAULT_DESIGNATION + "," + UPDATED_DESIGNATION);

        // Get all the contactMasterList where designation equals to UPDATED_DESIGNATION
        defaultContactMasterShouldNotBeFound("designation.in=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDesignationIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where designation is not null
        defaultContactMasterShouldBeFound("designation.specified=true");

        // Get all the contactMasterList where designation is null
        defaultContactMasterShouldNotBeFound("designation.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByDesignationContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where designation contains DEFAULT_DESIGNATION
        defaultContactMasterShouldBeFound("designation.contains=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterList where designation contains UPDATED_DESIGNATION
        defaultContactMasterShouldNotBeFound("designation.contains=" + UPDATED_DESIGNATION);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDesignationNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where designation does not contain DEFAULT_DESIGNATION
        defaultContactMasterShouldNotBeFound("designation.doesNotContain=" + DEFAULT_DESIGNATION);

        // Get all the contactMasterList where designation does not contain UPDATED_DESIGNATION
        defaultContactMasterShouldBeFound("designation.doesNotContain=" + UPDATED_DESIGNATION);
    }


    @Test
    @Transactional
    public void getAllContactMastersByDepartmentIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where department equals to DEFAULT_DEPARTMENT
        defaultContactMasterShouldBeFound("department.equals=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterList where department equals to UPDATED_DEPARTMENT
        defaultContactMasterShouldNotBeFound("department.equals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDepartmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where department not equals to DEFAULT_DEPARTMENT
        defaultContactMasterShouldNotBeFound("department.notEquals=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterList where department not equals to UPDATED_DEPARTMENT
        defaultContactMasterShouldBeFound("department.notEquals=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDepartmentIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where department in DEFAULT_DEPARTMENT or UPDATED_DEPARTMENT
        defaultContactMasterShouldBeFound("department.in=" + DEFAULT_DEPARTMENT + "," + UPDATED_DEPARTMENT);

        // Get all the contactMasterList where department equals to UPDATED_DEPARTMENT
        defaultContactMasterShouldNotBeFound("department.in=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDepartmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where department is not null
        defaultContactMasterShouldBeFound("department.specified=true");

        // Get all the contactMasterList where department is null
        defaultContactMasterShouldNotBeFound("department.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByDepartmentContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where department contains DEFAULT_DEPARTMENT
        defaultContactMasterShouldBeFound("department.contains=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterList where department contains UPDATED_DEPARTMENT
        defaultContactMasterShouldNotBeFound("department.contains=" + UPDATED_DEPARTMENT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDepartmentNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where department does not contain DEFAULT_DEPARTMENT
        defaultContactMasterShouldNotBeFound("department.doesNotContain=" + DEFAULT_DEPARTMENT);

        // Get all the contactMasterList where department does not contain UPDATED_DEPARTMENT
        defaultContactMasterShouldBeFound("department.doesNotContain=" + UPDATED_DEPARTMENT);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCityIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where city equals to DEFAULT_CITY
        defaultContactMasterShouldBeFound("city.equals=" + DEFAULT_CITY);

        // Get all the contactMasterList where city equals to UPDATED_CITY
        defaultContactMasterShouldNotBeFound("city.equals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where city not equals to DEFAULT_CITY
        defaultContactMasterShouldNotBeFound("city.notEquals=" + DEFAULT_CITY);

        // Get all the contactMasterList where city not equals to UPDATED_CITY
        defaultContactMasterShouldBeFound("city.notEquals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where city in DEFAULT_CITY or UPDATED_CITY
        defaultContactMasterShouldBeFound("city.in=" + DEFAULT_CITY + "," + UPDATED_CITY);

        // Get all the contactMasterList where city equals to UPDATED_CITY
        defaultContactMasterShouldNotBeFound("city.in=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where city is not null
        defaultContactMasterShouldBeFound("city.specified=true");

        // Get all the contactMasterList where city is null
        defaultContactMasterShouldNotBeFound("city.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByCityContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where city contains DEFAULT_CITY
        defaultContactMasterShouldBeFound("city.contains=" + DEFAULT_CITY);

        // Get all the contactMasterList where city contains UPDATED_CITY
        defaultContactMasterShouldNotBeFound("city.contains=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where city does not contain DEFAULT_CITY
        defaultContactMasterShouldNotBeFound("city.doesNotContain=" + DEFAULT_CITY);

        // Get all the contactMasterList where city does not contain UPDATED_CITY
        defaultContactMasterShouldBeFound("city.doesNotContain=" + UPDATED_CITY);
    }


    @Test
    @Transactional
    public void getAllContactMastersByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where state equals to DEFAULT_STATE
        defaultContactMasterShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the contactMasterList where state equals to UPDATED_STATE
        defaultContactMasterShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where state not equals to DEFAULT_STATE
        defaultContactMasterShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the contactMasterList where state not equals to UPDATED_STATE
        defaultContactMasterShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where state in DEFAULT_STATE or UPDATED_STATE
        defaultContactMasterShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the contactMasterList where state equals to UPDATED_STATE
        defaultContactMasterShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where state is not null
        defaultContactMasterShouldBeFound("state.specified=true");

        // Get all the contactMasterList where state is null
        defaultContactMasterShouldNotBeFound("state.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByStateContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where state contains DEFAULT_STATE
        defaultContactMasterShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the contactMasterList where state contains UPDATED_STATE
        defaultContactMasterShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStateNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where state does not contain DEFAULT_STATE
        defaultContactMasterShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the contactMasterList where state does not contain UPDATED_STATE
        defaultContactMasterShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where country equals to DEFAULT_COUNTRY
        defaultContactMasterShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the contactMasterList where country equals to UPDATED_COUNTRY
        defaultContactMasterShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCountryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where country not equals to DEFAULT_COUNTRY
        defaultContactMasterShouldNotBeFound("country.notEquals=" + DEFAULT_COUNTRY);

        // Get all the contactMasterList where country not equals to UPDATED_COUNTRY
        defaultContactMasterShouldBeFound("country.notEquals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultContactMasterShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the contactMasterList where country equals to UPDATED_COUNTRY
        defaultContactMasterShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where country is not null
        defaultContactMasterShouldBeFound("country.specified=true");

        // Get all the contactMasterList where country is null
        defaultContactMasterShouldNotBeFound("country.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByCountryContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where country contains DEFAULT_COUNTRY
        defaultContactMasterShouldBeFound("country.contains=" + DEFAULT_COUNTRY);

        // Get all the contactMasterList where country contains UPDATED_COUNTRY
        defaultContactMasterShouldNotBeFound("country.contains=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCountryNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where country does not contain DEFAULT_COUNTRY
        defaultContactMasterShouldNotBeFound("country.doesNotContain=" + DEFAULT_COUNTRY);

        // Get all the contactMasterList where country does not contain UPDATED_COUNTRY
        defaultContactMasterShouldBeFound("country.doesNotContain=" + UPDATED_COUNTRY);
    }


    @Test
    @Transactional
    public void getAllContactMastersByPinIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where pin equals to DEFAULT_PIN
        defaultContactMasterShouldBeFound("pin.equals=" + DEFAULT_PIN);

        // Get all the contactMasterList where pin equals to UPDATED_PIN
        defaultContactMasterShouldNotBeFound("pin.equals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where pin not equals to DEFAULT_PIN
        defaultContactMasterShouldNotBeFound("pin.notEquals=" + DEFAULT_PIN);

        // Get all the contactMasterList where pin not equals to UPDATED_PIN
        defaultContactMasterShouldBeFound("pin.notEquals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPinIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where pin in DEFAULT_PIN or UPDATED_PIN
        defaultContactMasterShouldBeFound("pin.in=" + DEFAULT_PIN + "," + UPDATED_PIN);

        // Get all the contactMasterList where pin equals to UPDATED_PIN
        defaultContactMasterShouldNotBeFound("pin.in=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPinIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where pin is not null
        defaultContactMasterShouldBeFound("pin.specified=true");

        // Get all the contactMasterList where pin is null
        defaultContactMasterShouldNotBeFound("pin.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByPinContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where pin contains DEFAULT_PIN
        defaultContactMasterShouldBeFound("pin.contains=" + DEFAULT_PIN);

        // Get all the contactMasterList where pin contains UPDATED_PIN
        defaultContactMasterShouldNotBeFound("pin.contains=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPinNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where pin does not contain DEFAULT_PIN
        defaultContactMasterShouldNotBeFound("pin.doesNotContain=" + DEFAULT_PIN);

        // Get all the contactMasterList where pin does not contain UPDATED_PIN
        defaultContactMasterShouldBeFound("pin.doesNotContain=" + UPDATED_PIN);
    }


    @Test
    @Transactional
    public void getAllContactMastersByPhoneNumIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where phoneNum equals to DEFAULT_PHONE_NUM
        defaultContactMasterShouldBeFound("phoneNum.equals=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterList where phoneNum equals to UPDATED_PHONE_NUM
        defaultContactMasterShouldNotBeFound("phoneNum.equals=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPhoneNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where phoneNum not equals to DEFAULT_PHONE_NUM
        defaultContactMasterShouldNotBeFound("phoneNum.notEquals=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterList where phoneNum not equals to UPDATED_PHONE_NUM
        defaultContactMasterShouldBeFound("phoneNum.notEquals=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPhoneNumIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where phoneNum in DEFAULT_PHONE_NUM or UPDATED_PHONE_NUM
        defaultContactMasterShouldBeFound("phoneNum.in=" + DEFAULT_PHONE_NUM + "," + UPDATED_PHONE_NUM);

        // Get all the contactMasterList where phoneNum equals to UPDATED_PHONE_NUM
        defaultContactMasterShouldNotBeFound("phoneNum.in=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPhoneNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where phoneNum is not null
        defaultContactMasterShouldBeFound("phoneNum.specified=true");

        // Get all the contactMasterList where phoneNum is null
        defaultContactMasterShouldNotBeFound("phoneNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByPhoneNumContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where phoneNum contains DEFAULT_PHONE_NUM
        defaultContactMasterShouldBeFound("phoneNum.contains=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterList where phoneNum contains UPDATED_PHONE_NUM
        defaultContactMasterShouldNotBeFound("phoneNum.contains=" + UPDATED_PHONE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByPhoneNumNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where phoneNum does not contain DEFAULT_PHONE_NUM
        defaultContactMasterShouldNotBeFound("phoneNum.doesNotContain=" + DEFAULT_PHONE_NUM);

        // Get all the contactMasterList where phoneNum does not contain UPDATED_PHONE_NUM
        defaultContactMasterShouldBeFound("phoneNum.doesNotContain=" + UPDATED_PHONE_NUM);
    }


    @Test
    @Transactional
    public void getAllContactMastersByFaxNumIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxNum equals to DEFAULT_FAX_NUM
        defaultContactMasterShouldBeFound("faxNum.equals=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterList where faxNum equals to UPDATED_FAX_NUM
        defaultContactMasterShouldNotBeFound("faxNum.equals=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxNum not equals to DEFAULT_FAX_NUM
        defaultContactMasterShouldNotBeFound("faxNum.notEquals=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterList where faxNum not equals to UPDATED_FAX_NUM
        defaultContactMasterShouldBeFound("faxNum.notEquals=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxNumIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxNum in DEFAULT_FAX_NUM or UPDATED_FAX_NUM
        defaultContactMasterShouldBeFound("faxNum.in=" + DEFAULT_FAX_NUM + "," + UPDATED_FAX_NUM);

        // Get all the contactMasterList where faxNum equals to UPDATED_FAX_NUM
        defaultContactMasterShouldNotBeFound("faxNum.in=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxNum is not null
        defaultContactMasterShouldBeFound("faxNum.specified=true");

        // Get all the contactMasterList where faxNum is null
        defaultContactMasterShouldNotBeFound("faxNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByFaxNumContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxNum contains DEFAULT_FAX_NUM
        defaultContactMasterShouldBeFound("faxNum.contains=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterList where faxNum contains UPDATED_FAX_NUM
        defaultContactMasterShouldNotBeFound("faxNum.contains=" + UPDATED_FAX_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxNumNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxNum does not contain DEFAULT_FAX_NUM
        defaultContactMasterShouldNotBeFound("faxNum.doesNotContain=" + DEFAULT_FAX_NUM);

        // Get all the contactMasterList where faxNum does not contain UPDATED_FAX_NUM
        defaultContactMasterShouldBeFound("faxNum.doesNotContain=" + UPDATED_FAX_NUM);
    }


    @Test
    @Transactional
    public void getAllContactMastersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where email equals to DEFAULT_EMAIL
        defaultContactMasterShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the contactMasterList where email equals to UPDATED_EMAIL
        defaultContactMasterShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where email not equals to DEFAULT_EMAIL
        defaultContactMasterShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the contactMasterList where email not equals to UPDATED_EMAIL
        defaultContactMasterShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultContactMasterShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the contactMasterList where email equals to UPDATED_EMAIL
        defaultContactMasterShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where email is not null
        defaultContactMasterShouldBeFound("email.specified=true");

        // Get all the contactMasterList where email is null
        defaultContactMasterShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByEmailContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where email contains DEFAULT_EMAIL
        defaultContactMasterShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the contactMasterList where email contains UPDATED_EMAIL
        defaultContactMasterShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where email does not contain DEFAULT_EMAIL
        defaultContactMasterShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the contactMasterList where email does not contain UPDATED_EMAIL
        defaultContactMasterShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllContactMastersByMobileNumIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where mobileNum equals to DEFAULT_MOBILE_NUM
        defaultContactMasterShouldBeFound("mobileNum.equals=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterList where mobileNum equals to UPDATED_MOBILE_NUM
        defaultContactMasterShouldNotBeFound("mobileNum.equals=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMobileNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where mobileNum not equals to DEFAULT_MOBILE_NUM
        defaultContactMasterShouldNotBeFound("mobileNum.notEquals=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterList where mobileNum not equals to UPDATED_MOBILE_NUM
        defaultContactMasterShouldBeFound("mobileNum.notEquals=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMobileNumIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where mobileNum in DEFAULT_MOBILE_NUM or UPDATED_MOBILE_NUM
        defaultContactMasterShouldBeFound("mobileNum.in=" + DEFAULT_MOBILE_NUM + "," + UPDATED_MOBILE_NUM);

        // Get all the contactMasterList where mobileNum equals to UPDATED_MOBILE_NUM
        defaultContactMasterShouldNotBeFound("mobileNum.in=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMobileNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where mobileNum is not null
        defaultContactMasterShouldBeFound("mobileNum.specified=true");

        // Get all the contactMasterList where mobileNum is null
        defaultContactMasterShouldNotBeFound("mobileNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByMobileNumContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where mobileNum contains DEFAULT_MOBILE_NUM
        defaultContactMasterShouldBeFound("mobileNum.contains=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterList where mobileNum contains UPDATED_MOBILE_NUM
        defaultContactMasterShouldNotBeFound("mobileNum.contains=" + UPDATED_MOBILE_NUM);
    }

    @Test
    @Transactional
    public void getAllContactMastersByMobileNumNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where mobileNum does not contain DEFAULT_MOBILE_NUM
        defaultContactMasterShouldNotBeFound("mobileNum.doesNotContain=" + DEFAULT_MOBILE_NUM);

        // Get all the contactMasterList where mobileNum does not contain UPDATED_MOBILE_NUM
        defaultContactMasterShouldBeFound("mobileNum.doesNotContain=" + UPDATED_MOBILE_NUM);
    }


    @Test
    @Transactional
    public void getAllContactMastersByRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where recordId equals to DEFAULT_RECORD_ID
        defaultContactMasterShouldBeFound("recordId.equals=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterList where recordId equals to UPDATED_RECORD_ID
        defaultContactMasterShouldNotBeFound("recordId.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where recordId not equals to DEFAULT_RECORD_ID
        defaultContactMasterShouldNotBeFound("recordId.notEquals=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterList where recordId not equals to UPDATED_RECORD_ID
        defaultContactMasterShouldBeFound("recordId.notEquals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where recordId in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultContactMasterShouldBeFound("recordId.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the contactMasterList where recordId equals to UPDATED_RECORD_ID
        defaultContactMasterShouldNotBeFound("recordId.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where recordId is not null
        defaultContactMasterShouldBeFound("recordId.specified=true");

        // Get all the contactMasterList where recordId is null
        defaultContactMasterShouldNotBeFound("recordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByRecordIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where recordId contains DEFAULT_RECORD_ID
        defaultContactMasterShouldBeFound("recordId.contains=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterList where recordId contains UPDATED_RECORD_ID
        defaultContactMasterShouldNotBeFound("recordId.contains=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where recordId does not contain DEFAULT_RECORD_ID
        defaultContactMasterShouldNotBeFound("recordId.doesNotContain=" + DEFAULT_RECORD_ID);

        // Get all the contactMasterList where recordId does not contain UPDATED_RECORD_ID
        defaultContactMasterShouldBeFound("recordId.doesNotContain=" + UPDATED_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where comments equals to DEFAULT_COMMENTS
        defaultContactMasterShouldBeFound("comments.equals=" + DEFAULT_COMMENTS);

        // Get all the contactMasterList where comments equals to UPDATED_COMMENTS
        defaultContactMasterShouldNotBeFound("comments.equals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where comments not equals to DEFAULT_COMMENTS
        defaultContactMasterShouldNotBeFound("comments.notEquals=" + DEFAULT_COMMENTS);

        // Get all the contactMasterList where comments not equals to UPDATED_COMMENTS
        defaultContactMasterShouldBeFound("comments.notEquals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where comments in DEFAULT_COMMENTS or UPDATED_COMMENTS
        defaultContactMasterShouldBeFound("comments.in=" + DEFAULT_COMMENTS + "," + UPDATED_COMMENTS);

        // Get all the contactMasterList where comments equals to UPDATED_COMMENTS
        defaultContactMasterShouldNotBeFound("comments.in=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where comments is not null
        defaultContactMasterShouldBeFound("comments.specified=true");

        // Get all the contactMasterList where comments is null
        defaultContactMasterShouldNotBeFound("comments.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByCommentsContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where comments contains DEFAULT_COMMENTS
        defaultContactMasterShouldBeFound("comments.contains=" + DEFAULT_COMMENTS);

        // Get all the contactMasterList where comments contains UPDATED_COMMENTS
        defaultContactMasterShouldNotBeFound("comments.contains=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where comments does not contain DEFAULT_COMMENTS
        defaultContactMasterShouldNotBeFound("comments.doesNotContain=" + DEFAULT_COMMENTS);

        // Get all the contactMasterList where comments does not contain UPDATED_COMMENTS
        defaultContactMasterShouldBeFound("comments.doesNotContain=" + UPDATED_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllContactMastersByKeyPersonIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where keyPerson equals to DEFAULT_KEY_PERSON
        defaultContactMasterShouldBeFound("keyPerson.equals=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterList where keyPerson equals to UPDATED_KEY_PERSON
        defaultContactMasterShouldNotBeFound("keyPerson.equals=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByKeyPersonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where keyPerson not equals to DEFAULT_KEY_PERSON
        defaultContactMasterShouldNotBeFound("keyPerson.notEquals=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterList where keyPerson not equals to UPDATED_KEY_PERSON
        defaultContactMasterShouldBeFound("keyPerson.notEquals=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByKeyPersonIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where keyPerson in DEFAULT_KEY_PERSON or UPDATED_KEY_PERSON
        defaultContactMasterShouldBeFound("keyPerson.in=" + DEFAULT_KEY_PERSON + "," + UPDATED_KEY_PERSON);

        // Get all the contactMasterList where keyPerson equals to UPDATED_KEY_PERSON
        defaultContactMasterShouldNotBeFound("keyPerson.in=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByKeyPersonIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where keyPerson is not null
        defaultContactMasterShouldBeFound("keyPerson.specified=true");

        // Get all the contactMasterList where keyPerson is null
        defaultContactMasterShouldNotBeFound("keyPerson.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByKeyPersonContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where keyPerson contains DEFAULT_KEY_PERSON
        defaultContactMasterShouldBeFound("keyPerson.contains=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterList where keyPerson contains UPDATED_KEY_PERSON
        defaultContactMasterShouldNotBeFound("keyPerson.contains=" + UPDATED_KEY_PERSON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByKeyPersonNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where keyPerson does not contain DEFAULT_KEY_PERSON
        defaultContactMasterShouldNotBeFound("keyPerson.doesNotContain=" + DEFAULT_KEY_PERSON);

        // Get all the contactMasterList where keyPerson does not contain UPDATED_KEY_PERSON
        defaultContactMasterShouldBeFound("keyPerson.doesNotContain=" + UPDATED_KEY_PERSON);
    }


    @Test
    @Transactional
    public void getAllContactMastersByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where contactId equals to DEFAULT_CONTACT_ID
        defaultContactMasterShouldBeFound("contactId.equals=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterList where contactId equals to UPDATED_CONTACT_ID
        defaultContactMasterShouldNotBeFound("contactId.equals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where contactId not equals to DEFAULT_CONTACT_ID
        defaultContactMasterShouldNotBeFound("contactId.notEquals=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterList where contactId not equals to UPDATED_CONTACT_ID
        defaultContactMasterShouldBeFound("contactId.notEquals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where contactId in DEFAULT_CONTACT_ID or UPDATED_CONTACT_ID
        defaultContactMasterShouldBeFound("contactId.in=" + DEFAULT_CONTACT_ID + "," + UPDATED_CONTACT_ID);

        // Get all the contactMasterList where contactId equals to UPDATED_CONTACT_ID
        defaultContactMasterShouldNotBeFound("contactId.in=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where contactId is not null
        defaultContactMasterShouldBeFound("contactId.specified=true");

        // Get all the contactMasterList where contactId is null
        defaultContactMasterShouldNotBeFound("contactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByContactIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where contactId contains DEFAULT_CONTACT_ID
        defaultContactMasterShouldBeFound("contactId.contains=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterList where contactId contains UPDATED_CONTACT_ID
        defaultContactMasterShouldNotBeFound("contactId.contains=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where contactId does not contain DEFAULT_CONTACT_ID
        defaultContactMasterShouldNotBeFound("contactId.doesNotContain=" + DEFAULT_CONTACT_ID);

        // Get all the contactMasterList where contactId does not contain UPDATED_CONTACT_ID
        defaultContactMasterShouldBeFound("contactId.doesNotContain=" + UPDATED_CONTACT_ID);
    }


    @Test
    @Transactional
    public void getAllContactMastersByLevelImportanceIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where levelImportance equals to DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterShouldBeFound("levelImportance.equals=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterList where levelImportance equals to UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterShouldNotBeFound("levelImportance.equals=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLevelImportanceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where levelImportance not equals to DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterShouldNotBeFound("levelImportance.notEquals=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterList where levelImportance not equals to UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterShouldBeFound("levelImportance.notEquals=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLevelImportanceIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where levelImportance in DEFAULT_LEVEL_IMPORTANCE or UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterShouldBeFound("levelImportance.in=" + DEFAULT_LEVEL_IMPORTANCE + "," + UPDATED_LEVEL_IMPORTANCE);

        // Get all the contactMasterList where levelImportance equals to UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterShouldNotBeFound("levelImportance.in=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLevelImportanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where levelImportance is not null
        defaultContactMasterShouldBeFound("levelImportance.specified=true");

        // Get all the contactMasterList where levelImportance is null
        defaultContactMasterShouldNotBeFound("levelImportance.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByLevelImportanceContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where levelImportance contains DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterShouldBeFound("levelImportance.contains=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterList where levelImportance contains UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterShouldNotBeFound("levelImportance.contains=" + UPDATED_LEVEL_IMPORTANCE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLevelImportanceNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where levelImportance does not contain DEFAULT_LEVEL_IMPORTANCE
        defaultContactMasterShouldNotBeFound("levelImportance.doesNotContain=" + DEFAULT_LEVEL_IMPORTANCE);

        // Get all the contactMasterList where levelImportance does not contain UPDATED_LEVEL_IMPORTANCE
        defaultContactMasterShouldBeFound("levelImportance.doesNotContain=" + UPDATED_LEVEL_IMPORTANCE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where status equals to DEFAULT_STATUS
        defaultContactMasterShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the contactMasterList where status equals to UPDATED_STATUS
        defaultContactMasterShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where status not equals to DEFAULT_STATUS
        defaultContactMasterShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the contactMasterList where status not equals to UPDATED_STATUS
        defaultContactMasterShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultContactMasterShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the contactMasterList where status equals to UPDATED_STATUS
        defaultContactMasterShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where status is not null
        defaultContactMasterShouldBeFound("status.specified=true");

        // Get all the contactMasterList where status is null
        defaultContactMasterShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByStatusContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where status contains DEFAULT_STATUS
        defaultContactMasterShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the contactMasterList where status contains UPDATED_STATUS
        defaultContactMasterShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactMastersByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where status does not contain DEFAULT_STATUS
        defaultContactMasterShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the contactMasterList where status does not contain UPDATED_STATUS
        defaultContactMasterShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllContactMastersByRemoveReasonIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where removeReason equals to DEFAULT_REMOVE_REASON
        defaultContactMasterShouldBeFound("removeReason.equals=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterList where removeReason equals to UPDATED_REMOVE_REASON
        defaultContactMasterShouldNotBeFound("removeReason.equals=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRemoveReasonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where removeReason not equals to DEFAULT_REMOVE_REASON
        defaultContactMasterShouldNotBeFound("removeReason.notEquals=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterList where removeReason not equals to UPDATED_REMOVE_REASON
        defaultContactMasterShouldBeFound("removeReason.notEquals=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRemoveReasonIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where removeReason in DEFAULT_REMOVE_REASON or UPDATED_REMOVE_REASON
        defaultContactMasterShouldBeFound("removeReason.in=" + DEFAULT_REMOVE_REASON + "," + UPDATED_REMOVE_REASON);

        // Get all the contactMasterList where removeReason equals to UPDATED_REMOVE_REASON
        defaultContactMasterShouldNotBeFound("removeReason.in=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRemoveReasonIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where removeReason is not null
        defaultContactMasterShouldBeFound("removeReason.specified=true");

        // Get all the contactMasterList where removeReason is null
        defaultContactMasterShouldNotBeFound("removeReason.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByRemoveReasonContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where removeReason contains DEFAULT_REMOVE_REASON
        defaultContactMasterShouldBeFound("removeReason.contains=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterList where removeReason contains UPDATED_REMOVE_REASON
        defaultContactMasterShouldNotBeFound("removeReason.contains=" + UPDATED_REMOVE_REASON);
    }

    @Test
    @Transactional
    public void getAllContactMastersByRemoveReasonNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where removeReason does not contain DEFAULT_REMOVE_REASON
        defaultContactMasterShouldNotBeFound("removeReason.doesNotContain=" + DEFAULT_REMOVE_REASON);

        // Get all the contactMasterList where removeReason does not contain UPDATED_REMOVE_REASON
        defaultContactMasterShouldBeFound("removeReason.doesNotContain=" + UPDATED_REMOVE_REASON);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where cityId equals to DEFAULT_CITY_ID
        defaultContactMasterShouldBeFound("cityId.equals=" + DEFAULT_CITY_ID);

        // Get all the contactMasterList where cityId equals to UPDATED_CITY_ID
        defaultContactMasterShouldNotBeFound("cityId.equals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where cityId not equals to DEFAULT_CITY_ID
        defaultContactMasterShouldNotBeFound("cityId.notEquals=" + DEFAULT_CITY_ID);

        // Get all the contactMasterList where cityId not equals to UPDATED_CITY_ID
        defaultContactMasterShouldBeFound("cityId.notEquals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where cityId in DEFAULT_CITY_ID or UPDATED_CITY_ID
        defaultContactMasterShouldBeFound("cityId.in=" + DEFAULT_CITY_ID + "," + UPDATED_CITY_ID);

        // Get all the contactMasterList where cityId equals to UPDATED_CITY_ID
        defaultContactMasterShouldNotBeFound("cityId.in=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where cityId is not null
        defaultContactMasterShouldBeFound("cityId.specified=true");

        // Get all the contactMasterList where cityId is null
        defaultContactMasterShouldNotBeFound("cityId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByCityIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where cityId contains DEFAULT_CITY_ID
        defaultContactMasterShouldBeFound("cityId.contains=" + DEFAULT_CITY_ID);

        // Get all the contactMasterList where cityId contains UPDATED_CITY_ID
        defaultContactMasterShouldNotBeFound("cityId.contains=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCityIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where cityId does not contain DEFAULT_CITY_ID
        defaultContactMasterShouldNotBeFound("cityId.doesNotContain=" + DEFAULT_CITY_ID);

        // Get all the contactMasterList where cityId does not contain UPDATED_CITY_ID
        defaultContactMasterShouldBeFound("cityId.doesNotContain=" + UPDATED_CITY_ID);
    }


    @Test
    @Transactional
    public void getAllContactMastersByoFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where oFlag equals to DEFAULT_O_FLAG
        defaultContactMasterShouldBeFound("oFlag.equals=" + DEFAULT_O_FLAG);

        // Get all the contactMasterList where oFlag equals to UPDATED_O_FLAG
        defaultContactMasterShouldNotBeFound("oFlag.equals=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByoFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where oFlag not equals to DEFAULT_O_FLAG
        defaultContactMasterShouldNotBeFound("oFlag.notEquals=" + DEFAULT_O_FLAG);

        // Get all the contactMasterList where oFlag not equals to UPDATED_O_FLAG
        defaultContactMasterShouldBeFound("oFlag.notEquals=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByoFlagIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where oFlag in DEFAULT_O_FLAG or UPDATED_O_FLAG
        defaultContactMasterShouldBeFound("oFlag.in=" + DEFAULT_O_FLAG + "," + UPDATED_O_FLAG);

        // Get all the contactMasterList where oFlag equals to UPDATED_O_FLAG
        defaultContactMasterShouldNotBeFound("oFlag.in=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByoFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where oFlag is not null
        defaultContactMasterShouldBeFound("oFlag.specified=true");

        // Get all the contactMasterList where oFlag is null
        defaultContactMasterShouldNotBeFound("oFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByoFlagContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where oFlag contains DEFAULT_O_FLAG
        defaultContactMasterShouldBeFound("oFlag.contains=" + DEFAULT_O_FLAG);

        // Get all the contactMasterList where oFlag contains UPDATED_O_FLAG
        defaultContactMasterShouldNotBeFound("oFlag.contains=" + UPDATED_O_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByoFlagNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where oFlag does not contain DEFAULT_O_FLAG
        defaultContactMasterShouldNotBeFound("oFlag.doesNotContain=" + DEFAULT_O_FLAG);

        // Get all the contactMasterList where oFlag does not contain UPDATED_O_FLAG
        defaultContactMasterShouldBeFound("oFlag.doesNotContain=" + UPDATED_O_FLAG);
    }


    @Test
    @Transactional
    public void getAllContactMastersByOptOutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOut equals to DEFAULT_OPT_OUT
        defaultContactMasterShouldBeFound("optOut.equals=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterList where optOut equals to UPDATED_OPT_OUT
        defaultContactMasterShouldNotBeFound("optOut.equals=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOut not equals to DEFAULT_OPT_OUT
        defaultContactMasterShouldNotBeFound("optOut.notEquals=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterList where optOut not equals to UPDATED_OPT_OUT
        defaultContactMasterShouldBeFound("optOut.notEquals=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOut in DEFAULT_OPT_OUT or UPDATED_OPT_OUT
        defaultContactMasterShouldBeFound("optOut.in=" + DEFAULT_OPT_OUT + "," + UPDATED_OPT_OUT);

        // Get all the contactMasterList where optOut equals to UPDATED_OPT_OUT
        defaultContactMasterShouldNotBeFound("optOut.in=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOut is not null
        defaultContactMasterShouldBeFound("optOut.specified=true");

        // Get all the contactMasterList where optOut is null
        defaultContactMasterShouldNotBeFound("optOut.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByOptOutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOut contains DEFAULT_OPT_OUT
        defaultContactMasterShouldBeFound("optOut.contains=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterList where optOut contains UPDATED_OPT_OUT
        defaultContactMasterShouldNotBeFound("optOut.contains=" + UPDATED_OPT_OUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOut does not contain DEFAULT_OPT_OUT
        defaultContactMasterShouldNotBeFound("optOut.doesNotContain=" + DEFAULT_OPT_OUT);

        // Get all the contactMasterList where optOut does not contain UPDATED_OPT_OUT
        defaultContactMasterShouldBeFound("optOut.doesNotContain=" + UPDATED_OPT_OUT);
    }


    @Test
    @Transactional
    public void getAllContactMastersByOfaContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where ofaContactId equals to DEFAULT_OFA_CONTACT_ID
        defaultContactMasterShouldBeFound("ofaContactId.equals=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterList where ofaContactId equals to UPDATED_OFA_CONTACT_ID
        defaultContactMasterShouldNotBeFound("ofaContactId.equals=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOfaContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where ofaContactId not equals to DEFAULT_OFA_CONTACT_ID
        defaultContactMasterShouldNotBeFound("ofaContactId.notEquals=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterList where ofaContactId not equals to UPDATED_OFA_CONTACT_ID
        defaultContactMasterShouldBeFound("ofaContactId.notEquals=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOfaContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where ofaContactId in DEFAULT_OFA_CONTACT_ID or UPDATED_OFA_CONTACT_ID
        defaultContactMasterShouldBeFound("ofaContactId.in=" + DEFAULT_OFA_CONTACT_ID + "," + UPDATED_OFA_CONTACT_ID);

        // Get all the contactMasterList where ofaContactId equals to UPDATED_OFA_CONTACT_ID
        defaultContactMasterShouldNotBeFound("ofaContactId.in=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOfaContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where ofaContactId is not null
        defaultContactMasterShouldBeFound("ofaContactId.specified=true");

        // Get all the contactMasterList where ofaContactId is null
        defaultContactMasterShouldNotBeFound("ofaContactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByOfaContactIdContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where ofaContactId contains DEFAULT_OFA_CONTACT_ID
        defaultContactMasterShouldBeFound("ofaContactId.contains=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterList where ofaContactId contains UPDATED_OFA_CONTACT_ID
        defaultContactMasterShouldNotBeFound("ofaContactId.contains=" + UPDATED_OFA_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOfaContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where ofaContactId does not contain DEFAULT_OFA_CONTACT_ID
        defaultContactMasterShouldNotBeFound("ofaContactId.doesNotContain=" + DEFAULT_OFA_CONTACT_ID);

        // Get all the contactMasterList where ofaContactId does not contain UPDATED_OFA_CONTACT_ID
        defaultContactMasterShouldBeFound("ofaContactId.doesNotContain=" + UPDATED_OFA_CONTACT_ID);
    }


    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate equals to DEFAULT_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.equals=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate equals to UPDATED_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.equals=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate not equals to DEFAULT_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.notEquals=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate not equals to UPDATED_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.notEquals=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate in DEFAULT_OPT_OUT_DATE or UPDATED_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.in=" + DEFAULT_OPT_OUT_DATE + "," + UPDATED_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate equals to UPDATED_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.in=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate is not null
        defaultContactMasterShouldBeFound("optOutDate.specified=true");

        // Get all the contactMasterList where optOutDate is null
        defaultContactMasterShouldNotBeFound("optOutDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate is greater than or equal to DEFAULT_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.greaterThanOrEqual=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate is greater than or equal to UPDATED_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.greaterThanOrEqual=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate is less than or equal to DEFAULT_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.lessThanOrEqual=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate is less than or equal to SMALLER_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.lessThanOrEqual=" + SMALLER_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate is less than DEFAULT_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.lessThan=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate is less than UPDATED_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.lessThan=" + UPDATED_OPT_OUT_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOptOutDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where optOutDate is greater than DEFAULT_OPT_OUT_DATE
        defaultContactMasterShouldNotBeFound("optOutDate.greaterThan=" + DEFAULT_OPT_OUT_DATE);

        // Get all the contactMasterList where optOutDate is greater than SMALLER_OPT_OUT_DATE
        defaultContactMasterShouldBeFound("optOutDate.greaterThan=" + SMALLER_OPT_OUT_DATE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByFaxOptoutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxOptout equals to DEFAULT_FAX_OPTOUT
        defaultContactMasterShouldBeFound("faxOptout.equals=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterList where faxOptout equals to UPDATED_FAX_OPTOUT
        defaultContactMasterShouldNotBeFound("faxOptout.equals=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxOptoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxOptout not equals to DEFAULT_FAX_OPTOUT
        defaultContactMasterShouldNotBeFound("faxOptout.notEquals=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterList where faxOptout not equals to UPDATED_FAX_OPTOUT
        defaultContactMasterShouldBeFound("faxOptout.notEquals=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxOptoutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxOptout in DEFAULT_FAX_OPTOUT or UPDATED_FAX_OPTOUT
        defaultContactMasterShouldBeFound("faxOptout.in=" + DEFAULT_FAX_OPTOUT + "," + UPDATED_FAX_OPTOUT);

        // Get all the contactMasterList where faxOptout equals to UPDATED_FAX_OPTOUT
        defaultContactMasterShouldNotBeFound("faxOptout.in=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxOptoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxOptout is not null
        defaultContactMasterShouldBeFound("faxOptout.specified=true");

        // Get all the contactMasterList where faxOptout is null
        defaultContactMasterShouldNotBeFound("faxOptout.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByFaxOptoutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxOptout contains DEFAULT_FAX_OPTOUT
        defaultContactMasterShouldBeFound("faxOptout.contains=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterList where faxOptout contains UPDATED_FAX_OPTOUT
        defaultContactMasterShouldNotBeFound("faxOptout.contains=" + UPDATED_FAX_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByFaxOptoutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where faxOptout does not contain DEFAULT_FAX_OPTOUT
        defaultContactMasterShouldNotBeFound("faxOptout.doesNotContain=" + DEFAULT_FAX_OPTOUT);

        // Get all the contactMasterList where faxOptout does not contain UPDATED_FAX_OPTOUT
        defaultContactMasterShouldBeFound("faxOptout.doesNotContain=" + UPDATED_FAX_OPTOUT);
    }


    @Test
    @Transactional
    public void getAllContactMastersByDonotcallIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where donotcall equals to DEFAULT_DONOTCALL
        defaultContactMasterShouldBeFound("donotcall.equals=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterList where donotcall equals to UPDATED_DONOTCALL
        defaultContactMasterShouldNotBeFound("donotcall.equals=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDonotcallIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where donotcall not equals to DEFAULT_DONOTCALL
        defaultContactMasterShouldNotBeFound("donotcall.notEquals=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterList where donotcall not equals to UPDATED_DONOTCALL
        defaultContactMasterShouldBeFound("donotcall.notEquals=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDonotcallIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where donotcall in DEFAULT_DONOTCALL or UPDATED_DONOTCALL
        defaultContactMasterShouldBeFound("donotcall.in=" + DEFAULT_DONOTCALL + "," + UPDATED_DONOTCALL);

        // Get all the contactMasterList where donotcall equals to UPDATED_DONOTCALL
        defaultContactMasterShouldNotBeFound("donotcall.in=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDonotcallIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where donotcall is not null
        defaultContactMasterShouldBeFound("donotcall.specified=true");

        // Get all the contactMasterList where donotcall is null
        defaultContactMasterShouldNotBeFound("donotcall.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByDonotcallContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where donotcall contains DEFAULT_DONOTCALL
        defaultContactMasterShouldBeFound("donotcall.contains=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterList where donotcall contains UPDATED_DONOTCALL
        defaultContactMasterShouldNotBeFound("donotcall.contains=" + UPDATED_DONOTCALL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDonotcallNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where donotcall does not contain DEFAULT_DONOTCALL
        defaultContactMasterShouldNotBeFound("donotcall.doesNotContain=" + DEFAULT_DONOTCALL);

        // Get all the contactMasterList where donotcall does not contain UPDATED_DONOTCALL
        defaultContactMasterShouldBeFound("donotcall.doesNotContain=" + UPDATED_DONOTCALL);
    }


    @Test
    @Transactional
    public void getAllContactMastersByDnsendDirectMailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnsendDirectMail equals to DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldBeFound("dnsendDirectMail.equals=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterList where dnsendDirectMail equals to UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldNotBeFound("dnsendDirectMail.equals=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnsendDirectMailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnsendDirectMail not equals to DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldNotBeFound("dnsendDirectMail.notEquals=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterList where dnsendDirectMail not equals to UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldBeFound("dnsendDirectMail.notEquals=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnsendDirectMailIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnsendDirectMail in DEFAULT_DNSEND_DIRECT_MAIL or UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldBeFound("dnsendDirectMail.in=" + DEFAULT_DNSEND_DIRECT_MAIL + "," + UPDATED_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterList where dnsendDirectMail equals to UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldNotBeFound("dnsendDirectMail.in=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnsendDirectMailIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnsendDirectMail is not null
        defaultContactMasterShouldBeFound("dnsendDirectMail.specified=true");

        // Get all the contactMasterList where dnsendDirectMail is null
        defaultContactMasterShouldNotBeFound("dnsendDirectMail.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByDnsendDirectMailContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnsendDirectMail contains DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldBeFound("dnsendDirectMail.contains=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterList where dnsendDirectMail contains UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldNotBeFound("dnsendDirectMail.contains=" + UPDATED_DNSEND_DIRECT_MAIL);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnsendDirectMailNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnsendDirectMail does not contain DEFAULT_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldNotBeFound("dnsendDirectMail.doesNotContain=" + DEFAULT_DNSEND_DIRECT_MAIL);

        // Get all the contactMasterList where dnsendDirectMail does not contain UPDATED_DNSEND_DIRECT_MAIL
        defaultContactMasterShouldBeFound("dnsendDirectMail.doesNotContain=" + UPDATED_DNSEND_DIRECT_MAIL);
    }


    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideMhpIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideMhp equals to DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldBeFound("dnshareOutsideMhp.equals=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterList where dnshareOutsideMhp equals to UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldNotBeFound("dnshareOutsideMhp.equals=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideMhpIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideMhp not equals to DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldNotBeFound("dnshareOutsideMhp.notEquals=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterList where dnshareOutsideMhp not equals to UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldBeFound("dnshareOutsideMhp.notEquals=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideMhpIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideMhp in DEFAULT_DNSHARE_OUTSIDE_MHP or UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldBeFound("dnshareOutsideMhp.in=" + DEFAULT_DNSHARE_OUTSIDE_MHP + "," + UPDATED_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterList where dnshareOutsideMhp equals to UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldNotBeFound("dnshareOutsideMhp.in=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideMhpIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideMhp is not null
        defaultContactMasterShouldBeFound("dnshareOutsideMhp.specified=true");

        // Get all the contactMasterList where dnshareOutsideMhp is null
        defaultContactMasterShouldNotBeFound("dnshareOutsideMhp.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideMhpContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideMhp contains DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldBeFound("dnshareOutsideMhp.contains=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterList where dnshareOutsideMhp contains UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldNotBeFound("dnshareOutsideMhp.contains=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideMhpNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideMhp does not contain DEFAULT_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldNotBeFound("dnshareOutsideMhp.doesNotContain=" + DEFAULT_DNSHARE_OUTSIDE_MHP);

        // Get all the contactMasterList where dnshareOutsideMhp does not contain UPDATED_DNSHARE_OUTSIDE_MHP
        defaultContactMasterShouldBeFound("dnshareOutsideMhp.doesNotContain=" + UPDATED_DNSHARE_OUTSIDE_MHP);
    }


    @Test
    @Transactional
    public void getAllContactMastersByOutsideOfContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where outsideOfContact equals to DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldBeFound("outsideOfContact.equals=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterList where outsideOfContact equals to UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldNotBeFound("outsideOfContact.equals=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOutsideOfContactIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where outsideOfContact not equals to DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldNotBeFound("outsideOfContact.notEquals=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterList where outsideOfContact not equals to UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldBeFound("outsideOfContact.notEquals=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOutsideOfContactIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where outsideOfContact in DEFAULT_OUTSIDE_OF_CONTACT or UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldBeFound("outsideOfContact.in=" + DEFAULT_OUTSIDE_OF_CONTACT + "," + UPDATED_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterList where outsideOfContact equals to UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldNotBeFound("outsideOfContact.in=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOutsideOfContactIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where outsideOfContact is not null
        defaultContactMasterShouldBeFound("outsideOfContact.specified=true");

        // Get all the contactMasterList where outsideOfContact is null
        defaultContactMasterShouldNotBeFound("outsideOfContact.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByOutsideOfContactContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where outsideOfContact contains DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldBeFound("outsideOfContact.contains=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterList where outsideOfContact contains UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldNotBeFound("outsideOfContact.contains=" + UPDATED_OUTSIDE_OF_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByOutsideOfContactNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where outsideOfContact does not contain DEFAULT_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldNotBeFound("outsideOfContact.doesNotContain=" + DEFAULT_OUTSIDE_OF_CONTACT);

        // Get all the contactMasterList where outsideOfContact does not contain UPDATED_OUTSIDE_OF_CONTACT
        defaultContactMasterShouldBeFound("outsideOfContact.doesNotContain=" + UPDATED_OUTSIDE_OF_CONTACT);
    }


    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideSnpIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideSnp equals to DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldBeFound("dnshareOutsideSnp.equals=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterList where dnshareOutsideSnp equals to UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldNotBeFound("dnshareOutsideSnp.equals=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideSnpIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideSnp not equals to DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldNotBeFound("dnshareOutsideSnp.notEquals=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterList where dnshareOutsideSnp not equals to UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldBeFound("dnshareOutsideSnp.notEquals=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideSnpIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideSnp in DEFAULT_DNSHARE_OUTSIDE_SNP or UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldBeFound("dnshareOutsideSnp.in=" + DEFAULT_DNSHARE_OUTSIDE_SNP + "," + UPDATED_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterList where dnshareOutsideSnp equals to UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldNotBeFound("dnshareOutsideSnp.in=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideSnpIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideSnp is not null
        defaultContactMasterShouldBeFound("dnshareOutsideSnp.specified=true");

        // Get all the contactMasterList where dnshareOutsideSnp is null
        defaultContactMasterShouldNotBeFound("dnshareOutsideSnp.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideSnpContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideSnp contains DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldBeFound("dnshareOutsideSnp.contains=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterList where dnshareOutsideSnp contains UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldNotBeFound("dnshareOutsideSnp.contains=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }

    @Test
    @Transactional
    public void getAllContactMastersByDnshareOutsideSnpNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where dnshareOutsideSnp does not contain DEFAULT_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldNotBeFound("dnshareOutsideSnp.doesNotContain=" + DEFAULT_DNSHARE_OUTSIDE_SNP);

        // Get all the contactMasterList where dnshareOutsideSnp does not contain UPDATED_DNSHARE_OUTSIDE_SNP
        defaultContactMasterShouldBeFound("dnshareOutsideSnp.doesNotContain=" + UPDATED_DNSHARE_OUTSIDE_SNP);
    }


    @Test
    @Transactional
    public void getAllContactMastersByEmailOptoutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where emailOptout equals to DEFAULT_EMAIL_OPTOUT
        defaultContactMasterShouldBeFound("emailOptout.equals=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterList where emailOptout equals to UPDATED_EMAIL_OPTOUT
        defaultContactMasterShouldNotBeFound("emailOptout.equals=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailOptoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where emailOptout not equals to DEFAULT_EMAIL_OPTOUT
        defaultContactMasterShouldNotBeFound("emailOptout.notEquals=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterList where emailOptout not equals to UPDATED_EMAIL_OPTOUT
        defaultContactMasterShouldBeFound("emailOptout.notEquals=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailOptoutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where emailOptout in DEFAULT_EMAIL_OPTOUT or UPDATED_EMAIL_OPTOUT
        defaultContactMasterShouldBeFound("emailOptout.in=" + DEFAULT_EMAIL_OPTOUT + "," + UPDATED_EMAIL_OPTOUT);

        // Get all the contactMasterList where emailOptout equals to UPDATED_EMAIL_OPTOUT
        defaultContactMasterShouldNotBeFound("emailOptout.in=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailOptoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where emailOptout is not null
        defaultContactMasterShouldBeFound("emailOptout.specified=true");

        // Get all the contactMasterList where emailOptout is null
        defaultContactMasterShouldNotBeFound("emailOptout.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByEmailOptoutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where emailOptout contains DEFAULT_EMAIL_OPTOUT
        defaultContactMasterShouldBeFound("emailOptout.contains=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterList where emailOptout contains UPDATED_EMAIL_OPTOUT
        defaultContactMasterShouldNotBeFound("emailOptout.contains=" + UPDATED_EMAIL_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByEmailOptoutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where emailOptout does not contain DEFAULT_EMAIL_OPTOUT
        defaultContactMasterShouldNotBeFound("emailOptout.doesNotContain=" + DEFAULT_EMAIL_OPTOUT);

        // Get all the contactMasterList where emailOptout does not contain UPDATED_EMAIL_OPTOUT
        defaultContactMasterShouldBeFound("emailOptout.doesNotContain=" + UPDATED_EMAIL_OPTOUT);
    }


    @Test
    @Transactional
    public void getAllContactMastersBySmsOptoutIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where smsOptout equals to DEFAULT_SMS_OPTOUT
        defaultContactMasterShouldBeFound("smsOptout.equals=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterList where smsOptout equals to UPDATED_SMS_OPTOUT
        defaultContactMasterShouldNotBeFound("smsOptout.equals=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySmsOptoutIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where smsOptout not equals to DEFAULT_SMS_OPTOUT
        defaultContactMasterShouldNotBeFound("smsOptout.notEquals=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterList where smsOptout not equals to UPDATED_SMS_OPTOUT
        defaultContactMasterShouldBeFound("smsOptout.notEquals=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySmsOptoutIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where smsOptout in DEFAULT_SMS_OPTOUT or UPDATED_SMS_OPTOUT
        defaultContactMasterShouldBeFound("smsOptout.in=" + DEFAULT_SMS_OPTOUT + "," + UPDATED_SMS_OPTOUT);

        // Get all the contactMasterList where smsOptout equals to UPDATED_SMS_OPTOUT
        defaultContactMasterShouldNotBeFound("smsOptout.in=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySmsOptoutIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where smsOptout is not null
        defaultContactMasterShouldBeFound("smsOptout.specified=true");

        // Get all the contactMasterList where smsOptout is null
        defaultContactMasterShouldNotBeFound("smsOptout.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersBySmsOptoutContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where smsOptout contains DEFAULT_SMS_OPTOUT
        defaultContactMasterShouldBeFound("smsOptout.contains=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterList where smsOptout contains UPDATED_SMS_OPTOUT
        defaultContactMasterShouldNotBeFound("smsOptout.contains=" + UPDATED_SMS_OPTOUT);
    }

    @Test
    @Transactional
    public void getAllContactMastersBySmsOptoutNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where smsOptout does not contain DEFAULT_SMS_OPTOUT
        defaultContactMasterShouldNotBeFound("smsOptout.doesNotContain=" + DEFAULT_SMS_OPTOUT);

        // Get all the contactMasterList where smsOptout does not contain UPDATED_SMS_OPTOUT
        defaultContactMasterShouldBeFound("smsOptout.doesNotContain=" + UPDATED_SMS_OPTOUT);
    }


    @Test
    @Transactional
    public void getAllContactMastersByIsSezContactFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isSezContactFlag equals to DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isSezContactFlag.equals=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterList where isSezContactFlag equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isSezContactFlag.equals=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsSezContactFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isSezContactFlag not equals to DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isSezContactFlag.notEquals=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterList where isSezContactFlag not equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isSezContactFlag.notEquals=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsSezContactFlagIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isSezContactFlag in DEFAULT_IS_SEZ_CONTACT_FLAG or UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isSezContactFlag.in=" + DEFAULT_IS_SEZ_CONTACT_FLAG + "," + UPDATED_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterList where isSezContactFlag equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isSezContactFlag.in=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsSezContactFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isSezContactFlag is not null
        defaultContactMasterShouldBeFound("isSezContactFlag.specified=true");

        // Get all the contactMasterList where isSezContactFlag is null
        defaultContactMasterShouldNotBeFound("isSezContactFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByIsSezContactFlagContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isSezContactFlag contains DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isSezContactFlag.contains=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterList where isSezContactFlag contains UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isSezContactFlag.contains=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsSezContactFlagNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isSezContactFlag does not contain DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isSezContactFlag.doesNotContain=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the contactMasterList where isSezContactFlag does not contain UPDATED_IS_SEZ_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isSezContactFlag.doesNotContain=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }


    @Test
    @Transactional
    public void getAllContactMastersByIsExemptContactFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isExemptContactFlag equals to DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isExemptContactFlag.equals=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterList where isExemptContactFlag equals to UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isExemptContactFlag.equals=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsExemptContactFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isExemptContactFlag not equals to DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isExemptContactFlag.notEquals=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterList where isExemptContactFlag not equals to UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isExemptContactFlag.notEquals=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsExemptContactFlagIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isExemptContactFlag in DEFAULT_IS_EXEMPT_CONTACT_FLAG or UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isExemptContactFlag.in=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG + "," + UPDATED_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterList where isExemptContactFlag equals to UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isExemptContactFlag.in=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsExemptContactFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isExemptContactFlag is not null
        defaultContactMasterShouldBeFound("isExemptContactFlag.specified=true");

        // Get all the contactMasterList where isExemptContactFlag is null
        defaultContactMasterShouldNotBeFound("isExemptContactFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByIsExemptContactFlagContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isExemptContactFlag contains DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isExemptContactFlag.contains=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterList where isExemptContactFlag contains UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isExemptContactFlag.contains=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsExemptContactFlagNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isExemptContactFlag does not contain DEFAULT_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldNotBeFound("isExemptContactFlag.doesNotContain=" + DEFAULT_IS_EXEMPT_CONTACT_FLAG);

        // Get all the contactMasterList where isExemptContactFlag does not contain UPDATED_IS_EXEMPT_CONTACT_FLAG
        defaultContactMasterShouldBeFound("isExemptContactFlag.doesNotContain=" + UPDATED_IS_EXEMPT_CONTACT_FLAG);
    }


    @Test
    @Transactional
    public void getAllContactMastersByGstNotApplicableIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNotApplicable equals to DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterShouldBeFound("gstNotApplicable.equals=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterList where gstNotApplicable equals to UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterShouldNotBeFound("gstNotApplicable.equals=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNotApplicableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNotApplicable not equals to DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterShouldNotBeFound("gstNotApplicable.notEquals=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterList where gstNotApplicable not equals to UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterShouldBeFound("gstNotApplicable.notEquals=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNotApplicableIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNotApplicable in DEFAULT_GST_NOT_APPLICABLE or UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterShouldBeFound("gstNotApplicable.in=" + DEFAULT_GST_NOT_APPLICABLE + "," + UPDATED_GST_NOT_APPLICABLE);

        // Get all the contactMasterList where gstNotApplicable equals to UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterShouldNotBeFound("gstNotApplicable.in=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNotApplicableIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNotApplicable is not null
        defaultContactMasterShouldBeFound("gstNotApplicable.specified=true");

        // Get all the contactMasterList where gstNotApplicable is null
        defaultContactMasterShouldNotBeFound("gstNotApplicable.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByGstNotApplicableContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNotApplicable contains DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterShouldBeFound("gstNotApplicable.contains=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterList where gstNotApplicable contains UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterShouldNotBeFound("gstNotApplicable.contains=" + UPDATED_GST_NOT_APPLICABLE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNotApplicableNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNotApplicable does not contain DEFAULT_GST_NOT_APPLICABLE
        defaultContactMasterShouldNotBeFound("gstNotApplicable.doesNotContain=" + DEFAULT_GST_NOT_APPLICABLE);

        // Get all the contactMasterList where gstNotApplicable does not contain UPDATED_GST_NOT_APPLICABLE
        defaultContactMasterShouldBeFound("gstNotApplicable.doesNotContain=" + UPDATED_GST_NOT_APPLICABLE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByBillingContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where billingContact equals to DEFAULT_BILLING_CONTACT
        defaultContactMasterShouldBeFound("billingContact.equals=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterList where billingContact equals to UPDATED_BILLING_CONTACT
        defaultContactMasterShouldNotBeFound("billingContact.equals=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByBillingContactIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where billingContact not equals to DEFAULT_BILLING_CONTACT
        defaultContactMasterShouldNotBeFound("billingContact.notEquals=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterList where billingContact not equals to UPDATED_BILLING_CONTACT
        defaultContactMasterShouldBeFound("billingContact.notEquals=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByBillingContactIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where billingContact in DEFAULT_BILLING_CONTACT or UPDATED_BILLING_CONTACT
        defaultContactMasterShouldBeFound("billingContact.in=" + DEFAULT_BILLING_CONTACT + "," + UPDATED_BILLING_CONTACT);

        // Get all the contactMasterList where billingContact equals to UPDATED_BILLING_CONTACT
        defaultContactMasterShouldNotBeFound("billingContact.in=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByBillingContactIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where billingContact is not null
        defaultContactMasterShouldBeFound("billingContact.specified=true");

        // Get all the contactMasterList where billingContact is null
        defaultContactMasterShouldNotBeFound("billingContact.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByBillingContactContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where billingContact contains DEFAULT_BILLING_CONTACT
        defaultContactMasterShouldBeFound("billingContact.contains=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterList where billingContact contains UPDATED_BILLING_CONTACT
        defaultContactMasterShouldNotBeFound("billingContact.contains=" + UPDATED_BILLING_CONTACT);
    }

    @Test
    @Transactional
    public void getAllContactMastersByBillingContactNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where billingContact does not contain DEFAULT_BILLING_CONTACT
        defaultContactMasterShouldNotBeFound("billingContact.doesNotContain=" + DEFAULT_BILLING_CONTACT);

        // Get all the contactMasterList where billingContact does not contain UPDATED_BILLING_CONTACT
        defaultContactMasterShouldBeFound("billingContact.doesNotContain=" + UPDATED_BILLING_CONTACT);
    }


    @Test
    @Transactional
    public void getAllContactMastersByGstPartyTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstPartyType equals to DEFAULT_GST_PARTY_TYPE
        defaultContactMasterShouldBeFound("gstPartyType.equals=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterList where gstPartyType equals to UPDATED_GST_PARTY_TYPE
        defaultContactMasterShouldNotBeFound("gstPartyType.equals=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstPartyTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstPartyType not equals to DEFAULT_GST_PARTY_TYPE
        defaultContactMasterShouldNotBeFound("gstPartyType.notEquals=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterList where gstPartyType not equals to UPDATED_GST_PARTY_TYPE
        defaultContactMasterShouldBeFound("gstPartyType.notEquals=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstPartyTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstPartyType in DEFAULT_GST_PARTY_TYPE or UPDATED_GST_PARTY_TYPE
        defaultContactMasterShouldBeFound("gstPartyType.in=" + DEFAULT_GST_PARTY_TYPE + "," + UPDATED_GST_PARTY_TYPE);

        // Get all the contactMasterList where gstPartyType equals to UPDATED_GST_PARTY_TYPE
        defaultContactMasterShouldNotBeFound("gstPartyType.in=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstPartyTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstPartyType is not null
        defaultContactMasterShouldBeFound("gstPartyType.specified=true");

        // Get all the contactMasterList where gstPartyType is null
        defaultContactMasterShouldNotBeFound("gstPartyType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByGstPartyTypeContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstPartyType contains DEFAULT_GST_PARTY_TYPE
        defaultContactMasterShouldBeFound("gstPartyType.contains=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterList where gstPartyType contains UPDATED_GST_PARTY_TYPE
        defaultContactMasterShouldNotBeFound("gstPartyType.contains=" + UPDATED_GST_PARTY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstPartyTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstPartyType does not contain DEFAULT_GST_PARTY_TYPE
        defaultContactMasterShouldNotBeFound("gstPartyType.doesNotContain=" + DEFAULT_GST_PARTY_TYPE);

        // Get all the contactMasterList where gstPartyType does not contain UPDATED_GST_PARTY_TYPE
        defaultContactMasterShouldBeFound("gstPartyType.doesNotContain=" + UPDATED_GST_PARTY_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByTanIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tan equals to DEFAULT_TAN
        defaultContactMasterShouldBeFound("tan.equals=" + DEFAULT_TAN);

        // Get all the contactMasterList where tan equals to UPDATED_TAN
        defaultContactMasterShouldNotBeFound("tan.equals=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tan not equals to DEFAULT_TAN
        defaultContactMasterShouldNotBeFound("tan.notEquals=" + DEFAULT_TAN);

        // Get all the contactMasterList where tan not equals to UPDATED_TAN
        defaultContactMasterShouldBeFound("tan.notEquals=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTanIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tan in DEFAULT_TAN or UPDATED_TAN
        defaultContactMasterShouldBeFound("tan.in=" + DEFAULT_TAN + "," + UPDATED_TAN);

        // Get all the contactMasterList where tan equals to UPDATED_TAN
        defaultContactMasterShouldNotBeFound("tan.in=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTanIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tan is not null
        defaultContactMasterShouldBeFound("tan.specified=true");

        // Get all the contactMasterList where tan is null
        defaultContactMasterShouldNotBeFound("tan.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByTanContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tan contains DEFAULT_TAN
        defaultContactMasterShouldBeFound("tan.contains=" + DEFAULT_TAN);

        // Get all the contactMasterList where tan contains UPDATED_TAN
        defaultContactMasterShouldNotBeFound("tan.contains=" + UPDATED_TAN);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTanNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tan does not contain DEFAULT_TAN
        defaultContactMasterShouldNotBeFound("tan.doesNotContain=" + DEFAULT_TAN);

        // Get all the contactMasterList where tan does not contain UPDATED_TAN
        defaultContactMasterShouldBeFound("tan.doesNotContain=" + UPDATED_TAN);
    }


    @Test
    @Transactional
    public void getAllContactMastersByGstNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNumber equals to DEFAULT_GST_NUMBER
        defaultContactMasterShouldBeFound("gstNumber.equals=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterList where gstNumber equals to UPDATED_GST_NUMBER
        defaultContactMasterShouldNotBeFound("gstNumber.equals=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNumber not equals to DEFAULT_GST_NUMBER
        defaultContactMasterShouldNotBeFound("gstNumber.notEquals=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterList where gstNumber not equals to UPDATED_GST_NUMBER
        defaultContactMasterShouldBeFound("gstNumber.notEquals=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNumberIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNumber in DEFAULT_GST_NUMBER or UPDATED_GST_NUMBER
        defaultContactMasterShouldBeFound("gstNumber.in=" + DEFAULT_GST_NUMBER + "," + UPDATED_GST_NUMBER);

        // Get all the contactMasterList where gstNumber equals to UPDATED_GST_NUMBER
        defaultContactMasterShouldNotBeFound("gstNumber.in=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNumber is not null
        defaultContactMasterShouldBeFound("gstNumber.specified=true");

        // Get all the contactMasterList where gstNumber is null
        defaultContactMasterShouldNotBeFound("gstNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByGstNumberContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNumber contains DEFAULT_GST_NUMBER
        defaultContactMasterShouldBeFound("gstNumber.contains=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterList where gstNumber contains UPDATED_GST_NUMBER
        defaultContactMasterShouldNotBeFound("gstNumber.contains=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByGstNumberNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where gstNumber does not contain DEFAULT_GST_NUMBER
        defaultContactMasterShouldNotBeFound("gstNumber.doesNotContain=" + DEFAULT_GST_NUMBER);

        // Get all the contactMasterList where gstNumber does not contain UPDATED_GST_NUMBER
        defaultContactMasterShouldBeFound("gstNumber.doesNotContain=" + UPDATED_GST_NUMBER);
    }


    @Test
    @Transactional
    public void getAllContactMastersByTdsNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tdsNumber equals to DEFAULT_TDS_NUMBER
        defaultContactMasterShouldBeFound("tdsNumber.equals=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterList where tdsNumber equals to UPDATED_TDS_NUMBER
        defaultContactMasterShouldNotBeFound("tdsNumber.equals=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTdsNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tdsNumber not equals to DEFAULT_TDS_NUMBER
        defaultContactMasterShouldNotBeFound("tdsNumber.notEquals=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterList where tdsNumber not equals to UPDATED_TDS_NUMBER
        defaultContactMasterShouldBeFound("tdsNumber.notEquals=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTdsNumberIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tdsNumber in DEFAULT_TDS_NUMBER or UPDATED_TDS_NUMBER
        defaultContactMasterShouldBeFound("tdsNumber.in=" + DEFAULT_TDS_NUMBER + "," + UPDATED_TDS_NUMBER);

        // Get all the contactMasterList where tdsNumber equals to UPDATED_TDS_NUMBER
        defaultContactMasterShouldNotBeFound("tdsNumber.in=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTdsNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tdsNumber is not null
        defaultContactMasterShouldBeFound("tdsNumber.specified=true");

        // Get all the contactMasterList where tdsNumber is null
        defaultContactMasterShouldNotBeFound("tdsNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByTdsNumberContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tdsNumber contains DEFAULT_TDS_NUMBER
        defaultContactMasterShouldBeFound("tdsNumber.contains=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterList where tdsNumber contains UPDATED_TDS_NUMBER
        defaultContactMasterShouldNotBeFound("tdsNumber.contains=" + UPDATED_TDS_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactMastersByTdsNumberNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where tdsNumber does not contain DEFAULT_TDS_NUMBER
        defaultContactMasterShouldNotBeFound("tdsNumber.doesNotContain=" + DEFAULT_TDS_NUMBER);

        // Get all the contactMasterList where tdsNumber does not contain UPDATED_TDS_NUMBER
        defaultContactMasterShouldBeFound("tdsNumber.doesNotContain=" + UPDATED_TDS_NUMBER);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultContactMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultContactMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultContactMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultContactMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultContactMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the contactMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultContactMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdBy is not null
        defaultContactMasterShouldBeFound("createdBy.specified=true");

        // Get all the contactMasterList where createdBy is null
        defaultContactMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultContactMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterList where createdBy contains UPDATED_CREATED_BY
        defaultContactMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultContactMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the contactMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultContactMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the contactMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate is not null
        defaultContactMasterShouldBeFound("createdDate.specified=true");

        // Get all the contactMasterList where createdDate is null
        defaultContactMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate is less than DEFAULT_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterList where createdDate is less than UPDATED_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultContactMasterShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the contactMasterList where createdDate is greater than SMALLER_CREATED_DATE
        defaultContactMasterShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultContactMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultContactMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultContactMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the contactMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultContactMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedBy is not null
        defaultContactMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the contactMasterList where lastModifiedBy is null
        defaultContactMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultContactMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultContactMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the contactMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultContactMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate is not null
        defaultContactMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the contactMasterList where lastModifiedDate is null
        defaultContactMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllContactMastersByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultContactMasterShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the contactMasterList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultContactMasterShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllContactMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultContactMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultContactMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultContactMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultContactMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultContactMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the contactMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultContactMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isDeleted is not null
        defaultContactMasterShouldBeFound("isDeleted.specified=true");

        // Get all the contactMasterList where isDeleted is null
        defaultContactMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultContactMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultContactMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllContactMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);

        // Get all the contactMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultContactMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the contactMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultContactMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllContactMastersByCompGstNotApplicableIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);
        CompGstNotApplicable compGstNotApplicable = CompGstNotApplicableResourceIT.createEntity(em);
        em.persist(compGstNotApplicable);
        em.flush();
        contactMaster.addCompGstNotApplicable(compGstNotApplicable);
        contactMasterRepository.saveAndFlush(contactMaster);
        Long compGstNotApplicableId = compGstNotApplicable.getId();

        // Get all the contactMasterList where compGstNotApplicable equals to compGstNotApplicableId
        defaultContactMasterShouldBeFound("compGstNotApplicableId.equals=" + compGstNotApplicableId);

        // Get all the contactMasterList where compGstNotApplicable equals to compGstNotApplicableId + 1
        defaultContactMasterShouldNotBeFound("compGstNotApplicableId.equals=" + (compGstNotApplicableId + 1));
    }


    @Test
    @Transactional
    public void getAllContactMastersByContactChangeRequestIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMasterRepository.saveAndFlush(contactMaster);
        ContactChangeRequest contactChangeRequest = ContactChangeRequestResourceIT.createEntity(em);
        em.persist(contactChangeRequest);
        em.flush();
        contactMaster.addContactChangeRequest(contactChangeRequest);
        contactMasterRepository.saveAndFlush(contactMaster);
        Long contactChangeRequestId = contactChangeRequest.getId();

        // Get all the contactMasterList where contactChangeRequest equals to contactChangeRequestId
        defaultContactMasterShouldBeFound("contactChangeRequestId.equals=" + contactChangeRequestId);

        // Get all the contactMasterList where contactChangeRequest equals to contactChangeRequestId + 1
        defaultContactMasterShouldNotBeFound("contactChangeRequestId.equals=" + (contactChangeRequestId + 1));
    }


    @Test
    @Transactional
    public void getAllContactMastersByCompanyCodeIsEqualToSomething() throws Exception {
        // Get already existing entity
        OnesourceCompanyMaster companyCode = contactMaster.getCompanyCode();
        contactMasterRepository.saveAndFlush(contactMaster);
        Long companyCodeId = companyCode.getId();

        // Get all the contactMasterList where companyCode equals to companyCodeId
        defaultContactMasterShouldBeFound("companyCodeId.equals=" + companyCodeId);

        // Get all the contactMasterList where companyCode equals to companyCodeId + 1
        defaultContactMasterShouldNotBeFound("companyCodeId.equals=" + (companyCodeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactMasterShouldBeFound(String filter) throws Exception {
        restContactMasterMockMvc.perform(get("/api/contact-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME)))
            .andExpect(jsonPath("$.[*].salutation").value(hasItem(DEFAULT_SALUTATION)))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3)))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].phoneNum").value(hasItem(DEFAULT_PHONE_NUM)))
            .andExpect(jsonPath("$.[*].faxNum").value(hasItem(DEFAULT_FAX_NUM)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].mobileNum").value(hasItem(DEFAULT_MOBILE_NUM)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].keyPerson").value(hasItem(DEFAULT_KEY_PERSON)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].levelImportance").value(hasItem(DEFAULT_LEVEL_IMPORTANCE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].removeReason").value(hasItem(DEFAULT_REMOVE_REASON)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].oFlag").value(hasItem(DEFAULT_O_FLAG)))
            .andExpect(jsonPath("$.[*].optOut").value(hasItem(DEFAULT_OPT_OUT)))
            .andExpect(jsonPath("$.[*].ofaContactId").value(hasItem(DEFAULT_OFA_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].optOutDate").value(hasItem(DEFAULT_OPT_OUT_DATE.toString())))
            .andExpect(jsonPath("$.[*].faxOptout").value(hasItem(DEFAULT_FAX_OPTOUT)))
            .andExpect(jsonPath("$.[*].donotcall").value(hasItem(DEFAULT_DONOTCALL)))
            .andExpect(jsonPath("$.[*].dnsendDirectMail").value(hasItem(DEFAULT_DNSEND_DIRECT_MAIL)))
            .andExpect(jsonPath("$.[*].dnshareOutsideMhp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_MHP)))
            .andExpect(jsonPath("$.[*].outsideOfContact").value(hasItem(DEFAULT_OUTSIDE_OF_CONTACT)))
            .andExpect(jsonPath("$.[*].dnshareOutsideSnp").value(hasItem(DEFAULT_DNSHARE_OUTSIDE_SNP)))
            .andExpect(jsonPath("$.[*].emailOptout").value(hasItem(DEFAULT_EMAIL_OPTOUT)))
            .andExpect(jsonPath("$.[*].smsOptout").value(hasItem(DEFAULT_SMS_OPTOUT)))
            .andExpect(jsonPath("$.[*].isSezContactFlag").value(hasItem(DEFAULT_IS_SEZ_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].isExemptContactFlag").value(hasItem(DEFAULT_IS_EXEMPT_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].gstNotApplicable").value(hasItem(DEFAULT_GST_NOT_APPLICABLE)))
            .andExpect(jsonPath("$.[*].billingContact").value(hasItem(DEFAULT_BILLING_CONTACT)))
            .andExpect(jsonPath("$.[*].gstPartyType").value(hasItem(DEFAULT_GST_PARTY_TYPE)))
            .andExpect(jsonPath("$.[*].tan").value(hasItem(DEFAULT_TAN)))
            .andExpect(jsonPath("$.[*].gstNumber").value(hasItem(DEFAULT_GST_NUMBER)))
            .andExpect(jsonPath("$.[*].tdsNumber").value(hasItem(DEFAULT_TDS_NUMBER)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restContactMasterMockMvc.perform(get("/api/contact-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactMasterShouldNotBeFound(String filter) throws Exception {
        restContactMasterMockMvc.perform(get("/api/contact-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactMasterMockMvc.perform(get("/api/contact-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactMaster() throws Exception {
        // Get the contactMaster
        restContactMasterMockMvc.perform(get("/api/contact-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactMaster() throws Exception {
        // Initialize the database
        contactMasterService.save(contactMaster);

        int databaseSizeBeforeUpdate = contactMasterRepository.findAll().size();

        // Update the contactMaster
        ContactMaster updatedContactMaster = contactMasterRepository.findById(contactMaster.getId()).get();
        // Disconnect from session so that the updates on updatedContactMaster are not directly saved in db
        em.detach(updatedContactMaster);
        updatedContactMaster
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .salutation(UPDATED_SALUTATION)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .designation(UPDATED_DESIGNATION)
            .department(UPDATED_DEPARTMENT)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .pin(UPDATED_PIN)
            .phoneNum(UPDATED_PHONE_NUM)
            .faxNum(UPDATED_FAX_NUM)
            .email(UPDATED_EMAIL)
            .mobileNum(UPDATED_MOBILE_NUM)
            .recordId(UPDATED_RECORD_ID)
            .comments(UPDATED_COMMENTS)
            .keyPerson(UPDATED_KEY_PERSON)
            .contactId(UPDATED_CONTACT_ID)
            .levelImportance(UPDATED_LEVEL_IMPORTANCE)
            .status(UPDATED_STATUS)
            .removeReason(UPDATED_REMOVE_REASON)
            .cityId(UPDATED_CITY_ID)
            .oFlag(UPDATED_O_FLAG)
            .optOut(UPDATED_OPT_OUT)
            .ofaContactId(UPDATED_OFA_CONTACT_ID)
            .optOutDate(UPDATED_OPT_OUT_DATE)
            .faxOptout(UPDATED_FAX_OPTOUT)
            .donotcall(UPDATED_DONOTCALL)
            .dnsendDirectMail(UPDATED_DNSEND_DIRECT_MAIL)
            .dnshareOutsideMhp(UPDATED_DNSHARE_OUTSIDE_MHP)
            .outsideOfContact(UPDATED_OUTSIDE_OF_CONTACT)
            .dnshareOutsideSnp(UPDATED_DNSHARE_OUTSIDE_SNP)
            .emailOptout(UPDATED_EMAIL_OPTOUT)
            .smsOptout(UPDATED_SMS_OPTOUT)
            .isSezContactFlag(UPDATED_IS_SEZ_CONTACT_FLAG)
            .isExemptContactFlag(UPDATED_IS_EXEMPT_CONTACT_FLAG)
            .gstNotApplicable(UPDATED_GST_NOT_APPLICABLE)
            .billingContact(UPDATED_BILLING_CONTACT)
            .gstPartyType(UPDATED_GST_PARTY_TYPE)
            .tan(UPDATED_TAN)
            .gstNumber(UPDATED_GST_NUMBER)
            .tdsNumber(UPDATED_TDS_NUMBER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);

        restContactMasterMockMvc.perform(put("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedContactMaster)))
            .andExpect(status().isOk());

        // Validate the ContactMaster in the database
        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeUpdate);
        ContactMaster testContactMaster = contactMasterList.get(contactMasterList.size() - 1);
        assertThat(testContactMaster.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testContactMaster.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testContactMaster.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testContactMaster.getSalutation()).isEqualTo(UPDATED_SALUTATION);
        assertThat(testContactMaster.getAddress1()).isEqualTo(UPDATED_ADDRESS_1);
        assertThat(testContactMaster.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testContactMaster.getAddress3()).isEqualTo(UPDATED_ADDRESS_3);
        assertThat(testContactMaster.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testContactMaster.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testContactMaster.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testContactMaster.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testContactMaster.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testContactMaster.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testContactMaster.getPhoneNum()).isEqualTo(UPDATED_PHONE_NUM);
        assertThat(testContactMaster.getFaxNum()).isEqualTo(UPDATED_FAX_NUM);
        assertThat(testContactMaster.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testContactMaster.getMobileNum()).isEqualTo(UPDATED_MOBILE_NUM);
        assertThat(testContactMaster.getRecordId()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testContactMaster.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testContactMaster.getKeyPerson()).isEqualTo(UPDATED_KEY_PERSON);
        assertThat(testContactMaster.getContactId()).isEqualTo(UPDATED_CONTACT_ID);
        assertThat(testContactMaster.getLevelImportance()).isEqualTo(UPDATED_LEVEL_IMPORTANCE);
        assertThat(testContactMaster.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testContactMaster.getRemoveReason()).isEqualTo(UPDATED_REMOVE_REASON);
        assertThat(testContactMaster.getCityId()).isEqualTo(UPDATED_CITY_ID);
        assertThat(testContactMaster.getoFlag()).isEqualTo(UPDATED_O_FLAG);
        assertThat(testContactMaster.getOptOut()).isEqualTo(UPDATED_OPT_OUT);
        assertThat(testContactMaster.getOfaContactId()).isEqualTo(UPDATED_OFA_CONTACT_ID);
        assertThat(testContactMaster.getOptOutDate()).isEqualTo(UPDATED_OPT_OUT_DATE);
        assertThat(testContactMaster.getFaxOptout()).isEqualTo(UPDATED_FAX_OPTOUT);
        assertThat(testContactMaster.getDonotcall()).isEqualTo(UPDATED_DONOTCALL);
        assertThat(testContactMaster.getDnsendDirectMail()).isEqualTo(UPDATED_DNSEND_DIRECT_MAIL);
        assertThat(testContactMaster.getDnshareOutsideMhp()).isEqualTo(UPDATED_DNSHARE_OUTSIDE_MHP);
        assertThat(testContactMaster.getOutsideOfContact()).isEqualTo(UPDATED_OUTSIDE_OF_CONTACT);
        assertThat(testContactMaster.getDnshareOutsideSnp()).isEqualTo(UPDATED_DNSHARE_OUTSIDE_SNP);
        assertThat(testContactMaster.getEmailOptout()).isEqualTo(UPDATED_EMAIL_OPTOUT);
        assertThat(testContactMaster.getSmsOptout()).isEqualTo(UPDATED_SMS_OPTOUT);
        assertThat(testContactMaster.getIsSezContactFlag()).isEqualTo(UPDATED_IS_SEZ_CONTACT_FLAG);
        assertThat(testContactMaster.getIsExemptContactFlag()).isEqualTo(UPDATED_IS_EXEMPT_CONTACT_FLAG);
        assertThat(testContactMaster.getGstNotApplicable()).isEqualTo(UPDATED_GST_NOT_APPLICABLE);
        assertThat(testContactMaster.getBillingContact()).isEqualTo(UPDATED_BILLING_CONTACT);
        assertThat(testContactMaster.getGstPartyType()).isEqualTo(UPDATED_GST_PARTY_TYPE);
        assertThat(testContactMaster.getTan()).isEqualTo(UPDATED_TAN);
        assertThat(testContactMaster.getGstNumber()).isEqualTo(UPDATED_GST_NUMBER);
        assertThat(testContactMaster.getTdsNumber()).isEqualTo(UPDATED_TDS_NUMBER);
        assertThat(testContactMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testContactMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testContactMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testContactMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testContactMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingContactMaster() throws Exception {
        int databaseSizeBeforeUpdate = contactMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactMasterMockMvc.perform(put("/api/contact-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMaster)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMaster in the database
        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactMaster() throws Exception {
        // Initialize the database
        contactMasterService.save(contactMaster);

        int databaseSizeBeforeDelete = contactMasterRepository.findAll().size();

        // Delete the contactMaster
        restContactMasterMockMvc.perform(delete("/api/contact-masters/{id}", contactMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactMaster> contactMasterList = contactMasterRepository.findAll();
        assertThat(contactMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
