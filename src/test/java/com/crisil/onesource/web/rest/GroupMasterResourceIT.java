package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.GroupMaster;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.GroupMasterRepository;
import com.crisil.onesource.service.GroupMasterService;
import com.crisil.onesource.service.dto.GroupMasterCriteria;
import com.crisil.onesource.service.GroupMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link GroupMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class GroupMasterResourceIT {

    private static final Integer DEFAULT_GROUP_ID = 1;
    private static final Integer UPDATED_GROUP_ID = 2;
    private static final Integer SMALLER_GROUP_ID = 1 - 1;

    private static final String DEFAULT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUP_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private GroupMasterRepository groupMasterRepository;

    @Autowired
    private GroupMasterService groupMasterService;

    @Autowired
    private GroupMasterQueryService groupMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGroupMasterMockMvc;

    private GroupMaster groupMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GroupMaster createEntity(EntityManager em) {
        GroupMaster groupMaster = new GroupMaster()
            .groupId(DEFAULT_GROUP_ID)
            .groupName(DEFAULT_GROUP_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isDeleted(DEFAULT_IS_DELETED);
        return groupMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static GroupMaster createUpdatedEntity(EntityManager em) {
        GroupMaster groupMaster = new GroupMaster()
            .groupId(UPDATED_GROUP_ID)
            .groupName(UPDATED_GROUP_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);
        return groupMaster;
    }

    @BeforeEach
    public void initTest() {
        groupMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createGroupMaster() throws Exception {
        int databaseSizeBeforeCreate = groupMasterRepository.findAll().size();
        // Create the GroupMaster
        restGroupMasterMockMvc.perform(post("/api/group-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(groupMaster)))
            .andExpect(status().isCreated());

        // Validate the GroupMaster in the database
        List<GroupMaster> groupMasterList = groupMasterRepository.findAll();
        assertThat(groupMasterList).hasSize(databaseSizeBeforeCreate + 1);
        GroupMaster testGroupMaster = groupMasterList.get(groupMasterList.size() - 1);
        assertThat(testGroupMaster.getGroupId()).isEqualTo(DEFAULT_GROUP_ID);
        assertThat(testGroupMaster.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testGroupMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testGroupMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testGroupMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testGroupMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testGroupMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createGroupMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = groupMasterRepository.findAll().size();

        // Create the GroupMaster with an existing ID
        groupMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGroupMasterMockMvc.perform(post("/api/group-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(groupMaster)))
            .andExpect(status().isBadRequest());

        // Validate the GroupMaster in the database
        List<GroupMaster> groupMasterList = groupMasterRepository.findAll();
        assertThat(groupMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkGroupIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = groupMasterRepository.findAll().size();
        // set the field null
        groupMaster.setGroupId(null);

        // Create the GroupMaster, which fails.


        restGroupMasterMockMvc.perform(post("/api/group-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(groupMaster)))
            .andExpect(status().isBadRequest());

        List<GroupMaster> groupMasterList = groupMasterRepository.findAll();
        assertThat(groupMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGroupMasters() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList
        restGroupMasterMockMvc.perform(get("/api/group-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(groupMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getGroupMaster() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get the groupMaster
        restGroupMasterMockMvc.perform(get("/api/group-masters/{id}", groupMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(groupMaster.getId().intValue()))
            .andExpect(jsonPath("$.groupId").value(DEFAULT_GROUP_ID))
            .andExpect(jsonPath("$.groupName").value(DEFAULT_GROUP_NAME))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getGroupMastersByIdFiltering() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        Long id = groupMaster.getId();

        defaultGroupMasterShouldBeFound("id.equals=" + id);
        defaultGroupMasterShouldNotBeFound("id.notEquals=" + id);

        defaultGroupMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultGroupMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultGroupMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultGroupMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId equals to DEFAULT_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.equals=" + DEFAULT_GROUP_ID);

        // Get all the groupMasterList where groupId equals to UPDATED_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.equals=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId not equals to DEFAULT_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.notEquals=" + DEFAULT_GROUP_ID);

        // Get all the groupMasterList where groupId not equals to UPDATED_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.notEquals=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId in DEFAULT_GROUP_ID or UPDATED_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.in=" + DEFAULT_GROUP_ID + "," + UPDATED_GROUP_ID);

        // Get all the groupMasterList where groupId equals to UPDATED_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.in=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId is not null
        defaultGroupMasterShouldBeFound("groupId.specified=true");

        // Get all the groupMasterList where groupId is null
        defaultGroupMasterShouldNotBeFound("groupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId is greater than or equal to DEFAULT_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.greaterThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the groupMasterList where groupId is greater than or equal to UPDATED_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.greaterThanOrEqual=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId is less than or equal to DEFAULT_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.lessThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the groupMasterList where groupId is less than or equal to SMALLER_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.lessThanOrEqual=" + SMALLER_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsLessThanSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId is less than DEFAULT_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.lessThan=" + DEFAULT_GROUP_ID);

        // Get all the groupMasterList where groupId is less than UPDATED_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.lessThan=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupId is greater than DEFAULT_GROUP_ID
        defaultGroupMasterShouldNotBeFound("groupId.greaterThan=" + DEFAULT_GROUP_ID);

        // Get all the groupMasterList where groupId is greater than SMALLER_GROUP_ID
        defaultGroupMasterShouldBeFound("groupId.greaterThan=" + SMALLER_GROUP_ID);
    }


    @Test
    @Transactional
    public void getAllGroupMastersByGroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupName equals to DEFAULT_GROUP_NAME
        defaultGroupMasterShouldBeFound("groupName.equals=" + DEFAULT_GROUP_NAME);

        // Get all the groupMasterList where groupName equals to UPDATED_GROUP_NAME
        defaultGroupMasterShouldNotBeFound("groupName.equals=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupName not equals to DEFAULT_GROUP_NAME
        defaultGroupMasterShouldNotBeFound("groupName.notEquals=" + DEFAULT_GROUP_NAME);

        // Get all the groupMasterList where groupName not equals to UPDATED_GROUP_NAME
        defaultGroupMasterShouldBeFound("groupName.notEquals=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupName in DEFAULT_GROUP_NAME or UPDATED_GROUP_NAME
        defaultGroupMasterShouldBeFound("groupName.in=" + DEFAULT_GROUP_NAME + "," + UPDATED_GROUP_NAME);

        // Get all the groupMasterList where groupName equals to UPDATED_GROUP_NAME
        defaultGroupMasterShouldNotBeFound("groupName.in=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupName is not null
        defaultGroupMasterShouldBeFound("groupName.specified=true");

        // Get all the groupMasterList where groupName is null
        defaultGroupMasterShouldNotBeFound("groupName.specified=false");
    }
                @Test
    @Transactional
    public void getAllGroupMastersByGroupNameContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupName contains DEFAULT_GROUP_NAME
        defaultGroupMasterShouldBeFound("groupName.contains=" + DEFAULT_GROUP_NAME);

        // Get all the groupMasterList where groupName contains UPDATED_GROUP_NAME
        defaultGroupMasterShouldNotBeFound("groupName.contains=" + UPDATED_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByGroupNameNotContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where groupName does not contain DEFAULT_GROUP_NAME
        defaultGroupMasterShouldNotBeFound("groupName.doesNotContain=" + DEFAULT_GROUP_NAME);

        // Get all the groupMasterList where groupName does not contain UPDATED_GROUP_NAME
        defaultGroupMasterShouldBeFound("groupName.doesNotContain=" + UPDATED_GROUP_NAME);
    }


    @Test
    @Transactional
    public void getAllGroupMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultGroupMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the groupMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultGroupMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultGroupMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the groupMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultGroupMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultGroupMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the groupMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultGroupMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdDate is not null
        defaultGroupMasterShouldBeFound("createdDate.specified=true");

        // Get all the groupMasterList where createdDate is null
        defaultGroupMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultGroupMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the groupMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultGroupMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultGroupMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the groupMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultGroupMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultGroupMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the groupMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultGroupMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedDate is not null
        defaultGroupMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the groupMasterList where lastModifiedDate is null
        defaultGroupMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultGroupMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the groupMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultGroupMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultGroupMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the groupMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultGroupMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultGroupMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the groupMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultGroupMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdBy is not null
        defaultGroupMasterShouldBeFound("createdBy.specified=true");

        // Get all the groupMasterList where createdBy is null
        defaultGroupMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllGroupMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultGroupMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the groupMasterList where createdBy contains UPDATED_CREATED_BY
        defaultGroupMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultGroupMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the groupMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultGroupMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultGroupMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the groupMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultGroupMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultGroupMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the groupMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultGroupMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultGroupMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the groupMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultGroupMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedBy is not null
        defaultGroupMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the groupMasterList where lastModifiedBy is null
        defaultGroupMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultGroupMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the groupMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultGroupMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultGroupMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the groupMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultGroupMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllGroupMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultGroupMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the groupMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultGroupMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultGroupMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the groupMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultGroupMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultGroupMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the groupMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultGroupMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where isDeleted is not null
        defaultGroupMasterShouldBeFound("isDeleted.specified=true");

        // Get all the groupMasterList where isDeleted is null
        defaultGroupMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllGroupMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultGroupMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the groupMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultGroupMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllGroupMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);

        // Get all the groupMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultGroupMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the groupMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultGroupMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllGroupMastersByOnesourceCompanyMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        groupMasterRepository.saveAndFlush(groupMaster);
        OnesourceCompanyMaster onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(onesourceCompanyMaster);
        em.flush();
        groupMaster.addOnesourceCompanyMaster(onesourceCompanyMaster);
        groupMasterRepository.saveAndFlush(groupMaster);
        Long onesourceCompanyMasterId = onesourceCompanyMaster.getId();

        // Get all the groupMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId
        defaultGroupMasterShouldBeFound("onesourceCompanyMasterId.equals=" + onesourceCompanyMasterId);

        // Get all the groupMasterList where onesourceCompanyMaster equals to onesourceCompanyMasterId + 1
        defaultGroupMasterShouldNotBeFound("onesourceCompanyMasterId.equals=" + (onesourceCompanyMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultGroupMasterShouldBeFound(String filter) throws Exception {
        restGroupMasterMockMvc.perform(get("/api/group-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(groupMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restGroupMasterMockMvc.perform(get("/api/group-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultGroupMasterShouldNotBeFound(String filter) throws Exception {
        restGroupMasterMockMvc.perform(get("/api/group-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restGroupMasterMockMvc.perform(get("/api/group-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingGroupMaster() throws Exception {
        // Get the groupMaster
        restGroupMasterMockMvc.perform(get("/api/group-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGroupMaster() throws Exception {
        // Initialize the database
        groupMasterService.save(groupMaster);

        int databaseSizeBeforeUpdate = groupMasterRepository.findAll().size();

        // Update the groupMaster
        GroupMaster updatedGroupMaster = groupMasterRepository.findById(groupMaster.getId()).get();
        // Disconnect from session so that the updates on updatedGroupMaster are not directly saved in db
        em.detach(updatedGroupMaster);
        updatedGroupMaster
            .groupId(UPDATED_GROUP_ID)
            .groupName(UPDATED_GROUP_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED);

        restGroupMasterMockMvc.perform(put("/api/group-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedGroupMaster)))
            .andExpect(status().isOk());

        // Validate the GroupMaster in the database
        List<GroupMaster> groupMasterList = groupMasterRepository.findAll();
        assertThat(groupMasterList).hasSize(databaseSizeBeforeUpdate);
        GroupMaster testGroupMaster = groupMasterList.get(groupMasterList.size() - 1);
        assertThat(testGroupMaster.getGroupId()).isEqualTo(UPDATED_GROUP_ID);
        assertThat(testGroupMaster.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testGroupMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testGroupMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testGroupMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testGroupMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testGroupMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingGroupMaster() throws Exception {
        int databaseSizeBeforeUpdate = groupMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGroupMasterMockMvc.perform(put("/api/group-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(groupMaster)))
            .andExpect(status().isBadRequest());

        // Validate the GroupMaster in the database
        List<GroupMaster> groupMasterList = groupMasterRepository.findAll();
        assertThat(groupMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGroupMaster() throws Exception {
        // Initialize the database
        groupMasterService.save(groupMaster);

        int databaseSizeBeforeDelete = groupMasterRepository.findAll().size();

        // Delete the groupMaster
        restGroupMasterMockMvc.perform(delete("/api/group-masters/{id}", groupMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<GroupMaster> groupMasterList = groupMasterRepository.findAll();
        assertThat(groupMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
