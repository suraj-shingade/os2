package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyIsinMapping;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.CompanyIsinMappingRepository;
import com.crisil.onesource.service.CompanyIsinMappingService;
import com.crisil.onesource.service.dto.CompanyIsinMappingCriteria;
import com.crisil.onesource.service.CompanyIsinMappingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyIsinMappingResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyIsinMappingResourceIT {

    private static final String DEFAULT_ISIN_NO = "AAAAAAAAAA";
    private static final String UPDATED_ISIN_NO = "BBBBBBBBBB";

    private static final String DEFAULT_INSTRUMENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_INSTRUMENT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_INSTRUMENT_SUB_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_INSTRUMENT_SUB_TYPE = "BBBBBBBBBB";

    private static final Integer DEFAULT_FACE_VALUE = 1;
    private static final Integer UPDATED_FACE_VALUE = 2;
    private static final Integer SMALLER_FACE_VALUE = 1 - 1;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_SCRIPT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SCRIPT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SCRIPT_SOURCE = "AAAAA";
    private static final String UPDATED_SCRIPT_SOURCE = "BBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private CompanyIsinMappingRepository companyIsinMappingRepository;

    @Autowired
    private CompanyIsinMappingService companyIsinMappingService;

    @Autowired
    private CompanyIsinMappingQueryService companyIsinMappingQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyIsinMappingMockMvc;

    private CompanyIsinMapping companyIsinMapping;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyIsinMapping createEntity(EntityManager em) {
        CompanyIsinMapping companyIsinMapping = new CompanyIsinMapping()
            .isinNo(DEFAULT_ISIN_NO)
            .instrumentType(DEFAULT_INSTRUMENT_TYPE)
            .instrumentSubType(DEFAULT_INSTRUMENT_SUB_TYPE)
            .faceValue(DEFAULT_FACE_VALUE)
            .status(DEFAULT_STATUS)
            .scriptCode(DEFAULT_SCRIPT_CODE)
            .scriptSource(DEFAULT_SCRIPT_SOURCE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED);
        // Add required entity
        OnesourceCompanyMaster onesourceCompanyMaster;
        if (TestUtil.findAll(em, OnesourceCompanyMaster.class).isEmpty()) {
            onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
            em.persist(onesourceCompanyMaster);
            em.flush();
        } else {
            onesourceCompanyMaster = TestUtil.findAll(em, OnesourceCompanyMaster.class).get(0);
        }
        companyIsinMapping.setCompanyCode(onesourceCompanyMaster);
        return companyIsinMapping;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyIsinMapping createUpdatedEntity(EntityManager em) {
        CompanyIsinMapping companyIsinMapping = new CompanyIsinMapping()
            .isinNo(UPDATED_ISIN_NO)
            .instrumentType(UPDATED_INSTRUMENT_TYPE)
            .instrumentSubType(UPDATED_INSTRUMENT_SUB_TYPE)
            .faceValue(UPDATED_FACE_VALUE)
            .status(UPDATED_STATUS)
            .scriptCode(UPDATED_SCRIPT_CODE)
            .scriptSource(UPDATED_SCRIPT_SOURCE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);
        // Add required entity
        OnesourceCompanyMaster onesourceCompanyMaster;
        if (TestUtil.findAll(em, OnesourceCompanyMaster.class).isEmpty()) {
            onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createUpdatedEntity(em);
            em.persist(onesourceCompanyMaster);
            em.flush();
        } else {
            onesourceCompanyMaster = TestUtil.findAll(em, OnesourceCompanyMaster.class).get(0);
        }
        companyIsinMapping.setCompanyCode(onesourceCompanyMaster);
        return companyIsinMapping;
    }

    @BeforeEach
    public void initTest() {
        companyIsinMapping = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyIsinMapping() throws Exception {
        int databaseSizeBeforeCreate = companyIsinMappingRepository.findAll().size();
        // Create the CompanyIsinMapping
        restCompanyIsinMappingMockMvc.perform(post("/api/company-isin-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyIsinMapping)))
            .andExpect(status().isCreated());

        // Validate the CompanyIsinMapping in the database
        List<CompanyIsinMapping> companyIsinMappingList = companyIsinMappingRepository.findAll();
        assertThat(companyIsinMappingList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyIsinMapping testCompanyIsinMapping = companyIsinMappingList.get(companyIsinMappingList.size() - 1);
        assertThat(testCompanyIsinMapping.getIsinNo()).isEqualTo(DEFAULT_ISIN_NO);
        assertThat(testCompanyIsinMapping.getInstrumentType()).isEqualTo(DEFAULT_INSTRUMENT_TYPE);
        assertThat(testCompanyIsinMapping.getInstrumentSubType()).isEqualTo(DEFAULT_INSTRUMENT_SUB_TYPE);
        assertThat(testCompanyIsinMapping.getFaceValue()).isEqualTo(DEFAULT_FACE_VALUE);
        assertThat(testCompanyIsinMapping.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCompanyIsinMapping.getScriptCode()).isEqualTo(DEFAULT_SCRIPT_CODE);
        assertThat(testCompanyIsinMapping.getScriptSource()).isEqualTo(DEFAULT_SCRIPT_SOURCE);
        assertThat(testCompanyIsinMapping.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyIsinMapping.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyIsinMapping.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanyIsinMapping.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyIsinMapping.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createCompanyIsinMappingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyIsinMappingRepository.findAll().size();

        // Create the CompanyIsinMapping with an existing ID
        companyIsinMapping.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyIsinMappingMockMvc.perform(post("/api/company-isin-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyIsinMapping)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyIsinMapping in the database
        List<CompanyIsinMapping> companyIsinMappingList = companyIsinMappingRepository.findAll();
        assertThat(companyIsinMappingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkIsinNoIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyIsinMappingRepository.findAll().size();
        // set the field null
        companyIsinMapping.setIsinNo(null);

        // Create the CompanyIsinMapping, which fails.


        restCompanyIsinMappingMockMvc.perform(post("/api/company-isin-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyIsinMapping)))
            .andExpect(status().isBadRequest());

        List<CompanyIsinMapping> companyIsinMappingList = companyIsinMappingRepository.findAll();
        assertThat(companyIsinMappingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappings() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyIsinMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].isinNo").value(hasItem(DEFAULT_ISIN_NO)))
            .andExpect(jsonPath("$.[*].instrumentType").value(hasItem(DEFAULT_INSTRUMENT_TYPE)))
            .andExpect(jsonPath("$.[*].instrumentSubType").value(hasItem(DEFAULT_INSTRUMENT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].faceValue").value(hasItem(DEFAULT_FACE_VALUE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].scriptCode").value(hasItem(DEFAULT_SCRIPT_CODE)))
            .andExpect(jsonPath("$.[*].scriptSource").value(hasItem(DEFAULT_SCRIPT_SOURCE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getCompanyIsinMapping() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get the companyIsinMapping
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings/{id}", companyIsinMapping.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyIsinMapping.getId().intValue()))
            .andExpect(jsonPath("$.isinNo").value(DEFAULT_ISIN_NO))
            .andExpect(jsonPath("$.instrumentType").value(DEFAULT_INSTRUMENT_TYPE))
            .andExpect(jsonPath("$.instrumentSubType").value(DEFAULT_INSTRUMENT_SUB_TYPE))
            .andExpect(jsonPath("$.faceValue").value(DEFAULT_FACE_VALUE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.scriptCode").value(DEFAULT_SCRIPT_CODE))
            .andExpect(jsonPath("$.scriptSource").value(DEFAULT_SCRIPT_SOURCE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getCompanyIsinMappingsByIdFiltering() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        Long id = companyIsinMapping.getId();

        defaultCompanyIsinMappingShouldBeFound("id.equals=" + id);
        defaultCompanyIsinMappingShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyIsinMappingShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyIsinMappingShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyIsinMappingShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyIsinMappingShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsinNoIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isinNo equals to DEFAULT_ISIN_NO
        defaultCompanyIsinMappingShouldBeFound("isinNo.equals=" + DEFAULT_ISIN_NO);

        // Get all the companyIsinMappingList where isinNo equals to UPDATED_ISIN_NO
        defaultCompanyIsinMappingShouldNotBeFound("isinNo.equals=" + UPDATED_ISIN_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsinNoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isinNo not equals to DEFAULT_ISIN_NO
        defaultCompanyIsinMappingShouldNotBeFound("isinNo.notEquals=" + DEFAULT_ISIN_NO);

        // Get all the companyIsinMappingList where isinNo not equals to UPDATED_ISIN_NO
        defaultCompanyIsinMappingShouldBeFound("isinNo.notEquals=" + UPDATED_ISIN_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsinNoIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isinNo in DEFAULT_ISIN_NO or UPDATED_ISIN_NO
        defaultCompanyIsinMappingShouldBeFound("isinNo.in=" + DEFAULT_ISIN_NO + "," + UPDATED_ISIN_NO);

        // Get all the companyIsinMappingList where isinNo equals to UPDATED_ISIN_NO
        defaultCompanyIsinMappingShouldNotBeFound("isinNo.in=" + UPDATED_ISIN_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsinNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isinNo is not null
        defaultCompanyIsinMappingShouldBeFound("isinNo.specified=true");

        // Get all the companyIsinMappingList where isinNo is null
        defaultCompanyIsinMappingShouldNotBeFound("isinNo.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsinNoContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isinNo contains DEFAULT_ISIN_NO
        defaultCompanyIsinMappingShouldBeFound("isinNo.contains=" + DEFAULT_ISIN_NO);

        // Get all the companyIsinMappingList where isinNo contains UPDATED_ISIN_NO
        defaultCompanyIsinMappingShouldNotBeFound("isinNo.contains=" + UPDATED_ISIN_NO);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsinNoNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isinNo does not contain DEFAULT_ISIN_NO
        defaultCompanyIsinMappingShouldNotBeFound("isinNo.doesNotContain=" + DEFAULT_ISIN_NO);

        // Get all the companyIsinMappingList where isinNo does not contain UPDATED_ISIN_NO
        defaultCompanyIsinMappingShouldBeFound("isinNo.doesNotContain=" + UPDATED_ISIN_NO);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentType equals to DEFAULT_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentType.equals=" + DEFAULT_INSTRUMENT_TYPE);

        // Get all the companyIsinMappingList where instrumentType equals to UPDATED_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentType.equals=" + UPDATED_INSTRUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentType not equals to DEFAULT_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentType.notEquals=" + DEFAULT_INSTRUMENT_TYPE);

        // Get all the companyIsinMappingList where instrumentType not equals to UPDATED_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentType.notEquals=" + UPDATED_INSTRUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentTypeIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentType in DEFAULT_INSTRUMENT_TYPE or UPDATED_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentType.in=" + DEFAULT_INSTRUMENT_TYPE + "," + UPDATED_INSTRUMENT_TYPE);

        // Get all the companyIsinMappingList where instrumentType equals to UPDATED_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentType.in=" + UPDATED_INSTRUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentType is not null
        defaultCompanyIsinMappingShouldBeFound("instrumentType.specified=true");

        // Get all the companyIsinMappingList where instrumentType is null
        defaultCompanyIsinMappingShouldNotBeFound("instrumentType.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentTypeContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentType contains DEFAULT_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentType.contains=" + DEFAULT_INSTRUMENT_TYPE);

        // Get all the companyIsinMappingList where instrumentType contains UPDATED_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentType.contains=" + UPDATED_INSTRUMENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentTypeNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentType does not contain DEFAULT_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentType.doesNotContain=" + DEFAULT_INSTRUMENT_TYPE);

        // Get all the companyIsinMappingList where instrumentType does not contain UPDATED_INSTRUMENT_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentType.doesNotContain=" + UPDATED_INSTRUMENT_TYPE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentSubTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentSubType equals to DEFAULT_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentSubType.equals=" + DEFAULT_INSTRUMENT_SUB_TYPE);

        // Get all the companyIsinMappingList where instrumentSubType equals to UPDATED_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentSubType.equals=" + UPDATED_INSTRUMENT_SUB_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentSubTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentSubType not equals to DEFAULT_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentSubType.notEquals=" + DEFAULT_INSTRUMENT_SUB_TYPE);

        // Get all the companyIsinMappingList where instrumentSubType not equals to UPDATED_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentSubType.notEquals=" + UPDATED_INSTRUMENT_SUB_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentSubTypeIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentSubType in DEFAULT_INSTRUMENT_SUB_TYPE or UPDATED_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentSubType.in=" + DEFAULT_INSTRUMENT_SUB_TYPE + "," + UPDATED_INSTRUMENT_SUB_TYPE);

        // Get all the companyIsinMappingList where instrumentSubType equals to UPDATED_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentSubType.in=" + UPDATED_INSTRUMENT_SUB_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentSubTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentSubType is not null
        defaultCompanyIsinMappingShouldBeFound("instrumentSubType.specified=true");

        // Get all the companyIsinMappingList where instrumentSubType is null
        defaultCompanyIsinMappingShouldNotBeFound("instrumentSubType.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentSubTypeContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentSubType contains DEFAULT_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentSubType.contains=" + DEFAULT_INSTRUMENT_SUB_TYPE);

        // Get all the companyIsinMappingList where instrumentSubType contains UPDATED_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentSubType.contains=" + UPDATED_INSTRUMENT_SUB_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByInstrumentSubTypeNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where instrumentSubType does not contain DEFAULT_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldNotBeFound("instrumentSubType.doesNotContain=" + DEFAULT_INSTRUMENT_SUB_TYPE);

        // Get all the companyIsinMappingList where instrumentSubType does not contain UPDATED_INSTRUMENT_SUB_TYPE
        defaultCompanyIsinMappingShouldBeFound("instrumentSubType.doesNotContain=" + UPDATED_INSTRUMENT_SUB_TYPE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue equals to DEFAULT_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.equals=" + DEFAULT_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue equals to UPDATED_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.equals=" + UPDATED_FACE_VALUE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue not equals to DEFAULT_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.notEquals=" + DEFAULT_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue not equals to UPDATED_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.notEquals=" + UPDATED_FACE_VALUE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue in DEFAULT_FACE_VALUE or UPDATED_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.in=" + DEFAULT_FACE_VALUE + "," + UPDATED_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue equals to UPDATED_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.in=" + UPDATED_FACE_VALUE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue is not null
        defaultCompanyIsinMappingShouldBeFound("faceValue.specified=true");

        // Get all the companyIsinMappingList where faceValue is null
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue is greater than or equal to DEFAULT_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.greaterThanOrEqual=" + DEFAULT_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue is greater than or equal to UPDATED_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.greaterThanOrEqual=" + UPDATED_FACE_VALUE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue is less than or equal to DEFAULT_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.lessThanOrEqual=" + DEFAULT_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue is less than or equal to SMALLER_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.lessThanOrEqual=" + SMALLER_FACE_VALUE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsLessThanSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue is less than DEFAULT_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.lessThan=" + DEFAULT_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue is less than UPDATED_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.lessThan=" + UPDATED_FACE_VALUE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByFaceValueIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where faceValue is greater than DEFAULT_FACE_VALUE
        defaultCompanyIsinMappingShouldNotBeFound("faceValue.greaterThan=" + DEFAULT_FACE_VALUE);

        // Get all the companyIsinMappingList where faceValue is greater than SMALLER_FACE_VALUE
        defaultCompanyIsinMappingShouldBeFound("faceValue.greaterThan=" + SMALLER_FACE_VALUE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where status equals to DEFAULT_STATUS
        defaultCompanyIsinMappingShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the companyIsinMappingList where status equals to UPDATED_STATUS
        defaultCompanyIsinMappingShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where status not equals to DEFAULT_STATUS
        defaultCompanyIsinMappingShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the companyIsinMappingList where status not equals to UPDATED_STATUS
        defaultCompanyIsinMappingShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCompanyIsinMappingShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the companyIsinMappingList where status equals to UPDATED_STATUS
        defaultCompanyIsinMappingShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where status is not null
        defaultCompanyIsinMappingShouldBeFound("status.specified=true");

        // Get all the companyIsinMappingList where status is null
        defaultCompanyIsinMappingShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByStatusContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where status contains DEFAULT_STATUS
        defaultCompanyIsinMappingShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the companyIsinMappingList where status contains UPDATED_STATUS
        defaultCompanyIsinMappingShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where status does not contain DEFAULT_STATUS
        defaultCompanyIsinMappingShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the companyIsinMappingList where status does not contain UPDATED_STATUS
        defaultCompanyIsinMappingShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptCode equals to DEFAULT_SCRIPT_CODE
        defaultCompanyIsinMappingShouldBeFound("scriptCode.equals=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyIsinMappingList where scriptCode equals to UPDATED_SCRIPT_CODE
        defaultCompanyIsinMappingShouldNotBeFound("scriptCode.equals=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptCode not equals to DEFAULT_SCRIPT_CODE
        defaultCompanyIsinMappingShouldNotBeFound("scriptCode.notEquals=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyIsinMappingList where scriptCode not equals to UPDATED_SCRIPT_CODE
        defaultCompanyIsinMappingShouldBeFound("scriptCode.notEquals=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptCode in DEFAULT_SCRIPT_CODE or UPDATED_SCRIPT_CODE
        defaultCompanyIsinMappingShouldBeFound("scriptCode.in=" + DEFAULT_SCRIPT_CODE + "," + UPDATED_SCRIPT_CODE);

        // Get all the companyIsinMappingList where scriptCode equals to UPDATED_SCRIPT_CODE
        defaultCompanyIsinMappingShouldNotBeFound("scriptCode.in=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptCode is not null
        defaultCompanyIsinMappingShouldBeFound("scriptCode.specified=true");

        // Get all the companyIsinMappingList where scriptCode is null
        defaultCompanyIsinMappingShouldNotBeFound("scriptCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptCodeContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptCode contains DEFAULT_SCRIPT_CODE
        defaultCompanyIsinMappingShouldBeFound("scriptCode.contains=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyIsinMappingList where scriptCode contains UPDATED_SCRIPT_CODE
        defaultCompanyIsinMappingShouldNotBeFound("scriptCode.contains=" + UPDATED_SCRIPT_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptCode does not contain DEFAULT_SCRIPT_CODE
        defaultCompanyIsinMappingShouldNotBeFound("scriptCode.doesNotContain=" + DEFAULT_SCRIPT_CODE);

        // Get all the companyIsinMappingList where scriptCode does not contain UPDATED_SCRIPT_CODE
        defaultCompanyIsinMappingShouldBeFound("scriptCode.doesNotContain=" + UPDATED_SCRIPT_CODE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptSourceIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptSource equals to DEFAULT_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldBeFound("scriptSource.equals=" + DEFAULT_SCRIPT_SOURCE);

        // Get all the companyIsinMappingList where scriptSource equals to UPDATED_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldNotBeFound("scriptSource.equals=" + UPDATED_SCRIPT_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptSourceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptSource not equals to DEFAULT_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldNotBeFound("scriptSource.notEquals=" + DEFAULT_SCRIPT_SOURCE);

        // Get all the companyIsinMappingList where scriptSource not equals to UPDATED_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldBeFound("scriptSource.notEquals=" + UPDATED_SCRIPT_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptSourceIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptSource in DEFAULT_SCRIPT_SOURCE or UPDATED_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldBeFound("scriptSource.in=" + DEFAULT_SCRIPT_SOURCE + "," + UPDATED_SCRIPT_SOURCE);

        // Get all the companyIsinMappingList where scriptSource equals to UPDATED_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldNotBeFound("scriptSource.in=" + UPDATED_SCRIPT_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptSourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptSource is not null
        defaultCompanyIsinMappingShouldBeFound("scriptSource.specified=true");

        // Get all the companyIsinMappingList where scriptSource is null
        defaultCompanyIsinMappingShouldNotBeFound("scriptSource.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptSourceContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptSource contains DEFAULT_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldBeFound("scriptSource.contains=" + DEFAULT_SCRIPT_SOURCE);

        // Get all the companyIsinMappingList where scriptSource contains UPDATED_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldNotBeFound("scriptSource.contains=" + UPDATED_SCRIPT_SOURCE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByScriptSourceNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where scriptSource does not contain DEFAULT_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldNotBeFound("scriptSource.doesNotContain=" + DEFAULT_SCRIPT_SOURCE);

        // Get all the companyIsinMappingList where scriptSource does not contain UPDATED_SCRIPT_SOURCE
        defaultCompanyIsinMappingShouldBeFound("scriptSource.doesNotContain=" + UPDATED_SCRIPT_SOURCE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyIsinMappingShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyIsinMappingList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyIsinMappingShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyIsinMappingShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyIsinMappingList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyIsinMappingShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyIsinMappingShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyIsinMappingList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyIsinMappingShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdBy is not null
        defaultCompanyIsinMappingShouldBeFound("createdBy.specified=true");

        // Get all the companyIsinMappingList where createdBy is null
        defaultCompanyIsinMappingShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyIsinMappingShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyIsinMappingList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyIsinMappingShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyIsinMappingShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyIsinMappingList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyIsinMappingShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate is not null
        defaultCompanyIsinMappingShouldBeFound("createdDate.specified=true");

        // Get all the companyIsinMappingList where createdDate is null
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyIsinMappingList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompanyIsinMappingShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyIsinMappingList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyIsinMappingList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyIsinMappingList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedBy is not null
        defaultCompanyIsinMappingShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyIsinMappingList where lastModifiedBy is null
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyIsinMappingList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyIsinMappingList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyIsinMappingShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate is not null
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyIsinMappingList where lastModifiedDate is null
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyIsinMappingList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompanyIsinMappingShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyIsinMappingShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyIsinMappingList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyIsinMappingShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyIsinMappingShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyIsinMappingList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyIsinMappingShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyIsinMappingShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyIsinMappingList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyIsinMappingShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isDeleted is not null
        defaultCompanyIsinMappingShouldBeFound("isDeleted.specified=true");

        // Get all the companyIsinMappingList where isDeleted is null
        defaultCompanyIsinMappingShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyIsinMappingShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyIsinMappingList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyIsinMappingShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);

        // Get all the companyIsinMappingList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyIsinMappingShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyIsinMappingList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyIsinMappingShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyIsinMappingsByCompanyCodeIsEqualToSomething() throws Exception {
        // Get already existing entity
        OnesourceCompanyMaster companyCode = companyIsinMapping.getCompanyCode();
        companyIsinMappingRepository.saveAndFlush(companyIsinMapping);
        Long companyCodeId = companyCode.getId();

        // Get all the companyIsinMappingList where companyCode equals to companyCodeId
        defaultCompanyIsinMappingShouldBeFound("companyCodeId.equals=" + companyCodeId);

        // Get all the companyIsinMappingList where companyCode equals to companyCodeId + 1
        defaultCompanyIsinMappingShouldNotBeFound("companyCodeId.equals=" + (companyCodeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyIsinMappingShouldBeFound(String filter) throws Exception {
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyIsinMapping.getId().intValue())))
            .andExpect(jsonPath("$.[*].isinNo").value(hasItem(DEFAULT_ISIN_NO)))
            .andExpect(jsonPath("$.[*].instrumentType").value(hasItem(DEFAULT_INSTRUMENT_TYPE)))
            .andExpect(jsonPath("$.[*].instrumentSubType").value(hasItem(DEFAULT_INSTRUMENT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].faceValue").value(hasItem(DEFAULT_FACE_VALUE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].scriptCode").value(hasItem(DEFAULT_SCRIPT_CODE)))
            .andExpect(jsonPath("$.[*].scriptSource").value(hasItem(DEFAULT_SCRIPT_SOURCE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyIsinMappingShouldNotBeFound(String filter) throws Exception {
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyIsinMapping() throws Exception {
        // Get the companyIsinMapping
        restCompanyIsinMappingMockMvc.perform(get("/api/company-isin-mappings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyIsinMapping() throws Exception {
        // Initialize the database
        companyIsinMappingService.save(companyIsinMapping);

        int databaseSizeBeforeUpdate = companyIsinMappingRepository.findAll().size();

        // Update the companyIsinMapping
        CompanyIsinMapping updatedCompanyIsinMapping = companyIsinMappingRepository.findById(companyIsinMapping.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyIsinMapping are not directly saved in db
        em.detach(updatedCompanyIsinMapping);
        updatedCompanyIsinMapping
            .isinNo(UPDATED_ISIN_NO)
            .instrumentType(UPDATED_INSTRUMENT_TYPE)
            .instrumentSubType(UPDATED_INSTRUMENT_SUB_TYPE)
            .faceValue(UPDATED_FACE_VALUE)
            .status(UPDATED_STATUS)
            .scriptCode(UPDATED_SCRIPT_CODE)
            .scriptSource(UPDATED_SCRIPT_SOURCE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);

        restCompanyIsinMappingMockMvc.perform(put("/api/company-isin-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyIsinMapping)))
            .andExpect(status().isOk());

        // Validate the CompanyIsinMapping in the database
        List<CompanyIsinMapping> companyIsinMappingList = companyIsinMappingRepository.findAll();
        assertThat(companyIsinMappingList).hasSize(databaseSizeBeforeUpdate);
        CompanyIsinMapping testCompanyIsinMapping = companyIsinMappingList.get(companyIsinMappingList.size() - 1);
        assertThat(testCompanyIsinMapping.getIsinNo()).isEqualTo(UPDATED_ISIN_NO);
        assertThat(testCompanyIsinMapping.getInstrumentType()).isEqualTo(UPDATED_INSTRUMENT_TYPE);
        assertThat(testCompanyIsinMapping.getInstrumentSubType()).isEqualTo(UPDATED_INSTRUMENT_SUB_TYPE);
        assertThat(testCompanyIsinMapping.getFaceValue()).isEqualTo(UPDATED_FACE_VALUE);
        assertThat(testCompanyIsinMapping.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCompanyIsinMapping.getScriptCode()).isEqualTo(UPDATED_SCRIPT_CODE);
        assertThat(testCompanyIsinMapping.getScriptSource()).isEqualTo(UPDATED_SCRIPT_SOURCE);
        assertThat(testCompanyIsinMapping.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyIsinMapping.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyIsinMapping.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanyIsinMapping.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyIsinMapping.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyIsinMapping() throws Exception {
        int databaseSizeBeforeUpdate = companyIsinMappingRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyIsinMappingMockMvc.perform(put("/api/company-isin-mappings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyIsinMapping)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyIsinMapping in the database
        List<CompanyIsinMapping> companyIsinMappingList = companyIsinMappingRepository.findAll();
        assertThat(companyIsinMappingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyIsinMapping() throws Exception {
        // Initialize the database
        companyIsinMappingService.save(companyIsinMapping);

        int databaseSizeBeforeDelete = companyIsinMappingRepository.findAll().size();

        // Delete the companyIsinMapping
        restCompanyIsinMappingMockMvc.perform(delete("/api/company-isin-mappings/{id}", companyIsinMapping.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyIsinMapping> companyIsinMappingList = companyIsinMappingRepository.findAll();
        assertThat(companyIsinMappingList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
