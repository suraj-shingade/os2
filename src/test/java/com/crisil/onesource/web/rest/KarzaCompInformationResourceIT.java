package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.KarzaCompInformation;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.KarzaCompInformationRepository;
import com.crisil.onesource.service.KarzaCompInformationService;
import com.crisil.onesource.service.dto.KarzaCompInformationCriteria;
import com.crisil.onesource.service.KarzaCompInformationQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link KarzaCompInformationResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class KarzaCompInformationResourceIT {

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_NAME_KARZA = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME_KARZA = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_TYPEOFAPPROVAL = "AAAAAAAAAA";
    private static final String UPDATED_TYPEOFAPPROVAL = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_GSTIN = "AAAAAAAAAA";
    private static final String UPDATED_GSTIN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_IS_VALID_GST = "A";
    private static final String UPDATED_IS_VALID_GST = "B";

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final String DEFAULT_CHECKDIGIT = "AAAAAAAAAA";
    private static final String UPDATED_CHECKDIGIT = "BBBBBBBBBB";

    private static final String DEFAULT_TAXPAYER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TAXPAYER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NATUTE_OF_BUSS = "AAAAAAAAAA";
    private static final String UPDATED_NATUTE_OF_BUSS = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NO = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TRADE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_TRADE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CONSTIT_BUSINESS = "AAAAAAAAAA";
    private static final String UPDATED_CONSTIT_BUSINESS = "BBBBBBBBBB";

    @Autowired
    private KarzaCompInformationRepository karzaCompInformationRepository;

    @Autowired
    private KarzaCompInformationService karzaCompInformationService;

    @Autowired
    private KarzaCompInformationQueryService karzaCompInformationQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restKarzaCompInformationMockMvc;

    private KarzaCompInformation karzaCompInformation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KarzaCompInformation createEntity(EntityManager em) {
        KarzaCompInformation karzaCompInformation = new KarzaCompInformation()
            .pan(DEFAULT_PAN)
            .companyNameKarza(DEFAULT_COMPANY_NAME_KARZA)
            .address(DEFAULT_ADDRESS)
            .typeofapproval(DEFAULT_TYPEOFAPPROVAL)
            .status(DEFAULT_STATUS)
            .gstin(DEFAULT_GSTIN)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED)
            .state(DEFAULT_STATE)
            .isValidGst(DEFAULT_IS_VALID_GST)
            .source(DEFAULT_SOURCE)
            .comments(DEFAULT_COMMENTS)
            .checkdigit(DEFAULT_CHECKDIGIT)
            .taxpayerName(DEFAULT_TAXPAYER_NAME)
            .natuteOfBuss(DEFAULT_NATUTE_OF_BUSS)
            .mobileNo(DEFAULT_MOBILE_NO)
            .email(DEFAULT_EMAIL)
            .tradeName(DEFAULT_TRADE_NAME)
            .constitBusiness(DEFAULT_CONSTIT_BUSINESS);
        return karzaCompInformation;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static KarzaCompInformation createUpdatedEntity(EntityManager em) {
        KarzaCompInformation karzaCompInformation = new KarzaCompInformation()
            .pan(UPDATED_PAN)
            .companyNameKarza(UPDATED_COMPANY_NAME_KARZA)
            .address(UPDATED_ADDRESS)
            .typeofapproval(UPDATED_TYPEOFAPPROVAL)
            .status(UPDATED_STATUS)
            .gstin(UPDATED_GSTIN)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED)
            .state(UPDATED_STATE)
            .isValidGst(UPDATED_IS_VALID_GST)
            .source(UPDATED_SOURCE)
            .comments(UPDATED_COMMENTS)
            .checkdigit(UPDATED_CHECKDIGIT)
            .taxpayerName(UPDATED_TAXPAYER_NAME)
            .natuteOfBuss(UPDATED_NATUTE_OF_BUSS)
            .mobileNo(UPDATED_MOBILE_NO)
            .email(UPDATED_EMAIL)
            .tradeName(UPDATED_TRADE_NAME)
            .constitBusiness(UPDATED_CONSTIT_BUSINESS);
        return karzaCompInformation;
    }

    @BeforeEach
    public void initTest() {
        karzaCompInformation = createEntity(em);
    }

    @Test
    @Transactional
    public void createKarzaCompInformation() throws Exception {
        int databaseSizeBeforeCreate = karzaCompInformationRepository.findAll().size();
        // Create the KarzaCompInformation
        restKarzaCompInformationMockMvc.perform(post("/api/karza-comp-informations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(karzaCompInformation)))
            .andExpect(status().isCreated());

        // Validate the KarzaCompInformation in the database
        List<KarzaCompInformation> karzaCompInformationList = karzaCompInformationRepository.findAll();
        assertThat(karzaCompInformationList).hasSize(databaseSizeBeforeCreate + 1);
        KarzaCompInformation testKarzaCompInformation = karzaCompInformationList.get(karzaCompInformationList.size() - 1);
        assertThat(testKarzaCompInformation.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testKarzaCompInformation.getCompanyNameKarza()).isEqualTo(DEFAULT_COMPANY_NAME_KARZA);
        assertThat(testKarzaCompInformation.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testKarzaCompInformation.getTypeofapproval()).isEqualTo(DEFAULT_TYPEOFAPPROVAL);
        assertThat(testKarzaCompInformation.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testKarzaCompInformation.getGstin()).isEqualTo(DEFAULT_GSTIN);
        assertThat(testKarzaCompInformation.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testKarzaCompInformation.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testKarzaCompInformation.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testKarzaCompInformation.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testKarzaCompInformation.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testKarzaCompInformation.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testKarzaCompInformation.getIsValidGst()).isEqualTo(DEFAULT_IS_VALID_GST);
        assertThat(testKarzaCompInformation.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testKarzaCompInformation.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testKarzaCompInformation.getCheckdigit()).isEqualTo(DEFAULT_CHECKDIGIT);
        assertThat(testKarzaCompInformation.getTaxpayerName()).isEqualTo(DEFAULT_TAXPAYER_NAME);
        assertThat(testKarzaCompInformation.getNatuteOfBuss()).isEqualTo(DEFAULT_NATUTE_OF_BUSS);
        assertThat(testKarzaCompInformation.getMobileNo()).isEqualTo(DEFAULT_MOBILE_NO);
        assertThat(testKarzaCompInformation.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testKarzaCompInformation.getTradeName()).isEqualTo(DEFAULT_TRADE_NAME);
        assertThat(testKarzaCompInformation.getConstitBusiness()).isEqualTo(DEFAULT_CONSTIT_BUSINESS);
    }

    @Test
    @Transactional
    public void createKarzaCompInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = karzaCompInformationRepository.findAll().size();

        // Create the KarzaCompInformation with an existing ID
        karzaCompInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restKarzaCompInformationMockMvc.perform(post("/api/karza-comp-informations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(karzaCompInformation)))
            .andExpect(status().isBadRequest());

        // Validate the KarzaCompInformation in the database
        List<KarzaCompInformation> karzaCompInformationList = karzaCompInformationRepository.findAll();
        assertThat(karzaCompInformationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformations() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(karzaCompInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].companyNameKarza").value(hasItem(DEFAULT_COMPANY_NAME_KARZA)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].typeofapproval").value(hasItem(DEFAULT_TYPEOFAPPROVAL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].gstin").value(hasItem(DEFAULT_GSTIN)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].isValidGst").value(hasItem(DEFAULT_IS_VALID_GST)))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].checkdigit").value(hasItem(DEFAULT_CHECKDIGIT)))
            .andExpect(jsonPath("$.[*].taxpayerName").value(hasItem(DEFAULT_TAXPAYER_NAME)))
            .andExpect(jsonPath("$.[*].natuteOfBuss").value(hasItem(DEFAULT_NATUTE_OF_BUSS)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].tradeName").value(hasItem(DEFAULT_TRADE_NAME)))
            .andExpect(jsonPath("$.[*].constitBusiness").value(hasItem(DEFAULT_CONSTIT_BUSINESS)));
    }
    
    @Test
    @Transactional
    public void getKarzaCompInformation() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get the karzaCompInformation
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations/{id}", karzaCompInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(karzaCompInformation.getId().intValue()))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.companyNameKarza").value(DEFAULT_COMPANY_NAME_KARZA))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.typeofapproval").value(DEFAULT_TYPEOFAPPROVAL))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.gstin").value(DEFAULT_GSTIN))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.isValidGst").value(DEFAULT_IS_VALID_GST))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.checkdigit").value(DEFAULT_CHECKDIGIT))
            .andExpect(jsonPath("$.taxpayerName").value(DEFAULT_TAXPAYER_NAME))
            .andExpect(jsonPath("$.natuteOfBuss").value(DEFAULT_NATUTE_OF_BUSS))
            .andExpect(jsonPath("$.mobileNo").value(DEFAULT_MOBILE_NO))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.tradeName").value(DEFAULT_TRADE_NAME))
            .andExpect(jsonPath("$.constitBusiness").value(DEFAULT_CONSTIT_BUSINESS));
    }


    @Test
    @Transactional
    public void getKarzaCompInformationsByIdFiltering() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        Long id = karzaCompInformation.getId();

        defaultKarzaCompInformationShouldBeFound("id.equals=" + id);
        defaultKarzaCompInformationShouldNotBeFound("id.notEquals=" + id);

        defaultKarzaCompInformationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultKarzaCompInformationShouldNotBeFound("id.greaterThan=" + id);

        defaultKarzaCompInformationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultKarzaCompInformationShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByPanIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where pan equals to DEFAULT_PAN
        defaultKarzaCompInformationShouldBeFound("pan.equals=" + DEFAULT_PAN);

        // Get all the karzaCompInformationList where pan equals to UPDATED_PAN
        defaultKarzaCompInformationShouldNotBeFound("pan.equals=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByPanIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where pan not equals to DEFAULT_PAN
        defaultKarzaCompInformationShouldNotBeFound("pan.notEquals=" + DEFAULT_PAN);

        // Get all the karzaCompInformationList where pan not equals to UPDATED_PAN
        defaultKarzaCompInformationShouldBeFound("pan.notEquals=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByPanIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where pan in DEFAULT_PAN or UPDATED_PAN
        defaultKarzaCompInformationShouldBeFound("pan.in=" + DEFAULT_PAN + "," + UPDATED_PAN);

        // Get all the karzaCompInformationList where pan equals to UPDATED_PAN
        defaultKarzaCompInformationShouldNotBeFound("pan.in=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByPanIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where pan is not null
        defaultKarzaCompInformationShouldBeFound("pan.specified=true");

        // Get all the karzaCompInformationList where pan is null
        defaultKarzaCompInformationShouldNotBeFound("pan.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByPanContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where pan contains DEFAULT_PAN
        defaultKarzaCompInformationShouldBeFound("pan.contains=" + DEFAULT_PAN);

        // Get all the karzaCompInformationList where pan contains UPDATED_PAN
        defaultKarzaCompInformationShouldNotBeFound("pan.contains=" + UPDATED_PAN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByPanNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where pan does not contain DEFAULT_PAN
        defaultKarzaCompInformationShouldNotBeFound("pan.doesNotContain=" + DEFAULT_PAN);

        // Get all the karzaCompInformationList where pan does not contain UPDATED_PAN
        defaultKarzaCompInformationShouldBeFound("pan.doesNotContain=" + UPDATED_PAN);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyNameKarzaIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where companyNameKarza equals to DEFAULT_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldBeFound("companyNameKarza.equals=" + DEFAULT_COMPANY_NAME_KARZA);

        // Get all the karzaCompInformationList where companyNameKarza equals to UPDATED_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldNotBeFound("companyNameKarza.equals=" + UPDATED_COMPANY_NAME_KARZA);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyNameKarzaIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where companyNameKarza not equals to DEFAULT_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldNotBeFound("companyNameKarza.notEquals=" + DEFAULT_COMPANY_NAME_KARZA);

        // Get all the karzaCompInformationList where companyNameKarza not equals to UPDATED_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldBeFound("companyNameKarza.notEquals=" + UPDATED_COMPANY_NAME_KARZA);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyNameKarzaIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where companyNameKarza in DEFAULT_COMPANY_NAME_KARZA or UPDATED_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldBeFound("companyNameKarza.in=" + DEFAULT_COMPANY_NAME_KARZA + "," + UPDATED_COMPANY_NAME_KARZA);

        // Get all the karzaCompInformationList where companyNameKarza equals to UPDATED_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldNotBeFound("companyNameKarza.in=" + UPDATED_COMPANY_NAME_KARZA);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyNameKarzaIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where companyNameKarza is not null
        defaultKarzaCompInformationShouldBeFound("companyNameKarza.specified=true");

        // Get all the karzaCompInformationList where companyNameKarza is null
        defaultKarzaCompInformationShouldNotBeFound("companyNameKarza.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyNameKarzaContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where companyNameKarza contains DEFAULT_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldBeFound("companyNameKarza.contains=" + DEFAULT_COMPANY_NAME_KARZA);

        // Get all the karzaCompInformationList where companyNameKarza contains UPDATED_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldNotBeFound("companyNameKarza.contains=" + UPDATED_COMPANY_NAME_KARZA);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyNameKarzaNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where companyNameKarza does not contain DEFAULT_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldNotBeFound("companyNameKarza.doesNotContain=" + DEFAULT_COMPANY_NAME_KARZA);

        // Get all the karzaCompInformationList where companyNameKarza does not contain UPDATED_COMPANY_NAME_KARZA
        defaultKarzaCompInformationShouldBeFound("companyNameKarza.doesNotContain=" + UPDATED_COMPANY_NAME_KARZA);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where address equals to DEFAULT_ADDRESS
        defaultKarzaCompInformationShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the karzaCompInformationList where address equals to UPDATED_ADDRESS
        defaultKarzaCompInformationShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where address not equals to DEFAULT_ADDRESS
        defaultKarzaCompInformationShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS);

        // Get all the karzaCompInformationList where address not equals to UPDATED_ADDRESS
        defaultKarzaCompInformationShouldBeFound("address.notEquals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultKarzaCompInformationShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the karzaCompInformationList where address equals to UPDATED_ADDRESS
        defaultKarzaCompInformationShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where address is not null
        defaultKarzaCompInformationShouldBeFound("address.specified=true");

        // Get all the karzaCompInformationList where address is null
        defaultKarzaCompInformationShouldNotBeFound("address.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByAddressContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where address contains DEFAULT_ADDRESS
        defaultKarzaCompInformationShouldBeFound("address.contains=" + DEFAULT_ADDRESS);

        // Get all the karzaCompInformationList where address contains UPDATED_ADDRESS
        defaultKarzaCompInformationShouldNotBeFound("address.contains=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByAddressNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where address does not contain DEFAULT_ADDRESS
        defaultKarzaCompInformationShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS);

        // Get all the karzaCompInformationList where address does not contain UPDATED_ADDRESS
        defaultKarzaCompInformationShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTypeofapprovalIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where typeofapproval equals to DEFAULT_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldBeFound("typeofapproval.equals=" + DEFAULT_TYPEOFAPPROVAL);

        // Get all the karzaCompInformationList where typeofapproval equals to UPDATED_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldNotBeFound("typeofapproval.equals=" + UPDATED_TYPEOFAPPROVAL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTypeofapprovalIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where typeofapproval not equals to DEFAULT_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldNotBeFound("typeofapproval.notEquals=" + DEFAULT_TYPEOFAPPROVAL);

        // Get all the karzaCompInformationList where typeofapproval not equals to UPDATED_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldBeFound("typeofapproval.notEquals=" + UPDATED_TYPEOFAPPROVAL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTypeofapprovalIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where typeofapproval in DEFAULT_TYPEOFAPPROVAL or UPDATED_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldBeFound("typeofapproval.in=" + DEFAULT_TYPEOFAPPROVAL + "," + UPDATED_TYPEOFAPPROVAL);

        // Get all the karzaCompInformationList where typeofapproval equals to UPDATED_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldNotBeFound("typeofapproval.in=" + UPDATED_TYPEOFAPPROVAL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTypeofapprovalIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where typeofapproval is not null
        defaultKarzaCompInformationShouldBeFound("typeofapproval.specified=true");

        // Get all the karzaCompInformationList where typeofapproval is null
        defaultKarzaCompInformationShouldNotBeFound("typeofapproval.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByTypeofapprovalContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where typeofapproval contains DEFAULT_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldBeFound("typeofapproval.contains=" + DEFAULT_TYPEOFAPPROVAL);

        // Get all the karzaCompInformationList where typeofapproval contains UPDATED_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldNotBeFound("typeofapproval.contains=" + UPDATED_TYPEOFAPPROVAL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTypeofapprovalNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where typeofapproval does not contain DEFAULT_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldNotBeFound("typeofapproval.doesNotContain=" + DEFAULT_TYPEOFAPPROVAL);

        // Get all the karzaCompInformationList where typeofapproval does not contain UPDATED_TYPEOFAPPROVAL
        defaultKarzaCompInformationShouldBeFound("typeofapproval.doesNotContain=" + UPDATED_TYPEOFAPPROVAL);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where status equals to DEFAULT_STATUS
        defaultKarzaCompInformationShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the karzaCompInformationList where status equals to UPDATED_STATUS
        defaultKarzaCompInformationShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where status not equals to DEFAULT_STATUS
        defaultKarzaCompInformationShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the karzaCompInformationList where status not equals to UPDATED_STATUS
        defaultKarzaCompInformationShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultKarzaCompInformationShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the karzaCompInformationList where status equals to UPDATED_STATUS
        defaultKarzaCompInformationShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where status is not null
        defaultKarzaCompInformationShouldBeFound("status.specified=true");

        // Get all the karzaCompInformationList where status is null
        defaultKarzaCompInformationShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByStatusContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where status contains DEFAULT_STATUS
        defaultKarzaCompInformationShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the karzaCompInformationList where status contains UPDATED_STATUS
        defaultKarzaCompInformationShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where status does not contain DEFAULT_STATUS
        defaultKarzaCompInformationShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the karzaCompInformationList where status does not contain UPDATED_STATUS
        defaultKarzaCompInformationShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByGstinIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where gstin equals to DEFAULT_GSTIN
        defaultKarzaCompInformationShouldBeFound("gstin.equals=" + DEFAULT_GSTIN);

        // Get all the karzaCompInformationList where gstin equals to UPDATED_GSTIN
        defaultKarzaCompInformationShouldNotBeFound("gstin.equals=" + UPDATED_GSTIN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByGstinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where gstin not equals to DEFAULT_GSTIN
        defaultKarzaCompInformationShouldNotBeFound("gstin.notEquals=" + DEFAULT_GSTIN);

        // Get all the karzaCompInformationList where gstin not equals to UPDATED_GSTIN
        defaultKarzaCompInformationShouldBeFound("gstin.notEquals=" + UPDATED_GSTIN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByGstinIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where gstin in DEFAULT_GSTIN or UPDATED_GSTIN
        defaultKarzaCompInformationShouldBeFound("gstin.in=" + DEFAULT_GSTIN + "," + UPDATED_GSTIN);

        // Get all the karzaCompInformationList where gstin equals to UPDATED_GSTIN
        defaultKarzaCompInformationShouldNotBeFound("gstin.in=" + UPDATED_GSTIN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByGstinIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where gstin is not null
        defaultKarzaCompInformationShouldBeFound("gstin.specified=true");

        // Get all the karzaCompInformationList where gstin is null
        defaultKarzaCompInformationShouldNotBeFound("gstin.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByGstinContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where gstin contains DEFAULT_GSTIN
        defaultKarzaCompInformationShouldBeFound("gstin.contains=" + DEFAULT_GSTIN);

        // Get all the karzaCompInformationList where gstin contains UPDATED_GSTIN
        defaultKarzaCompInformationShouldNotBeFound("gstin.contains=" + UPDATED_GSTIN);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByGstinNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where gstin does not contain DEFAULT_GSTIN
        defaultKarzaCompInformationShouldNotBeFound("gstin.doesNotContain=" + DEFAULT_GSTIN);

        // Get all the karzaCompInformationList where gstin does not contain UPDATED_GSTIN
        defaultKarzaCompInformationShouldBeFound("gstin.doesNotContain=" + UPDATED_GSTIN);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate equals to DEFAULT_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate equals to UPDATED_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate not equals to UPDATED_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate equals to UPDATED_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate is not null
        defaultKarzaCompInformationShouldBeFound("createdDate.specified=true");

        // Get all the karzaCompInformationList where createdDate is null
        defaultKarzaCompInformationShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate is less than DEFAULT_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate is less than UPDATED_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultKarzaCompInformationShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the karzaCompInformationList where createdDate is greater than SMALLER_CREATED_DATE
        defaultKarzaCompInformationShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdBy equals to DEFAULT_CREATED_BY
        defaultKarzaCompInformationShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the karzaCompInformationList where createdBy equals to UPDATED_CREATED_BY
        defaultKarzaCompInformationShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdBy not equals to DEFAULT_CREATED_BY
        defaultKarzaCompInformationShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the karzaCompInformationList where createdBy not equals to UPDATED_CREATED_BY
        defaultKarzaCompInformationShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultKarzaCompInformationShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the karzaCompInformationList where createdBy equals to UPDATED_CREATED_BY
        defaultKarzaCompInformationShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdBy is not null
        defaultKarzaCompInformationShouldBeFound("createdBy.specified=true");

        // Get all the karzaCompInformationList where createdBy is null
        defaultKarzaCompInformationShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdBy contains DEFAULT_CREATED_BY
        defaultKarzaCompInformationShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the karzaCompInformationList where createdBy contains UPDATED_CREATED_BY
        defaultKarzaCompInformationShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where createdBy does not contain DEFAULT_CREATED_BY
        defaultKarzaCompInformationShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the karzaCompInformationList where createdBy does not contain UPDATED_CREATED_BY
        defaultKarzaCompInformationShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the karzaCompInformationList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the karzaCompInformationList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the karzaCompInformationList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedBy is not null
        defaultKarzaCompInformationShouldBeFound("lastModifiedBy.specified=true");

        // Get all the karzaCompInformationList where lastModifiedBy is null
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the karzaCompInformationList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the karzaCompInformationList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultKarzaCompInformationShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate is not null
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.specified=true");

        // Get all the karzaCompInformationList where lastModifiedDate is null
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the karzaCompInformationList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultKarzaCompInformationShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isDeleted equals to DEFAULT_IS_DELETED
        defaultKarzaCompInformationShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the karzaCompInformationList where isDeleted equals to UPDATED_IS_DELETED
        defaultKarzaCompInformationShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultKarzaCompInformationShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the karzaCompInformationList where isDeleted not equals to UPDATED_IS_DELETED
        defaultKarzaCompInformationShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultKarzaCompInformationShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the karzaCompInformationList where isDeleted equals to UPDATED_IS_DELETED
        defaultKarzaCompInformationShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isDeleted is not null
        defaultKarzaCompInformationShouldBeFound("isDeleted.specified=true");

        // Get all the karzaCompInformationList where isDeleted is null
        defaultKarzaCompInformationShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isDeleted contains DEFAULT_IS_DELETED
        defaultKarzaCompInformationShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the karzaCompInformationList where isDeleted contains UPDATED_IS_DELETED
        defaultKarzaCompInformationShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultKarzaCompInformationShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the karzaCompInformationList where isDeleted does not contain UPDATED_IS_DELETED
        defaultKarzaCompInformationShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where state equals to DEFAULT_STATE
        defaultKarzaCompInformationShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the karzaCompInformationList where state equals to UPDATED_STATE
        defaultKarzaCompInformationShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where state not equals to DEFAULT_STATE
        defaultKarzaCompInformationShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the karzaCompInformationList where state not equals to UPDATED_STATE
        defaultKarzaCompInformationShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStateIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where state in DEFAULT_STATE or UPDATED_STATE
        defaultKarzaCompInformationShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the karzaCompInformationList where state equals to UPDATED_STATE
        defaultKarzaCompInformationShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where state is not null
        defaultKarzaCompInformationShouldBeFound("state.specified=true");

        // Get all the karzaCompInformationList where state is null
        defaultKarzaCompInformationShouldNotBeFound("state.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByStateContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where state contains DEFAULT_STATE
        defaultKarzaCompInformationShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the karzaCompInformationList where state contains UPDATED_STATE
        defaultKarzaCompInformationShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByStateNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where state does not contain DEFAULT_STATE
        defaultKarzaCompInformationShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the karzaCompInformationList where state does not contain UPDATED_STATE
        defaultKarzaCompInformationShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsValidGstIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isValidGst equals to DEFAULT_IS_VALID_GST
        defaultKarzaCompInformationShouldBeFound("isValidGst.equals=" + DEFAULT_IS_VALID_GST);

        // Get all the karzaCompInformationList where isValidGst equals to UPDATED_IS_VALID_GST
        defaultKarzaCompInformationShouldNotBeFound("isValidGst.equals=" + UPDATED_IS_VALID_GST);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsValidGstIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isValidGst not equals to DEFAULT_IS_VALID_GST
        defaultKarzaCompInformationShouldNotBeFound("isValidGst.notEquals=" + DEFAULT_IS_VALID_GST);

        // Get all the karzaCompInformationList where isValidGst not equals to UPDATED_IS_VALID_GST
        defaultKarzaCompInformationShouldBeFound("isValidGst.notEquals=" + UPDATED_IS_VALID_GST);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsValidGstIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isValidGst in DEFAULT_IS_VALID_GST or UPDATED_IS_VALID_GST
        defaultKarzaCompInformationShouldBeFound("isValidGst.in=" + DEFAULT_IS_VALID_GST + "," + UPDATED_IS_VALID_GST);

        // Get all the karzaCompInformationList where isValidGst equals to UPDATED_IS_VALID_GST
        defaultKarzaCompInformationShouldNotBeFound("isValidGst.in=" + UPDATED_IS_VALID_GST);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsValidGstIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isValidGst is not null
        defaultKarzaCompInformationShouldBeFound("isValidGst.specified=true");

        // Get all the karzaCompInformationList where isValidGst is null
        defaultKarzaCompInformationShouldNotBeFound("isValidGst.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsValidGstContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isValidGst contains DEFAULT_IS_VALID_GST
        defaultKarzaCompInformationShouldBeFound("isValidGst.contains=" + DEFAULT_IS_VALID_GST);

        // Get all the karzaCompInformationList where isValidGst contains UPDATED_IS_VALID_GST
        defaultKarzaCompInformationShouldNotBeFound("isValidGst.contains=" + UPDATED_IS_VALID_GST);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByIsValidGstNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where isValidGst does not contain DEFAULT_IS_VALID_GST
        defaultKarzaCompInformationShouldNotBeFound("isValidGst.doesNotContain=" + DEFAULT_IS_VALID_GST);

        // Get all the karzaCompInformationList where isValidGst does not contain UPDATED_IS_VALID_GST
        defaultKarzaCompInformationShouldBeFound("isValidGst.doesNotContain=" + UPDATED_IS_VALID_GST);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsBySourceIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where source equals to DEFAULT_SOURCE
        defaultKarzaCompInformationShouldBeFound("source.equals=" + DEFAULT_SOURCE);

        // Get all the karzaCompInformationList where source equals to UPDATED_SOURCE
        defaultKarzaCompInformationShouldNotBeFound("source.equals=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsBySourceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where source not equals to DEFAULT_SOURCE
        defaultKarzaCompInformationShouldNotBeFound("source.notEquals=" + DEFAULT_SOURCE);

        // Get all the karzaCompInformationList where source not equals to UPDATED_SOURCE
        defaultKarzaCompInformationShouldBeFound("source.notEquals=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsBySourceIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where source in DEFAULT_SOURCE or UPDATED_SOURCE
        defaultKarzaCompInformationShouldBeFound("source.in=" + DEFAULT_SOURCE + "," + UPDATED_SOURCE);

        // Get all the karzaCompInformationList where source equals to UPDATED_SOURCE
        defaultKarzaCompInformationShouldNotBeFound("source.in=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsBySourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where source is not null
        defaultKarzaCompInformationShouldBeFound("source.specified=true");

        // Get all the karzaCompInformationList where source is null
        defaultKarzaCompInformationShouldNotBeFound("source.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsBySourceContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where source contains DEFAULT_SOURCE
        defaultKarzaCompInformationShouldBeFound("source.contains=" + DEFAULT_SOURCE);

        // Get all the karzaCompInformationList where source contains UPDATED_SOURCE
        defaultKarzaCompInformationShouldNotBeFound("source.contains=" + UPDATED_SOURCE);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsBySourceNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where source does not contain DEFAULT_SOURCE
        defaultKarzaCompInformationShouldNotBeFound("source.doesNotContain=" + DEFAULT_SOURCE);

        // Get all the karzaCompInformationList where source does not contain UPDATED_SOURCE
        defaultKarzaCompInformationShouldBeFound("source.doesNotContain=" + UPDATED_SOURCE);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where comments equals to DEFAULT_COMMENTS
        defaultKarzaCompInformationShouldBeFound("comments.equals=" + DEFAULT_COMMENTS);

        // Get all the karzaCompInformationList where comments equals to UPDATED_COMMENTS
        defaultKarzaCompInformationShouldNotBeFound("comments.equals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where comments not equals to DEFAULT_COMMENTS
        defaultKarzaCompInformationShouldNotBeFound("comments.notEquals=" + DEFAULT_COMMENTS);

        // Get all the karzaCompInformationList where comments not equals to UPDATED_COMMENTS
        defaultKarzaCompInformationShouldBeFound("comments.notEquals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where comments in DEFAULT_COMMENTS or UPDATED_COMMENTS
        defaultKarzaCompInformationShouldBeFound("comments.in=" + DEFAULT_COMMENTS + "," + UPDATED_COMMENTS);

        // Get all the karzaCompInformationList where comments equals to UPDATED_COMMENTS
        defaultKarzaCompInformationShouldNotBeFound("comments.in=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where comments is not null
        defaultKarzaCompInformationShouldBeFound("comments.specified=true");

        // Get all the karzaCompInformationList where comments is null
        defaultKarzaCompInformationShouldNotBeFound("comments.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByCommentsContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where comments contains DEFAULT_COMMENTS
        defaultKarzaCompInformationShouldBeFound("comments.contains=" + DEFAULT_COMMENTS);

        // Get all the karzaCompInformationList where comments contains UPDATED_COMMENTS
        defaultKarzaCompInformationShouldNotBeFound("comments.contains=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where comments does not contain DEFAULT_COMMENTS
        defaultKarzaCompInformationShouldNotBeFound("comments.doesNotContain=" + DEFAULT_COMMENTS);

        // Get all the karzaCompInformationList where comments does not contain UPDATED_COMMENTS
        defaultKarzaCompInformationShouldBeFound("comments.doesNotContain=" + UPDATED_COMMENTS);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCheckdigitIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where checkdigit equals to DEFAULT_CHECKDIGIT
        defaultKarzaCompInformationShouldBeFound("checkdigit.equals=" + DEFAULT_CHECKDIGIT);

        // Get all the karzaCompInformationList where checkdigit equals to UPDATED_CHECKDIGIT
        defaultKarzaCompInformationShouldNotBeFound("checkdigit.equals=" + UPDATED_CHECKDIGIT);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCheckdigitIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where checkdigit not equals to DEFAULT_CHECKDIGIT
        defaultKarzaCompInformationShouldNotBeFound("checkdigit.notEquals=" + DEFAULT_CHECKDIGIT);

        // Get all the karzaCompInformationList where checkdigit not equals to UPDATED_CHECKDIGIT
        defaultKarzaCompInformationShouldBeFound("checkdigit.notEquals=" + UPDATED_CHECKDIGIT);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCheckdigitIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where checkdigit in DEFAULT_CHECKDIGIT or UPDATED_CHECKDIGIT
        defaultKarzaCompInformationShouldBeFound("checkdigit.in=" + DEFAULT_CHECKDIGIT + "," + UPDATED_CHECKDIGIT);

        // Get all the karzaCompInformationList where checkdigit equals to UPDATED_CHECKDIGIT
        defaultKarzaCompInformationShouldNotBeFound("checkdigit.in=" + UPDATED_CHECKDIGIT);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCheckdigitIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where checkdigit is not null
        defaultKarzaCompInformationShouldBeFound("checkdigit.specified=true");

        // Get all the karzaCompInformationList where checkdigit is null
        defaultKarzaCompInformationShouldNotBeFound("checkdigit.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByCheckdigitContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where checkdigit contains DEFAULT_CHECKDIGIT
        defaultKarzaCompInformationShouldBeFound("checkdigit.contains=" + DEFAULT_CHECKDIGIT);

        // Get all the karzaCompInformationList where checkdigit contains UPDATED_CHECKDIGIT
        defaultKarzaCompInformationShouldNotBeFound("checkdigit.contains=" + UPDATED_CHECKDIGIT);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCheckdigitNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where checkdigit does not contain DEFAULT_CHECKDIGIT
        defaultKarzaCompInformationShouldNotBeFound("checkdigit.doesNotContain=" + DEFAULT_CHECKDIGIT);

        // Get all the karzaCompInformationList where checkdigit does not contain UPDATED_CHECKDIGIT
        defaultKarzaCompInformationShouldBeFound("checkdigit.doesNotContain=" + UPDATED_CHECKDIGIT);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTaxpayerNameIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where taxpayerName equals to DEFAULT_TAXPAYER_NAME
        defaultKarzaCompInformationShouldBeFound("taxpayerName.equals=" + DEFAULT_TAXPAYER_NAME);

        // Get all the karzaCompInformationList where taxpayerName equals to UPDATED_TAXPAYER_NAME
        defaultKarzaCompInformationShouldNotBeFound("taxpayerName.equals=" + UPDATED_TAXPAYER_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTaxpayerNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where taxpayerName not equals to DEFAULT_TAXPAYER_NAME
        defaultKarzaCompInformationShouldNotBeFound("taxpayerName.notEquals=" + DEFAULT_TAXPAYER_NAME);

        // Get all the karzaCompInformationList where taxpayerName not equals to UPDATED_TAXPAYER_NAME
        defaultKarzaCompInformationShouldBeFound("taxpayerName.notEquals=" + UPDATED_TAXPAYER_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTaxpayerNameIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where taxpayerName in DEFAULT_TAXPAYER_NAME or UPDATED_TAXPAYER_NAME
        defaultKarzaCompInformationShouldBeFound("taxpayerName.in=" + DEFAULT_TAXPAYER_NAME + "," + UPDATED_TAXPAYER_NAME);

        // Get all the karzaCompInformationList where taxpayerName equals to UPDATED_TAXPAYER_NAME
        defaultKarzaCompInformationShouldNotBeFound("taxpayerName.in=" + UPDATED_TAXPAYER_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTaxpayerNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where taxpayerName is not null
        defaultKarzaCompInformationShouldBeFound("taxpayerName.specified=true");

        // Get all the karzaCompInformationList where taxpayerName is null
        defaultKarzaCompInformationShouldNotBeFound("taxpayerName.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByTaxpayerNameContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where taxpayerName contains DEFAULT_TAXPAYER_NAME
        defaultKarzaCompInformationShouldBeFound("taxpayerName.contains=" + DEFAULT_TAXPAYER_NAME);

        // Get all the karzaCompInformationList where taxpayerName contains UPDATED_TAXPAYER_NAME
        defaultKarzaCompInformationShouldNotBeFound("taxpayerName.contains=" + UPDATED_TAXPAYER_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTaxpayerNameNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where taxpayerName does not contain DEFAULT_TAXPAYER_NAME
        defaultKarzaCompInformationShouldNotBeFound("taxpayerName.doesNotContain=" + DEFAULT_TAXPAYER_NAME);

        // Get all the karzaCompInformationList where taxpayerName does not contain UPDATED_TAXPAYER_NAME
        defaultKarzaCompInformationShouldBeFound("taxpayerName.doesNotContain=" + UPDATED_TAXPAYER_NAME);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByNatuteOfBussIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where natuteOfBuss equals to DEFAULT_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldBeFound("natuteOfBuss.equals=" + DEFAULT_NATUTE_OF_BUSS);

        // Get all the karzaCompInformationList where natuteOfBuss equals to UPDATED_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldNotBeFound("natuteOfBuss.equals=" + UPDATED_NATUTE_OF_BUSS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByNatuteOfBussIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where natuteOfBuss not equals to DEFAULT_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldNotBeFound("natuteOfBuss.notEquals=" + DEFAULT_NATUTE_OF_BUSS);

        // Get all the karzaCompInformationList where natuteOfBuss not equals to UPDATED_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldBeFound("natuteOfBuss.notEquals=" + UPDATED_NATUTE_OF_BUSS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByNatuteOfBussIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where natuteOfBuss in DEFAULT_NATUTE_OF_BUSS or UPDATED_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldBeFound("natuteOfBuss.in=" + DEFAULT_NATUTE_OF_BUSS + "," + UPDATED_NATUTE_OF_BUSS);

        // Get all the karzaCompInformationList where natuteOfBuss equals to UPDATED_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldNotBeFound("natuteOfBuss.in=" + UPDATED_NATUTE_OF_BUSS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByNatuteOfBussIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where natuteOfBuss is not null
        defaultKarzaCompInformationShouldBeFound("natuteOfBuss.specified=true");

        // Get all the karzaCompInformationList where natuteOfBuss is null
        defaultKarzaCompInformationShouldNotBeFound("natuteOfBuss.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByNatuteOfBussContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where natuteOfBuss contains DEFAULT_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldBeFound("natuteOfBuss.contains=" + DEFAULT_NATUTE_OF_BUSS);

        // Get all the karzaCompInformationList where natuteOfBuss contains UPDATED_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldNotBeFound("natuteOfBuss.contains=" + UPDATED_NATUTE_OF_BUSS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByNatuteOfBussNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where natuteOfBuss does not contain DEFAULT_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldNotBeFound("natuteOfBuss.doesNotContain=" + DEFAULT_NATUTE_OF_BUSS);

        // Get all the karzaCompInformationList where natuteOfBuss does not contain UPDATED_NATUTE_OF_BUSS
        defaultKarzaCompInformationShouldBeFound("natuteOfBuss.doesNotContain=" + UPDATED_NATUTE_OF_BUSS);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByMobileNoIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where mobileNo equals to DEFAULT_MOBILE_NO
        defaultKarzaCompInformationShouldBeFound("mobileNo.equals=" + DEFAULT_MOBILE_NO);

        // Get all the karzaCompInformationList where mobileNo equals to UPDATED_MOBILE_NO
        defaultKarzaCompInformationShouldNotBeFound("mobileNo.equals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByMobileNoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where mobileNo not equals to DEFAULT_MOBILE_NO
        defaultKarzaCompInformationShouldNotBeFound("mobileNo.notEquals=" + DEFAULT_MOBILE_NO);

        // Get all the karzaCompInformationList where mobileNo not equals to UPDATED_MOBILE_NO
        defaultKarzaCompInformationShouldBeFound("mobileNo.notEquals=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByMobileNoIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where mobileNo in DEFAULT_MOBILE_NO or UPDATED_MOBILE_NO
        defaultKarzaCompInformationShouldBeFound("mobileNo.in=" + DEFAULT_MOBILE_NO + "," + UPDATED_MOBILE_NO);

        // Get all the karzaCompInformationList where mobileNo equals to UPDATED_MOBILE_NO
        defaultKarzaCompInformationShouldNotBeFound("mobileNo.in=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByMobileNoIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where mobileNo is not null
        defaultKarzaCompInformationShouldBeFound("mobileNo.specified=true");

        // Get all the karzaCompInformationList where mobileNo is null
        defaultKarzaCompInformationShouldNotBeFound("mobileNo.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByMobileNoContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where mobileNo contains DEFAULT_MOBILE_NO
        defaultKarzaCompInformationShouldBeFound("mobileNo.contains=" + DEFAULT_MOBILE_NO);

        // Get all the karzaCompInformationList where mobileNo contains UPDATED_MOBILE_NO
        defaultKarzaCompInformationShouldNotBeFound("mobileNo.contains=" + UPDATED_MOBILE_NO);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByMobileNoNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where mobileNo does not contain DEFAULT_MOBILE_NO
        defaultKarzaCompInformationShouldNotBeFound("mobileNo.doesNotContain=" + DEFAULT_MOBILE_NO);

        // Get all the karzaCompInformationList where mobileNo does not contain UPDATED_MOBILE_NO
        defaultKarzaCompInformationShouldBeFound("mobileNo.doesNotContain=" + UPDATED_MOBILE_NO);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where email equals to DEFAULT_EMAIL
        defaultKarzaCompInformationShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the karzaCompInformationList where email equals to UPDATED_EMAIL
        defaultKarzaCompInformationShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where email not equals to DEFAULT_EMAIL
        defaultKarzaCompInformationShouldNotBeFound("email.notEquals=" + DEFAULT_EMAIL);

        // Get all the karzaCompInformationList where email not equals to UPDATED_EMAIL
        defaultKarzaCompInformationShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultKarzaCompInformationShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the karzaCompInformationList where email equals to UPDATED_EMAIL
        defaultKarzaCompInformationShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where email is not null
        defaultKarzaCompInformationShouldBeFound("email.specified=true");

        // Get all the karzaCompInformationList where email is null
        defaultKarzaCompInformationShouldNotBeFound("email.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByEmailContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where email contains DEFAULT_EMAIL
        defaultKarzaCompInformationShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the karzaCompInformationList where email contains UPDATED_EMAIL
        defaultKarzaCompInformationShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where email does not contain DEFAULT_EMAIL
        defaultKarzaCompInformationShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

        // Get all the karzaCompInformationList where email does not contain UPDATED_EMAIL
        defaultKarzaCompInformationShouldBeFound("email.doesNotContain=" + UPDATED_EMAIL);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTradeNameIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where tradeName equals to DEFAULT_TRADE_NAME
        defaultKarzaCompInformationShouldBeFound("tradeName.equals=" + DEFAULT_TRADE_NAME);

        // Get all the karzaCompInformationList where tradeName equals to UPDATED_TRADE_NAME
        defaultKarzaCompInformationShouldNotBeFound("tradeName.equals=" + UPDATED_TRADE_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTradeNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where tradeName not equals to DEFAULT_TRADE_NAME
        defaultKarzaCompInformationShouldNotBeFound("tradeName.notEquals=" + DEFAULT_TRADE_NAME);

        // Get all the karzaCompInformationList where tradeName not equals to UPDATED_TRADE_NAME
        defaultKarzaCompInformationShouldBeFound("tradeName.notEquals=" + UPDATED_TRADE_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTradeNameIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where tradeName in DEFAULT_TRADE_NAME or UPDATED_TRADE_NAME
        defaultKarzaCompInformationShouldBeFound("tradeName.in=" + DEFAULT_TRADE_NAME + "," + UPDATED_TRADE_NAME);

        // Get all the karzaCompInformationList where tradeName equals to UPDATED_TRADE_NAME
        defaultKarzaCompInformationShouldNotBeFound("tradeName.in=" + UPDATED_TRADE_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTradeNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where tradeName is not null
        defaultKarzaCompInformationShouldBeFound("tradeName.specified=true");

        // Get all the karzaCompInformationList where tradeName is null
        defaultKarzaCompInformationShouldNotBeFound("tradeName.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByTradeNameContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where tradeName contains DEFAULT_TRADE_NAME
        defaultKarzaCompInformationShouldBeFound("tradeName.contains=" + DEFAULT_TRADE_NAME);

        // Get all the karzaCompInformationList where tradeName contains UPDATED_TRADE_NAME
        defaultKarzaCompInformationShouldNotBeFound("tradeName.contains=" + UPDATED_TRADE_NAME);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByTradeNameNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where tradeName does not contain DEFAULT_TRADE_NAME
        defaultKarzaCompInformationShouldNotBeFound("tradeName.doesNotContain=" + DEFAULT_TRADE_NAME);

        // Get all the karzaCompInformationList where tradeName does not contain UPDATED_TRADE_NAME
        defaultKarzaCompInformationShouldBeFound("tradeName.doesNotContain=" + UPDATED_TRADE_NAME);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByConstitBusinessIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where constitBusiness equals to DEFAULT_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldBeFound("constitBusiness.equals=" + DEFAULT_CONSTIT_BUSINESS);

        // Get all the karzaCompInformationList where constitBusiness equals to UPDATED_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldNotBeFound("constitBusiness.equals=" + UPDATED_CONSTIT_BUSINESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByConstitBusinessIsNotEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where constitBusiness not equals to DEFAULT_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldNotBeFound("constitBusiness.notEquals=" + DEFAULT_CONSTIT_BUSINESS);

        // Get all the karzaCompInformationList where constitBusiness not equals to UPDATED_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldBeFound("constitBusiness.notEquals=" + UPDATED_CONSTIT_BUSINESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByConstitBusinessIsInShouldWork() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where constitBusiness in DEFAULT_CONSTIT_BUSINESS or UPDATED_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldBeFound("constitBusiness.in=" + DEFAULT_CONSTIT_BUSINESS + "," + UPDATED_CONSTIT_BUSINESS);

        // Get all the karzaCompInformationList where constitBusiness equals to UPDATED_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldNotBeFound("constitBusiness.in=" + UPDATED_CONSTIT_BUSINESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByConstitBusinessIsNullOrNotNull() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where constitBusiness is not null
        defaultKarzaCompInformationShouldBeFound("constitBusiness.specified=true");

        // Get all the karzaCompInformationList where constitBusiness is null
        defaultKarzaCompInformationShouldNotBeFound("constitBusiness.specified=false");
    }
                @Test
    @Transactional
    public void getAllKarzaCompInformationsByConstitBusinessContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where constitBusiness contains DEFAULT_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldBeFound("constitBusiness.contains=" + DEFAULT_CONSTIT_BUSINESS);

        // Get all the karzaCompInformationList where constitBusiness contains UPDATED_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldNotBeFound("constitBusiness.contains=" + UPDATED_CONSTIT_BUSINESS);
    }

    @Test
    @Transactional
    public void getAllKarzaCompInformationsByConstitBusinessNotContainsSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);

        // Get all the karzaCompInformationList where constitBusiness does not contain DEFAULT_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldNotBeFound("constitBusiness.doesNotContain=" + DEFAULT_CONSTIT_BUSINESS);

        // Get all the karzaCompInformationList where constitBusiness does not contain UPDATED_CONSTIT_BUSINESS
        defaultKarzaCompInformationShouldBeFound("constitBusiness.doesNotContain=" + UPDATED_CONSTIT_BUSINESS);
    }


    @Test
    @Transactional
    public void getAllKarzaCompInformationsByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);
        OnesourceCompanyMaster companyCode = OnesourceCompanyMasterResourceIT.createEntity(em);
        em.persist(companyCode);
        em.flush();
        karzaCompInformation.setCompanyCode(companyCode);
        karzaCompInformationRepository.saveAndFlush(karzaCompInformation);
        Long companyCodeId = companyCode.getId();

        // Get all the karzaCompInformationList where companyCode equals to companyCodeId
        defaultKarzaCompInformationShouldBeFound("companyCodeId.equals=" + companyCodeId);

        // Get all the karzaCompInformationList where companyCode equals to companyCodeId + 1
        defaultKarzaCompInformationShouldNotBeFound("companyCodeId.equals=" + (companyCodeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultKarzaCompInformationShouldBeFound(String filter) throws Exception {
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(karzaCompInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].companyNameKarza").value(hasItem(DEFAULT_COMPANY_NAME_KARZA)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].typeofapproval").value(hasItem(DEFAULT_TYPEOFAPPROVAL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].gstin").value(hasItem(DEFAULT_GSTIN)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].isValidGst").value(hasItem(DEFAULT_IS_VALID_GST)))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].checkdigit").value(hasItem(DEFAULT_CHECKDIGIT)))
            .andExpect(jsonPath("$.[*].taxpayerName").value(hasItem(DEFAULT_TAXPAYER_NAME)))
            .andExpect(jsonPath("$.[*].natuteOfBuss").value(hasItem(DEFAULT_NATUTE_OF_BUSS)))
            .andExpect(jsonPath("$.[*].mobileNo").value(hasItem(DEFAULT_MOBILE_NO)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].tradeName").value(hasItem(DEFAULT_TRADE_NAME)))
            .andExpect(jsonPath("$.[*].constitBusiness").value(hasItem(DEFAULT_CONSTIT_BUSINESS)));

        // Check, that the count call also returns 1
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultKarzaCompInformationShouldNotBeFound(String filter) throws Exception {
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingKarzaCompInformation() throws Exception {
        // Get the karzaCompInformation
        restKarzaCompInformationMockMvc.perform(get("/api/karza-comp-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateKarzaCompInformation() throws Exception {
        // Initialize the database
        karzaCompInformationService.save(karzaCompInformation);

        int databaseSizeBeforeUpdate = karzaCompInformationRepository.findAll().size();

        // Update the karzaCompInformation
        KarzaCompInformation updatedKarzaCompInformation = karzaCompInformationRepository.findById(karzaCompInformation.getId()).get();
        // Disconnect from session so that the updates on updatedKarzaCompInformation are not directly saved in db
        em.detach(updatedKarzaCompInformation);
        updatedKarzaCompInformation
            .pan(UPDATED_PAN)
            .companyNameKarza(UPDATED_COMPANY_NAME_KARZA)
            .address(UPDATED_ADDRESS)
            .typeofapproval(UPDATED_TYPEOFAPPROVAL)
            .status(UPDATED_STATUS)
            .gstin(UPDATED_GSTIN)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED)
            .state(UPDATED_STATE)
            .isValidGst(UPDATED_IS_VALID_GST)
            .source(UPDATED_SOURCE)
            .comments(UPDATED_COMMENTS)
            .checkdigit(UPDATED_CHECKDIGIT)
            .taxpayerName(UPDATED_TAXPAYER_NAME)
            .natuteOfBuss(UPDATED_NATUTE_OF_BUSS)
            .mobileNo(UPDATED_MOBILE_NO)
            .email(UPDATED_EMAIL)
            .tradeName(UPDATED_TRADE_NAME)
            .constitBusiness(UPDATED_CONSTIT_BUSINESS);

        restKarzaCompInformationMockMvc.perform(put("/api/karza-comp-informations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedKarzaCompInformation)))
            .andExpect(status().isOk());

        // Validate the KarzaCompInformation in the database
        List<KarzaCompInformation> karzaCompInformationList = karzaCompInformationRepository.findAll();
        assertThat(karzaCompInformationList).hasSize(databaseSizeBeforeUpdate);
        KarzaCompInformation testKarzaCompInformation = karzaCompInformationList.get(karzaCompInformationList.size() - 1);
        assertThat(testKarzaCompInformation.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testKarzaCompInformation.getCompanyNameKarza()).isEqualTo(UPDATED_COMPANY_NAME_KARZA);
        assertThat(testKarzaCompInformation.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testKarzaCompInformation.getTypeofapproval()).isEqualTo(UPDATED_TYPEOFAPPROVAL);
        assertThat(testKarzaCompInformation.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testKarzaCompInformation.getGstin()).isEqualTo(UPDATED_GSTIN);
        assertThat(testKarzaCompInformation.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testKarzaCompInformation.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testKarzaCompInformation.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testKarzaCompInformation.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testKarzaCompInformation.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testKarzaCompInformation.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testKarzaCompInformation.getIsValidGst()).isEqualTo(UPDATED_IS_VALID_GST);
        assertThat(testKarzaCompInformation.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testKarzaCompInformation.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testKarzaCompInformation.getCheckdigit()).isEqualTo(UPDATED_CHECKDIGIT);
        assertThat(testKarzaCompInformation.getTaxpayerName()).isEqualTo(UPDATED_TAXPAYER_NAME);
        assertThat(testKarzaCompInformation.getNatuteOfBuss()).isEqualTo(UPDATED_NATUTE_OF_BUSS);
        assertThat(testKarzaCompInformation.getMobileNo()).isEqualTo(UPDATED_MOBILE_NO);
        assertThat(testKarzaCompInformation.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testKarzaCompInformation.getTradeName()).isEqualTo(UPDATED_TRADE_NAME);
        assertThat(testKarzaCompInformation.getConstitBusiness()).isEqualTo(UPDATED_CONSTIT_BUSINESS);
    }

    @Test
    @Transactional
    public void updateNonExistingKarzaCompInformation() throws Exception {
        int databaseSizeBeforeUpdate = karzaCompInformationRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restKarzaCompInformationMockMvc.perform(put("/api/karza-comp-informations")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(karzaCompInformation)))
            .andExpect(status().isBadRequest());

        // Validate the KarzaCompInformation in the database
        List<KarzaCompInformation> karzaCompInformationList = karzaCompInformationRepository.findAll();
        assertThat(karzaCompInformationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteKarzaCompInformation() throws Exception {
        // Initialize the database
        karzaCompInformationService.save(karzaCompInformation);

        int databaseSizeBeforeDelete = karzaCompInformationRepository.findAll().size();

        // Delete the karzaCompInformation
        restKarzaCompInformationMockMvc.perform(delete("/api/karza-comp-informations/{id}", karzaCompInformation.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<KarzaCompInformation> karzaCompInformationList = karzaCompInformationRepository.findAll();
        assertThat(karzaCompInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
