package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.domain.LocRegionMaster;
import com.crisil.onesource.domain.LocCountryMaster;
import com.crisil.onesource.repository.LocStateMasterRepository;
import com.crisil.onesource.service.LocStateMasterService;
import com.crisil.onesource.service.dto.LocStateMasterCriteria;
import com.crisil.onesource.service.LocStateMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocStateMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocStateMasterResourceIT {

    private static final String DEFAULT_STATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STATE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATE_ID = 1;
    private static final Integer UPDATED_STATE_ID = 2;
    private static final Integer SMALLER_STATE_ID = 1 - 1;

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_COUNTRY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_STATE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_IS_APPROVED = "A";
    private static final String UPDATED_IS_APPROVED = "B";

    private static final Integer DEFAULT_SEQ_ORDER = 1;
    private static final Integer UPDATED_SEQ_ORDER = 2;
    private static final Integer SMALLER_SEQ_ORDER = 1 - 1;

    @Autowired
    private LocStateMasterRepository locStateMasterRepository;

    @Autowired
    private LocStateMasterService locStateMasterService;

    @Autowired
    private LocStateMasterQueryService locStateMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocStateMasterMockMvc;

    private LocStateMaster locStateMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocStateMaster createEntity(EntityManager em) {
        LocStateMaster locStateMaster = new LocStateMaster()
            .stateName(DEFAULT_STATE_NAME)
            .description(DEFAULT_DESCRIPTION)
            .stateId(DEFAULT_STATE_ID)
            .isDeleted(DEFAULT_IS_DELETED)
            .countryCode(DEFAULT_COUNTRY_CODE)
            .stateCode(DEFAULT_STATE_CODE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isApproved(DEFAULT_IS_APPROVED)
            .seqOrder(DEFAULT_SEQ_ORDER);
        return locStateMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocStateMaster createUpdatedEntity(EntityManager em) {
        LocStateMaster locStateMaster = new LocStateMaster()
            .stateName(UPDATED_STATE_NAME)
            .description(UPDATED_DESCRIPTION)
            .stateId(UPDATED_STATE_ID)
            .isDeleted(UPDATED_IS_DELETED)
            .countryCode(UPDATED_COUNTRY_CODE)
            .stateCode(UPDATED_STATE_CODE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);
        return locStateMaster;
    }

    @BeforeEach
    public void initTest() {
        locStateMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocStateMaster() throws Exception {
        int databaseSizeBeforeCreate = locStateMasterRepository.findAll().size();
        // Create the LocStateMaster
        restLocStateMasterMockMvc.perform(post("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locStateMaster)))
            .andExpect(status().isCreated());

        // Validate the LocStateMaster in the database
        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeCreate + 1);
        LocStateMaster testLocStateMaster = locStateMasterList.get(locStateMasterList.size() - 1);
        assertThat(testLocStateMaster.getStateName()).isEqualTo(DEFAULT_STATE_NAME);
        assertThat(testLocStateMaster.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testLocStateMaster.getStateId()).isEqualTo(DEFAULT_STATE_ID);
        assertThat(testLocStateMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocStateMaster.getCountryCode()).isEqualTo(DEFAULT_COUNTRY_CODE);
        assertThat(testLocStateMaster.getStateCode()).isEqualTo(DEFAULT_STATE_CODE);
        assertThat(testLocStateMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocStateMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocStateMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testLocStateMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocStateMaster.getIsApproved()).isEqualTo(DEFAULT_IS_APPROVED);
        assertThat(testLocStateMaster.getSeqOrder()).isEqualTo(DEFAULT_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void createLocStateMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locStateMasterRepository.findAll().size();

        // Create the LocStateMaster with an existing ID
        locStateMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocStateMasterMockMvc.perform(post("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locStateMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocStateMaster in the database
        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStateNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = locStateMasterRepository.findAll().size();
        // set the field null
        locStateMaster.setStateName(null);

        // Create the LocStateMaster, which fails.


        restLocStateMasterMockMvc.perform(post("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locStateMaster)))
            .andExpect(status().isBadRequest());

        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = locStateMasterRepository.findAll().size();
        // set the field null
        locStateMaster.setDescription(null);

        // Create the LocStateMaster, which fails.


        restLocStateMasterMockMvc.perform(post("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locStateMaster)))
            .andExpect(status().isBadRequest());

        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStateIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = locStateMasterRepository.findAll().size();
        // set the field null
        locStateMaster.setStateId(null);

        // Create the LocStateMaster, which fails.


        restLocStateMasterMockMvc.perform(post("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locStateMaster)))
            .andExpect(status().isBadRequest());

        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocStateMasters() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locStateMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].stateId").value(hasItem(DEFAULT_STATE_ID)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].stateCode").value(hasItem(DEFAULT_STATE_CODE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));
    }
    
    @Test
    @Transactional
    public void getLocStateMaster() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get the locStateMaster
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters/{id}", locStateMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locStateMaster.getId().intValue()))
            .andExpect(jsonPath("$.stateName").value(DEFAULT_STATE_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.stateId").value(DEFAULT_STATE_ID))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.countryCode").value(DEFAULT_COUNTRY_CODE))
            .andExpect(jsonPath("$.stateCode").value(DEFAULT_STATE_CODE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isApproved").value(DEFAULT_IS_APPROVED))
            .andExpect(jsonPath("$.seqOrder").value(DEFAULT_SEQ_ORDER));
    }


    @Test
    @Transactional
    public void getLocStateMastersByIdFiltering() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        Long id = locStateMaster.getId();

        defaultLocStateMasterShouldBeFound("id.equals=" + id);
        defaultLocStateMasterShouldNotBeFound("id.notEquals=" + id);

        defaultLocStateMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocStateMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultLocStateMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocStateMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByStateNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateName equals to DEFAULT_STATE_NAME
        defaultLocStateMasterShouldBeFound("stateName.equals=" + DEFAULT_STATE_NAME);

        // Get all the locStateMasterList where stateName equals to UPDATED_STATE_NAME
        defaultLocStateMasterShouldNotBeFound("stateName.equals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateName not equals to DEFAULT_STATE_NAME
        defaultLocStateMasterShouldNotBeFound("stateName.notEquals=" + DEFAULT_STATE_NAME);

        // Get all the locStateMasterList where stateName not equals to UPDATED_STATE_NAME
        defaultLocStateMasterShouldBeFound("stateName.notEquals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateNameIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateName in DEFAULT_STATE_NAME or UPDATED_STATE_NAME
        defaultLocStateMasterShouldBeFound("stateName.in=" + DEFAULT_STATE_NAME + "," + UPDATED_STATE_NAME);

        // Get all the locStateMasterList where stateName equals to UPDATED_STATE_NAME
        defaultLocStateMasterShouldNotBeFound("stateName.in=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateName is not null
        defaultLocStateMasterShouldBeFound("stateName.specified=true");

        // Get all the locStateMasterList where stateName is null
        defaultLocStateMasterShouldNotBeFound("stateName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByStateNameContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateName contains DEFAULT_STATE_NAME
        defaultLocStateMasterShouldBeFound("stateName.contains=" + DEFAULT_STATE_NAME);

        // Get all the locStateMasterList where stateName contains UPDATED_STATE_NAME
        defaultLocStateMasterShouldNotBeFound("stateName.contains=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateNameNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateName does not contain DEFAULT_STATE_NAME
        defaultLocStateMasterShouldNotBeFound("stateName.doesNotContain=" + DEFAULT_STATE_NAME);

        // Get all the locStateMasterList where stateName does not contain UPDATED_STATE_NAME
        defaultLocStateMasterShouldBeFound("stateName.doesNotContain=" + UPDATED_STATE_NAME);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where description equals to DEFAULT_DESCRIPTION
        defaultLocStateMasterShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the locStateMasterList where description equals to UPDATED_DESCRIPTION
        defaultLocStateMasterShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where description not equals to DEFAULT_DESCRIPTION
        defaultLocStateMasterShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the locStateMasterList where description not equals to UPDATED_DESCRIPTION
        defaultLocStateMasterShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultLocStateMasterShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the locStateMasterList where description equals to UPDATED_DESCRIPTION
        defaultLocStateMasterShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where description is not null
        defaultLocStateMasterShouldBeFound("description.specified=true");

        // Get all the locStateMasterList where description is null
        defaultLocStateMasterShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where description contains DEFAULT_DESCRIPTION
        defaultLocStateMasterShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the locStateMasterList where description contains UPDATED_DESCRIPTION
        defaultLocStateMasterShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where description does not contain DEFAULT_DESCRIPTION
        defaultLocStateMasterShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the locStateMasterList where description does not contain UPDATED_DESCRIPTION
        defaultLocStateMasterShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId equals to DEFAULT_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.equals=" + DEFAULT_STATE_ID);

        // Get all the locStateMasterList where stateId equals to UPDATED_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.equals=" + UPDATED_STATE_ID);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId not equals to DEFAULT_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.notEquals=" + DEFAULT_STATE_ID);

        // Get all the locStateMasterList where stateId not equals to UPDATED_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.notEquals=" + UPDATED_STATE_ID);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId in DEFAULT_STATE_ID or UPDATED_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.in=" + DEFAULT_STATE_ID + "," + UPDATED_STATE_ID);

        // Get all the locStateMasterList where stateId equals to UPDATED_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.in=" + UPDATED_STATE_ID);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId is not null
        defaultLocStateMasterShouldBeFound("stateId.specified=true");

        // Get all the locStateMasterList where stateId is null
        defaultLocStateMasterShouldNotBeFound("stateId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId is greater than or equal to DEFAULT_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.greaterThanOrEqual=" + DEFAULT_STATE_ID);

        // Get all the locStateMasterList where stateId is greater than or equal to UPDATED_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.greaterThanOrEqual=" + UPDATED_STATE_ID);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId is less than or equal to DEFAULT_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.lessThanOrEqual=" + DEFAULT_STATE_ID);

        // Get all the locStateMasterList where stateId is less than or equal to SMALLER_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.lessThanOrEqual=" + SMALLER_STATE_ID);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId is less than DEFAULT_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.lessThan=" + DEFAULT_STATE_ID);

        // Get all the locStateMasterList where stateId is less than UPDATED_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.lessThan=" + UPDATED_STATE_ID);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateId is greater than DEFAULT_STATE_ID
        defaultLocStateMasterShouldNotBeFound("stateId.greaterThan=" + DEFAULT_STATE_ID);

        // Get all the locStateMasterList where stateId is greater than SMALLER_STATE_ID
        defaultLocStateMasterShouldBeFound("stateId.greaterThan=" + SMALLER_STATE_ID);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocStateMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locStateMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocStateMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocStateMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locStateMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocStateMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocStateMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locStateMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocStateMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isDeleted is not null
        defaultLocStateMasterShouldBeFound("isDeleted.specified=true");

        // Get all the locStateMasterList where isDeleted is null
        defaultLocStateMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocStateMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locStateMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultLocStateMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocStateMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locStateMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocStateMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByCountryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where countryCode equals to DEFAULT_COUNTRY_CODE
        defaultLocStateMasterShouldBeFound("countryCode.equals=" + DEFAULT_COUNTRY_CODE);

        // Get all the locStateMasterList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultLocStateMasterShouldNotBeFound("countryCode.equals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCountryCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where countryCode not equals to DEFAULT_COUNTRY_CODE
        defaultLocStateMasterShouldNotBeFound("countryCode.notEquals=" + DEFAULT_COUNTRY_CODE);

        // Get all the locStateMasterList where countryCode not equals to UPDATED_COUNTRY_CODE
        defaultLocStateMasterShouldBeFound("countryCode.notEquals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCountryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where countryCode in DEFAULT_COUNTRY_CODE or UPDATED_COUNTRY_CODE
        defaultLocStateMasterShouldBeFound("countryCode.in=" + DEFAULT_COUNTRY_CODE + "," + UPDATED_COUNTRY_CODE);

        // Get all the locStateMasterList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultLocStateMasterShouldNotBeFound("countryCode.in=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCountryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where countryCode is not null
        defaultLocStateMasterShouldBeFound("countryCode.specified=true");

        // Get all the locStateMasterList where countryCode is null
        defaultLocStateMasterShouldNotBeFound("countryCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByCountryCodeContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where countryCode contains DEFAULT_COUNTRY_CODE
        defaultLocStateMasterShouldBeFound("countryCode.contains=" + DEFAULT_COUNTRY_CODE);

        // Get all the locStateMasterList where countryCode contains UPDATED_COUNTRY_CODE
        defaultLocStateMasterShouldNotBeFound("countryCode.contains=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCountryCodeNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where countryCode does not contain DEFAULT_COUNTRY_CODE
        defaultLocStateMasterShouldNotBeFound("countryCode.doesNotContain=" + DEFAULT_COUNTRY_CODE);

        // Get all the locStateMasterList where countryCode does not contain UPDATED_COUNTRY_CODE
        defaultLocStateMasterShouldBeFound("countryCode.doesNotContain=" + UPDATED_COUNTRY_CODE);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByStateCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateCode equals to DEFAULT_STATE_CODE
        defaultLocStateMasterShouldBeFound("stateCode.equals=" + DEFAULT_STATE_CODE);

        // Get all the locStateMasterList where stateCode equals to UPDATED_STATE_CODE
        defaultLocStateMasterShouldNotBeFound("stateCode.equals=" + UPDATED_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateCode not equals to DEFAULT_STATE_CODE
        defaultLocStateMasterShouldNotBeFound("stateCode.notEquals=" + DEFAULT_STATE_CODE);

        // Get all the locStateMasterList where stateCode not equals to UPDATED_STATE_CODE
        defaultLocStateMasterShouldBeFound("stateCode.notEquals=" + UPDATED_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateCodeIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateCode in DEFAULT_STATE_CODE or UPDATED_STATE_CODE
        defaultLocStateMasterShouldBeFound("stateCode.in=" + DEFAULT_STATE_CODE + "," + UPDATED_STATE_CODE);

        // Get all the locStateMasterList where stateCode equals to UPDATED_STATE_CODE
        defaultLocStateMasterShouldNotBeFound("stateCode.in=" + UPDATED_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateCode is not null
        defaultLocStateMasterShouldBeFound("stateCode.specified=true");

        // Get all the locStateMasterList where stateCode is null
        defaultLocStateMasterShouldNotBeFound("stateCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByStateCodeContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateCode contains DEFAULT_STATE_CODE
        defaultLocStateMasterShouldBeFound("stateCode.contains=" + DEFAULT_STATE_CODE);

        // Get all the locStateMasterList where stateCode contains UPDATED_STATE_CODE
        defaultLocStateMasterShouldNotBeFound("stateCode.contains=" + UPDATED_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByStateCodeNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where stateCode does not contain DEFAULT_STATE_CODE
        defaultLocStateMasterShouldNotBeFound("stateCode.doesNotContain=" + DEFAULT_STATE_CODE);

        // Get all the locStateMasterList where stateCode does not contain UPDATED_STATE_CODE
        defaultLocStateMasterShouldBeFound("stateCode.doesNotContain=" + UPDATED_STATE_CODE);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocStateMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locStateMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocStateMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocStateMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locStateMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocStateMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocStateMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locStateMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocStateMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdBy is not null
        defaultLocStateMasterShouldBeFound("createdBy.specified=true");

        // Get all the locStateMasterList where createdBy is null
        defaultLocStateMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultLocStateMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locStateMasterList where createdBy contains UPDATED_CREATED_BY
        defaultLocStateMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocStateMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locStateMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocStateMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocStateMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locStateMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocStateMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocStateMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locStateMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocStateMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocStateMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locStateMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocStateMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where createdDate is not null
        defaultLocStateMasterShouldBeFound("createdDate.specified=true");

        // Get all the locStateMasterList where createdDate is null
        defaultLocStateMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocStateMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locStateMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocStateMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocStateMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locStateMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocStateMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocStateMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locStateMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocStateMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedBy is not null
        defaultLocStateMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locStateMasterList where lastModifiedBy is null
        defaultLocStateMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocStateMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locStateMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocStateMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocStateMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locStateMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocStateMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocStateMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locStateMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocStateMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocStateMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locStateMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocStateMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocStateMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locStateMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocStateMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where lastModifiedDate is not null
        defaultLocStateMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locStateMasterList where lastModifiedDate is null
        defaultLocStateMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsApprovedIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isApproved equals to DEFAULT_IS_APPROVED
        defaultLocStateMasterShouldBeFound("isApproved.equals=" + DEFAULT_IS_APPROVED);

        // Get all the locStateMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocStateMasterShouldNotBeFound("isApproved.equals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsApprovedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isApproved not equals to DEFAULT_IS_APPROVED
        defaultLocStateMasterShouldNotBeFound("isApproved.notEquals=" + DEFAULT_IS_APPROVED);

        // Get all the locStateMasterList where isApproved not equals to UPDATED_IS_APPROVED
        defaultLocStateMasterShouldBeFound("isApproved.notEquals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsApprovedIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isApproved in DEFAULT_IS_APPROVED or UPDATED_IS_APPROVED
        defaultLocStateMasterShouldBeFound("isApproved.in=" + DEFAULT_IS_APPROVED + "," + UPDATED_IS_APPROVED);

        // Get all the locStateMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocStateMasterShouldNotBeFound("isApproved.in=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsApprovedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isApproved is not null
        defaultLocStateMasterShouldBeFound("isApproved.specified=true");

        // Get all the locStateMasterList where isApproved is null
        defaultLocStateMasterShouldNotBeFound("isApproved.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocStateMastersByIsApprovedContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isApproved contains DEFAULT_IS_APPROVED
        defaultLocStateMasterShouldBeFound("isApproved.contains=" + DEFAULT_IS_APPROVED);

        // Get all the locStateMasterList where isApproved contains UPDATED_IS_APPROVED
        defaultLocStateMasterShouldNotBeFound("isApproved.contains=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersByIsApprovedNotContainsSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where isApproved does not contain DEFAULT_IS_APPROVED
        defaultLocStateMasterShouldNotBeFound("isApproved.doesNotContain=" + DEFAULT_IS_APPROVED);

        // Get all the locStateMasterList where isApproved does not contain UPDATED_IS_APPROVED
        defaultLocStateMasterShouldBeFound("isApproved.doesNotContain=" + UPDATED_IS_APPROVED);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder equals to DEFAULT_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.equals=" + DEFAULT_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.equals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder not equals to DEFAULT_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.notEquals=" + DEFAULT_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder not equals to UPDATED_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.notEquals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsInShouldWork() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder in DEFAULT_SEQ_ORDER or UPDATED_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.in=" + DEFAULT_SEQ_ORDER + "," + UPDATED_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.in=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder is not null
        defaultLocStateMasterShouldBeFound("seqOrder.specified=true");

        // Get all the locStateMasterList where seqOrder is null
        defaultLocStateMasterShouldNotBeFound("seqOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder is greater than or equal to DEFAULT_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.greaterThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder is greater than or equal to UPDATED_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.greaterThanOrEqual=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder is less than or equal to DEFAULT_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.lessThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder is less than or equal to SMALLER_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.lessThanOrEqual=" + SMALLER_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder is less than DEFAULT_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.lessThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder is less than UPDATED_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.lessThan=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocStateMastersBySeqOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);

        // Get all the locStateMasterList where seqOrder is greater than DEFAULT_SEQ_ORDER
        defaultLocStateMasterShouldNotBeFound("seqOrder.greaterThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locStateMasterList where seqOrder is greater than SMALLER_SEQ_ORDER
        defaultLocStateMasterShouldBeFound("seqOrder.greaterThan=" + SMALLER_SEQ_ORDER);
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByLocCityMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);
        LocCityMaster locCityMaster = LocCityMasterResourceIT.createEntity(em);
        em.persist(locCityMaster);
        em.flush();
        locStateMaster.addLocCityMaster(locCityMaster);
        locStateMasterRepository.saveAndFlush(locStateMaster);
        Long locCityMasterId = locCityMaster.getId();

        // Get all the locStateMasterList where locCityMaster equals to locCityMasterId
        defaultLocStateMasterShouldBeFound("locCityMasterId.equals=" + locCityMasterId);

        // Get all the locStateMasterList where locCityMaster equals to locCityMasterId + 1
        defaultLocStateMasterShouldNotBeFound("locCityMasterId.equals=" + (locCityMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByLocDistrictMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);
        LocDistrictMaster locDistrictMaster = LocDistrictMasterResourceIT.createEntity(em);
        em.persist(locDistrictMaster);
        em.flush();
        locStateMaster.addLocDistrictMaster(locDistrictMaster);
        locStateMasterRepository.saveAndFlush(locStateMaster);
        Long locDistrictMasterId = locDistrictMaster.getId();

        // Get all the locStateMasterList where locDistrictMaster equals to locDistrictMasterId
        defaultLocStateMasterShouldBeFound("locDistrictMasterId.equals=" + locDistrictMasterId);

        // Get all the locStateMasterList where locDistrictMaster equals to locDistrictMasterId + 1
        defaultLocStateMasterShouldNotBeFound("locDistrictMasterId.equals=" + (locDistrictMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByLocPincodeMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);
        LocPincodeMaster locPincodeMaster = LocPincodeMasterResourceIT.createEntity(em);
        em.persist(locPincodeMaster);
        em.flush();
        locStateMaster.addLocPincodeMaster(locPincodeMaster);
        locStateMasterRepository.saveAndFlush(locStateMaster);
        Long locPincodeMasterId = locPincodeMaster.getId();

        // Get all the locStateMasterList where locPincodeMaster equals to locPincodeMasterId
        defaultLocStateMasterShouldBeFound("locPincodeMasterId.equals=" + locPincodeMasterId);

        // Get all the locStateMasterList where locPincodeMaster equals to locPincodeMasterId + 1
        defaultLocStateMasterShouldNotBeFound("locPincodeMasterId.equals=" + (locPincodeMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByRegionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);
        LocRegionMaster regionId = LocRegionMasterResourceIT.createEntity(em);
        em.persist(regionId);
        em.flush();
        locStateMaster.setRegionId(regionId);
        locStateMasterRepository.saveAndFlush(locStateMaster);
        Long regionIdId = regionId.getId();

        // Get all the locStateMasterList where regionId equals to regionIdId
        defaultLocStateMasterShouldBeFound("regionIdId.equals=" + regionIdId);

        // Get all the locStateMasterList where regionId equals to regionIdId + 1
        defaultLocStateMasterShouldNotBeFound("regionIdId.equals=" + (regionIdId + 1));
    }


    @Test
    @Transactional
    public void getAllLocStateMastersByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        locStateMasterRepository.saveAndFlush(locStateMaster);
        LocCountryMaster country = LocCountryMasterResourceIT.createEntity(em);
        em.persist(country);
        em.flush();
        locStateMaster.setCountry(country);
        locStateMasterRepository.saveAndFlush(locStateMaster);
        Long countryId = country.getId();

        // Get all the locStateMasterList where country equals to countryId
        defaultLocStateMasterShouldBeFound("countryId.equals=" + countryId);

        // Get all the locStateMasterList where country equals to countryId + 1
        defaultLocStateMasterShouldNotBeFound("countryId.equals=" + (countryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocStateMasterShouldBeFound(String filter) throws Exception {
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locStateMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].stateId").value(hasItem(DEFAULT_STATE_ID)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].stateCode").value(hasItem(DEFAULT_STATE_CODE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));

        // Check, that the count call also returns 1
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocStateMasterShouldNotBeFound(String filter) throws Exception {
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocStateMaster() throws Exception {
        // Get the locStateMaster
        restLocStateMasterMockMvc.perform(get("/api/loc-state-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocStateMaster() throws Exception {
        // Initialize the database
        locStateMasterService.save(locStateMaster);

        int databaseSizeBeforeUpdate = locStateMasterRepository.findAll().size();

        // Update the locStateMaster
        LocStateMaster updatedLocStateMaster = locStateMasterRepository.findById(locStateMaster.getId()).get();
        // Disconnect from session so that the updates on updatedLocStateMaster are not directly saved in db
        em.detach(updatedLocStateMaster);
        updatedLocStateMaster
            .stateName(UPDATED_STATE_NAME)
            .description(UPDATED_DESCRIPTION)
            .stateId(UPDATED_STATE_ID)
            .isDeleted(UPDATED_IS_DELETED)
            .countryCode(UPDATED_COUNTRY_CODE)
            .stateCode(UPDATED_STATE_CODE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);

        restLocStateMasterMockMvc.perform(put("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocStateMaster)))
            .andExpect(status().isOk());

        // Validate the LocStateMaster in the database
        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeUpdate);
        LocStateMaster testLocStateMaster = locStateMasterList.get(locStateMasterList.size() - 1);
        assertThat(testLocStateMaster.getStateName()).isEqualTo(UPDATED_STATE_NAME);
        assertThat(testLocStateMaster.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testLocStateMaster.getStateId()).isEqualTo(UPDATED_STATE_ID);
        assertThat(testLocStateMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocStateMaster.getCountryCode()).isEqualTo(UPDATED_COUNTRY_CODE);
        assertThat(testLocStateMaster.getStateCode()).isEqualTo(UPDATED_STATE_CODE);
        assertThat(testLocStateMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocStateMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocStateMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testLocStateMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocStateMaster.getIsApproved()).isEqualTo(UPDATED_IS_APPROVED);
        assertThat(testLocStateMaster.getSeqOrder()).isEqualTo(UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void updateNonExistingLocStateMaster() throws Exception {
        int databaseSizeBeforeUpdate = locStateMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocStateMasterMockMvc.perform(put("/api/loc-state-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locStateMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocStateMaster in the database
        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocStateMaster() throws Exception {
        // Initialize the database
        locStateMasterService.save(locStateMaster);

        int databaseSizeBeforeDelete = locStateMasterRepository.findAll().size();

        // Delete the locStateMaster
        restLocStateMasterMockMvc.perform(delete("/api/loc-state-masters/{id}", locStateMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocStateMaster> locStateMasterList = locStateMasterRepository.findAll();
        assertThat(locStateMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
