package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyChangeRequest;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.CompanyChangeRequestRepository;
import com.crisil.onesource.service.CompanyChangeRequestService;
import com.crisil.onesource.service.dto.CompanyChangeRequestCriteria;
import com.crisil.onesource.service.CompanyChangeRequestQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyChangeRequestResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyChangeRequestResourceIT {

    private static final String DEFAULT_OLD_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_OLD_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NEW_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NEW_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_NEW_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_NEW_COMPANY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_EFFECTIVE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EFFECTIVE_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_EFFECTIVE_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_REQUESTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_CHANGE_ID = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CHANGE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CASE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CASE_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_REASON_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_REASON_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_MODIFIED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_LAST_MODIFIED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    @Autowired
    private CompanyChangeRequestRepository companyChangeRequestRepository;

    @Autowired
    private CompanyChangeRequestService companyChangeRequestService;

    @Autowired
    private CompanyChangeRequestQueryService companyChangeRequestQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyChangeRequestMockMvc;

    private CompanyChangeRequest companyChangeRequest;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyChangeRequest createEntity(EntityManager em) {
        CompanyChangeRequest companyChangeRequest = new CompanyChangeRequest()
            .oldCompanyName(DEFAULT_OLD_COMPANY_NAME)
            .newCompanyName(DEFAULT_NEW_COMPANY_NAME)
            .newCompanyCode(DEFAULT_NEW_COMPANY_CODE)
            .status(DEFAULT_STATUS)
            .effectiveDate(DEFAULT_EFFECTIVE_DATE)
            .requestedBy(DEFAULT_REQUESTED_BY)
            .companyChangeId(DEFAULT_COMPANY_CHANGE_ID)
            .caseType(DEFAULT_CASE_TYPE)
            .reason(DEFAULT_REASON)
            .reasonDescription(DEFAULT_REASON_DESCRIPTION)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .isDeleted(DEFAULT_IS_DELETED);
        // Add required entity
        OnesourceCompanyMaster onesourceCompanyMaster;
        if (TestUtil.findAll(em, OnesourceCompanyMaster.class).isEmpty()) {
            onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createEntity(em);
            em.persist(onesourceCompanyMaster);
            em.flush();
        } else {
            onesourceCompanyMaster = TestUtil.findAll(em, OnesourceCompanyMaster.class).get(0);
        }
        companyChangeRequest.setOldCompanyCode(onesourceCompanyMaster);
        return companyChangeRequest;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyChangeRequest createUpdatedEntity(EntityManager em) {
        CompanyChangeRequest companyChangeRequest = new CompanyChangeRequest()
            .oldCompanyName(UPDATED_OLD_COMPANY_NAME)
            .newCompanyName(UPDATED_NEW_COMPANY_NAME)
            .newCompanyCode(UPDATED_NEW_COMPANY_CODE)
            .status(UPDATED_STATUS)
            .effectiveDate(UPDATED_EFFECTIVE_DATE)
            .requestedBy(UPDATED_REQUESTED_BY)
            .companyChangeId(UPDATED_COMPANY_CHANGE_ID)
            .caseType(UPDATED_CASE_TYPE)
            .reason(UPDATED_REASON)
            .reasonDescription(UPDATED_REASON_DESCRIPTION)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);
        // Add required entity
        OnesourceCompanyMaster onesourceCompanyMaster;
        if (TestUtil.findAll(em, OnesourceCompanyMaster.class).isEmpty()) {
            onesourceCompanyMaster = OnesourceCompanyMasterResourceIT.createUpdatedEntity(em);
            em.persist(onesourceCompanyMaster);
            em.flush();
        } else {
            onesourceCompanyMaster = TestUtil.findAll(em, OnesourceCompanyMaster.class).get(0);
        }
        companyChangeRequest.setOldCompanyCode(onesourceCompanyMaster);
        return companyChangeRequest;
    }

    @BeforeEach
    public void initTest() {
        companyChangeRequest = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyChangeRequest() throws Exception {
        int databaseSizeBeforeCreate = companyChangeRequestRepository.findAll().size();
        // Create the CompanyChangeRequest
        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isCreated());

        // Validate the CompanyChangeRequest in the database
        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyChangeRequest testCompanyChangeRequest = companyChangeRequestList.get(companyChangeRequestList.size() - 1);
        assertThat(testCompanyChangeRequest.getOldCompanyName()).isEqualTo(DEFAULT_OLD_COMPANY_NAME);
        assertThat(testCompanyChangeRequest.getNewCompanyName()).isEqualTo(DEFAULT_NEW_COMPANY_NAME);
        assertThat(testCompanyChangeRequest.getNewCompanyCode()).isEqualTo(DEFAULT_NEW_COMPANY_CODE);
        assertThat(testCompanyChangeRequest.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCompanyChangeRequest.getEffectiveDate()).isEqualTo(DEFAULT_EFFECTIVE_DATE);
        assertThat(testCompanyChangeRequest.getRequestedBy()).isEqualTo(DEFAULT_REQUESTED_BY);
        assertThat(testCompanyChangeRequest.getCompanyChangeId()).isEqualTo(DEFAULT_COMPANY_CHANGE_ID);
        assertThat(testCompanyChangeRequest.getCaseType()).isEqualTo(DEFAULT_CASE_TYPE);
        assertThat(testCompanyChangeRequest.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testCompanyChangeRequest.getReasonDescription()).isEqualTo(DEFAULT_REASON_DESCRIPTION);
        assertThat(testCompanyChangeRequest.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyChangeRequest.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyChangeRequest.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanyChangeRequest.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyChangeRequest.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
    }

    @Test
    @Transactional
    public void createCompanyChangeRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyChangeRequestRepository.findAll().size();

        // Create the CompanyChangeRequest with an existing ID
        companyChangeRequest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyChangeRequest in the database
        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkOldCompanyNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setOldCompanyName(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNewCompanyNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setNewCompanyName(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNewCompanyCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setNewCompanyCode(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setStatus(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEffectiveDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setEffectiveDate(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRequestedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setRequestedBy(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCaseTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyChangeRequestRepository.findAll().size();
        // set the field null
        companyChangeRequest.setCaseType(null);

        // Create the CompanyChangeRequest, which fails.


        restCompanyChangeRequestMockMvc.perform(post("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequests() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyChangeRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].oldCompanyName").value(hasItem(DEFAULT_OLD_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].newCompanyName").value(hasItem(DEFAULT_NEW_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].newCompanyCode").value(hasItem(DEFAULT_NEW_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].effectiveDate").value(hasItem(DEFAULT_EFFECTIVE_DATE.toString())))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].companyChangeId").value(hasItem(DEFAULT_COMPANY_CHANGE_ID)))
            .andExpect(jsonPath("$.[*].caseType").value(hasItem(DEFAULT_CASE_TYPE)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].reasonDescription").value(hasItem(DEFAULT_REASON_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));
    }
    
    @Test
    @Transactional
    public void getCompanyChangeRequest() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get the companyChangeRequest
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests/{id}", companyChangeRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyChangeRequest.getId().intValue()))
            .andExpect(jsonPath("$.oldCompanyName").value(DEFAULT_OLD_COMPANY_NAME))
            .andExpect(jsonPath("$.newCompanyName").value(DEFAULT_NEW_COMPANY_NAME))
            .andExpect(jsonPath("$.newCompanyCode").value(DEFAULT_NEW_COMPANY_CODE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.effectiveDate").value(DEFAULT_EFFECTIVE_DATE.toString()))
            .andExpect(jsonPath("$.requestedBy").value(DEFAULT_REQUESTED_BY))
            .andExpect(jsonPath("$.companyChangeId").value(DEFAULT_COMPANY_CHANGE_ID))
            .andExpect(jsonPath("$.caseType").value(DEFAULT_CASE_TYPE))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON))
            .andExpect(jsonPath("$.reasonDescription").value(DEFAULT_REASON_DESCRIPTION))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED));
    }


    @Test
    @Transactional
    public void getCompanyChangeRequestsByIdFiltering() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        Long id = companyChangeRequest.getId();

        defaultCompanyChangeRequestShouldBeFound("id.equals=" + id);
        defaultCompanyChangeRequestShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyChangeRequestShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyChangeRequestShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyChangeRequestShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyChangeRequestShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where oldCompanyName equals to DEFAULT_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("oldCompanyName.equals=" + DEFAULT_OLD_COMPANY_NAME);

        // Get all the companyChangeRequestList where oldCompanyName equals to UPDATED_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyName.equals=" + UPDATED_OLD_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where oldCompanyName not equals to DEFAULT_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyName.notEquals=" + DEFAULT_OLD_COMPANY_NAME);

        // Get all the companyChangeRequestList where oldCompanyName not equals to UPDATED_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("oldCompanyName.notEquals=" + UPDATED_OLD_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where oldCompanyName in DEFAULT_OLD_COMPANY_NAME or UPDATED_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("oldCompanyName.in=" + DEFAULT_OLD_COMPANY_NAME + "," + UPDATED_OLD_COMPANY_NAME);

        // Get all the companyChangeRequestList where oldCompanyName equals to UPDATED_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyName.in=" + UPDATED_OLD_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where oldCompanyName is not null
        defaultCompanyChangeRequestShouldBeFound("oldCompanyName.specified=true");

        // Get all the companyChangeRequestList where oldCompanyName is null
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where oldCompanyName contains DEFAULT_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("oldCompanyName.contains=" + DEFAULT_OLD_COMPANY_NAME);

        // Get all the companyChangeRequestList where oldCompanyName contains UPDATED_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyName.contains=" + UPDATED_OLD_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where oldCompanyName does not contain DEFAULT_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyName.doesNotContain=" + DEFAULT_OLD_COMPANY_NAME);

        // Get all the companyChangeRequestList where oldCompanyName does not contain UPDATED_OLD_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("oldCompanyName.doesNotContain=" + UPDATED_OLD_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyName equals to DEFAULT_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("newCompanyName.equals=" + DEFAULT_NEW_COMPANY_NAME);

        // Get all the companyChangeRequestList where newCompanyName equals to UPDATED_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyName.equals=" + UPDATED_NEW_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyName not equals to DEFAULT_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyName.notEquals=" + DEFAULT_NEW_COMPANY_NAME);

        // Get all the companyChangeRequestList where newCompanyName not equals to UPDATED_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("newCompanyName.notEquals=" + UPDATED_NEW_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyName in DEFAULT_NEW_COMPANY_NAME or UPDATED_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("newCompanyName.in=" + DEFAULT_NEW_COMPANY_NAME + "," + UPDATED_NEW_COMPANY_NAME);

        // Get all the companyChangeRequestList where newCompanyName equals to UPDATED_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyName.in=" + UPDATED_NEW_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyName is not null
        defaultCompanyChangeRequestShouldBeFound("newCompanyName.specified=true");

        // Get all the companyChangeRequestList where newCompanyName is null
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyName contains DEFAULT_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("newCompanyName.contains=" + DEFAULT_NEW_COMPANY_NAME);

        // Get all the companyChangeRequestList where newCompanyName contains UPDATED_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyName.contains=" + UPDATED_NEW_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyName does not contain DEFAULT_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyName.doesNotContain=" + DEFAULT_NEW_COMPANY_NAME);

        // Get all the companyChangeRequestList where newCompanyName does not contain UPDATED_NEW_COMPANY_NAME
        defaultCompanyChangeRequestShouldBeFound("newCompanyName.doesNotContain=" + UPDATED_NEW_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyCode equals to DEFAULT_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldBeFound("newCompanyCode.equals=" + DEFAULT_NEW_COMPANY_CODE);

        // Get all the companyChangeRequestList where newCompanyCode equals to UPDATED_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyCode.equals=" + UPDATED_NEW_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyCode not equals to DEFAULT_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyCode.notEquals=" + DEFAULT_NEW_COMPANY_CODE);

        // Get all the companyChangeRequestList where newCompanyCode not equals to UPDATED_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldBeFound("newCompanyCode.notEquals=" + UPDATED_NEW_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyCode in DEFAULT_NEW_COMPANY_CODE or UPDATED_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldBeFound("newCompanyCode.in=" + DEFAULT_NEW_COMPANY_CODE + "," + UPDATED_NEW_COMPANY_CODE);

        // Get all the companyChangeRequestList where newCompanyCode equals to UPDATED_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyCode.in=" + UPDATED_NEW_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyCode is not null
        defaultCompanyChangeRequestShouldBeFound("newCompanyCode.specified=true");

        // Get all the companyChangeRequestList where newCompanyCode is null
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyCode contains DEFAULT_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldBeFound("newCompanyCode.contains=" + DEFAULT_NEW_COMPANY_CODE);

        // Get all the companyChangeRequestList where newCompanyCode contains UPDATED_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyCode.contains=" + UPDATED_NEW_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByNewCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where newCompanyCode does not contain DEFAULT_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldNotBeFound("newCompanyCode.doesNotContain=" + DEFAULT_NEW_COMPANY_CODE);

        // Get all the companyChangeRequestList where newCompanyCode does not contain UPDATED_NEW_COMPANY_CODE
        defaultCompanyChangeRequestShouldBeFound("newCompanyCode.doesNotContain=" + UPDATED_NEW_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where status equals to DEFAULT_STATUS
        defaultCompanyChangeRequestShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the companyChangeRequestList where status equals to UPDATED_STATUS
        defaultCompanyChangeRequestShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where status not equals to DEFAULT_STATUS
        defaultCompanyChangeRequestShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the companyChangeRequestList where status not equals to UPDATED_STATUS
        defaultCompanyChangeRequestShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCompanyChangeRequestShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the companyChangeRequestList where status equals to UPDATED_STATUS
        defaultCompanyChangeRequestShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where status is not null
        defaultCompanyChangeRequestShouldBeFound("status.specified=true");

        // Get all the companyChangeRequestList where status is null
        defaultCompanyChangeRequestShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByStatusContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where status contains DEFAULT_STATUS
        defaultCompanyChangeRequestShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the companyChangeRequestList where status contains UPDATED_STATUS
        defaultCompanyChangeRequestShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where status does not contain DEFAULT_STATUS
        defaultCompanyChangeRequestShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the companyChangeRequestList where status does not contain UPDATED_STATUS
        defaultCompanyChangeRequestShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate equals to DEFAULT_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.equals=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate equals to UPDATED_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.equals=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate not equals to DEFAULT_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.notEquals=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate not equals to UPDATED_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.notEquals=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate in DEFAULT_EFFECTIVE_DATE or UPDATED_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.in=" + DEFAULT_EFFECTIVE_DATE + "," + UPDATED_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate equals to UPDATED_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.in=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate is not null
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.specified=true");

        // Get all the companyChangeRequestList where effectiveDate is null
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate is greater than or equal to DEFAULT_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.greaterThanOrEqual=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate is greater than or equal to UPDATED_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.greaterThanOrEqual=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate is less than or equal to DEFAULT_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.lessThanOrEqual=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate is less than or equal to SMALLER_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.lessThanOrEqual=" + SMALLER_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate is less than DEFAULT_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.lessThan=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate is less than UPDATED_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.lessThan=" + UPDATED_EFFECTIVE_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByEffectiveDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where effectiveDate is greater than DEFAULT_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldNotBeFound("effectiveDate.greaterThan=" + DEFAULT_EFFECTIVE_DATE);

        // Get all the companyChangeRequestList where effectiveDate is greater than SMALLER_EFFECTIVE_DATE
        defaultCompanyChangeRequestShouldBeFound("effectiveDate.greaterThan=" + SMALLER_EFFECTIVE_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByRequestedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where requestedBy equals to DEFAULT_REQUESTED_BY
        defaultCompanyChangeRequestShouldBeFound("requestedBy.equals=" + DEFAULT_REQUESTED_BY);

        // Get all the companyChangeRequestList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultCompanyChangeRequestShouldNotBeFound("requestedBy.equals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByRequestedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where requestedBy not equals to DEFAULT_REQUESTED_BY
        defaultCompanyChangeRequestShouldNotBeFound("requestedBy.notEquals=" + DEFAULT_REQUESTED_BY);

        // Get all the companyChangeRequestList where requestedBy not equals to UPDATED_REQUESTED_BY
        defaultCompanyChangeRequestShouldBeFound("requestedBy.notEquals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByRequestedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where requestedBy in DEFAULT_REQUESTED_BY or UPDATED_REQUESTED_BY
        defaultCompanyChangeRequestShouldBeFound("requestedBy.in=" + DEFAULT_REQUESTED_BY + "," + UPDATED_REQUESTED_BY);

        // Get all the companyChangeRequestList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultCompanyChangeRequestShouldNotBeFound("requestedBy.in=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByRequestedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where requestedBy is not null
        defaultCompanyChangeRequestShouldBeFound("requestedBy.specified=true");

        // Get all the companyChangeRequestList where requestedBy is null
        defaultCompanyChangeRequestShouldNotBeFound("requestedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByRequestedByContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where requestedBy contains DEFAULT_REQUESTED_BY
        defaultCompanyChangeRequestShouldBeFound("requestedBy.contains=" + DEFAULT_REQUESTED_BY);

        // Get all the companyChangeRequestList where requestedBy contains UPDATED_REQUESTED_BY
        defaultCompanyChangeRequestShouldNotBeFound("requestedBy.contains=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByRequestedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where requestedBy does not contain DEFAULT_REQUESTED_BY
        defaultCompanyChangeRequestShouldNotBeFound("requestedBy.doesNotContain=" + DEFAULT_REQUESTED_BY);

        // Get all the companyChangeRequestList where requestedBy does not contain UPDATED_REQUESTED_BY
        defaultCompanyChangeRequestShouldBeFound("requestedBy.doesNotContain=" + UPDATED_REQUESTED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCompanyChangeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where companyChangeId equals to DEFAULT_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldBeFound("companyChangeId.equals=" + DEFAULT_COMPANY_CHANGE_ID);

        // Get all the companyChangeRequestList where companyChangeId equals to UPDATED_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldNotBeFound("companyChangeId.equals=" + UPDATED_COMPANY_CHANGE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCompanyChangeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where companyChangeId not equals to DEFAULT_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldNotBeFound("companyChangeId.notEquals=" + DEFAULT_COMPANY_CHANGE_ID);

        // Get all the companyChangeRequestList where companyChangeId not equals to UPDATED_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldBeFound("companyChangeId.notEquals=" + UPDATED_COMPANY_CHANGE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCompanyChangeIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where companyChangeId in DEFAULT_COMPANY_CHANGE_ID or UPDATED_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldBeFound("companyChangeId.in=" + DEFAULT_COMPANY_CHANGE_ID + "," + UPDATED_COMPANY_CHANGE_ID);

        // Get all the companyChangeRequestList where companyChangeId equals to UPDATED_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldNotBeFound("companyChangeId.in=" + UPDATED_COMPANY_CHANGE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCompanyChangeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where companyChangeId is not null
        defaultCompanyChangeRequestShouldBeFound("companyChangeId.specified=true");

        // Get all the companyChangeRequestList where companyChangeId is null
        defaultCompanyChangeRequestShouldNotBeFound("companyChangeId.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCompanyChangeIdContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where companyChangeId contains DEFAULT_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldBeFound("companyChangeId.contains=" + DEFAULT_COMPANY_CHANGE_ID);

        // Get all the companyChangeRequestList where companyChangeId contains UPDATED_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldNotBeFound("companyChangeId.contains=" + UPDATED_COMPANY_CHANGE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCompanyChangeIdNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where companyChangeId does not contain DEFAULT_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldNotBeFound("companyChangeId.doesNotContain=" + DEFAULT_COMPANY_CHANGE_ID);

        // Get all the companyChangeRequestList where companyChangeId does not contain UPDATED_COMPANY_CHANGE_ID
        defaultCompanyChangeRequestShouldBeFound("companyChangeId.doesNotContain=" + UPDATED_COMPANY_CHANGE_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCaseTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where caseType equals to DEFAULT_CASE_TYPE
        defaultCompanyChangeRequestShouldBeFound("caseType.equals=" + DEFAULT_CASE_TYPE);

        // Get all the companyChangeRequestList where caseType equals to UPDATED_CASE_TYPE
        defaultCompanyChangeRequestShouldNotBeFound("caseType.equals=" + UPDATED_CASE_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCaseTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where caseType not equals to DEFAULT_CASE_TYPE
        defaultCompanyChangeRequestShouldNotBeFound("caseType.notEquals=" + DEFAULT_CASE_TYPE);

        // Get all the companyChangeRequestList where caseType not equals to UPDATED_CASE_TYPE
        defaultCompanyChangeRequestShouldBeFound("caseType.notEquals=" + UPDATED_CASE_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCaseTypeIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where caseType in DEFAULT_CASE_TYPE or UPDATED_CASE_TYPE
        defaultCompanyChangeRequestShouldBeFound("caseType.in=" + DEFAULT_CASE_TYPE + "," + UPDATED_CASE_TYPE);

        // Get all the companyChangeRequestList where caseType equals to UPDATED_CASE_TYPE
        defaultCompanyChangeRequestShouldNotBeFound("caseType.in=" + UPDATED_CASE_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCaseTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where caseType is not null
        defaultCompanyChangeRequestShouldBeFound("caseType.specified=true");

        // Get all the companyChangeRequestList where caseType is null
        defaultCompanyChangeRequestShouldNotBeFound("caseType.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCaseTypeContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where caseType contains DEFAULT_CASE_TYPE
        defaultCompanyChangeRequestShouldBeFound("caseType.contains=" + DEFAULT_CASE_TYPE);

        // Get all the companyChangeRequestList where caseType contains UPDATED_CASE_TYPE
        defaultCompanyChangeRequestShouldNotBeFound("caseType.contains=" + UPDATED_CASE_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCaseTypeNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where caseType does not contain DEFAULT_CASE_TYPE
        defaultCompanyChangeRequestShouldNotBeFound("caseType.doesNotContain=" + DEFAULT_CASE_TYPE);

        // Get all the companyChangeRequestList where caseType does not contain UPDATED_CASE_TYPE
        defaultCompanyChangeRequestShouldBeFound("caseType.doesNotContain=" + UPDATED_CASE_TYPE);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reason equals to DEFAULT_REASON
        defaultCompanyChangeRequestShouldBeFound("reason.equals=" + DEFAULT_REASON);

        // Get all the companyChangeRequestList where reason equals to UPDATED_REASON
        defaultCompanyChangeRequestShouldNotBeFound("reason.equals=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reason not equals to DEFAULT_REASON
        defaultCompanyChangeRequestShouldNotBeFound("reason.notEquals=" + DEFAULT_REASON);

        // Get all the companyChangeRequestList where reason not equals to UPDATED_REASON
        defaultCompanyChangeRequestShouldBeFound("reason.notEquals=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reason in DEFAULT_REASON or UPDATED_REASON
        defaultCompanyChangeRequestShouldBeFound("reason.in=" + DEFAULT_REASON + "," + UPDATED_REASON);

        // Get all the companyChangeRequestList where reason equals to UPDATED_REASON
        defaultCompanyChangeRequestShouldNotBeFound("reason.in=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reason is not null
        defaultCompanyChangeRequestShouldBeFound("reason.specified=true");

        // Get all the companyChangeRequestList where reason is null
        defaultCompanyChangeRequestShouldNotBeFound("reason.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reason contains DEFAULT_REASON
        defaultCompanyChangeRequestShouldBeFound("reason.contains=" + DEFAULT_REASON);

        // Get all the companyChangeRequestList where reason contains UPDATED_REASON
        defaultCompanyChangeRequestShouldNotBeFound("reason.contains=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reason does not contain DEFAULT_REASON
        defaultCompanyChangeRequestShouldNotBeFound("reason.doesNotContain=" + DEFAULT_REASON);

        // Get all the companyChangeRequestList where reason does not contain UPDATED_REASON
        defaultCompanyChangeRequestShouldBeFound("reason.doesNotContain=" + UPDATED_REASON);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reasonDescription equals to DEFAULT_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldBeFound("reasonDescription.equals=" + DEFAULT_REASON_DESCRIPTION);

        // Get all the companyChangeRequestList where reasonDescription equals to UPDATED_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldNotBeFound("reasonDescription.equals=" + UPDATED_REASON_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reasonDescription not equals to DEFAULT_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldNotBeFound("reasonDescription.notEquals=" + DEFAULT_REASON_DESCRIPTION);

        // Get all the companyChangeRequestList where reasonDescription not equals to UPDATED_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldBeFound("reasonDescription.notEquals=" + UPDATED_REASON_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reasonDescription in DEFAULT_REASON_DESCRIPTION or UPDATED_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldBeFound("reasonDescription.in=" + DEFAULT_REASON_DESCRIPTION + "," + UPDATED_REASON_DESCRIPTION);

        // Get all the companyChangeRequestList where reasonDescription equals to UPDATED_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldNotBeFound("reasonDescription.in=" + UPDATED_REASON_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reasonDescription is not null
        defaultCompanyChangeRequestShouldBeFound("reasonDescription.specified=true");

        // Get all the companyChangeRequestList where reasonDescription is null
        defaultCompanyChangeRequestShouldNotBeFound("reasonDescription.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonDescriptionContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reasonDescription contains DEFAULT_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldBeFound("reasonDescription.contains=" + DEFAULT_REASON_DESCRIPTION);

        // Get all the companyChangeRequestList where reasonDescription contains UPDATED_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldNotBeFound("reasonDescription.contains=" + UPDATED_REASON_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByReasonDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where reasonDescription does not contain DEFAULT_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldNotBeFound("reasonDescription.doesNotContain=" + DEFAULT_REASON_DESCRIPTION);

        // Get all the companyChangeRequestList where reasonDescription does not contain UPDATED_REASON_DESCRIPTION
        defaultCompanyChangeRequestShouldBeFound("reasonDescription.doesNotContain=" + UPDATED_REASON_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyChangeRequestShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyChangeRequestList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyChangeRequestShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyChangeRequestShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyChangeRequestList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyChangeRequestShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyChangeRequestShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyChangeRequestList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyChangeRequestShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdBy is not null
        defaultCompanyChangeRequestShouldBeFound("createdBy.specified=true");

        // Get all the companyChangeRequestList where createdBy is null
        defaultCompanyChangeRequestShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyChangeRequestShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyChangeRequestList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyChangeRequestShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyChangeRequestShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyChangeRequestList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyChangeRequestShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate is not null
        defaultCompanyChangeRequestShouldBeFound("createdDate.specified=true");

        // Get all the companyChangeRequestList where createdDate is null
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate is greater than or equal to DEFAULT_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.greaterThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate is greater than or equal to UPDATED_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.greaterThanOrEqual=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate is less than or equal to DEFAULT_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.lessThanOrEqual=" + DEFAULT_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate is less than or equal to SMALLER_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.lessThanOrEqual=" + SMALLER_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate is less than DEFAULT_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.lessThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate is less than UPDATED_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.lessThan=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByCreatedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where createdDate is greater than DEFAULT_CREATED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("createdDate.greaterThan=" + DEFAULT_CREATED_DATE);

        // Get all the companyChangeRequestList where createdDate is greater than SMALLER_CREATED_DATE
        defaultCompanyChangeRequestShouldBeFound("createdDate.greaterThan=" + SMALLER_CREATED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyChangeRequestList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyChangeRequestList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyChangeRequestList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedBy is not null
        defaultCompanyChangeRequestShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyChangeRequestList where lastModifiedBy is null
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyChangeRequestList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyChangeRequestList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyChangeRequestShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate is not null
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyChangeRequestList where lastModifiedDate is null
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate is greater than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.greaterThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate is greater than or equal to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.greaterThanOrEqual=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate is less than or equal to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.lessThanOrEqual=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate is less than or equal to SMALLER_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.lessThanOrEqual=" + SMALLER_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate is less than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.lessThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate is less than UPDATED_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.lessThan=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByLastModifiedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where lastModifiedDate is greater than DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldNotBeFound("lastModifiedDate.greaterThan=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyChangeRequestList where lastModifiedDate is greater than SMALLER_LAST_MODIFIED_DATE
        defaultCompanyChangeRequestShouldBeFound("lastModifiedDate.greaterThan=" + SMALLER_LAST_MODIFIED_DATE);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyChangeRequestShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyChangeRequestList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyChangeRequestShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyChangeRequestShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyChangeRequestList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyChangeRequestShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyChangeRequestShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyChangeRequestList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyChangeRequestShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where isDeleted is not null
        defaultCompanyChangeRequestShouldBeFound("isDeleted.specified=true");

        // Get all the companyChangeRequestList where isDeleted is null
        defaultCompanyChangeRequestShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyChangeRequestsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyChangeRequestShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyChangeRequestList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyChangeRequestShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);

        // Get all the companyChangeRequestList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyChangeRequestShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyChangeRequestList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyChangeRequestShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyChangeRequestsByOldCompanyCodeIsEqualToSomething() throws Exception {
        // Get already existing entity
        OnesourceCompanyMaster oldCompanyCode = companyChangeRequest.getOldCompanyCode();
        companyChangeRequestRepository.saveAndFlush(companyChangeRequest);
        Long oldCompanyCodeId = oldCompanyCode.getId();

        // Get all the companyChangeRequestList where oldCompanyCode equals to oldCompanyCodeId
        defaultCompanyChangeRequestShouldBeFound("oldCompanyCodeId.equals=" + oldCompanyCodeId);

        // Get all the companyChangeRequestList where oldCompanyCode equals to oldCompanyCodeId + 1
        defaultCompanyChangeRequestShouldNotBeFound("oldCompanyCodeId.equals=" + (oldCompanyCodeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyChangeRequestShouldBeFound(String filter) throws Exception {
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyChangeRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].oldCompanyName").value(hasItem(DEFAULT_OLD_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].newCompanyName").value(hasItem(DEFAULT_NEW_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].newCompanyCode").value(hasItem(DEFAULT_NEW_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].effectiveDate").value(hasItem(DEFAULT_EFFECTIVE_DATE.toString())))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].companyChangeId").value(hasItem(DEFAULT_COMPANY_CHANGE_ID)))
            .andExpect(jsonPath("$.[*].caseType").value(hasItem(DEFAULT_CASE_TYPE)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].reasonDescription").value(hasItem(DEFAULT_REASON_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)));

        // Check, that the count call also returns 1
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyChangeRequestShouldNotBeFound(String filter) throws Exception {
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyChangeRequest() throws Exception {
        // Get the companyChangeRequest
        restCompanyChangeRequestMockMvc.perform(get("/api/company-change-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyChangeRequest() throws Exception {
        // Initialize the database
        companyChangeRequestService.save(companyChangeRequest);

        int databaseSizeBeforeUpdate = companyChangeRequestRepository.findAll().size();

        // Update the companyChangeRequest
        CompanyChangeRequest updatedCompanyChangeRequest = companyChangeRequestRepository.findById(companyChangeRequest.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyChangeRequest are not directly saved in db
        em.detach(updatedCompanyChangeRequest);
        updatedCompanyChangeRequest
            .oldCompanyName(UPDATED_OLD_COMPANY_NAME)
            .newCompanyName(UPDATED_NEW_COMPANY_NAME)
            .newCompanyCode(UPDATED_NEW_COMPANY_CODE)
            .status(UPDATED_STATUS)
            .effectiveDate(UPDATED_EFFECTIVE_DATE)
            .requestedBy(UPDATED_REQUESTED_BY)
            .companyChangeId(UPDATED_COMPANY_CHANGE_ID)
            .caseType(UPDATED_CASE_TYPE)
            .reason(UPDATED_REASON)
            .reasonDescription(UPDATED_REASON_DESCRIPTION)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .isDeleted(UPDATED_IS_DELETED);

        restCompanyChangeRequestMockMvc.perform(put("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyChangeRequest)))
            .andExpect(status().isOk());

        // Validate the CompanyChangeRequest in the database
        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeUpdate);
        CompanyChangeRequest testCompanyChangeRequest = companyChangeRequestList.get(companyChangeRequestList.size() - 1);
        assertThat(testCompanyChangeRequest.getOldCompanyName()).isEqualTo(UPDATED_OLD_COMPANY_NAME);
        assertThat(testCompanyChangeRequest.getNewCompanyName()).isEqualTo(UPDATED_NEW_COMPANY_NAME);
        assertThat(testCompanyChangeRequest.getNewCompanyCode()).isEqualTo(UPDATED_NEW_COMPANY_CODE);
        assertThat(testCompanyChangeRequest.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCompanyChangeRequest.getEffectiveDate()).isEqualTo(UPDATED_EFFECTIVE_DATE);
        assertThat(testCompanyChangeRequest.getRequestedBy()).isEqualTo(UPDATED_REQUESTED_BY);
        assertThat(testCompanyChangeRequest.getCompanyChangeId()).isEqualTo(UPDATED_COMPANY_CHANGE_ID);
        assertThat(testCompanyChangeRequest.getCaseType()).isEqualTo(UPDATED_CASE_TYPE);
        assertThat(testCompanyChangeRequest.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testCompanyChangeRequest.getReasonDescription()).isEqualTo(UPDATED_REASON_DESCRIPTION);
        assertThat(testCompanyChangeRequest.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyChangeRequest.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyChangeRequest.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanyChangeRequest.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyChangeRequest.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyChangeRequest() throws Exception {
        int databaseSizeBeforeUpdate = companyChangeRequestRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyChangeRequestMockMvc.perform(put("/api/company-change-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyChangeRequest)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyChangeRequest in the database
        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyChangeRequest() throws Exception {
        // Initialize the database
        companyChangeRequestService.save(companyChangeRequest);

        int databaseSizeBeforeDelete = companyChangeRequestRepository.findAll().size();

        // Delete the companyChangeRequest
        restCompanyChangeRequestMockMvc.perform(delete("/api/company-change-requests/{id}", companyChangeRequest.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyChangeRequest> companyChangeRequestList = companyChangeRequestRepository.findAll();
        assertThat(companyChangeRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
