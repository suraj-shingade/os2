package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.OnesourceCompanyMasterHistory;
import com.crisil.onesource.repository.OnesourceCompanyMasterHistoryRepository;
import com.crisil.onesource.service.OnesourceCompanyMasterHistoryService;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterHistoryCriteria;
import com.crisil.onesource.service.OnesourceCompanyMasterHistoryQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OnesourceCompanyMasterHistoryResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OnesourceCompanyMasterHistoryResourceIT {

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_COMPANY_TYPE_ID = 1;
    private static final Integer UPDATED_COMPANY_TYPE_ID = 2;
    private static final Integer SMALLER_COMPANY_TYPE_ID = 1 - 1;

    private static final Integer DEFAULT_SECTOR_ID = 1;
    private static final Integer UPDATED_SECTOR_ID = 2;
    private static final Integer SMALLER_SECTOR_ID = 1 - 1;

    private static final Integer DEFAULT_INDUSTRY_ID = 1;
    private static final Integer UPDATED_INDUSTRY_ID = 2;
    private static final Integer SMALLER_INDUSTRY_ID = 1 - 1;

    private static final Integer DEFAULT_GROUP_ID = 1;
    private static final Integer UPDATED_GROUP_ID = 2;
    private static final Integer SMALLER_GROUP_ID = 1 - 1;

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;
    private static final Integer SMALLER_STATUS = 1 - 1;

    private static final String DEFAULT_PIN = "AAAAAAAAAA";
    private static final String UPDATED_PIN = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BILLING_TYPE = "A";
    private static final String UPDATED_BILLING_TYPE = "B";

    private static final String DEFAULT_IS_BILLABLE = "A";
    private static final String UPDATED_IS_BILLABLE = "B";

    private static final String DEFAULT_LISTING_STATUS = "A";
    private static final String UPDATED_LISTING_STATUS = "B";

    private static final Instant DEFAULT_DATE_OF_INCOOPERATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_OF_INCOOPERATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_ORIGINAL_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_FILENAME = "BBBBBBBBBB";

    @Autowired
    private OnesourceCompanyMasterHistoryRepository onesourceCompanyMasterHistoryRepository;

    @Autowired
    private OnesourceCompanyMasterHistoryService onesourceCompanyMasterHistoryService;

    @Autowired
    private OnesourceCompanyMasterHistoryQueryService onesourceCompanyMasterHistoryQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOnesourceCompanyMasterHistoryMockMvc;

    private OnesourceCompanyMasterHistory onesourceCompanyMasterHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OnesourceCompanyMasterHistory createEntity(EntityManager em) {
        OnesourceCompanyMasterHistory onesourceCompanyMasterHistory = new OnesourceCompanyMasterHistory()
            .companyCode(DEFAULT_COMPANY_CODE)
            .companyName(DEFAULT_COMPANY_NAME)
            .companyTypeId(DEFAULT_COMPANY_TYPE_ID)
            .sectorId(DEFAULT_SECTOR_ID)
            .industryId(DEFAULT_INDUSTRY_ID)
            .groupId(DEFAULT_GROUP_ID)
            .remarks(DEFAULT_REMARKS)
            .recordId(DEFAULT_RECORD_ID)
            .status(DEFAULT_STATUS)
            .pin(DEFAULT_PIN)
            .clientStatus(DEFAULT_CLIENT_STATUS)
            .displayCompanyName(DEFAULT_DISPLAY_COMPANY_NAME)
            .billingType(DEFAULT_BILLING_TYPE)
            .isBillable(DEFAULT_IS_BILLABLE)
            .listingStatus(DEFAULT_LISTING_STATUS)
            .dateOfIncooperation(DEFAULT_DATE_OF_INCOOPERATION)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isDeleted(DEFAULT_IS_DELETED)
            .originalFilename(DEFAULT_ORIGINAL_FILENAME)
            .displayFilename(DEFAULT_DISPLAY_FILENAME);
        return onesourceCompanyMasterHistory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OnesourceCompanyMasterHistory createUpdatedEntity(EntityManager em) {
        OnesourceCompanyMasterHistory onesourceCompanyMasterHistory = new OnesourceCompanyMasterHistory()
            .companyCode(UPDATED_COMPANY_CODE)
            .companyName(UPDATED_COMPANY_NAME)
            .companyTypeId(UPDATED_COMPANY_TYPE_ID)
            .sectorId(UPDATED_SECTOR_ID)
            .industryId(UPDATED_INDUSTRY_ID)
            .groupId(UPDATED_GROUP_ID)
            .remarks(UPDATED_REMARKS)
            .recordId(UPDATED_RECORD_ID)
            .status(UPDATED_STATUS)
            .pin(UPDATED_PIN)
            .clientStatus(UPDATED_CLIENT_STATUS)
            .displayCompanyName(UPDATED_DISPLAY_COMPANY_NAME)
            .billingType(UPDATED_BILLING_TYPE)
            .isBillable(UPDATED_IS_BILLABLE)
            .listingStatus(UPDATED_LISTING_STATUS)
            .dateOfIncooperation(UPDATED_DATE_OF_INCOOPERATION)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED)
            .originalFilename(UPDATED_ORIGINAL_FILENAME)
            .displayFilename(UPDATED_DISPLAY_FILENAME);
        return onesourceCompanyMasterHistory;
    }

    @BeforeEach
    public void initTest() {
        onesourceCompanyMasterHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createOnesourceCompanyMasterHistory() throws Exception {
        int databaseSizeBeforeCreate = onesourceCompanyMasterHistoryRepository.findAll().size();
        // Create the OnesourceCompanyMasterHistory
        restOnesourceCompanyMasterHistoryMockMvc.perform(post("/api/onesource-company-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMasterHistory)))
            .andExpect(status().isCreated());

        // Validate the OnesourceCompanyMasterHistory in the database
        List<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistoryList = onesourceCompanyMasterHistoryRepository.findAll();
        assertThat(onesourceCompanyMasterHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        OnesourceCompanyMasterHistory testOnesourceCompanyMasterHistory = onesourceCompanyMasterHistoryList.get(onesourceCompanyMasterHistoryList.size() - 1);
        assertThat(testOnesourceCompanyMasterHistory.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
        assertThat(testOnesourceCompanyMasterHistory.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testOnesourceCompanyMasterHistory.getCompanyTypeId()).isEqualTo(DEFAULT_COMPANY_TYPE_ID);
        assertThat(testOnesourceCompanyMasterHistory.getSectorId()).isEqualTo(DEFAULT_SECTOR_ID);
        assertThat(testOnesourceCompanyMasterHistory.getIndustryId()).isEqualTo(DEFAULT_INDUSTRY_ID);
        assertThat(testOnesourceCompanyMasterHistory.getGroupId()).isEqualTo(DEFAULT_GROUP_ID);
        assertThat(testOnesourceCompanyMasterHistory.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testOnesourceCompanyMasterHistory.getRecordId()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testOnesourceCompanyMasterHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOnesourceCompanyMasterHistory.getPin()).isEqualTo(DEFAULT_PIN);
        assertThat(testOnesourceCompanyMasterHistory.getClientStatus()).isEqualTo(DEFAULT_CLIENT_STATUS);
        assertThat(testOnesourceCompanyMasterHistory.getDisplayCompanyName()).isEqualTo(DEFAULT_DISPLAY_COMPANY_NAME);
        assertThat(testOnesourceCompanyMasterHistory.getBillingType()).isEqualTo(DEFAULT_BILLING_TYPE);
        assertThat(testOnesourceCompanyMasterHistory.getIsBillable()).isEqualTo(DEFAULT_IS_BILLABLE);
        assertThat(testOnesourceCompanyMasterHistory.getListingStatus()).isEqualTo(DEFAULT_LISTING_STATUS);
        assertThat(testOnesourceCompanyMasterHistory.getDateOfIncooperation()).isEqualTo(DEFAULT_DATE_OF_INCOOPERATION);
        assertThat(testOnesourceCompanyMasterHistory.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOnesourceCompanyMasterHistory.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testOnesourceCompanyMasterHistory.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOnesourceCompanyMasterHistory.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testOnesourceCompanyMasterHistory.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testOnesourceCompanyMasterHistory.getOriginalFilename()).isEqualTo(DEFAULT_ORIGINAL_FILENAME);
        assertThat(testOnesourceCompanyMasterHistory.getDisplayFilename()).isEqualTo(DEFAULT_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void createOnesourceCompanyMasterHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = onesourceCompanyMasterHistoryRepository.findAll().size();

        // Create the OnesourceCompanyMasterHistory with an existing ID
        onesourceCompanyMasterHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOnesourceCompanyMasterHistoryMockMvc.perform(post("/api/onesource-company-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMasterHistory)))
            .andExpect(status().isBadRequest());

        // Validate the OnesourceCompanyMasterHistory in the database
        List<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistoryList = onesourceCompanyMasterHistoryRepository.findAll();
        assertThat(onesourceCompanyMasterHistoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCompanyCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = onesourceCompanyMasterHistoryRepository.findAll().size();
        // set the field null
        onesourceCompanyMasterHistory.setCompanyCode(null);

        // Create the OnesourceCompanyMasterHistory, which fails.


        restOnesourceCompanyMasterHistoryMockMvc.perform(post("/api/onesource-company-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMasterHistory)))
            .andExpect(status().isBadRequest());

        List<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistoryList = onesourceCompanyMasterHistoryRepository.findAll();
        assertThat(onesourceCompanyMasterHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistories() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(onesourceCompanyMasterHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].companyTypeId").value(hasItem(DEFAULT_COMPANY_TYPE_ID)))
            .andExpect(jsonPath("$.[*].sectorId").value(hasItem(DEFAULT_SECTOR_ID)))
            .andExpect(jsonPath("$.[*].industryId").value(hasItem(DEFAULT_INDUSTRY_ID)))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].clientStatus").value(hasItem(DEFAULT_CLIENT_STATUS)))
            .andExpect(jsonPath("$.[*].displayCompanyName").value(hasItem(DEFAULT_DISPLAY_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].billingType").value(hasItem(DEFAULT_BILLING_TYPE)))
            .andExpect(jsonPath("$.[*].isBillable").value(hasItem(DEFAULT_IS_BILLABLE)))
            .andExpect(jsonPath("$.[*].listingStatus").value(hasItem(DEFAULT_LISTING_STATUS)))
            .andExpect(jsonPath("$.[*].dateOfIncooperation").value(hasItem(DEFAULT_DATE_OF_INCOOPERATION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].originalFilename").value(hasItem(DEFAULT_ORIGINAL_FILENAME)))
            .andExpect(jsonPath("$.[*].displayFilename").value(hasItem(DEFAULT_DISPLAY_FILENAME)));
    }
    
    @Test
    @Transactional
    public void getOnesourceCompanyMasterHistory() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get the onesourceCompanyMasterHistory
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories/{id}", onesourceCompanyMasterHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(onesourceCompanyMasterHistory.getId().intValue()))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.companyTypeId").value(DEFAULT_COMPANY_TYPE_ID))
            .andExpect(jsonPath("$.sectorId").value(DEFAULT_SECTOR_ID))
            .andExpect(jsonPath("$.industryId").value(DEFAULT_INDUSTRY_ID))
            .andExpect(jsonPath("$.groupId").value(DEFAULT_GROUP_ID))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.recordId").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.pin").value(DEFAULT_PIN))
            .andExpect(jsonPath("$.clientStatus").value(DEFAULT_CLIENT_STATUS))
            .andExpect(jsonPath("$.displayCompanyName").value(DEFAULT_DISPLAY_COMPANY_NAME))
            .andExpect(jsonPath("$.billingType").value(DEFAULT_BILLING_TYPE))
            .andExpect(jsonPath("$.isBillable").value(DEFAULT_IS_BILLABLE))
            .andExpect(jsonPath("$.listingStatus").value(DEFAULT_LISTING_STATUS))
            .andExpect(jsonPath("$.dateOfIncooperation").value(DEFAULT_DATE_OF_INCOOPERATION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.originalFilename").value(DEFAULT_ORIGINAL_FILENAME))
            .andExpect(jsonPath("$.displayFilename").value(DEFAULT_DISPLAY_FILENAME));
    }


    @Test
    @Transactional
    public void getOnesourceCompanyMasterHistoriesByIdFiltering() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        Long id = onesourceCompanyMasterHistory.getId();

        defaultOnesourceCompanyMasterHistoryShouldBeFound("id.equals=" + id);
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("id.notEquals=" + id);

        defaultOnesourceCompanyMasterHistoryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("id.greaterThan=" + id);

        defaultOnesourceCompanyMasterHistoryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterHistoryList where companyCode equals to UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterHistoryList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the onesourceCompanyMasterHistoryList where companyCode equals to UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyCode is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyCode.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where companyCode is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyCode contains DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterHistoryList where companyCode contains UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterHistoryList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyName equals to DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyName.equals=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where companyName equals to UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyName.equals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyName not equals to DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyName.notEquals=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where companyName not equals to UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyName.notEquals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyName in DEFAULT_COMPANY_NAME or UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyName.in=" + DEFAULT_COMPANY_NAME + "," + UPDATED_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where companyName equals to UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyName.in=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyName is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyName.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where companyName is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyName contains DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyName.contains=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where companyName contains UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyName.contains=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyName does not contain DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyName.doesNotContain=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where companyName does not contain UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyName.doesNotContain=" + UPDATED_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId equals to DEFAULT_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.equals=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId equals to UPDATED_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.equals=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId not equals to DEFAULT_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.notEquals=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId not equals to UPDATED_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.notEquals=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId in DEFAULT_COMPANY_TYPE_ID or UPDATED_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.in=" + DEFAULT_COMPANY_TYPE_ID + "," + UPDATED_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId equals to UPDATED_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.in=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is greater than or equal to DEFAULT_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.greaterThanOrEqual=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is greater than or equal to UPDATED_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.greaterThanOrEqual=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is less than or equal to DEFAULT_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.lessThanOrEqual=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is less than or equal to SMALLER_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.lessThanOrEqual=" + SMALLER_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsLessThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is less than DEFAULT_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.lessThan=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is less than UPDATED_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.lessThan=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCompanyTypeIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is greater than DEFAULT_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("companyTypeId.greaterThan=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the onesourceCompanyMasterHistoryList where companyTypeId is greater than SMALLER_COMPANY_TYPE_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("companyTypeId.greaterThan=" + SMALLER_COMPANY_TYPE_ID);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId equals to DEFAULT_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.equals=" + DEFAULT_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId equals to UPDATED_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.equals=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId not equals to DEFAULT_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.notEquals=" + DEFAULT_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId not equals to UPDATED_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.notEquals=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId in DEFAULT_SECTOR_ID or UPDATED_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.in=" + DEFAULT_SECTOR_ID + "," + UPDATED_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId equals to UPDATED_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.in=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where sectorId is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is greater than or equal to DEFAULT_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.greaterThanOrEqual=" + DEFAULT_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is greater than or equal to UPDATED_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.greaterThanOrEqual=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is less than or equal to DEFAULT_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.lessThanOrEqual=" + DEFAULT_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is less than or equal to SMALLER_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.lessThanOrEqual=" + SMALLER_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsLessThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is less than DEFAULT_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.lessThan=" + DEFAULT_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is less than UPDATED_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.lessThan=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesBySectorIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is greater than DEFAULT_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("sectorId.greaterThan=" + DEFAULT_SECTOR_ID);

        // Get all the onesourceCompanyMasterHistoryList where sectorId is greater than SMALLER_SECTOR_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("sectorId.greaterThan=" + SMALLER_SECTOR_ID);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId equals to DEFAULT_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.equals=" + DEFAULT_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId equals to UPDATED_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.equals=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId not equals to DEFAULT_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.notEquals=" + DEFAULT_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId not equals to UPDATED_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.notEquals=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId in DEFAULT_INDUSTRY_ID or UPDATED_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.in=" + DEFAULT_INDUSTRY_ID + "," + UPDATED_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId equals to UPDATED_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.in=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where industryId is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId is greater than or equal to DEFAULT_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.greaterThanOrEqual=" + DEFAULT_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId is greater than or equal to UPDATED_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.greaterThanOrEqual=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId is less than or equal to DEFAULT_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.lessThanOrEqual=" + DEFAULT_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId is less than or equal to SMALLER_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.lessThanOrEqual=" + SMALLER_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsLessThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId is less than DEFAULT_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.lessThan=" + DEFAULT_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId is less than UPDATED_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.lessThan=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIndustryIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where industryId is greater than DEFAULT_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("industryId.greaterThan=" + DEFAULT_INDUSTRY_ID);

        // Get all the onesourceCompanyMasterHistoryList where industryId is greater than SMALLER_INDUSTRY_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("industryId.greaterThan=" + SMALLER_INDUSTRY_ID);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId equals to DEFAULT_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.equals=" + DEFAULT_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId equals to UPDATED_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.equals=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId not equals to DEFAULT_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.notEquals=" + DEFAULT_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId not equals to UPDATED_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.notEquals=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId in DEFAULT_GROUP_ID or UPDATED_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.in=" + DEFAULT_GROUP_ID + "," + UPDATED_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId equals to UPDATED_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.in=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where groupId is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId is greater than or equal to DEFAULT_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.greaterThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId is greater than or equal to UPDATED_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.greaterThanOrEqual=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId is less than or equal to DEFAULT_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.lessThanOrEqual=" + DEFAULT_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId is less than or equal to SMALLER_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.lessThanOrEqual=" + SMALLER_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsLessThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId is less than DEFAULT_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.lessThan=" + DEFAULT_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId is less than UPDATED_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.lessThan=" + UPDATED_GROUP_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByGroupIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where groupId is greater than DEFAULT_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("groupId.greaterThan=" + DEFAULT_GROUP_ID);

        // Get all the onesourceCompanyMasterHistoryList where groupId is greater than SMALLER_GROUP_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("groupId.greaterThan=" + SMALLER_GROUP_ID);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where remarks equals to DEFAULT_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterHistoryList where remarks equals to UPDATED_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRemarksIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where remarks not equals to DEFAULT_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("remarks.notEquals=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterHistoryList where remarks not equals to UPDATED_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("remarks.notEquals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the onesourceCompanyMasterHistoryList where remarks equals to UPDATED_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where remarks is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("remarks.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where remarks is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("remarks.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRemarksContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where remarks contains DEFAULT_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("remarks.contains=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterHistoryList where remarks contains UPDATED_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("remarks.contains=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRemarksNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where remarks does not contain DEFAULT_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("remarks.doesNotContain=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterHistoryList where remarks does not contain UPDATED_REMARKS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("remarks.doesNotContain=" + UPDATED_REMARKS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where recordId equals to DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("recordId.equals=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterHistoryList where recordId equals to UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("recordId.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where recordId not equals to DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("recordId.notEquals=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterHistoryList where recordId not equals to UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("recordId.notEquals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where recordId in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("recordId.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the onesourceCompanyMasterHistoryList where recordId equals to UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("recordId.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where recordId is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("recordId.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where recordId is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("recordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRecordIdContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where recordId contains DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("recordId.contains=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterHistoryList where recordId contains UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("recordId.contains=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where recordId does not contain DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("recordId.doesNotContain=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterHistoryList where recordId does not contain UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterHistoryShouldBeFound("recordId.doesNotContain=" + UPDATED_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status equals to DEFAULT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status equals to UPDATED_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status not equals to DEFAULT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status not equals to UPDATED_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status equals to UPDATED_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where status is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status is greater than or equal to DEFAULT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.greaterThanOrEqual=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status is greater than or equal to UPDATED_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.greaterThanOrEqual=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status is less than or equal to DEFAULT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.lessThanOrEqual=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status is less than or equal to SMALLER_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.lessThanOrEqual=" + SMALLER_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status is less than DEFAULT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status is less than UPDATED_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByStatusIsGreaterThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where status is greater than DEFAULT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("status.greaterThan=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where status is greater than SMALLER_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("status.greaterThan=" + SMALLER_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByPinIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where pin equals to DEFAULT_PIN
        defaultOnesourceCompanyMasterHistoryShouldBeFound("pin.equals=" + DEFAULT_PIN);

        // Get all the onesourceCompanyMasterHistoryList where pin equals to UPDATED_PIN
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("pin.equals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByPinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where pin not equals to DEFAULT_PIN
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("pin.notEquals=" + DEFAULT_PIN);

        // Get all the onesourceCompanyMasterHistoryList where pin not equals to UPDATED_PIN
        defaultOnesourceCompanyMasterHistoryShouldBeFound("pin.notEquals=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByPinIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where pin in DEFAULT_PIN or UPDATED_PIN
        defaultOnesourceCompanyMasterHistoryShouldBeFound("pin.in=" + DEFAULT_PIN + "," + UPDATED_PIN);

        // Get all the onesourceCompanyMasterHistoryList where pin equals to UPDATED_PIN
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("pin.in=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByPinIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where pin is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("pin.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where pin is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("pin.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByPinContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where pin contains DEFAULT_PIN
        defaultOnesourceCompanyMasterHistoryShouldBeFound("pin.contains=" + DEFAULT_PIN);

        // Get all the onesourceCompanyMasterHistoryList where pin contains UPDATED_PIN
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("pin.contains=" + UPDATED_PIN);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByPinNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where pin does not contain DEFAULT_PIN
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("pin.doesNotContain=" + DEFAULT_PIN);

        // Get all the onesourceCompanyMasterHistoryList where pin does not contain UPDATED_PIN
        defaultOnesourceCompanyMasterHistoryShouldBeFound("pin.doesNotContain=" + UPDATED_PIN);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByClientStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus equals to DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("clientStatus.equals=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus equals to UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("clientStatus.equals=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByClientStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus not equals to DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("clientStatus.notEquals=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus not equals to UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("clientStatus.notEquals=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByClientStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus in DEFAULT_CLIENT_STATUS or UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("clientStatus.in=" + DEFAULT_CLIENT_STATUS + "," + UPDATED_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus equals to UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("clientStatus.in=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByClientStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("clientStatus.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where clientStatus is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("clientStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByClientStatusContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus contains DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("clientStatus.contains=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus contains UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("clientStatus.contains=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByClientStatusNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus does not contain DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("clientStatus.doesNotContain=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where clientStatus does not contain UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("clientStatus.doesNotContain=" + UPDATED_CLIENT_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName equals to DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayCompanyName.equals=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayCompanyName.equals=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName not equals to DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayCompanyName.notEquals=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName not equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayCompanyName.notEquals=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName in DEFAULT_DISPLAY_COMPANY_NAME or UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayCompanyName.in=" + DEFAULT_DISPLAY_COMPANY_NAME + "," + UPDATED_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayCompanyName.in=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayCompanyName.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayCompanyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName contains DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayCompanyName.contains=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName contains UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayCompanyName.contains=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName does not contain DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayCompanyName.doesNotContain=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterHistoryList where displayCompanyName does not contain UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayCompanyName.doesNotContain=" + UPDATED_DISPLAY_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByBillingTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where billingType equals to DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("billingType.equals=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterHistoryList where billingType equals to UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("billingType.equals=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByBillingTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where billingType not equals to DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("billingType.notEquals=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterHistoryList where billingType not equals to UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("billingType.notEquals=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByBillingTypeIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where billingType in DEFAULT_BILLING_TYPE or UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("billingType.in=" + DEFAULT_BILLING_TYPE + "," + UPDATED_BILLING_TYPE);

        // Get all the onesourceCompanyMasterHistoryList where billingType equals to UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("billingType.in=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByBillingTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where billingType is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("billingType.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where billingType is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("billingType.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByBillingTypeContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where billingType contains DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("billingType.contains=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterHistoryList where billingType contains UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("billingType.contains=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByBillingTypeNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where billingType does not contain DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("billingType.doesNotContain=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterHistoryList where billingType does not contain UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("billingType.doesNotContain=" + UPDATED_BILLING_TYPE);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsBillableIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isBillable equals to DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isBillable.equals=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterHistoryList where isBillable equals to UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isBillable.equals=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsBillableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isBillable not equals to DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isBillable.notEquals=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterHistoryList where isBillable not equals to UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isBillable.notEquals=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsBillableIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isBillable in DEFAULT_IS_BILLABLE or UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isBillable.in=" + DEFAULT_IS_BILLABLE + "," + UPDATED_IS_BILLABLE);

        // Get all the onesourceCompanyMasterHistoryList where isBillable equals to UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isBillable.in=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsBillableIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isBillable is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isBillable.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where isBillable is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isBillable.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsBillableContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isBillable contains DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isBillable.contains=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterHistoryList where isBillable contains UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isBillable.contains=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsBillableNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isBillable does not contain DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isBillable.doesNotContain=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterHistoryList where isBillable does not contain UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isBillable.doesNotContain=" + UPDATED_IS_BILLABLE);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByListingStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus equals to DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("listingStatus.equals=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus equals to UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("listingStatus.equals=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByListingStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus not equals to DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("listingStatus.notEquals=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus not equals to UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("listingStatus.notEquals=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByListingStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus in DEFAULT_LISTING_STATUS or UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("listingStatus.in=" + DEFAULT_LISTING_STATUS + "," + UPDATED_LISTING_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus equals to UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("listingStatus.in=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByListingStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("listingStatus.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where listingStatus is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("listingStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByListingStatusContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus contains DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("listingStatus.contains=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus contains UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("listingStatus.contains=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByListingStatusNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus does not contain DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("listingStatus.doesNotContain=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterHistoryList where listingStatus does not contain UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterHistoryShouldBeFound("listingStatus.doesNotContain=" + UPDATED_LISTING_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDateOfIncooperationIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation equals to DEFAULT_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterHistoryShouldBeFound("dateOfIncooperation.equals=" + DEFAULT_DATE_OF_INCOOPERATION);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation equals to UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("dateOfIncooperation.equals=" + UPDATED_DATE_OF_INCOOPERATION);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDateOfIncooperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation not equals to DEFAULT_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("dateOfIncooperation.notEquals=" + DEFAULT_DATE_OF_INCOOPERATION);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation not equals to UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterHistoryShouldBeFound("dateOfIncooperation.notEquals=" + UPDATED_DATE_OF_INCOOPERATION);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDateOfIncooperationIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation in DEFAULT_DATE_OF_INCOOPERATION or UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterHistoryShouldBeFound("dateOfIncooperation.in=" + DEFAULT_DATE_OF_INCOOPERATION + "," + UPDATED_DATE_OF_INCOOPERATION);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation equals to UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("dateOfIncooperation.in=" + UPDATED_DATE_OF_INCOOPERATION);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDateOfIncooperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("dateOfIncooperation.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where dateOfIncooperation is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("dateOfIncooperation.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdDate equals to DEFAULT_CREATED_DATE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the onesourceCompanyMasterHistoryList where createdDate equals to UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the onesourceCompanyMasterHistoryList where createdDate not equals to UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the onesourceCompanyMasterHistoryList where createdDate equals to UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdDate is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdDate.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where createdDate is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedDate.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedDate is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdBy equals to DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterHistoryList where createdBy equals to UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdBy not equals to DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterHistoryList where createdBy not equals to UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the onesourceCompanyMasterHistoryList where createdBy equals to UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdBy is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdBy.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where createdBy is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdBy contains DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterHistoryList where createdBy contains UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where createdBy does not contain DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterHistoryList where createdBy does not contain UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedBy.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterHistoryList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterHistoryShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted equals to DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted equals to UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted not equals to UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted equals to UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isDeleted.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where isDeleted is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted contains DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted contains UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterHistoryList where isDeleted does not contain UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterHistoryShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByOriginalFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename equals to DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("originalFilename.equals=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename equals to UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("originalFilename.equals=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByOriginalFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename not equals to DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("originalFilename.notEquals=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename not equals to UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("originalFilename.notEquals=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByOriginalFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename in DEFAULT_ORIGINAL_FILENAME or UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("originalFilename.in=" + DEFAULT_ORIGINAL_FILENAME + "," + UPDATED_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename equals to UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("originalFilename.in=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByOriginalFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("originalFilename.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where originalFilename is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("originalFilename.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByOriginalFilenameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename contains DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("originalFilename.contains=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename contains UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("originalFilename.contains=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByOriginalFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename does not contain DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("originalFilename.doesNotContain=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where originalFilename does not contain UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("originalFilename.doesNotContain=" + UPDATED_ORIGINAL_FILENAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename equals to DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayFilename.equals=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename equals to UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayFilename.equals=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename not equals to DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayFilename.notEquals=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename not equals to UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayFilename.notEquals=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename in DEFAULT_DISPLAY_FILENAME or UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayFilename.in=" + DEFAULT_DISPLAY_FILENAME + "," + UPDATED_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename equals to UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayFilename.in=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename is not null
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayFilename.specified=true");

        // Get all the onesourceCompanyMasterHistoryList where displayFilename is null
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayFilename.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayFilenameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename contains DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayFilename.contains=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename contains UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayFilename.contains=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasterHistoriesByDisplayFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryRepository.saveAndFlush(onesourceCompanyMasterHistory);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename does not contain DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldNotBeFound("displayFilename.doesNotContain=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterHistoryList where displayFilename does not contain UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterHistoryShouldBeFound("displayFilename.doesNotContain=" + UPDATED_DISPLAY_FILENAME);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOnesourceCompanyMasterHistoryShouldBeFound(String filter) throws Exception {
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(onesourceCompanyMasterHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].companyTypeId").value(hasItem(DEFAULT_COMPANY_TYPE_ID)))
            .andExpect(jsonPath("$.[*].sectorId").value(hasItem(DEFAULT_SECTOR_ID)))
            .andExpect(jsonPath("$.[*].industryId").value(hasItem(DEFAULT_INDUSTRY_ID)))
            .andExpect(jsonPath("$.[*].groupId").value(hasItem(DEFAULT_GROUP_ID)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].pin").value(hasItem(DEFAULT_PIN)))
            .andExpect(jsonPath("$.[*].clientStatus").value(hasItem(DEFAULT_CLIENT_STATUS)))
            .andExpect(jsonPath("$.[*].displayCompanyName").value(hasItem(DEFAULT_DISPLAY_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].billingType").value(hasItem(DEFAULT_BILLING_TYPE)))
            .andExpect(jsonPath("$.[*].isBillable").value(hasItem(DEFAULT_IS_BILLABLE)))
            .andExpect(jsonPath("$.[*].listingStatus").value(hasItem(DEFAULT_LISTING_STATUS)))
            .andExpect(jsonPath("$.[*].dateOfIncooperation").value(hasItem(DEFAULT_DATE_OF_INCOOPERATION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].originalFilename").value(hasItem(DEFAULT_ORIGINAL_FILENAME)))
            .andExpect(jsonPath("$.[*].displayFilename").value(hasItem(DEFAULT_DISPLAY_FILENAME)));

        // Check, that the count call also returns 1
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOnesourceCompanyMasterHistoryShouldNotBeFound(String filter) throws Exception {
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingOnesourceCompanyMasterHistory() throws Exception {
        // Get the onesourceCompanyMasterHistory
        restOnesourceCompanyMasterHistoryMockMvc.perform(get("/api/onesource-company-master-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOnesourceCompanyMasterHistory() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryService.save(onesourceCompanyMasterHistory);

        int databaseSizeBeforeUpdate = onesourceCompanyMasterHistoryRepository.findAll().size();

        // Update the onesourceCompanyMasterHistory
        OnesourceCompanyMasterHistory updatedOnesourceCompanyMasterHistory = onesourceCompanyMasterHistoryRepository.findById(onesourceCompanyMasterHistory.getId()).get();
        // Disconnect from session so that the updates on updatedOnesourceCompanyMasterHistory are not directly saved in db
        em.detach(updatedOnesourceCompanyMasterHistory);
        updatedOnesourceCompanyMasterHistory
            .companyCode(UPDATED_COMPANY_CODE)
            .companyName(UPDATED_COMPANY_NAME)
            .companyTypeId(UPDATED_COMPANY_TYPE_ID)
            .sectorId(UPDATED_SECTOR_ID)
            .industryId(UPDATED_INDUSTRY_ID)
            .groupId(UPDATED_GROUP_ID)
            .remarks(UPDATED_REMARKS)
            .recordId(UPDATED_RECORD_ID)
            .status(UPDATED_STATUS)
            .pin(UPDATED_PIN)
            .clientStatus(UPDATED_CLIENT_STATUS)
            .displayCompanyName(UPDATED_DISPLAY_COMPANY_NAME)
            .billingType(UPDATED_BILLING_TYPE)
            .isBillable(UPDATED_IS_BILLABLE)
            .listingStatus(UPDATED_LISTING_STATUS)
            .dateOfIncooperation(UPDATED_DATE_OF_INCOOPERATION)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED)
            .originalFilename(UPDATED_ORIGINAL_FILENAME)
            .displayFilename(UPDATED_DISPLAY_FILENAME);

        restOnesourceCompanyMasterHistoryMockMvc.perform(put("/api/onesource-company-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOnesourceCompanyMasterHistory)))
            .andExpect(status().isOk());

        // Validate the OnesourceCompanyMasterHistory in the database
        List<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistoryList = onesourceCompanyMasterHistoryRepository.findAll();
        assertThat(onesourceCompanyMasterHistoryList).hasSize(databaseSizeBeforeUpdate);
        OnesourceCompanyMasterHistory testOnesourceCompanyMasterHistory = onesourceCompanyMasterHistoryList.get(onesourceCompanyMasterHistoryList.size() - 1);
        assertThat(testOnesourceCompanyMasterHistory.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
        assertThat(testOnesourceCompanyMasterHistory.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testOnesourceCompanyMasterHistory.getCompanyTypeId()).isEqualTo(UPDATED_COMPANY_TYPE_ID);
        assertThat(testOnesourceCompanyMasterHistory.getSectorId()).isEqualTo(UPDATED_SECTOR_ID);
        assertThat(testOnesourceCompanyMasterHistory.getIndustryId()).isEqualTo(UPDATED_INDUSTRY_ID);
        assertThat(testOnesourceCompanyMasterHistory.getGroupId()).isEqualTo(UPDATED_GROUP_ID);
        assertThat(testOnesourceCompanyMasterHistory.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testOnesourceCompanyMasterHistory.getRecordId()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testOnesourceCompanyMasterHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOnesourceCompanyMasterHistory.getPin()).isEqualTo(UPDATED_PIN);
        assertThat(testOnesourceCompanyMasterHistory.getClientStatus()).isEqualTo(UPDATED_CLIENT_STATUS);
        assertThat(testOnesourceCompanyMasterHistory.getDisplayCompanyName()).isEqualTo(UPDATED_DISPLAY_COMPANY_NAME);
        assertThat(testOnesourceCompanyMasterHistory.getBillingType()).isEqualTo(UPDATED_BILLING_TYPE);
        assertThat(testOnesourceCompanyMasterHistory.getIsBillable()).isEqualTo(UPDATED_IS_BILLABLE);
        assertThat(testOnesourceCompanyMasterHistory.getListingStatus()).isEqualTo(UPDATED_LISTING_STATUS);
        assertThat(testOnesourceCompanyMasterHistory.getDateOfIncooperation()).isEqualTo(UPDATED_DATE_OF_INCOOPERATION);
        assertThat(testOnesourceCompanyMasterHistory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOnesourceCompanyMasterHistory.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testOnesourceCompanyMasterHistory.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOnesourceCompanyMasterHistory.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOnesourceCompanyMasterHistory.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testOnesourceCompanyMasterHistory.getOriginalFilename()).isEqualTo(UPDATED_ORIGINAL_FILENAME);
        assertThat(testOnesourceCompanyMasterHistory.getDisplayFilename()).isEqualTo(UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void updateNonExistingOnesourceCompanyMasterHistory() throws Exception {
        int databaseSizeBeforeUpdate = onesourceCompanyMasterHistoryRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOnesourceCompanyMasterHistoryMockMvc.perform(put("/api/onesource-company-master-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMasterHistory)))
            .andExpect(status().isBadRequest());

        // Validate the OnesourceCompanyMasterHistory in the database
        List<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistoryList = onesourceCompanyMasterHistoryRepository.findAll();
        assertThat(onesourceCompanyMasterHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOnesourceCompanyMasterHistory() throws Exception {
        // Initialize the database
        onesourceCompanyMasterHistoryService.save(onesourceCompanyMasterHistory);

        int databaseSizeBeforeDelete = onesourceCompanyMasterHistoryRepository.findAll().size();

        // Delete the onesourceCompanyMasterHistory
        restOnesourceCompanyMasterHistoryMockMvc.perform(delete("/api/onesource-company-master-histories/{id}", onesourceCompanyMasterHistory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistoryList = onesourceCompanyMasterHistoryRepository.findAll();
        assertThat(onesourceCompanyMasterHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
