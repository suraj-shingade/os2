package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.domain.CompanyChangeRequest;
import com.crisil.onesource.domain.CompanyIsinMapping;
import com.crisil.onesource.domain.CompanyListingStatus;
import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.domain.KarzaCompInformation;
import com.crisil.onesource.domain.CompanyTypeMaster;
import com.crisil.onesource.domain.CompanySectorMaster;
import com.crisil.onesource.domain.IndustryMaster;
import com.crisil.onesource.domain.GroupMaster;
import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.repository.OnesourceCompanyMasterRepository;
import com.crisil.onesource.service.OnesourceCompanyMasterService;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterCriteria;
import com.crisil.onesource.service.OnesourceCompanyMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OnesourceCompanyMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OnesourceCompanyMasterResourceIT {

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;
    private static final Integer SMALLER_STATUS = 1 - 1;

    private static final String DEFAULT_CLIENT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_BILLING_TYPE = "A";
    private static final String UPDATED_BILLING_TYPE = "B";

    private static final String DEFAULT_IS_BILLABLE = "A";
    private static final String UPDATED_IS_BILLABLE = "B";

    private static final String DEFAULT_LISTING_STATUS = "A";
    private static final String UPDATED_LISTING_STATUS = "B";

    private static final Instant DEFAULT_DATE_OF_INCOOPERATION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE_OF_INCOOPERATION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_ORIGINAL_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_STATUS = "BBBBBBBBBB";

    @Autowired
    private OnesourceCompanyMasterRepository onesourceCompanyMasterRepository;

    @Autowired
    private OnesourceCompanyMasterService onesourceCompanyMasterService;

    @Autowired
    private OnesourceCompanyMasterQueryService onesourceCompanyMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOnesourceCompanyMasterMockMvc;

    private OnesourceCompanyMaster onesourceCompanyMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OnesourceCompanyMaster createEntity(EntityManager em) {
        OnesourceCompanyMaster onesourceCompanyMaster = new OnesourceCompanyMaster()
            .companyCode(DEFAULT_COMPANY_CODE)
            .companyName(DEFAULT_COMPANY_NAME)
            .remarks(DEFAULT_REMARKS)
            .recordId(DEFAULT_RECORD_ID)
            .status(DEFAULT_STATUS)
            .clientStatus(DEFAULT_CLIENT_STATUS)
            .displayCompanyName(DEFAULT_DISPLAY_COMPANY_NAME)
            .billingType(DEFAULT_BILLING_TYPE)
            .isBillable(DEFAULT_IS_BILLABLE)
            .listingStatus(DEFAULT_LISTING_STATUS)
            .dateOfIncooperation(DEFAULT_DATE_OF_INCOOPERATION)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isDeleted(DEFAULT_IS_DELETED)
            .originalFilename(DEFAULT_ORIGINAL_FILENAME)
            .displayFilename(DEFAULT_DISPLAY_FILENAME)
            .companyStatus(DEFAULT_COMPANY_STATUS);
        return onesourceCompanyMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OnesourceCompanyMaster createUpdatedEntity(EntityManager em) {
        OnesourceCompanyMaster onesourceCompanyMaster = new OnesourceCompanyMaster()
            .companyCode(UPDATED_COMPANY_CODE)
            .companyName(UPDATED_COMPANY_NAME)
            .remarks(UPDATED_REMARKS)
            .recordId(UPDATED_RECORD_ID)
            .status(UPDATED_STATUS)
            .clientStatus(UPDATED_CLIENT_STATUS)
            .displayCompanyName(UPDATED_DISPLAY_COMPANY_NAME)
            .billingType(UPDATED_BILLING_TYPE)
            .isBillable(UPDATED_IS_BILLABLE)
            .listingStatus(UPDATED_LISTING_STATUS)
            .dateOfIncooperation(UPDATED_DATE_OF_INCOOPERATION)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED)
            .originalFilename(UPDATED_ORIGINAL_FILENAME)
            .displayFilename(UPDATED_DISPLAY_FILENAME)
            .companyStatus(UPDATED_COMPANY_STATUS);
        return onesourceCompanyMaster;
    }

    @BeforeEach
    public void initTest() {
        onesourceCompanyMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createOnesourceCompanyMaster() throws Exception {
        int databaseSizeBeforeCreate = onesourceCompanyMasterRepository.findAll().size();
        // Create the OnesourceCompanyMaster
        restOnesourceCompanyMasterMockMvc.perform(post("/api/onesource-company-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMaster)))
            .andExpect(status().isCreated());

        // Validate the OnesourceCompanyMaster in the database
        List<OnesourceCompanyMaster> onesourceCompanyMasterList = onesourceCompanyMasterRepository.findAll();
        assertThat(onesourceCompanyMasterList).hasSize(databaseSizeBeforeCreate + 1);
        OnesourceCompanyMaster testOnesourceCompanyMaster = onesourceCompanyMasterList.get(onesourceCompanyMasterList.size() - 1);
        assertThat(testOnesourceCompanyMaster.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
        assertThat(testOnesourceCompanyMaster.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testOnesourceCompanyMaster.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testOnesourceCompanyMaster.getRecordId()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testOnesourceCompanyMaster.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOnesourceCompanyMaster.getClientStatus()).isEqualTo(DEFAULT_CLIENT_STATUS);
        assertThat(testOnesourceCompanyMaster.getDisplayCompanyName()).isEqualTo(DEFAULT_DISPLAY_COMPANY_NAME);
        assertThat(testOnesourceCompanyMaster.getBillingType()).isEqualTo(DEFAULT_BILLING_TYPE);
        assertThat(testOnesourceCompanyMaster.getIsBillable()).isEqualTo(DEFAULT_IS_BILLABLE);
        assertThat(testOnesourceCompanyMaster.getListingStatus()).isEqualTo(DEFAULT_LISTING_STATUS);
        assertThat(testOnesourceCompanyMaster.getDateOfIncooperation()).isEqualTo(DEFAULT_DATE_OF_INCOOPERATION);
        assertThat(testOnesourceCompanyMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOnesourceCompanyMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testOnesourceCompanyMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOnesourceCompanyMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testOnesourceCompanyMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testOnesourceCompanyMaster.getOriginalFilename()).isEqualTo(DEFAULT_ORIGINAL_FILENAME);
        assertThat(testOnesourceCompanyMaster.getDisplayFilename()).isEqualTo(DEFAULT_DISPLAY_FILENAME);
        assertThat(testOnesourceCompanyMaster.getCompanyStatus()).isEqualTo(DEFAULT_COMPANY_STATUS);
    }

    @Test
    @Transactional
    public void createOnesourceCompanyMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = onesourceCompanyMasterRepository.findAll().size();

        // Create the OnesourceCompanyMaster with an existing ID
        onesourceCompanyMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOnesourceCompanyMasterMockMvc.perform(post("/api/onesource-company-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMaster)))
            .andExpect(status().isBadRequest());

        // Validate the OnesourceCompanyMaster in the database
        List<OnesourceCompanyMaster> onesourceCompanyMasterList = onesourceCompanyMasterRepository.findAll();
        assertThat(onesourceCompanyMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCompanyCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = onesourceCompanyMasterRepository.findAll().size();
        // set the field null
        onesourceCompanyMaster.setCompanyCode(null);

        // Create the OnesourceCompanyMaster, which fails.


        restOnesourceCompanyMasterMockMvc.perform(post("/api/onesource-company-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMaster)))
            .andExpect(status().isBadRequest());

        List<OnesourceCompanyMaster> onesourceCompanyMasterList = onesourceCompanyMasterRepository.findAll();
        assertThat(onesourceCompanyMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMasters() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(onesourceCompanyMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].clientStatus").value(hasItem(DEFAULT_CLIENT_STATUS)))
            .andExpect(jsonPath("$.[*].displayCompanyName").value(hasItem(DEFAULT_DISPLAY_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].billingType").value(hasItem(DEFAULT_BILLING_TYPE)))
            .andExpect(jsonPath("$.[*].isBillable").value(hasItem(DEFAULT_IS_BILLABLE)))
            .andExpect(jsonPath("$.[*].listingStatus").value(hasItem(DEFAULT_LISTING_STATUS)))
            .andExpect(jsonPath("$.[*].dateOfIncooperation").value(hasItem(DEFAULT_DATE_OF_INCOOPERATION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].originalFilename").value(hasItem(DEFAULT_ORIGINAL_FILENAME)))
            .andExpect(jsonPath("$.[*].displayFilename").value(hasItem(DEFAULT_DISPLAY_FILENAME)))
            .andExpect(jsonPath("$.[*].companyStatus").value(hasItem(DEFAULT_COMPANY_STATUS)));
    }
    
    @Test
    @Transactional
    public void getOnesourceCompanyMaster() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get the onesourceCompanyMaster
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters/{id}", onesourceCompanyMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(onesourceCompanyMaster.getId().intValue()))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.recordId").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.clientStatus").value(DEFAULT_CLIENT_STATUS))
            .andExpect(jsonPath("$.displayCompanyName").value(DEFAULT_DISPLAY_COMPANY_NAME))
            .andExpect(jsonPath("$.billingType").value(DEFAULT_BILLING_TYPE))
            .andExpect(jsonPath("$.isBillable").value(DEFAULT_IS_BILLABLE))
            .andExpect(jsonPath("$.listingStatus").value(DEFAULT_LISTING_STATUS))
            .andExpect(jsonPath("$.dateOfIncooperation").value(DEFAULT_DATE_OF_INCOOPERATION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.originalFilename").value(DEFAULT_ORIGINAL_FILENAME))
            .andExpect(jsonPath("$.displayFilename").value(DEFAULT_DISPLAY_FILENAME))
            .andExpect(jsonPath("$.companyStatus").value(DEFAULT_COMPANY_STATUS));
    }


    @Test
    @Transactional
    public void getOnesourceCompanyMastersByIdFiltering() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        Long id = onesourceCompanyMaster.getId();

        defaultOnesourceCompanyMasterShouldBeFound("id.equals=" + id);
        defaultOnesourceCompanyMasterShouldNotBeFound("id.notEquals=" + id);

        defaultOnesourceCompanyMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultOnesourceCompanyMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultOnesourceCompanyMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultOnesourceCompanyMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterList where companyCode equals to UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the onesourceCompanyMasterList where companyCode equals to UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyCode is not null
        defaultOnesourceCompanyMasterShouldBeFound("companyCode.specified=true");

        // Get all the onesourceCompanyMasterList where companyCode is null
        defaultOnesourceCompanyMasterShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyCode contains DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterList where companyCode contains UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the onesourceCompanyMasterList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultOnesourceCompanyMasterShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyName equals to DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("companyName.equals=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where companyName equals to UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("companyName.equals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyName not equals to DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("companyName.notEquals=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where companyName not equals to UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("companyName.notEquals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyName in DEFAULT_COMPANY_NAME or UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("companyName.in=" + DEFAULT_COMPANY_NAME + "," + UPDATED_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where companyName equals to UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("companyName.in=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyName is not null
        defaultOnesourceCompanyMasterShouldBeFound("companyName.specified=true");

        // Get all the onesourceCompanyMasterList where companyName is null
        defaultOnesourceCompanyMasterShouldNotBeFound("companyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyName contains DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("companyName.contains=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where companyName contains UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("companyName.contains=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyName does not contain DEFAULT_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("companyName.doesNotContain=" + DEFAULT_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where companyName does not contain UPDATED_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("companyName.doesNotContain=" + UPDATED_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where remarks equals to DEFAULT_REMARKS
        defaultOnesourceCompanyMasterShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterList where remarks equals to UPDATED_REMARKS
        defaultOnesourceCompanyMasterShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRemarksIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where remarks not equals to DEFAULT_REMARKS
        defaultOnesourceCompanyMasterShouldNotBeFound("remarks.notEquals=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterList where remarks not equals to UPDATED_REMARKS
        defaultOnesourceCompanyMasterShouldBeFound("remarks.notEquals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultOnesourceCompanyMasterShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the onesourceCompanyMasterList where remarks equals to UPDATED_REMARKS
        defaultOnesourceCompanyMasterShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where remarks is not null
        defaultOnesourceCompanyMasterShouldBeFound("remarks.specified=true");

        // Get all the onesourceCompanyMasterList where remarks is null
        defaultOnesourceCompanyMasterShouldNotBeFound("remarks.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRemarksContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where remarks contains DEFAULT_REMARKS
        defaultOnesourceCompanyMasterShouldBeFound("remarks.contains=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterList where remarks contains UPDATED_REMARKS
        defaultOnesourceCompanyMasterShouldNotBeFound("remarks.contains=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRemarksNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where remarks does not contain DEFAULT_REMARKS
        defaultOnesourceCompanyMasterShouldNotBeFound("remarks.doesNotContain=" + DEFAULT_REMARKS);

        // Get all the onesourceCompanyMasterList where remarks does not contain UPDATED_REMARKS
        defaultOnesourceCompanyMasterShouldBeFound("remarks.doesNotContain=" + UPDATED_REMARKS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where recordId equals to DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterShouldBeFound("recordId.equals=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterList where recordId equals to UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterShouldNotBeFound("recordId.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where recordId not equals to DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterShouldNotBeFound("recordId.notEquals=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterList where recordId not equals to UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterShouldBeFound("recordId.notEquals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where recordId in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterShouldBeFound("recordId.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the onesourceCompanyMasterList where recordId equals to UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterShouldNotBeFound("recordId.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where recordId is not null
        defaultOnesourceCompanyMasterShouldBeFound("recordId.specified=true");

        // Get all the onesourceCompanyMasterList where recordId is null
        defaultOnesourceCompanyMasterShouldNotBeFound("recordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRecordIdContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where recordId contains DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterShouldBeFound("recordId.contains=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterList where recordId contains UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterShouldNotBeFound("recordId.contains=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where recordId does not contain DEFAULT_RECORD_ID
        defaultOnesourceCompanyMasterShouldNotBeFound("recordId.doesNotContain=" + DEFAULT_RECORD_ID);

        // Get all the onesourceCompanyMasterList where recordId does not contain UPDATED_RECORD_ID
        defaultOnesourceCompanyMasterShouldBeFound("recordId.doesNotContain=" + UPDATED_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status equals to DEFAULT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterList where status equals to UPDATED_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status not equals to DEFAULT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterList where status not equals to UPDATED_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the onesourceCompanyMasterList where status equals to UPDATED_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status is not null
        defaultOnesourceCompanyMasterShouldBeFound("status.specified=true");

        // Get all the onesourceCompanyMasterList where status is null
        defaultOnesourceCompanyMasterShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status is greater than or equal to DEFAULT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.greaterThanOrEqual=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterList where status is greater than or equal to UPDATED_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.greaterThanOrEqual=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status is less than or equal to DEFAULT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.lessThanOrEqual=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterList where status is less than or equal to SMALLER_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.lessThanOrEqual=" + SMALLER_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsLessThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status is less than DEFAULT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.lessThan=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterList where status is less than UPDATED_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.lessThan=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByStatusIsGreaterThanSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where status is greater than DEFAULT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("status.greaterThan=" + DEFAULT_STATUS);

        // Get all the onesourceCompanyMasterList where status is greater than SMALLER_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("status.greaterThan=" + SMALLER_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByClientStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where clientStatus equals to DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("clientStatus.equals=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterList where clientStatus equals to UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("clientStatus.equals=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByClientStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where clientStatus not equals to DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("clientStatus.notEquals=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterList where clientStatus not equals to UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("clientStatus.notEquals=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByClientStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where clientStatus in DEFAULT_CLIENT_STATUS or UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("clientStatus.in=" + DEFAULT_CLIENT_STATUS + "," + UPDATED_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterList where clientStatus equals to UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("clientStatus.in=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByClientStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where clientStatus is not null
        defaultOnesourceCompanyMasterShouldBeFound("clientStatus.specified=true");

        // Get all the onesourceCompanyMasterList where clientStatus is null
        defaultOnesourceCompanyMasterShouldNotBeFound("clientStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByClientStatusContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where clientStatus contains DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("clientStatus.contains=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterList where clientStatus contains UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("clientStatus.contains=" + UPDATED_CLIENT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByClientStatusNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where clientStatus does not contain DEFAULT_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("clientStatus.doesNotContain=" + DEFAULT_CLIENT_STATUS);

        // Get all the onesourceCompanyMasterList where clientStatus does not contain UPDATED_CLIENT_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("clientStatus.doesNotContain=" + UPDATED_CLIENT_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayCompanyName equals to DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("displayCompanyName.equals=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where displayCompanyName equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayCompanyName.equals=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayCompanyName not equals to DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayCompanyName.notEquals=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where displayCompanyName not equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("displayCompanyName.notEquals=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayCompanyName in DEFAULT_DISPLAY_COMPANY_NAME or UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("displayCompanyName.in=" + DEFAULT_DISPLAY_COMPANY_NAME + "," + UPDATED_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where displayCompanyName equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayCompanyName.in=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayCompanyName is not null
        defaultOnesourceCompanyMasterShouldBeFound("displayCompanyName.specified=true");

        // Get all the onesourceCompanyMasterList where displayCompanyName is null
        defaultOnesourceCompanyMasterShouldNotBeFound("displayCompanyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayCompanyName contains DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("displayCompanyName.contains=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where displayCompanyName contains UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayCompanyName.contains=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayCompanyName does not contain DEFAULT_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayCompanyName.doesNotContain=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the onesourceCompanyMasterList where displayCompanyName does not contain UPDATED_DISPLAY_COMPANY_NAME
        defaultOnesourceCompanyMasterShouldBeFound("displayCompanyName.doesNotContain=" + UPDATED_DISPLAY_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByBillingTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where billingType equals to DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldBeFound("billingType.equals=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterList where billingType equals to UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldNotBeFound("billingType.equals=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByBillingTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where billingType not equals to DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldNotBeFound("billingType.notEquals=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterList where billingType not equals to UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldBeFound("billingType.notEquals=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByBillingTypeIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where billingType in DEFAULT_BILLING_TYPE or UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldBeFound("billingType.in=" + DEFAULT_BILLING_TYPE + "," + UPDATED_BILLING_TYPE);

        // Get all the onesourceCompanyMasterList where billingType equals to UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldNotBeFound("billingType.in=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByBillingTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where billingType is not null
        defaultOnesourceCompanyMasterShouldBeFound("billingType.specified=true");

        // Get all the onesourceCompanyMasterList where billingType is null
        defaultOnesourceCompanyMasterShouldNotBeFound("billingType.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByBillingTypeContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where billingType contains DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldBeFound("billingType.contains=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterList where billingType contains UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldNotBeFound("billingType.contains=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByBillingTypeNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where billingType does not contain DEFAULT_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldNotBeFound("billingType.doesNotContain=" + DEFAULT_BILLING_TYPE);

        // Get all the onesourceCompanyMasterList where billingType does not contain UPDATED_BILLING_TYPE
        defaultOnesourceCompanyMasterShouldBeFound("billingType.doesNotContain=" + UPDATED_BILLING_TYPE);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsBillableIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isBillable equals to DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldBeFound("isBillable.equals=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterList where isBillable equals to UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldNotBeFound("isBillable.equals=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsBillableIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isBillable not equals to DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldNotBeFound("isBillable.notEquals=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterList where isBillable not equals to UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldBeFound("isBillable.notEquals=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsBillableIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isBillable in DEFAULT_IS_BILLABLE or UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldBeFound("isBillable.in=" + DEFAULT_IS_BILLABLE + "," + UPDATED_IS_BILLABLE);

        // Get all the onesourceCompanyMasterList where isBillable equals to UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldNotBeFound("isBillable.in=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsBillableIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isBillable is not null
        defaultOnesourceCompanyMasterShouldBeFound("isBillable.specified=true");

        // Get all the onesourceCompanyMasterList where isBillable is null
        defaultOnesourceCompanyMasterShouldNotBeFound("isBillable.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsBillableContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isBillable contains DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldBeFound("isBillable.contains=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterList where isBillable contains UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldNotBeFound("isBillable.contains=" + UPDATED_IS_BILLABLE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsBillableNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isBillable does not contain DEFAULT_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldNotBeFound("isBillable.doesNotContain=" + DEFAULT_IS_BILLABLE);

        // Get all the onesourceCompanyMasterList where isBillable does not contain UPDATED_IS_BILLABLE
        defaultOnesourceCompanyMasterShouldBeFound("isBillable.doesNotContain=" + UPDATED_IS_BILLABLE);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByListingStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where listingStatus equals to DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("listingStatus.equals=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterList where listingStatus equals to UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("listingStatus.equals=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByListingStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where listingStatus not equals to DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("listingStatus.notEquals=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterList where listingStatus not equals to UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("listingStatus.notEquals=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByListingStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where listingStatus in DEFAULT_LISTING_STATUS or UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("listingStatus.in=" + DEFAULT_LISTING_STATUS + "," + UPDATED_LISTING_STATUS);

        // Get all the onesourceCompanyMasterList where listingStatus equals to UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("listingStatus.in=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByListingStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where listingStatus is not null
        defaultOnesourceCompanyMasterShouldBeFound("listingStatus.specified=true");

        // Get all the onesourceCompanyMasterList where listingStatus is null
        defaultOnesourceCompanyMasterShouldNotBeFound("listingStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByListingStatusContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where listingStatus contains DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("listingStatus.contains=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterList where listingStatus contains UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("listingStatus.contains=" + UPDATED_LISTING_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByListingStatusNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where listingStatus does not contain DEFAULT_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("listingStatus.doesNotContain=" + DEFAULT_LISTING_STATUS);

        // Get all the onesourceCompanyMasterList where listingStatus does not contain UPDATED_LISTING_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("listingStatus.doesNotContain=" + UPDATED_LISTING_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDateOfIncooperationIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation equals to DEFAULT_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterShouldBeFound("dateOfIncooperation.equals=" + DEFAULT_DATE_OF_INCOOPERATION);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation equals to UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterShouldNotBeFound("dateOfIncooperation.equals=" + UPDATED_DATE_OF_INCOOPERATION);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDateOfIncooperationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation not equals to DEFAULT_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterShouldNotBeFound("dateOfIncooperation.notEquals=" + DEFAULT_DATE_OF_INCOOPERATION);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation not equals to UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterShouldBeFound("dateOfIncooperation.notEquals=" + UPDATED_DATE_OF_INCOOPERATION);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDateOfIncooperationIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation in DEFAULT_DATE_OF_INCOOPERATION or UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterShouldBeFound("dateOfIncooperation.in=" + DEFAULT_DATE_OF_INCOOPERATION + "," + UPDATED_DATE_OF_INCOOPERATION);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation equals to UPDATED_DATE_OF_INCOOPERATION
        defaultOnesourceCompanyMasterShouldNotBeFound("dateOfIncooperation.in=" + UPDATED_DATE_OF_INCOOPERATION);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDateOfIncooperationIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where dateOfIncooperation is not null
        defaultOnesourceCompanyMasterShouldBeFound("dateOfIncooperation.specified=true");

        // Get all the onesourceCompanyMasterList where dateOfIncooperation is null
        defaultOnesourceCompanyMasterShouldNotBeFound("dateOfIncooperation.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultOnesourceCompanyMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the onesourceCompanyMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultOnesourceCompanyMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the onesourceCompanyMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the onesourceCompanyMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultOnesourceCompanyMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdDate is not null
        defaultOnesourceCompanyMasterShouldBeFound("createdDate.specified=true");

        // Get all the onesourceCompanyMasterList where createdDate is null
        defaultOnesourceCompanyMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the onesourceCompanyMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the onesourceCompanyMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the onesourceCompanyMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedDate is not null
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the onesourceCompanyMasterList where lastModifiedDate is null
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the onesourceCompanyMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdBy is not null
        defaultOnesourceCompanyMasterShouldBeFound("createdBy.specified=true");

        // Get all the onesourceCompanyMasterList where createdBy is null
        defaultOnesourceCompanyMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterList where createdBy contains UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the onesourceCompanyMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultOnesourceCompanyMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedBy is not null
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the onesourceCompanyMasterList where lastModifiedBy is null
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the onesourceCompanyMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultOnesourceCompanyMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the onesourceCompanyMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isDeleted is not null
        defaultOnesourceCompanyMasterShouldBeFound("isDeleted.specified=true");

        // Get all the onesourceCompanyMasterList where isDeleted is null
        defaultOnesourceCompanyMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultOnesourceCompanyMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the onesourceCompanyMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultOnesourceCompanyMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByOriginalFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where originalFilename equals to DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("originalFilename.equals=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterList where originalFilename equals to UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("originalFilename.equals=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByOriginalFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where originalFilename not equals to DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("originalFilename.notEquals=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterList where originalFilename not equals to UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("originalFilename.notEquals=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByOriginalFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where originalFilename in DEFAULT_ORIGINAL_FILENAME or UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("originalFilename.in=" + DEFAULT_ORIGINAL_FILENAME + "," + UPDATED_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterList where originalFilename equals to UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("originalFilename.in=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByOriginalFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where originalFilename is not null
        defaultOnesourceCompanyMasterShouldBeFound("originalFilename.specified=true");

        // Get all the onesourceCompanyMasterList where originalFilename is null
        defaultOnesourceCompanyMasterShouldNotBeFound("originalFilename.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByOriginalFilenameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where originalFilename contains DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("originalFilename.contains=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterList where originalFilename contains UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("originalFilename.contains=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByOriginalFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where originalFilename does not contain DEFAULT_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("originalFilename.doesNotContain=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the onesourceCompanyMasterList where originalFilename does not contain UPDATED_ORIGINAL_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("originalFilename.doesNotContain=" + UPDATED_ORIGINAL_FILENAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayFilename equals to DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("displayFilename.equals=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterList where displayFilename equals to UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayFilename.equals=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayFilename not equals to DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayFilename.notEquals=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterList where displayFilename not equals to UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("displayFilename.notEquals=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayFilename in DEFAULT_DISPLAY_FILENAME or UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("displayFilename.in=" + DEFAULT_DISPLAY_FILENAME + "," + UPDATED_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterList where displayFilename equals to UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayFilename.in=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayFilename is not null
        defaultOnesourceCompanyMasterShouldBeFound("displayFilename.specified=true");

        // Get all the onesourceCompanyMasterList where displayFilename is null
        defaultOnesourceCompanyMasterShouldNotBeFound("displayFilename.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayFilenameContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayFilename contains DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("displayFilename.contains=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterList where displayFilename contains UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayFilename.contains=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByDisplayFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where displayFilename does not contain DEFAULT_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldNotBeFound("displayFilename.doesNotContain=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the onesourceCompanyMasterList where displayFilename does not contain UPDATED_DISPLAY_FILENAME
        defaultOnesourceCompanyMasterShouldBeFound("displayFilename.doesNotContain=" + UPDATED_DISPLAY_FILENAME);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyStatus equals to DEFAULT_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("companyStatus.equals=" + DEFAULT_COMPANY_STATUS);

        // Get all the onesourceCompanyMasterList where companyStatus equals to UPDATED_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("companyStatus.equals=" + UPDATED_COMPANY_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyStatus not equals to DEFAULT_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("companyStatus.notEquals=" + DEFAULT_COMPANY_STATUS);

        // Get all the onesourceCompanyMasterList where companyStatus not equals to UPDATED_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("companyStatus.notEquals=" + UPDATED_COMPANY_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyStatusIsInShouldWork() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyStatus in DEFAULT_COMPANY_STATUS or UPDATED_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("companyStatus.in=" + DEFAULT_COMPANY_STATUS + "," + UPDATED_COMPANY_STATUS);

        // Get all the onesourceCompanyMasterList where companyStatus equals to UPDATED_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("companyStatus.in=" + UPDATED_COMPANY_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyStatus is not null
        defaultOnesourceCompanyMasterShouldBeFound("companyStatus.specified=true");

        // Get all the onesourceCompanyMasterList where companyStatus is null
        defaultOnesourceCompanyMasterShouldNotBeFound("companyStatus.specified=false");
    }
                @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyStatusContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyStatus contains DEFAULT_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("companyStatus.contains=" + DEFAULT_COMPANY_STATUS);

        // Get all the onesourceCompanyMasterList where companyStatus contains UPDATED_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("companyStatus.contains=" + UPDATED_COMPANY_STATUS);
    }

    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyStatusNotContainsSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);

        // Get all the onesourceCompanyMasterList where companyStatus does not contain DEFAULT_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldNotBeFound("companyStatus.doesNotContain=" + DEFAULT_COMPANY_STATUS);

        // Get all the onesourceCompanyMasterList where companyStatus does not contain UPDATED_COMPANY_STATUS
        defaultOnesourceCompanyMasterShouldBeFound("companyStatus.doesNotContain=" + UPDATED_COMPANY_STATUS);
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyChangeRequestIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        CompanyChangeRequest companyChangeRequest = CompanyChangeRequestResourceIT.createEntity(em);
        em.persist(companyChangeRequest);
        em.flush();
        onesourceCompanyMaster.addCompanyChangeRequest(companyChangeRequest);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long companyChangeRequestId = companyChangeRequest.getId();

        // Get all the onesourceCompanyMasterList where companyChangeRequest equals to companyChangeRequestId
        defaultOnesourceCompanyMasterShouldBeFound("companyChangeRequestId.equals=" + companyChangeRequestId);

        // Get all the onesourceCompanyMasterList where companyChangeRequest equals to companyChangeRequestId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("companyChangeRequestId.equals=" + (companyChangeRequestId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyIsinMappingIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        CompanyIsinMapping companyIsinMapping = CompanyIsinMappingResourceIT.createEntity(em);
        em.persist(companyIsinMapping);
        em.flush();
        onesourceCompanyMaster.addCompanyIsinMapping(companyIsinMapping);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long companyIsinMappingId = companyIsinMapping.getId();

        // Get all the onesourceCompanyMasterList where companyIsinMapping equals to companyIsinMappingId
        defaultOnesourceCompanyMasterShouldBeFound("companyIsinMappingId.equals=" + companyIsinMappingId);

        // Get all the onesourceCompanyMasterList where companyIsinMapping equals to companyIsinMappingId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("companyIsinMappingId.equals=" + (companyIsinMappingId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyListingStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        CompanyListingStatus companyListingStatus = CompanyListingStatusResourceIT.createEntity(em);
        em.persist(companyListingStatus);
        em.flush();
        onesourceCompanyMaster.addCompanyListingStatus(companyListingStatus);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long companyListingStatusId = companyListingStatus.getId();

        // Get all the onesourceCompanyMasterList where companyListingStatus equals to companyListingStatusId
        defaultOnesourceCompanyMasterShouldBeFound("companyListingStatusId.equals=" + companyListingStatusId);

        // Get all the onesourceCompanyMasterList where companyListingStatus equals to companyListingStatusId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("companyListingStatusId.equals=" + (companyListingStatusId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByContactMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        ContactMaster contactMaster = ContactMasterResourceIT.createEntity(em);
        em.persist(contactMaster);
        em.flush();
        onesourceCompanyMaster.addContactMaster(contactMaster);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long contactMasterId = contactMaster.getId();

        // Get all the onesourceCompanyMasterList where contactMaster equals to contactMasterId
        defaultOnesourceCompanyMasterShouldBeFound("contactMasterId.equals=" + contactMasterId);

        // Get all the onesourceCompanyMasterList where contactMaster equals to contactMasterId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("contactMasterId.equals=" + (contactMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByKarzaCompInformationIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        KarzaCompInformation karzaCompInformation = KarzaCompInformationResourceIT.createEntity(em);
        em.persist(karzaCompInformation);
        em.flush();
        onesourceCompanyMaster.addKarzaCompInformation(karzaCompInformation);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long karzaCompInformationId = karzaCompInformation.getId();

        // Get all the onesourceCompanyMasterList where karzaCompInformation equals to karzaCompInformationId
        defaultOnesourceCompanyMasterShouldBeFound("karzaCompInformationId.equals=" + karzaCompInformationId);

        // Get all the onesourceCompanyMasterList where karzaCompInformation equals to karzaCompInformationId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("karzaCompInformationId.equals=" + (karzaCompInformationId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByCompanyTypeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        CompanyTypeMaster companyTypeId = CompanyTypeMasterResourceIT.createEntity(em);
        em.persist(companyTypeId);
        em.flush();
        onesourceCompanyMaster.setCompanyTypeId(companyTypeId);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long companyTypeIdId = companyTypeId.getId();

        // Get all the onesourceCompanyMasterList where companyTypeId equals to companyTypeIdId
        defaultOnesourceCompanyMasterShouldBeFound("companyTypeIdId.equals=" + companyTypeIdId);

        // Get all the onesourceCompanyMasterList where companyTypeId equals to companyTypeIdId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("companyTypeIdId.equals=" + (companyTypeIdId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersBySectorIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        CompanySectorMaster sectorId = CompanySectorMasterResourceIT.createEntity(em);
        em.persist(sectorId);
        em.flush();
        onesourceCompanyMaster.setSectorId(sectorId);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long sectorIdId = sectorId.getId();

        // Get all the onesourceCompanyMasterList where sectorId equals to sectorIdId
        defaultOnesourceCompanyMasterShouldBeFound("sectorIdId.equals=" + sectorIdId);

        // Get all the onesourceCompanyMasterList where sectorId equals to sectorIdId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("sectorIdId.equals=" + (sectorIdId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByIndustryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        IndustryMaster industryId = IndustryMasterResourceIT.createEntity(em);
        em.persist(industryId);
        em.flush();
        onesourceCompanyMaster.setIndustryId(industryId);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long industryIdId = industryId.getId();

        // Get all the onesourceCompanyMasterList where industryId equals to industryIdId
        defaultOnesourceCompanyMasterShouldBeFound("industryIdId.equals=" + industryIdId);

        // Get all the onesourceCompanyMasterList where industryId equals to industryIdId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("industryIdId.equals=" + (industryIdId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByGroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        GroupMaster groupId = GroupMasterResourceIT.createEntity(em);
        em.persist(groupId);
        em.flush();
        onesourceCompanyMaster.setGroupId(groupId);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long groupIdId = groupId.getId();

        // Get all the onesourceCompanyMasterList where groupId equals to groupIdId
        defaultOnesourceCompanyMasterShouldBeFound("groupIdId.equals=" + groupIdId);

        // Get all the onesourceCompanyMasterList where groupId equals to groupIdId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("groupIdId.equals=" + (groupIdId + 1));
    }


    @Test
    @Transactional
    public void getAllOnesourceCompanyMastersByPinIsEqualToSomething() throws Exception {
        // Initialize the database
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        LocPincodeMaster pin = LocPincodeMasterResourceIT.createEntity(em);
        em.persist(pin);
        em.flush();
        onesourceCompanyMaster.setPin(pin);
        onesourceCompanyMasterRepository.saveAndFlush(onesourceCompanyMaster);
        Long pinId = pin.getId();

        // Get all the onesourceCompanyMasterList where pin equals to pinId
        defaultOnesourceCompanyMasterShouldBeFound("pinId.equals=" + pinId);

        // Get all the onesourceCompanyMasterList where pin equals to pinId + 1
        defaultOnesourceCompanyMasterShouldNotBeFound("pinId.equals=" + (pinId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOnesourceCompanyMasterShouldBeFound(String filter) throws Exception {
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(onesourceCompanyMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].clientStatus").value(hasItem(DEFAULT_CLIENT_STATUS)))
            .andExpect(jsonPath("$.[*].displayCompanyName").value(hasItem(DEFAULT_DISPLAY_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].billingType").value(hasItem(DEFAULT_BILLING_TYPE)))
            .andExpect(jsonPath("$.[*].isBillable").value(hasItem(DEFAULT_IS_BILLABLE)))
            .andExpect(jsonPath("$.[*].listingStatus").value(hasItem(DEFAULT_LISTING_STATUS)))
            .andExpect(jsonPath("$.[*].dateOfIncooperation").value(hasItem(DEFAULT_DATE_OF_INCOOPERATION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].originalFilename").value(hasItem(DEFAULT_ORIGINAL_FILENAME)))
            .andExpect(jsonPath("$.[*].displayFilename").value(hasItem(DEFAULT_DISPLAY_FILENAME)))
            .andExpect(jsonPath("$.[*].companyStatus").value(hasItem(DEFAULT_COMPANY_STATUS)));

        // Check, that the count call also returns 1
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOnesourceCompanyMasterShouldNotBeFound(String filter) throws Exception {
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingOnesourceCompanyMaster() throws Exception {
        // Get the onesourceCompanyMaster
        restOnesourceCompanyMasterMockMvc.perform(get("/api/onesource-company-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOnesourceCompanyMaster() throws Exception {
        // Initialize the database
        onesourceCompanyMasterService.save(onesourceCompanyMaster);

        int databaseSizeBeforeUpdate = onesourceCompanyMasterRepository.findAll().size();

        // Update the onesourceCompanyMaster
        OnesourceCompanyMaster updatedOnesourceCompanyMaster = onesourceCompanyMasterRepository.findById(onesourceCompanyMaster.getId()).get();
        // Disconnect from session so that the updates on updatedOnesourceCompanyMaster are not directly saved in db
        em.detach(updatedOnesourceCompanyMaster);
        updatedOnesourceCompanyMaster
            .companyCode(UPDATED_COMPANY_CODE)
            .companyName(UPDATED_COMPANY_NAME)
            .remarks(UPDATED_REMARKS)
            .recordId(UPDATED_RECORD_ID)
            .status(UPDATED_STATUS)
            .clientStatus(UPDATED_CLIENT_STATUS)
            .displayCompanyName(UPDATED_DISPLAY_COMPANY_NAME)
            .billingType(UPDATED_BILLING_TYPE)
            .isBillable(UPDATED_IS_BILLABLE)
            .listingStatus(UPDATED_LISTING_STATUS)
            .dateOfIncooperation(UPDATED_DATE_OF_INCOOPERATION)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isDeleted(UPDATED_IS_DELETED)
            .originalFilename(UPDATED_ORIGINAL_FILENAME)
            .displayFilename(UPDATED_DISPLAY_FILENAME)
            .companyStatus(UPDATED_COMPANY_STATUS);

        restOnesourceCompanyMasterMockMvc.perform(put("/api/onesource-company-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedOnesourceCompanyMaster)))
            .andExpect(status().isOk());

        // Validate the OnesourceCompanyMaster in the database
        List<OnesourceCompanyMaster> onesourceCompanyMasterList = onesourceCompanyMasterRepository.findAll();
        assertThat(onesourceCompanyMasterList).hasSize(databaseSizeBeforeUpdate);
        OnesourceCompanyMaster testOnesourceCompanyMaster = onesourceCompanyMasterList.get(onesourceCompanyMasterList.size() - 1);
        assertThat(testOnesourceCompanyMaster.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
        assertThat(testOnesourceCompanyMaster.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testOnesourceCompanyMaster.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testOnesourceCompanyMaster.getRecordId()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testOnesourceCompanyMaster.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOnesourceCompanyMaster.getClientStatus()).isEqualTo(UPDATED_CLIENT_STATUS);
        assertThat(testOnesourceCompanyMaster.getDisplayCompanyName()).isEqualTo(UPDATED_DISPLAY_COMPANY_NAME);
        assertThat(testOnesourceCompanyMaster.getBillingType()).isEqualTo(UPDATED_BILLING_TYPE);
        assertThat(testOnesourceCompanyMaster.getIsBillable()).isEqualTo(UPDATED_IS_BILLABLE);
        assertThat(testOnesourceCompanyMaster.getListingStatus()).isEqualTo(UPDATED_LISTING_STATUS);
        assertThat(testOnesourceCompanyMaster.getDateOfIncooperation()).isEqualTo(UPDATED_DATE_OF_INCOOPERATION);
        assertThat(testOnesourceCompanyMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOnesourceCompanyMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testOnesourceCompanyMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOnesourceCompanyMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOnesourceCompanyMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testOnesourceCompanyMaster.getOriginalFilename()).isEqualTo(UPDATED_ORIGINAL_FILENAME);
        assertThat(testOnesourceCompanyMaster.getDisplayFilename()).isEqualTo(UPDATED_DISPLAY_FILENAME);
        assertThat(testOnesourceCompanyMaster.getCompanyStatus()).isEqualTo(UPDATED_COMPANY_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingOnesourceCompanyMaster() throws Exception {
        int databaseSizeBeforeUpdate = onesourceCompanyMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOnesourceCompanyMasterMockMvc.perform(put("/api/onesource-company-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(onesourceCompanyMaster)))
            .andExpect(status().isBadRequest());

        // Validate the OnesourceCompanyMaster in the database
        List<OnesourceCompanyMaster> onesourceCompanyMasterList = onesourceCompanyMasterRepository.findAll();
        assertThat(onesourceCompanyMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOnesourceCompanyMaster() throws Exception {
        // Initialize the database
        onesourceCompanyMasterService.save(onesourceCompanyMaster);

        int databaseSizeBeforeDelete = onesourceCompanyMasterRepository.findAll().size();

        // Delete the onesourceCompanyMaster
        restOnesourceCompanyMasterMockMvc.perform(delete("/api/onesource-company-masters/{id}", onesourceCompanyMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OnesourceCompanyMaster> onesourceCompanyMasterList = onesourceCompanyMasterRepository.findAll();
        assertThat(onesourceCompanyMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
