package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.VwContactGstDetails;
import com.crisil.onesource.repository.VwContactGstDetailsRepository;
import com.crisil.onesource.service.VwContactGstDetailsService;
import com.crisil.onesource.service.dto.VwContactGstDetailsCriteria;
import com.crisil.onesource.service.VwContactGstDetailsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VwContactGstDetailsResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class VwContactGstDetailsResourceIT {

    private static final String DEFAULT_COMPANY_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_GST_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_GST_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_IS_SEZ_CONTACT_FLAG = "A";
    private static final String UPDATED_IS_SEZ_CONTACT_FLAG = "B";

    private static final String DEFAULT_STATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STATE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_GST_STATE_CODE = "";
    private static final String UPDATED_GST_STATE_CODE = "";

    private static final String DEFAULT_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ID = "BBBBBBBBBB";

    @Autowired
    private VwContactGstDetailsRepository vwContactGstDetailsRepository;

    @Autowired
    private VwContactGstDetailsService vwContactGstDetailsService;

    @Autowired
    private VwContactGstDetailsQueryService vwContactGstDetailsQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restVwContactGstDetailsMockMvc;

    private VwContactGstDetails vwContactGstDetails;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VwContactGstDetails createEntity(EntityManager em) {
        VwContactGstDetails vwContactGstDetails = new VwContactGstDetails()
            .companyRecordId(DEFAULT_COMPANY_RECORD_ID)
            .companyName(DEFAULT_COMPANY_NAME)
            .companyCode(DEFAULT_COMPANY_CODE)
            .gstNumber(DEFAULT_GST_NUMBER)
            .isSezContactFlag(DEFAULT_IS_SEZ_CONTACT_FLAG)
            .stateName(DEFAULT_STATE_NAME)
            .gstStateCode(DEFAULT_GST_STATE_CODE)
            .contactId(DEFAULT_CONTACT_ID);
        return vwContactGstDetails;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VwContactGstDetails createUpdatedEntity(EntityManager em) {
        VwContactGstDetails vwContactGstDetails = new VwContactGstDetails()
            .companyRecordId(UPDATED_COMPANY_RECORD_ID)
            .companyName(UPDATED_COMPANY_NAME)
            .companyCode(UPDATED_COMPANY_CODE)
            .gstNumber(UPDATED_GST_NUMBER)
            .isSezContactFlag(UPDATED_IS_SEZ_CONTACT_FLAG)
            .stateName(UPDATED_STATE_NAME)
            .gstStateCode(UPDATED_GST_STATE_CODE)
            .contactId(UPDATED_CONTACT_ID);
        return vwContactGstDetails;
    }

    @BeforeEach
    public void initTest() {
        vwContactGstDetails = createEntity(em);
    }

    @Test
    @Transactional
    public void createVwContactGstDetails() throws Exception {
        int databaseSizeBeforeCreate = vwContactGstDetailsRepository.findAll().size();
        // Create the VwContactGstDetails
        restVwContactGstDetailsMockMvc.perform(post("/api/vw-contact-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vwContactGstDetails)))
            .andExpect(status().isCreated());

        // Validate the VwContactGstDetails in the database
        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeCreate + 1);
        VwContactGstDetails testVwContactGstDetails = vwContactGstDetailsList.get(vwContactGstDetailsList.size() - 1);
        assertThat(testVwContactGstDetails.getCompanyRecordId()).isEqualTo(DEFAULT_COMPANY_RECORD_ID);
        assertThat(testVwContactGstDetails.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testVwContactGstDetails.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
        assertThat(testVwContactGstDetails.getGstNumber()).isEqualTo(DEFAULT_GST_NUMBER);
        assertThat(testVwContactGstDetails.getIsSezContactFlag()).isEqualTo(DEFAULT_IS_SEZ_CONTACT_FLAG);
        assertThat(testVwContactGstDetails.getStateName()).isEqualTo(DEFAULT_STATE_NAME);
        assertThat(testVwContactGstDetails.getGstStateCode()).isEqualTo(DEFAULT_GST_STATE_CODE);
        assertThat(testVwContactGstDetails.getContactId()).isEqualTo(DEFAULT_CONTACT_ID);
    }

    @Test
    @Transactional
    public void createVwContactGstDetailsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vwContactGstDetailsRepository.findAll().size();

        // Create the VwContactGstDetails with an existing ID
        vwContactGstDetails.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVwContactGstDetailsMockMvc.perform(post("/api/vw-contact-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vwContactGstDetails)))
            .andExpect(status().isBadRequest());

        // Validate the VwContactGstDetails in the database
        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCompanyCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = vwContactGstDetailsRepository.findAll().size();
        // set the field null
        vwContactGstDetails.setCompanyCode(null);

        // Create the VwContactGstDetails, which fails.


        restVwContactGstDetailsMockMvc.perform(post("/api/vw-contact-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vwContactGstDetails)))
            .andExpect(status().isBadRequest());

        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = vwContactGstDetailsRepository.findAll().size();
        // set the field null
        vwContactGstDetails.setContactId(null);

        // Create the VwContactGstDetails, which fails.


        restVwContactGstDetailsMockMvc.perform(post("/api/vw-contact-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vwContactGstDetails)))
            .andExpect(status().isBadRequest());

        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetails() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vwContactGstDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyRecordId").value(hasItem(DEFAULT_COMPANY_RECORD_ID)))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].gstNumber").value(hasItem(DEFAULT_GST_NUMBER)))
            .andExpect(jsonPath("$.[*].isSezContactFlag").value(hasItem(DEFAULT_IS_SEZ_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].gstStateCode").value(hasItem(DEFAULT_GST_STATE_CODE)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)));
    }
    
    @Test
    @Transactional
    public void getVwContactGstDetails() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get the vwContactGstDetails
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details/{id}", vwContactGstDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(vwContactGstDetails.getId().intValue()))
            .andExpect(jsonPath("$.companyRecordId").value(DEFAULT_COMPANY_RECORD_ID))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE))
            .andExpect(jsonPath("$.gstNumber").value(DEFAULT_GST_NUMBER))
            .andExpect(jsonPath("$.isSezContactFlag").value(DEFAULT_IS_SEZ_CONTACT_FLAG))
            .andExpect(jsonPath("$.stateName").value(DEFAULT_STATE_NAME))
            .andExpect(jsonPath("$.gstStateCode").value(DEFAULT_GST_STATE_CODE))
            .andExpect(jsonPath("$.contactId").value(DEFAULT_CONTACT_ID));
    }


    @Test
    @Transactional
    public void getVwContactGstDetailsByIdFiltering() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        Long id = vwContactGstDetails.getId();

        defaultVwContactGstDetailsShouldBeFound("id.equals=" + id);
        defaultVwContactGstDetailsShouldNotBeFound("id.notEquals=" + id);

        defaultVwContactGstDetailsShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultVwContactGstDetailsShouldNotBeFound("id.greaterThan=" + id);

        defaultVwContactGstDetailsShouldBeFound("id.lessThanOrEqual=" + id);
        defaultVwContactGstDetailsShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyRecordId equals to DEFAULT_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldBeFound("companyRecordId.equals=" + DEFAULT_COMPANY_RECORD_ID);

        // Get all the vwContactGstDetailsList where companyRecordId equals to UPDATED_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldNotBeFound("companyRecordId.equals=" + UPDATED_COMPANY_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyRecordId not equals to DEFAULT_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldNotBeFound("companyRecordId.notEquals=" + DEFAULT_COMPANY_RECORD_ID);

        // Get all the vwContactGstDetailsList where companyRecordId not equals to UPDATED_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldBeFound("companyRecordId.notEquals=" + UPDATED_COMPANY_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyRecordId in DEFAULT_COMPANY_RECORD_ID or UPDATED_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldBeFound("companyRecordId.in=" + DEFAULT_COMPANY_RECORD_ID + "," + UPDATED_COMPANY_RECORD_ID);

        // Get all the vwContactGstDetailsList where companyRecordId equals to UPDATED_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldNotBeFound("companyRecordId.in=" + UPDATED_COMPANY_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyRecordId is not null
        defaultVwContactGstDetailsShouldBeFound("companyRecordId.specified=true");

        // Get all the vwContactGstDetailsList where companyRecordId is null
        defaultVwContactGstDetailsShouldNotBeFound("companyRecordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyRecordIdContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyRecordId contains DEFAULT_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldBeFound("companyRecordId.contains=" + DEFAULT_COMPANY_RECORD_ID);

        // Get all the vwContactGstDetailsList where companyRecordId contains UPDATED_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldNotBeFound("companyRecordId.contains=" + UPDATED_COMPANY_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyRecordId does not contain DEFAULT_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldNotBeFound("companyRecordId.doesNotContain=" + DEFAULT_COMPANY_RECORD_ID);

        // Get all the vwContactGstDetailsList where companyRecordId does not contain UPDATED_COMPANY_RECORD_ID
        defaultVwContactGstDetailsShouldBeFound("companyRecordId.doesNotContain=" + UPDATED_COMPANY_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyName equals to DEFAULT_COMPANY_NAME
        defaultVwContactGstDetailsShouldBeFound("companyName.equals=" + DEFAULT_COMPANY_NAME);

        // Get all the vwContactGstDetailsList where companyName equals to UPDATED_COMPANY_NAME
        defaultVwContactGstDetailsShouldNotBeFound("companyName.equals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyName not equals to DEFAULT_COMPANY_NAME
        defaultVwContactGstDetailsShouldNotBeFound("companyName.notEquals=" + DEFAULT_COMPANY_NAME);

        // Get all the vwContactGstDetailsList where companyName not equals to UPDATED_COMPANY_NAME
        defaultVwContactGstDetailsShouldBeFound("companyName.notEquals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyName in DEFAULT_COMPANY_NAME or UPDATED_COMPANY_NAME
        defaultVwContactGstDetailsShouldBeFound("companyName.in=" + DEFAULT_COMPANY_NAME + "," + UPDATED_COMPANY_NAME);

        // Get all the vwContactGstDetailsList where companyName equals to UPDATED_COMPANY_NAME
        defaultVwContactGstDetailsShouldNotBeFound("companyName.in=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyName is not null
        defaultVwContactGstDetailsShouldBeFound("companyName.specified=true");

        // Get all the vwContactGstDetailsList where companyName is null
        defaultVwContactGstDetailsShouldNotBeFound("companyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyName contains DEFAULT_COMPANY_NAME
        defaultVwContactGstDetailsShouldBeFound("companyName.contains=" + DEFAULT_COMPANY_NAME);

        // Get all the vwContactGstDetailsList where companyName contains UPDATED_COMPANY_NAME
        defaultVwContactGstDetailsShouldNotBeFound("companyName.contains=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyName does not contain DEFAULT_COMPANY_NAME
        defaultVwContactGstDetailsShouldNotBeFound("companyName.doesNotContain=" + DEFAULT_COMPANY_NAME);

        // Get all the vwContactGstDetailsList where companyName does not contain UPDATED_COMPANY_NAME
        defaultVwContactGstDetailsShouldBeFound("companyName.doesNotContain=" + UPDATED_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultVwContactGstDetailsShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the vwContactGstDetailsList where companyCode equals to UPDATED_COMPANY_CODE
        defaultVwContactGstDetailsShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultVwContactGstDetailsShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the vwContactGstDetailsList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultVwContactGstDetailsShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultVwContactGstDetailsShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the vwContactGstDetailsList where companyCode equals to UPDATED_COMPANY_CODE
        defaultVwContactGstDetailsShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyCode is not null
        defaultVwContactGstDetailsShouldBeFound("companyCode.specified=true");

        // Get all the vwContactGstDetailsList where companyCode is null
        defaultVwContactGstDetailsShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyCode contains DEFAULT_COMPANY_CODE
        defaultVwContactGstDetailsShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the vwContactGstDetailsList where companyCode contains UPDATED_COMPANY_CODE
        defaultVwContactGstDetailsShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultVwContactGstDetailsShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the vwContactGstDetailsList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultVwContactGstDetailsShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstNumber equals to DEFAULT_GST_NUMBER
        defaultVwContactGstDetailsShouldBeFound("gstNumber.equals=" + DEFAULT_GST_NUMBER);

        // Get all the vwContactGstDetailsList where gstNumber equals to UPDATED_GST_NUMBER
        defaultVwContactGstDetailsShouldNotBeFound("gstNumber.equals=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstNumber not equals to DEFAULT_GST_NUMBER
        defaultVwContactGstDetailsShouldNotBeFound("gstNumber.notEquals=" + DEFAULT_GST_NUMBER);

        // Get all the vwContactGstDetailsList where gstNumber not equals to UPDATED_GST_NUMBER
        defaultVwContactGstDetailsShouldBeFound("gstNumber.notEquals=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstNumberIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstNumber in DEFAULT_GST_NUMBER or UPDATED_GST_NUMBER
        defaultVwContactGstDetailsShouldBeFound("gstNumber.in=" + DEFAULT_GST_NUMBER + "," + UPDATED_GST_NUMBER);

        // Get all the vwContactGstDetailsList where gstNumber equals to UPDATED_GST_NUMBER
        defaultVwContactGstDetailsShouldNotBeFound("gstNumber.in=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstNumber is not null
        defaultVwContactGstDetailsShouldBeFound("gstNumber.specified=true");

        // Get all the vwContactGstDetailsList where gstNumber is null
        defaultVwContactGstDetailsShouldNotBeFound("gstNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstNumberContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstNumber contains DEFAULT_GST_NUMBER
        defaultVwContactGstDetailsShouldBeFound("gstNumber.contains=" + DEFAULT_GST_NUMBER);

        // Get all the vwContactGstDetailsList where gstNumber contains UPDATED_GST_NUMBER
        defaultVwContactGstDetailsShouldNotBeFound("gstNumber.contains=" + UPDATED_GST_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstNumberNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstNumber does not contain DEFAULT_GST_NUMBER
        defaultVwContactGstDetailsShouldNotBeFound("gstNumber.doesNotContain=" + DEFAULT_GST_NUMBER);

        // Get all the vwContactGstDetailsList where gstNumber does not contain UPDATED_GST_NUMBER
        defaultVwContactGstDetailsShouldBeFound("gstNumber.doesNotContain=" + UPDATED_GST_NUMBER);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByIsSezContactFlagIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where isSezContactFlag equals to DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldBeFound("isSezContactFlag.equals=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the vwContactGstDetailsList where isSezContactFlag equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldNotBeFound("isSezContactFlag.equals=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByIsSezContactFlagIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where isSezContactFlag not equals to DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldNotBeFound("isSezContactFlag.notEquals=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the vwContactGstDetailsList where isSezContactFlag not equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldBeFound("isSezContactFlag.notEquals=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByIsSezContactFlagIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where isSezContactFlag in DEFAULT_IS_SEZ_CONTACT_FLAG or UPDATED_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldBeFound("isSezContactFlag.in=" + DEFAULT_IS_SEZ_CONTACT_FLAG + "," + UPDATED_IS_SEZ_CONTACT_FLAG);

        // Get all the vwContactGstDetailsList where isSezContactFlag equals to UPDATED_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldNotBeFound("isSezContactFlag.in=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByIsSezContactFlagIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where isSezContactFlag is not null
        defaultVwContactGstDetailsShouldBeFound("isSezContactFlag.specified=true");

        // Get all the vwContactGstDetailsList where isSezContactFlag is null
        defaultVwContactGstDetailsShouldNotBeFound("isSezContactFlag.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByIsSezContactFlagContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where isSezContactFlag contains DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldBeFound("isSezContactFlag.contains=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the vwContactGstDetailsList where isSezContactFlag contains UPDATED_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldNotBeFound("isSezContactFlag.contains=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByIsSezContactFlagNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where isSezContactFlag does not contain DEFAULT_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldNotBeFound("isSezContactFlag.doesNotContain=" + DEFAULT_IS_SEZ_CONTACT_FLAG);

        // Get all the vwContactGstDetailsList where isSezContactFlag does not contain UPDATED_IS_SEZ_CONTACT_FLAG
        defaultVwContactGstDetailsShouldBeFound("isSezContactFlag.doesNotContain=" + UPDATED_IS_SEZ_CONTACT_FLAG);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByStateNameIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where stateName equals to DEFAULT_STATE_NAME
        defaultVwContactGstDetailsShouldBeFound("stateName.equals=" + DEFAULT_STATE_NAME);

        // Get all the vwContactGstDetailsList where stateName equals to UPDATED_STATE_NAME
        defaultVwContactGstDetailsShouldNotBeFound("stateName.equals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByStateNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where stateName not equals to DEFAULT_STATE_NAME
        defaultVwContactGstDetailsShouldNotBeFound("stateName.notEquals=" + DEFAULT_STATE_NAME);

        // Get all the vwContactGstDetailsList where stateName not equals to UPDATED_STATE_NAME
        defaultVwContactGstDetailsShouldBeFound("stateName.notEquals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByStateNameIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where stateName in DEFAULT_STATE_NAME or UPDATED_STATE_NAME
        defaultVwContactGstDetailsShouldBeFound("stateName.in=" + DEFAULT_STATE_NAME + "," + UPDATED_STATE_NAME);

        // Get all the vwContactGstDetailsList where stateName equals to UPDATED_STATE_NAME
        defaultVwContactGstDetailsShouldNotBeFound("stateName.in=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByStateNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where stateName is not null
        defaultVwContactGstDetailsShouldBeFound("stateName.specified=true");

        // Get all the vwContactGstDetailsList where stateName is null
        defaultVwContactGstDetailsShouldNotBeFound("stateName.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByStateNameContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where stateName contains DEFAULT_STATE_NAME
        defaultVwContactGstDetailsShouldBeFound("stateName.contains=" + DEFAULT_STATE_NAME);

        // Get all the vwContactGstDetailsList where stateName contains UPDATED_STATE_NAME
        defaultVwContactGstDetailsShouldNotBeFound("stateName.contains=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByStateNameNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where stateName does not contain DEFAULT_STATE_NAME
        defaultVwContactGstDetailsShouldNotBeFound("stateName.doesNotContain=" + DEFAULT_STATE_NAME);

        // Get all the vwContactGstDetailsList where stateName does not contain UPDATED_STATE_NAME
        defaultVwContactGstDetailsShouldBeFound("stateName.doesNotContain=" + UPDATED_STATE_NAME);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstStateCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstStateCode equals to DEFAULT_GST_STATE_CODE
        defaultVwContactGstDetailsShouldBeFound("gstStateCode.equals=" + DEFAULT_GST_STATE_CODE);

        // Get all the vwContactGstDetailsList where gstStateCode equals to UPDATED_GST_STATE_CODE
        defaultVwContactGstDetailsShouldNotBeFound("gstStateCode.equals=" + UPDATED_GST_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstStateCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstStateCode not equals to DEFAULT_GST_STATE_CODE
        defaultVwContactGstDetailsShouldNotBeFound("gstStateCode.notEquals=" + DEFAULT_GST_STATE_CODE);

        // Get all the vwContactGstDetailsList where gstStateCode not equals to UPDATED_GST_STATE_CODE
        defaultVwContactGstDetailsShouldBeFound("gstStateCode.notEquals=" + UPDATED_GST_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstStateCodeIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstStateCode in DEFAULT_GST_STATE_CODE or UPDATED_GST_STATE_CODE
        defaultVwContactGstDetailsShouldBeFound("gstStateCode.in=" + DEFAULT_GST_STATE_CODE + "," + UPDATED_GST_STATE_CODE);

        // Get all the vwContactGstDetailsList where gstStateCode equals to UPDATED_GST_STATE_CODE
        defaultVwContactGstDetailsShouldNotBeFound("gstStateCode.in=" + UPDATED_GST_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstStateCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstStateCode is not null
        defaultVwContactGstDetailsShouldBeFound("gstStateCode.specified=true");

        // Get all the vwContactGstDetailsList where gstStateCode is null
        defaultVwContactGstDetailsShouldNotBeFound("gstStateCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstStateCodeContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstStateCode contains DEFAULT_GST_STATE_CODE
        defaultVwContactGstDetailsShouldBeFound("gstStateCode.contains=" + DEFAULT_GST_STATE_CODE);

        // Get all the vwContactGstDetailsList where gstStateCode contains UPDATED_GST_STATE_CODE
        defaultVwContactGstDetailsShouldNotBeFound("gstStateCode.contains=" + UPDATED_GST_STATE_CODE);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByGstStateCodeNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where gstStateCode does not contain DEFAULT_GST_STATE_CODE
        defaultVwContactGstDetailsShouldNotBeFound("gstStateCode.doesNotContain=" + DEFAULT_GST_STATE_CODE);

        // Get all the vwContactGstDetailsList where gstStateCode does not contain UPDATED_GST_STATE_CODE
        defaultVwContactGstDetailsShouldBeFound("gstStateCode.doesNotContain=" + UPDATED_GST_STATE_CODE);
    }


    @Test
    @Transactional
    public void getAllVwContactGstDetailsByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where contactId equals to DEFAULT_CONTACT_ID
        defaultVwContactGstDetailsShouldBeFound("contactId.equals=" + DEFAULT_CONTACT_ID);

        // Get all the vwContactGstDetailsList where contactId equals to UPDATED_CONTACT_ID
        defaultVwContactGstDetailsShouldNotBeFound("contactId.equals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where contactId not equals to DEFAULT_CONTACT_ID
        defaultVwContactGstDetailsShouldNotBeFound("contactId.notEquals=" + DEFAULT_CONTACT_ID);

        // Get all the vwContactGstDetailsList where contactId not equals to UPDATED_CONTACT_ID
        defaultVwContactGstDetailsShouldBeFound("contactId.notEquals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where contactId in DEFAULT_CONTACT_ID or UPDATED_CONTACT_ID
        defaultVwContactGstDetailsShouldBeFound("contactId.in=" + DEFAULT_CONTACT_ID + "," + UPDATED_CONTACT_ID);

        // Get all the vwContactGstDetailsList where contactId equals to UPDATED_CONTACT_ID
        defaultVwContactGstDetailsShouldNotBeFound("contactId.in=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where contactId is not null
        defaultVwContactGstDetailsShouldBeFound("contactId.specified=true");

        // Get all the vwContactGstDetailsList where contactId is null
        defaultVwContactGstDetailsShouldNotBeFound("contactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllVwContactGstDetailsByContactIdContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where contactId contains DEFAULT_CONTACT_ID
        defaultVwContactGstDetailsShouldBeFound("contactId.contains=" + DEFAULT_CONTACT_ID);

        // Get all the vwContactGstDetailsList where contactId contains UPDATED_CONTACT_ID
        defaultVwContactGstDetailsShouldNotBeFound("contactId.contains=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllVwContactGstDetailsByContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        vwContactGstDetailsRepository.saveAndFlush(vwContactGstDetails);

        // Get all the vwContactGstDetailsList where contactId does not contain DEFAULT_CONTACT_ID
        defaultVwContactGstDetailsShouldNotBeFound("contactId.doesNotContain=" + DEFAULT_CONTACT_ID);

        // Get all the vwContactGstDetailsList where contactId does not contain UPDATED_CONTACT_ID
        defaultVwContactGstDetailsShouldBeFound("contactId.doesNotContain=" + UPDATED_CONTACT_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultVwContactGstDetailsShouldBeFound(String filter) throws Exception {
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vwContactGstDetails.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyRecordId").value(hasItem(DEFAULT_COMPANY_RECORD_ID)))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].gstNumber").value(hasItem(DEFAULT_GST_NUMBER)))
            .andExpect(jsonPath("$.[*].isSezContactFlag").value(hasItem(DEFAULT_IS_SEZ_CONTACT_FLAG)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].gstStateCode").value(hasItem(DEFAULT_GST_STATE_CODE)))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)));

        // Check, that the count call also returns 1
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultVwContactGstDetailsShouldNotBeFound(String filter) throws Exception {
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingVwContactGstDetails() throws Exception {
        // Get the vwContactGstDetails
        restVwContactGstDetailsMockMvc.perform(get("/api/vw-contact-gst-details/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVwContactGstDetails() throws Exception {
        // Initialize the database
        vwContactGstDetailsService.save(vwContactGstDetails);

        int databaseSizeBeforeUpdate = vwContactGstDetailsRepository.findAll().size();

        // Update the vwContactGstDetails
        VwContactGstDetails updatedVwContactGstDetails = vwContactGstDetailsRepository.findById(vwContactGstDetails.getId()).get();
        // Disconnect from session so that the updates on updatedVwContactGstDetails are not directly saved in db
        em.detach(updatedVwContactGstDetails);
        updatedVwContactGstDetails
            .companyRecordId(UPDATED_COMPANY_RECORD_ID)
            .companyName(UPDATED_COMPANY_NAME)
            .companyCode(UPDATED_COMPANY_CODE)
            .gstNumber(UPDATED_GST_NUMBER)
            .isSezContactFlag(UPDATED_IS_SEZ_CONTACT_FLAG)
            .stateName(UPDATED_STATE_NAME)
            .gstStateCode(UPDATED_GST_STATE_CODE)
            .contactId(UPDATED_CONTACT_ID);

        restVwContactGstDetailsMockMvc.perform(put("/api/vw-contact-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVwContactGstDetails)))
            .andExpect(status().isOk());

        // Validate the VwContactGstDetails in the database
        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeUpdate);
        VwContactGstDetails testVwContactGstDetails = vwContactGstDetailsList.get(vwContactGstDetailsList.size() - 1);
        assertThat(testVwContactGstDetails.getCompanyRecordId()).isEqualTo(UPDATED_COMPANY_RECORD_ID);
        assertThat(testVwContactGstDetails.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testVwContactGstDetails.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
        assertThat(testVwContactGstDetails.getGstNumber()).isEqualTo(UPDATED_GST_NUMBER);
        assertThat(testVwContactGstDetails.getIsSezContactFlag()).isEqualTo(UPDATED_IS_SEZ_CONTACT_FLAG);
        assertThat(testVwContactGstDetails.getStateName()).isEqualTo(UPDATED_STATE_NAME);
        assertThat(testVwContactGstDetails.getGstStateCode()).isEqualTo(UPDATED_GST_STATE_CODE);
        assertThat(testVwContactGstDetails.getContactId()).isEqualTo(UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingVwContactGstDetails() throws Exception {
        int databaseSizeBeforeUpdate = vwContactGstDetailsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVwContactGstDetailsMockMvc.perform(put("/api/vw-contact-gst-details")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(vwContactGstDetails)))
            .andExpect(status().isBadRequest());

        // Validate the VwContactGstDetails in the database
        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteVwContactGstDetails() throws Exception {
        // Initialize the database
        vwContactGstDetailsService.save(vwContactGstDetails);

        int databaseSizeBeforeDelete = vwContactGstDetailsRepository.findAll().size();

        // Delete the vwContactGstDetails
        restVwContactGstDetailsMockMvc.perform(delete("/api/vw-contact-gst-details/{id}", vwContactGstDetails.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<VwContactGstDetails> vwContactGstDetailsList = vwContactGstDetailsRepository.findAll();
        assertThat(vwContactGstDetailsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
