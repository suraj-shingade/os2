package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.repository.LocDistrictMasterRepository;
import com.crisil.onesource.service.LocDistrictMasterService;
import com.crisil.onesource.service.dto.LocDistrictMasterCriteria;
import com.crisil.onesource.service.LocDistrictMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocDistrictMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocDistrictMasterResourceIT {

    private static final String DEFAULT_DISTRICT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_DISTRICT_ID = 1;
    private static final Integer UPDATED_DISTRICT_ID = 2;
    private static final Integer SMALLER_DISTRICT_ID = 1 - 1;

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_IS_APPROVED = "A";
    private static final String UPDATED_IS_APPROVED = "B";

    private static final Integer DEFAULT_SEQ_ORDER = 1;
    private static final Integer UPDATED_SEQ_ORDER = 2;
    private static final Integer SMALLER_SEQ_ORDER = 1 - 1;

    @Autowired
    private LocDistrictMasterRepository locDistrictMasterRepository;

    @Autowired
    private LocDistrictMasterService locDistrictMasterService;

    @Autowired
    private LocDistrictMasterQueryService locDistrictMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocDistrictMasterMockMvc;

    private LocDistrictMaster locDistrictMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocDistrictMaster createEntity(EntityManager em) {
        LocDistrictMaster locDistrictMaster = new LocDistrictMaster()
            .districtName(DEFAULT_DISTRICT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .districtId(DEFAULT_DISTRICT_ID)
            .isDeleted(DEFAULT_IS_DELETED)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .isApproved(DEFAULT_IS_APPROVED)
            .seqOrder(DEFAULT_SEQ_ORDER);
        return locDistrictMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocDistrictMaster createUpdatedEntity(EntityManager em) {
        LocDistrictMaster locDistrictMaster = new LocDistrictMaster()
            .districtName(UPDATED_DISTRICT_NAME)
            .description(UPDATED_DESCRIPTION)
            .districtId(UPDATED_DISTRICT_ID)
            .isDeleted(UPDATED_IS_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);
        return locDistrictMaster;
    }

    @BeforeEach
    public void initTest() {
        locDistrictMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocDistrictMaster() throws Exception {
        int databaseSizeBeforeCreate = locDistrictMasterRepository.findAll().size();
        // Create the LocDistrictMaster
        restLocDistrictMasterMockMvc.perform(post("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locDistrictMaster)))
            .andExpect(status().isCreated());

        // Validate the LocDistrictMaster in the database
        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeCreate + 1);
        LocDistrictMaster testLocDistrictMaster = locDistrictMasterList.get(locDistrictMasterList.size() - 1);
        assertThat(testLocDistrictMaster.getDistrictName()).isEqualTo(DEFAULT_DISTRICT_NAME);
        assertThat(testLocDistrictMaster.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testLocDistrictMaster.getDistrictId()).isEqualTo(DEFAULT_DISTRICT_ID);
        assertThat(testLocDistrictMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocDistrictMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocDistrictMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocDistrictMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocDistrictMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testLocDistrictMaster.getIsApproved()).isEqualTo(DEFAULT_IS_APPROVED);
        assertThat(testLocDistrictMaster.getSeqOrder()).isEqualTo(DEFAULT_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void createLocDistrictMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locDistrictMasterRepository.findAll().size();

        // Create the LocDistrictMaster with an existing ID
        locDistrictMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocDistrictMasterMockMvc.perform(post("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locDistrictMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocDistrictMaster in the database
        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDistrictNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = locDistrictMasterRepository.findAll().size();
        // set the field null
        locDistrictMaster.setDistrictName(null);

        // Create the LocDistrictMaster, which fails.


        restLocDistrictMasterMockMvc.perform(post("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locDistrictMaster)))
            .andExpect(status().isBadRequest());

        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = locDistrictMasterRepository.findAll().size();
        // set the field null
        locDistrictMaster.setDescription(null);

        // Create the LocDistrictMaster, which fails.


        restLocDistrictMasterMockMvc.perform(post("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locDistrictMaster)))
            .andExpect(status().isBadRequest());

        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDistrictIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = locDistrictMasterRepository.findAll().size();
        // set the field null
        locDistrictMaster.setDistrictId(null);

        // Create the LocDistrictMaster, which fails.


        restLocDistrictMasterMockMvc.perform(post("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locDistrictMaster)))
            .andExpect(status().isBadRequest());

        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMasters() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locDistrictMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].districtName").value(hasItem(DEFAULT_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].districtId").value(hasItem(DEFAULT_DISTRICT_ID)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));
    }
    
    @Test
    @Transactional
    public void getLocDistrictMaster() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get the locDistrictMaster
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters/{id}", locDistrictMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locDistrictMaster.getId().intValue()))
            .andExpect(jsonPath("$.districtName").value(DEFAULT_DISTRICT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.districtId").value(DEFAULT_DISTRICT_ID))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.isApproved").value(DEFAULT_IS_APPROVED))
            .andExpect(jsonPath("$.seqOrder").value(DEFAULT_SEQ_ORDER));
    }


    @Test
    @Transactional
    public void getLocDistrictMastersByIdFiltering() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        Long id = locDistrictMaster.getId();

        defaultLocDistrictMasterShouldBeFound("id.equals=" + id);
        defaultLocDistrictMasterShouldNotBeFound("id.notEquals=" + id);

        defaultLocDistrictMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocDistrictMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultLocDistrictMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocDistrictMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtName equals to DEFAULT_DISTRICT_NAME
        defaultLocDistrictMasterShouldBeFound("districtName.equals=" + DEFAULT_DISTRICT_NAME);

        // Get all the locDistrictMasterList where districtName equals to UPDATED_DISTRICT_NAME
        defaultLocDistrictMasterShouldNotBeFound("districtName.equals=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtName not equals to DEFAULT_DISTRICT_NAME
        defaultLocDistrictMasterShouldNotBeFound("districtName.notEquals=" + DEFAULT_DISTRICT_NAME);

        // Get all the locDistrictMasterList where districtName not equals to UPDATED_DISTRICT_NAME
        defaultLocDistrictMasterShouldBeFound("districtName.notEquals=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictNameIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtName in DEFAULT_DISTRICT_NAME or UPDATED_DISTRICT_NAME
        defaultLocDistrictMasterShouldBeFound("districtName.in=" + DEFAULT_DISTRICT_NAME + "," + UPDATED_DISTRICT_NAME);

        // Get all the locDistrictMasterList where districtName equals to UPDATED_DISTRICT_NAME
        defaultLocDistrictMasterShouldNotBeFound("districtName.in=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtName is not null
        defaultLocDistrictMasterShouldBeFound("districtName.specified=true");

        // Get all the locDistrictMasterList where districtName is null
        defaultLocDistrictMasterShouldNotBeFound("districtName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictNameContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtName contains DEFAULT_DISTRICT_NAME
        defaultLocDistrictMasterShouldBeFound("districtName.contains=" + DEFAULT_DISTRICT_NAME);

        // Get all the locDistrictMasterList where districtName contains UPDATED_DISTRICT_NAME
        defaultLocDistrictMasterShouldNotBeFound("districtName.contains=" + UPDATED_DISTRICT_NAME);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictNameNotContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtName does not contain DEFAULT_DISTRICT_NAME
        defaultLocDistrictMasterShouldNotBeFound("districtName.doesNotContain=" + DEFAULT_DISTRICT_NAME);

        // Get all the locDistrictMasterList where districtName does not contain UPDATED_DISTRICT_NAME
        defaultLocDistrictMasterShouldBeFound("districtName.doesNotContain=" + UPDATED_DISTRICT_NAME);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where description equals to DEFAULT_DESCRIPTION
        defaultLocDistrictMasterShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the locDistrictMasterList where description equals to UPDATED_DESCRIPTION
        defaultLocDistrictMasterShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where description not equals to DEFAULT_DESCRIPTION
        defaultLocDistrictMasterShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the locDistrictMasterList where description not equals to UPDATED_DESCRIPTION
        defaultLocDistrictMasterShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultLocDistrictMasterShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the locDistrictMasterList where description equals to UPDATED_DESCRIPTION
        defaultLocDistrictMasterShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where description is not null
        defaultLocDistrictMasterShouldBeFound("description.specified=true");

        // Get all the locDistrictMasterList where description is null
        defaultLocDistrictMasterShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocDistrictMastersByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where description contains DEFAULT_DESCRIPTION
        defaultLocDistrictMasterShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the locDistrictMasterList where description contains UPDATED_DESCRIPTION
        defaultLocDistrictMasterShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where description does not contain DEFAULT_DESCRIPTION
        defaultLocDistrictMasterShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the locDistrictMasterList where description does not contain UPDATED_DESCRIPTION
        defaultLocDistrictMasterShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId equals to DEFAULT_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.equals=" + DEFAULT_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId equals to UPDATED_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.equals=" + UPDATED_DISTRICT_ID);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId not equals to DEFAULT_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.notEquals=" + DEFAULT_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId not equals to UPDATED_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.notEquals=" + UPDATED_DISTRICT_ID);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId in DEFAULT_DISTRICT_ID or UPDATED_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.in=" + DEFAULT_DISTRICT_ID + "," + UPDATED_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId equals to UPDATED_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.in=" + UPDATED_DISTRICT_ID);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId is not null
        defaultLocDistrictMasterShouldBeFound("districtId.specified=true");

        // Get all the locDistrictMasterList where districtId is null
        defaultLocDistrictMasterShouldNotBeFound("districtId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId is greater than or equal to DEFAULT_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.greaterThanOrEqual=" + DEFAULT_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId is greater than or equal to UPDATED_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.greaterThanOrEqual=" + UPDATED_DISTRICT_ID);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId is less than or equal to DEFAULT_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.lessThanOrEqual=" + DEFAULT_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId is less than or equal to SMALLER_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.lessThanOrEqual=" + SMALLER_DISTRICT_ID);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId is less than DEFAULT_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.lessThan=" + DEFAULT_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId is less than UPDATED_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.lessThan=" + UPDATED_DISTRICT_ID);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByDistrictIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where districtId is greater than DEFAULT_DISTRICT_ID
        defaultLocDistrictMasterShouldNotBeFound("districtId.greaterThan=" + DEFAULT_DISTRICT_ID);

        // Get all the locDistrictMasterList where districtId is greater than SMALLER_DISTRICT_ID
        defaultLocDistrictMasterShouldBeFound("districtId.greaterThan=" + SMALLER_DISTRICT_ID);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocDistrictMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locDistrictMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocDistrictMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocDistrictMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locDistrictMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocDistrictMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocDistrictMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locDistrictMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocDistrictMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isDeleted is not null
        defaultLocDistrictMasterShouldBeFound("isDeleted.specified=true");

        // Get all the locDistrictMasterList where isDeleted is null
        defaultLocDistrictMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocDistrictMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocDistrictMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locDistrictMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultLocDistrictMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocDistrictMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locDistrictMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocDistrictMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocDistrictMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locDistrictMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocDistrictMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocDistrictMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locDistrictMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocDistrictMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocDistrictMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locDistrictMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocDistrictMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdDate is not null
        defaultLocDistrictMasterShouldBeFound("createdDate.specified=true");

        // Get all the locDistrictMasterList where createdDate is null
        defaultLocDistrictMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocDistrictMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locDistrictMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocDistrictMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocDistrictMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locDistrictMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocDistrictMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocDistrictMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locDistrictMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocDistrictMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdBy is not null
        defaultLocDistrictMasterShouldBeFound("createdBy.specified=true");

        // Get all the locDistrictMasterList where createdBy is null
        defaultLocDistrictMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultLocDistrictMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locDistrictMasterList where createdBy contains UPDATED_CREATED_BY
        defaultLocDistrictMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocDistrictMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locDistrictMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocDistrictMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocDistrictMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locDistrictMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locDistrictMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocDistrictMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocDistrictMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locDistrictMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedDate is not null
        defaultLocDistrictMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locDistrictMasterList where lastModifiedDate is null
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locDistrictMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locDistrictMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locDistrictMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedBy is not null
        defaultLocDistrictMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locDistrictMasterList where lastModifiedBy is null
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locDistrictMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locDistrictMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocDistrictMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsApprovedIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isApproved equals to DEFAULT_IS_APPROVED
        defaultLocDistrictMasterShouldBeFound("isApproved.equals=" + DEFAULT_IS_APPROVED);

        // Get all the locDistrictMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocDistrictMasterShouldNotBeFound("isApproved.equals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsApprovedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isApproved not equals to DEFAULT_IS_APPROVED
        defaultLocDistrictMasterShouldNotBeFound("isApproved.notEquals=" + DEFAULT_IS_APPROVED);

        // Get all the locDistrictMasterList where isApproved not equals to UPDATED_IS_APPROVED
        defaultLocDistrictMasterShouldBeFound("isApproved.notEquals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsApprovedIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isApproved in DEFAULT_IS_APPROVED or UPDATED_IS_APPROVED
        defaultLocDistrictMasterShouldBeFound("isApproved.in=" + DEFAULT_IS_APPROVED + "," + UPDATED_IS_APPROVED);

        // Get all the locDistrictMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocDistrictMasterShouldNotBeFound("isApproved.in=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsApprovedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isApproved is not null
        defaultLocDistrictMasterShouldBeFound("isApproved.specified=true");

        // Get all the locDistrictMasterList where isApproved is null
        defaultLocDistrictMasterShouldNotBeFound("isApproved.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocDistrictMastersByIsApprovedContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isApproved contains DEFAULT_IS_APPROVED
        defaultLocDistrictMasterShouldBeFound("isApproved.contains=" + DEFAULT_IS_APPROVED);

        // Get all the locDistrictMasterList where isApproved contains UPDATED_IS_APPROVED
        defaultLocDistrictMasterShouldNotBeFound("isApproved.contains=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersByIsApprovedNotContainsSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where isApproved does not contain DEFAULT_IS_APPROVED
        defaultLocDistrictMasterShouldNotBeFound("isApproved.doesNotContain=" + DEFAULT_IS_APPROVED);

        // Get all the locDistrictMasterList where isApproved does not contain UPDATED_IS_APPROVED
        defaultLocDistrictMasterShouldBeFound("isApproved.doesNotContain=" + UPDATED_IS_APPROVED);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder equals to DEFAULT_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.equals=" + DEFAULT_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.equals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder not equals to DEFAULT_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.notEquals=" + DEFAULT_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder not equals to UPDATED_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.notEquals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsInShouldWork() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder in DEFAULT_SEQ_ORDER or UPDATED_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.in=" + DEFAULT_SEQ_ORDER + "," + UPDATED_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.in=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder is not null
        defaultLocDistrictMasterShouldBeFound("seqOrder.specified=true");

        // Get all the locDistrictMasterList where seqOrder is null
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder is greater than or equal to DEFAULT_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.greaterThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder is greater than or equal to UPDATED_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.greaterThanOrEqual=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder is less than or equal to DEFAULT_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.lessThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder is less than or equal to SMALLER_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.lessThanOrEqual=" + SMALLER_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder is less than DEFAULT_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.lessThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder is less than UPDATED_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.lessThan=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocDistrictMastersBySeqOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);

        // Get all the locDistrictMasterList where seqOrder is greater than DEFAULT_SEQ_ORDER
        defaultLocDistrictMasterShouldNotBeFound("seqOrder.greaterThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locDistrictMasterList where seqOrder is greater than SMALLER_SEQ_ORDER
        defaultLocDistrictMasterShouldBeFound("seqOrder.greaterThan=" + SMALLER_SEQ_ORDER);
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByLocCityMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);
        LocCityMaster locCityMaster = LocCityMasterResourceIT.createEntity(em);
        em.persist(locCityMaster);
        em.flush();
        locDistrictMaster.addLocCityMaster(locCityMaster);
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);
        Long locCityMasterId = locCityMaster.getId();

        // Get all the locDistrictMasterList where locCityMaster equals to locCityMasterId
        defaultLocDistrictMasterShouldBeFound("locCityMasterId.equals=" + locCityMasterId);

        // Get all the locDistrictMasterList where locCityMaster equals to locCityMasterId + 1
        defaultLocDistrictMasterShouldNotBeFound("locCityMasterId.equals=" + (locCityMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByLocPincodeMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);
        LocPincodeMaster locPincodeMaster = LocPincodeMasterResourceIT.createEntity(em);
        em.persist(locPincodeMaster);
        em.flush();
        locDistrictMaster.addLocPincodeMaster(locPincodeMaster);
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);
        Long locPincodeMasterId = locPincodeMaster.getId();

        // Get all the locDistrictMasterList where locPincodeMaster equals to locPincodeMasterId
        defaultLocDistrictMasterShouldBeFound("locPincodeMasterId.equals=" + locPincodeMasterId);

        // Get all the locDistrictMasterList where locPincodeMaster equals to locPincodeMasterId + 1
        defaultLocDistrictMasterShouldNotBeFound("locPincodeMasterId.equals=" + (locPincodeMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllLocDistrictMastersByStateIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);
        LocStateMaster stateId = LocStateMasterResourceIT.createEntity(em);
        em.persist(stateId);
        em.flush();
        locDistrictMaster.setStateId(stateId);
        locDistrictMasterRepository.saveAndFlush(locDistrictMaster);
        Long stateIdId = stateId.getId();

        // Get all the locDistrictMasterList where stateId equals to stateIdId
        defaultLocDistrictMasterShouldBeFound("stateIdId.equals=" + stateIdId);

        // Get all the locDistrictMasterList where stateId equals to stateIdId + 1
        defaultLocDistrictMasterShouldNotBeFound("stateIdId.equals=" + (stateIdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocDistrictMasterShouldBeFound(String filter) throws Exception {
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locDistrictMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].districtName").value(hasItem(DEFAULT_DISTRICT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].districtId").value(hasItem(DEFAULT_DISTRICT_ID)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));

        // Check, that the count call also returns 1
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocDistrictMasterShouldNotBeFound(String filter) throws Exception {
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocDistrictMaster() throws Exception {
        // Get the locDistrictMaster
        restLocDistrictMasterMockMvc.perform(get("/api/loc-district-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocDistrictMaster() throws Exception {
        // Initialize the database
        locDistrictMasterService.save(locDistrictMaster);

        int databaseSizeBeforeUpdate = locDistrictMasterRepository.findAll().size();

        // Update the locDistrictMaster
        LocDistrictMaster updatedLocDistrictMaster = locDistrictMasterRepository.findById(locDistrictMaster.getId()).get();
        // Disconnect from session so that the updates on updatedLocDistrictMaster are not directly saved in db
        em.detach(updatedLocDistrictMaster);
        updatedLocDistrictMaster
            .districtName(UPDATED_DISTRICT_NAME)
            .description(UPDATED_DESCRIPTION)
            .districtId(UPDATED_DISTRICT_ID)
            .isDeleted(UPDATED_IS_DELETED)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);

        restLocDistrictMasterMockMvc.perform(put("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocDistrictMaster)))
            .andExpect(status().isOk());

        // Validate the LocDistrictMaster in the database
        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeUpdate);
        LocDistrictMaster testLocDistrictMaster = locDistrictMasterList.get(locDistrictMasterList.size() - 1);
        assertThat(testLocDistrictMaster.getDistrictName()).isEqualTo(UPDATED_DISTRICT_NAME);
        assertThat(testLocDistrictMaster.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testLocDistrictMaster.getDistrictId()).isEqualTo(UPDATED_DISTRICT_ID);
        assertThat(testLocDistrictMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocDistrictMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocDistrictMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocDistrictMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocDistrictMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testLocDistrictMaster.getIsApproved()).isEqualTo(UPDATED_IS_APPROVED);
        assertThat(testLocDistrictMaster.getSeqOrder()).isEqualTo(UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void updateNonExistingLocDistrictMaster() throws Exception {
        int databaseSizeBeforeUpdate = locDistrictMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocDistrictMasterMockMvc.perform(put("/api/loc-district-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locDistrictMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocDistrictMaster in the database
        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocDistrictMaster() throws Exception {
        // Initialize the database
        locDistrictMasterService.save(locDistrictMaster);

        int databaseSizeBeforeDelete = locDistrictMasterRepository.findAll().size();

        // Delete the locDistrictMaster
        restLocDistrictMasterMockMvc.perform(delete("/api/loc-district-masters/{id}", locDistrictMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocDistrictMaster> locDistrictMasterList = locDistrictMasterRepository.findAll();
        assertThat(locDistrictMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
