package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocCountryMaster;
import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.repository.LocCountryMasterRepository;
import com.crisil.onesource.service.LocCountryMasterService;
import com.crisil.onesource.service.dto.LocCountryMasterCriteria;
import com.crisil.onesource.service.LocCountryMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocCountryMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocCountryMasterResourceIT {

    private static final String DEFAULT_COUNTRY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_ZIP_CODE_FORMAT = "AAAAAAAAAA";
    private static final String UPDATED_ZIP_CODE_FORMAT = "BBBBBBBBBB";

    private static final String DEFAULT_CONTINENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTINENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FLAG_IMAGE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_FLAG_IMAGE_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_COUNTRY_ID = 1;
    private static final Integer UPDATED_COUNTRY_ID = 2;
    private static final Integer SMALLER_COUNTRY_ID = 1 - 1;

    private static final String DEFAULT_IS_APPROVED = "A";
    private static final String UPDATED_IS_APPROVED = "B";

    private static final Integer DEFAULT_SEQ_ORDER = 1;
    private static final Integer UPDATED_SEQ_ORDER = 2;
    private static final Integer SMALLER_SEQ_ORDER = 1 - 1;

    @Autowired
    private LocCountryMasterRepository locCountryMasterRepository;

    @Autowired
    private LocCountryMasterService locCountryMasterService;

    @Autowired
    private LocCountryMasterQueryService locCountryMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocCountryMasterMockMvc;

    private LocCountryMaster locCountryMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocCountryMaster createEntity(EntityManager em) {
        LocCountryMaster locCountryMaster = new LocCountryMaster()
            .countryName(DEFAULT_COUNTRY_NAME)
            .countryCode(DEFAULT_COUNTRY_CODE)
            .isDeleted(DEFAULT_IS_DELETED)
            .zipCodeFormat(DEFAULT_ZIP_CODE_FORMAT)
            .continentId(DEFAULT_CONTINENT_ID)
            .flagImagePath(DEFAULT_FLAG_IMAGE_PATH)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .countryId(DEFAULT_COUNTRY_ID)
            .isApproved(DEFAULT_IS_APPROVED)
            .seqOrder(DEFAULT_SEQ_ORDER);
        return locCountryMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocCountryMaster createUpdatedEntity(EntityManager em) {
        LocCountryMaster locCountryMaster = new LocCountryMaster()
            .countryName(UPDATED_COUNTRY_NAME)
            .countryCode(UPDATED_COUNTRY_CODE)
            .isDeleted(UPDATED_IS_DELETED)
            .zipCodeFormat(UPDATED_ZIP_CODE_FORMAT)
            .continentId(UPDATED_CONTINENT_ID)
            .flagImagePath(UPDATED_FLAG_IMAGE_PATH)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .countryId(UPDATED_COUNTRY_ID)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);
        return locCountryMaster;
    }

    @BeforeEach
    public void initTest() {
        locCountryMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocCountryMaster() throws Exception {
        int databaseSizeBeforeCreate = locCountryMasterRepository.findAll().size();
        // Create the LocCountryMaster
        restLocCountryMasterMockMvc.perform(post("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCountryMaster)))
            .andExpect(status().isCreated());

        // Validate the LocCountryMaster in the database
        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeCreate + 1);
        LocCountryMaster testLocCountryMaster = locCountryMasterList.get(locCountryMasterList.size() - 1);
        assertThat(testLocCountryMaster.getCountryName()).isEqualTo(DEFAULT_COUNTRY_NAME);
        assertThat(testLocCountryMaster.getCountryCode()).isEqualTo(DEFAULT_COUNTRY_CODE);
        assertThat(testLocCountryMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocCountryMaster.getZipCodeFormat()).isEqualTo(DEFAULT_ZIP_CODE_FORMAT);
        assertThat(testLocCountryMaster.getContinentId()).isEqualTo(DEFAULT_CONTINENT_ID);
        assertThat(testLocCountryMaster.getFlagImagePath()).isEqualTo(DEFAULT_FLAG_IMAGE_PATH);
        assertThat(testLocCountryMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocCountryMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocCountryMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testLocCountryMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocCountryMaster.getCountryId()).isEqualTo(DEFAULT_COUNTRY_ID);
        assertThat(testLocCountryMaster.getIsApproved()).isEqualTo(DEFAULT_IS_APPROVED);
        assertThat(testLocCountryMaster.getSeqOrder()).isEqualTo(DEFAULT_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void createLocCountryMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locCountryMasterRepository.findAll().size();

        // Create the LocCountryMaster with an existing ID
        locCountryMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocCountryMasterMockMvc.perform(post("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCountryMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocCountryMaster in the database
        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCountryNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = locCountryMasterRepository.findAll().size();
        // set the field null
        locCountryMaster.setCountryName(null);

        // Create the LocCountryMaster, which fails.


        restLocCountryMasterMockMvc.perform(post("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCountryMaster)))
            .andExpect(status().isBadRequest());

        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = locCountryMasterRepository.findAll().size();
        // set the field null
        locCountryMaster.setCountryCode(null);

        // Create the LocCountryMaster, which fails.


        restLocCountryMasterMockMvc.perform(post("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCountryMaster)))
            .andExpect(status().isBadRequest());

        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = locCountryMasterRepository.findAll().size();
        // set the field null
        locCountryMaster.setCountryId(null);

        // Create the LocCountryMaster, which fails.


        restLocCountryMasterMockMvc.perform(post("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCountryMaster)))
            .andExpect(status().isBadRequest());

        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocCountryMasters() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locCountryMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].countryName").value(hasItem(DEFAULT_COUNTRY_NAME)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].zipCodeFormat").value(hasItem(DEFAULT_ZIP_CODE_FORMAT)))
            .andExpect(jsonPath("$.[*].continentId").value(hasItem(DEFAULT_CONTINENT_ID)))
            .andExpect(jsonPath("$.[*].flagImagePath").value(hasItem(DEFAULT_FLAG_IMAGE_PATH)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].countryId").value(hasItem(DEFAULT_COUNTRY_ID)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));
    }
    
    @Test
    @Transactional
    public void getLocCountryMaster() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get the locCountryMaster
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters/{id}", locCountryMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locCountryMaster.getId().intValue()))
            .andExpect(jsonPath("$.countryName").value(DEFAULT_COUNTRY_NAME))
            .andExpect(jsonPath("$.countryCode").value(DEFAULT_COUNTRY_CODE))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.zipCodeFormat").value(DEFAULT_ZIP_CODE_FORMAT))
            .andExpect(jsonPath("$.continentId").value(DEFAULT_CONTINENT_ID))
            .andExpect(jsonPath("$.flagImagePath").value(DEFAULT_FLAG_IMAGE_PATH))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.countryId").value(DEFAULT_COUNTRY_ID))
            .andExpect(jsonPath("$.isApproved").value(DEFAULT_IS_APPROVED))
            .andExpect(jsonPath("$.seqOrder").value(DEFAULT_SEQ_ORDER));
    }


    @Test
    @Transactional
    public void getLocCountryMastersByIdFiltering() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        Long id = locCountryMaster.getId();

        defaultLocCountryMasterShouldBeFound("id.equals=" + id);
        defaultLocCountryMasterShouldNotBeFound("id.notEquals=" + id);

        defaultLocCountryMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocCountryMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultLocCountryMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocCountryMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryName equals to DEFAULT_COUNTRY_NAME
        defaultLocCountryMasterShouldBeFound("countryName.equals=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCountryMasterList where countryName equals to UPDATED_COUNTRY_NAME
        defaultLocCountryMasterShouldNotBeFound("countryName.equals=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryName not equals to DEFAULT_COUNTRY_NAME
        defaultLocCountryMasterShouldNotBeFound("countryName.notEquals=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCountryMasterList where countryName not equals to UPDATED_COUNTRY_NAME
        defaultLocCountryMasterShouldBeFound("countryName.notEquals=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryNameIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryName in DEFAULT_COUNTRY_NAME or UPDATED_COUNTRY_NAME
        defaultLocCountryMasterShouldBeFound("countryName.in=" + DEFAULT_COUNTRY_NAME + "," + UPDATED_COUNTRY_NAME);

        // Get all the locCountryMasterList where countryName equals to UPDATED_COUNTRY_NAME
        defaultLocCountryMasterShouldNotBeFound("countryName.in=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryName is not null
        defaultLocCountryMasterShouldBeFound("countryName.specified=true");

        // Get all the locCountryMasterList where countryName is null
        defaultLocCountryMasterShouldNotBeFound("countryName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByCountryNameContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryName contains DEFAULT_COUNTRY_NAME
        defaultLocCountryMasterShouldBeFound("countryName.contains=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCountryMasterList where countryName contains UPDATED_COUNTRY_NAME
        defaultLocCountryMasterShouldNotBeFound("countryName.contains=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryNameNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryName does not contain DEFAULT_COUNTRY_NAME
        defaultLocCountryMasterShouldNotBeFound("countryName.doesNotContain=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCountryMasterList where countryName does not contain UPDATED_COUNTRY_NAME
        defaultLocCountryMasterShouldBeFound("countryName.doesNotContain=" + UPDATED_COUNTRY_NAME);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryCode equals to DEFAULT_COUNTRY_CODE
        defaultLocCountryMasterShouldBeFound("countryCode.equals=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCountryMasterList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultLocCountryMasterShouldNotBeFound("countryCode.equals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryCode not equals to DEFAULT_COUNTRY_CODE
        defaultLocCountryMasterShouldNotBeFound("countryCode.notEquals=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCountryMasterList where countryCode not equals to UPDATED_COUNTRY_CODE
        defaultLocCountryMasterShouldBeFound("countryCode.notEquals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryCode in DEFAULT_COUNTRY_CODE or UPDATED_COUNTRY_CODE
        defaultLocCountryMasterShouldBeFound("countryCode.in=" + DEFAULT_COUNTRY_CODE + "," + UPDATED_COUNTRY_CODE);

        // Get all the locCountryMasterList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultLocCountryMasterShouldNotBeFound("countryCode.in=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryCode is not null
        defaultLocCountryMasterShouldBeFound("countryCode.specified=true");

        // Get all the locCountryMasterList where countryCode is null
        defaultLocCountryMasterShouldNotBeFound("countryCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByCountryCodeContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryCode contains DEFAULT_COUNTRY_CODE
        defaultLocCountryMasterShouldBeFound("countryCode.contains=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCountryMasterList where countryCode contains UPDATED_COUNTRY_CODE
        defaultLocCountryMasterShouldNotBeFound("countryCode.contains=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryCodeNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryCode does not contain DEFAULT_COUNTRY_CODE
        defaultLocCountryMasterShouldNotBeFound("countryCode.doesNotContain=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCountryMasterList where countryCode does not contain UPDATED_COUNTRY_CODE
        defaultLocCountryMasterShouldBeFound("countryCode.doesNotContain=" + UPDATED_COUNTRY_CODE);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocCountryMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locCountryMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocCountryMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocCountryMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locCountryMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocCountryMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocCountryMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locCountryMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocCountryMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isDeleted is not null
        defaultLocCountryMasterShouldBeFound("isDeleted.specified=true");

        // Get all the locCountryMasterList where isDeleted is null
        defaultLocCountryMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocCountryMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locCountryMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultLocCountryMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocCountryMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locCountryMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocCountryMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByZipCodeFormatIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where zipCodeFormat equals to DEFAULT_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldBeFound("zipCodeFormat.equals=" + DEFAULT_ZIP_CODE_FORMAT);

        // Get all the locCountryMasterList where zipCodeFormat equals to UPDATED_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldNotBeFound("zipCodeFormat.equals=" + UPDATED_ZIP_CODE_FORMAT);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByZipCodeFormatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where zipCodeFormat not equals to DEFAULT_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldNotBeFound("zipCodeFormat.notEquals=" + DEFAULT_ZIP_CODE_FORMAT);

        // Get all the locCountryMasterList where zipCodeFormat not equals to UPDATED_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldBeFound("zipCodeFormat.notEquals=" + UPDATED_ZIP_CODE_FORMAT);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByZipCodeFormatIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where zipCodeFormat in DEFAULT_ZIP_CODE_FORMAT or UPDATED_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldBeFound("zipCodeFormat.in=" + DEFAULT_ZIP_CODE_FORMAT + "," + UPDATED_ZIP_CODE_FORMAT);

        // Get all the locCountryMasterList where zipCodeFormat equals to UPDATED_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldNotBeFound("zipCodeFormat.in=" + UPDATED_ZIP_CODE_FORMAT);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByZipCodeFormatIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where zipCodeFormat is not null
        defaultLocCountryMasterShouldBeFound("zipCodeFormat.specified=true");

        // Get all the locCountryMasterList where zipCodeFormat is null
        defaultLocCountryMasterShouldNotBeFound("zipCodeFormat.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByZipCodeFormatContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where zipCodeFormat contains DEFAULT_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldBeFound("zipCodeFormat.contains=" + DEFAULT_ZIP_CODE_FORMAT);

        // Get all the locCountryMasterList where zipCodeFormat contains UPDATED_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldNotBeFound("zipCodeFormat.contains=" + UPDATED_ZIP_CODE_FORMAT);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByZipCodeFormatNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where zipCodeFormat does not contain DEFAULT_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldNotBeFound("zipCodeFormat.doesNotContain=" + DEFAULT_ZIP_CODE_FORMAT);

        // Get all the locCountryMasterList where zipCodeFormat does not contain UPDATED_ZIP_CODE_FORMAT
        defaultLocCountryMasterShouldBeFound("zipCodeFormat.doesNotContain=" + UPDATED_ZIP_CODE_FORMAT);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByContinentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where continentId equals to DEFAULT_CONTINENT_ID
        defaultLocCountryMasterShouldBeFound("continentId.equals=" + DEFAULT_CONTINENT_ID);

        // Get all the locCountryMasterList where continentId equals to UPDATED_CONTINENT_ID
        defaultLocCountryMasterShouldNotBeFound("continentId.equals=" + UPDATED_CONTINENT_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByContinentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where continentId not equals to DEFAULT_CONTINENT_ID
        defaultLocCountryMasterShouldNotBeFound("continentId.notEquals=" + DEFAULT_CONTINENT_ID);

        // Get all the locCountryMasterList where continentId not equals to UPDATED_CONTINENT_ID
        defaultLocCountryMasterShouldBeFound("continentId.notEquals=" + UPDATED_CONTINENT_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByContinentIdIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where continentId in DEFAULT_CONTINENT_ID or UPDATED_CONTINENT_ID
        defaultLocCountryMasterShouldBeFound("continentId.in=" + DEFAULT_CONTINENT_ID + "," + UPDATED_CONTINENT_ID);

        // Get all the locCountryMasterList where continentId equals to UPDATED_CONTINENT_ID
        defaultLocCountryMasterShouldNotBeFound("continentId.in=" + UPDATED_CONTINENT_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByContinentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where continentId is not null
        defaultLocCountryMasterShouldBeFound("continentId.specified=true");

        // Get all the locCountryMasterList where continentId is null
        defaultLocCountryMasterShouldNotBeFound("continentId.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByContinentIdContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where continentId contains DEFAULT_CONTINENT_ID
        defaultLocCountryMasterShouldBeFound("continentId.contains=" + DEFAULT_CONTINENT_ID);

        // Get all the locCountryMasterList where continentId contains UPDATED_CONTINENT_ID
        defaultLocCountryMasterShouldNotBeFound("continentId.contains=" + UPDATED_CONTINENT_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByContinentIdNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where continentId does not contain DEFAULT_CONTINENT_ID
        defaultLocCountryMasterShouldNotBeFound("continentId.doesNotContain=" + DEFAULT_CONTINENT_ID);

        // Get all the locCountryMasterList where continentId does not contain UPDATED_CONTINENT_ID
        defaultLocCountryMasterShouldBeFound("continentId.doesNotContain=" + UPDATED_CONTINENT_ID);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByFlagImagePathIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where flagImagePath equals to DEFAULT_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldBeFound("flagImagePath.equals=" + DEFAULT_FLAG_IMAGE_PATH);

        // Get all the locCountryMasterList where flagImagePath equals to UPDATED_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldNotBeFound("flagImagePath.equals=" + UPDATED_FLAG_IMAGE_PATH);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByFlagImagePathIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where flagImagePath not equals to DEFAULT_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldNotBeFound("flagImagePath.notEquals=" + DEFAULT_FLAG_IMAGE_PATH);

        // Get all the locCountryMasterList where flagImagePath not equals to UPDATED_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldBeFound("flagImagePath.notEquals=" + UPDATED_FLAG_IMAGE_PATH);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByFlagImagePathIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where flagImagePath in DEFAULT_FLAG_IMAGE_PATH or UPDATED_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldBeFound("flagImagePath.in=" + DEFAULT_FLAG_IMAGE_PATH + "," + UPDATED_FLAG_IMAGE_PATH);

        // Get all the locCountryMasterList where flagImagePath equals to UPDATED_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldNotBeFound("flagImagePath.in=" + UPDATED_FLAG_IMAGE_PATH);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByFlagImagePathIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where flagImagePath is not null
        defaultLocCountryMasterShouldBeFound("flagImagePath.specified=true");

        // Get all the locCountryMasterList where flagImagePath is null
        defaultLocCountryMasterShouldNotBeFound("flagImagePath.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByFlagImagePathContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where flagImagePath contains DEFAULT_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldBeFound("flagImagePath.contains=" + DEFAULT_FLAG_IMAGE_PATH);

        // Get all the locCountryMasterList where flagImagePath contains UPDATED_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldNotBeFound("flagImagePath.contains=" + UPDATED_FLAG_IMAGE_PATH);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByFlagImagePathNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where flagImagePath does not contain DEFAULT_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldNotBeFound("flagImagePath.doesNotContain=" + DEFAULT_FLAG_IMAGE_PATH);

        // Get all the locCountryMasterList where flagImagePath does not contain UPDATED_FLAG_IMAGE_PATH
        defaultLocCountryMasterShouldBeFound("flagImagePath.doesNotContain=" + UPDATED_FLAG_IMAGE_PATH);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocCountryMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locCountryMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocCountryMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocCountryMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locCountryMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocCountryMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocCountryMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locCountryMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocCountryMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdBy is not null
        defaultLocCountryMasterShouldBeFound("createdBy.specified=true");

        // Get all the locCountryMasterList where createdBy is null
        defaultLocCountryMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultLocCountryMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locCountryMasterList where createdBy contains UPDATED_CREATED_BY
        defaultLocCountryMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocCountryMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locCountryMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocCountryMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocCountryMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locCountryMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocCountryMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocCountryMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locCountryMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocCountryMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocCountryMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locCountryMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocCountryMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where createdDate is not null
        defaultLocCountryMasterShouldBeFound("createdDate.specified=true");

        // Get all the locCountryMasterList where createdDate is null
        defaultLocCountryMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCountryMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCountryMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locCountryMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedBy is not null
        defaultLocCountryMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locCountryMasterList where lastModifiedBy is null
        defaultLocCountryMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCountryMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCountryMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocCountryMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocCountryMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locCountryMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCountryMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocCountryMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locCountryMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCountryMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocCountryMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locCountryMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCountryMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where lastModifiedDate is not null
        defaultLocCountryMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locCountryMasterList where lastModifiedDate is null
        defaultLocCountryMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId equals to DEFAULT_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.equals=" + DEFAULT_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId equals to UPDATED_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.equals=" + UPDATED_COUNTRY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId not equals to DEFAULT_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.notEquals=" + DEFAULT_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId not equals to UPDATED_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.notEquals=" + UPDATED_COUNTRY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId in DEFAULT_COUNTRY_ID or UPDATED_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.in=" + DEFAULT_COUNTRY_ID + "," + UPDATED_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId equals to UPDATED_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.in=" + UPDATED_COUNTRY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId is not null
        defaultLocCountryMasterShouldBeFound("countryId.specified=true");

        // Get all the locCountryMasterList where countryId is null
        defaultLocCountryMasterShouldNotBeFound("countryId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId is greater than or equal to DEFAULT_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.greaterThanOrEqual=" + DEFAULT_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId is greater than or equal to UPDATED_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.greaterThanOrEqual=" + UPDATED_COUNTRY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId is less than or equal to DEFAULT_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.lessThanOrEqual=" + DEFAULT_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId is less than or equal to SMALLER_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.lessThanOrEqual=" + SMALLER_COUNTRY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId is less than DEFAULT_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.lessThan=" + DEFAULT_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId is less than UPDATED_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.lessThan=" + UPDATED_COUNTRY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByCountryIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where countryId is greater than DEFAULT_COUNTRY_ID
        defaultLocCountryMasterShouldNotBeFound("countryId.greaterThan=" + DEFAULT_COUNTRY_ID);

        // Get all the locCountryMasterList where countryId is greater than SMALLER_COUNTRY_ID
        defaultLocCountryMasterShouldBeFound("countryId.greaterThan=" + SMALLER_COUNTRY_ID);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByIsApprovedIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isApproved equals to DEFAULT_IS_APPROVED
        defaultLocCountryMasterShouldBeFound("isApproved.equals=" + DEFAULT_IS_APPROVED);

        // Get all the locCountryMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocCountryMasterShouldNotBeFound("isApproved.equals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsApprovedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isApproved not equals to DEFAULT_IS_APPROVED
        defaultLocCountryMasterShouldNotBeFound("isApproved.notEquals=" + DEFAULT_IS_APPROVED);

        // Get all the locCountryMasterList where isApproved not equals to UPDATED_IS_APPROVED
        defaultLocCountryMasterShouldBeFound("isApproved.notEquals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsApprovedIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isApproved in DEFAULT_IS_APPROVED or UPDATED_IS_APPROVED
        defaultLocCountryMasterShouldBeFound("isApproved.in=" + DEFAULT_IS_APPROVED + "," + UPDATED_IS_APPROVED);

        // Get all the locCountryMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocCountryMasterShouldNotBeFound("isApproved.in=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsApprovedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isApproved is not null
        defaultLocCountryMasterShouldBeFound("isApproved.specified=true");

        // Get all the locCountryMasterList where isApproved is null
        defaultLocCountryMasterShouldNotBeFound("isApproved.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCountryMastersByIsApprovedContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isApproved contains DEFAULT_IS_APPROVED
        defaultLocCountryMasterShouldBeFound("isApproved.contains=" + DEFAULT_IS_APPROVED);

        // Get all the locCountryMasterList where isApproved contains UPDATED_IS_APPROVED
        defaultLocCountryMasterShouldNotBeFound("isApproved.contains=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersByIsApprovedNotContainsSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where isApproved does not contain DEFAULT_IS_APPROVED
        defaultLocCountryMasterShouldNotBeFound("isApproved.doesNotContain=" + DEFAULT_IS_APPROVED);

        // Get all the locCountryMasterList where isApproved does not contain UPDATED_IS_APPROVED
        defaultLocCountryMasterShouldBeFound("isApproved.doesNotContain=" + UPDATED_IS_APPROVED);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder equals to DEFAULT_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.equals=" + DEFAULT_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.equals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder not equals to DEFAULT_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.notEquals=" + DEFAULT_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder not equals to UPDATED_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.notEquals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsInShouldWork() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder in DEFAULT_SEQ_ORDER or UPDATED_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.in=" + DEFAULT_SEQ_ORDER + "," + UPDATED_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.in=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder is not null
        defaultLocCountryMasterShouldBeFound("seqOrder.specified=true");

        // Get all the locCountryMasterList where seqOrder is null
        defaultLocCountryMasterShouldNotBeFound("seqOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder is greater than or equal to DEFAULT_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.greaterThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder is greater than or equal to UPDATED_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.greaterThanOrEqual=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder is less than or equal to DEFAULT_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.lessThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder is less than or equal to SMALLER_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.lessThanOrEqual=" + SMALLER_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder is less than DEFAULT_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.lessThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder is less than UPDATED_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.lessThan=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCountryMastersBySeqOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);

        // Get all the locCountryMasterList where seqOrder is greater than DEFAULT_SEQ_ORDER
        defaultLocCountryMasterShouldNotBeFound("seqOrder.greaterThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locCountryMasterList where seqOrder is greater than SMALLER_SEQ_ORDER
        defaultLocCountryMasterShouldBeFound("seqOrder.greaterThan=" + SMALLER_SEQ_ORDER);
    }


    @Test
    @Transactional
    public void getAllLocCountryMastersByLocStateMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        locCountryMasterRepository.saveAndFlush(locCountryMaster);
        LocStateMaster locStateMaster = LocStateMasterResourceIT.createEntity(em);
        em.persist(locStateMaster);
        em.flush();
        locCountryMaster.addLocStateMaster(locStateMaster);
        locCountryMasterRepository.saveAndFlush(locCountryMaster);
        Long locStateMasterId = locStateMaster.getId();

        // Get all the locCountryMasterList where locStateMaster equals to locStateMasterId
        defaultLocCountryMasterShouldBeFound("locStateMasterId.equals=" + locStateMasterId);

        // Get all the locCountryMasterList where locStateMaster equals to locStateMasterId + 1
        defaultLocCountryMasterShouldNotBeFound("locStateMasterId.equals=" + (locStateMasterId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocCountryMasterShouldBeFound(String filter) throws Exception {
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locCountryMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].countryName").value(hasItem(DEFAULT_COUNTRY_NAME)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].zipCodeFormat").value(hasItem(DEFAULT_ZIP_CODE_FORMAT)))
            .andExpect(jsonPath("$.[*].continentId").value(hasItem(DEFAULT_CONTINENT_ID)))
            .andExpect(jsonPath("$.[*].flagImagePath").value(hasItem(DEFAULT_FLAG_IMAGE_PATH)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].countryId").value(hasItem(DEFAULT_COUNTRY_ID)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));

        // Check, that the count call also returns 1
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocCountryMasterShouldNotBeFound(String filter) throws Exception {
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocCountryMaster() throws Exception {
        // Get the locCountryMaster
        restLocCountryMasterMockMvc.perform(get("/api/loc-country-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocCountryMaster() throws Exception {
        // Initialize the database
        locCountryMasterService.save(locCountryMaster);

        int databaseSizeBeforeUpdate = locCountryMasterRepository.findAll().size();

        // Update the locCountryMaster
        LocCountryMaster updatedLocCountryMaster = locCountryMasterRepository.findById(locCountryMaster.getId()).get();
        // Disconnect from session so that the updates on updatedLocCountryMaster are not directly saved in db
        em.detach(updatedLocCountryMaster);
        updatedLocCountryMaster
            .countryName(UPDATED_COUNTRY_NAME)
            .countryCode(UPDATED_COUNTRY_CODE)
            .isDeleted(UPDATED_IS_DELETED)
            .zipCodeFormat(UPDATED_ZIP_CODE_FORMAT)
            .continentId(UPDATED_CONTINENT_ID)
            .flagImagePath(UPDATED_FLAG_IMAGE_PATH)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .countryId(UPDATED_COUNTRY_ID)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);

        restLocCountryMasterMockMvc.perform(put("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocCountryMaster)))
            .andExpect(status().isOk());

        // Validate the LocCountryMaster in the database
        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeUpdate);
        LocCountryMaster testLocCountryMaster = locCountryMasterList.get(locCountryMasterList.size() - 1);
        assertThat(testLocCountryMaster.getCountryName()).isEqualTo(UPDATED_COUNTRY_NAME);
        assertThat(testLocCountryMaster.getCountryCode()).isEqualTo(UPDATED_COUNTRY_CODE);
        assertThat(testLocCountryMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocCountryMaster.getZipCodeFormat()).isEqualTo(UPDATED_ZIP_CODE_FORMAT);
        assertThat(testLocCountryMaster.getContinentId()).isEqualTo(UPDATED_CONTINENT_ID);
        assertThat(testLocCountryMaster.getFlagImagePath()).isEqualTo(UPDATED_FLAG_IMAGE_PATH);
        assertThat(testLocCountryMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocCountryMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocCountryMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testLocCountryMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocCountryMaster.getCountryId()).isEqualTo(UPDATED_COUNTRY_ID);
        assertThat(testLocCountryMaster.getIsApproved()).isEqualTo(UPDATED_IS_APPROVED);
        assertThat(testLocCountryMaster.getSeqOrder()).isEqualTo(UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void updateNonExistingLocCountryMaster() throws Exception {
        int databaseSizeBeforeUpdate = locCountryMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocCountryMasterMockMvc.perform(put("/api/loc-country-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCountryMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocCountryMaster in the database
        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocCountryMaster() throws Exception {
        // Initialize the database
        locCountryMasterService.save(locCountryMaster);

        int databaseSizeBeforeDelete = locCountryMasterRepository.findAll().size();

        // Delete the locCountryMaster
        restLocCountryMasterMockMvc.perform(delete("/api/loc-country-masters/{id}", locCountryMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocCountryMaster> locCountryMasterList = locCountryMasterRepository.findAll();
        assertThat(locCountryMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
