package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.CompanyRequest;
import com.crisil.onesource.domain.CompanyAddressMaster;
import com.crisil.onesource.domain.CompanyTanPanRocDtls;
import com.crisil.onesource.repository.CompanyRequestRepository;
import com.crisil.onesource.service.CompanyRequestService;
import com.crisil.onesource.service.dto.CompanyRequestCriteria;
import com.crisil.onesource.service.CompanyRequestQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyRequestResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CompanyRequestResourceIT {

    private static final String DEFAULT_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_COMPANY_TYPE_ID = 1;
    private static final Integer UPDATED_COMPANY_TYPE_ID = 2;
    private static final Integer SMALLER_COMPANY_TYPE_ID = 1 - 1;

    private static final Integer DEFAULT_SECTOR_ID = 1;
    private static final Integer UPDATED_SECTOR_ID = 2;
    private static final Integer SMALLER_SECTOR_ID = 1 - 1;

    private static final Integer DEFAULT_INDUSTRY_ID = 1;
    private static final Integer UPDATED_INDUSTRY_ID = 2;
    private static final Integer SMALLER_INDUSTRY_ID = 1 - 1;

    private static final Integer DEFAULT_BUSINESS_AREA_ID = 1;
    private static final Integer UPDATED_BUSINESS_AREA_ID = 2;
    private static final Integer SMALLER_BUSINESS_AREA_ID = 1 - 1;

    private static final String DEFAULT_CLIENT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_GROUP_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REMARKS = "AAAAAAAAAA";
    private static final String UPDATED_REMARKS = "BBBBBBBBBB";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_BILLING_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_BILLING_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_COMPANY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_COMPANY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REQUESTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_REQUESTED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_APPROVED_BY = "AAAAAAAAAA";
    private static final String UPDATED_APPROVED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_REQUESTED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_REQUESTED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_APPROVED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_APPROVED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_ORIGINAL_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_ORIGINAL_FILENAME = "BBBBBBBBBB";

    private static final String DEFAULT_DISPLAY_FILENAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_FILENAME = "BBBBBBBBBB";

    @Autowired
    private CompanyRequestRepository companyRequestRepository;

    @Autowired
    private CompanyRequestService companyRequestService;

    @Autowired
    private CompanyRequestQueryService companyRequestQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyRequestMockMvc;

    private CompanyRequest companyRequest;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyRequest createEntity(EntityManager em) {
        CompanyRequest companyRequest = new CompanyRequest()
            .companyName(DEFAULT_COMPANY_NAME)
            .companyTypeId(DEFAULT_COMPANY_TYPE_ID)
            .sectorId(DEFAULT_SECTOR_ID)
            .industryId(DEFAULT_INDUSTRY_ID)
            .businessAreaId(DEFAULT_BUSINESS_AREA_ID)
            .clientGroupName(DEFAULT_CLIENT_GROUP_NAME)
            .remarks(DEFAULT_REMARKS)
            .recordId(DEFAULT_RECORD_ID)
            .companyCode(DEFAULT_COMPANY_CODE)
            .status(DEFAULT_STATUS)
            .reason(DEFAULT_REASON)
            .isDeleted(DEFAULT_IS_DELETED)
            .billingType(DEFAULT_BILLING_TYPE)
            .displayCompanyName(DEFAULT_DISPLAY_COMPANY_NAME)
            .requestedBy(DEFAULT_REQUESTED_BY)
            .approvedBy(DEFAULT_APPROVED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .requestedDate(DEFAULT_REQUESTED_DATE)
            .approvedDate(DEFAULT_APPROVED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .originalFilename(DEFAULT_ORIGINAL_FILENAME)
            .displayFilename(DEFAULT_DISPLAY_FILENAME);
        return companyRequest;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompanyRequest createUpdatedEntity(EntityManager em) {
        CompanyRequest companyRequest = new CompanyRequest()
            .companyName(UPDATED_COMPANY_NAME)
            .companyTypeId(UPDATED_COMPANY_TYPE_ID)
            .sectorId(UPDATED_SECTOR_ID)
            .industryId(UPDATED_INDUSTRY_ID)
            .businessAreaId(UPDATED_BUSINESS_AREA_ID)
            .clientGroupName(UPDATED_CLIENT_GROUP_NAME)
            .remarks(UPDATED_REMARKS)
            .recordId(UPDATED_RECORD_ID)
            .companyCode(UPDATED_COMPANY_CODE)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .isDeleted(UPDATED_IS_DELETED)
            .billingType(UPDATED_BILLING_TYPE)
            .displayCompanyName(UPDATED_DISPLAY_COMPANY_NAME)
            .requestedBy(UPDATED_REQUESTED_BY)
            .approvedBy(UPDATED_APPROVED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .requestedDate(UPDATED_REQUESTED_DATE)
            .approvedDate(UPDATED_APPROVED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .originalFilename(UPDATED_ORIGINAL_FILENAME)
            .displayFilename(UPDATED_DISPLAY_FILENAME);
        return companyRequest;
    }

    @BeforeEach
    public void initTest() {
        companyRequest = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompanyRequest() throws Exception {
        int databaseSizeBeforeCreate = companyRequestRepository.findAll().size();
        // Create the CompanyRequest
        restCompanyRequestMockMvc.perform(post("/api/company-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyRequest)))
            .andExpect(status().isCreated());

        // Validate the CompanyRequest in the database
        List<CompanyRequest> companyRequestList = companyRequestRepository.findAll();
        assertThat(companyRequestList).hasSize(databaseSizeBeforeCreate + 1);
        CompanyRequest testCompanyRequest = companyRequestList.get(companyRequestList.size() - 1);
        assertThat(testCompanyRequest.getCompanyName()).isEqualTo(DEFAULT_COMPANY_NAME);
        assertThat(testCompanyRequest.getCompanyTypeId()).isEqualTo(DEFAULT_COMPANY_TYPE_ID);
        assertThat(testCompanyRequest.getSectorId()).isEqualTo(DEFAULT_SECTOR_ID);
        assertThat(testCompanyRequest.getIndustryId()).isEqualTo(DEFAULT_INDUSTRY_ID);
        assertThat(testCompanyRequest.getBusinessAreaId()).isEqualTo(DEFAULT_BUSINESS_AREA_ID);
        assertThat(testCompanyRequest.getClientGroupName()).isEqualTo(DEFAULT_CLIENT_GROUP_NAME);
        assertThat(testCompanyRequest.getRemarks()).isEqualTo(DEFAULT_REMARKS);
        assertThat(testCompanyRequest.getRecordId()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testCompanyRequest.getCompanyCode()).isEqualTo(DEFAULT_COMPANY_CODE);
        assertThat(testCompanyRequest.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCompanyRequest.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testCompanyRequest.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testCompanyRequest.getBillingType()).isEqualTo(DEFAULT_BILLING_TYPE);
        assertThat(testCompanyRequest.getDisplayCompanyName()).isEqualTo(DEFAULT_DISPLAY_COMPANY_NAME);
        assertThat(testCompanyRequest.getRequestedBy()).isEqualTo(DEFAULT_REQUESTED_BY);
        assertThat(testCompanyRequest.getApprovedBy()).isEqualTo(DEFAULT_APPROVED_BY);
        assertThat(testCompanyRequest.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCompanyRequest.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCompanyRequest.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testCompanyRequest.getRequestedDate()).isEqualTo(DEFAULT_REQUESTED_DATE);
        assertThat(testCompanyRequest.getApprovedDate()).isEqualTo(DEFAULT_APPROVED_DATE);
        assertThat(testCompanyRequest.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testCompanyRequest.getOriginalFilename()).isEqualTo(DEFAULT_ORIGINAL_FILENAME);
        assertThat(testCompanyRequest.getDisplayFilename()).isEqualTo(DEFAULT_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void createCompanyRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyRequestRepository.findAll().size();

        // Create the CompanyRequest with an existing ID
        companyRequest.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyRequestMockMvc.perform(post("/api/company-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyRequest)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyRequest in the database
        List<CompanyRequest> companyRequestList = companyRequestRepository.findAll();
        assertThat(companyRequestList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkRecordIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRequestRepository.findAll().size();
        // set the field null
        companyRequest.setRecordId(null);

        // Create the CompanyRequest, which fails.


        restCompanyRequestMockMvc.perform(post("/api/company-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyRequest)))
            .andExpect(status().isBadRequest());

        List<CompanyRequest> companyRequestList = companyRequestRepository.findAll();
        assertThat(companyRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanyRequests() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList
        restCompanyRequestMockMvc.perform(get("/api/company-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].companyTypeId").value(hasItem(DEFAULT_COMPANY_TYPE_ID)))
            .andExpect(jsonPath("$.[*].sectorId").value(hasItem(DEFAULT_SECTOR_ID)))
            .andExpect(jsonPath("$.[*].industryId").value(hasItem(DEFAULT_INDUSTRY_ID)))
            .andExpect(jsonPath("$.[*].businessAreaId").value(hasItem(DEFAULT_BUSINESS_AREA_ID)))
            .andExpect(jsonPath("$.[*].clientGroupName").value(hasItem(DEFAULT_CLIENT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].billingType").value(hasItem(DEFAULT_BILLING_TYPE)))
            .andExpect(jsonPath("$.[*].displayCompanyName").value(hasItem(DEFAULT_DISPLAY_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].approvedBy").value(hasItem(DEFAULT_APPROVED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].requestedDate").value(hasItem(DEFAULT_REQUESTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].approvedDate").value(hasItem(DEFAULT_APPROVED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].originalFilename").value(hasItem(DEFAULT_ORIGINAL_FILENAME)))
            .andExpect(jsonPath("$.[*].displayFilename").value(hasItem(DEFAULT_DISPLAY_FILENAME)));
    }
    
    @Test
    @Transactional
    public void getCompanyRequest() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get the companyRequest
        restCompanyRequestMockMvc.perform(get("/api/company-requests/{id}", companyRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(companyRequest.getId().intValue()))
            .andExpect(jsonPath("$.companyName").value(DEFAULT_COMPANY_NAME))
            .andExpect(jsonPath("$.companyTypeId").value(DEFAULT_COMPANY_TYPE_ID))
            .andExpect(jsonPath("$.sectorId").value(DEFAULT_SECTOR_ID))
            .andExpect(jsonPath("$.industryId").value(DEFAULT_INDUSTRY_ID))
            .andExpect(jsonPath("$.businessAreaId").value(DEFAULT_BUSINESS_AREA_ID))
            .andExpect(jsonPath("$.clientGroupName").value(DEFAULT_CLIENT_GROUP_NAME))
            .andExpect(jsonPath("$.remarks").value(DEFAULT_REMARKS))
            .andExpect(jsonPath("$.recordId").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.companyCode").value(DEFAULT_COMPANY_CODE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.billingType").value(DEFAULT_BILLING_TYPE))
            .andExpect(jsonPath("$.displayCompanyName").value(DEFAULT_DISPLAY_COMPANY_NAME))
            .andExpect(jsonPath("$.requestedBy").value(DEFAULT_REQUESTED_BY))
            .andExpect(jsonPath("$.approvedBy").value(DEFAULT_APPROVED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.requestedDate").value(DEFAULT_REQUESTED_DATE.toString()))
            .andExpect(jsonPath("$.approvedDate").value(DEFAULT_APPROVED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.originalFilename").value(DEFAULT_ORIGINAL_FILENAME))
            .andExpect(jsonPath("$.displayFilename").value(DEFAULT_DISPLAY_FILENAME));
    }


    @Test
    @Transactional
    public void getCompanyRequestsByIdFiltering() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        Long id = companyRequest.getId();

        defaultCompanyRequestShouldBeFound("id.equals=" + id);
        defaultCompanyRequestShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyRequestShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyRequestShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyRequestShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyRequestShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyName equals to DEFAULT_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("companyName.equals=" + DEFAULT_COMPANY_NAME);

        // Get all the companyRequestList where companyName equals to UPDATED_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("companyName.equals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyName not equals to DEFAULT_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("companyName.notEquals=" + DEFAULT_COMPANY_NAME);

        // Get all the companyRequestList where companyName not equals to UPDATED_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("companyName.notEquals=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyName in DEFAULT_COMPANY_NAME or UPDATED_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("companyName.in=" + DEFAULT_COMPANY_NAME + "," + UPDATED_COMPANY_NAME);

        // Get all the companyRequestList where companyName equals to UPDATED_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("companyName.in=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyName is not null
        defaultCompanyRequestShouldBeFound("companyName.specified=true");

        // Get all the companyRequestList where companyName is null
        defaultCompanyRequestShouldNotBeFound("companyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyName contains DEFAULT_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("companyName.contains=" + DEFAULT_COMPANY_NAME);

        // Get all the companyRequestList where companyName contains UPDATED_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("companyName.contains=" + UPDATED_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyName does not contain DEFAULT_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("companyName.doesNotContain=" + DEFAULT_COMPANY_NAME);

        // Get all the companyRequestList where companyName does not contain UPDATED_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("companyName.doesNotContain=" + UPDATED_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId equals to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.equals=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId equals to UPDATED_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.equals=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId not equals to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.notEquals=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId not equals to UPDATED_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.notEquals=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId in DEFAULT_COMPANY_TYPE_ID or UPDATED_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.in=" + DEFAULT_COMPANY_TYPE_ID + "," + UPDATED_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId equals to UPDATED_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.in=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId is not null
        defaultCompanyRequestShouldBeFound("companyTypeId.specified=true");

        // Get all the companyRequestList where companyTypeId is null
        defaultCompanyRequestShouldNotBeFound("companyTypeId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId is greater than or equal to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.greaterThanOrEqual=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId is greater than or equal to UPDATED_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.greaterThanOrEqual=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId is less than or equal to DEFAULT_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.lessThanOrEqual=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId is less than or equal to SMALLER_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.lessThanOrEqual=" + SMALLER_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId is less than DEFAULT_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.lessThan=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId is less than UPDATED_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.lessThan=" + UPDATED_COMPANY_TYPE_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTypeIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyTypeId is greater than DEFAULT_COMPANY_TYPE_ID
        defaultCompanyRequestShouldNotBeFound("companyTypeId.greaterThan=" + DEFAULT_COMPANY_TYPE_ID);

        // Get all the companyRequestList where companyTypeId is greater than SMALLER_COMPANY_TYPE_ID
        defaultCompanyRequestShouldBeFound("companyTypeId.greaterThan=" + SMALLER_COMPANY_TYPE_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId equals to DEFAULT_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.equals=" + DEFAULT_SECTOR_ID);

        // Get all the companyRequestList where sectorId equals to UPDATED_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.equals=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId not equals to DEFAULT_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.notEquals=" + DEFAULT_SECTOR_ID);

        // Get all the companyRequestList where sectorId not equals to UPDATED_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.notEquals=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId in DEFAULT_SECTOR_ID or UPDATED_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.in=" + DEFAULT_SECTOR_ID + "," + UPDATED_SECTOR_ID);

        // Get all the companyRequestList where sectorId equals to UPDATED_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.in=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId is not null
        defaultCompanyRequestShouldBeFound("sectorId.specified=true");

        // Get all the companyRequestList where sectorId is null
        defaultCompanyRequestShouldNotBeFound("sectorId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId is greater than or equal to DEFAULT_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.greaterThanOrEqual=" + DEFAULT_SECTOR_ID);

        // Get all the companyRequestList where sectorId is greater than or equal to UPDATED_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.greaterThanOrEqual=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId is less than or equal to DEFAULT_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.lessThanOrEqual=" + DEFAULT_SECTOR_ID);

        // Get all the companyRequestList where sectorId is less than or equal to SMALLER_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.lessThanOrEqual=" + SMALLER_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId is less than DEFAULT_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.lessThan=" + DEFAULT_SECTOR_ID);

        // Get all the companyRequestList where sectorId is less than UPDATED_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.lessThan=" + UPDATED_SECTOR_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsBySectorIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where sectorId is greater than DEFAULT_SECTOR_ID
        defaultCompanyRequestShouldNotBeFound("sectorId.greaterThan=" + DEFAULT_SECTOR_ID);

        // Get all the companyRequestList where sectorId is greater than SMALLER_SECTOR_ID
        defaultCompanyRequestShouldBeFound("sectorId.greaterThan=" + SMALLER_SECTOR_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId equals to DEFAULT_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.equals=" + DEFAULT_INDUSTRY_ID);

        // Get all the companyRequestList where industryId equals to UPDATED_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.equals=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId not equals to DEFAULT_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.notEquals=" + DEFAULT_INDUSTRY_ID);

        // Get all the companyRequestList where industryId not equals to UPDATED_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.notEquals=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId in DEFAULT_INDUSTRY_ID or UPDATED_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.in=" + DEFAULT_INDUSTRY_ID + "," + UPDATED_INDUSTRY_ID);

        // Get all the companyRequestList where industryId equals to UPDATED_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.in=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId is not null
        defaultCompanyRequestShouldBeFound("industryId.specified=true");

        // Get all the companyRequestList where industryId is null
        defaultCompanyRequestShouldNotBeFound("industryId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId is greater than or equal to DEFAULT_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.greaterThanOrEqual=" + DEFAULT_INDUSTRY_ID);

        // Get all the companyRequestList where industryId is greater than or equal to UPDATED_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.greaterThanOrEqual=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId is less than or equal to DEFAULT_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.lessThanOrEqual=" + DEFAULT_INDUSTRY_ID);

        // Get all the companyRequestList where industryId is less than or equal to SMALLER_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.lessThanOrEqual=" + SMALLER_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId is less than DEFAULT_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.lessThan=" + DEFAULT_INDUSTRY_ID);

        // Get all the companyRequestList where industryId is less than UPDATED_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.lessThan=" + UPDATED_INDUSTRY_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIndustryIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where industryId is greater than DEFAULT_INDUSTRY_ID
        defaultCompanyRequestShouldNotBeFound("industryId.greaterThan=" + DEFAULT_INDUSTRY_ID);

        // Get all the companyRequestList where industryId is greater than SMALLER_INDUSTRY_ID
        defaultCompanyRequestShouldBeFound("industryId.greaterThan=" + SMALLER_INDUSTRY_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId equals to DEFAULT_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.equals=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId equals to UPDATED_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.equals=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId not equals to DEFAULT_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.notEquals=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId not equals to UPDATED_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.notEquals=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId in DEFAULT_BUSINESS_AREA_ID or UPDATED_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.in=" + DEFAULT_BUSINESS_AREA_ID + "," + UPDATED_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId equals to UPDATED_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.in=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId is not null
        defaultCompanyRequestShouldBeFound("businessAreaId.specified=true");

        // Get all the companyRequestList where businessAreaId is null
        defaultCompanyRequestShouldNotBeFound("businessAreaId.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId is greater than or equal to DEFAULT_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.greaterThanOrEqual=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId is greater than or equal to UPDATED_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.greaterThanOrEqual=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId is less than or equal to DEFAULT_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.lessThanOrEqual=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId is less than or equal to SMALLER_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.lessThanOrEqual=" + SMALLER_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsLessThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId is less than DEFAULT_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.lessThan=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId is less than UPDATED_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.lessThan=" + UPDATED_BUSINESS_AREA_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBusinessAreaIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where businessAreaId is greater than DEFAULT_BUSINESS_AREA_ID
        defaultCompanyRequestShouldNotBeFound("businessAreaId.greaterThan=" + DEFAULT_BUSINESS_AREA_ID);

        // Get all the companyRequestList where businessAreaId is greater than SMALLER_BUSINESS_AREA_ID
        defaultCompanyRequestShouldBeFound("businessAreaId.greaterThan=" + SMALLER_BUSINESS_AREA_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByClientGroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where clientGroupName equals to DEFAULT_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldBeFound("clientGroupName.equals=" + DEFAULT_CLIENT_GROUP_NAME);

        // Get all the companyRequestList where clientGroupName equals to UPDATED_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldNotBeFound("clientGroupName.equals=" + UPDATED_CLIENT_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByClientGroupNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where clientGroupName not equals to DEFAULT_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldNotBeFound("clientGroupName.notEquals=" + DEFAULT_CLIENT_GROUP_NAME);

        // Get all the companyRequestList where clientGroupName not equals to UPDATED_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldBeFound("clientGroupName.notEquals=" + UPDATED_CLIENT_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByClientGroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where clientGroupName in DEFAULT_CLIENT_GROUP_NAME or UPDATED_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldBeFound("clientGroupName.in=" + DEFAULT_CLIENT_GROUP_NAME + "," + UPDATED_CLIENT_GROUP_NAME);

        // Get all the companyRequestList where clientGroupName equals to UPDATED_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldNotBeFound("clientGroupName.in=" + UPDATED_CLIENT_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByClientGroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where clientGroupName is not null
        defaultCompanyRequestShouldBeFound("clientGroupName.specified=true");

        // Get all the companyRequestList where clientGroupName is null
        defaultCompanyRequestShouldNotBeFound("clientGroupName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByClientGroupNameContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where clientGroupName contains DEFAULT_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldBeFound("clientGroupName.contains=" + DEFAULT_CLIENT_GROUP_NAME);

        // Get all the companyRequestList where clientGroupName contains UPDATED_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldNotBeFound("clientGroupName.contains=" + UPDATED_CLIENT_GROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByClientGroupNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where clientGroupName does not contain DEFAULT_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldNotBeFound("clientGroupName.doesNotContain=" + DEFAULT_CLIENT_GROUP_NAME);

        // Get all the companyRequestList where clientGroupName does not contain UPDATED_CLIENT_GROUP_NAME
        defaultCompanyRequestShouldBeFound("clientGroupName.doesNotContain=" + UPDATED_CLIENT_GROUP_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByRemarksIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where remarks equals to DEFAULT_REMARKS
        defaultCompanyRequestShouldBeFound("remarks.equals=" + DEFAULT_REMARKS);

        // Get all the companyRequestList where remarks equals to UPDATED_REMARKS
        defaultCompanyRequestShouldNotBeFound("remarks.equals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRemarksIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where remarks not equals to DEFAULT_REMARKS
        defaultCompanyRequestShouldNotBeFound("remarks.notEquals=" + DEFAULT_REMARKS);

        // Get all the companyRequestList where remarks not equals to UPDATED_REMARKS
        defaultCompanyRequestShouldBeFound("remarks.notEquals=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRemarksIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where remarks in DEFAULT_REMARKS or UPDATED_REMARKS
        defaultCompanyRequestShouldBeFound("remarks.in=" + DEFAULT_REMARKS + "," + UPDATED_REMARKS);

        // Get all the companyRequestList where remarks equals to UPDATED_REMARKS
        defaultCompanyRequestShouldNotBeFound("remarks.in=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRemarksIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where remarks is not null
        defaultCompanyRequestShouldBeFound("remarks.specified=true");

        // Get all the companyRequestList where remarks is null
        defaultCompanyRequestShouldNotBeFound("remarks.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByRemarksContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where remarks contains DEFAULT_REMARKS
        defaultCompanyRequestShouldBeFound("remarks.contains=" + DEFAULT_REMARKS);

        // Get all the companyRequestList where remarks contains UPDATED_REMARKS
        defaultCompanyRequestShouldNotBeFound("remarks.contains=" + UPDATED_REMARKS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRemarksNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where remarks does not contain DEFAULT_REMARKS
        defaultCompanyRequestShouldNotBeFound("remarks.doesNotContain=" + DEFAULT_REMARKS);

        // Get all the companyRequestList where remarks does not contain UPDATED_REMARKS
        defaultCompanyRequestShouldBeFound("remarks.doesNotContain=" + UPDATED_REMARKS);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where recordId equals to DEFAULT_RECORD_ID
        defaultCompanyRequestShouldBeFound("recordId.equals=" + DEFAULT_RECORD_ID);

        // Get all the companyRequestList where recordId equals to UPDATED_RECORD_ID
        defaultCompanyRequestShouldNotBeFound("recordId.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where recordId not equals to DEFAULT_RECORD_ID
        defaultCompanyRequestShouldNotBeFound("recordId.notEquals=" + DEFAULT_RECORD_ID);

        // Get all the companyRequestList where recordId not equals to UPDATED_RECORD_ID
        defaultCompanyRequestShouldBeFound("recordId.notEquals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where recordId in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultCompanyRequestShouldBeFound("recordId.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the companyRequestList where recordId equals to UPDATED_RECORD_ID
        defaultCompanyRequestShouldNotBeFound("recordId.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where recordId is not null
        defaultCompanyRequestShouldBeFound("recordId.specified=true");

        // Get all the companyRequestList where recordId is null
        defaultCompanyRequestShouldNotBeFound("recordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByRecordIdContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where recordId contains DEFAULT_RECORD_ID
        defaultCompanyRequestShouldBeFound("recordId.contains=" + DEFAULT_RECORD_ID);

        // Get all the companyRequestList where recordId contains UPDATED_RECORD_ID
        defaultCompanyRequestShouldNotBeFound("recordId.contains=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where recordId does not contain DEFAULT_RECORD_ID
        defaultCompanyRequestShouldNotBeFound("recordId.doesNotContain=" + DEFAULT_RECORD_ID);

        // Get all the companyRequestList where recordId does not contain UPDATED_RECORD_ID
        defaultCompanyRequestShouldBeFound("recordId.doesNotContain=" + UPDATED_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyCode equals to DEFAULT_COMPANY_CODE
        defaultCompanyRequestShouldBeFound("companyCode.equals=" + DEFAULT_COMPANY_CODE);

        // Get all the companyRequestList where companyCode equals to UPDATED_COMPANY_CODE
        defaultCompanyRequestShouldNotBeFound("companyCode.equals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyCode not equals to DEFAULT_COMPANY_CODE
        defaultCompanyRequestShouldNotBeFound("companyCode.notEquals=" + DEFAULT_COMPANY_CODE);

        // Get all the companyRequestList where companyCode not equals to UPDATED_COMPANY_CODE
        defaultCompanyRequestShouldBeFound("companyCode.notEquals=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyCode in DEFAULT_COMPANY_CODE or UPDATED_COMPANY_CODE
        defaultCompanyRequestShouldBeFound("companyCode.in=" + DEFAULT_COMPANY_CODE + "," + UPDATED_COMPANY_CODE);

        // Get all the companyRequestList where companyCode equals to UPDATED_COMPANY_CODE
        defaultCompanyRequestShouldNotBeFound("companyCode.in=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyCode is not null
        defaultCompanyRequestShouldBeFound("companyCode.specified=true");

        // Get all the companyRequestList where companyCode is null
        defaultCompanyRequestShouldNotBeFound("companyCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyCodeContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyCode contains DEFAULT_COMPANY_CODE
        defaultCompanyRequestShouldBeFound("companyCode.contains=" + DEFAULT_COMPANY_CODE);

        // Get all the companyRequestList where companyCode contains UPDATED_COMPANY_CODE
        defaultCompanyRequestShouldNotBeFound("companyCode.contains=" + UPDATED_COMPANY_CODE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where companyCode does not contain DEFAULT_COMPANY_CODE
        defaultCompanyRequestShouldNotBeFound("companyCode.doesNotContain=" + DEFAULT_COMPANY_CODE);

        // Get all the companyRequestList where companyCode does not contain UPDATED_COMPANY_CODE
        defaultCompanyRequestShouldBeFound("companyCode.doesNotContain=" + UPDATED_COMPANY_CODE);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where status equals to DEFAULT_STATUS
        defaultCompanyRequestShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the companyRequestList where status equals to UPDATED_STATUS
        defaultCompanyRequestShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where status not equals to DEFAULT_STATUS
        defaultCompanyRequestShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the companyRequestList where status not equals to UPDATED_STATUS
        defaultCompanyRequestShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultCompanyRequestShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the companyRequestList where status equals to UPDATED_STATUS
        defaultCompanyRequestShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where status is not null
        defaultCompanyRequestShouldBeFound("status.specified=true");

        // Get all the companyRequestList where status is null
        defaultCompanyRequestShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByStatusContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where status contains DEFAULT_STATUS
        defaultCompanyRequestShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the companyRequestList where status contains UPDATED_STATUS
        defaultCompanyRequestShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where status does not contain DEFAULT_STATUS
        defaultCompanyRequestShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the companyRequestList where status does not contain UPDATED_STATUS
        defaultCompanyRequestShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByReasonIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where reason equals to DEFAULT_REASON
        defaultCompanyRequestShouldBeFound("reason.equals=" + DEFAULT_REASON);

        // Get all the companyRequestList where reason equals to UPDATED_REASON
        defaultCompanyRequestShouldNotBeFound("reason.equals=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByReasonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where reason not equals to DEFAULT_REASON
        defaultCompanyRequestShouldNotBeFound("reason.notEquals=" + DEFAULT_REASON);

        // Get all the companyRequestList where reason not equals to UPDATED_REASON
        defaultCompanyRequestShouldBeFound("reason.notEquals=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByReasonIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where reason in DEFAULT_REASON or UPDATED_REASON
        defaultCompanyRequestShouldBeFound("reason.in=" + DEFAULT_REASON + "," + UPDATED_REASON);

        // Get all the companyRequestList where reason equals to UPDATED_REASON
        defaultCompanyRequestShouldNotBeFound("reason.in=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByReasonIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where reason is not null
        defaultCompanyRequestShouldBeFound("reason.specified=true");

        // Get all the companyRequestList where reason is null
        defaultCompanyRequestShouldNotBeFound("reason.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByReasonContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where reason contains DEFAULT_REASON
        defaultCompanyRequestShouldBeFound("reason.contains=" + DEFAULT_REASON);

        // Get all the companyRequestList where reason contains UPDATED_REASON
        defaultCompanyRequestShouldNotBeFound("reason.contains=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByReasonNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where reason does not contain DEFAULT_REASON
        defaultCompanyRequestShouldNotBeFound("reason.doesNotContain=" + DEFAULT_REASON);

        // Get all the companyRequestList where reason does not contain UPDATED_REASON
        defaultCompanyRequestShouldBeFound("reason.doesNotContain=" + UPDATED_REASON);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where isDeleted equals to DEFAULT_IS_DELETED
        defaultCompanyRequestShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the companyRequestList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyRequestShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultCompanyRequestShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the companyRequestList where isDeleted not equals to UPDATED_IS_DELETED
        defaultCompanyRequestShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultCompanyRequestShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the companyRequestList where isDeleted equals to UPDATED_IS_DELETED
        defaultCompanyRequestShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where isDeleted is not null
        defaultCompanyRequestShouldBeFound("isDeleted.specified=true");

        // Get all the companyRequestList where isDeleted is null
        defaultCompanyRequestShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where isDeleted contains DEFAULT_IS_DELETED
        defaultCompanyRequestShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the companyRequestList where isDeleted contains UPDATED_IS_DELETED
        defaultCompanyRequestShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultCompanyRequestShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the companyRequestList where isDeleted does not contain UPDATED_IS_DELETED
        defaultCompanyRequestShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByBillingTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where billingType equals to DEFAULT_BILLING_TYPE
        defaultCompanyRequestShouldBeFound("billingType.equals=" + DEFAULT_BILLING_TYPE);

        // Get all the companyRequestList where billingType equals to UPDATED_BILLING_TYPE
        defaultCompanyRequestShouldNotBeFound("billingType.equals=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBillingTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where billingType not equals to DEFAULT_BILLING_TYPE
        defaultCompanyRequestShouldNotBeFound("billingType.notEquals=" + DEFAULT_BILLING_TYPE);

        // Get all the companyRequestList where billingType not equals to UPDATED_BILLING_TYPE
        defaultCompanyRequestShouldBeFound("billingType.notEquals=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBillingTypeIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where billingType in DEFAULT_BILLING_TYPE or UPDATED_BILLING_TYPE
        defaultCompanyRequestShouldBeFound("billingType.in=" + DEFAULT_BILLING_TYPE + "," + UPDATED_BILLING_TYPE);

        // Get all the companyRequestList where billingType equals to UPDATED_BILLING_TYPE
        defaultCompanyRequestShouldNotBeFound("billingType.in=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBillingTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where billingType is not null
        defaultCompanyRequestShouldBeFound("billingType.specified=true");

        // Get all the companyRequestList where billingType is null
        defaultCompanyRequestShouldNotBeFound("billingType.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByBillingTypeContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where billingType contains DEFAULT_BILLING_TYPE
        defaultCompanyRequestShouldBeFound("billingType.contains=" + DEFAULT_BILLING_TYPE);

        // Get all the companyRequestList where billingType contains UPDATED_BILLING_TYPE
        defaultCompanyRequestShouldNotBeFound("billingType.contains=" + UPDATED_BILLING_TYPE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByBillingTypeNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where billingType does not contain DEFAULT_BILLING_TYPE
        defaultCompanyRequestShouldNotBeFound("billingType.doesNotContain=" + DEFAULT_BILLING_TYPE);

        // Get all the companyRequestList where billingType does not contain UPDATED_BILLING_TYPE
        defaultCompanyRequestShouldBeFound("billingType.doesNotContain=" + UPDATED_BILLING_TYPE);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayCompanyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayCompanyName equals to DEFAULT_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("displayCompanyName.equals=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the companyRequestList where displayCompanyName equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("displayCompanyName.equals=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayCompanyNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayCompanyName not equals to DEFAULT_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("displayCompanyName.notEquals=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the companyRequestList where displayCompanyName not equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("displayCompanyName.notEquals=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayCompanyNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayCompanyName in DEFAULT_DISPLAY_COMPANY_NAME or UPDATED_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("displayCompanyName.in=" + DEFAULT_DISPLAY_COMPANY_NAME + "," + UPDATED_DISPLAY_COMPANY_NAME);

        // Get all the companyRequestList where displayCompanyName equals to UPDATED_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("displayCompanyName.in=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayCompanyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayCompanyName is not null
        defaultCompanyRequestShouldBeFound("displayCompanyName.specified=true");

        // Get all the companyRequestList where displayCompanyName is null
        defaultCompanyRequestShouldNotBeFound("displayCompanyName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayCompanyNameContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayCompanyName contains DEFAULT_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("displayCompanyName.contains=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the companyRequestList where displayCompanyName contains UPDATED_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("displayCompanyName.contains=" + UPDATED_DISPLAY_COMPANY_NAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayCompanyNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayCompanyName does not contain DEFAULT_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldNotBeFound("displayCompanyName.doesNotContain=" + DEFAULT_DISPLAY_COMPANY_NAME);

        // Get all the companyRequestList where displayCompanyName does not contain UPDATED_DISPLAY_COMPANY_NAME
        defaultCompanyRequestShouldBeFound("displayCompanyName.doesNotContain=" + UPDATED_DISPLAY_COMPANY_NAME);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedBy equals to DEFAULT_REQUESTED_BY
        defaultCompanyRequestShouldBeFound("requestedBy.equals=" + DEFAULT_REQUESTED_BY);

        // Get all the companyRequestList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultCompanyRequestShouldNotBeFound("requestedBy.equals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedBy not equals to DEFAULT_REQUESTED_BY
        defaultCompanyRequestShouldNotBeFound("requestedBy.notEquals=" + DEFAULT_REQUESTED_BY);

        // Get all the companyRequestList where requestedBy not equals to UPDATED_REQUESTED_BY
        defaultCompanyRequestShouldBeFound("requestedBy.notEquals=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedBy in DEFAULT_REQUESTED_BY or UPDATED_REQUESTED_BY
        defaultCompanyRequestShouldBeFound("requestedBy.in=" + DEFAULT_REQUESTED_BY + "," + UPDATED_REQUESTED_BY);

        // Get all the companyRequestList where requestedBy equals to UPDATED_REQUESTED_BY
        defaultCompanyRequestShouldNotBeFound("requestedBy.in=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedBy is not null
        defaultCompanyRequestShouldBeFound("requestedBy.specified=true");

        // Get all the companyRequestList where requestedBy is null
        defaultCompanyRequestShouldNotBeFound("requestedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedByContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedBy contains DEFAULT_REQUESTED_BY
        defaultCompanyRequestShouldBeFound("requestedBy.contains=" + DEFAULT_REQUESTED_BY);

        // Get all the companyRequestList where requestedBy contains UPDATED_REQUESTED_BY
        defaultCompanyRequestShouldNotBeFound("requestedBy.contains=" + UPDATED_REQUESTED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedBy does not contain DEFAULT_REQUESTED_BY
        defaultCompanyRequestShouldNotBeFound("requestedBy.doesNotContain=" + DEFAULT_REQUESTED_BY);

        // Get all the companyRequestList where requestedBy does not contain UPDATED_REQUESTED_BY
        defaultCompanyRequestShouldBeFound("requestedBy.doesNotContain=" + UPDATED_REQUESTED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedBy equals to DEFAULT_APPROVED_BY
        defaultCompanyRequestShouldBeFound("approvedBy.equals=" + DEFAULT_APPROVED_BY);

        // Get all the companyRequestList where approvedBy equals to UPDATED_APPROVED_BY
        defaultCompanyRequestShouldNotBeFound("approvedBy.equals=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedBy not equals to DEFAULT_APPROVED_BY
        defaultCompanyRequestShouldNotBeFound("approvedBy.notEquals=" + DEFAULT_APPROVED_BY);

        // Get all the companyRequestList where approvedBy not equals to UPDATED_APPROVED_BY
        defaultCompanyRequestShouldBeFound("approvedBy.notEquals=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedBy in DEFAULT_APPROVED_BY or UPDATED_APPROVED_BY
        defaultCompanyRequestShouldBeFound("approvedBy.in=" + DEFAULT_APPROVED_BY + "," + UPDATED_APPROVED_BY);

        // Get all the companyRequestList where approvedBy equals to UPDATED_APPROVED_BY
        defaultCompanyRequestShouldNotBeFound("approvedBy.in=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedBy is not null
        defaultCompanyRequestShouldBeFound("approvedBy.specified=true");

        // Get all the companyRequestList where approvedBy is null
        defaultCompanyRequestShouldNotBeFound("approvedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedByContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedBy contains DEFAULT_APPROVED_BY
        defaultCompanyRequestShouldBeFound("approvedBy.contains=" + DEFAULT_APPROVED_BY);

        // Get all the companyRequestList where approvedBy contains UPDATED_APPROVED_BY
        defaultCompanyRequestShouldNotBeFound("approvedBy.contains=" + UPDATED_APPROVED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedBy does not contain DEFAULT_APPROVED_BY
        defaultCompanyRequestShouldNotBeFound("approvedBy.doesNotContain=" + DEFAULT_APPROVED_BY);

        // Get all the companyRequestList where approvedBy does not contain UPDATED_APPROVED_BY
        defaultCompanyRequestShouldBeFound("approvedBy.doesNotContain=" + UPDATED_APPROVED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdDate equals to DEFAULT_CREATED_DATE
        defaultCompanyRequestShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the companyRequestList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyRequestShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultCompanyRequestShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the companyRequestList where createdDate not equals to UPDATED_CREATED_DATE
        defaultCompanyRequestShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultCompanyRequestShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the companyRequestList where createdDate equals to UPDATED_CREATED_DATE
        defaultCompanyRequestShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdDate is not null
        defaultCompanyRequestShouldBeFound("createdDate.specified=true");

        // Get all the companyRequestList where createdDate is null
        defaultCompanyRequestShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdBy equals to DEFAULT_CREATED_BY
        defaultCompanyRequestShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the companyRequestList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyRequestShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCompanyRequestShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the companyRequestList where createdBy not equals to UPDATED_CREATED_BY
        defaultCompanyRequestShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCompanyRequestShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the companyRequestList where createdBy equals to UPDATED_CREATED_BY
        defaultCompanyRequestShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdBy is not null
        defaultCompanyRequestShouldBeFound("createdBy.specified=true");

        // Get all the companyRequestList where createdBy is null
        defaultCompanyRequestShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdBy contains DEFAULT_CREATED_BY
        defaultCompanyRequestShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the companyRequestList where createdBy contains UPDATED_CREATED_BY
        defaultCompanyRequestShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCompanyRequestShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the companyRequestList where createdBy does not contain UPDATED_CREATED_BY
        defaultCompanyRequestShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyRequestShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyRequestList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyRequestShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultCompanyRequestShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the companyRequestList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyRequestShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultCompanyRequestShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the companyRequestList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultCompanyRequestShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedDate is not null
        defaultCompanyRequestShouldBeFound("lastModifiedDate.specified=true");

        // Get all the companyRequestList where lastModifiedDate is null
        defaultCompanyRequestShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedDate equals to DEFAULT_REQUESTED_DATE
        defaultCompanyRequestShouldBeFound("requestedDate.equals=" + DEFAULT_REQUESTED_DATE);

        // Get all the companyRequestList where requestedDate equals to UPDATED_REQUESTED_DATE
        defaultCompanyRequestShouldNotBeFound("requestedDate.equals=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedDate not equals to DEFAULT_REQUESTED_DATE
        defaultCompanyRequestShouldNotBeFound("requestedDate.notEquals=" + DEFAULT_REQUESTED_DATE);

        // Get all the companyRequestList where requestedDate not equals to UPDATED_REQUESTED_DATE
        defaultCompanyRequestShouldBeFound("requestedDate.notEquals=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedDate in DEFAULT_REQUESTED_DATE or UPDATED_REQUESTED_DATE
        defaultCompanyRequestShouldBeFound("requestedDate.in=" + DEFAULT_REQUESTED_DATE + "," + UPDATED_REQUESTED_DATE);

        // Get all the companyRequestList where requestedDate equals to UPDATED_REQUESTED_DATE
        defaultCompanyRequestShouldNotBeFound("requestedDate.in=" + UPDATED_REQUESTED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByRequestedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where requestedDate is not null
        defaultCompanyRequestShouldBeFound("requestedDate.specified=true");

        // Get all the companyRequestList where requestedDate is null
        defaultCompanyRequestShouldNotBeFound("requestedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedDate equals to DEFAULT_APPROVED_DATE
        defaultCompanyRequestShouldBeFound("approvedDate.equals=" + DEFAULT_APPROVED_DATE);

        // Get all the companyRequestList where approvedDate equals to UPDATED_APPROVED_DATE
        defaultCompanyRequestShouldNotBeFound("approvedDate.equals=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedDate not equals to DEFAULT_APPROVED_DATE
        defaultCompanyRequestShouldNotBeFound("approvedDate.notEquals=" + DEFAULT_APPROVED_DATE);

        // Get all the companyRequestList where approvedDate not equals to UPDATED_APPROVED_DATE
        defaultCompanyRequestShouldBeFound("approvedDate.notEquals=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedDate in DEFAULT_APPROVED_DATE or UPDATED_APPROVED_DATE
        defaultCompanyRequestShouldBeFound("approvedDate.in=" + DEFAULT_APPROVED_DATE + "," + UPDATED_APPROVED_DATE);

        // Get all the companyRequestList where approvedDate equals to UPDATED_APPROVED_DATE
        defaultCompanyRequestShouldNotBeFound("approvedDate.in=" + UPDATED_APPROVED_DATE);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByApprovedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where approvedDate is not null
        defaultCompanyRequestShouldBeFound("approvedDate.specified=true");

        // Get all the companyRequestList where approvedDate is null
        defaultCompanyRequestShouldNotBeFound("approvedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyRequestShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyRequestList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyRequestShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultCompanyRequestShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyRequestList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyRequestShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultCompanyRequestShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the companyRequestList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultCompanyRequestShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedBy is not null
        defaultCompanyRequestShouldBeFound("lastModifiedBy.specified=true");

        // Get all the companyRequestList where lastModifiedBy is null
        defaultCompanyRequestShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultCompanyRequestShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyRequestList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultCompanyRequestShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultCompanyRequestShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the companyRequestList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultCompanyRequestShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByOriginalFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where originalFilename equals to DEFAULT_ORIGINAL_FILENAME
        defaultCompanyRequestShouldBeFound("originalFilename.equals=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the companyRequestList where originalFilename equals to UPDATED_ORIGINAL_FILENAME
        defaultCompanyRequestShouldNotBeFound("originalFilename.equals=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByOriginalFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where originalFilename not equals to DEFAULT_ORIGINAL_FILENAME
        defaultCompanyRequestShouldNotBeFound("originalFilename.notEquals=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the companyRequestList where originalFilename not equals to UPDATED_ORIGINAL_FILENAME
        defaultCompanyRequestShouldBeFound("originalFilename.notEquals=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByOriginalFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where originalFilename in DEFAULT_ORIGINAL_FILENAME or UPDATED_ORIGINAL_FILENAME
        defaultCompanyRequestShouldBeFound("originalFilename.in=" + DEFAULT_ORIGINAL_FILENAME + "," + UPDATED_ORIGINAL_FILENAME);

        // Get all the companyRequestList where originalFilename equals to UPDATED_ORIGINAL_FILENAME
        defaultCompanyRequestShouldNotBeFound("originalFilename.in=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByOriginalFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where originalFilename is not null
        defaultCompanyRequestShouldBeFound("originalFilename.specified=true");

        // Get all the companyRequestList where originalFilename is null
        defaultCompanyRequestShouldNotBeFound("originalFilename.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByOriginalFilenameContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where originalFilename contains DEFAULT_ORIGINAL_FILENAME
        defaultCompanyRequestShouldBeFound("originalFilename.contains=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the companyRequestList where originalFilename contains UPDATED_ORIGINAL_FILENAME
        defaultCompanyRequestShouldNotBeFound("originalFilename.contains=" + UPDATED_ORIGINAL_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByOriginalFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where originalFilename does not contain DEFAULT_ORIGINAL_FILENAME
        defaultCompanyRequestShouldNotBeFound("originalFilename.doesNotContain=" + DEFAULT_ORIGINAL_FILENAME);

        // Get all the companyRequestList where originalFilename does not contain UPDATED_ORIGINAL_FILENAME
        defaultCompanyRequestShouldBeFound("originalFilename.doesNotContain=" + UPDATED_ORIGINAL_FILENAME);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayFilenameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayFilename equals to DEFAULT_DISPLAY_FILENAME
        defaultCompanyRequestShouldBeFound("displayFilename.equals=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the companyRequestList where displayFilename equals to UPDATED_DISPLAY_FILENAME
        defaultCompanyRequestShouldNotBeFound("displayFilename.equals=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayFilenameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayFilename not equals to DEFAULT_DISPLAY_FILENAME
        defaultCompanyRequestShouldNotBeFound("displayFilename.notEquals=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the companyRequestList where displayFilename not equals to UPDATED_DISPLAY_FILENAME
        defaultCompanyRequestShouldBeFound("displayFilename.notEquals=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayFilenameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayFilename in DEFAULT_DISPLAY_FILENAME or UPDATED_DISPLAY_FILENAME
        defaultCompanyRequestShouldBeFound("displayFilename.in=" + DEFAULT_DISPLAY_FILENAME + "," + UPDATED_DISPLAY_FILENAME);

        // Get all the companyRequestList where displayFilename equals to UPDATED_DISPLAY_FILENAME
        defaultCompanyRequestShouldNotBeFound("displayFilename.in=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayFilenameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayFilename is not null
        defaultCompanyRequestShouldBeFound("displayFilename.specified=true");

        // Get all the companyRequestList where displayFilename is null
        defaultCompanyRequestShouldNotBeFound("displayFilename.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayFilenameContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayFilename contains DEFAULT_DISPLAY_FILENAME
        defaultCompanyRequestShouldBeFound("displayFilename.contains=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the companyRequestList where displayFilename contains UPDATED_DISPLAY_FILENAME
        defaultCompanyRequestShouldNotBeFound("displayFilename.contains=" + UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void getAllCompanyRequestsByDisplayFilenameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);

        // Get all the companyRequestList where displayFilename does not contain DEFAULT_DISPLAY_FILENAME
        defaultCompanyRequestShouldNotBeFound("displayFilename.doesNotContain=" + DEFAULT_DISPLAY_FILENAME);

        // Get all the companyRequestList where displayFilename does not contain UPDATED_DISPLAY_FILENAME
        defaultCompanyRequestShouldBeFound("displayFilename.doesNotContain=" + UPDATED_DISPLAY_FILENAME);
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyAddressMasterIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);
        CompanyAddressMaster companyAddressMaster = CompanyAddressMasterResourceIT.createEntity(em);
        em.persist(companyAddressMaster);
        em.flush();
        companyRequest.addCompanyAddressMaster(companyAddressMaster);
        companyRequestRepository.saveAndFlush(companyRequest);
        Long companyAddressMasterId = companyAddressMaster.getId();

        // Get all the companyRequestList where companyAddressMaster equals to companyAddressMasterId
        defaultCompanyRequestShouldBeFound("companyAddressMasterId.equals=" + companyAddressMasterId);

        // Get all the companyRequestList where companyAddressMaster equals to companyAddressMasterId + 1
        defaultCompanyRequestShouldNotBeFound("companyAddressMasterId.equals=" + (companyAddressMasterId + 1));
    }


    @Test
    @Transactional
    public void getAllCompanyRequestsByCompanyTanPanRocDtlsIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRequestRepository.saveAndFlush(companyRequest);
        CompanyTanPanRocDtls companyTanPanRocDtls = CompanyTanPanRocDtlsResourceIT.createEntity(em);
        em.persist(companyTanPanRocDtls);
        em.flush();
        companyRequest.addCompanyTanPanRocDtls(companyTanPanRocDtls);
        companyRequestRepository.saveAndFlush(companyRequest);
        Long companyTanPanRocDtlsId = companyTanPanRocDtls.getId();

        // Get all the companyRequestList where companyTanPanRocDtls equals to companyTanPanRocDtlsId
        defaultCompanyRequestShouldBeFound("companyTanPanRocDtlsId.equals=" + companyTanPanRocDtlsId);

        // Get all the companyRequestList where companyTanPanRocDtls equals to companyTanPanRocDtlsId + 1
        defaultCompanyRequestShouldNotBeFound("companyTanPanRocDtlsId.equals=" + (companyTanPanRocDtlsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyRequestShouldBeFound(String filter) throws Exception {
        restCompanyRequestMockMvc.perform(get("/api/company-requests?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(companyRequest.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyName").value(hasItem(DEFAULT_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].companyTypeId").value(hasItem(DEFAULT_COMPANY_TYPE_ID)))
            .andExpect(jsonPath("$.[*].sectorId").value(hasItem(DEFAULT_SECTOR_ID)))
            .andExpect(jsonPath("$.[*].industryId").value(hasItem(DEFAULT_INDUSTRY_ID)))
            .andExpect(jsonPath("$.[*].businessAreaId").value(hasItem(DEFAULT_BUSINESS_AREA_ID)))
            .andExpect(jsonPath("$.[*].clientGroupName").value(hasItem(DEFAULT_CLIENT_GROUP_NAME)))
            .andExpect(jsonPath("$.[*].remarks").value(hasItem(DEFAULT_REMARKS)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].companyCode").value(hasItem(DEFAULT_COMPANY_CODE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].billingType").value(hasItem(DEFAULT_BILLING_TYPE)))
            .andExpect(jsonPath("$.[*].displayCompanyName").value(hasItem(DEFAULT_DISPLAY_COMPANY_NAME)))
            .andExpect(jsonPath("$.[*].requestedBy").value(hasItem(DEFAULT_REQUESTED_BY)))
            .andExpect(jsonPath("$.[*].approvedBy").value(hasItem(DEFAULT_APPROVED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].requestedDate").value(hasItem(DEFAULT_REQUESTED_DATE.toString())))
            .andExpect(jsonPath("$.[*].approvedDate").value(hasItem(DEFAULT_APPROVED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].originalFilename").value(hasItem(DEFAULT_ORIGINAL_FILENAME)))
            .andExpect(jsonPath("$.[*].displayFilename").value(hasItem(DEFAULT_DISPLAY_FILENAME)));

        // Check, that the count call also returns 1
        restCompanyRequestMockMvc.perform(get("/api/company-requests/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyRequestShouldNotBeFound(String filter) throws Exception {
        restCompanyRequestMockMvc.perform(get("/api/company-requests?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyRequestMockMvc.perform(get("/api/company-requests/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingCompanyRequest() throws Exception {
        // Get the companyRequest
        restCompanyRequestMockMvc.perform(get("/api/company-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompanyRequest() throws Exception {
        // Initialize the database
        companyRequestService.save(companyRequest);

        int databaseSizeBeforeUpdate = companyRequestRepository.findAll().size();

        // Update the companyRequest
        CompanyRequest updatedCompanyRequest = companyRequestRepository.findById(companyRequest.getId()).get();
        // Disconnect from session so that the updates on updatedCompanyRequest are not directly saved in db
        em.detach(updatedCompanyRequest);
        updatedCompanyRequest
            .companyName(UPDATED_COMPANY_NAME)
            .companyTypeId(UPDATED_COMPANY_TYPE_ID)
            .sectorId(UPDATED_SECTOR_ID)
            .industryId(UPDATED_INDUSTRY_ID)
            .businessAreaId(UPDATED_BUSINESS_AREA_ID)
            .clientGroupName(UPDATED_CLIENT_GROUP_NAME)
            .remarks(UPDATED_REMARKS)
            .recordId(UPDATED_RECORD_ID)
            .companyCode(UPDATED_COMPANY_CODE)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .isDeleted(UPDATED_IS_DELETED)
            .billingType(UPDATED_BILLING_TYPE)
            .displayCompanyName(UPDATED_DISPLAY_COMPANY_NAME)
            .requestedBy(UPDATED_REQUESTED_BY)
            .approvedBy(UPDATED_APPROVED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .requestedDate(UPDATED_REQUESTED_DATE)
            .approvedDate(UPDATED_APPROVED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .originalFilename(UPDATED_ORIGINAL_FILENAME)
            .displayFilename(UPDATED_DISPLAY_FILENAME);

        restCompanyRequestMockMvc.perform(put("/api/company-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompanyRequest)))
            .andExpect(status().isOk());

        // Validate the CompanyRequest in the database
        List<CompanyRequest> companyRequestList = companyRequestRepository.findAll();
        assertThat(companyRequestList).hasSize(databaseSizeBeforeUpdate);
        CompanyRequest testCompanyRequest = companyRequestList.get(companyRequestList.size() - 1);
        assertThat(testCompanyRequest.getCompanyName()).isEqualTo(UPDATED_COMPANY_NAME);
        assertThat(testCompanyRequest.getCompanyTypeId()).isEqualTo(UPDATED_COMPANY_TYPE_ID);
        assertThat(testCompanyRequest.getSectorId()).isEqualTo(UPDATED_SECTOR_ID);
        assertThat(testCompanyRequest.getIndustryId()).isEqualTo(UPDATED_INDUSTRY_ID);
        assertThat(testCompanyRequest.getBusinessAreaId()).isEqualTo(UPDATED_BUSINESS_AREA_ID);
        assertThat(testCompanyRequest.getClientGroupName()).isEqualTo(UPDATED_CLIENT_GROUP_NAME);
        assertThat(testCompanyRequest.getRemarks()).isEqualTo(UPDATED_REMARKS);
        assertThat(testCompanyRequest.getRecordId()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testCompanyRequest.getCompanyCode()).isEqualTo(UPDATED_COMPANY_CODE);
        assertThat(testCompanyRequest.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCompanyRequest.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testCompanyRequest.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testCompanyRequest.getBillingType()).isEqualTo(UPDATED_BILLING_TYPE);
        assertThat(testCompanyRequest.getDisplayCompanyName()).isEqualTo(UPDATED_DISPLAY_COMPANY_NAME);
        assertThat(testCompanyRequest.getRequestedBy()).isEqualTo(UPDATED_REQUESTED_BY);
        assertThat(testCompanyRequest.getApprovedBy()).isEqualTo(UPDATED_APPROVED_BY);
        assertThat(testCompanyRequest.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCompanyRequest.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCompanyRequest.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testCompanyRequest.getRequestedDate()).isEqualTo(UPDATED_REQUESTED_DATE);
        assertThat(testCompanyRequest.getApprovedDate()).isEqualTo(UPDATED_APPROVED_DATE);
        assertThat(testCompanyRequest.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testCompanyRequest.getOriginalFilename()).isEqualTo(UPDATED_ORIGINAL_FILENAME);
        assertThat(testCompanyRequest.getDisplayFilename()).isEqualTo(UPDATED_DISPLAY_FILENAME);
    }

    @Test
    @Transactional
    public void updateNonExistingCompanyRequest() throws Exception {
        int databaseSizeBeforeUpdate = companyRequestRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyRequestMockMvc.perform(put("/api/company-requests")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyRequest)))
            .andExpect(status().isBadRequest());

        // Validate the CompanyRequest in the database
        List<CompanyRequest> companyRequestList = companyRequestRepository.findAll();
        assertThat(companyRequestList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCompanyRequest() throws Exception {
        // Initialize the database
        companyRequestService.save(companyRequest);

        int databaseSizeBeforeDelete = companyRequestRepository.findAll().size();

        // Delete the companyRequest
        restCompanyRequestMockMvc.perform(delete("/api/company-requests/{id}", companyRequest.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompanyRequest> companyRequestList = companyRequestRepository.findAll();
        assertThat(companyRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
