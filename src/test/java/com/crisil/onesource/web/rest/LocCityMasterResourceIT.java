package com.crisil.onesource.web.rest;

import com.crisil.onesource.OneSourceApp;
import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.domain.LocCityPincodeMapping;
import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.repository.LocCityMasterRepository;
import com.crisil.onesource.service.LocCityMasterService;
import com.crisil.onesource.service.dto.LocCityMasterCriteria;
import com.crisil.onesource.service.LocCityMasterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LocCityMasterResource} REST controller.
 */
@SpringBootTest(classes = OneSourceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LocCityMasterResourceIT {

    private static final String DEFAULT_CITY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CITY_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_CITY_ID = 1;
    private static final Integer UPDATED_CITY_ID = 2;
    private static final Integer SMALLER_CITY_ID = 1 - 1;

    private static final String DEFAULT_STATE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STATE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PIN_NUM = "AAAAAAAAAA";
    private static final String UPDATED_PIN_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_IS_DELETED = "A";
    private static final String UPDATED_IS_DELETED = "B";

    private static final String DEFAULT_RECORD_ID = "AAAAAAAAAA";
    private static final String UPDATED_RECORD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_CODE = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Integer DEFAULT_SRNO = 1;
    private static final Integer UPDATED_SRNO = 2;
    private static final Integer SMALLER_SRNO = 1 - 1;

    private static final String DEFAULT_IS_APPROVED = "A";
    private static final String UPDATED_IS_APPROVED = "B";

    private static final Integer DEFAULT_SEQ_ORDER = 1;
    private static final Integer UPDATED_SEQ_ORDER = 2;
    private static final Integer SMALLER_SEQ_ORDER = 1 - 1;

    @Autowired
    private LocCityMasterRepository locCityMasterRepository;

    @Autowired
    private LocCityMasterService locCityMasterService;

    @Autowired
    private LocCityMasterQueryService locCityMasterQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restLocCityMasterMockMvc;

    private LocCityMaster locCityMaster;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocCityMaster createEntity(EntityManager em) {
        LocCityMaster locCityMaster = new LocCityMaster()
            .cityName(DEFAULT_CITY_NAME)
            .cityId(DEFAULT_CITY_ID)
            .stateName(DEFAULT_STATE_NAME)
            .countryName(DEFAULT_COUNTRY_NAME)
            .pinNum(DEFAULT_PIN_NUM)
            .isDeleted(DEFAULT_IS_DELETED)
            .recordId(DEFAULT_RECORD_ID)
            .countryCode(DEFAULT_COUNTRY_CODE)
            .createdDate(DEFAULT_CREATED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .srno(DEFAULT_SRNO)
            .isApproved(DEFAULT_IS_APPROVED)
            .seqOrder(DEFAULT_SEQ_ORDER);
        return locCityMaster;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LocCityMaster createUpdatedEntity(EntityManager em) {
        LocCityMaster locCityMaster = new LocCityMaster()
            .cityName(UPDATED_CITY_NAME)
            .cityId(UPDATED_CITY_ID)
            .stateName(UPDATED_STATE_NAME)
            .countryName(UPDATED_COUNTRY_NAME)
            .pinNum(UPDATED_PIN_NUM)
            .isDeleted(UPDATED_IS_DELETED)
            .recordId(UPDATED_RECORD_ID)
            .countryCode(UPDATED_COUNTRY_CODE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .srno(UPDATED_SRNO)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);
        return locCityMaster;
    }

    @BeforeEach
    public void initTest() {
        locCityMaster = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocCityMaster() throws Exception {
        int databaseSizeBeforeCreate = locCityMasterRepository.findAll().size();
        // Create the LocCityMaster
        restLocCityMasterMockMvc.perform(post("/api/loc-city-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityMaster)))
            .andExpect(status().isCreated());

        // Validate the LocCityMaster in the database
        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeCreate + 1);
        LocCityMaster testLocCityMaster = locCityMasterList.get(locCityMasterList.size() - 1);
        assertThat(testLocCityMaster.getCityName()).isEqualTo(DEFAULT_CITY_NAME);
        assertThat(testLocCityMaster.getCityId()).isEqualTo(DEFAULT_CITY_ID);
        assertThat(testLocCityMaster.getStateName()).isEqualTo(DEFAULT_STATE_NAME);
        assertThat(testLocCityMaster.getCountryName()).isEqualTo(DEFAULT_COUNTRY_NAME);
        assertThat(testLocCityMaster.getPinNum()).isEqualTo(DEFAULT_PIN_NUM);
        assertThat(testLocCityMaster.getIsDeleted()).isEqualTo(DEFAULT_IS_DELETED);
        assertThat(testLocCityMaster.getRecordId()).isEqualTo(DEFAULT_RECORD_ID);
        assertThat(testLocCityMaster.getCountryCode()).isEqualTo(DEFAULT_COUNTRY_CODE);
        assertThat(testLocCityMaster.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testLocCityMaster.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testLocCityMaster.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testLocCityMaster.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testLocCityMaster.getSrno()).isEqualTo(DEFAULT_SRNO);
        assertThat(testLocCityMaster.getIsApproved()).isEqualTo(DEFAULT_IS_APPROVED);
        assertThat(testLocCityMaster.getSeqOrder()).isEqualTo(DEFAULT_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void createLocCityMasterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locCityMasterRepository.findAll().size();

        // Create the LocCityMaster with an existing ID
        locCityMaster.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocCityMasterMockMvc.perform(post("/api/loc-city-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocCityMaster in the database
        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCityNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = locCityMasterRepository.findAll().size();
        // set the field null
        locCityMaster.setCityName(null);

        // Create the LocCityMaster, which fails.


        restLocCityMasterMockMvc.perform(post("/api/loc-city-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityMaster)))
            .andExpect(status().isBadRequest());

        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCityIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = locCityMasterRepository.findAll().size();
        // set the field null
        locCityMaster.setCityId(null);

        // Create the LocCityMaster, which fails.


        restLocCityMasterMockMvc.perform(post("/api/loc-city-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityMaster)))
            .andExpect(status().isBadRequest());

        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocCityMasters() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locCityMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityName").value(hasItem(DEFAULT_CITY_NAME)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].countryName").value(hasItem(DEFAULT_COUNTRY_NAME)))
            .andExpect(jsonPath("$.[*].pinNum").value(hasItem(DEFAULT_PIN_NUM)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].srno").value(hasItem(DEFAULT_SRNO)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));
    }
    
    @Test
    @Transactional
    public void getLocCityMaster() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get the locCityMaster
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters/{id}", locCityMaster.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(locCityMaster.getId().intValue()))
            .andExpect(jsonPath("$.cityName").value(DEFAULT_CITY_NAME))
            .andExpect(jsonPath("$.cityId").value(DEFAULT_CITY_ID))
            .andExpect(jsonPath("$.stateName").value(DEFAULT_STATE_NAME))
            .andExpect(jsonPath("$.countryName").value(DEFAULT_COUNTRY_NAME))
            .andExpect(jsonPath("$.pinNum").value(DEFAULT_PIN_NUM))
            .andExpect(jsonPath("$.isDeleted").value(DEFAULT_IS_DELETED))
            .andExpect(jsonPath("$.recordId").value(DEFAULT_RECORD_ID))
            .andExpect(jsonPath("$.countryCode").value(DEFAULT_COUNTRY_CODE))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.srno").value(DEFAULT_SRNO))
            .andExpect(jsonPath("$.isApproved").value(DEFAULT_IS_APPROVED))
            .andExpect(jsonPath("$.seqOrder").value(DEFAULT_SEQ_ORDER));
    }


    @Test
    @Transactional
    public void getLocCityMastersByIdFiltering() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        Long id = locCityMaster.getId();

        defaultLocCityMasterShouldBeFound("id.equals=" + id);
        defaultLocCityMasterShouldNotBeFound("id.notEquals=" + id);

        defaultLocCityMasterShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultLocCityMasterShouldNotBeFound("id.greaterThan=" + id);

        defaultLocCityMasterShouldBeFound("id.lessThanOrEqual=" + id);
        defaultLocCityMasterShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByCityNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityName equals to DEFAULT_CITY_NAME
        defaultLocCityMasterShouldBeFound("cityName.equals=" + DEFAULT_CITY_NAME);

        // Get all the locCityMasterList where cityName equals to UPDATED_CITY_NAME
        defaultLocCityMasterShouldNotBeFound("cityName.equals=" + UPDATED_CITY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityName not equals to DEFAULT_CITY_NAME
        defaultLocCityMasterShouldNotBeFound("cityName.notEquals=" + DEFAULT_CITY_NAME);

        // Get all the locCityMasterList where cityName not equals to UPDATED_CITY_NAME
        defaultLocCityMasterShouldBeFound("cityName.notEquals=" + UPDATED_CITY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityNameIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityName in DEFAULT_CITY_NAME or UPDATED_CITY_NAME
        defaultLocCityMasterShouldBeFound("cityName.in=" + DEFAULT_CITY_NAME + "," + UPDATED_CITY_NAME);

        // Get all the locCityMasterList where cityName equals to UPDATED_CITY_NAME
        defaultLocCityMasterShouldNotBeFound("cityName.in=" + UPDATED_CITY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityName is not null
        defaultLocCityMasterShouldBeFound("cityName.specified=true");

        // Get all the locCityMasterList where cityName is null
        defaultLocCityMasterShouldNotBeFound("cityName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByCityNameContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityName contains DEFAULT_CITY_NAME
        defaultLocCityMasterShouldBeFound("cityName.contains=" + DEFAULT_CITY_NAME);

        // Get all the locCityMasterList where cityName contains UPDATED_CITY_NAME
        defaultLocCityMasterShouldNotBeFound("cityName.contains=" + UPDATED_CITY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityNameNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityName does not contain DEFAULT_CITY_NAME
        defaultLocCityMasterShouldNotBeFound("cityName.doesNotContain=" + DEFAULT_CITY_NAME);

        // Get all the locCityMasterList where cityName does not contain UPDATED_CITY_NAME
        defaultLocCityMasterShouldBeFound("cityName.doesNotContain=" + UPDATED_CITY_NAME);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId equals to DEFAULT_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.equals=" + DEFAULT_CITY_ID);

        // Get all the locCityMasterList where cityId equals to UPDATED_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.equals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId not equals to DEFAULT_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.notEquals=" + DEFAULT_CITY_ID);

        // Get all the locCityMasterList where cityId not equals to UPDATED_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.notEquals=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId in DEFAULT_CITY_ID or UPDATED_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.in=" + DEFAULT_CITY_ID + "," + UPDATED_CITY_ID);

        // Get all the locCityMasterList where cityId equals to UPDATED_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.in=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId is not null
        defaultLocCityMasterShouldBeFound("cityId.specified=true");

        // Get all the locCityMasterList where cityId is null
        defaultLocCityMasterShouldNotBeFound("cityId.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId is greater than or equal to DEFAULT_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.greaterThanOrEqual=" + DEFAULT_CITY_ID);

        // Get all the locCityMasterList where cityId is greater than or equal to UPDATED_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.greaterThanOrEqual=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId is less than or equal to DEFAULT_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.lessThanOrEqual=" + DEFAULT_CITY_ID);

        // Get all the locCityMasterList where cityId is less than or equal to SMALLER_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.lessThanOrEqual=" + SMALLER_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsLessThanSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId is less than DEFAULT_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.lessThan=" + DEFAULT_CITY_ID);

        // Get all the locCityMasterList where cityId is less than UPDATED_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.lessThan=" + UPDATED_CITY_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCityIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where cityId is greater than DEFAULT_CITY_ID
        defaultLocCityMasterShouldNotBeFound("cityId.greaterThan=" + DEFAULT_CITY_ID);

        // Get all the locCityMasterList where cityId is greater than SMALLER_CITY_ID
        defaultLocCityMasterShouldBeFound("cityId.greaterThan=" + SMALLER_CITY_ID);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByStateNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where stateName equals to DEFAULT_STATE_NAME
        defaultLocCityMasterShouldBeFound("stateName.equals=" + DEFAULT_STATE_NAME);

        // Get all the locCityMasterList where stateName equals to UPDATED_STATE_NAME
        defaultLocCityMasterShouldNotBeFound("stateName.equals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByStateNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where stateName not equals to DEFAULT_STATE_NAME
        defaultLocCityMasterShouldNotBeFound("stateName.notEquals=" + DEFAULT_STATE_NAME);

        // Get all the locCityMasterList where stateName not equals to UPDATED_STATE_NAME
        defaultLocCityMasterShouldBeFound("stateName.notEquals=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByStateNameIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where stateName in DEFAULT_STATE_NAME or UPDATED_STATE_NAME
        defaultLocCityMasterShouldBeFound("stateName.in=" + DEFAULT_STATE_NAME + "," + UPDATED_STATE_NAME);

        // Get all the locCityMasterList where stateName equals to UPDATED_STATE_NAME
        defaultLocCityMasterShouldNotBeFound("stateName.in=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByStateNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where stateName is not null
        defaultLocCityMasterShouldBeFound("stateName.specified=true");

        // Get all the locCityMasterList where stateName is null
        defaultLocCityMasterShouldNotBeFound("stateName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByStateNameContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where stateName contains DEFAULT_STATE_NAME
        defaultLocCityMasterShouldBeFound("stateName.contains=" + DEFAULT_STATE_NAME);

        // Get all the locCityMasterList where stateName contains UPDATED_STATE_NAME
        defaultLocCityMasterShouldNotBeFound("stateName.contains=" + UPDATED_STATE_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByStateNameNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where stateName does not contain DEFAULT_STATE_NAME
        defaultLocCityMasterShouldNotBeFound("stateName.doesNotContain=" + DEFAULT_STATE_NAME);

        // Get all the locCityMasterList where stateName does not contain UPDATED_STATE_NAME
        defaultLocCityMasterShouldBeFound("stateName.doesNotContain=" + UPDATED_STATE_NAME);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByCountryNameIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryName equals to DEFAULT_COUNTRY_NAME
        defaultLocCityMasterShouldBeFound("countryName.equals=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCityMasterList where countryName equals to UPDATED_COUNTRY_NAME
        defaultLocCityMasterShouldNotBeFound("countryName.equals=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryName not equals to DEFAULT_COUNTRY_NAME
        defaultLocCityMasterShouldNotBeFound("countryName.notEquals=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCityMasterList where countryName not equals to UPDATED_COUNTRY_NAME
        defaultLocCityMasterShouldBeFound("countryName.notEquals=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryNameIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryName in DEFAULT_COUNTRY_NAME or UPDATED_COUNTRY_NAME
        defaultLocCityMasterShouldBeFound("countryName.in=" + DEFAULT_COUNTRY_NAME + "," + UPDATED_COUNTRY_NAME);

        // Get all the locCityMasterList where countryName equals to UPDATED_COUNTRY_NAME
        defaultLocCityMasterShouldNotBeFound("countryName.in=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryName is not null
        defaultLocCityMasterShouldBeFound("countryName.specified=true");

        // Get all the locCityMasterList where countryName is null
        defaultLocCityMasterShouldNotBeFound("countryName.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByCountryNameContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryName contains DEFAULT_COUNTRY_NAME
        defaultLocCityMasterShouldBeFound("countryName.contains=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCityMasterList where countryName contains UPDATED_COUNTRY_NAME
        defaultLocCityMasterShouldNotBeFound("countryName.contains=" + UPDATED_COUNTRY_NAME);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryNameNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryName does not contain DEFAULT_COUNTRY_NAME
        defaultLocCityMasterShouldNotBeFound("countryName.doesNotContain=" + DEFAULT_COUNTRY_NAME);

        // Get all the locCityMasterList where countryName does not contain UPDATED_COUNTRY_NAME
        defaultLocCityMasterShouldBeFound("countryName.doesNotContain=" + UPDATED_COUNTRY_NAME);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByPinNumIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where pinNum equals to DEFAULT_PIN_NUM
        defaultLocCityMasterShouldBeFound("pinNum.equals=" + DEFAULT_PIN_NUM);

        // Get all the locCityMasterList where pinNum equals to UPDATED_PIN_NUM
        defaultLocCityMasterShouldNotBeFound("pinNum.equals=" + UPDATED_PIN_NUM);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByPinNumIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where pinNum not equals to DEFAULT_PIN_NUM
        defaultLocCityMasterShouldNotBeFound("pinNum.notEquals=" + DEFAULT_PIN_NUM);

        // Get all the locCityMasterList where pinNum not equals to UPDATED_PIN_NUM
        defaultLocCityMasterShouldBeFound("pinNum.notEquals=" + UPDATED_PIN_NUM);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByPinNumIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where pinNum in DEFAULT_PIN_NUM or UPDATED_PIN_NUM
        defaultLocCityMasterShouldBeFound("pinNum.in=" + DEFAULT_PIN_NUM + "," + UPDATED_PIN_NUM);

        // Get all the locCityMasterList where pinNum equals to UPDATED_PIN_NUM
        defaultLocCityMasterShouldNotBeFound("pinNum.in=" + UPDATED_PIN_NUM);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByPinNumIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where pinNum is not null
        defaultLocCityMasterShouldBeFound("pinNum.specified=true");

        // Get all the locCityMasterList where pinNum is null
        defaultLocCityMasterShouldNotBeFound("pinNum.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByPinNumContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where pinNum contains DEFAULT_PIN_NUM
        defaultLocCityMasterShouldBeFound("pinNum.contains=" + DEFAULT_PIN_NUM);

        // Get all the locCityMasterList where pinNum contains UPDATED_PIN_NUM
        defaultLocCityMasterShouldNotBeFound("pinNum.contains=" + UPDATED_PIN_NUM);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByPinNumNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where pinNum does not contain DEFAULT_PIN_NUM
        defaultLocCityMasterShouldNotBeFound("pinNum.doesNotContain=" + DEFAULT_PIN_NUM);

        // Get all the locCityMasterList where pinNum does not contain UPDATED_PIN_NUM
        defaultLocCityMasterShouldBeFound("pinNum.doesNotContain=" + UPDATED_PIN_NUM);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByIsDeletedIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isDeleted equals to DEFAULT_IS_DELETED
        defaultLocCityMasterShouldBeFound("isDeleted.equals=" + DEFAULT_IS_DELETED);

        // Get all the locCityMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocCityMasterShouldNotBeFound("isDeleted.equals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsDeletedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isDeleted not equals to DEFAULT_IS_DELETED
        defaultLocCityMasterShouldNotBeFound("isDeleted.notEquals=" + DEFAULT_IS_DELETED);

        // Get all the locCityMasterList where isDeleted not equals to UPDATED_IS_DELETED
        defaultLocCityMasterShouldBeFound("isDeleted.notEquals=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsDeletedIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isDeleted in DEFAULT_IS_DELETED or UPDATED_IS_DELETED
        defaultLocCityMasterShouldBeFound("isDeleted.in=" + DEFAULT_IS_DELETED + "," + UPDATED_IS_DELETED);

        // Get all the locCityMasterList where isDeleted equals to UPDATED_IS_DELETED
        defaultLocCityMasterShouldNotBeFound("isDeleted.in=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsDeletedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isDeleted is not null
        defaultLocCityMasterShouldBeFound("isDeleted.specified=true");

        // Get all the locCityMasterList where isDeleted is null
        defaultLocCityMasterShouldNotBeFound("isDeleted.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByIsDeletedContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isDeleted contains DEFAULT_IS_DELETED
        defaultLocCityMasterShouldBeFound("isDeleted.contains=" + DEFAULT_IS_DELETED);

        // Get all the locCityMasterList where isDeleted contains UPDATED_IS_DELETED
        defaultLocCityMasterShouldNotBeFound("isDeleted.contains=" + UPDATED_IS_DELETED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsDeletedNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isDeleted does not contain DEFAULT_IS_DELETED
        defaultLocCityMasterShouldNotBeFound("isDeleted.doesNotContain=" + DEFAULT_IS_DELETED);

        // Get all the locCityMasterList where isDeleted does not contain UPDATED_IS_DELETED
        defaultLocCityMasterShouldBeFound("isDeleted.doesNotContain=" + UPDATED_IS_DELETED);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByRecordIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where recordId equals to DEFAULT_RECORD_ID
        defaultLocCityMasterShouldBeFound("recordId.equals=" + DEFAULT_RECORD_ID);

        // Get all the locCityMasterList where recordId equals to UPDATED_RECORD_ID
        defaultLocCityMasterShouldNotBeFound("recordId.equals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByRecordIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where recordId not equals to DEFAULT_RECORD_ID
        defaultLocCityMasterShouldNotBeFound("recordId.notEquals=" + DEFAULT_RECORD_ID);

        // Get all the locCityMasterList where recordId not equals to UPDATED_RECORD_ID
        defaultLocCityMasterShouldBeFound("recordId.notEquals=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByRecordIdIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where recordId in DEFAULT_RECORD_ID or UPDATED_RECORD_ID
        defaultLocCityMasterShouldBeFound("recordId.in=" + DEFAULT_RECORD_ID + "," + UPDATED_RECORD_ID);

        // Get all the locCityMasterList where recordId equals to UPDATED_RECORD_ID
        defaultLocCityMasterShouldNotBeFound("recordId.in=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByRecordIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where recordId is not null
        defaultLocCityMasterShouldBeFound("recordId.specified=true");

        // Get all the locCityMasterList where recordId is null
        defaultLocCityMasterShouldNotBeFound("recordId.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByRecordIdContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where recordId contains DEFAULT_RECORD_ID
        defaultLocCityMasterShouldBeFound("recordId.contains=" + DEFAULT_RECORD_ID);

        // Get all the locCityMasterList where recordId contains UPDATED_RECORD_ID
        defaultLocCityMasterShouldNotBeFound("recordId.contains=" + UPDATED_RECORD_ID);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByRecordIdNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where recordId does not contain DEFAULT_RECORD_ID
        defaultLocCityMasterShouldNotBeFound("recordId.doesNotContain=" + DEFAULT_RECORD_ID);

        // Get all the locCityMasterList where recordId does not contain UPDATED_RECORD_ID
        defaultLocCityMasterShouldBeFound("recordId.doesNotContain=" + UPDATED_RECORD_ID);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByCountryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryCode equals to DEFAULT_COUNTRY_CODE
        defaultLocCityMasterShouldBeFound("countryCode.equals=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCityMasterList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultLocCityMasterShouldNotBeFound("countryCode.equals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryCode not equals to DEFAULT_COUNTRY_CODE
        defaultLocCityMasterShouldNotBeFound("countryCode.notEquals=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCityMasterList where countryCode not equals to UPDATED_COUNTRY_CODE
        defaultLocCityMasterShouldBeFound("countryCode.notEquals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryCode in DEFAULT_COUNTRY_CODE or UPDATED_COUNTRY_CODE
        defaultLocCityMasterShouldBeFound("countryCode.in=" + DEFAULT_COUNTRY_CODE + "," + UPDATED_COUNTRY_CODE);

        // Get all the locCityMasterList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultLocCityMasterShouldNotBeFound("countryCode.in=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryCode is not null
        defaultLocCityMasterShouldBeFound("countryCode.specified=true");

        // Get all the locCityMasterList where countryCode is null
        defaultLocCityMasterShouldNotBeFound("countryCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByCountryCodeContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryCode contains DEFAULT_COUNTRY_CODE
        defaultLocCityMasterShouldBeFound("countryCode.contains=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCityMasterList where countryCode contains UPDATED_COUNTRY_CODE
        defaultLocCityMasterShouldNotBeFound("countryCode.contains=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCountryCodeNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where countryCode does not contain DEFAULT_COUNTRY_CODE
        defaultLocCityMasterShouldNotBeFound("countryCode.doesNotContain=" + DEFAULT_COUNTRY_CODE);

        // Get all the locCityMasterList where countryCode does not contain UPDATED_COUNTRY_CODE
        defaultLocCityMasterShouldBeFound("countryCode.doesNotContain=" + UPDATED_COUNTRY_CODE);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdDate equals to DEFAULT_CREATED_DATE
        defaultLocCityMasterShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the locCityMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocCityMasterShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultLocCityMasterShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the locCityMasterList where createdDate not equals to UPDATED_CREATED_DATE
        defaultLocCityMasterShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultLocCityMasterShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the locCityMasterList where createdDate equals to UPDATED_CREATED_DATE
        defaultLocCityMasterShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdDate is not null
        defaultLocCityMasterShouldBeFound("createdDate.specified=true");

        // Get all the locCityMasterList where createdDate is null
        defaultLocCityMasterShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdBy equals to DEFAULT_CREATED_BY
        defaultLocCityMasterShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the locCityMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocCityMasterShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdBy not equals to DEFAULT_CREATED_BY
        defaultLocCityMasterShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the locCityMasterList where createdBy not equals to UPDATED_CREATED_BY
        defaultLocCityMasterShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultLocCityMasterShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the locCityMasterList where createdBy equals to UPDATED_CREATED_BY
        defaultLocCityMasterShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdBy is not null
        defaultLocCityMasterShouldBeFound("createdBy.specified=true");

        // Get all the locCityMasterList where createdBy is null
        defaultLocCityMasterShouldNotBeFound("createdBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdBy contains DEFAULT_CREATED_BY
        defaultLocCityMasterShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the locCityMasterList where createdBy contains UPDATED_CREATED_BY
        defaultLocCityMasterShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where createdBy does not contain DEFAULT_CREATED_BY
        defaultLocCityMasterShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the locCityMasterList where createdBy does not contain UPDATED_CREATED_BY
        defaultLocCityMasterShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocCityMasterShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locCityMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCityMasterShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultLocCityMasterShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the locCityMasterList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCityMasterShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultLocCityMasterShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the locCityMasterList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultLocCityMasterShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedDate is not null
        defaultLocCityMasterShouldBeFound("lastModifiedDate.specified=true");

        // Get all the locCityMasterList where lastModifiedDate is null
        defaultLocCityMasterShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocCityMasterShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCityMasterShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultLocCityMasterShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityMasterList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCityMasterShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultLocCityMasterShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the locCityMasterList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultLocCityMasterShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedBy is not null
        defaultLocCityMasterShouldBeFound("lastModifiedBy.specified=true");

        // Get all the locCityMasterList where lastModifiedBy is null
        defaultLocCityMasterShouldNotBeFound("lastModifiedBy.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultLocCityMasterShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityMasterList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultLocCityMasterShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultLocCityMasterShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the locCityMasterList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultLocCityMasterShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno equals to DEFAULT_SRNO
        defaultLocCityMasterShouldBeFound("srno.equals=" + DEFAULT_SRNO);

        // Get all the locCityMasterList where srno equals to UPDATED_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.equals=" + UPDATED_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno not equals to DEFAULT_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.notEquals=" + DEFAULT_SRNO);

        // Get all the locCityMasterList where srno not equals to UPDATED_SRNO
        defaultLocCityMasterShouldBeFound("srno.notEquals=" + UPDATED_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno in DEFAULT_SRNO or UPDATED_SRNO
        defaultLocCityMasterShouldBeFound("srno.in=" + DEFAULT_SRNO + "," + UPDATED_SRNO);

        // Get all the locCityMasterList where srno equals to UPDATED_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.in=" + UPDATED_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno is not null
        defaultLocCityMasterShouldBeFound("srno.specified=true");

        // Get all the locCityMasterList where srno is null
        defaultLocCityMasterShouldNotBeFound("srno.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno is greater than or equal to DEFAULT_SRNO
        defaultLocCityMasterShouldBeFound("srno.greaterThanOrEqual=" + DEFAULT_SRNO);

        // Get all the locCityMasterList where srno is greater than or equal to UPDATED_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.greaterThanOrEqual=" + UPDATED_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno is less than or equal to DEFAULT_SRNO
        defaultLocCityMasterShouldBeFound("srno.lessThanOrEqual=" + DEFAULT_SRNO);

        // Get all the locCityMasterList where srno is less than or equal to SMALLER_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.lessThanOrEqual=" + SMALLER_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsLessThanSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno is less than DEFAULT_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.lessThan=" + DEFAULT_SRNO);

        // Get all the locCityMasterList where srno is less than UPDATED_SRNO
        defaultLocCityMasterShouldBeFound("srno.lessThan=" + UPDATED_SRNO);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySrnoIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where srno is greater than DEFAULT_SRNO
        defaultLocCityMasterShouldNotBeFound("srno.greaterThan=" + DEFAULT_SRNO);

        // Get all the locCityMasterList where srno is greater than SMALLER_SRNO
        defaultLocCityMasterShouldBeFound("srno.greaterThan=" + SMALLER_SRNO);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByIsApprovedIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isApproved equals to DEFAULT_IS_APPROVED
        defaultLocCityMasterShouldBeFound("isApproved.equals=" + DEFAULT_IS_APPROVED);

        // Get all the locCityMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocCityMasterShouldNotBeFound("isApproved.equals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsApprovedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isApproved not equals to DEFAULT_IS_APPROVED
        defaultLocCityMasterShouldNotBeFound("isApproved.notEquals=" + DEFAULT_IS_APPROVED);

        // Get all the locCityMasterList where isApproved not equals to UPDATED_IS_APPROVED
        defaultLocCityMasterShouldBeFound("isApproved.notEquals=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsApprovedIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isApproved in DEFAULT_IS_APPROVED or UPDATED_IS_APPROVED
        defaultLocCityMasterShouldBeFound("isApproved.in=" + DEFAULT_IS_APPROVED + "," + UPDATED_IS_APPROVED);

        // Get all the locCityMasterList where isApproved equals to UPDATED_IS_APPROVED
        defaultLocCityMasterShouldNotBeFound("isApproved.in=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsApprovedIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isApproved is not null
        defaultLocCityMasterShouldBeFound("isApproved.specified=true");

        // Get all the locCityMasterList where isApproved is null
        defaultLocCityMasterShouldNotBeFound("isApproved.specified=false");
    }
                @Test
    @Transactional
    public void getAllLocCityMastersByIsApprovedContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isApproved contains DEFAULT_IS_APPROVED
        defaultLocCityMasterShouldBeFound("isApproved.contains=" + DEFAULT_IS_APPROVED);

        // Get all the locCityMasterList where isApproved contains UPDATED_IS_APPROVED
        defaultLocCityMasterShouldNotBeFound("isApproved.contains=" + UPDATED_IS_APPROVED);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersByIsApprovedNotContainsSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where isApproved does not contain DEFAULT_IS_APPROVED
        defaultLocCityMasterShouldNotBeFound("isApproved.doesNotContain=" + DEFAULT_IS_APPROVED);

        // Get all the locCityMasterList where isApproved does not contain UPDATED_IS_APPROVED
        defaultLocCityMasterShouldBeFound("isApproved.doesNotContain=" + UPDATED_IS_APPROVED);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder equals to DEFAULT_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.equals=" + DEFAULT_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.equals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsNotEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder not equals to DEFAULT_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.notEquals=" + DEFAULT_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder not equals to UPDATED_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.notEquals=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsInShouldWork() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder in DEFAULT_SEQ_ORDER or UPDATED_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.in=" + DEFAULT_SEQ_ORDER + "," + UPDATED_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder equals to UPDATED_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.in=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder is not null
        defaultLocCityMasterShouldBeFound("seqOrder.specified=true");

        // Get all the locCityMasterList where seqOrder is null
        defaultLocCityMasterShouldNotBeFound("seqOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder is greater than or equal to DEFAULT_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.greaterThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder is greater than or equal to UPDATED_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.greaterThanOrEqual=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder is less than or equal to DEFAULT_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.lessThanOrEqual=" + DEFAULT_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder is less than or equal to SMALLER_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.lessThanOrEqual=" + SMALLER_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder is less than DEFAULT_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.lessThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder is less than UPDATED_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.lessThan=" + UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void getAllLocCityMastersBySeqOrderIsGreaterThanSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);

        // Get all the locCityMasterList where seqOrder is greater than DEFAULT_SEQ_ORDER
        defaultLocCityMasterShouldNotBeFound("seqOrder.greaterThan=" + DEFAULT_SEQ_ORDER);

        // Get all the locCityMasterList where seqOrder is greater than SMALLER_SEQ_ORDER
        defaultLocCityMasterShouldBeFound("seqOrder.greaterThan=" + SMALLER_SEQ_ORDER);
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByLocCityPincodeMappingIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);
        LocCityPincodeMapping locCityPincodeMapping = LocCityPincodeMappingResourceIT.createEntity(em);
        em.persist(locCityPincodeMapping);
        em.flush();
        locCityMaster.addLocCityPincodeMapping(locCityPincodeMapping);
        locCityMasterRepository.saveAndFlush(locCityMaster);
        Long locCityPincodeMappingId = locCityPincodeMapping.getId();

        // Get all the locCityMasterList where locCityPincodeMapping equals to locCityPincodeMappingId
        defaultLocCityMasterShouldBeFound("locCityPincodeMappingId.equals=" + locCityPincodeMappingId);

        // Get all the locCityMasterList where locCityPincodeMapping equals to locCityPincodeMappingId + 1
        defaultLocCityMasterShouldNotBeFound("locCityPincodeMappingId.equals=" + (locCityPincodeMappingId + 1));
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByStateIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);
        LocStateMaster stateId = LocStateMasterResourceIT.createEntity(em);
        em.persist(stateId);
        em.flush();
        locCityMaster.setStateId(stateId);
        locCityMasterRepository.saveAndFlush(locCityMaster);
        Long stateIdId = stateId.getId();

        // Get all the locCityMasterList where stateId equals to stateIdId
        defaultLocCityMasterShouldBeFound("stateIdId.equals=" + stateIdId);

        // Get all the locCityMasterList where stateId equals to stateIdId + 1
        defaultLocCityMasterShouldNotBeFound("stateIdId.equals=" + (stateIdId + 1));
    }


    @Test
    @Transactional
    public void getAllLocCityMastersByDistrictIdIsEqualToSomething() throws Exception {
        // Initialize the database
        locCityMasterRepository.saveAndFlush(locCityMaster);
        LocDistrictMaster districtId = LocDistrictMasterResourceIT.createEntity(em);
        em.persist(districtId);
        em.flush();
        locCityMaster.setDistrictId(districtId);
        locCityMasterRepository.saveAndFlush(locCityMaster);
        Long districtIdId = districtId.getId();

        // Get all the locCityMasterList where districtId equals to districtIdId
        defaultLocCityMasterShouldBeFound("districtIdId.equals=" + districtIdId);

        // Get all the locCityMasterList where districtId equals to districtIdId + 1
        defaultLocCityMasterShouldNotBeFound("districtIdId.equals=" + (districtIdId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultLocCityMasterShouldBeFound(String filter) throws Exception {
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(locCityMaster.getId().intValue())))
            .andExpect(jsonPath("$.[*].cityName").value(hasItem(DEFAULT_CITY_NAME)))
            .andExpect(jsonPath("$.[*].cityId").value(hasItem(DEFAULT_CITY_ID)))
            .andExpect(jsonPath("$.[*].stateName").value(hasItem(DEFAULT_STATE_NAME)))
            .andExpect(jsonPath("$.[*].countryName").value(hasItem(DEFAULT_COUNTRY_NAME)))
            .andExpect(jsonPath("$.[*].pinNum").value(hasItem(DEFAULT_PIN_NUM)))
            .andExpect(jsonPath("$.[*].isDeleted").value(hasItem(DEFAULT_IS_DELETED)))
            .andExpect(jsonPath("$.[*].recordId").value(hasItem(DEFAULT_RECORD_ID)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].srno").value(hasItem(DEFAULT_SRNO)))
            .andExpect(jsonPath("$.[*].isApproved").value(hasItem(DEFAULT_IS_APPROVED)))
            .andExpect(jsonPath("$.[*].seqOrder").value(hasItem(DEFAULT_SEQ_ORDER)));

        // Check, that the count call also returns 1
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultLocCityMasterShouldNotBeFound(String filter) throws Exception {
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingLocCityMaster() throws Exception {
        // Get the locCityMaster
        restLocCityMasterMockMvc.perform(get("/api/loc-city-masters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocCityMaster() throws Exception {
        // Initialize the database
        locCityMasterService.save(locCityMaster);

        int databaseSizeBeforeUpdate = locCityMasterRepository.findAll().size();

        // Update the locCityMaster
        LocCityMaster updatedLocCityMaster = locCityMasterRepository.findById(locCityMaster.getId()).get();
        // Disconnect from session so that the updates on updatedLocCityMaster are not directly saved in db
        em.detach(updatedLocCityMaster);
        updatedLocCityMaster
            .cityName(UPDATED_CITY_NAME)
            .cityId(UPDATED_CITY_ID)
            .stateName(UPDATED_STATE_NAME)
            .countryName(UPDATED_COUNTRY_NAME)
            .pinNum(UPDATED_PIN_NUM)
            .isDeleted(UPDATED_IS_DELETED)
            .recordId(UPDATED_RECORD_ID)
            .countryCode(UPDATED_COUNTRY_CODE)
            .createdDate(UPDATED_CREATED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .srno(UPDATED_SRNO)
            .isApproved(UPDATED_IS_APPROVED)
            .seqOrder(UPDATED_SEQ_ORDER);

        restLocCityMasterMockMvc.perform(put("/api/loc-city-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocCityMaster)))
            .andExpect(status().isOk());

        // Validate the LocCityMaster in the database
        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeUpdate);
        LocCityMaster testLocCityMaster = locCityMasterList.get(locCityMasterList.size() - 1);
        assertThat(testLocCityMaster.getCityName()).isEqualTo(UPDATED_CITY_NAME);
        assertThat(testLocCityMaster.getCityId()).isEqualTo(UPDATED_CITY_ID);
        assertThat(testLocCityMaster.getStateName()).isEqualTo(UPDATED_STATE_NAME);
        assertThat(testLocCityMaster.getCountryName()).isEqualTo(UPDATED_COUNTRY_NAME);
        assertThat(testLocCityMaster.getPinNum()).isEqualTo(UPDATED_PIN_NUM);
        assertThat(testLocCityMaster.getIsDeleted()).isEqualTo(UPDATED_IS_DELETED);
        assertThat(testLocCityMaster.getRecordId()).isEqualTo(UPDATED_RECORD_ID);
        assertThat(testLocCityMaster.getCountryCode()).isEqualTo(UPDATED_COUNTRY_CODE);
        assertThat(testLocCityMaster.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testLocCityMaster.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testLocCityMaster.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testLocCityMaster.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testLocCityMaster.getSrno()).isEqualTo(UPDATED_SRNO);
        assertThat(testLocCityMaster.getIsApproved()).isEqualTo(UPDATED_IS_APPROVED);
        assertThat(testLocCityMaster.getSeqOrder()).isEqualTo(UPDATED_SEQ_ORDER);
    }

    @Test
    @Transactional
    public void updateNonExistingLocCityMaster() throws Exception {
        int databaseSizeBeforeUpdate = locCityMasterRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLocCityMasterMockMvc.perform(put("/api/loc-city-masters")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(locCityMaster)))
            .andExpect(status().isBadRequest());

        // Validate the LocCityMaster in the database
        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocCityMaster() throws Exception {
        // Initialize the database
        locCityMasterService.save(locCityMaster);

        int databaseSizeBeforeDelete = locCityMasterRepository.findAll().size();

        // Delete the locCityMaster
        restLocCityMasterMockMvc.perform(delete("/api/loc-city-masters/{id}", locCityMaster.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<LocCityMaster> locCityMasterList = locCityMasterRepository.findAll();
        assertThat(locCityMasterList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
