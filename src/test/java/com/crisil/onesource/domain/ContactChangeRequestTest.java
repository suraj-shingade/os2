package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class ContactChangeRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactChangeRequest.class);
        ContactChangeRequest contactChangeRequest1 = new ContactChangeRequest();
        contactChangeRequest1.setId(1L);
        ContactChangeRequest contactChangeRequest2 = new ContactChangeRequest();
        contactChangeRequest2.setId(contactChangeRequest1.getId());
        assertThat(contactChangeRequest1).isEqualTo(contactChangeRequest2);
        contactChangeRequest2.setId(2L);
        assertThat(contactChangeRequest1).isNotEqualTo(contactChangeRequest2);
        contactChangeRequest1.setId(null);
        assertThat(contactChangeRequest1).isNotEqualTo(contactChangeRequest2);
    }
}
