package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocCountryMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocCountryMaster.class);
        LocCountryMaster locCountryMaster1 = new LocCountryMaster();
        locCountryMaster1.setId(1L);
        LocCountryMaster locCountryMaster2 = new LocCountryMaster();
        locCountryMaster2.setId(locCountryMaster1.getId());
        assertThat(locCountryMaster1).isEqualTo(locCountryMaster2);
        locCountryMaster2.setId(2L);
        assertThat(locCountryMaster1).isNotEqualTo(locCountryMaster2);
        locCountryMaster1.setId(null);
        assertThat(locCountryMaster1).isNotEqualTo(locCountryMaster2);
    }
}
