package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyRequest.class);
        CompanyRequest companyRequest1 = new CompanyRequest();
        companyRequest1.setId(1L);
        CompanyRequest companyRequest2 = new CompanyRequest();
        companyRequest2.setId(companyRequest1.getId());
        assertThat(companyRequest1).isEqualTo(companyRequest2);
        companyRequest2.setId(2L);
        assertThat(companyRequest1).isNotEqualTo(companyRequest2);
        companyRequest1.setId(null);
        assertThat(companyRequest1).isNotEqualTo(companyRequest2);
    }
}
