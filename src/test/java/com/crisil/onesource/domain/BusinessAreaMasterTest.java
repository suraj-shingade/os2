package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class BusinessAreaMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BusinessAreaMaster.class);
        BusinessAreaMaster businessAreaMaster1 = new BusinessAreaMaster();
        businessAreaMaster1.setId(1L);
        BusinessAreaMaster businessAreaMaster2 = new BusinessAreaMaster();
        businessAreaMaster2.setId(businessAreaMaster1.getId());
        assertThat(businessAreaMaster1).isEqualTo(businessAreaMaster2);
        businessAreaMaster2.setId(2L);
        assertThat(businessAreaMaster1).isNotEqualTo(businessAreaMaster2);
        businessAreaMaster1.setId(null);
        assertThat(businessAreaMaster1).isNotEqualTo(businessAreaMaster2);
    }
}
