package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocCityPincodeMappingTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocCityPincodeMapping.class);
        LocCityPincodeMapping locCityPincodeMapping1 = new LocCityPincodeMapping();
        locCityPincodeMapping1.setId(1L);
        LocCityPincodeMapping locCityPincodeMapping2 = new LocCityPincodeMapping();
        locCityPincodeMapping2.setId(locCityPincodeMapping1.getId());
        assertThat(locCityPincodeMapping1).isEqualTo(locCityPincodeMapping2);
        locCityPincodeMapping2.setId(2L);
        assertThat(locCityPincodeMapping1).isNotEqualTo(locCityPincodeMapping2);
        locCityPincodeMapping1.setId(null);
        assertThat(locCityPincodeMapping1).isNotEqualTo(locCityPincodeMapping2);
    }
}
