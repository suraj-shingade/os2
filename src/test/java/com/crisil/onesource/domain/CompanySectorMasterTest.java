package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanySectorMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanySectorMaster.class);
        CompanySectorMaster companySectorMaster1 = new CompanySectorMaster();
        companySectorMaster1.setId(1L);
        CompanySectorMaster companySectorMaster2 = new CompanySectorMaster();
        companySectorMaster2.setId(companySectorMaster1.getId());
        assertThat(companySectorMaster1).isEqualTo(companySectorMaster2);
        companySectorMaster2.setId(2L);
        assertThat(companySectorMaster1).isNotEqualTo(companySectorMaster2);
        companySectorMaster1.setId(null);
        assertThat(companySectorMaster1).isNotEqualTo(companySectorMaster2);
    }
}
