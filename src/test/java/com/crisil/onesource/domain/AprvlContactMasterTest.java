package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class AprvlContactMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AprvlContactMaster.class);
        AprvlContactMaster aprvlContactMaster1 = new AprvlContactMaster();
        aprvlContactMaster1.setId(1L);
        AprvlContactMaster aprvlContactMaster2 = new AprvlContactMaster();
        aprvlContactMaster2.setId(aprvlContactMaster1.getId());
        assertThat(aprvlContactMaster1).isEqualTo(aprvlContactMaster2);
        aprvlContactMaster2.setId(2L);
        assertThat(aprvlContactMaster1).isNotEqualTo(aprvlContactMaster2);
        aprvlContactMaster1.setId(null);
        assertThat(aprvlContactMaster1).isNotEqualTo(aprvlContactMaster2);
    }
}
