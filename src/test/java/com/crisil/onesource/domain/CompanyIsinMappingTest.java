package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyIsinMappingTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyIsinMapping.class);
        CompanyIsinMapping companyIsinMapping1 = new CompanyIsinMapping();
        companyIsinMapping1.setId(1L);
        CompanyIsinMapping companyIsinMapping2 = new CompanyIsinMapping();
        companyIsinMapping2.setId(companyIsinMapping1.getId());
        assertThat(companyIsinMapping1).isEqualTo(companyIsinMapping2);
        companyIsinMapping2.setId(2L);
        assertThat(companyIsinMapping1).isNotEqualTo(companyIsinMapping2);
        companyIsinMapping1.setId(null);
        assertThat(companyIsinMapping1).isNotEqualTo(companyIsinMapping2);
    }
}
