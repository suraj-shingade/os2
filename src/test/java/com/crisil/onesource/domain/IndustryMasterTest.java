package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class IndustryMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndustryMaster.class);
        IndustryMaster industryMaster1 = new IndustryMaster();
        industryMaster1.setId(1L);
        IndustryMaster industryMaster2 = new IndustryMaster();
        industryMaster2.setId(industryMaster1.getId());
        assertThat(industryMaster1).isEqualTo(industryMaster2);
        industryMaster2.setId(2L);
        assertThat(industryMaster1).isNotEqualTo(industryMaster2);
        industryMaster1.setId(null);
        assertThat(industryMaster1).isNotEqualTo(industryMaster2);
    }
}
