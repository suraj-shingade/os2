package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocPincodeMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocPincodeMaster.class);
        LocPincodeMaster locPincodeMaster1 = new LocPincodeMaster();
        locPincodeMaster1.setId(1L);
        LocPincodeMaster locPincodeMaster2 = new LocPincodeMaster();
        locPincodeMaster2.setId(locPincodeMaster1.getId());
        assertThat(locPincodeMaster1).isEqualTo(locPincodeMaster2);
        locPincodeMaster2.setId(2L);
        assertThat(locPincodeMaster1).isNotEqualTo(locPincodeMaster2);
        locPincodeMaster1.setId(null);
        assertThat(locPincodeMaster1).isNotEqualTo(locPincodeMaster2);
    }
}
