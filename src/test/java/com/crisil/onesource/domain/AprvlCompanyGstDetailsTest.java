package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class AprvlCompanyGstDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AprvlCompanyGstDetails.class);
        AprvlCompanyGstDetails aprvlCompanyGstDetails1 = new AprvlCompanyGstDetails();
        aprvlCompanyGstDetails1.setId(1L);
        AprvlCompanyGstDetails aprvlCompanyGstDetails2 = new AprvlCompanyGstDetails();
        aprvlCompanyGstDetails2.setId(aprvlCompanyGstDetails1.getId());
        assertThat(aprvlCompanyGstDetails1).isEqualTo(aprvlCompanyGstDetails2);
        aprvlCompanyGstDetails2.setId(2L);
        assertThat(aprvlCompanyGstDetails1).isNotEqualTo(aprvlCompanyGstDetails2);
        aprvlCompanyGstDetails1.setId(null);
        assertThat(aprvlCompanyGstDetails1).isNotEqualTo(aprvlCompanyGstDetails2);
    }
}
