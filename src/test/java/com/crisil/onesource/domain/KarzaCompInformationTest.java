package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class KarzaCompInformationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(KarzaCompInformation.class);
        KarzaCompInformation karzaCompInformation1 = new KarzaCompInformation();
        karzaCompInformation1.setId(1L);
        KarzaCompInformation karzaCompInformation2 = new KarzaCompInformation();
        karzaCompInformation2.setId(karzaCompInformation1.getId());
        assertThat(karzaCompInformation1).isEqualTo(karzaCompInformation2);
        karzaCompInformation2.setId(2L);
        assertThat(karzaCompInformation1).isNotEqualTo(karzaCompInformation2);
        karzaCompInformation1.setId(null);
        assertThat(karzaCompInformation1).isNotEqualTo(karzaCompInformation2);
    }
}
