package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocRegionMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocRegionMaster.class);
        LocRegionMaster locRegionMaster1 = new LocRegionMaster();
        locRegionMaster1.setId(1L);
        LocRegionMaster locRegionMaster2 = new LocRegionMaster();
        locRegionMaster2.setId(locRegionMaster1.getId());
        assertThat(locRegionMaster1).isEqualTo(locRegionMaster2);
        locRegionMaster2.setId(2L);
        assertThat(locRegionMaster1).isNotEqualTo(locRegionMaster2);
        locRegionMaster1.setId(null);
        assertThat(locRegionMaster1).isNotEqualTo(locRegionMaster2);
    }
}
