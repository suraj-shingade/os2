package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocStateMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocStateMaster.class);
        LocStateMaster locStateMaster1 = new LocStateMaster();
        locStateMaster1.setId(1L);
        LocStateMaster locStateMaster2 = new LocStateMaster();
        locStateMaster2.setId(locStateMaster1.getId());
        assertThat(locStateMaster1).isEqualTo(locStateMaster2);
        locStateMaster2.setId(2L);
        assertThat(locStateMaster1).isNotEqualTo(locStateMaster2);
        locStateMaster1.setId(null);
        assertThat(locStateMaster1).isNotEqualTo(locStateMaster2);
    }
}
