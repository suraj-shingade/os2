package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class ContactMasterHistoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMasterHistory.class);
        ContactMasterHistory contactMasterHistory1 = new ContactMasterHistory();
        contactMasterHistory1.setId(1L);
        ContactMasterHistory contactMasterHistory2 = new ContactMasterHistory();
        contactMasterHistory2.setId(contactMasterHistory1.getId());
        assertThat(contactMasterHistory1).isEqualTo(contactMasterHistory2);
        contactMasterHistory2.setId(2L);
        assertThat(contactMasterHistory1).isNotEqualTo(contactMasterHistory2);
        contactMasterHistory1.setId(null);
        assertThat(contactMasterHistory1).isNotEqualTo(contactMasterHistory2);
    }
}
