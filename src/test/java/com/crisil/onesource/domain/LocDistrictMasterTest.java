package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocDistrictMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocDistrictMaster.class);
        LocDistrictMaster locDistrictMaster1 = new LocDistrictMaster();
        locDistrictMaster1.setId(1L);
        LocDistrictMaster locDistrictMaster2 = new LocDistrictMaster();
        locDistrictMaster2.setId(locDistrictMaster1.getId());
        assertThat(locDistrictMaster1).isEqualTo(locDistrictMaster2);
        locDistrictMaster2.setId(2L);
        assertThat(locDistrictMaster1).isNotEqualTo(locDistrictMaster2);
        locDistrictMaster1.setId(null);
        assertThat(locDistrictMaster1).isNotEqualTo(locDistrictMaster2);
    }
}
