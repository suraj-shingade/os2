package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanySegmentMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanySegmentMaster.class);
        CompanySegmentMaster companySegmentMaster1 = new CompanySegmentMaster();
        companySegmentMaster1.setId(1L);
        CompanySegmentMaster companySegmentMaster2 = new CompanySegmentMaster();
        companySegmentMaster2.setId(companySegmentMaster1.getId());
        assertThat(companySegmentMaster1).isEqualTo(companySegmentMaster2);
        companySegmentMaster2.setId(2L);
        assertThat(companySegmentMaster1).isNotEqualTo(companySegmentMaster2);
        companySegmentMaster1.setId(null);
        assertThat(companySegmentMaster1).isNotEqualTo(companySegmentMaster2);
    }
}
