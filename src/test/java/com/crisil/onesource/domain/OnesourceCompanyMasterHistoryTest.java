package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class OnesourceCompanyMasterHistoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OnesourceCompanyMasterHistory.class);
        OnesourceCompanyMasterHistory onesourceCompanyMasterHistory1 = new OnesourceCompanyMasterHistory();
        onesourceCompanyMasterHistory1.setId(1L);
        OnesourceCompanyMasterHistory onesourceCompanyMasterHistory2 = new OnesourceCompanyMasterHistory();
        onesourceCompanyMasterHistory2.setId(onesourceCompanyMasterHistory1.getId());
        assertThat(onesourceCompanyMasterHistory1).isEqualTo(onesourceCompanyMasterHistory2);
        onesourceCompanyMasterHistory2.setId(2L);
        assertThat(onesourceCompanyMasterHistory1).isNotEqualTo(onesourceCompanyMasterHistory2);
        onesourceCompanyMasterHistory1.setId(null);
        assertThat(onesourceCompanyMasterHistory1).isNotEqualTo(onesourceCompanyMasterHistory2);
    }
}
