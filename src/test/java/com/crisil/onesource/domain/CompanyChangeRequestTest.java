package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyChangeRequestTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyChangeRequest.class);
        CompanyChangeRequest companyChangeRequest1 = new CompanyChangeRequest();
        companyChangeRequest1.setId(1L);
        CompanyChangeRequest companyChangeRequest2 = new CompanyChangeRequest();
        companyChangeRequest2.setId(companyChangeRequest1.getId());
        assertThat(companyChangeRequest1).isEqualTo(companyChangeRequest2);
        companyChangeRequest2.setId(2L);
        assertThat(companyChangeRequest1).isNotEqualTo(companyChangeRequest2);
        companyChangeRequest1.setId(null);
        assertThat(companyChangeRequest1).isNotEqualTo(companyChangeRequest2);
    }
}
