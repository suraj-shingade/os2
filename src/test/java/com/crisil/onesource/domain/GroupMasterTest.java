package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class GroupMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(GroupMaster.class);
        GroupMaster groupMaster1 = new GroupMaster();
        groupMaster1.setId(1L);
        GroupMaster groupMaster2 = new GroupMaster();
        groupMaster2.setId(groupMaster1.getId());
        assertThat(groupMaster1).isEqualTo(groupMaster2);
        groupMaster2.setId(2L);
        assertThat(groupMaster1).isNotEqualTo(groupMaster2);
        groupMaster1.setId(null);
        assertThat(groupMaster1).isNotEqualTo(groupMaster2);
    }
}
