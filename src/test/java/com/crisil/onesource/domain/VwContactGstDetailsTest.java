package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class VwContactGstDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VwContactGstDetails.class);
        VwContactGstDetails vwContactGstDetails1 = new VwContactGstDetails();
        vwContactGstDetails1.setId(1L);
        VwContactGstDetails vwContactGstDetails2 = new VwContactGstDetails();
        vwContactGstDetails2.setId(vwContactGstDetails1.getId());
        assertThat(vwContactGstDetails1).isEqualTo(vwContactGstDetails2);
        vwContactGstDetails2.setId(2L);
        assertThat(vwContactGstDetails1).isNotEqualTo(vwContactGstDetails2);
        vwContactGstDetails1.setId(null);
        assertThat(vwContactGstDetails1).isNotEqualTo(vwContactGstDetails2);
    }
}
