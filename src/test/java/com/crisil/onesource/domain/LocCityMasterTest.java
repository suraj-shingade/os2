package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocCityMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocCityMaster.class);
        LocCityMaster locCityMaster1 = new LocCityMaster();
        locCityMaster1.setId(1L);
        LocCityMaster locCityMaster2 = new LocCityMaster();
        locCityMaster2.setId(locCityMaster1.getId());
        assertThat(locCityMaster1).isEqualTo(locCityMaster2);
        locCityMaster2.setId(2L);
        assertThat(locCityMaster1).isNotEqualTo(locCityMaster2);
        locCityMaster1.setId(null);
        assertThat(locCityMaster1).isNotEqualTo(locCityMaster2);
    }
}
