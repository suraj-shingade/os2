package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class OnesourceCompanyMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OnesourceCompanyMaster.class);
        OnesourceCompanyMaster onesourceCompanyMaster1 = new OnesourceCompanyMaster();
        onesourceCompanyMaster1.setId(1L);
        OnesourceCompanyMaster onesourceCompanyMaster2 = new OnesourceCompanyMaster();
        onesourceCompanyMaster2.setId(onesourceCompanyMaster1.getId());
        assertThat(onesourceCompanyMaster1).isEqualTo(onesourceCompanyMaster2);
        onesourceCompanyMaster2.setId(2L);
        assertThat(onesourceCompanyMaster1).isNotEqualTo(onesourceCompanyMaster2);
        onesourceCompanyMaster1.setId(null);
        assertThat(onesourceCompanyMaster1).isNotEqualTo(onesourceCompanyMaster2);
    }
}
