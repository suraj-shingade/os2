package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class AprvlCompanyTdsDetailsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AprvlCompanyTdsDetails.class);
        AprvlCompanyTdsDetails aprvlCompanyTdsDetails1 = new AprvlCompanyTdsDetails();
        aprvlCompanyTdsDetails1.setId(1L);
        AprvlCompanyTdsDetails aprvlCompanyTdsDetails2 = new AprvlCompanyTdsDetails();
        aprvlCompanyTdsDetails2.setId(aprvlCompanyTdsDetails1.getId());
        assertThat(aprvlCompanyTdsDetails1).isEqualTo(aprvlCompanyTdsDetails2);
        aprvlCompanyTdsDetails2.setId(2L);
        assertThat(aprvlCompanyTdsDetails1).isNotEqualTo(aprvlCompanyTdsDetails2);
        aprvlCompanyTdsDetails1.setId(null);
        assertThat(aprvlCompanyTdsDetails1).isNotEqualTo(aprvlCompanyTdsDetails2);
    }
}
