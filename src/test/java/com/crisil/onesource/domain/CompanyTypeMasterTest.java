package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyTypeMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyTypeMaster.class);
        CompanyTypeMaster companyTypeMaster1 = new CompanyTypeMaster();
        companyTypeMaster1.setId(1L);
        CompanyTypeMaster companyTypeMaster2 = new CompanyTypeMaster();
        companyTypeMaster2.setId(companyTypeMaster1.getId());
        assertThat(companyTypeMaster1).isEqualTo(companyTypeMaster2);
        companyTypeMaster2.setId(2L);
        assertThat(companyTypeMaster1).isNotEqualTo(companyTypeMaster2);
        companyTypeMaster1.setId(null);
        assertThat(companyTypeMaster1).isNotEqualTo(companyTypeMaster2);
    }
}
