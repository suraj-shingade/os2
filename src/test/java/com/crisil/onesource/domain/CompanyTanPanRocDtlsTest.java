package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyTanPanRocDtlsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyTanPanRocDtls.class);
        CompanyTanPanRocDtls companyTanPanRocDtls1 = new CompanyTanPanRocDtls();
        companyTanPanRocDtls1.setId(1L);
        CompanyTanPanRocDtls companyTanPanRocDtls2 = new CompanyTanPanRocDtls();
        companyTanPanRocDtls2.setId(companyTanPanRocDtls1.getId());
        assertThat(companyTanPanRocDtls1).isEqualTo(companyTanPanRocDtls2);
        companyTanPanRocDtls2.setId(2L);
        assertThat(companyTanPanRocDtls1).isNotEqualTo(companyTanPanRocDtls2);
        companyTanPanRocDtls1.setId(null);
        assertThat(companyTanPanRocDtls1).isNotEqualTo(companyTanPanRocDtls2);
    }
}
