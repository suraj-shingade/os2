package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class ContactMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMaster.class);
        ContactMaster contactMaster1 = new ContactMaster();
        contactMaster1.setId(1L);
        ContactMaster contactMaster2 = new ContactMaster();
        contactMaster2.setId(contactMaster1.getId());
        assertThat(contactMaster1).isEqualTo(contactMaster2);
        contactMaster2.setId(2L);
        assertThat(contactMaster1).isNotEqualTo(contactMaster2);
        contactMaster1.setId(null);
        assertThat(contactMaster1).isNotEqualTo(contactMaster2);
    }
}
