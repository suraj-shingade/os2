package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompGstNotApplicableTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompGstNotApplicable.class);
        CompGstNotApplicable compGstNotApplicable1 = new CompGstNotApplicable();
        compGstNotApplicable1.setId(1L);
        CompGstNotApplicable compGstNotApplicable2 = new CompGstNotApplicable();
        compGstNotApplicable2.setId(compGstNotApplicable1.getId());
        assertThat(compGstNotApplicable1).isEqualTo(compGstNotApplicable2);
        compGstNotApplicable2.setId(2L);
        assertThat(compGstNotApplicable1).isNotEqualTo(compGstNotApplicable2);
        compGstNotApplicable1.setId(null);
        assertThat(compGstNotApplicable1).isNotEqualTo(compGstNotApplicable2);
    }
}
