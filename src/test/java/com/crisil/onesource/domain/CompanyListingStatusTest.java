package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyListingStatusTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyListingStatus.class);
        CompanyListingStatus companyListingStatus1 = new CompanyListingStatus();
        companyListingStatus1.setId(1L);
        CompanyListingStatus companyListingStatus2 = new CompanyListingStatus();
        companyListingStatus2.setId(companyListingStatus1.getId());
        assertThat(companyListingStatus1).isEqualTo(companyListingStatus2);
        companyListingStatus2.setId(2L);
        assertThat(companyListingStatus1).isNotEqualTo(companyListingStatus2);
        companyListingStatus1.setId(null);
        assertThat(companyListingStatus1).isNotEqualTo(companyListingStatus2);
    }
}
