package com.crisil.onesource.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyAddressMasterTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyAddressMaster.class);
        CompanyAddressMaster companyAddressMaster1 = new CompanyAddressMaster();
        companyAddressMaster1.setId(1L);
        CompanyAddressMaster companyAddressMaster2 = new CompanyAddressMaster();
        companyAddressMaster2.setId(companyAddressMaster1.getId());
        assertThat(companyAddressMaster1).isEqualTo(companyAddressMaster2);
        companyAddressMaster2.setId(2L);
        assertThat(companyAddressMaster1).isNotEqualTo(companyAddressMaster2);
        companyAddressMaster1.setId(null);
        assertThat(companyAddressMaster1).isNotEqualTo(companyAddressMaster2);
    }
}
