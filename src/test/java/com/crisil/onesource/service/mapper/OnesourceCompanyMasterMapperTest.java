package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OnesourceCompanyMasterMapperTest {

    private OnesourceCompanyMasterMapper onesourceCompanyMasterMapper;

    @BeforeEach
    public void setUp() {
        onesourceCompanyMasterMapper = new OnesourceCompanyMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(onesourceCompanyMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(onesourceCompanyMasterMapper.fromId(null)).isNull();
    }
}
