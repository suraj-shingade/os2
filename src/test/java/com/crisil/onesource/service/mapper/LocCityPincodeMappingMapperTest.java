package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocCityPincodeMappingMapperTest {

    private LocCityPincodeMappingMapper locCityPincodeMappingMapper;

    @BeforeEach
    public void setUp() {
        locCityPincodeMappingMapper = new LocCityPincodeMappingMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locCityPincodeMappingMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locCityPincodeMappingMapper.fromId(null)).isNull();
    }
}
