package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyRequestMapperTest {

    private CompanyRequestMapper companyRequestMapper;

    @BeforeEach
    public void setUp() {
        companyRequestMapper = new CompanyRequestMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyRequestMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyRequestMapper.fromId(null)).isNull();
    }
}
