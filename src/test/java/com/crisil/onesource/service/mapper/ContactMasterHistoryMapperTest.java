package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactMasterHistoryMapperTest {

    private ContactMasterHistoryMapper contactMasterHistoryMapper;

    @BeforeEach
    public void setUp() {
        contactMasterHistoryMapper = new ContactMasterHistoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactMasterHistoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactMasterHistoryMapper.fromId(null)).isNull();
    }
}
