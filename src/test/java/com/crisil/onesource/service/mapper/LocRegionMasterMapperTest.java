package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocRegionMasterMapperTest {

    private LocRegionMasterMapper locRegionMasterMapper;

    @BeforeEach
    public void setUp() {
        locRegionMasterMapper = new LocRegionMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locRegionMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locRegionMasterMapper.fromId(null)).isNull();
    }
}
