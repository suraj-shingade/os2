package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class VwContactGstDetailsMapperTest {

    private VwContactGstDetailsMapper vwContactGstDetailsMapper;

    @BeforeEach
    public void setUp() {
        vwContactGstDetailsMapper = new VwContactGstDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(vwContactGstDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(vwContactGstDetailsMapper.fromId(null)).isNull();
    }
}
