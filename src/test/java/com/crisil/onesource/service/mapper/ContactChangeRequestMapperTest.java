package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactChangeRequestMapperTest {

    private ContactChangeRequestMapper contactChangeRequestMapper;

    @BeforeEach
    public void setUp() {
        contactChangeRequestMapper = new ContactChangeRequestMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactChangeRequestMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactChangeRequestMapper.fromId(null)).isNull();
    }
}
