package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class GroupMasterMapperTest {

    private GroupMasterMapper groupMasterMapper;

    @BeforeEach
    public void setUp() {
        groupMasterMapper = new GroupMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(groupMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(groupMasterMapper.fromId(null)).isNull();
    }
}
