package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class KarzaCompInformationMapperTest {

    private KarzaCompInformationMapper karzaCompInformationMapper;

    @BeforeEach
    public void setUp() {
        karzaCompInformationMapper = new KarzaCompInformationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(karzaCompInformationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(karzaCompInformationMapper.fromId(null)).isNull();
    }
}
