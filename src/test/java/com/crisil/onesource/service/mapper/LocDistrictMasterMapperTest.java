package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocDistrictMasterMapperTest {

    private LocDistrictMasterMapper locDistrictMasterMapper;

    @BeforeEach
    public void setUp() {
        locDistrictMasterMapper = new LocDistrictMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locDistrictMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locDistrictMasterMapper.fromId(null)).isNull();
    }
}
