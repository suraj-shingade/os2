package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocCityMasterMapperTest {

    private LocCityMasterMapper locCityMasterMapper;

    @BeforeEach
    public void setUp() {
        locCityMasterMapper = new LocCityMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locCityMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locCityMasterMapper.fromId(null)).isNull();
    }
}
