package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AprvlCompanyGstDetailsMapperTest {

    private AprvlCompanyGstDetailsMapper aprvlCompanyGstDetailsMapper;

    @BeforeEach
    public void setUp() {
        aprvlCompanyGstDetailsMapper = new AprvlCompanyGstDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(aprvlCompanyGstDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(aprvlCompanyGstDetailsMapper.fromId(null)).isNull();
    }
}
