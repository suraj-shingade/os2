package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocStateMasterMapperTest {

    private LocStateMasterMapper locStateMasterMapper;

    @BeforeEach
    public void setUp() {
        locStateMasterMapper = new LocStateMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locStateMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locStateMasterMapper.fromId(null)).isNull();
    }
}
