package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyListingStatusMapperTest {

    private CompanyListingStatusMapper companyListingStatusMapper;

    @BeforeEach
    public void setUp() {
        companyListingStatusMapper = new CompanyListingStatusMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyListingStatusMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyListingStatusMapper.fromId(null)).isNull();
    }
}
