package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanySectorMasterMapperTest {

    private CompanySectorMasterMapper companySectorMasterMapper;

    @BeforeEach
    public void setUp() {
        companySectorMasterMapper = new CompanySectorMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companySectorMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companySectorMasterMapper.fromId(null)).isNull();
    }
}
