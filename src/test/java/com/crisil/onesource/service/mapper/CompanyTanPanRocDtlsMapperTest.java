package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyTanPanRocDtlsMapperTest {

    private CompanyTanPanRocDtlsMapper companyTanPanRocDtlsMapper;

    @BeforeEach
    public void setUp() {
        companyTanPanRocDtlsMapper = new CompanyTanPanRocDtlsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyTanPanRocDtlsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyTanPanRocDtlsMapper.fromId(null)).isNull();
    }
}
