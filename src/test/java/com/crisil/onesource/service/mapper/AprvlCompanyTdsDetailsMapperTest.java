package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AprvlCompanyTdsDetailsMapperTest {

    private AprvlCompanyTdsDetailsMapper aprvlCompanyTdsDetailsMapper;

    @BeforeEach
    public void setUp() {
        aprvlCompanyTdsDetailsMapper = new AprvlCompanyTdsDetailsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(aprvlCompanyTdsDetailsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(aprvlCompanyTdsDetailsMapper.fromId(null)).isNull();
    }
}
