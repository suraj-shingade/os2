package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocCountryMasterMapperTest {

    private LocCountryMasterMapper locCountryMasterMapper;

    @BeforeEach
    public void setUp() {
        locCountryMasterMapper = new LocCountryMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locCountryMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locCountryMasterMapper.fromId(null)).isNull();
    }
}
