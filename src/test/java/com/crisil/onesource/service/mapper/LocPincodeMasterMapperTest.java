package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class LocPincodeMasterMapperTest {

    private LocPincodeMasterMapper locPincodeMasterMapper;

    @BeforeEach
    public void setUp() {
        locPincodeMasterMapper = new LocPincodeMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(locPincodeMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(locPincodeMasterMapper.fromId(null)).isNull();
    }
}
