package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactMasterMapperTest {

    private ContactMasterMapper contactMasterMapper;

    @BeforeEach
    public void setUp() {
        contactMasterMapper = new ContactMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactMasterMapper.fromId(null)).isNull();
    }
}
