package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class IndustryMasterMapperTest {

    private IndustryMasterMapper industryMasterMapper;

    @BeforeEach
    public void setUp() {
        industryMasterMapper = new IndustryMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(industryMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(industryMasterMapper.fromId(null)).isNull();
    }
}
