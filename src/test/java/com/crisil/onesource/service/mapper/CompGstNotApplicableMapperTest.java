package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompGstNotApplicableMapperTest {

    private CompGstNotApplicableMapper compGstNotApplicableMapper;

    @BeforeEach
    public void setUp() {
        compGstNotApplicableMapper = new CompGstNotApplicableMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(compGstNotApplicableMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(compGstNotApplicableMapper.fromId(null)).isNull();
    }
}
