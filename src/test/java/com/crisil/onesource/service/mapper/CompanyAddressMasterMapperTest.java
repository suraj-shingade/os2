package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyAddressMasterMapperTest {

    private CompanyAddressMasterMapper companyAddressMasterMapper;

    @BeforeEach
    public void setUp() {
        companyAddressMasterMapper = new CompanyAddressMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyAddressMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyAddressMasterMapper.fromId(null)).isNull();
    }
}
