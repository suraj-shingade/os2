package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyChangeRequestMapperTest {

    private CompanyChangeRequestMapper companyChangeRequestMapper;

    @BeforeEach
    public void setUp() {
        companyChangeRequestMapper = new CompanyChangeRequestMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyChangeRequestMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyChangeRequestMapper.fromId(null)).isNull();
    }
}
