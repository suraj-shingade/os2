package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyIsinMappingMapperTest {

    private CompanyIsinMappingMapper companyIsinMappingMapper;

    @BeforeEach
    public void setUp() {
        companyIsinMappingMapper = new CompanyIsinMappingMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyIsinMappingMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyIsinMappingMapper.fromId(null)).isNull();
    }
}
