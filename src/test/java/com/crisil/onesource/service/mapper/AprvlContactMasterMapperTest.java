package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AprvlContactMasterMapperTest {

    private AprvlContactMasterMapper aprvlContactMasterMapper;

    @BeforeEach
    public void setUp() {
        aprvlContactMasterMapper = new AprvlContactMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(aprvlContactMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(aprvlContactMasterMapper.fromId(null)).isNull();
    }
}
