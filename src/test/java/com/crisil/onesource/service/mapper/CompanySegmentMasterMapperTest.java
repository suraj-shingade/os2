package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanySegmentMasterMapperTest {

    private CompanySegmentMasterMapper companySegmentMasterMapper;

    @BeforeEach
    public void setUp() {
        companySegmentMasterMapper = new CompanySegmentMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companySegmentMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companySegmentMasterMapper.fromId(null)).isNull();
    }
}
