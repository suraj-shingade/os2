package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OnesourceCompanyMasterHistoryMapperTest {

    private OnesourceCompanyMasterHistoryMapper onesourceCompanyMasterHistoryMapper;

    @BeforeEach
    public void setUp() {
        onesourceCompanyMasterHistoryMapper = new OnesourceCompanyMasterHistoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(onesourceCompanyMasterHistoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(onesourceCompanyMasterHistoryMapper.fromId(null)).isNull();
    }
}
