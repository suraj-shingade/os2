package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BusinessAreaMasterMapperTest {

    private BusinessAreaMasterMapper businessAreaMasterMapper;

    @BeforeEach
    public void setUp() {
        businessAreaMasterMapper = new BusinessAreaMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(businessAreaMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(businessAreaMasterMapper.fromId(null)).isNull();
    }
}
