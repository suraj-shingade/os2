package com.crisil.onesource.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CompanyTypeMasterMapperTest {

    private CompanyTypeMasterMapper companyTypeMasterMapper;

    @BeforeEach
    public void setUp() {
        companyTypeMasterMapper = new CompanyTypeMasterMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(companyTypeMasterMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(companyTypeMasterMapper.fromId(null)).isNull();
    }
}
