package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyIsinMappingDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyIsinMappingDTO.class);
        CompanyIsinMappingDTO companyIsinMappingDTO1 = new CompanyIsinMappingDTO();
        companyIsinMappingDTO1.setId(1L);
        CompanyIsinMappingDTO companyIsinMappingDTO2 = new CompanyIsinMappingDTO();
        assertThat(companyIsinMappingDTO1).isNotEqualTo(companyIsinMappingDTO2);
        companyIsinMappingDTO2.setId(companyIsinMappingDTO1.getId());
        assertThat(companyIsinMappingDTO1).isEqualTo(companyIsinMappingDTO2);
        companyIsinMappingDTO2.setId(2L);
        assertThat(companyIsinMappingDTO1).isNotEqualTo(companyIsinMappingDTO2);
        companyIsinMappingDTO1.setId(null);
        assertThat(companyIsinMappingDTO1).isNotEqualTo(companyIsinMappingDTO2);
    }
}
