package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyRequestDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyRequestDTO.class);
        CompanyRequestDTO companyRequestDTO1 = new CompanyRequestDTO();
        companyRequestDTO1.setId(1L);
        CompanyRequestDTO companyRequestDTO2 = new CompanyRequestDTO();
        assertThat(companyRequestDTO1).isNotEqualTo(companyRequestDTO2);
        companyRequestDTO2.setId(companyRequestDTO1.getId());
        assertThat(companyRequestDTO1).isEqualTo(companyRequestDTO2);
        companyRequestDTO2.setId(2L);
        assertThat(companyRequestDTO1).isNotEqualTo(companyRequestDTO2);
        companyRequestDTO1.setId(null);
        assertThat(companyRequestDTO1).isNotEqualTo(companyRequestDTO2);
    }
}
