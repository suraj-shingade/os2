package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocCountryMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocCountryMasterDTO.class);
        LocCountryMasterDTO locCountryMasterDTO1 = new LocCountryMasterDTO();
        locCountryMasterDTO1.setId(1L);
        LocCountryMasterDTO locCountryMasterDTO2 = new LocCountryMasterDTO();
        assertThat(locCountryMasterDTO1).isNotEqualTo(locCountryMasterDTO2);
        locCountryMasterDTO2.setId(locCountryMasterDTO1.getId());
        assertThat(locCountryMasterDTO1).isEqualTo(locCountryMasterDTO2);
        locCountryMasterDTO2.setId(2L);
        assertThat(locCountryMasterDTO1).isNotEqualTo(locCountryMasterDTO2);
        locCountryMasterDTO1.setId(null);
        assertThat(locCountryMasterDTO1).isNotEqualTo(locCountryMasterDTO2);
    }
}
