package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompGstNotApplicableDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompGstNotApplicableDTO.class);
        CompGstNotApplicableDTO compGstNotApplicableDTO1 = new CompGstNotApplicableDTO();
        compGstNotApplicableDTO1.setId(1L);
        CompGstNotApplicableDTO compGstNotApplicableDTO2 = new CompGstNotApplicableDTO();
        assertThat(compGstNotApplicableDTO1).isNotEqualTo(compGstNotApplicableDTO2);
        compGstNotApplicableDTO2.setId(compGstNotApplicableDTO1.getId());
        assertThat(compGstNotApplicableDTO1).isEqualTo(compGstNotApplicableDTO2);
        compGstNotApplicableDTO2.setId(2L);
        assertThat(compGstNotApplicableDTO1).isNotEqualTo(compGstNotApplicableDTO2);
        compGstNotApplicableDTO1.setId(null);
        assertThat(compGstNotApplicableDTO1).isNotEqualTo(compGstNotApplicableDTO2);
    }
}
