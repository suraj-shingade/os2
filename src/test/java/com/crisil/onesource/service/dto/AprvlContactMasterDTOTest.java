package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class AprvlContactMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AprvlContactMasterDTO.class);
        AprvlContactMasterDTO aprvlContactMasterDTO1 = new AprvlContactMasterDTO();
        aprvlContactMasterDTO1.setId(1L);
        AprvlContactMasterDTO aprvlContactMasterDTO2 = new AprvlContactMasterDTO();
        assertThat(aprvlContactMasterDTO1).isNotEqualTo(aprvlContactMasterDTO2);
        aprvlContactMasterDTO2.setId(aprvlContactMasterDTO1.getId());
        assertThat(aprvlContactMasterDTO1).isEqualTo(aprvlContactMasterDTO2);
        aprvlContactMasterDTO2.setId(2L);
        assertThat(aprvlContactMasterDTO1).isNotEqualTo(aprvlContactMasterDTO2);
        aprvlContactMasterDTO1.setId(null);
        assertThat(aprvlContactMasterDTO1).isNotEqualTo(aprvlContactMasterDTO2);
    }
}
