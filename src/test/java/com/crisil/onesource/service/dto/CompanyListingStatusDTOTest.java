package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyListingStatusDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyListingStatusDTO.class);
        CompanyListingStatusDTO companyListingStatusDTO1 = new CompanyListingStatusDTO();
        companyListingStatusDTO1.setId(1L);
        CompanyListingStatusDTO companyListingStatusDTO2 = new CompanyListingStatusDTO();
        assertThat(companyListingStatusDTO1).isNotEqualTo(companyListingStatusDTO2);
        companyListingStatusDTO2.setId(companyListingStatusDTO1.getId());
        assertThat(companyListingStatusDTO1).isEqualTo(companyListingStatusDTO2);
        companyListingStatusDTO2.setId(2L);
        assertThat(companyListingStatusDTO1).isNotEqualTo(companyListingStatusDTO2);
        companyListingStatusDTO1.setId(null);
        assertThat(companyListingStatusDTO1).isNotEqualTo(companyListingStatusDTO2);
    }
}
