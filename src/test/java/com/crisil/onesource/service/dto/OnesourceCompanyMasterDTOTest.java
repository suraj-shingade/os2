package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class OnesourceCompanyMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OnesourceCompanyMasterDTO.class);
        OnesourceCompanyMasterDTO onesourceCompanyMasterDTO1 = new OnesourceCompanyMasterDTO();
        onesourceCompanyMasterDTO1.setId(1L);
        OnesourceCompanyMasterDTO onesourceCompanyMasterDTO2 = new OnesourceCompanyMasterDTO();
        assertThat(onesourceCompanyMasterDTO1).isNotEqualTo(onesourceCompanyMasterDTO2);
        onesourceCompanyMasterDTO2.setId(onesourceCompanyMasterDTO1.getId());
        assertThat(onesourceCompanyMasterDTO1).isEqualTo(onesourceCompanyMasterDTO2);
        onesourceCompanyMasterDTO2.setId(2L);
        assertThat(onesourceCompanyMasterDTO1).isNotEqualTo(onesourceCompanyMasterDTO2);
        onesourceCompanyMasterDTO1.setId(null);
        assertThat(onesourceCompanyMasterDTO1).isNotEqualTo(onesourceCompanyMasterDTO2);
    }
}
