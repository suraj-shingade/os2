package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocCityPincodeMappingDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocCityPincodeMappingDTO.class);
        LocCityPincodeMappingDTO locCityPincodeMappingDTO1 = new LocCityPincodeMappingDTO();
        locCityPincodeMappingDTO1.setId(1L);
        LocCityPincodeMappingDTO locCityPincodeMappingDTO2 = new LocCityPincodeMappingDTO();
        assertThat(locCityPincodeMappingDTO1).isNotEqualTo(locCityPincodeMappingDTO2);
        locCityPincodeMappingDTO2.setId(locCityPincodeMappingDTO1.getId());
        assertThat(locCityPincodeMappingDTO1).isEqualTo(locCityPincodeMappingDTO2);
        locCityPincodeMappingDTO2.setId(2L);
        assertThat(locCityPincodeMappingDTO1).isNotEqualTo(locCityPincodeMappingDTO2);
        locCityPincodeMappingDTO1.setId(null);
        assertThat(locCityPincodeMappingDTO1).isNotEqualTo(locCityPincodeMappingDTO2);
    }
}
