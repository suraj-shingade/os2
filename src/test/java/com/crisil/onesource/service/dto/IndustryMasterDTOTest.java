package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class IndustryMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IndustryMasterDTO.class);
        IndustryMasterDTO industryMasterDTO1 = new IndustryMasterDTO();
        industryMasterDTO1.setId(1L);
        IndustryMasterDTO industryMasterDTO2 = new IndustryMasterDTO();
        assertThat(industryMasterDTO1).isNotEqualTo(industryMasterDTO2);
        industryMasterDTO2.setId(industryMasterDTO1.getId());
        assertThat(industryMasterDTO1).isEqualTo(industryMasterDTO2);
        industryMasterDTO2.setId(2L);
        assertThat(industryMasterDTO1).isNotEqualTo(industryMasterDTO2);
        industryMasterDTO1.setId(null);
        assertThat(industryMasterDTO1).isNotEqualTo(industryMasterDTO2);
    }
}
