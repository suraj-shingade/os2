package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanySegmentMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanySegmentMasterDTO.class);
        CompanySegmentMasterDTO companySegmentMasterDTO1 = new CompanySegmentMasterDTO();
        companySegmentMasterDTO1.setId(1L);
        CompanySegmentMasterDTO companySegmentMasterDTO2 = new CompanySegmentMasterDTO();
        assertThat(companySegmentMasterDTO1).isNotEqualTo(companySegmentMasterDTO2);
        companySegmentMasterDTO2.setId(companySegmentMasterDTO1.getId());
        assertThat(companySegmentMasterDTO1).isEqualTo(companySegmentMasterDTO2);
        companySegmentMasterDTO2.setId(2L);
        assertThat(companySegmentMasterDTO1).isNotEqualTo(companySegmentMasterDTO2);
        companySegmentMasterDTO1.setId(null);
        assertThat(companySegmentMasterDTO1).isNotEqualTo(companySegmentMasterDTO2);
    }
}
