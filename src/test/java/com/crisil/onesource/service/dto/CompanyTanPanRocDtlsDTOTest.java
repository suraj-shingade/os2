package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyTanPanRocDtlsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyTanPanRocDtlsDTO.class);
        CompanyTanPanRocDtlsDTO companyTanPanRocDtlsDTO1 = new CompanyTanPanRocDtlsDTO();
        companyTanPanRocDtlsDTO1.setId(1L);
        CompanyTanPanRocDtlsDTO companyTanPanRocDtlsDTO2 = new CompanyTanPanRocDtlsDTO();
        assertThat(companyTanPanRocDtlsDTO1).isNotEqualTo(companyTanPanRocDtlsDTO2);
        companyTanPanRocDtlsDTO2.setId(companyTanPanRocDtlsDTO1.getId());
        assertThat(companyTanPanRocDtlsDTO1).isEqualTo(companyTanPanRocDtlsDTO2);
        companyTanPanRocDtlsDTO2.setId(2L);
        assertThat(companyTanPanRocDtlsDTO1).isNotEqualTo(companyTanPanRocDtlsDTO2);
        companyTanPanRocDtlsDTO1.setId(null);
        assertThat(companyTanPanRocDtlsDTO1).isNotEqualTo(companyTanPanRocDtlsDTO2);
    }
}
