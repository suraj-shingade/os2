package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class KarzaCompInformationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(KarzaCompInformationDTO.class);
        KarzaCompInformationDTO karzaCompInformationDTO1 = new KarzaCompInformationDTO();
        karzaCompInformationDTO1.setId(1L);
        KarzaCompInformationDTO karzaCompInformationDTO2 = new KarzaCompInformationDTO();
        assertThat(karzaCompInformationDTO1).isNotEqualTo(karzaCompInformationDTO2);
        karzaCompInformationDTO2.setId(karzaCompInformationDTO1.getId());
        assertThat(karzaCompInformationDTO1).isEqualTo(karzaCompInformationDTO2);
        karzaCompInformationDTO2.setId(2L);
        assertThat(karzaCompInformationDTO1).isNotEqualTo(karzaCompInformationDTO2);
        karzaCompInformationDTO1.setId(null);
        assertThat(karzaCompInformationDTO1).isNotEqualTo(karzaCompInformationDTO2);
    }
}
