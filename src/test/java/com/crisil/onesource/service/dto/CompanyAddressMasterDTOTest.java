package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyAddressMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyAddressMasterDTO.class);
        CompanyAddressMasterDTO companyAddressMasterDTO1 = new CompanyAddressMasterDTO();
        companyAddressMasterDTO1.setId(1L);
        CompanyAddressMasterDTO companyAddressMasterDTO2 = new CompanyAddressMasterDTO();
        assertThat(companyAddressMasterDTO1).isNotEqualTo(companyAddressMasterDTO2);
        companyAddressMasterDTO2.setId(companyAddressMasterDTO1.getId());
        assertThat(companyAddressMasterDTO1).isEqualTo(companyAddressMasterDTO2);
        companyAddressMasterDTO2.setId(2L);
        assertThat(companyAddressMasterDTO1).isNotEqualTo(companyAddressMasterDTO2);
        companyAddressMasterDTO1.setId(null);
        assertThat(companyAddressMasterDTO1).isNotEqualTo(companyAddressMasterDTO2);
    }
}
