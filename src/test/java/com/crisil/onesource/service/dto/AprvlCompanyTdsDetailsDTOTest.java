package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class AprvlCompanyTdsDetailsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AprvlCompanyTdsDetailsDTO.class);
        AprvlCompanyTdsDetailsDTO aprvlCompanyTdsDetailsDTO1 = new AprvlCompanyTdsDetailsDTO();
        aprvlCompanyTdsDetailsDTO1.setId(1L);
        AprvlCompanyTdsDetailsDTO aprvlCompanyTdsDetailsDTO2 = new AprvlCompanyTdsDetailsDTO();
        assertThat(aprvlCompanyTdsDetailsDTO1).isNotEqualTo(aprvlCompanyTdsDetailsDTO2);
        aprvlCompanyTdsDetailsDTO2.setId(aprvlCompanyTdsDetailsDTO1.getId());
        assertThat(aprvlCompanyTdsDetailsDTO1).isEqualTo(aprvlCompanyTdsDetailsDTO2);
        aprvlCompanyTdsDetailsDTO2.setId(2L);
        assertThat(aprvlCompanyTdsDetailsDTO1).isNotEqualTo(aprvlCompanyTdsDetailsDTO2);
        aprvlCompanyTdsDetailsDTO1.setId(null);
        assertThat(aprvlCompanyTdsDetailsDTO1).isNotEqualTo(aprvlCompanyTdsDetailsDTO2);
    }
}
