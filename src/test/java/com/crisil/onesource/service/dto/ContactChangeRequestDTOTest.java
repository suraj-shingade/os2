package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class ContactChangeRequestDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactChangeRequestDTO.class);
        ContactChangeRequestDTO contactChangeRequestDTO1 = new ContactChangeRequestDTO();
        contactChangeRequestDTO1.setId(1L);
        ContactChangeRequestDTO contactChangeRequestDTO2 = new ContactChangeRequestDTO();
        assertThat(contactChangeRequestDTO1).isNotEqualTo(contactChangeRequestDTO2);
        contactChangeRequestDTO2.setId(contactChangeRequestDTO1.getId());
        assertThat(contactChangeRequestDTO1).isEqualTo(contactChangeRequestDTO2);
        contactChangeRequestDTO2.setId(2L);
        assertThat(contactChangeRequestDTO1).isNotEqualTo(contactChangeRequestDTO2);
        contactChangeRequestDTO1.setId(null);
        assertThat(contactChangeRequestDTO1).isNotEqualTo(contactChangeRequestDTO2);
    }
}
