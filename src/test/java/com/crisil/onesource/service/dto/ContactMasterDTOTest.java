package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class ContactMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMasterDTO.class);
        ContactMasterDTO contactMasterDTO1 = new ContactMasterDTO();
        contactMasterDTO1.setId(1L);
        ContactMasterDTO contactMasterDTO2 = new ContactMasterDTO();
        assertThat(contactMasterDTO1).isNotEqualTo(contactMasterDTO2);
        contactMasterDTO2.setId(contactMasterDTO1.getId());
        assertThat(contactMasterDTO1).isEqualTo(contactMasterDTO2);
        contactMasterDTO2.setId(2L);
        assertThat(contactMasterDTO1).isNotEqualTo(contactMasterDTO2);
        contactMasterDTO1.setId(null);
        assertThat(contactMasterDTO1).isNotEqualTo(contactMasterDTO2);
    }
}
