package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class AprvlCompanyGstDetailsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AprvlCompanyGstDetailsDTO.class);
        AprvlCompanyGstDetailsDTO aprvlCompanyGstDetailsDTO1 = new AprvlCompanyGstDetailsDTO();
        aprvlCompanyGstDetailsDTO1.setId(1L);
        AprvlCompanyGstDetailsDTO aprvlCompanyGstDetailsDTO2 = new AprvlCompanyGstDetailsDTO();
        assertThat(aprvlCompanyGstDetailsDTO1).isNotEqualTo(aprvlCompanyGstDetailsDTO2);
        aprvlCompanyGstDetailsDTO2.setId(aprvlCompanyGstDetailsDTO1.getId());
        assertThat(aprvlCompanyGstDetailsDTO1).isEqualTo(aprvlCompanyGstDetailsDTO2);
        aprvlCompanyGstDetailsDTO2.setId(2L);
        assertThat(aprvlCompanyGstDetailsDTO1).isNotEqualTo(aprvlCompanyGstDetailsDTO2);
        aprvlCompanyGstDetailsDTO1.setId(null);
        assertThat(aprvlCompanyGstDetailsDTO1).isNotEqualTo(aprvlCompanyGstDetailsDTO2);
    }
}
