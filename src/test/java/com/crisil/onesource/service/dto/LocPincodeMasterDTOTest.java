package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocPincodeMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocPincodeMasterDTO.class);
        LocPincodeMasterDTO locPincodeMasterDTO1 = new LocPincodeMasterDTO();
        locPincodeMasterDTO1.setId(1L);
        LocPincodeMasterDTO locPincodeMasterDTO2 = new LocPincodeMasterDTO();
        assertThat(locPincodeMasterDTO1).isNotEqualTo(locPincodeMasterDTO2);
        locPincodeMasterDTO2.setId(locPincodeMasterDTO1.getId());
        assertThat(locPincodeMasterDTO1).isEqualTo(locPincodeMasterDTO2);
        locPincodeMasterDTO2.setId(2L);
        assertThat(locPincodeMasterDTO1).isNotEqualTo(locPincodeMasterDTO2);
        locPincodeMasterDTO1.setId(null);
        assertThat(locPincodeMasterDTO1).isNotEqualTo(locPincodeMasterDTO2);
    }
}
