package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocRegionMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocRegionMasterDTO.class);
        LocRegionMasterDTO locRegionMasterDTO1 = new LocRegionMasterDTO();
        locRegionMasterDTO1.setId(1L);
        LocRegionMasterDTO locRegionMasterDTO2 = new LocRegionMasterDTO();
        assertThat(locRegionMasterDTO1).isNotEqualTo(locRegionMasterDTO2);
        locRegionMasterDTO2.setId(locRegionMasterDTO1.getId());
        assertThat(locRegionMasterDTO1).isEqualTo(locRegionMasterDTO2);
        locRegionMasterDTO2.setId(2L);
        assertThat(locRegionMasterDTO1).isNotEqualTo(locRegionMasterDTO2);
        locRegionMasterDTO1.setId(null);
        assertThat(locRegionMasterDTO1).isNotEqualTo(locRegionMasterDTO2);
    }
}
