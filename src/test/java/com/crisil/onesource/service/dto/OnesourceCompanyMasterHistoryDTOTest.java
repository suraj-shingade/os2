package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class OnesourceCompanyMasterHistoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OnesourceCompanyMasterHistoryDTO.class);
        OnesourceCompanyMasterHistoryDTO onesourceCompanyMasterHistoryDTO1 = new OnesourceCompanyMasterHistoryDTO();
        onesourceCompanyMasterHistoryDTO1.setId(1L);
        OnesourceCompanyMasterHistoryDTO onesourceCompanyMasterHistoryDTO2 = new OnesourceCompanyMasterHistoryDTO();
        assertThat(onesourceCompanyMasterHistoryDTO1).isNotEqualTo(onesourceCompanyMasterHistoryDTO2);
        onesourceCompanyMasterHistoryDTO2.setId(onesourceCompanyMasterHistoryDTO1.getId());
        assertThat(onesourceCompanyMasterHistoryDTO1).isEqualTo(onesourceCompanyMasterHistoryDTO2);
        onesourceCompanyMasterHistoryDTO2.setId(2L);
        assertThat(onesourceCompanyMasterHistoryDTO1).isNotEqualTo(onesourceCompanyMasterHistoryDTO2);
        onesourceCompanyMasterHistoryDTO1.setId(null);
        assertThat(onesourceCompanyMasterHistoryDTO1).isNotEqualTo(onesourceCompanyMasterHistoryDTO2);
    }
}
