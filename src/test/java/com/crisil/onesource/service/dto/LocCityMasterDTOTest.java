package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocCityMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocCityMasterDTO.class);
        LocCityMasterDTO locCityMasterDTO1 = new LocCityMasterDTO();
        locCityMasterDTO1.setId(1L);
        LocCityMasterDTO locCityMasterDTO2 = new LocCityMasterDTO();
        assertThat(locCityMasterDTO1).isNotEqualTo(locCityMasterDTO2);
        locCityMasterDTO2.setId(locCityMasterDTO1.getId());
        assertThat(locCityMasterDTO1).isEqualTo(locCityMasterDTO2);
        locCityMasterDTO2.setId(2L);
        assertThat(locCityMasterDTO1).isNotEqualTo(locCityMasterDTO2);
        locCityMasterDTO1.setId(null);
        assertThat(locCityMasterDTO1).isNotEqualTo(locCityMasterDTO2);
    }
}
