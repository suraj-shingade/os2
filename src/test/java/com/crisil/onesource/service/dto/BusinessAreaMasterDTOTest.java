package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class BusinessAreaMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BusinessAreaMasterDTO.class);
        BusinessAreaMasterDTO businessAreaMasterDTO1 = new BusinessAreaMasterDTO();
        businessAreaMasterDTO1.setId(1L);
        BusinessAreaMasterDTO businessAreaMasterDTO2 = new BusinessAreaMasterDTO();
        assertThat(businessAreaMasterDTO1).isNotEqualTo(businessAreaMasterDTO2);
        businessAreaMasterDTO2.setId(businessAreaMasterDTO1.getId());
        assertThat(businessAreaMasterDTO1).isEqualTo(businessAreaMasterDTO2);
        businessAreaMasterDTO2.setId(2L);
        assertThat(businessAreaMasterDTO1).isNotEqualTo(businessAreaMasterDTO2);
        businessAreaMasterDTO1.setId(null);
        assertThat(businessAreaMasterDTO1).isNotEqualTo(businessAreaMasterDTO2);
    }
}
