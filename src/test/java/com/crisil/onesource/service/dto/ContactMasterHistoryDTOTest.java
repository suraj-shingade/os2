package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class ContactMasterHistoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMasterHistoryDTO.class);
        ContactMasterHistoryDTO contactMasterHistoryDTO1 = new ContactMasterHistoryDTO();
        contactMasterHistoryDTO1.setId(1L);
        ContactMasterHistoryDTO contactMasterHistoryDTO2 = new ContactMasterHistoryDTO();
        assertThat(contactMasterHistoryDTO1).isNotEqualTo(contactMasterHistoryDTO2);
        contactMasterHistoryDTO2.setId(contactMasterHistoryDTO1.getId());
        assertThat(contactMasterHistoryDTO1).isEqualTo(contactMasterHistoryDTO2);
        contactMasterHistoryDTO2.setId(2L);
        assertThat(contactMasterHistoryDTO1).isNotEqualTo(contactMasterHistoryDTO2);
        contactMasterHistoryDTO1.setId(null);
        assertThat(contactMasterHistoryDTO1).isNotEqualTo(contactMasterHistoryDTO2);
    }
}
