package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class VwContactGstDetailsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VwContactGstDetailsDTO.class);
        VwContactGstDetailsDTO vwContactGstDetailsDTO1 = new VwContactGstDetailsDTO();
        vwContactGstDetailsDTO1.setId(1L);
        VwContactGstDetailsDTO vwContactGstDetailsDTO2 = new VwContactGstDetailsDTO();
        assertThat(vwContactGstDetailsDTO1).isNotEqualTo(vwContactGstDetailsDTO2);
        vwContactGstDetailsDTO2.setId(vwContactGstDetailsDTO1.getId());
        assertThat(vwContactGstDetailsDTO1).isEqualTo(vwContactGstDetailsDTO2);
        vwContactGstDetailsDTO2.setId(2L);
        assertThat(vwContactGstDetailsDTO1).isNotEqualTo(vwContactGstDetailsDTO2);
        vwContactGstDetailsDTO1.setId(null);
        assertThat(vwContactGstDetailsDTO1).isNotEqualTo(vwContactGstDetailsDTO2);
    }
}
