package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocDistrictMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocDistrictMasterDTO.class);
        LocDistrictMasterDTO locDistrictMasterDTO1 = new LocDistrictMasterDTO();
        locDistrictMasterDTO1.setId(1L);
        LocDistrictMasterDTO locDistrictMasterDTO2 = new LocDistrictMasterDTO();
        assertThat(locDistrictMasterDTO1).isNotEqualTo(locDistrictMasterDTO2);
        locDistrictMasterDTO2.setId(locDistrictMasterDTO1.getId());
        assertThat(locDistrictMasterDTO1).isEqualTo(locDistrictMasterDTO2);
        locDistrictMasterDTO2.setId(2L);
        assertThat(locDistrictMasterDTO1).isNotEqualTo(locDistrictMasterDTO2);
        locDistrictMasterDTO1.setId(null);
        assertThat(locDistrictMasterDTO1).isNotEqualTo(locDistrictMasterDTO2);
    }
}
