package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyChangeRequestDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyChangeRequestDTO.class);
        CompanyChangeRequestDTO companyChangeRequestDTO1 = new CompanyChangeRequestDTO();
        companyChangeRequestDTO1.setId(1L);
        CompanyChangeRequestDTO companyChangeRequestDTO2 = new CompanyChangeRequestDTO();
        assertThat(companyChangeRequestDTO1).isNotEqualTo(companyChangeRequestDTO2);
        companyChangeRequestDTO2.setId(companyChangeRequestDTO1.getId());
        assertThat(companyChangeRequestDTO1).isEqualTo(companyChangeRequestDTO2);
        companyChangeRequestDTO2.setId(2L);
        assertThat(companyChangeRequestDTO1).isNotEqualTo(companyChangeRequestDTO2);
        companyChangeRequestDTO1.setId(null);
        assertThat(companyChangeRequestDTO1).isNotEqualTo(companyChangeRequestDTO2);
    }
}
