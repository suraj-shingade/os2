package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class LocStateMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(LocStateMasterDTO.class);
        LocStateMasterDTO locStateMasterDTO1 = new LocStateMasterDTO();
        locStateMasterDTO1.setId(1L);
        LocStateMasterDTO locStateMasterDTO2 = new LocStateMasterDTO();
        assertThat(locStateMasterDTO1).isNotEqualTo(locStateMasterDTO2);
        locStateMasterDTO2.setId(locStateMasterDTO1.getId());
        assertThat(locStateMasterDTO1).isEqualTo(locStateMasterDTO2);
        locStateMasterDTO2.setId(2L);
        assertThat(locStateMasterDTO1).isNotEqualTo(locStateMasterDTO2);
        locStateMasterDTO1.setId(null);
        assertThat(locStateMasterDTO1).isNotEqualTo(locStateMasterDTO2);
    }
}
