package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class GroupMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GroupMasterDTO.class);
        GroupMasterDTO groupMasterDTO1 = new GroupMasterDTO();
        groupMasterDTO1.setId(1L);
        GroupMasterDTO groupMasterDTO2 = new GroupMasterDTO();
        assertThat(groupMasterDTO1).isNotEqualTo(groupMasterDTO2);
        groupMasterDTO2.setId(groupMasterDTO1.getId());
        assertThat(groupMasterDTO1).isEqualTo(groupMasterDTO2);
        groupMasterDTO2.setId(2L);
        assertThat(groupMasterDTO1).isNotEqualTo(groupMasterDTO2);
        groupMasterDTO1.setId(null);
        assertThat(groupMasterDTO1).isNotEqualTo(groupMasterDTO2);
    }
}
