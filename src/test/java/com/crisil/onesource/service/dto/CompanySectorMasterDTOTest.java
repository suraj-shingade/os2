package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanySectorMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanySectorMasterDTO.class);
        CompanySectorMasterDTO companySectorMasterDTO1 = new CompanySectorMasterDTO();
        companySectorMasterDTO1.setId(1L);
        CompanySectorMasterDTO companySectorMasterDTO2 = new CompanySectorMasterDTO();
        assertThat(companySectorMasterDTO1).isNotEqualTo(companySectorMasterDTO2);
        companySectorMasterDTO2.setId(companySectorMasterDTO1.getId());
        assertThat(companySectorMasterDTO1).isEqualTo(companySectorMasterDTO2);
        companySectorMasterDTO2.setId(2L);
        assertThat(companySectorMasterDTO1).isNotEqualTo(companySectorMasterDTO2);
        companySectorMasterDTO1.setId(null);
        assertThat(companySectorMasterDTO1).isNotEqualTo(companySectorMasterDTO2);
    }
}
