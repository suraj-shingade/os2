package com.crisil.onesource.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.crisil.onesource.web.rest.TestUtil;

public class CompanyTypeMasterDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompanyTypeMasterDTO.class);
        CompanyTypeMasterDTO companyTypeMasterDTO1 = new CompanyTypeMasterDTO();
        companyTypeMasterDTO1.setId(1L);
        CompanyTypeMasterDTO companyTypeMasterDTO2 = new CompanyTypeMasterDTO();
        assertThat(companyTypeMasterDTO1).isNotEqualTo(companyTypeMasterDTO2);
        companyTypeMasterDTO2.setId(companyTypeMasterDTO1.getId());
        assertThat(companyTypeMasterDTO1).isEqualTo(companyTypeMasterDTO2);
        companyTypeMasterDTO2.setId(2L);
        assertThat(companyTypeMasterDTO1).isNotEqualTo(companyTypeMasterDTO2);
        companyTypeMasterDTO1.setId(null);
        assertThat(companyTypeMasterDTO1).isNotEqualTo(companyTypeMasterDTO2);
    }
}
