package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanyAddressMaster;
import com.crisil.onesource.service.CompanyAddressMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanyAddressMasterCriteria;
import com.crisil.onesource.service.CompanyAddressMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanyAddressMaster}.
 */
@RestController
@RequestMapping("/api")
public class CompanyAddressMasterResource {

    private final Logger log = LoggerFactory.getLogger(CompanyAddressMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanyAddressMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyAddressMasterService companyAddressMasterService;

    private final CompanyAddressMasterQueryService companyAddressMasterQueryService;

    public CompanyAddressMasterResource(CompanyAddressMasterService companyAddressMasterService, CompanyAddressMasterQueryService companyAddressMasterQueryService) {
        this.companyAddressMasterService = companyAddressMasterService;
        this.companyAddressMasterQueryService = companyAddressMasterQueryService;
    }

    /**
     * {@code POST  /company-address-masters} : Create a new companyAddressMaster.
     *
     * @param companyAddressMaster the companyAddressMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyAddressMaster, or with status {@code 400 (Bad Request)} if the companyAddressMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-address-masters")
    public ResponseEntity<CompanyAddressMaster> createCompanyAddressMaster(@Valid @RequestBody CompanyAddressMaster companyAddressMaster) throws URISyntaxException {
        log.debug("REST request to save CompanyAddressMaster : {}", companyAddressMaster);
        if (companyAddressMaster.getId() != null) {
            throw new BadRequestAlertException("A new companyAddressMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyAddressMaster result = companyAddressMasterService.save(companyAddressMaster);
        return ResponseEntity.created(new URI("/api/company-address-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-address-masters} : Updates an existing companyAddressMaster.
     *
     * @param companyAddressMaster the companyAddressMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyAddressMaster,
     * or with status {@code 400 (Bad Request)} if the companyAddressMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyAddressMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-address-masters")
    public ResponseEntity<CompanyAddressMaster> updateCompanyAddressMaster(@Valid @RequestBody CompanyAddressMaster companyAddressMaster) throws URISyntaxException {
        log.debug("REST request to update CompanyAddressMaster : {}", companyAddressMaster);
        if (companyAddressMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyAddressMaster result = companyAddressMasterService.save(companyAddressMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companyAddressMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-address-masters} : get all the companyAddressMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyAddressMasters in body.
     */
    @GetMapping("/company-address-masters")
    public ResponseEntity<List<CompanyAddressMaster>> getAllCompanyAddressMasters(CompanyAddressMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanyAddressMasters by criteria: {}", criteria);
        Page<CompanyAddressMaster> page = companyAddressMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-address-masters/count} : count all the companyAddressMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-address-masters/count")
    public ResponseEntity<Long> countCompanyAddressMasters(CompanyAddressMasterCriteria criteria) {
        log.debug("REST request to count CompanyAddressMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(companyAddressMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-address-masters/:id} : get the "id" companyAddressMaster.
     *
     * @param id the id of the companyAddressMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyAddressMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-address-masters/{id}")
    public ResponseEntity<CompanyAddressMaster> getCompanyAddressMaster(@PathVariable Long id) {
        log.debug("REST request to get CompanyAddressMaster : {}", id);
        Optional<CompanyAddressMaster> companyAddressMaster = companyAddressMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyAddressMaster);
    }

    /**
     * {@code DELETE  /company-address-masters/:id} : delete the "id" companyAddressMaster.
     *
     * @param id the id of the companyAddressMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-address-masters/{id}")
    public ResponseEntity<Void> deleteCompanyAddressMaster(@PathVariable Long id) {
        log.debug("REST request to delete CompanyAddressMaster : {}", id);
        companyAddressMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
