package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.BusinessAreaMaster;
import com.crisil.onesource.service.BusinessAreaMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.BusinessAreaMasterCriteria;
import com.crisil.onesource.service.BusinessAreaMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.BusinessAreaMaster}.
 */
@RestController
@RequestMapping("/api")
public class BusinessAreaMasterResource {

    private final Logger log = LoggerFactory.getLogger(BusinessAreaMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceBusinessAreaMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BusinessAreaMasterService businessAreaMasterService;

    private final BusinessAreaMasterQueryService businessAreaMasterQueryService;

    public BusinessAreaMasterResource(BusinessAreaMasterService businessAreaMasterService, BusinessAreaMasterQueryService businessAreaMasterQueryService) {
        this.businessAreaMasterService = businessAreaMasterService;
        this.businessAreaMasterQueryService = businessAreaMasterQueryService;
    }

    /**
     * {@code POST  /business-area-masters} : Create a new businessAreaMaster.
     *
     * @param businessAreaMaster the businessAreaMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new businessAreaMaster, or with status {@code 400 (Bad Request)} if the businessAreaMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/business-area-masters")
    public ResponseEntity<BusinessAreaMaster> createBusinessAreaMaster(@Valid @RequestBody BusinessAreaMaster businessAreaMaster) throws URISyntaxException {
        log.debug("REST request to save BusinessAreaMaster : {}", businessAreaMaster);
        if (businessAreaMaster.getId() != null) {
            throw new BadRequestAlertException("A new businessAreaMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BusinessAreaMaster result = businessAreaMasterService.save(businessAreaMaster);
        return ResponseEntity.created(new URI("/api/business-area-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /business-area-masters} : Updates an existing businessAreaMaster.
     *
     * @param businessAreaMaster the businessAreaMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated businessAreaMaster,
     * or with status {@code 400 (Bad Request)} if the businessAreaMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the businessAreaMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/business-area-masters")
    public ResponseEntity<BusinessAreaMaster> updateBusinessAreaMaster(@Valid @RequestBody BusinessAreaMaster businessAreaMaster) throws URISyntaxException {
        log.debug("REST request to update BusinessAreaMaster : {}", businessAreaMaster);
        if (businessAreaMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BusinessAreaMaster result = businessAreaMasterService.save(businessAreaMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, businessAreaMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /business-area-masters} : get all the businessAreaMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of businessAreaMasters in body.
     */
    @GetMapping("/business-area-masters")
    public ResponseEntity<List<BusinessAreaMaster>> getAllBusinessAreaMasters(BusinessAreaMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BusinessAreaMasters by criteria: {}", criteria);
        Page<BusinessAreaMaster> page = businessAreaMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /business-area-masters/count} : count all the businessAreaMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/business-area-masters/count")
    public ResponseEntity<Long> countBusinessAreaMasters(BusinessAreaMasterCriteria criteria) {
        log.debug("REST request to count BusinessAreaMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(businessAreaMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /business-area-masters/:id} : get the "id" businessAreaMaster.
     *
     * @param id the id of the businessAreaMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the businessAreaMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/business-area-masters/{id}")
    public ResponseEntity<BusinessAreaMaster> getBusinessAreaMaster(@PathVariable Long id) {
        log.debug("REST request to get BusinessAreaMaster : {}", id);
        Optional<BusinessAreaMaster> businessAreaMaster = businessAreaMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(businessAreaMaster);
    }

    /**
     * {@code DELETE  /business-area-masters/:id} : delete the "id" businessAreaMaster.
     *
     * @param id the id of the businessAreaMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/business-area-masters/{id}")
    public ResponseEntity<Void> deleteBusinessAreaMaster(@PathVariable Long id) {
        log.debug("REST request to delete BusinessAreaMaster : {}", id);
        businessAreaMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
