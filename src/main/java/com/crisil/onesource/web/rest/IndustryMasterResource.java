package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.IndustryMaster;
import com.crisil.onesource.service.IndustryMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.IndustryMasterCriteria;
import com.crisil.onesource.service.IndustryMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.IndustryMaster}.
 */
@RestController
@RequestMapping("/api")
public class IndustryMasterResource {

    private final Logger log = LoggerFactory.getLogger(IndustryMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceIndustryMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndustryMasterService industryMasterService;

    private final IndustryMasterQueryService industryMasterQueryService;

    public IndustryMasterResource(IndustryMasterService industryMasterService, IndustryMasterQueryService industryMasterQueryService) {
        this.industryMasterService = industryMasterService;
        this.industryMasterQueryService = industryMasterQueryService;
    }

    /**
     * {@code POST  /industry-masters} : Create a new industryMaster.
     *
     * @param industryMaster the industryMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new industryMaster, or with status {@code 400 (Bad Request)} if the industryMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/industry-masters")
    public ResponseEntity<IndustryMaster> createIndustryMaster(@Valid @RequestBody IndustryMaster industryMaster) throws URISyntaxException {
        log.debug("REST request to save IndustryMaster : {}", industryMaster);
        if (industryMaster.getId() != null) {
            throw new BadRequestAlertException("A new industryMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndustryMaster result = industryMasterService.save(industryMaster);
        return ResponseEntity.created(new URI("/api/industry-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /industry-masters} : Updates an existing industryMaster.
     *
     * @param industryMaster the industryMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated industryMaster,
     * or with status {@code 400 (Bad Request)} if the industryMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the industryMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/industry-masters")
    public ResponseEntity<IndustryMaster> updateIndustryMaster(@Valid @RequestBody IndustryMaster industryMaster) throws URISyntaxException {
        log.debug("REST request to update IndustryMaster : {}", industryMaster);
        if (industryMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        IndustryMaster result = industryMasterService.save(industryMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, industryMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /industry-masters} : get all the industryMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of industryMasters in body.
     */
    @GetMapping("/industry-masters")
    public ResponseEntity<List<IndustryMaster>> getAllIndustryMasters(IndustryMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get IndustryMasters by criteria: {}", criteria);
        Page<IndustryMaster> page = industryMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /industry-masters/count} : count all the industryMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/industry-masters/count")
    public ResponseEntity<Long> countIndustryMasters(IndustryMasterCriteria criteria) {
        log.debug("REST request to count IndustryMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(industryMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /industry-masters/:id} : get the "id" industryMaster.
     *
     * @param id the id of the industryMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the industryMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/industry-masters/{id}")
    public ResponseEntity<IndustryMaster> getIndustryMaster(@PathVariable Long id) {
        log.debug("REST request to get IndustryMaster : {}", id);
        Optional<IndustryMaster> industryMaster = industryMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(industryMaster);
    }

    /**
     * {@code DELETE  /industry-masters/:id} : delete the "id" industryMaster.
     *
     * @param id the id of the industryMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/industry-masters/{id}")
    public ResponseEntity<Void> deleteIndustryMaster(@PathVariable Long id) {
        log.debug("REST request to delete IndustryMaster : {}", id);
        industryMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
