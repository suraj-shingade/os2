package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.ContactMasterHistory;
import com.crisil.onesource.service.ContactMasterHistoryService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.ContactMasterHistoryCriteria;
import com.crisil.onesource.service.ContactMasterHistoryQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.ContactMasterHistory}.
 */
@RestController
@RequestMapping("/api")
public class ContactMasterHistoryResource {

    private final Logger log = LoggerFactory.getLogger(ContactMasterHistoryResource.class);

    private static final String ENTITY_NAME = "oneSourceContactMasterHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactMasterHistoryService contactMasterHistoryService;

    private final ContactMasterHistoryQueryService contactMasterHistoryQueryService;

    public ContactMasterHistoryResource(ContactMasterHistoryService contactMasterHistoryService, ContactMasterHistoryQueryService contactMasterHistoryQueryService) {
        this.contactMasterHistoryService = contactMasterHistoryService;
        this.contactMasterHistoryQueryService = contactMasterHistoryQueryService;
    }

    /**
     * {@code POST  /contact-master-histories} : Create a new contactMasterHistory.
     *
     * @param contactMasterHistory the contactMasterHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactMasterHistory, or with status {@code 400 (Bad Request)} if the contactMasterHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-master-histories")
    public ResponseEntity<ContactMasterHistory> createContactMasterHistory(@Valid @RequestBody ContactMasterHistory contactMasterHistory) throws URISyntaxException {
        log.debug("REST request to save ContactMasterHistory : {}", contactMasterHistory);
        if (contactMasterHistory.getId() != null) {
            throw new BadRequestAlertException("A new contactMasterHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactMasterHistory result = contactMasterHistoryService.save(contactMasterHistory);
        return ResponseEntity.created(new URI("/api/contact-master-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-master-histories} : Updates an existing contactMasterHistory.
     *
     * @param contactMasterHistory the contactMasterHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactMasterHistory,
     * or with status {@code 400 (Bad Request)} if the contactMasterHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactMasterHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-master-histories")
    public ResponseEntity<ContactMasterHistory> updateContactMasterHistory(@Valid @RequestBody ContactMasterHistory contactMasterHistory) throws URISyntaxException {
        log.debug("REST request to update ContactMasterHistory : {}", contactMasterHistory);
        if (contactMasterHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactMasterHistory result = contactMasterHistoryService.save(contactMasterHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, contactMasterHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-master-histories} : get all the contactMasterHistories.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactMasterHistories in body.
     */
    @GetMapping("/contact-master-histories")
    public ResponseEntity<List<ContactMasterHistory>> getAllContactMasterHistories(ContactMasterHistoryCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactMasterHistories by criteria: {}", criteria);
        Page<ContactMasterHistory> page = contactMasterHistoryQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-master-histories/count} : count all the contactMasterHistories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-master-histories/count")
    public ResponseEntity<Long> countContactMasterHistories(ContactMasterHistoryCriteria criteria) {
        log.debug("REST request to count ContactMasterHistories by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactMasterHistoryQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-master-histories/:id} : get the "id" contactMasterHistory.
     *
     * @param id the id of the contactMasterHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactMasterHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-master-histories/{id}")
    public ResponseEntity<ContactMasterHistory> getContactMasterHistory(@PathVariable Long id) {
        log.debug("REST request to get ContactMasterHistory : {}", id);
        Optional<ContactMasterHistory> contactMasterHistory = contactMasterHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactMasterHistory);
    }

    /**
     * {@code DELETE  /contact-master-histories/:id} : delete the "id" contactMasterHistory.
     *
     * @param id the id of the contactMasterHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-master-histories/{id}")
    public ResponseEntity<Void> deleteContactMasterHistory(@PathVariable Long id) {
        log.debug("REST request to delete ContactMasterHistory : {}", id);
        contactMasterHistoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
