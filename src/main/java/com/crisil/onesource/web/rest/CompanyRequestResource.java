package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanyRequest;
import com.crisil.onesource.service.CompanyRequestService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanyRequestCriteria;
import com.crisil.onesource.service.CompanyRequestQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanyRequest}.
 */
@RestController
@RequestMapping("/api")
public class CompanyRequestResource {

    private final Logger log = LoggerFactory.getLogger(CompanyRequestResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanyRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyRequestService companyRequestService;

    private final CompanyRequestQueryService companyRequestQueryService;

    public CompanyRequestResource(CompanyRequestService companyRequestService, CompanyRequestQueryService companyRequestQueryService) {
        this.companyRequestService = companyRequestService;
        this.companyRequestQueryService = companyRequestQueryService;
    }

    /**
     * {@code POST  /company-requests} : Create a new companyRequest.
     *
     * @param companyRequest the companyRequest to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyRequest, or with status {@code 400 (Bad Request)} if the companyRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-requests")
    public ResponseEntity<CompanyRequest> createCompanyRequest(@Valid @RequestBody CompanyRequest companyRequest) throws URISyntaxException {
        log.debug("REST request to save CompanyRequest : {}", companyRequest);
        if (companyRequest.getId() != null) {
            throw new BadRequestAlertException("A new companyRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyRequest result = companyRequestService.save(companyRequest);
        return ResponseEntity.created(new URI("/api/company-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-requests} : Updates an existing companyRequest.
     *
     * @param companyRequest the companyRequest to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyRequest,
     * or with status {@code 400 (Bad Request)} if the companyRequest is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyRequest couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-requests")
    public ResponseEntity<CompanyRequest> updateCompanyRequest(@Valid @RequestBody CompanyRequest companyRequest) throws URISyntaxException {
        log.debug("REST request to update CompanyRequest : {}", companyRequest);
        if (companyRequest.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyRequest result = companyRequestService.save(companyRequest);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companyRequest.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-requests} : get all the companyRequests.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyRequests in body.
     */
    @GetMapping("/company-requests")
    public ResponseEntity<List<CompanyRequest>> getAllCompanyRequests(CompanyRequestCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanyRequests by criteria: {}", criteria);
        Page<CompanyRequest> page = companyRequestQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-requests/count} : count all the companyRequests.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-requests/count")
    public ResponseEntity<Long> countCompanyRequests(CompanyRequestCriteria criteria) {
        log.debug("REST request to count CompanyRequests by criteria: {}", criteria);
        return ResponseEntity.ok().body(companyRequestQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-requests/:id} : get the "id" companyRequest.
     *
     * @param id the id of the companyRequest to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyRequest, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-requests/{id}")
    public ResponseEntity<CompanyRequest> getCompanyRequest(@PathVariable Long id) {
        log.debug("REST request to get CompanyRequest : {}", id);
        Optional<CompanyRequest> companyRequest = companyRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyRequest);
    }

    /**
     * {@code DELETE  /company-requests/:id} : delete the "id" companyRequest.
     *
     * @param id the id of the companyRequest to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-requests/{id}")
    public ResponseEntity<Void> deleteCompanyRequest(@PathVariable Long id) {
        log.debug("REST request to delete CompanyRequest : {}", id);
        companyRequestService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
