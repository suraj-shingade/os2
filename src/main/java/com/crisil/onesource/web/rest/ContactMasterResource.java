package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.service.ContactMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.ContactMasterCriteria;
import com.crisil.onesource.service.ContactMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.ContactMaster}.
 */
@RestController
@RequestMapping("/api")
public class ContactMasterResource {

    private final Logger log = LoggerFactory.getLogger(ContactMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceContactMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactMasterService contactMasterService;

    private final ContactMasterQueryService contactMasterQueryService;

    public ContactMasterResource(ContactMasterService contactMasterService, ContactMasterQueryService contactMasterQueryService) {
        this.contactMasterService = contactMasterService;
        this.contactMasterQueryService = contactMasterQueryService;
    }

    /**
     * {@code POST  /contact-masters} : Create a new contactMaster.
     *
     * @param contactMaster the contactMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactMaster, or with status {@code 400 (Bad Request)} if the contactMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-masters")
    public ResponseEntity<ContactMaster> createContactMaster(@Valid @RequestBody ContactMaster contactMaster) throws URISyntaxException {
        log.debug("REST request to save ContactMaster : {}", contactMaster);
        if (contactMaster.getId() != null) {
            throw new BadRequestAlertException("A new contactMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactMaster result = contactMasterService.save(contactMaster);
        return ResponseEntity.created(new URI("/api/contact-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-masters} : Updates an existing contactMaster.
     *
     * @param contactMaster the contactMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactMaster,
     * or with status {@code 400 (Bad Request)} if the contactMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-masters")
    public ResponseEntity<ContactMaster> updateContactMaster(@Valid @RequestBody ContactMaster contactMaster) throws URISyntaxException {
        log.debug("REST request to update ContactMaster : {}", contactMaster);
        if (contactMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactMaster result = contactMasterService.save(contactMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, contactMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-masters} : get all the contactMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactMasters in body.
     */
    @GetMapping("/contact-masters")
    public ResponseEntity<List<ContactMaster>> getAllContactMasters(ContactMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactMasters by criteria: {}", criteria);
        Page<ContactMaster> page = contactMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-masters/count} : count all the contactMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-masters/count")
    public ResponseEntity<Long> countContactMasters(ContactMasterCriteria criteria) {
        log.debug("REST request to count ContactMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-masters/:id} : get the "id" contactMaster.
     *
     * @param id the id of the contactMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-masters/{id}")
    public ResponseEntity<ContactMaster> getContactMaster(@PathVariable Long id) {
        log.debug("REST request to get ContactMaster : {}", id);
        Optional<ContactMaster> contactMaster = contactMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactMaster);
    }

    /**
     * {@code DELETE  /contact-masters/:id} : delete the "id" contactMaster.
     *
     * @param id the id of the contactMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-masters/{id}")
    public ResponseEntity<Void> deleteContactMaster(@PathVariable Long id) {
        log.debug("REST request to delete ContactMaster : {}", id);
        contactMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
