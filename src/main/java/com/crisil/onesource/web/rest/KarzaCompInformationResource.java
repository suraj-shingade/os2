package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.KarzaCompInformation;
import com.crisil.onesource.service.KarzaCompInformationService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.KarzaCompInformationCriteria;
import com.crisil.onesource.service.KarzaCompInformationQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.KarzaCompInformation}.
 */
@RestController
@RequestMapping("/api")
public class KarzaCompInformationResource {

    private final Logger log = LoggerFactory.getLogger(KarzaCompInformationResource.class);

    private static final String ENTITY_NAME = "oneSourceKarzaCompInformation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KarzaCompInformationService karzaCompInformationService;

    private final KarzaCompInformationQueryService karzaCompInformationQueryService;

    public KarzaCompInformationResource(KarzaCompInformationService karzaCompInformationService, KarzaCompInformationQueryService karzaCompInformationQueryService) {
        this.karzaCompInformationService = karzaCompInformationService;
        this.karzaCompInformationQueryService = karzaCompInformationQueryService;
    }

    /**
     * {@code POST  /karza-comp-informations} : Create a new karzaCompInformation.
     *
     * @param karzaCompInformation the karzaCompInformation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new karzaCompInformation, or with status {@code 400 (Bad Request)} if the karzaCompInformation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/karza-comp-informations")
    public ResponseEntity<KarzaCompInformation> createKarzaCompInformation(@Valid @RequestBody KarzaCompInformation karzaCompInformation) throws URISyntaxException {
        log.debug("REST request to save KarzaCompInformation : {}", karzaCompInformation);
        if (karzaCompInformation.getId() != null) {
            throw new BadRequestAlertException("A new karzaCompInformation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        KarzaCompInformation result = karzaCompInformationService.save(karzaCompInformation);
        return ResponseEntity.created(new URI("/api/karza-comp-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /karza-comp-informations} : Updates an existing karzaCompInformation.
     *
     * @param karzaCompInformation the karzaCompInformation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated karzaCompInformation,
     * or with status {@code 400 (Bad Request)} if the karzaCompInformation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the karzaCompInformation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/karza-comp-informations")
    public ResponseEntity<KarzaCompInformation> updateKarzaCompInformation(@Valid @RequestBody KarzaCompInformation karzaCompInformation) throws URISyntaxException {
        log.debug("REST request to update KarzaCompInformation : {}", karzaCompInformation);
        if (karzaCompInformation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        KarzaCompInformation result = karzaCompInformationService.save(karzaCompInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, karzaCompInformation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /karza-comp-informations} : get all the karzaCompInformations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of karzaCompInformations in body.
     */
    @GetMapping("/karza-comp-informations")
    public ResponseEntity<List<KarzaCompInformation>> getAllKarzaCompInformations(KarzaCompInformationCriteria criteria, Pageable pageable) {
        log.debug("REST request to get KarzaCompInformations by criteria: {}", criteria);
        Page<KarzaCompInformation> page = karzaCompInformationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /karza-comp-informations/count} : count all the karzaCompInformations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/karza-comp-informations/count")
    public ResponseEntity<Long> countKarzaCompInformations(KarzaCompInformationCriteria criteria) {
        log.debug("REST request to count KarzaCompInformations by criteria: {}", criteria);
        return ResponseEntity.ok().body(karzaCompInformationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /karza-comp-informations/:id} : get the "id" karzaCompInformation.
     *
     * @param id the id of the karzaCompInformation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the karzaCompInformation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/karza-comp-informations/{id}")
    public ResponseEntity<KarzaCompInformation> getKarzaCompInformation(@PathVariable Long id) {
        log.debug("REST request to get KarzaCompInformation : {}", id);
        Optional<KarzaCompInformation> karzaCompInformation = karzaCompInformationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(karzaCompInformation);
    }

    /**
     * {@code DELETE  /karza-comp-informations/:id} : delete the "id" karzaCompInformation.
     *
     * @param id the id of the karzaCompInformation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/karza-comp-informations/{id}")
    public ResponseEntity<Void> deleteKarzaCompInformation(@PathVariable Long id) {
        log.debug("REST request to delete KarzaCompInformation : {}", id);
        karzaCompInformationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
