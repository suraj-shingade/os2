package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.service.LocCityMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocCityMasterCriteria;
import com.crisil.onesource.service.LocCityMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocCityMaster}.
 */
@RestController
@RequestMapping("/api")
public class LocCityMasterResource {

    private final Logger log = LoggerFactory.getLogger(LocCityMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceLocCityMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocCityMasterService locCityMasterService;

    private final LocCityMasterQueryService locCityMasterQueryService;

    public LocCityMasterResource(LocCityMasterService locCityMasterService, LocCityMasterQueryService locCityMasterQueryService) {
        this.locCityMasterService = locCityMasterService;
        this.locCityMasterQueryService = locCityMasterQueryService;
    }

    /**
     * {@code POST  /loc-city-masters} : Create a new locCityMaster.
     *
     * @param locCityMaster the locCityMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locCityMaster, or with status {@code 400 (Bad Request)} if the locCityMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-city-masters")
    public ResponseEntity<LocCityMaster> createLocCityMaster(@Valid @RequestBody LocCityMaster locCityMaster) throws URISyntaxException {
        log.debug("REST request to save LocCityMaster : {}", locCityMaster);
        if (locCityMaster.getId() != null) {
            throw new BadRequestAlertException("A new locCityMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocCityMaster result = locCityMasterService.save(locCityMaster);
        return ResponseEntity.created(new URI("/api/loc-city-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-city-masters} : Updates an existing locCityMaster.
     *
     * @param locCityMaster the locCityMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locCityMaster,
     * or with status {@code 400 (Bad Request)} if the locCityMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locCityMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-city-masters")
    public ResponseEntity<LocCityMaster> updateLocCityMaster(@Valid @RequestBody LocCityMaster locCityMaster) throws URISyntaxException {
        log.debug("REST request to update LocCityMaster : {}", locCityMaster);
        if (locCityMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocCityMaster result = locCityMasterService.save(locCityMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locCityMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-city-masters} : get all the locCityMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locCityMasters in body.
     */
    @GetMapping("/loc-city-masters")
    public ResponseEntity<List<LocCityMaster>> getAllLocCityMasters(LocCityMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocCityMasters by criteria: {}", criteria);
        Page<LocCityMaster> page = locCityMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-city-masters/count} : count all the locCityMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-city-masters/count")
    public ResponseEntity<Long> countLocCityMasters(LocCityMasterCriteria criteria) {
        log.debug("REST request to count LocCityMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(locCityMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-city-masters/:id} : get the "id" locCityMaster.
     *
     * @param id the id of the locCityMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locCityMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-city-masters/{id}")
    public ResponseEntity<LocCityMaster> getLocCityMaster(@PathVariable Long id) {
        log.debug("REST request to get LocCityMaster : {}", id);
        Optional<LocCityMaster> locCityMaster = locCityMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locCityMaster);
    }

    /**
     * {@code DELETE  /loc-city-masters/:id} : delete the "id" locCityMaster.
     *
     * @param id the id of the locCityMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-city-masters/{id}")
    public ResponseEntity<Void> deleteLocCityMaster(@PathVariable Long id) {
        log.debug("REST request to delete LocCityMaster : {}", id);
        locCityMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
