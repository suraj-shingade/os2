package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocRegionMaster;
import com.crisil.onesource.service.LocRegionMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocRegionMasterCriteria;
import com.crisil.onesource.service.LocRegionMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocRegionMaster}.
 */
@RestController
@RequestMapping("/api")
public class LocRegionMasterResource {

    private final Logger log = LoggerFactory.getLogger(LocRegionMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceLocRegionMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocRegionMasterService locRegionMasterService;

    private final LocRegionMasterQueryService locRegionMasterQueryService;

    public LocRegionMasterResource(LocRegionMasterService locRegionMasterService, LocRegionMasterQueryService locRegionMasterQueryService) {
        this.locRegionMasterService = locRegionMasterService;
        this.locRegionMasterQueryService = locRegionMasterQueryService;
    }

    /**
     * {@code POST  /loc-region-masters} : Create a new locRegionMaster.
     *
     * @param locRegionMaster the locRegionMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locRegionMaster, or with status {@code 400 (Bad Request)} if the locRegionMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-region-masters")
    public ResponseEntity<LocRegionMaster> createLocRegionMaster(@Valid @RequestBody LocRegionMaster locRegionMaster) throws URISyntaxException {
        log.debug("REST request to save LocRegionMaster : {}", locRegionMaster);
        if (locRegionMaster.getId() != null) {
            throw new BadRequestAlertException("A new locRegionMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocRegionMaster result = locRegionMasterService.save(locRegionMaster);
        return ResponseEntity.created(new URI("/api/loc-region-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-region-masters} : Updates an existing locRegionMaster.
     *
     * @param locRegionMaster the locRegionMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locRegionMaster,
     * or with status {@code 400 (Bad Request)} if the locRegionMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locRegionMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-region-masters")
    public ResponseEntity<LocRegionMaster> updateLocRegionMaster(@Valid @RequestBody LocRegionMaster locRegionMaster) throws URISyntaxException {
        log.debug("REST request to update LocRegionMaster : {}", locRegionMaster);
        if (locRegionMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocRegionMaster result = locRegionMasterService.save(locRegionMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locRegionMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-region-masters} : get all the locRegionMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locRegionMasters in body.
     */
    @GetMapping("/loc-region-masters")
    public ResponseEntity<List<LocRegionMaster>> getAllLocRegionMasters(LocRegionMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocRegionMasters by criteria: {}", criteria);
        Page<LocRegionMaster> page = locRegionMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-region-masters/count} : count all the locRegionMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-region-masters/count")
    public ResponseEntity<Long> countLocRegionMasters(LocRegionMasterCriteria criteria) {
        log.debug("REST request to count LocRegionMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(locRegionMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-region-masters/:id} : get the "id" locRegionMaster.
     *
     * @param id the id of the locRegionMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locRegionMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-region-masters/{id}")
    public ResponseEntity<LocRegionMaster> getLocRegionMaster(@PathVariable Long id) {
        log.debug("REST request to get LocRegionMaster : {}", id);
        Optional<LocRegionMaster> locRegionMaster = locRegionMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locRegionMaster);
    }

    /**
     * {@code DELETE  /loc-region-masters/:id} : delete the "id" locRegionMaster.
     *
     * @param id the id of the locRegionMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-region-masters/{id}")
    public ResponseEntity<Void> deleteLocRegionMaster(@PathVariable Long id) {
        log.debug("REST request to delete LocRegionMaster : {}", id);
        locRegionMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
