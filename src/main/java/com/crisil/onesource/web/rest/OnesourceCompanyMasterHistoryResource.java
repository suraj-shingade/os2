package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.OnesourceCompanyMasterHistory;
import com.crisil.onesource.service.OnesourceCompanyMasterHistoryService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterHistoryCriteria;
import com.crisil.onesource.service.OnesourceCompanyMasterHistoryQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.OnesourceCompanyMasterHistory}.
 */
@RestController
@RequestMapping("/api")
public class OnesourceCompanyMasterHistoryResource {

    private final Logger log = LoggerFactory.getLogger(OnesourceCompanyMasterHistoryResource.class);

    private static final String ENTITY_NAME = "oneSourceOnesourceCompanyMasterHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OnesourceCompanyMasterHistoryService onesourceCompanyMasterHistoryService;

    private final OnesourceCompanyMasterHistoryQueryService onesourceCompanyMasterHistoryQueryService;

    public OnesourceCompanyMasterHistoryResource(OnesourceCompanyMasterHistoryService onesourceCompanyMasterHistoryService, OnesourceCompanyMasterHistoryQueryService onesourceCompanyMasterHistoryQueryService) {
        this.onesourceCompanyMasterHistoryService = onesourceCompanyMasterHistoryService;
        this.onesourceCompanyMasterHistoryQueryService = onesourceCompanyMasterHistoryQueryService;
    }

    /**
     * {@code POST  /onesource-company-master-histories} : Create a new onesourceCompanyMasterHistory.
     *
     * @param onesourceCompanyMasterHistory the onesourceCompanyMasterHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new onesourceCompanyMasterHistory, or with status {@code 400 (Bad Request)} if the onesourceCompanyMasterHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/onesource-company-master-histories")
    public ResponseEntity<OnesourceCompanyMasterHistory> createOnesourceCompanyMasterHistory(@Valid @RequestBody OnesourceCompanyMasterHistory onesourceCompanyMasterHistory) throws URISyntaxException {
        log.debug("REST request to save OnesourceCompanyMasterHistory : {}", onesourceCompanyMasterHistory);
        if (onesourceCompanyMasterHistory.getId() != null) {
            throw new BadRequestAlertException("A new onesourceCompanyMasterHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OnesourceCompanyMasterHistory result = onesourceCompanyMasterHistoryService.save(onesourceCompanyMasterHistory);
        return ResponseEntity.created(new URI("/api/onesource-company-master-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /onesource-company-master-histories} : Updates an existing onesourceCompanyMasterHistory.
     *
     * @param onesourceCompanyMasterHistory the onesourceCompanyMasterHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated onesourceCompanyMasterHistory,
     * or with status {@code 400 (Bad Request)} if the onesourceCompanyMasterHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the onesourceCompanyMasterHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/onesource-company-master-histories")
    public ResponseEntity<OnesourceCompanyMasterHistory> updateOnesourceCompanyMasterHistory(@Valid @RequestBody OnesourceCompanyMasterHistory onesourceCompanyMasterHistory) throws URISyntaxException {
        log.debug("REST request to update OnesourceCompanyMasterHistory : {}", onesourceCompanyMasterHistory);
        if (onesourceCompanyMasterHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OnesourceCompanyMasterHistory result = onesourceCompanyMasterHistoryService.save(onesourceCompanyMasterHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, onesourceCompanyMasterHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /onesource-company-master-histories} : get all the onesourceCompanyMasterHistories.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of onesourceCompanyMasterHistories in body.
     */
    @GetMapping("/onesource-company-master-histories")
    public ResponseEntity<List<OnesourceCompanyMasterHistory>> getAllOnesourceCompanyMasterHistories(OnesourceCompanyMasterHistoryCriteria criteria, Pageable pageable) {
        log.debug("REST request to get OnesourceCompanyMasterHistories by criteria: {}", criteria);
        Page<OnesourceCompanyMasterHistory> page = onesourceCompanyMasterHistoryQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /onesource-company-master-histories/count} : count all the onesourceCompanyMasterHistories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/onesource-company-master-histories/count")
    public ResponseEntity<Long> countOnesourceCompanyMasterHistories(OnesourceCompanyMasterHistoryCriteria criteria) {
        log.debug("REST request to count OnesourceCompanyMasterHistories by criteria: {}", criteria);
        return ResponseEntity.ok().body(onesourceCompanyMasterHistoryQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /onesource-company-master-histories/:id} : get the "id" onesourceCompanyMasterHistory.
     *
     * @param id the id of the onesourceCompanyMasterHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the onesourceCompanyMasterHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/onesource-company-master-histories/{id}")
    public ResponseEntity<OnesourceCompanyMasterHistory> getOnesourceCompanyMasterHistory(@PathVariable Long id) {
        log.debug("REST request to get OnesourceCompanyMasterHistory : {}", id);
        Optional<OnesourceCompanyMasterHistory> onesourceCompanyMasterHistory = onesourceCompanyMasterHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(onesourceCompanyMasterHistory);
    }

    /**
     * {@code DELETE  /onesource-company-master-histories/:id} : delete the "id" onesourceCompanyMasterHistory.
     *
     * @param id the id of the onesourceCompanyMasterHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/onesource-company-master-histories/{id}")
    public ResponseEntity<Void> deleteOnesourceCompanyMasterHistory(@PathVariable Long id) {
        log.debug("REST request to delete OnesourceCompanyMasterHistory : {}", id);
        onesourceCompanyMasterHistoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
