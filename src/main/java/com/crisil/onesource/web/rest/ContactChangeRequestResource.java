package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.ContactChangeRequest;
import com.crisil.onesource.service.ContactChangeRequestService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.ContactChangeRequestCriteria;
import com.crisil.onesource.service.ContactChangeRequestQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.ContactChangeRequest}.
 */
@RestController
@RequestMapping("/api")
public class ContactChangeRequestResource {

    private final Logger log = LoggerFactory.getLogger(ContactChangeRequestResource.class);

    private static final String ENTITY_NAME = "oneSourceContactChangeRequest";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactChangeRequestService contactChangeRequestService;

    private final ContactChangeRequestQueryService contactChangeRequestQueryService;

    public ContactChangeRequestResource(ContactChangeRequestService contactChangeRequestService, ContactChangeRequestQueryService contactChangeRequestQueryService) {
        this.contactChangeRequestService = contactChangeRequestService;
        this.contactChangeRequestQueryService = contactChangeRequestQueryService;
    }

    /**
     * {@code POST  /contact-change-requests} : Create a new contactChangeRequest.
     *
     * @param contactChangeRequest the contactChangeRequest to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactChangeRequest, or with status {@code 400 (Bad Request)} if the contactChangeRequest has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-change-requests")
    public ResponseEntity<ContactChangeRequest> createContactChangeRequest(@Valid @RequestBody ContactChangeRequest contactChangeRequest) throws URISyntaxException {
        log.debug("REST request to save ContactChangeRequest : {}", contactChangeRequest);
        if (contactChangeRequest.getId() != null) {
            throw new BadRequestAlertException("A new contactChangeRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactChangeRequest result = contactChangeRequestService.save(contactChangeRequest);
        return ResponseEntity.created(new URI("/api/contact-change-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-change-requests} : Updates an existing contactChangeRequest.
     *
     * @param contactChangeRequest the contactChangeRequest to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactChangeRequest,
     * or with status {@code 400 (Bad Request)} if the contactChangeRequest is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactChangeRequest couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-change-requests")
    public ResponseEntity<ContactChangeRequest> updateContactChangeRequest(@Valid @RequestBody ContactChangeRequest contactChangeRequest) throws URISyntaxException {
        log.debug("REST request to update ContactChangeRequest : {}", contactChangeRequest);
        if (contactChangeRequest.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactChangeRequest result = contactChangeRequestService.save(contactChangeRequest);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, contactChangeRequest.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-change-requests} : get all the contactChangeRequests.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactChangeRequests in body.
     */
    @GetMapping("/contact-change-requests")
    public ResponseEntity<List<ContactChangeRequest>> getAllContactChangeRequests(ContactChangeRequestCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactChangeRequests by criteria: {}", criteria);
        Page<ContactChangeRequest> page = contactChangeRequestQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-change-requests/count} : count all the contactChangeRequests.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-change-requests/count")
    public ResponseEntity<Long> countContactChangeRequests(ContactChangeRequestCriteria criteria) {
        log.debug("REST request to count ContactChangeRequests by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactChangeRequestQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-change-requests/:id} : get the "id" contactChangeRequest.
     *
     * @param id the id of the contactChangeRequest to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactChangeRequest, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-change-requests/{id}")
    public ResponseEntity<ContactChangeRequest> getContactChangeRequest(@PathVariable Long id) {
        log.debug("REST request to get ContactChangeRequest : {}", id);
        Optional<ContactChangeRequest> contactChangeRequest = contactChangeRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactChangeRequest);
    }

    /**
     * {@code DELETE  /contact-change-requests/:id} : delete the "id" contactChangeRequest.
     *
     * @param id the id of the contactChangeRequest to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-change-requests/{id}")
    public ResponseEntity<Void> deleteContactChangeRequest(@PathVariable Long id) {
        log.debug("REST request to delete ContactChangeRequest : {}", id);
        contactChangeRequestService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
