package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanyTanPanRocDtls;
import com.crisil.onesource.service.CompanyTanPanRocDtlsService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanyTanPanRocDtlsCriteria;
import com.crisil.onesource.service.CompanyTanPanRocDtlsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanyTanPanRocDtls}.
 */
@RestController
@RequestMapping("/api")
public class CompanyTanPanRocDtlsResource {

    private final Logger log = LoggerFactory.getLogger(CompanyTanPanRocDtlsResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanyTanPanRocDtls";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyTanPanRocDtlsService companyTanPanRocDtlsService;

    private final CompanyTanPanRocDtlsQueryService companyTanPanRocDtlsQueryService;

    public CompanyTanPanRocDtlsResource(CompanyTanPanRocDtlsService companyTanPanRocDtlsService, CompanyTanPanRocDtlsQueryService companyTanPanRocDtlsQueryService) {
        this.companyTanPanRocDtlsService = companyTanPanRocDtlsService;
        this.companyTanPanRocDtlsQueryService = companyTanPanRocDtlsQueryService;
    }

    /**
     * {@code POST  /company-tan-pan-roc-dtls} : Create a new companyTanPanRocDtls.
     *
     * @param companyTanPanRocDtls the companyTanPanRocDtls to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyTanPanRocDtls, or with status {@code 400 (Bad Request)} if the companyTanPanRocDtls has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-tan-pan-roc-dtls")
    public ResponseEntity<CompanyTanPanRocDtls> createCompanyTanPanRocDtls(@Valid @RequestBody CompanyTanPanRocDtls companyTanPanRocDtls) throws URISyntaxException {
        log.debug("REST request to save CompanyTanPanRocDtls : {}", companyTanPanRocDtls);
        if (companyTanPanRocDtls.getId() != null) {
            throw new BadRequestAlertException("A new companyTanPanRocDtls cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyTanPanRocDtls result = companyTanPanRocDtlsService.save(companyTanPanRocDtls);
        return ResponseEntity.created(new URI("/api/company-tan-pan-roc-dtls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-tan-pan-roc-dtls} : Updates an existing companyTanPanRocDtls.
     *
     * @param companyTanPanRocDtls the companyTanPanRocDtls to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyTanPanRocDtls,
     * or with status {@code 400 (Bad Request)} if the companyTanPanRocDtls is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyTanPanRocDtls couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-tan-pan-roc-dtls")
    public ResponseEntity<CompanyTanPanRocDtls> updateCompanyTanPanRocDtls(@Valid @RequestBody CompanyTanPanRocDtls companyTanPanRocDtls) throws URISyntaxException {
        log.debug("REST request to update CompanyTanPanRocDtls : {}", companyTanPanRocDtls);
        if (companyTanPanRocDtls.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyTanPanRocDtls result = companyTanPanRocDtlsService.save(companyTanPanRocDtls);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companyTanPanRocDtls.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-tan-pan-roc-dtls} : get all the companyTanPanRocDtls.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyTanPanRocDtls in body.
     */
    @GetMapping("/company-tan-pan-roc-dtls")
    public ResponseEntity<List<CompanyTanPanRocDtls>> getAllCompanyTanPanRocDtls(CompanyTanPanRocDtlsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanyTanPanRocDtls by criteria: {}", criteria);
        Page<CompanyTanPanRocDtls> page = companyTanPanRocDtlsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-tan-pan-roc-dtls/count} : count all the companyTanPanRocDtls.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-tan-pan-roc-dtls/count")
    public ResponseEntity<Long> countCompanyTanPanRocDtls(CompanyTanPanRocDtlsCriteria criteria) {
        log.debug("REST request to count CompanyTanPanRocDtls by criteria: {}", criteria);
        return ResponseEntity.ok().body(companyTanPanRocDtlsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-tan-pan-roc-dtls/:id} : get the "id" companyTanPanRocDtls.
     *
     * @param id the id of the companyTanPanRocDtls to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyTanPanRocDtls, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-tan-pan-roc-dtls/{id}")
    public ResponseEntity<CompanyTanPanRocDtls> getCompanyTanPanRocDtls(@PathVariable Long id) {
        log.debug("REST request to get CompanyTanPanRocDtls : {}", id);
        Optional<CompanyTanPanRocDtls> companyTanPanRocDtls = companyTanPanRocDtlsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyTanPanRocDtls);
    }

    /**
     * {@code DELETE  /company-tan-pan-roc-dtls/:id} : delete the "id" companyTanPanRocDtls.
     *
     * @param id the id of the companyTanPanRocDtls to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-tan-pan-roc-dtls/{id}")
    public ResponseEntity<Void> deleteCompanyTanPanRocDtls(@PathVariable Long id) {
        log.debug("REST request to delete CompanyTanPanRocDtls : {}", id);
        companyTanPanRocDtlsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
