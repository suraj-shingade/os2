package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.service.LocPincodeMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocPincodeMasterCriteria;
import com.crisil.onesource.service.LocPincodeMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocPincodeMaster}.
 */
@RestController
@RequestMapping("/api")
public class LocPincodeMasterResource {

    private final Logger log = LoggerFactory.getLogger(LocPincodeMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceLocPincodeMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocPincodeMasterService locPincodeMasterService;

    private final LocPincodeMasterQueryService locPincodeMasterQueryService;

    public LocPincodeMasterResource(LocPincodeMasterService locPincodeMasterService, LocPincodeMasterQueryService locPincodeMasterQueryService) {
        this.locPincodeMasterService = locPincodeMasterService;
        this.locPincodeMasterQueryService = locPincodeMasterQueryService;
    }

    /**
     * {@code POST  /loc-pincode-masters} : Create a new locPincodeMaster.
     *
     * @param locPincodeMaster the locPincodeMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locPincodeMaster, or with status {@code 400 (Bad Request)} if the locPincodeMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-pincode-masters")
    public ResponseEntity<LocPincodeMaster> createLocPincodeMaster(@Valid @RequestBody LocPincodeMaster locPincodeMaster) throws URISyntaxException {
        log.debug("REST request to save LocPincodeMaster : {}", locPincodeMaster);
        if (locPincodeMaster.getId() != null) {
            throw new BadRequestAlertException("A new locPincodeMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocPincodeMaster result = locPincodeMasterService.save(locPincodeMaster);
        return ResponseEntity.created(new URI("/api/loc-pincode-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-pincode-masters} : Updates an existing locPincodeMaster.
     *
     * @param locPincodeMaster the locPincodeMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locPincodeMaster,
     * or with status {@code 400 (Bad Request)} if the locPincodeMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locPincodeMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-pincode-masters")
    public ResponseEntity<LocPincodeMaster> updateLocPincodeMaster(@Valid @RequestBody LocPincodeMaster locPincodeMaster) throws URISyntaxException {
        log.debug("REST request to update LocPincodeMaster : {}", locPincodeMaster);
        if (locPincodeMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocPincodeMaster result = locPincodeMasterService.save(locPincodeMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locPincodeMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-pincode-masters} : get all the locPincodeMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locPincodeMasters in body.
     */
    @GetMapping("/loc-pincode-masters")
    public ResponseEntity<List<LocPincodeMaster>> getAllLocPincodeMasters(LocPincodeMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocPincodeMasters by criteria: {}", criteria);
        Page<LocPincodeMaster> page = locPincodeMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-pincode-masters/count} : count all the locPincodeMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-pincode-masters/count")
    public ResponseEntity<Long> countLocPincodeMasters(LocPincodeMasterCriteria criteria) {
        log.debug("REST request to count LocPincodeMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(locPincodeMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-pincode-masters/:id} : get the "id" locPincodeMaster.
     *
     * @param id the id of the locPincodeMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locPincodeMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-pincode-masters/{id}")
    public ResponseEntity<LocPincodeMaster> getLocPincodeMaster(@PathVariable Long id) {
        log.debug("REST request to get LocPincodeMaster : {}", id);
        Optional<LocPincodeMaster> locPincodeMaster = locPincodeMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locPincodeMaster);
    }

    /**
     * {@code DELETE  /loc-pincode-masters/:id} : delete the "id" locPincodeMaster.
     *
     * @param id the id of the locPincodeMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-pincode-masters/{id}")
    public ResponseEntity<Void> deleteLocPincodeMaster(@PathVariable Long id) {
        log.debug("REST request to delete LocPincodeMaster : {}", id);
        locPincodeMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
