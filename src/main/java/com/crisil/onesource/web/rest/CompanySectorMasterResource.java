package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanySectorMaster;
import com.crisil.onesource.service.CompanySectorMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanySectorMasterCriteria;
import com.crisil.onesource.service.CompanySectorMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanySectorMaster}.
 */
@RestController
@RequestMapping("/api")
public class CompanySectorMasterResource {

    private final Logger log = LoggerFactory.getLogger(CompanySectorMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanySectorMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanySectorMasterService companySectorMasterService;

    private final CompanySectorMasterQueryService companySectorMasterQueryService;

    public CompanySectorMasterResource(CompanySectorMasterService companySectorMasterService, CompanySectorMasterQueryService companySectorMasterQueryService) {
        this.companySectorMasterService = companySectorMasterService;
        this.companySectorMasterQueryService = companySectorMasterQueryService;
    }

    /**
     * {@code POST  /company-sector-masters} : Create a new companySectorMaster.
     *
     * @param companySectorMaster the companySectorMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companySectorMaster, or with status {@code 400 (Bad Request)} if the companySectorMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-sector-masters")
    public ResponseEntity<CompanySectorMaster> createCompanySectorMaster(@Valid @RequestBody CompanySectorMaster companySectorMaster) throws URISyntaxException {
        log.debug("REST request to save CompanySectorMaster : {}", companySectorMaster);
        if (companySectorMaster.getId() != null) {
            throw new BadRequestAlertException("A new companySectorMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanySectorMaster result = companySectorMasterService.save(companySectorMaster);
        return ResponseEntity.created(new URI("/api/company-sector-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-sector-masters} : Updates an existing companySectorMaster.
     *
     * @param companySectorMaster the companySectorMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companySectorMaster,
     * or with status {@code 400 (Bad Request)} if the companySectorMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companySectorMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-sector-masters")
    public ResponseEntity<CompanySectorMaster> updateCompanySectorMaster(@Valid @RequestBody CompanySectorMaster companySectorMaster) throws URISyntaxException {
        log.debug("REST request to update CompanySectorMaster : {}", companySectorMaster);
        if (companySectorMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanySectorMaster result = companySectorMasterService.save(companySectorMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companySectorMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-sector-masters} : get all the companySectorMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companySectorMasters in body.
     */
    @GetMapping("/company-sector-masters")
    public ResponseEntity<List<CompanySectorMaster>> getAllCompanySectorMasters(CompanySectorMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanySectorMasters by criteria: {}", criteria);
        Page<CompanySectorMaster> page = companySectorMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-sector-masters/count} : count all the companySectorMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-sector-masters/count")
    public ResponseEntity<Long> countCompanySectorMasters(CompanySectorMasterCriteria criteria) {
        log.debug("REST request to count CompanySectorMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(companySectorMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-sector-masters/:id} : get the "id" companySectorMaster.
     *
     * @param id the id of the companySectorMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companySectorMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-sector-masters/{id}")
    public ResponseEntity<CompanySectorMaster> getCompanySectorMaster(@PathVariable Long id) {
        log.debug("REST request to get CompanySectorMaster : {}", id);
        Optional<CompanySectorMaster> companySectorMaster = companySectorMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companySectorMaster);
    }

    /**
     * {@code DELETE  /company-sector-masters/:id} : delete the "id" companySectorMaster.
     *
     * @param id the id of the companySectorMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-sector-masters/{id}")
    public ResponseEntity<Void> deleteCompanySectorMaster(@PathVariable Long id) {
        log.debug("REST request to delete CompanySectorMaster : {}", id);
        companySectorMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
