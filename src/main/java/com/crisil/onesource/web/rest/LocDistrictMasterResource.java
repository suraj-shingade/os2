package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.service.LocDistrictMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocDistrictMasterCriteria;
import com.crisil.onesource.service.LocDistrictMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocDistrictMaster}.
 */
@RestController
@RequestMapping("/api")
public class LocDistrictMasterResource {

    private final Logger log = LoggerFactory.getLogger(LocDistrictMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceLocDistrictMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocDistrictMasterService locDistrictMasterService;

    private final LocDistrictMasterQueryService locDistrictMasterQueryService;

    public LocDistrictMasterResource(LocDistrictMasterService locDistrictMasterService, LocDistrictMasterQueryService locDistrictMasterQueryService) {
        this.locDistrictMasterService = locDistrictMasterService;
        this.locDistrictMasterQueryService = locDistrictMasterQueryService;
    }

    /**
     * {@code POST  /loc-district-masters} : Create a new locDistrictMaster.
     *
     * @param locDistrictMaster the locDistrictMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locDistrictMaster, or with status {@code 400 (Bad Request)} if the locDistrictMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-district-masters")
    public ResponseEntity<LocDistrictMaster> createLocDistrictMaster(@Valid @RequestBody LocDistrictMaster locDistrictMaster) throws URISyntaxException {
        log.debug("REST request to save LocDistrictMaster : {}", locDistrictMaster);
        if (locDistrictMaster.getId() != null) {
            throw new BadRequestAlertException("A new locDistrictMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocDistrictMaster result = locDistrictMasterService.save(locDistrictMaster);
        return ResponseEntity.created(new URI("/api/loc-district-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-district-masters} : Updates an existing locDistrictMaster.
     *
     * @param locDistrictMaster the locDistrictMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locDistrictMaster,
     * or with status {@code 400 (Bad Request)} if the locDistrictMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locDistrictMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-district-masters")
    public ResponseEntity<LocDistrictMaster> updateLocDistrictMaster(@Valid @RequestBody LocDistrictMaster locDistrictMaster) throws URISyntaxException {
        log.debug("REST request to update LocDistrictMaster : {}", locDistrictMaster);
        if (locDistrictMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocDistrictMaster result = locDistrictMasterService.save(locDistrictMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locDistrictMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-district-masters} : get all the locDistrictMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locDistrictMasters in body.
     */
    @GetMapping("/loc-district-masters")
    public ResponseEntity<List<LocDistrictMaster>> getAllLocDistrictMasters(LocDistrictMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocDistrictMasters by criteria: {}", criteria);
        Page<LocDistrictMaster> page = locDistrictMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-district-masters/count} : count all the locDistrictMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-district-masters/count")
    public ResponseEntity<Long> countLocDistrictMasters(LocDistrictMasterCriteria criteria) {
        log.debug("REST request to count LocDistrictMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(locDistrictMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-district-masters/:id} : get the "id" locDistrictMaster.
     *
     * @param id the id of the locDistrictMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locDistrictMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-district-masters/{id}")
    public ResponseEntity<LocDistrictMaster> getLocDistrictMaster(@PathVariable Long id) {
        log.debug("REST request to get LocDistrictMaster : {}", id);
        Optional<LocDistrictMaster> locDistrictMaster = locDistrictMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locDistrictMaster);
    }

    /**
     * {@code DELETE  /loc-district-masters/:id} : delete the "id" locDistrictMaster.
     *
     * @param id the id of the locDistrictMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-district-masters/{id}")
    public ResponseEntity<Void> deleteLocDistrictMaster(@PathVariable Long id) {
        log.debug("REST request to delete LocDistrictMaster : {}", id);
        locDistrictMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
