package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.AprvlCompanyTdsDetails;
import com.crisil.onesource.service.AprvlCompanyTdsDetailsService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.AprvlCompanyTdsDetailsCriteria;
import com.crisil.onesource.service.AprvlCompanyTdsDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.AprvlCompanyTdsDetails}.
 */
@RestController
@RequestMapping("/api")
public class AprvlCompanyTdsDetailsResource {

    private final Logger log = LoggerFactory.getLogger(AprvlCompanyTdsDetailsResource.class);

    private static final String ENTITY_NAME = "oneSourceAprvlCompanyTdsDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AprvlCompanyTdsDetailsService aprvlCompanyTdsDetailsService;

    private final AprvlCompanyTdsDetailsQueryService aprvlCompanyTdsDetailsQueryService;

    public AprvlCompanyTdsDetailsResource(AprvlCompanyTdsDetailsService aprvlCompanyTdsDetailsService, AprvlCompanyTdsDetailsQueryService aprvlCompanyTdsDetailsQueryService) {
        this.aprvlCompanyTdsDetailsService = aprvlCompanyTdsDetailsService;
        this.aprvlCompanyTdsDetailsQueryService = aprvlCompanyTdsDetailsQueryService;
    }

    /**
     * {@code POST  /aprvl-company-tds-details} : Create a new aprvlCompanyTdsDetails.
     *
     * @param aprvlCompanyTdsDetails the aprvlCompanyTdsDetails to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aprvlCompanyTdsDetails, or with status {@code 400 (Bad Request)} if the aprvlCompanyTdsDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/aprvl-company-tds-details")
    public ResponseEntity<AprvlCompanyTdsDetails> createAprvlCompanyTdsDetails(@Valid @RequestBody AprvlCompanyTdsDetails aprvlCompanyTdsDetails) throws URISyntaxException {
        log.debug("REST request to save AprvlCompanyTdsDetails : {}", aprvlCompanyTdsDetails);
        if (aprvlCompanyTdsDetails.getId() != null) {
            throw new BadRequestAlertException("A new aprvlCompanyTdsDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AprvlCompanyTdsDetails result = aprvlCompanyTdsDetailsService.save(aprvlCompanyTdsDetails);
        return ResponseEntity.created(new URI("/api/aprvl-company-tds-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aprvl-company-tds-details} : Updates an existing aprvlCompanyTdsDetails.
     *
     * @param aprvlCompanyTdsDetails the aprvlCompanyTdsDetails to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aprvlCompanyTdsDetails,
     * or with status {@code 400 (Bad Request)} if the aprvlCompanyTdsDetails is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aprvlCompanyTdsDetails couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/aprvl-company-tds-details")
    public ResponseEntity<AprvlCompanyTdsDetails> updateAprvlCompanyTdsDetails(@Valid @RequestBody AprvlCompanyTdsDetails aprvlCompanyTdsDetails) throws URISyntaxException {
        log.debug("REST request to update AprvlCompanyTdsDetails : {}", aprvlCompanyTdsDetails);
        if (aprvlCompanyTdsDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AprvlCompanyTdsDetails result = aprvlCompanyTdsDetailsService.save(aprvlCompanyTdsDetails);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aprvlCompanyTdsDetails.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /aprvl-company-tds-details} : get all the aprvlCompanyTdsDetails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aprvlCompanyTdsDetails in body.
     */
    @GetMapping("/aprvl-company-tds-details")
    public ResponseEntity<List<AprvlCompanyTdsDetails>> getAllAprvlCompanyTdsDetails(AprvlCompanyTdsDetailsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AprvlCompanyTdsDetails by criteria: {}", criteria);
        Page<AprvlCompanyTdsDetails> page = aprvlCompanyTdsDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aprvl-company-tds-details/count} : count all the aprvlCompanyTdsDetails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/aprvl-company-tds-details/count")
    public ResponseEntity<Long> countAprvlCompanyTdsDetails(AprvlCompanyTdsDetailsCriteria criteria) {
        log.debug("REST request to count AprvlCompanyTdsDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(aprvlCompanyTdsDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /aprvl-company-tds-details/:id} : get the "id" aprvlCompanyTdsDetails.
     *
     * @param id the id of the aprvlCompanyTdsDetails to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aprvlCompanyTdsDetails, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/aprvl-company-tds-details/{id}")
    public ResponseEntity<AprvlCompanyTdsDetails> getAprvlCompanyTdsDetails(@PathVariable Long id) {
        log.debug("REST request to get AprvlCompanyTdsDetails : {}", id);
        Optional<AprvlCompanyTdsDetails> aprvlCompanyTdsDetails = aprvlCompanyTdsDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aprvlCompanyTdsDetails);
    }

    /**
     * {@code DELETE  /aprvl-company-tds-details/:id} : delete the "id" aprvlCompanyTdsDetails.
     *
     * @param id the id of the aprvlCompanyTdsDetails to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/aprvl-company-tds-details/{id}")
    public ResponseEntity<Void> deleteAprvlCompanyTdsDetails(@PathVariable Long id) {
        log.debug("REST request to delete AprvlCompanyTdsDetails : {}", id);
        aprvlCompanyTdsDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
