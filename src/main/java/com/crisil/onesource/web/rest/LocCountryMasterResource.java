package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocCountryMaster;
import com.crisil.onesource.service.LocCountryMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocCountryMasterCriteria;
import com.crisil.onesource.service.LocCountryMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocCountryMaster}.
 */
@RestController
@RequestMapping("/api")
public class LocCountryMasterResource {

    private final Logger log = LoggerFactory.getLogger(LocCountryMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceLocCountryMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocCountryMasterService locCountryMasterService;

    private final LocCountryMasterQueryService locCountryMasterQueryService;

    public LocCountryMasterResource(LocCountryMasterService locCountryMasterService, LocCountryMasterQueryService locCountryMasterQueryService) {
        this.locCountryMasterService = locCountryMasterService;
        this.locCountryMasterQueryService = locCountryMasterQueryService;
    }

    /**
     * {@code POST  /loc-country-masters} : Create a new locCountryMaster.
     *
     * @param locCountryMaster the locCountryMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locCountryMaster, or with status {@code 400 (Bad Request)} if the locCountryMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-country-masters")
    public ResponseEntity<LocCountryMaster> createLocCountryMaster(@Valid @RequestBody LocCountryMaster locCountryMaster) throws URISyntaxException {
        log.debug("REST request to save LocCountryMaster : {}", locCountryMaster);
        if (locCountryMaster.getId() != null) {
            throw new BadRequestAlertException("A new locCountryMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocCountryMaster result = locCountryMasterService.save(locCountryMaster);
        return ResponseEntity.created(new URI("/api/loc-country-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-country-masters} : Updates an existing locCountryMaster.
     *
     * @param locCountryMaster the locCountryMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locCountryMaster,
     * or with status {@code 400 (Bad Request)} if the locCountryMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locCountryMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-country-masters")
    public ResponseEntity<LocCountryMaster> updateLocCountryMaster(@Valid @RequestBody LocCountryMaster locCountryMaster) throws URISyntaxException {
        log.debug("REST request to update LocCountryMaster : {}", locCountryMaster);
        if (locCountryMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocCountryMaster result = locCountryMasterService.save(locCountryMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locCountryMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-country-masters} : get all the locCountryMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locCountryMasters in body.
     */
    @GetMapping("/loc-country-masters")
    public ResponseEntity<List<LocCountryMaster>> getAllLocCountryMasters(LocCountryMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocCountryMasters by criteria: {}", criteria);
        Page<LocCountryMaster> page = locCountryMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-country-masters/count} : count all the locCountryMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-country-masters/count")
    public ResponseEntity<Long> countLocCountryMasters(LocCountryMasterCriteria criteria) {
        log.debug("REST request to count LocCountryMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(locCountryMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-country-masters/:id} : get the "id" locCountryMaster.
     *
     * @param id the id of the locCountryMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locCountryMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-country-masters/{id}")
    public ResponseEntity<LocCountryMaster> getLocCountryMaster(@PathVariable Long id) {
        log.debug("REST request to get LocCountryMaster : {}", id);
        Optional<LocCountryMaster> locCountryMaster = locCountryMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locCountryMaster);
    }

    /**
     * {@code DELETE  /loc-country-masters/:id} : delete the "id" locCountryMaster.
     *
     * @param id the id of the locCountryMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-country-masters/{id}")
    public ResponseEntity<Void> deleteLocCountryMaster(@PathVariable Long id) {
        log.debug("REST request to delete LocCountryMaster : {}", id);
        locCountryMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
