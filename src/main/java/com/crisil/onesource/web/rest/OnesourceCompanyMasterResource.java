package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.service.OnesourceCompanyMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterCriteria;
import com.crisil.onesource.service.OnesourceCompanyMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.OnesourceCompanyMaster}.
 */
@RestController
@RequestMapping("/api")
public class OnesourceCompanyMasterResource {

    private final Logger log = LoggerFactory.getLogger(OnesourceCompanyMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceOnesourceCompanyMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OnesourceCompanyMasterService onesourceCompanyMasterService;

    private final OnesourceCompanyMasterQueryService onesourceCompanyMasterQueryService;

    public OnesourceCompanyMasterResource(OnesourceCompanyMasterService onesourceCompanyMasterService, OnesourceCompanyMasterQueryService onesourceCompanyMasterQueryService) {
        this.onesourceCompanyMasterService = onesourceCompanyMasterService;
        this.onesourceCompanyMasterQueryService = onesourceCompanyMasterQueryService;
    }

    /**
     * {@code POST  /onesource-company-masters} : Create a new onesourceCompanyMaster.
     *
     * @param onesourceCompanyMaster the onesourceCompanyMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new onesourceCompanyMaster, or with status {@code 400 (Bad Request)} if the onesourceCompanyMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/onesource-company-masters")
    public ResponseEntity<OnesourceCompanyMaster> createOnesourceCompanyMaster(@Valid @RequestBody OnesourceCompanyMaster onesourceCompanyMaster) throws URISyntaxException {
        log.debug("REST request to save OnesourceCompanyMaster : {}", onesourceCompanyMaster);
        if (onesourceCompanyMaster.getId() != null) {
            throw new BadRequestAlertException("A new onesourceCompanyMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OnesourceCompanyMaster result = onesourceCompanyMasterService.save(onesourceCompanyMaster);
        return ResponseEntity.created(new URI("/api/onesource-company-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /onesource-company-masters} : Updates an existing onesourceCompanyMaster.
     *
     * @param onesourceCompanyMaster the onesourceCompanyMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated onesourceCompanyMaster,
     * or with status {@code 400 (Bad Request)} if the onesourceCompanyMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the onesourceCompanyMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/onesource-company-masters")
    public ResponseEntity<OnesourceCompanyMaster> updateOnesourceCompanyMaster(@Valid @RequestBody OnesourceCompanyMaster onesourceCompanyMaster) throws URISyntaxException {
        log.debug("REST request to update OnesourceCompanyMaster : {}", onesourceCompanyMaster);
        if (onesourceCompanyMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OnesourceCompanyMaster result = onesourceCompanyMasterService.save(onesourceCompanyMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, onesourceCompanyMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /onesource-company-masters} : get all the onesourceCompanyMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of onesourceCompanyMasters in body.
     */
    @GetMapping("/onesource-company-masters")
    public ResponseEntity<List<OnesourceCompanyMaster>> getAllOnesourceCompanyMasters(OnesourceCompanyMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get OnesourceCompanyMasters by criteria: {}", criteria);
        Page<OnesourceCompanyMaster> page = onesourceCompanyMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /onesource-company-masters/count} : count all the onesourceCompanyMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/onesource-company-masters/count")
    public ResponseEntity<Long> countOnesourceCompanyMasters(OnesourceCompanyMasterCriteria criteria) {
        log.debug("REST request to count OnesourceCompanyMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(onesourceCompanyMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /onesource-company-masters/:id} : get the "id" onesourceCompanyMaster.
     *
     * @param id the id of the onesourceCompanyMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the onesourceCompanyMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/onesource-company-masters/{id}")
    public ResponseEntity<OnesourceCompanyMaster> getOnesourceCompanyMaster(@PathVariable Long id) {
        log.debug("REST request to get OnesourceCompanyMaster : {}", id);
        Optional<OnesourceCompanyMaster> onesourceCompanyMaster = onesourceCompanyMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(onesourceCompanyMaster);
    }

    /**
     * {@code DELETE  /onesource-company-masters/:id} : delete the "id" onesourceCompanyMaster.
     *
     * @param id the id of the onesourceCompanyMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/onesource-company-masters/{id}")
    public ResponseEntity<Void> deleteOnesourceCompanyMaster(@PathVariable Long id) {
        log.debug("REST request to delete OnesourceCompanyMaster : {}", id);
        onesourceCompanyMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
