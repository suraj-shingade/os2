package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.AprvlContactMaster;
import com.crisil.onesource.service.AprvlContactMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.AprvlContactMasterCriteria;
import com.crisil.onesource.service.AprvlContactMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.AprvlContactMaster}.
 */
@RestController
@RequestMapping("/api")
public class AprvlContactMasterResource {

    private final Logger log = LoggerFactory.getLogger(AprvlContactMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceAprvlContactMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AprvlContactMasterService aprvlContactMasterService;

    private final AprvlContactMasterQueryService aprvlContactMasterQueryService;

    public AprvlContactMasterResource(AprvlContactMasterService aprvlContactMasterService, AprvlContactMasterQueryService aprvlContactMasterQueryService) {
        this.aprvlContactMasterService = aprvlContactMasterService;
        this.aprvlContactMasterQueryService = aprvlContactMasterQueryService;
    }

    /**
     * {@code POST  /aprvl-contact-masters} : Create a new aprvlContactMaster.
     *
     * @param aprvlContactMaster the aprvlContactMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aprvlContactMaster, or with status {@code 400 (Bad Request)} if the aprvlContactMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/aprvl-contact-masters")
    public ResponseEntity<AprvlContactMaster> createAprvlContactMaster(@Valid @RequestBody AprvlContactMaster aprvlContactMaster) throws URISyntaxException {
        log.debug("REST request to save AprvlContactMaster : {}", aprvlContactMaster);
        if (aprvlContactMaster.getId() != null) {
            throw new BadRequestAlertException("A new aprvlContactMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AprvlContactMaster result = aprvlContactMasterService.save(aprvlContactMaster);
        return ResponseEntity.created(new URI("/api/aprvl-contact-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aprvl-contact-masters} : Updates an existing aprvlContactMaster.
     *
     * @param aprvlContactMaster the aprvlContactMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aprvlContactMaster,
     * or with status {@code 400 (Bad Request)} if the aprvlContactMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aprvlContactMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/aprvl-contact-masters")
    public ResponseEntity<AprvlContactMaster> updateAprvlContactMaster(@Valid @RequestBody AprvlContactMaster aprvlContactMaster) throws URISyntaxException {
        log.debug("REST request to update AprvlContactMaster : {}", aprvlContactMaster);
        if (aprvlContactMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AprvlContactMaster result = aprvlContactMasterService.save(aprvlContactMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aprvlContactMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /aprvl-contact-masters} : get all the aprvlContactMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aprvlContactMasters in body.
     */
    @GetMapping("/aprvl-contact-masters")
    public ResponseEntity<List<AprvlContactMaster>> getAllAprvlContactMasters(AprvlContactMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AprvlContactMasters by criteria: {}", criteria);
        Page<AprvlContactMaster> page = aprvlContactMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aprvl-contact-masters/count} : count all the aprvlContactMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/aprvl-contact-masters/count")
    public ResponseEntity<Long> countAprvlContactMasters(AprvlContactMasterCriteria criteria) {
        log.debug("REST request to count AprvlContactMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(aprvlContactMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /aprvl-contact-masters/:id} : get the "id" aprvlContactMaster.
     *
     * @param id the id of the aprvlContactMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aprvlContactMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/aprvl-contact-masters/{id}")
    public ResponseEntity<AprvlContactMaster> getAprvlContactMaster(@PathVariable Long id) {
        log.debug("REST request to get AprvlContactMaster : {}", id);
        Optional<AprvlContactMaster> aprvlContactMaster = aprvlContactMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aprvlContactMaster);
    }

    /**
     * {@code DELETE  /aprvl-contact-masters/:id} : delete the "id" aprvlContactMaster.
     *
     * @param id the id of the aprvlContactMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/aprvl-contact-masters/{id}")
    public ResponseEntity<Void> deleteAprvlContactMaster(@PathVariable Long id) {
        log.debug("REST request to delete AprvlContactMaster : {}", id);
        aprvlContactMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
