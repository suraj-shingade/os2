/**
 * View Models used by Spring MVC REST controllers.
 */
package com.crisil.onesource.web.rest.vm;
