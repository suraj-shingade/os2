package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanyListingStatus;
import com.crisil.onesource.service.CompanyListingStatusService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanyListingStatusCriteria;
import com.crisil.onesource.service.CompanyListingStatusQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanyListingStatus}.
 */
@RestController
@RequestMapping("/api")
public class CompanyListingStatusResource {

    private final Logger log = LoggerFactory.getLogger(CompanyListingStatusResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanyListingStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyListingStatusService companyListingStatusService;

    private final CompanyListingStatusQueryService companyListingStatusQueryService;

    public CompanyListingStatusResource(CompanyListingStatusService companyListingStatusService, CompanyListingStatusQueryService companyListingStatusQueryService) {
        this.companyListingStatusService = companyListingStatusService;
        this.companyListingStatusQueryService = companyListingStatusQueryService;
    }

    /**
     * {@code POST  /company-listing-statuses} : Create a new companyListingStatus.
     *
     * @param companyListingStatus the companyListingStatus to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyListingStatus, or with status {@code 400 (Bad Request)} if the companyListingStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-listing-statuses")
    public ResponseEntity<CompanyListingStatus> createCompanyListingStatus(@Valid @RequestBody CompanyListingStatus companyListingStatus) throws URISyntaxException {
        log.debug("REST request to save CompanyListingStatus : {}", companyListingStatus);
        if (companyListingStatus.getId() != null) {
            throw new BadRequestAlertException("A new companyListingStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyListingStatus result = companyListingStatusService.save(companyListingStatus);
        return ResponseEntity.created(new URI("/api/company-listing-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-listing-statuses} : Updates an existing companyListingStatus.
     *
     * @param companyListingStatus the companyListingStatus to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyListingStatus,
     * or with status {@code 400 (Bad Request)} if the companyListingStatus is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyListingStatus couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-listing-statuses")
    public ResponseEntity<CompanyListingStatus> updateCompanyListingStatus(@Valid @RequestBody CompanyListingStatus companyListingStatus) throws URISyntaxException {
        log.debug("REST request to update CompanyListingStatus : {}", companyListingStatus);
        if (companyListingStatus.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyListingStatus result = companyListingStatusService.save(companyListingStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companyListingStatus.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-listing-statuses} : get all the companyListingStatuses.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyListingStatuses in body.
     */
    @GetMapping("/company-listing-statuses")
    public ResponseEntity<List<CompanyListingStatus>> getAllCompanyListingStatuses(CompanyListingStatusCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanyListingStatuses by criteria: {}", criteria);
        Page<CompanyListingStatus> page = companyListingStatusQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-listing-statuses/count} : count all the companyListingStatuses.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-listing-statuses/count")
    public ResponseEntity<Long> countCompanyListingStatuses(CompanyListingStatusCriteria criteria) {
        log.debug("REST request to count CompanyListingStatuses by criteria: {}", criteria);
        return ResponseEntity.ok().body(companyListingStatusQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-listing-statuses/:id} : get the "id" companyListingStatus.
     *
     * @param id the id of the companyListingStatus to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyListingStatus, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-listing-statuses/{id}")
    public ResponseEntity<CompanyListingStatus> getCompanyListingStatus(@PathVariable Long id) {
        log.debug("REST request to get CompanyListingStatus : {}", id);
        Optional<CompanyListingStatus> companyListingStatus = companyListingStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyListingStatus);
    }

    /**
     * {@code DELETE  /company-listing-statuses/:id} : delete the "id" companyListingStatus.
     *
     * @param id the id of the companyListingStatus to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-listing-statuses/{id}")
    public ResponseEntity<Void> deleteCompanyListingStatus(@PathVariable Long id) {
        log.debug("REST request to delete CompanyListingStatus : {}", id);
        companyListingStatusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
