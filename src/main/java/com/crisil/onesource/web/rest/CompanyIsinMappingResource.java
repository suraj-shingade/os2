package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanyIsinMapping;
import com.crisil.onesource.service.CompanyIsinMappingService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanyIsinMappingCriteria;
import com.crisil.onesource.service.CompanyIsinMappingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanyIsinMapping}.
 */
@RestController
@RequestMapping("/api")
public class CompanyIsinMappingResource {

    private final Logger log = LoggerFactory.getLogger(CompanyIsinMappingResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanyIsinMapping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyIsinMappingService companyIsinMappingService;

    private final CompanyIsinMappingQueryService companyIsinMappingQueryService;

    public CompanyIsinMappingResource(CompanyIsinMappingService companyIsinMappingService, CompanyIsinMappingQueryService companyIsinMappingQueryService) {
        this.companyIsinMappingService = companyIsinMappingService;
        this.companyIsinMappingQueryService = companyIsinMappingQueryService;
    }

    /**
     * {@code POST  /company-isin-mappings} : Create a new companyIsinMapping.
     *
     * @param companyIsinMapping the companyIsinMapping to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyIsinMapping, or with status {@code 400 (Bad Request)} if the companyIsinMapping has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-isin-mappings")
    public ResponseEntity<CompanyIsinMapping> createCompanyIsinMapping(@Valid @RequestBody CompanyIsinMapping companyIsinMapping) throws URISyntaxException {
        log.debug("REST request to save CompanyIsinMapping : {}", companyIsinMapping);
        if (companyIsinMapping.getId() != null) {
            throw new BadRequestAlertException("A new companyIsinMapping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyIsinMapping result = companyIsinMappingService.save(companyIsinMapping);
        return ResponseEntity.created(new URI("/api/company-isin-mappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-isin-mappings} : Updates an existing companyIsinMapping.
     *
     * @param companyIsinMapping the companyIsinMapping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyIsinMapping,
     * or with status {@code 400 (Bad Request)} if the companyIsinMapping is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyIsinMapping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-isin-mappings")
    public ResponseEntity<CompanyIsinMapping> updateCompanyIsinMapping(@Valid @RequestBody CompanyIsinMapping companyIsinMapping) throws URISyntaxException {
        log.debug("REST request to update CompanyIsinMapping : {}", companyIsinMapping);
        if (companyIsinMapping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyIsinMapping result = companyIsinMappingService.save(companyIsinMapping);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companyIsinMapping.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-isin-mappings} : get all the companyIsinMappings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyIsinMappings in body.
     */
    @GetMapping("/company-isin-mappings")
    public ResponseEntity<List<CompanyIsinMapping>> getAllCompanyIsinMappings(CompanyIsinMappingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanyIsinMappings by criteria: {}", criteria);
        Page<CompanyIsinMapping> page = companyIsinMappingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-isin-mappings/count} : count all the companyIsinMappings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-isin-mappings/count")
    public ResponseEntity<Long> countCompanyIsinMappings(CompanyIsinMappingCriteria criteria) {
        log.debug("REST request to count CompanyIsinMappings by criteria: {}", criteria);
        return ResponseEntity.ok().body(companyIsinMappingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-isin-mappings/:id} : get the "id" companyIsinMapping.
     *
     * @param id the id of the companyIsinMapping to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyIsinMapping, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-isin-mappings/{id}")
    public ResponseEntity<CompanyIsinMapping> getCompanyIsinMapping(@PathVariable Long id) {
        log.debug("REST request to get CompanyIsinMapping : {}", id);
        Optional<CompanyIsinMapping> companyIsinMapping = companyIsinMappingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyIsinMapping);
    }

    /**
     * {@code DELETE  /company-isin-mappings/:id} : delete the "id" companyIsinMapping.
     *
     * @param id the id of the companyIsinMapping to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-isin-mappings/{id}")
    public ResponseEntity<Void> deleteCompanyIsinMapping(@PathVariable Long id) {
        log.debug("REST request to delete CompanyIsinMapping : {}", id);
        companyIsinMappingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
