package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocCityPincodeMapping;
import com.crisil.onesource.service.LocCityPincodeMappingService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocCityPincodeMappingCriteria;
import com.crisil.onesource.service.LocCityPincodeMappingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocCityPincodeMapping}.
 */
@RestController
@RequestMapping("/api")
public class LocCityPincodeMappingResource {

    private final Logger log = LoggerFactory.getLogger(LocCityPincodeMappingResource.class);

    private static final String ENTITY_NAME = "oneSourceLocCityPincodeMapping";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocCityPincodeMappingService locCityPincodeMappingService;

    private final LocCityPincodeMappingQueryService locCityPincodeMappingQueryService;

    public LocCityPincodeMappingResource(LocCityPincodeMappingService locCityPincodeMappingService, LocCityPincodeMappingQueryService locCityPincodeMappingQueryService) {
        this.locCityPincodeMappingService = locCityPincodeMappingService;
        this.locCityPincodeMappingQueryService = locCityPincodeMappingQueryService;
    }

    /**
     * {@code POST  /loc-city-pincode-mappings} : Create a new locCityPincodeMapping.
     *
     * @param locCityPincodeMapping the locCityPincodeMapping to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locCityPincodeMapping, or with status {@code 400 (Bad Request)} if the locCityPincodeMapping has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-city-pincode-mappings")
    public ResponseEntity<LocCityPincodeMapping> createLocCityPincodeMapping(@Valid @RequestBody LocCityPincodeMapping locCityPincodeMapping) throws URISyntaxException {
        log.debug("REST request to save LocCityPincodeMapping : {}", locCityPincodeMapping);
        if (locCityPincodeMapping.getId() != null) {
            throw new BadRequestAlertException("A new locCityPincodeMapping cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocCityPincodeMapping result = locCityPincodeMappingService.save(locCityPincodeMapping);
        return ResponseEntity.created(new URI("/api/loc-city-pincode-mappings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-city-pincode-mappings} : Updates an existing locCityPincodeMapping.
     *
     * @param locCityPincodeMapping the locCityPincodeMapping to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locCityPincodeMapping,
     * or with status {@code 400 (Bad Request)} if the locCityPincodeMapping is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locCityPincodeMapping couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-city-pincode-mappings")
    public ResponseEntity<LocCityPincodeMapping> updateLocCityPincodeMapping(@Valid @RequestBody LocCityPincodeMapping locCityPincodeMapping) throws URISyntaxException {
        log.debug("REST request to update LocCityPincodeMapping : {}", locCityPincodeMapping);
        if (locCityPincodeMapping.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocCityPincodeMapping result = locCityPincodeMappingService.save(locCityPincodeMapping);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locCityPincodeMapping.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-city-pincode-mappings} : get all the locCityPincodeMappings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locCityPincodeMappings in body.
     */
    @GetMapping("/loc-city-pincode-mappings")
    public ResponseEntity<List<LocCityPincodeMapping>> getAllLocCityPincodeMappings(LocCityPincodeMappingCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocCityPincodeMappings by criteria: {}", criteria);
        Page<LocCityPincodeMapping> page = locCityPincodeMappingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-city-pincode-mappings/count} : count all the locCityPincodeMappings.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-city-pincode-mappings/count")
    public ResponseEntity<Long> countLocCityPincodeMappings(LocCityPincodeMappingCriteria criteria) {
        log.debug("REST request to count LocCityPincodeMappings by criteria: {}", criteria);
        return ResponseEntity.ok().body(locCityPincodeMappingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-city-pincode-mappings/:id} : get the "id" locCityPincodeMapping.
     *
     * @param id the id of the locCityPincodeMapping to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locCityPincodeMapping, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-city-pincode-mappings/{id}")
    public ResponseEntity<LocCityPincodeMapping> getLocCityPincodeMapping(@PathVariable Long id) {
        log.debug("REST request to get LocCityPincodeMapping : {}", id);
        Optional<LocCityPincodeMapping> locCityPincodeMapping = locCityPincodeMappingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locCityPincodeMapping);
    }

    /**
     * {@code DELETE  /loc-city-pincode-mappings/:id} : delete the "id" locCityPincodeMapping.
     *
     * @param id the id of the locCityPincodeMapping to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-city-pincode-mappings/{id}")
    public ResponseEntity<Void> deleteLocCityPincodeMapping(@PathVariable Long id) {
        log.debug("REST request to delete LocCityPincodeMapping : {}", id);
        locCityPincodeMappingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
