package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.AprvlCompanyGstDetails;
import com.crisil.onesource.service.AprvlCompanyGstDetailsService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.AprvlCompanyGstDetailsCriteria;
import com.crisil.onesource.service.AprvlCompanyGstDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.AprvlCompanyGstDetails}.
 */
@RestController
@RequestMapping("/api")
public class AprvlCompanyGstDetailsResource {

    private final Logger log = LoggerFactory.getLogger(AprvlCompanyGstDetailsResource.class);

    private static final String ENTITY_NAME = "oneSourceAprvlCompanyGstDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AprvlCompanyGstDetailsService aprvlCompanyGstDetailsService;

    private final AprvlCompanyGstDetailsQueryService aprvlCompanyGstDetailsQueryService;

    public AprvlCompanyGstDetailsResource(AprvlCompanyGstDetailsService aprvlCompanyGstDetailsService, AprvlCompanyGstDetailsQueryService aprvlCompanyGstDetailsQueryService) {
        this.aprvlCompanyGstDetailsService = aprvlCompanyGstDetailsService;
        this.aprvlCompanyGstDetailsQueryService = aprvlCompanyGstDetailsQueryService;
    }

    /**
     * {@code POST  /aprvl-company-gst-details} : Create a new aprvlCompanyGstDetails.
     *
     * @param aprvlCompanyGstDetails the aprvlCompanyGstDetails to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new aprvlCompanyGstDetails, or with status {@code 400 (Bad Request)} if the aprvlCompanyGstDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/aprvl-company-gst-details")
    public ResponseEntity<AprvlCompanyGstDetails> createAprvlCompanyGstDetails(@Valid @RequestBody AprvlCompanyGstDetails aprvlCompanyGstDetails) throws URISyntaxException {
        log.debug("REST request to save AprvlCompanyGstDetails : {}", aprvlCompanyGstDetails);
        if (aprvlCompanyGstDetails.getId() != null) {
            throw new BadRequestAlertException("A new aprvlCompanyGstDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AprvlCompanyGstDetails result = aprvlCompanyGstDetailsService.save(aprvlCompanyGstDetails);
        return ResponseEntity.created(new URI("/api/aprvl-company-gst-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aprvl-company-gst-details} : Updates an existing aprvlCompanyGstDetails.
     *
     * @param aprvlCompanyGstDetails the aprvlCompanyGstDetails to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated aprvlCompanyGstDetails,
     * or with status {@code 400 (Bad Request)} if the aprvlCompanyGstDetails is not valid,
     * or with status {@code 500 (Internal Server Error)} if the aprvlCompanyGstDetails couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/aprvl-company-gst-details")
    public ResponseEntity<AprvlCompanyGstDetails> updateAprvlCompanyGstDetails(@Valid @RequestBody AprvlCompanyGstDetails aprvlCompanyGstDetails) throws URISyntaxException {
        log.debug("REST request to update AprvlCompanyGstDetails : {}", aprvlCompanyGstDetails);
        if (aprvlCompanyGstDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AprvlCompanyGstDetails result = aprvlCompanyGstDetailsService.save(aprvlCompanyGstDetails);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, aprvlCompanyGstDetails.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /aprvl-company-gst-details} : get all the aprvlCompanyGstDetails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aprvlCompanyGstDetails in body.
     */
    @GetMapping("/aprvl-company-gst-details")
    public ResponseEntity<List<AprvlCompanyGstDetails>> getAllAprvlCompanyGstDetails(AprvlCompanyGstDetailsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get AprvlCompanyGstDetails by criteria: {}", criteria);
        Page<AprvlCompanyGstDetails> page = aprvlCompanyGstDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aprvl-company-gst-details/count} : count all the aprvlCompanyGstDetails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/aprvl-company-gst-details/count")
    public ResponseEntity<Long> countAprvlCompanyGstDetails(AprvlCompanyGstDetailsCriteria criteria) {
        log.debug("REST request to count AprvlCompanyGstDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(aprvlCompanyGstDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /aprvl-company-gst-details/:id} : get the "id" aprvlCompanyGstDetails.
     *
     * @param id the id of the aprvlCompanyGstDetails to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the aprvlCompanyGstDetails, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/aprvl-company-gst-details/{id}")
    public ResponseEntity<AprvlCompanyGstDetails> getAprvlCompanyGstDetails(@PathVariable Long id) {
        log.debug("REST request to get AprvlCompanyGstDetails : {}", id);
        Optional<AprvlCompanyGstDetails> aprvlCompanyGstDetails = aprvlCompanyGstDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(aprvlCompanyGstDetails);
    }

    /**
     * {@code DELETE  /aprvl-company-gst-details/:id} : delete the "id" aprvlCompanyGstDetails.
     *
     * @param id the id of the aprvlCompanyGstDetails to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/aprvl-company-gst-details/{id}")
    public ResponseEntity<Void> deleteAprvlCompanyGstDetails(@PathVariable Long id) {
        log.debug("REST request to delete AprvlCompanyGstDetails : {}", id);
        aprvlCompanyGstDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
