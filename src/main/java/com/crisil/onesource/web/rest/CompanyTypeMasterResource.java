package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanyTypeMaster;
import com.crisil.onesource.service.CompanyTypeMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanyTypeMasterCriteria;
import com.crisil.onesource.service.CompanyTypeMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanyTypeMaster}.
 */
@RestController
@RequestMapping("/api")
public class CompanyTypeMasterResource {

    private final Logger log = LoggerFactory.getLogger(CompanyTypeMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanyTypeMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanyTypeMasterService companyTypeMasterService;

    private final CompanyTypeMasterQueryService companyTypeMasterQueryService;

    public CompanyTypeMasterResource(CompanyTypeMasterService companyTypeMasterService, CompanyTypeMasterQueryService companyTypeMasterQueryService) {
        this.companyTypeMasterService = companyTypeMasterService;
        this.companyTypeMasterQueryService = companyTypeMasterQueryService;
    }

    /**
     * {@code POST  /company-type-masters} : Create a new companyTypeMaster.
     *
     * @param companyTypeMaster the companyTypeMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companyTypeMaster, or with status {@code 400 (Bad Request)} if the companyTypeMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-type-masters")
    public ResponseEntity<CompanyTypeMaster> createCompanyTypeMaster(@Valid @RequestBody CompanyTypeMaster companyTypeMaster) throws URISyntaxException {
        log.debug("REST request to save CompanyTypeMaster : {}", companyTypeMaster);
        if (companyTypeMaster.getId() != null) {
            throw new BadRequestAlertException("A new companyTypeMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanyTypeMaster result = companyTypeMasterService.save(companyTypeMaster);
        return ResponseEntity.created(new URI("/api/company-type-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-type-masters} : Updates an existing companyTypeMaster.
     *
     * @param companyTypeMaster the companyTypeMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companyTypeMaster,
     * or with status {@code 400 (Bad Request)} if the companyTypeMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companyTypeMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-type-masters")
    public ResponseEntity<CompanyTypeMaster> updateCompanyTypeMaster(@Valid @RequestBody CompanyTypeMaster companyTypeMaster) throws URISyntaxException {
        log.debug("REST request to update CompanyTypeMaster : {}", companyTypeMaster);
        if (companyTypeMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanyTypeMaster result = companyTypeMasterService.save(companyTypeMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companyTypeMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-type-masters} : get all the companyTypeMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companyTypeMasters in body.
     */
    @GetMapping("/company-type-masters")
    public ResponseEntity<List<CompanyTypeMaster>> getAllCompanyTypeMasters(CompanyTypeMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanyTypeMasters by criteria: {}", criteria);
        Page<CompanyTypeMaster> page = companyTypeMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-type-masters/count} : count all the companyTypeMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-type-masters/count")
    public ResponseEntity<Long> countCompanyTypeMasters(CompanyTypeMasterCriteria criteria) {
        log.debug("REST request to count CompanyTypeMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(companyTypeMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-type-masters/:id} : get the "id" companyTypeMaster.
     *
     * @param id the id of the companyTypeMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companyTypeMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-type-masters/{id}")
    public ResponseEntity<CompanyTypeMaster> getCompanyTypeMaster(@PathVariable Long id) {
        log.debug("REST request to get CompanyTypeMaster : {}", id);
        Optional<CompanyTypeMaster> companyTypeMaster = companyTypeMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companyTypeMaster);
    }

    /**
     * {@code DELETE  /company-type-masters/:id} : delete the "id" companyTypeMaster.
     *
     * @param id the id of the companyTypeMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-type-masters/{id}")
    public ResponseEntity<Void> deleteCompanyTypeMaster(@PathVariable Long id) {
        log.debug("REST request to delete CompanyTypeMaster : {}", id);
        companyTypeMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
