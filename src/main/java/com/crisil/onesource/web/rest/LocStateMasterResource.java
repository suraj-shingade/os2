package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.service.LocStateMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.LocStateMasterCriteria;
import com.crisil.onesource.service.LocStateMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.LocStateMaster}.
 */
@RestController
@RequestMapping("/api")
public class LocStateMasterResource {

    private final Logger log = LoggerFactory.getLogger(LocStateMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceLocStateMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LocStateMasterService locStateMasterService;

    private final LocStateMasterQueryService locStateMasterQueryService;

    public LocStateMasterResource(LocStateMasterService locStateMasterService, LocStateMasterQueryService locStateMasterQueryService) {
        this.locStateMasterService = locStateMasterService;
        this.locStateMasterQueryService = locStateMasterQueryService;
    }

    /**
     * {@code POST  /loc-state-masters} : Create a new locStateMaster.
     *
     * @param locStateMaster the locStateMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new locStateMaster, or with status {@code 400 (Bad Request)} if the locStateMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/loc-state-masters")
    public ResponseEntity<LocStateMaster> createLocStateMaster(@Valid @RequestBody LocStateMaster locStateMaster) throws URISyntaxException {
        log.debug("REST request to save LocStateMaster : {}", locStateMaster);
        if (locStateMaster.getId() != null) {
            throw new BadRequestAlertException("A new locStateMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LocStateMaster result = locStateMasterService.save(locStateMaster);
        return ResponseEntity.created(new URI("/api/loc-state-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /loc-state-masters} : Updates an existing locStateMaster.
     *
     * @param locStateMaster the locStateMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated locStateMaster,
     * or with status {@code 400 (Bad Request)} if the locStateMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the locStateMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/loc-state-masters")
    public ResponseEntity<LocStateMaster> updateLocStateMaster(@Valid @RequestBody LocStateMaster locStateMaster) throws URISyntaxException {
        log.debug("REST request to update LocStateMaster : {}", locStateMaster);
        if (locStateMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LocStateMaster result = locStateMasterService.save(locStateMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, locStateMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /loc-state-masters} : get all the locStateMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of locStateMasters in body.
     */
    @GetMapping("/loc-state-masters")
    public ResponseEntity<List<LocStateMaster>> getAllLocStateMasters(LocStateMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get LocStateMasters by criteria: {}", criteria);
        Page<LocStateMaster> page = locStateMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /loc-state-masters/count} : count all the locStateMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/loc-state-masters/count")
    public ResponseEntity<Long> countLocStateMasters(LocStateMasterCriteria criteria) {
        log.debug("REST request to count LocStateMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(locStateMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /loc-state-masters/:id} : get the "id" locStateMaster.
     *
     * @param id the id of the locStateMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the locStateMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/loc-state-masters/{id}")
    public ResponseEntity<LocStateMaster> getLocStateMaster(@PathVariable Long id) {
        log.debug("REST request to get LocStateMaster : {}", id);
        Optional<LocStateMaster> locStateMaster = locStateMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(locStateMaster);
    }

    /**
     * {@code DELETE  /loc-state-masters/:id} : delete the "id" locStateMaster.
     *
     * @param id the id of the locStateMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/loc-state-masters/{id}")
    public ResponseEntity<Void> deleteLocStateMaster(@PathVariable Long id) {
        log.debug("REST request to delete LocStateMaster : {}", id);
        locStateMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
