package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompanySegmentMaster;
import com.crisil.onesource.service.CompanySegmentMasterService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompanySegmentMasterCriteria;
import com.crisil.onesource.service.CompanySegmentMasterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompanySegmentMaster}.
 */
@RestController
@RequestMapping("/api")
public class CompanySegmentMasterResource {

    private final Logger log = LoggerFactory.getLogger(CompanySegmentMasterResource.class);

    private static final String ENTITY_NAME = "oneSourceCompanySegmentMaster";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompanySegmentMasterService companySegmentMasterService;

    private final CompanySegmentMasterQueryService companySegmentMasterQueryService;

    public CompanySegmentMasterResource(CompanySegmentMasterService companySegmentMasterService, CompanySegmentMasterQueryService companySegmentMasterQueryService) {
        this.companySegmentMasterService = companySegmentMasterService;
        this.companySegmentMasterQueryService = companySegmentMasterQueryService;
    }

    /**
     * {@code POST  /company-segment-masters} : Create a new companySegmentMaster.
     *
     * @param companySegmentMaster the companySegmentMaster to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new companySegmentMaster, or with status {@code 400 (Bad Request)} if the companySegmentMaster has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/company-segment-masters")
    public ResponseEntity<CompanySegmentMaster> createCompanySegmentMaster(@Valid @RequestBody CompanySegmentMaster companySegmentMaster) throws URISyntaxException {
        log.debug("REST request to save CompanySegmentMaster : {}", companySegmentMaster);
        if (companySegmentMaster.getId() != null) {
            throw new BadRequestAlertException("A new companySegmentMaster cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompanySegmentMaster result = companySegmentMasterService.save(companySegmentMaster);
        return ResponseEntity.created(new URI("/api/company-segment-masters/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /company-segment-masters} : Updates an existing companySegmentMaster.
     *
     * @param companySegmentMaster the companySegmentMaster to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated companySegmentMaster,
     * or with status {@code 400 (Bad Request)} if the companySegmentMaster is not valid,
     * or with status {@code 500 (Internal Server Error)} if the companySegmentMaster couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/company-segment-masters")
    public ResponseEntity<CompanySegmentMaster> updateCompanySegmentMaster(@Valid @RequestBody CompanySegmentMaster companySegmentMaster) throws URISyntaxException {
        log.debug("REST request to update CompanySegmentMaster : {}", companySegmentMaster);
        if (companySegmentMaster.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompanySegmentMaster result = companySegmentMasterService.save(companySegmentMaster);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, companySegmentMaster.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /company-segment-masters} : get all the companySegmentMasters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of companySegmentMasters in body.
     */
    @GetMapping("/company-segment-masters")
    public ResponseEntity<List<CompanySegmentMaster>> getAllCompanySegmentMasters(CompanySegmentMasterCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompanySegmentMasters by criteria: {}", criteria);
        Page<CompanySegmentMaster> page = companySegmentMasterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /company-segment-masters/count} : count all the companySegmentMasters.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/company-segment-masters/count")
    public ResponseEntity<Long> countCompanySegmentMasters(CompanySegmentMasterCriteria criteria) {
        log.debug("REST request to count CompanySegmentMasters by criteria: {}", criteria);
        return ResponseEntity.ok().body(companySegmentMasterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /company-segment-masters/:id} : get the "id" companySegmentMaster.
     *
     * @param id the id of the companySegmentMaster to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the companySegmentMaster, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/company-segment-masters/{id}")
    public ResponseEntity<CompanySegmentMaster> getCompanySegmentMaster(@PathVariable Long id) {
        log.debug("REST request to get CompanySegmentMaster : {}", id);
        Optional<CompanySegmentMaster> companySegmentMaster = companySegmentMasterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(companySegmentMaster);
    }

    /**
     * {@code DELETE  /company-segment-masters/:id} : delete the "id" companySegmentMaster.
     *
     * @param id the id of the companySegmentMaster to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/company-segment-masters/{id}")
    public ResponseEntity<Void> deleteCompanySegmentMaster(@PathVariable Long id) {
        log.debug("REST request to delete CompanySegmentMaster : {}", id);
        companySegmentMasterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
