package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.CompGstNotApplicable;
import com.crisil.onesource.service.CompGstNotApplicableService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.CompGstNotApplicableCriteria;
import com.crisil.onesource.service.CompGstNotApplicableQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.CompGstNotApplicable}.
 */
@RestController
@RequestMapping("/api")
public class CompGstNotApplicableResource {

    private final Logger log = LoggerFactory.getLogger(CompGstNotApplicableResource.class);

    private static final String ENTITY_NAME = "oneSourceCompGstNotApplicable";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompGstNotApplicableService compGstNotApplicableService;

    private final CompGstNotApplicableQueryService compGstNotApplicableQueryService;

    public CompGstNotApplicableResource(CompGstNotApplicableService compGstNotApplicableService, CompGstNotApplicableQueryService compGstNotApplicableQueryService) {
        this.compGstNotApplicableService = compGstNotApplicableService;
        this.compGstNotApplicableQueryService = compGstNotApplicableQueryService;
    }

    /**
     * {@code POST  /comp-gst-not-applicables} : Create a new compGstNotApplicable.
     *
     * @param compGstNotApplicable the compGstNotApplicable to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new compGstNotApplicable, or with status {@code 400 (Bad Request)} if the compGstNotApplicable has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/comp-gst-not-applicables")
    public ResponseEntity<CompGstNotApplicable> createCompGstNotApplicable(@Valid @RequestBody CompGstNotApplicable compGstNotApplicable) throws URISyntaxException {
        log.debug("REST request to save CompGstNotApplicable : {}", compGstNotApplicable);
        if (compGstNotApplicable.getId() != null) {
            throw new BadRequestAlertException("A new compGstNotApplicable cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompGstNotApplicable result = compGstNotApplicableService.save(compGstNotApplicable);
        return ResponseEntity.created(new URI("/api/comp-gst-not-applicables/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /comp-gst-not-applicables} : Updates an existing compGstNotApplicable.
     *
     * @param compGstNotApplicable the compGstNotApplicable to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compGstNotApplicable,
     * or with status {@code 400 (Bad Request)} if the compGstNotApplicable is not valid,
     * or with status {@code 500 (Internal Server Error)} if the compGstNotApplicable couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/comp-gst-not-applicables")
    public ResponseEntity<CompGstNotApplicable> updateCompGstNotApplicable(@Valid @RequestBody CompGstNotApplicable compGstNotApplicable) throws URISyntaxException {
        log.debug("REST request to update CompGstNotApplicable : {}", compGstNotApplicable);
        if (compGstNotApplicable.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompGstNotApplicable result = compGstNotApplicableService.save(compGstNotApplicable);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, compGstNotApplicable.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /comp-gst-not-applicables} : get all the compGstNotApplicables.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of compGstNotApplicables in body.
     */
    @GetMapping("/comp-gst-not-applicables")
    public ResponseEntity<List<CompGstNotApplicable>> getAllCompGstNotApplicables(CompGstNotApplicableCriteria criteria, Pageable pageable) {
        log.debug("REST request to get CompGstNotApplicables by criteria: {}", criteria);
        Page<CompGstNotApplicable> page = compGstNotApplicableQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /comp-gst-not-applicables/count} : count all the compGstNotApplicables.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/comp-gst-not-applicables/count")
    public ResponseEntity<Long> countCompGstNotApplicables(CompGstNotApplicableCriteria criteria) {
        log.debug("REST request to count CompGstNotApplicables by criteria: {}", criteria);
        return ResponseEntity.ok().body(compGstNotApplicableQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /comp-gst-not-applicables/:id} : get the "id" compGstNotApplicable.
     *
     * @param id the id of the compGstNotApplicable to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the compGstNotApplicable, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/comp-gst-not-applicables/{id}")
    public ResponseEntity<CompGstNotApplicable> getCompGstNotApplicable(@PathVariable Long id) {
        log.debug("REST request to get CompGstNotApplicable : {}", id);
        Optional<CompGstNotApplicable> compGstNotApplicable = compGstNotApplicableService.findOne(id);
        return ResponseUtil.wrapOrNotFound(compGstNotApplicable);
    }

    /**
     * {@code DELETE  /comp-gst-not-applicables/:id} : delete the "id" compGstNotApplicable.
     *
     * @param id the id of the compGstNotApplicable to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/comp-gst-not-applicables/{id}")
    public ResponseEntity<Void> deleteCompGstNotApplicable(@PathVariable Long id) {
        log.debug("REST request to delete CompGstNotApplicable : {}", id);
        compGstNotApplicableService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
