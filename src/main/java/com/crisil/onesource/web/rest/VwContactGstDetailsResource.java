package com.crisil.onesource.web.rest;

import com.crisil.onesource.domain.VwContactGstDetails;
import com.crisil.onesource.service.VwContactGstDetailsService;
import com.crisil.onesource.web.rest.errors.BadRequestAlertException;
import com.crisil.onesource.service.dto.VwContactGstDetailsCriteria;
import com.crisil.onesource.service.VwContactGstDetailsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.crisil.onesource.domain.VwContactGstDetails}.
 */
@RestController
@RequestMapping("/api")
public class VwContactGstDetailsResource {

    private final Logger log = LoggerFactory.getLogger(VwContactGstDetailsResource.class);

    private static final String ENTITY_NAME = "oneSourceVwContactGstDetails";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VwContactGstDetailsService vwContactGstDetailsService;

    private final VwContactGstDetailsQueryService vwContactGstDetailsQueryService;

    public VwContactGstDetailsResource(VwContactGstDetailsService vwContactGstDetailsService, VwContactGstDetailsQueryService vwContactGstDetailsQueryService) {
        this.vwContactGstDetailsService = vwContactGstDetailsService;
        this.vwContactGstDetailsQueryService = vwContactGstDetailsQueryService;
    }

    /**
     * {@code POST  /vw-contact-gst-details} : Create a new vwContactGstDetails.
     *
     * @param vwContactGstDetails the vwContactGstDetails to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new vwContactGstDetails, or with status {@code 400 (Bad Request)} if the vwContactGstDetails has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/vw-contact-gst-details")
    public ResponseEntity<VwContactGstDetails> createVwContactGstDetails(@Valid @RequestBody VwContactGstDetails vwContactGstDetails) throws URISyntaxException {
        log.debug("REST request to save VwContactGstDetails : {}", vwContactGstDetails);
        if (vwContactGstDetails.getId() != null) {
            throw new BadRequestAlertException("A new vwContactGstDetails cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VwContactGstDetails result = vwContactGstDetailsService.save(vwContactGstDetails);
        return ResponseEntity.created(new URI("/api/vw-contact-gst-details/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /vw-contact-gst-details} : Updates an existing vwContactGstDetails.
     *
     * @param vwContactGstDetails the vwContactGstDetails to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated vwContactGstDetails,
     * or with status {@code 400 (Bad Request)} if the vwContactGstDetails is not valid,
     * or with status {@code 500 (Internal Server Error)} if the vwContactGstDetails couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/vw-contact-gst-details")
    public ResponseEntity<VwContactGstDetails> updateVwContactGstDetails(@Valid @RequestBody VwContactGstDetails vwContactGstDetails) throws URISyntaxException {
        log.debug("REST request to update VwContactGstDetails : {}", vwContactGstDetails);
        if (vwContactGstDetails.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        VwContactGstDetails result = vwContactGstDetailsService.save(vwContactGstDetails);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, vwContactGstDetails.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /vw-contact-gst-details} : get all the vwContactGstDetails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of vwContactGstDetails in body.
     */
    @GetMapping("/vw-contact-gst-details")
    public ResponseEntity<List<VwContactGstDetails>> getAllVwContactGstDetails(VwContactGstDetailsCriteria criteria, Pageable pageable) {
        log.debug("REST request to get VwContactGstDetails by criteria: {}", criteria);
        Page<VwContactGstDetails> page = vwContactGstDetailsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /vw-contact-gst-details/count} : count all the vwContactGstDetails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/vw-contact-gst-details/count")
    public ResponseEntity<Long> countVwContactGstDetails(VwContactGstDetailsCriteria criteria) {
        log.debug("REST request to count VwContactGstDetails by criteria: {}", criteria);
        return ResponseEntity.ok().body(vwContactGstDetailsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /vw-contact-gst-details/:id} : get the "id" vwContactGstDetails.
     *
     * @param id the id of the vwContactGstDetails to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the vwContactGstDetails, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/vw-contact-gst-details/{id}")
    public ResponseEntity<VwContactGstDetails> getVwContactGstDetails(@PathVariable Long id) {
        log.debug("REST request to get VwContactGstDetails : {}", id);
        Optional<VwContactGstDetails> vwContactGstDetails = vwContactGstDetailsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(vwContactGstDetails);
    }

    /**
     * {@code DELETE  /vw-contact-gst-details/:id} : delete the "id" vwContactGstDetails.
     *
     * @param id the id of the vwContactGstDetails to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/vw-contact-gst-details/{id}")
    public ResponseEntity<Void> deleteVwContactGstDetails(@PathVariable Long id) {
        log.debug("REST request to delete VwContactGstDetails : {}", id);
        vwContactGstDetailsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
