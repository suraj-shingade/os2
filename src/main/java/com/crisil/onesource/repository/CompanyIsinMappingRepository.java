package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyIsinMapping;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyIsinMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyIsinMappingRepository extends JpaRepository<CompanyIsinMapping, Long>, JpaSpecificationExecutor<CompanyIsinMapping> {
}
