package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyChangeRequest;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyChangeRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyChangeRequestRepository extends JpaRepository<CompanyChangeRequest, Long>, JpaSpecificationExecutor<CompanyChangeRequest> {
}
