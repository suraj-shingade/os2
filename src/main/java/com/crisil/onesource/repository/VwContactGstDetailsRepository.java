package com.crisil.onesource.repository;

import com.crisil.onesource.domain.VwContactGstDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the VwContactGstDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VwContactGstDetailsRepository extends JpaRepository<VwContactGstDetails, Long>, JpaSpecificationExecutor<VwContactGstDetails> {
}
