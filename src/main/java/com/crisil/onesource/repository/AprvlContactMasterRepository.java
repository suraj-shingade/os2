package com.crisil.onesource.repository;

import com.crisil.onesource.domain.AprvlContactMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AprvlContactMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AprvlContactMasterRepository extends JpaRepository<AprvlContactMaster, Long>, JpaSpecificationExecutor<AprvlContactMaster> {
}
