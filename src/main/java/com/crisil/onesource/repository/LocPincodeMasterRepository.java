package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocPincodeMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocPincodeMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocPincodeMasterRepository extends JpaRepository<LocPincodeMaster, Long>, JpaSpecificationExecutor<LocPincodeMaster> {
}
