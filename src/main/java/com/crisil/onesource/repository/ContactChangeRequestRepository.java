package com.crisil.onesource.repository;

import com.crisil.onesource.domain.ContactChangeRequest;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactChangeRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactChangeRequestRepository extends JpaRepository<ContactChangeRequest, Long>, JpaSpecificationExecutor<ContactChangeRequest> {
}
