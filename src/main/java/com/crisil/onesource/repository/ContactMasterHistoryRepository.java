package com.crisil.onesource.repository;

import com.crisil.onesource.domain.ContactMasterHistory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactMasterHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactMasterHistoryRepository extends JpaRepository<ContactMasterHistory, Long>, JpaSpecificationExecutor<ContactMasterHistory> {
}
