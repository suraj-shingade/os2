package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocCountryMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocCountryMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocCountryMasterRepository extends JpaRepository<LocCountryMaster, Long>, JpaSpecificationExecutor<LocCountryMaster> {
}
