package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyTypeMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyTypeMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyTypeMasterRepository extends JpaRepository<CompanyTypeMaster, Long>, JpaSpecificationExecutor<CompanyTypeMaster> {
}
