package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyAddressMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyAddressMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyAddressMasterRepository extends JpaRepository<CompanyAddressMaster, Long>, JpaSpecificationExecutor<CompanyAddressMaster> {
}
