package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanySegmentMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanySegmentMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanySegmentMasterRepository extends JpaRepository<CompanySegmentMaster, Long>, JpaSpecificationExecutor<CompanySegmentMaster> {
}
