package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocDistrictMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocDistrictMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocDistrictMasterRepository extends JpaRepository<LocDistrictMaster, Long>, JpaSpecificationExecutor<LocDistrictMaster> {
}
