package com.crisil.onesource.repository;

import com.crisil.onesource.domain.GroupMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the GroupMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GroupMasterRepository extends JpaRepository<GroupMaster, Long>, JpaSpecificationExecutor<GroupMaster> {
}
