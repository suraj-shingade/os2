package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocRegionMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocRegionMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocRegionMasterRepository extends JpaRepository<LocRegionMaster, Long>, JpaSpecificationExecutor<LocRegionMaster> {
}
