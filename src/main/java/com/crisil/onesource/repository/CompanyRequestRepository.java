package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyRequest;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyRequestRepository extends JpaRepository<CompanyRequest, Long>, JpaSpecificationExecutor<CompanyRequest> {
}
