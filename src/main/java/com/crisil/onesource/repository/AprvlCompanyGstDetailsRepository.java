package com.crisil.onesource.repository;

import com.crisil.onesource.domain.AprvlCompanyGstDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AprvlCompanyGstDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AprvlCompanyGstDetailsRepository extends JpaRepository<AprvlCompanyGstDetails, Long>, JpaSpecificationExecutor<AprvlCompanyGstDetails> {
}
