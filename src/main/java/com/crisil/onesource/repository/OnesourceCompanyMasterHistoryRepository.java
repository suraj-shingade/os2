package com.crisil.onesource.repository;

import com.crisil.onesource.domain.OnesourceCompanyMasterHistory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OnesourceCompanyMasterHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OnesourceCompanyMasterHistoryRepository extends JpaRepository<OnesourceCompanyMasterHistory, Long>, JpaSpecificationExecutor<OnesourceCompanyMasterHistory> {
}
