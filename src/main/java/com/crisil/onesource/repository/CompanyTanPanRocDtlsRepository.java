package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyTanPanRocDtls;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyTanPanRocDtls entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyTanPanRocDtlsRepository extends JpaRepository<CompanyTanPanRocDtls, Long>, JpaSpecificationExecutor<CompanyTanPanRocDtls> {
}
