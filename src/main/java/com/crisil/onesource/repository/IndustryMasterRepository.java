package com.crisil.onesource.repository;

import com.crisil.onesource.domain.IndustryMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the IndustryMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndustryMasterRepository extends JpaRepository<IndustryMaster, Long>, JpaSpecificationExecutor<IndustryMaster> {
}
