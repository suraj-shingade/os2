package com.crisil.onesource.repository;

import com.crisil.onesource.domain.AprvlCompanyTdsDetails;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AprvlCompanyTdsDetails entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AprvlCompanyTdsDetailsRepository extends JpaRepository<AprvlCompanyTdsDetails, Long>, JpaSpecificationExecutor<AprvlCompanyTdsDetails> {
}
