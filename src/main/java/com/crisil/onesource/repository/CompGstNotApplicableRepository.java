package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompGstNotApplicable;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompGstNotApplicable entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompGstNotApplicableRepository extends JpaRepository<CompGstNotApplicable, Long>, JpaSpecificationExecutor<CompGstNotApplicable> {
}
