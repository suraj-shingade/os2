package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanyListingStatus;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanyListingStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyListingStatusRepository extends JpaRepository<CompanyListingStatus, Long>, JpaSpecificationExecutor<CompanyListingStatus> {
}
