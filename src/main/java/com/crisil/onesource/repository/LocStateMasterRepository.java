package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocStateMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocStateMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocStateMasterRepository extends JpaRepository<LocStateMaster, Long>, JpaSpecificationExecutor<LocStateMaster> {
}
