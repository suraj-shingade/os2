package com.crisil.onesource.repository;

import com.crisil.onesource.domain.BusinessAreaMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BusinessAreaMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BusinessAreaMasterRepository extends JpaRepository<BusinessAreaMaster, Long>, JpaSpecificationExecutor<BusinessAreaMaster> {
}
