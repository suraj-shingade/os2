package com.crisil.onesource.repository;

import com.crisil.onesource.domain.KarzaCompInformation;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the KarzaCompInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KarzaCompInformationRepository extends JpaRepository<KarzaCompInformation, Long>, JpaSpecificationExecutor<KarzaCompInformation> {
}
