package com.crisil.onesource.repository;

import com.crisil.onesource.domain.ContactMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactMasterRepository extends JpaRepository<ContactMaster, Long>, JpaSpecificationExecutor<ContactMaster> {
}
