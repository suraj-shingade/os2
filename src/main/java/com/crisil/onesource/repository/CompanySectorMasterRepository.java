package com.crisil.onesource.repository;

import com.crisil.onesource.domain.CompanySectorMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CompanySectorMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanySectorMasterRepository extends JpaRepository<CompanySectorMaster, Long>, JpaSpecificationExecutor<CompanySectorMaster> {
}
