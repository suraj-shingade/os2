package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocCityPincodeMapping;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocCityPincodeMapping entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocCityPincodeMappingRepository extends JpaRepository<LocCityPincodeMapping, Long>, JpaSpecificationExecutor<LocCityPincodeMapping> {
}
