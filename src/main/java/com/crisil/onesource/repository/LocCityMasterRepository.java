package com.crisil.onesource.repository;

import com.crisil.onesource.domain.LocCityMaster;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the LocCityMaster entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LocCityMasterRepository extends JpaRepository<LocCityMaster, Long>, JpaSpecificationExecutor<LocCityMaster> {
}
