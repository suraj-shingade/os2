package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A KarzaCompInformation.
 */
@Entity
@Table(name = "karza_comp_information")
public class KarzaCompInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 15)
    @Column(name = "pan", length = 15)
    private String pan;

    @Size(max = 100)
    @Column(name = "company_name_karza", length = 100)
    private String companyNameKarza;

    @Size(max = 500)
    @Column(name = "address", length = 500)
    private String address;

    @Size(max = 100)
    @Column(name = "typeofapproval", length = 100)
    private String typeofapproval;

    @Size(max = 100)
    @Column(name = "status", length = 100)
    private String status;

    @Size(max = 100)
    @Column(name = "gstin", length = 100)
    private String gstin;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 50)
    @Column(name = "state", length = 50)
    private String state;

    @Size(max = 1)
    @Column(name = "is_valid_gst", length = 1)
    private String isValidGst;

    @Size(max = 20)
    @Column(name = "source", length = 20)
    private String source;

    @Size(max = 500)
    @Column(name = "comments", length = 500)
    private String comments;

    @Size(max = 100)
    @Column(name = "checkdigit", length = 100)
    private String checkdigit;

    @Size(max = 100)
    @Column(name = "taxpayer_name", length = 100)
    private String taxpayerName;

    @Size(max = 200)
    @Column(name = "natute_of_buss", length = 200)
    private String natuteOfBuss;

    @Size(max = 15)
    @Column(name = "mobile_no", length = 15)
    private String mobileNo;

    @Size(max = 50)
    @Column(name = "email", length = 50)
    private String email;

    @Size(max = 100)
    @Column(name = "trade_name", length = 100)
    private String tradeName;

    @Size(max = 50)
    @Column(name = "constit_business", length = 50)
    private String constitBusiness;

    @ManyToOne
    @JsonIgnoreProperties(value = "karzaCompInformations", allowSetters = true)
    private OnesourceCompanyMaster companyCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPan() {
        return pan;
    }

    public KarzaCompInformation pan(String pan) {
        this.pan = pan;
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCompanyNameKarza() {
        return companyNameKarza;
    }

    public KarzaCompInformation companyNameKarza(String companyNameKarza) {
        this.companyNameKarza = companyNameKarza;
        return this;
    }

    public void setCompanyNameKarza(String companyNameKarza) {
        this.companyNameKarza = companyNameKarza;
    }

    public String getAddress() {
        return address;
    }

    public KarzaCompInformation address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeofapproval() {
        return typeofapproval;
    }

    public KarzaCompInformation typeofapproval(String typeofapproval) {
        this.typeofapproval = typeofapproval;
        return this;
    }

    public void setTypeofapproval(String typeofapproval) {
        this.typeofapproval = typeofapproval;
    }

    public String getStatus() {
        return status;
    }

    public KarzaCompInformation status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGstin() {
        return gstin;
    }

    public KarzaCompInformation gstin(String gstin) {
        this.gstin = gstin;
        return this;
    }

    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public KarzaCompInformation createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public KarzaCompInformation createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public KarzaCompInformation lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public KarzaCompInformation lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public KarzaCompInformation isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getState() {
        return state;
    }

    public KarzaCompInformation state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsValidGst() {
        return isValidGst;
    }

    public KarzaCompInformation isValidGst(String isValidGst) {
        this.isValidGst = isValidGst;
        return this;
    }

    public void setIsValidGst(String isValidGst) {
        this.isValidGst = isValidGst;
    }

    public String getSource() {
        return source;
    }

    public KarzaCompInformation source(String source) {
        this.source = source;
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getComments() {
        return comments;
    }

    public KarzaCompInformation comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCheckdigit() {
        return checkdigit;
    }

    public KarzaCompInformation checkdigit(String checkdigit) {
        this.checkdigit = checkdigit;
        return this;
    }

    public void setCheckdigit(String checkdigit) {
        this.checkdigit = checkdigit;
    }

    public String getTaxpayerName() {
        return taxpayerName;
    }

    public KarzaCompInformation taxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
        return this;
    }

    public void setTaxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
    }

    public String getNatuteOfBuss() {
        return natuteOfBuss;
    }

    public KarzaCompInformation natuteOfBuss(String natuteOfBuss) {
        this.natuteOfBuss = natuteOfBuss;
        return this;
    }

    public void setNatuteOfBuss(String natuteOfBuss) {
        this.natuteOfBuss = natuteOfBuss;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public KarzaCompInformation mobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public KarzaCompInformation email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTradeName() {
        return tradeName;
    }

    public KarzaCompInformation tradeName(String tradeName) {
        this.tradeName = tradeName;
        return this;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getConstitBusiness() {
        return constitBusiness;
    }

    public KarzaCompInformation constitBusiness(String constitBusiness) {
        this.constitBusiness = constitBusiness;
        return this;
    }

    public void setConstitBusiness(String constitBusiness) {
        this.constitBusiness = constitBusiness;
    }

    public OnesourceCompanyMaster getCompanyCode() {
        return companyCode;
    }

    public KarzaCompInformation companyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.companyCode = onesourceCompanyMaster;
        return this;
    }

    public void setCompanyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.companyCode = onesourceCompanyMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KarzaCompInformation)) {
            return false;
        }
        return id != null && id.equals(((KarzaCompInformation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KarzaCompInformation{" +
            "id=" + getId() +
            ", pan='" + getPan() + "'" +
            ", companyNameKarza='" + getCompanyNameKarza() + "'" +
            ", address='" + getAddress() + "'" +
            ", typeofapproval='" + getTypeofapproval() + "'" +
            ", status='" + getStatus() + "'" +
            ", gstin='" + getGstin() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", state='" + getState() + "'" +
            ", isValidGst='" + getIsValidGst() + "'" +
            ", source='" + getSource() + "'" +
            ", comments='" + getComments() + "'" +
            ", checkdigit='" + getCheckdigit() + "'" +
            ", taxpayerName='" + getTaxpayerName() + "'" +
            ", natuteOfBuss='" + getNatuteOfBuss() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            ", email='" + getEmail() + "'" +
            ", tradeName='" + getTradeName() + "'" +
            ", constitBusiness='" + getConstitBusiness() + "'" +
            "}";
    }
}
