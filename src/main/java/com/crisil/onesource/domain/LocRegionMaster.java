package com.crisil.onesource.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Location_Master_Table
 */
@ApiModel(description = "Location_Master_Table")
@Entity
@Table(name = "loc_region_master")
public class LocRegionMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "region_id", nullable = false)
    private Integer regionId;

    @NotNull
    @Size(max = 100)
    @Column(name = "region_name", length = 100, nullable = false)
    private String regionName;

    @Size(max = 250)
    @Column(name = "description", length = 250)
    private String description;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_approved", length = 1)
    private String isApproved;

    @Column(name = "seq_order")
    private Integer seqOrder;

    @OneToMany(mappedBy = "regionId")
    private Set<LocStateMaster> locStateMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public LocRegionMaster regionId(Integer regionId) {
        this.regionId = regionId;
        return this;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegionName() {
        return regionName;
    }

    public LocRegionMaster regionName(String regionName) {
        this.regionName = regionName;
        return this;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getDescription() {
        return description;
    }

    public LocRegionMaster description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocRegionMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocRegionMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocRegionMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocRegionMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocRegionMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public LocRegionMaster isApproved(String isApproved) {
        this.isApproved = isApproved;
        return this;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public LocRegionMaster seqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
        return this;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Set<LocStateMaster> getLocStateMasters() {
        return locStateMasters;
    }

    public LocRegionMaster locStateMasters(Set<LocStateMaster> locStateMasters) {
        this.locStateMasters = locStateMasters;
        return this;
    }

    public LocRegionMaster addLocStateMaster(LocStateMaster locStateMaster) {
        this.locStateMasters.add(locStateMaster);
        locStateMaster.setRegionId(this);
        return this;
    }

    public LocRegionMaster removeLocStateMaster(LocStateMaster locStateMaster) {
        this.locStateMasters.remove(locStateMaster);
        locStateMaster.setRegionId(null);
        return this;
    }

    public void setLocStateMasters(Set<LocStateMaster> locStateMasters) {
        this.locStateMasters = locStateMasters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocRegionMaster)) {
            return false;
        }
        return id != null && id.equals(((LocRegionMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocRegionMaster{" +
            "id=" + getId() +
            ", regionId=" + getRegionId() +
            ", regionName='" + getRegionName() + "'" +
            ", description='" + getDescription() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            "}";
    }
}
