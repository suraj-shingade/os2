package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Location_Master_Table
 */
@ApiModel(description = "Location_Master_Table")
@Entity
@Table(name = "loc_city_master")
public class LocCityMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "city_name", length = 100, nullable = false)
    private String cityName;

    @NotNull
    @Column(name = "city_id", nullable = false)
    private Integer cityId;

    @Size(max = 100)
    @Column(name = "state_name", length = 100)
    private String stateName;

    @Size(max = 100)
    @Column(name = "country_name", length = 100)
    private String countryName;

    @Size(max = 20)
    @Column(name = "pin_num", length = 20)
    private String pinNum;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 25)
    @Column(name = "record_id", length = 25)
    private String recordId;

    @Size(max = 25)
    @Column(name = "country_code", length = 25)
    private String countryCode;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "srno")
    private Integer srno;

    @Size(max = 1)
    @Column(name = "is_approved", length = 1)
    private String isApproved;

    @Column(name = "seq_order")
    private Integer seqOrder;

    @OneToMany(mappedBy = "cityId")
    private Set<LocCityPincodeMapping> locCityPincodeMappings = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "locCityMasters", allowSetters = true)
    private LocStateMaster stateId;

    @ManyToOne
    @JsonIgnoreProperties(value = "locCityMasters", allowSetters = true)
    private LocDistrictMaster districtId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public LocCityMaster cityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public LocCityMaster cityId(Integer cityId) {
        this.cityId = cityId;
        return this;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getStateName() {
        return stateName;
    }

    public LocCityMaster stateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryName() {
        return countryName;
    }

    public LocCityMaster countryName(String countryName) {
        this.countryName = countryName;
        return this;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPinNum() {
        return pinNum;
    }

    public LocCityMaster pinNum(String pinNum) {
        this.pinNum = pinNum;
        return this;
    }

    public void setPinNum(String pinNum) {
        this.pinNum = pinNum;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocCityMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRecordId() {
        return recordId;
    }

    public LocCityMaster recordId(String recordId) {
        this.recordId = recordId;
        return this;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public LocCityMaster countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocCityMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocCityMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocCityMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocCityMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getSrno() {
        return srno;
    }

    public LocCityMaster srno(Integer srno) {
        this.srno = srno;
        return this;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public LocCityMaster isApproved(String isApproved) {
        this.isApproved = isApproved;
        return this;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public LocCityMaster seqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
        return this;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Set<LocCityPincodeMapping> getLocCityPincodeMappings() {
        return locCityPincodeMappings;
    }

    public LocCityMaster locCityPincodeMappings(Set<LocCityPincodeMapping> locCityPincodeMappings) {
        this.locCityPincodeMappings = locCityPincodeMappings;
        return this;
    }

    public LocCityMaster addLocCityPincodeMapping(LocCityPincodeMapping locCityPincodeMapping) {
        this.locCityPincodeMappings.add(locCityPincodeMapping);
        locCityPincodeMapping.setCityId(this);
        return this;
    }

    public LocCityMaster removeLocCityPincodeMapping(LocCityPincodeMapping locCityPincodeMapping) {
        this.locCityPincodeMappings.remove(locCityPincodeMapping);
        locCityPincodeMapping.setCityId(null);
        return this;
    }

    public void setLocCityPincodeMappings(Set<LocCityPincodeMapping> locCityPincodeMappings) {
        this.locCityPincodeMappings = locCityPincodeMappings;
    }

    public LocStateMaster getStateId() {
        return stateId;
    }

    public LocCityMaster stateId(LocStateMaster locStateMaster) {
        this.stateId = locStateMaster;
        return this;
    }

    public void setStateId(LocStateMaster locStateMaster) {
        this.stateId = locStateMaster;
    }

    public LocDistrictMaster getDistrictId() {
        return districtId;
    }

    public LocCityMaster districtId(LocDistrictMaster locDistrictMaster) {
        this.districtId = locDistrictMaster;
        return this;
    }

    public void setDistrictId(LocDistrictMaster locDistrictMaster) {
        this.districtId = locDistrictMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocCityMaster)) {
            return false;
        }
        return id != null && id.equals(((LocCityMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCityMaster{" +
            "id=" + getId() +
            ", cityName='" + getCityName() + "'" +
            ", cityId=" + getCityId() +
            ", stateName='" + getStateName() + "'" +
            ", countryName='" + getCountryName() + "'" +
            ", pinNum='" + getPinNum() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", srno=" + getSrno() +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            "}";
    }
}
