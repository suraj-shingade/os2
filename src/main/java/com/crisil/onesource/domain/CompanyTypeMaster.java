package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A CompanyTypeMaster.
 */
@Entity
@Table(name = "company_type_master")
public class CompanyTypeMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "company_type_id", nullable = false)
    private Integer companyTypeId;

    @NotNull
    @Size(max = 50)
    @Column(name = "company_type", length = 50, nullable = false)
    private String companyType;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @OneToMany(mappedBy = "companyTypeId")
    private Set<OnesourceCompanyMaster> onesourceCompanyMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCompanyTypeId() {
        return companyTypeId;
    }

    public CompanyTypeMaster companyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
        return this;
    }

    public void setCompanyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public String getCompanyType() {
        return companyType;
    }

    public CompanyTypeMaster companyType(String companyType) {
        this.companyType = companyType;
        return this;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyTypeMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyTypeMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public CompanyTypeMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyTypeMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyTypeMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Set<OnesourceCompanyMaster> getOnesourceCompanyMasters() {
        return onesourceCompanyMasters;
    }

    public CompanyTypeMaster onesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
        return this;
    }

    public CompanyTypeMaster addOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.add(onesourceCompanyMaster);
        onesourceCompanyMaster.setCompanyTypeId(this);
        return this;
    }

    public CompanyTypeMaster removeOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.remove(onesourceCompanyMaster);
        onesourceCompanyMaster.setCompanyTypeId(null);
        return this;
    }

    public void setOnesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyTypeMaster)) {
            return false;
        }
        return id != null && id.equals(((CompanyTypeMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyTypeMaster{" +
            "id=" + getId() +
            ", companyTypeId=" + getCompanyTypeId() +
            ", companyType='" + getCompanyType() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            "}";
    }
}
