package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A LocCityPincodeMapping.
 */
@Entity
@Table(name = "loc_city_pincode_mapping")
public class LocCityPincodeMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "city_pin_mapping_id", nullable = false)
    private Integer cityPinMappingId;

    @Size(max = 20)
    @Column(name = "pincode", length = 20)
    private String pincode;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @ManyToOne
    @JsonIgnoreProperties(value = "locCityPincodeMappings", allowSetters = true)
    private LocCityMaster cityId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCityPinMappingId() {
        return cityPinMappingId;
    }

    public LocCityPincodeMapping cityPinMappingId(Integer cityPinMappingId) {
        this.cityPinMappingId = cityPinMappingId;
        return this;
    }

    public void setCityPinMappingId(Integer cityPinMappingId) {
        this.cityPinMappingId = cityPinMappingId;
    }

    public String getPincode() {
        return pincode;
    }

    public LocCityPincodeMapping pincode(String pincode) {
        this.pincode = pincode;
        return this;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocCityPincodeMapping isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocCityPincodeMapping createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocCityPincodeMapping createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocCityPincodeMapping lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocCityPincodeMapping lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocCityMaster getCityId() {
        return cityId;
    }

    public LocCityPincodeMapping cityId(LocCityMaster locCityMaster) {
        this.cityId = locCityMaster;
        return this;
    }

    public void setCityId(LocCityMaster locCityMaster) {
        this.cityId = locCityMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocCityPincodeMapping)) {
            return false;
        }
        return id != null && id.equals(((LocCityPincodeMapping) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCityPincodeMapping{" +
            "id=" + getId() +
            ", cityPinMappingId=" + getCityPinMappingId() +
            ", pincode='" + getPincode() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            "}";
    }
}
