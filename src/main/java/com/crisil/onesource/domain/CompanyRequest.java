package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A CompanyRequest.
 */
@Entity
@Table(name = "company_request")
public class CompanyRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 200)
    @Column(name = "company_name", length = 200)
    private String companyName;

    @Column(name = "company_type_id")
    private Integer companyTypeId;

    @Column(name = "sector_id")
    private Integer sectorId;

    @Column(name = "industry_id")
    private Integer industryId;

    @Column(name = "business_area_id")
    private Integer businessAreaId;

    @Size(max = 50)
    @Column(name = "client_group_name", length = 50)
    private String clientGroupName;

    @Size(max = 100)
    @Column(name = "remarks", length = 100)
    private String remarks;

    @NotNull
    @Size(max = 50)
    @Column(name = "record_id", length = 50, nullable = false)
    private String recordId;

    @Size(max = 10)
    @Column(name = "company_code", length = 10)
    private String companyCode;

    @Size(max = 50)
    @Column(name = "status", length = 50)
    private String status;

    @Size(max = 50)
    @Column(name = "reason", length = 50)
    private String reason;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 50)
    @Column(name = "billing_type", length = 50)
    private String billingType;

    @Size(max = 200)
    @Column(name = "display_company_name", length = 200)
    private String displayCompanyName;

    @Size(max = 20)
    @Column(name = "requested_by", length = 20)
    private String requestedBy;

    @Size(max = 20)
    @Column(name = "approved_by", length = 20)
    private String approvedBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "requested_date")
    private Instant requestedDate;

    @Column(name = "approved_date")
    private Instant approvedDate;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 50)
    @Column(name = "original_filename", length = 50)
    private String originalFilename;

    @Size(max = 500)
    @Column(name = "display_filename", length = 500)
    private String displayFilename;

    @OneToMany(mappedBy = "record")
    private Set<CompanyAddressMaster> companyAddressMasters = new HashSet<>();

    @OneToMany(mappedBy = "record")
    private Set<CompanyTanPanRocDtls> companyTanPanRocDtls = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public CompanyRequest companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyTypeId() {
        return companyTypeId;
    }

    public CompanyRequest companyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
        return this;
    }

    public void setCompanyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public CompanyRequest sectorId(Integer sectorId) {
        this.sectorId = sectorId;
        return this;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Integer getIndustryId() {
        return industryId;
    }

    public CompanyRequest industryId(Integer industryId) {
        this.industryId = industryId;
        return this;
    }

    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    public CompanyRequest businessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
        return this;
    }

    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public String getClientGroupName() {
        return clientGroupName;
    }

    public CompanyRequest clientGroupName(String clientGroupName) {
        this.clientGroupName = clientGroupName;
        return this;
    }

    public void setClientGroupName(String clientGroupName) {
        this.clientGroupName = clientGroupName;
    }

    public String getRemarks() {
        return remarks;
    }

    public CompanyRequest remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRecordId() {
        return recordId;
    }

    public CompanyRequest recordId(String recordId) {
        this.recordId = recordId;
        return this;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public CompanyRequest companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getStatus() {
        return status;
    }

    public CompanyRequest status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public CompanyRequest reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyRequest isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getBillingType() {
        return billingType;
    }

    public CompanyRequest billingType(String billingType) {
        this.billingType = billingType;
        return this;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getDisplayCompanyName() {
        return displayCompanyName;
    }

    public CompanyRequest displayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
        return this;
    }

    public void setDisplayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public CompanyRequest requestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
        return this;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public CompanyRequest approvedBy(String approvedBy) {
        this.approvedBy = approvedBy;
        return this;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public CompanyRequest createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyRequest createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyRequest lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getRequestedDate() {
        return requestedDate;
    }

    public CompanyRequest requestedDate(Instant requestedDate) {
        this.requestedDate = requestedDate;
        return this;
    }

    public void setRequestedDate(Instant requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Instant getApprovedDate() {
        return approvedDate;
    }

    public CompanyRequest approvedDate(Instant approvedDate) {
        this.approvedDate = approvedDate;
        return this;
    }

    public void setApprovedDate(Instant approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyRequest lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public CompanyRequest originalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
        return this;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public CompanyRequest displayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
        return this;
    }

    public void setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
    }

    public Set<CompanyAddressMaster> getCompanyAddressMasters() {
        return companyAddressMasters;
    }

    public CompanyRequest companyAddressMasters(Set<CompanyAddressMaster> companyAddressMasters) {
        this.companyAddressMasters = companyAddressMasters;
        return this;
    }

    public CompanyRequest addCompanyAddressMaster(CompanyAddressMaster companyAddressMaster) {
        this.companyAddressMasters.add(companyAddressMaster);
        companyAddressMaster.setRecord(this);
        return this;
    }

    public CompanyRequest removeCompanyAddressMaster(CompanyAddressMaster companyAddressMaster) {
        this.companyAddressMasters.remove(companyAddressMaster);
        companyAddressMaster.setRecord(null);
        return this;
    }

    public void setCompanyAddressMasters(Set<CompanyAddressMaster> companyAddressMasters) {
        this.companyAddressMasters = companyAddressMasters;
    }

    public Set<CompanyTanPanRocDtls> getCompanyTanPanRocDtls() {
        return companyTanPanRocDtls;
    }

    public CompanyRequest companyTanPanRocDtls(Set<CompanyTanPanRocDtls> companyTanPanRocDtls) {
        this.companyTanPanRocDtls = companyTanPanRocDtls;
        return this;
    }

    public CompanyRequest addCompanyTanPanRocDtls(CompanyTanPanRocDtls companyTanPanRocDtls) {
        this.companyTanPanRocDtls.add(companyTanPanRocDtls);
        companyTanPanRocDtls.setRecord(this);
        return this;
    }

    public CompanyRequest removeCompanyTanPanRocDtls(CompanyTanPanRocDtls companyTanPanRocDtls) {
        this.companyTanPanRocDtls.remove(companyTanPanRocDtls);
        companyTanPanRocDtls.setRecord(null);
        return this;
    }

    public void setCompanyTanPanRocDtls(Set<CompanyTanPanRocDtls> companyTanPanRocDtls) {
        this.companyTanPanRocDtls = companyTanPanRocDtls;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyRequest)) {
            return false;
        }
        return id != null && id.equals(((CompanyRequest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyRequest{" +
            "id=" + getId() +
            ", companyName='" + getCompanyName() + "'" +
            ", companyTypeId=" + getCompanyTypeId() +
            ", sectorId=" + getSectorId() +
            ", industryId=" + getIndustryId() +
            ", businessAreaId=" + getBusinessAreaId() +
            ", clientGroupName='" + getClientGroupName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", reason='" + getReason() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", billingType='" + getBillingType() + "'" +
            ", displayCompanyName='" + getDisplayCompanyName() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", approvedBy='" + getApprovedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", requestedDate='" + getRequestedDate() + "'" +
            ", approvedDate='" + getApprovedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", originalFilename='" + getOriginalFilename() + "'" +
            ", displayFilename='" + getDisplayFilename() + "'" +
            "}";
    }
}
