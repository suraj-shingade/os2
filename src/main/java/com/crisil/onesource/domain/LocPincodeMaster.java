package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Location_Master_Table
 */
@ApiModel(description = "Location_Master_Table")
@Entity
@Table(name = "loc_pincode_master")
public class LocPincodeMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pincode_srno")
    private Integer pincodeSrno;

    @Size(max = 255)
    @Column(name = "office_name", length = 255)
    private String officeName;

    @Size(max = 25)
    @Column(name = "pincode", length = 25)
    private String pincode;

    @Size(max = 255)
    @Column(name = "office_type", length = 255)
    private String officeType;

    @Size(max = 255)
    @Column(name = "delivery_status", length = 255)
    private String deliveryStatus;

    @Size(max = 255)
    @Column(name = "division_name", length = 255)
    private String divisionName;

    @Size(max = 255)
    @Column(name = "region_name", length = 255)
    private String regionName;

    @Size(max = 255)
    @Column(name = "circle_name", length = 255)
    private String circleName;

    @Size(max = 255)
    @Column(name = "taluka", length = 255)
    private String taluka;

    @Size(max = 255)
    @Column(name = "district_name", length = 255)
    private String districtName;

    @Size(max = 255)
    @Column(name = "state_name", length = 255)
    private String stateName;

    @Size(max = 255)
    @Column(name = "country", length = 255)
    private String country;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_approved", length = 1)
    private String isApproved;

    @Column(name = "seq_order")
    private Integer seqOrder;

    @Size(max = 1)
    @Column(name = "is_data_processed", length = 1)
    private String isDataProcessed;

    @Column(name = "json_store_id")
    private Long jsonStoreId;

    @Size(max = 1)
    @Column(name = "ctlg_is_deleted", length = 1)
    private String ctlgIsDeleted;

    @Size(max = 1)
    @Column(name = "operation", length = 1)
    private String operation;

    @NotNull
    @Column(name = "ctlg_srno", nullable = false)
    private Integer ctlgSrno;

    @OneToMany(mappedBy = "pincode")
    private Set<CompanyAddressMaster> companyAddressMasters = new HashSet<>();

    @OneToMany(mappedBy = "pin")
    private Set<OnesourceCompanyMaster> onesourceCompanyMasters = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "locPincodeMasters", allowSetters = true)
    private LocStateMaster stateId;

    @ManyToOne
    @JsonIgnoreProperties(value = "locPincodeMasters", allowSetters = true)
    private LocDistrictMaster districtId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPincodeSrno() {
        return pincodeSrno;
    }

    public LocPincodeMaster pincodeSrno(Integer pincodeSrno) {
        this.pincodeSrno = pincodeSrno;
        return this;
    }

    public void setPincodeSrno(Integer pincodeSrno) {
        this.pincodeSrno = pincodeSrno;
    }

    public String getOfficeName() {
        return officeName;
    }

    public LocPincodeMaster officeName(String officeName) {
        this.officeName = officeName;
        return this;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getPincode() {
        return pincode;
    }

    public LocPincodeMaster pincode(String pincode) {
        this.pincode = pincode;
        return this;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getOfficeType() {
        return officeType;
    }

    public LocPincodeMaster officeType(String officeType) {
        this.officeType = officeType;
        return this;
    }

    public void setOfficeType(String officeType) {
        this.officeType = officeType;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public LocPincodeMaster deliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
        return this;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public LocPincodeMaster divisionName(String divisionName) {
        this.divisionName = divisionName;
        return this;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getRegionName() {
        return regionName;
    }

    public LocPincodeMaster regionName(String regionName) {
        this.regionName = regionName;
        return this;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCircleName() {
        return circleName;
    }

    public LocPincodeMaster circleName(String circleName) {
        this.circleName = circleName;
        return this;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getTaluka() {
        return taluka;
    }

    public LocPincodeMaster taluka(String taluka) {
        this.taluka = taluka;
        return this;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getDistrictName() {
        return districtName;
    }

    public LocPincodeMaster districtName(String districtName) {
        this.districtName = districtName;
        return this;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getStateName() {
        return stateName;
    }

    public LocPincodeMaster stateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountry() {
        return country;
    }

    public LocPincodeMaster country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocPincodeMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocPincodeMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocPincodeMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocPincodeMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocPincodeMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public LocPincodeMaster isApproved(String isApproved) {
        this.isApproved = isApproved;
        return this;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public LocPincodeMaster seqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
        return this;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public LocPincodeMaster isDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
        return this;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public Long getJsonStoreId() {
        return jsonStoreId;
    }

    public LocPincodeMaster jsonStoreId(Long jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
        return this;
    }

    public void setJsonStoreId(Long jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public LocPincodeMaster ctlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
        return this;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public String getOperation() {
        return operation;
    }

    public LocPincodeMaster operation(String operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public LocPincodeMaster ctlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
        return this;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public Set<CompanyAddressMaster> getCompanyAddressMasters() {
        return companyAddressMasters;
    }

    public LocPincodeMaster companyAddressMasters(Set<CompanyAddressMaster> companyAddressMasters) {
        this.companyAddressMasters = companyAddressMasters;
        return this;
    }

    public LocPincodeMaster addCompanyAddressMaster(CompanyAddressMaster companyAddressMaster) {
        this.companyAddressMasters.add(companyAddressMaster);
        companyAddressMaster.setPincode(this);
        return this;
    }

    public LocPincodeMaster removeCompanyAddressMaster(CompanyAddressMaster companyAddressMaster) {
        this.companyAddressMasters.remove(companyAddressMaster);
        companyAddressMaster.setPincode(null);
        return this;
    }

    public void setCompanyAddressMasters(Set<CompanyAddressMaster> companyAddressMasters) {
        this.companyAddressMasters = companyAddressMasters;
    }

    public Set<OnesourceCompanyMaster> getOnesourceCompanyMasters() {
        return onesourceCompanyMasters;
    }

    public LocPincodeMaster onesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
        return this;
    }

    public LocPincodeMaster addOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.add(onesourceCompanyMaster);
        onesourceCompanyMaster.setPin(this);
        return this;
    }

    public LocPincodeMaster removeOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.remove(onesourceCompanyMaster);
        onesourceCompanyMaster.setPin(null);
        return this;
    }

    public void setOnesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
    }

    public LocStateMaster getStateId() {
        return stateId;
    }

    public LocPincodeMaster stateId(LocStateMaster locStateMaster) {
        this.stateId = locStateMaster;
        return this;
    }

    public void setStateId(LocStateMaster locStateMaster) {
        this.stateId = locStateMaster;
    }

    public LocDistrictMaster getDistrictId() {
        return districtId;
    }

    public LocPincodeMaster districtId(LocDistrictMaster locDistrictMaster) {
        this.districtId = locDistrictMaster;
        return this;
    }

    public void setDistrictId(LocDistrictMaster locDistrictMaster) {
        this.districtId = locDistrictMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocPincodeMaster)) {
            return false;
        }
        return id != null && id.equals(((LocPincodeMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocPincodeMaster{" +
            "id=" + getId() +
            ", pincodeSrno=" + getPincodeSrno() +
            ", officeName='" + getOfficeName() + "'" +
            ", pincode='" + getPincode() + "'" +
            ", officeType='" + getOfficeType() + "'" +
            ", deliveryStatus='" + getDeliveryStatus() + "'" +
            ", divisionName='" + getDivisionName() + "'" +
            ", regionName='" + getRegionName() + "'" +
            ", circleName='" + getCircleName() + "'" +
            ", taluka='" + getTaluka() + "'" +
            ", districtName='" + getDistrictName() + "'" +
            ", stateName='" + getStateName() + "'" +
            ", country='" + getCountry() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            ", jsonStoreId=" + getJsonStoreId() +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", operation='" + getOperation() + "'" +
            ", ctlgSrno=" + getCtlgSrno() +
            "}";
    }
}
