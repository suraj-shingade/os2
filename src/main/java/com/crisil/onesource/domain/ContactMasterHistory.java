package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A ContactMasterHistory.
 */
@Entity
@Table(name = "contact_master_history")
public class ContactMasterHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 75)
    @Column(name = "first_name", length = 75)
    private String firstName;

    @NotNull
    @Size(max = 75)
    @Column(name = "last_name", length = 75, nullable = false)
    private String lastName;

    @Size(max = 75)
    @Column(name = "middle_name", length = 75)
    private String middleName;

    @NotNull
    @Size(max = 15)
    @Column(name = "salutation", length = 15, nullable = false)
    private String salutation;

    @NotNull
    @Size(max = 500)
    @Column(name = "address_1", length = 500, nullable = false)
    private String address1;

    @Size(max = 500)
    @Column(name = "address_2", length = 500)
    private String address2;

    @Size(max = 500)
    @Column(name = "address_3", length = 500)
    private String address3;

    @NotNull
    @Size(max = 200)
    @Column(name = "designation", length = 200, nullable = false)
    private String designation;

    @Size(max = 200)
    @Column(name = "department", length = 200)
    private String department;

    @Size(max = 100)
    @Column(name = "city", length = 100)
    private String city;

    @Size(max = 100)
    @Column(name = "state", length = 100)
    private String state;

    @Size(max = 100)
    @Column(name = "country", length = 100)
    private String country;

    @Size(max = 50)
    @Column(name = "pin", length = 50)
    private String pin;

    @Size(max = 200)
    @Column(name = "phone_num", length = 200)
    private String phoneNum;

    @Size(max = 200)
    @Column(name = "fax_num", length = 200)
    private String faxNum;

    @Size(max = 200)
    @Column(name = "email", length = 200)
    private String email;

    @Size(max = 200)
    @Column(name = "mobile_num", length = 200)
    private String mobileNum;

    @NotNull
    @Size(max = 25)
    @Column(name = "record_id", length = 25, nullable = false)
    private String recordId;

    @NotNull
    @Size(max = 50)
    @Column(name = "company_code", length = 50, nullable = false)
    private String companyCode;

    @Size(max = 100)
    @Column(name = "comments", length = 100)
    private String comments;

    @Size(max = 25)
    @Column(name = "key_person", length = 25)
    private String keyPerson;

    @NotNull
    @Size(max = 150)
    @Column(name = "contact_id", length = 150, nullable = false)
    private String contactId;

    @Size(max = 10)
    @Column(name = "level_importance", length = 10)
    private String levelImportance;

    @Size(max = 50)
    @Column(name = "status", length = 50)
    private String status;

    @Size(max = 100)
    @Column(name = "remove_reason", length = 100)
    private String removeReason;

    @Size(max = 15)
    @Column(name = "city_id", length = 15)
    private String cityId;

    @Size(max = 3)
    @Column(name = "o_flag", length = 3)
    private String oFlag;

    @Size(max = 20)
    @Column(name = "opt_out", length = 20)
    private String optOut;

    @Size(max = 150)
    @Column(name = "ofa_contact_id", length = 150)
    private String ofaContactId;

    @Column(name = "opt_out_date")
    private LocalDate optOutDate;

    @Size(max = 10)
    @Column(name = "fax_optout", length = 10)
    private String faxOptout;

    @Size(max = 10)
    @Column(name = "donotcall", length = 10)
    private String donotcall;

    @Size(max = 10)
    @Column(name = "dnsend_direct_mail", length = 10)
    private String dnsendDirectMail;

    @Size(max = 10)
    @Column(name = "dnshare_outside_mhp", length = 10)
    private String dnshareOutsideMhp;

    @Size(max = 10)
    @Column(name = "outside_of_contact", length = 10)
    private String outsideOfContact;

    @Size(max = 10)
    @Column(name = "dnshare_outside_snp", length = 10)
    private String dnshareOutsideSnp;

    @Size(max = 10)
    @Column(name = "email_optout", length = 10)
    private String emailOptout;

    @Size(max = 1)
    @Column(name = "sms_optout", length = 1)
    private String smsOptout;

    @Size(max = 1)
    @Column(name = "is_sez_contact_flag", length = 1)
    private String isSezContactFlag;

    @Size(max = 10)
    @Column(name = "is_exempt_contact_flag", length = 10)
    private String isExemptContactFlag;

    @Size(max = 1)
    @Column(name = "gst_not_applicable", length = 1)
    private String gstNotApplicable;

    @Size(max = 1)
    @Column(name = "billing_contact", length = 1)
    private String billingContact;

    @Size(max = 25)
    @Column(name = "gst_party_type", length = 25)
    private String gstPartyType;

    @Size(max = 20)
    @Column(name = "tan", length = 20)
    private String tan;

    @Size(max = 25)
    @Column(name = "gst_number", length = 25)
    private String gstNumber;

    @Size(max = 25)
    @Column(name = "tds_number", length = 25)
    private String tdsNumber;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public ContactMasterHistory firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ContactMasterHistory lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public ContactMasterHistory middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSalutation() {
        return salutation;
    }

    public ContactMasterHistory salutation(String salutation) {
        this.salutation = salutation;
        return this;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getAddress1() {
        return address1;
    }

    public ContactMasterHistory address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public ContactMasterHistory address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public ContactMasterHistory address3(String address3) {
        this.address3 = address3;
        return this;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getDesignation() {
        return designation;
    }

    public ContactMasterHistory designation(String designation) {
        this.designation = designation;
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public ContactMasterHistory department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCity() {
        return city;
    }

    public ContactMasterHistory city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public ContactMasterHistory state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public ContactMasterHistory country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPin() {
        return pin;
    }

    public ContactMasterHistory pin(String pin) {
        this.pin = pin;
        return this;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public ContactMasterHistory phoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
        return this;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getFaxNum() {
        return faxNum;
    }

    public ContactMasterHistory faxNum(String faxNum) {
        this.faxNum = faxNum;
        return this;
    }

    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    public String getEmail() {
        return email;
    }

    public ContactMasterHistory email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public ContactMasterHistory mobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
        return this;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getRecordId() {
        return recordId;
    }

    public ContactMasterHistory recordId(String recordId) {
        this.recordId = recordId;
        return this;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public ContactMasterHistory companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getComments() {
        return comments;
    }

    public ContactMasterHistory comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getKeyPerson() {
        return keyPerson;
    }

    public ContactMasterHistory keyPerson(String keyPerson) {
        this.keyPerson = keyPerson;
        return this;
    }

    public void setKeyPerson(String keyPerson) {
        this.keyPerson = keyPerson;
    }

    public String getContactId() {
        return contactId;
    }

    public ContactMasterHistory contactId(String contactId) {
        this.contactId = contactId;
        return this;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getLevelImportance() {
        return levelImportance;
    }

    public ContactMasterHistory levelImportance(String levelImportance) {
        this.levelImportance = levelImportance;
        return this;
    }

    public void setLevelImportance(String levelImportance) {
        this.levelImportance = levelImportance;
    }

    public String getStatus() {
        return status;
    }

    public ContactMasterHistory status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemoveReason() {
        return removeReason;
    }

    public ContactMasterHistory removeReason(String removeReason) {
        this.removeReason = removeReason;
        return this;
    }

    public void setRemoveReason(String removeReason) {
        this.removeReason = removeReason;
    }

    public String getCityId() {
        return cityId;
    }

    public ContactMasterHistory cityId(String cityId) {
        this.cityId = cityId;
        return this;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getoFlag() {
        return oFlag;
    }

    public ContactMasterHistory oFlag(String oFlag) {
        this.oFlag = oFlag;
        return this;
    }

    public void setoFlag(String oFlag) {
        this.oFlag = oFlag;
    }

    public String getOptOut() {
        return optOut;
    }

    public ContactMasterHistory optOut(String optOut) {
        this.optOut = optOut;
        return this;
    }

    public void setOptOut(String optOut) {
        this.optOut = optOut;
    }

    public String getOfaContactId() {
        return ofaContactId;
    }

    public ContactMasterHistory ofaContactId(String ofaContactId) {
        this.ofaContactId = ofaContactId;
        return this;
    }

    public void setOfaContactId(String ofaContactId) {
        this.ofaContactId = ofaContactId;
    }

    public LocalDate getOptOutDate() {
        return optOutDate;
    }

    public ContactMasterHistory optOutDate(LocalDate optOutDate) {
        this.optOutDate = optOutDate;
        return this;
    }

    public void setOptOutDate(LocalDate optOutDate) {
        this.optOutDate = optOutDate;
    }

    public String getFaxOptout() {
        return faxOptout;
    }

    public ContactMasterHistory faxOptout(String faxOptout) {
        this.faxOptout = faxOptout;
        return this;
    }

    public void setFaxOptout(String faxOptout) {
        this.faxOptout = faxOptout;
    }

    public String getDonotcall() {
        return donotcall;
    }

    public ContactMasterHistory donotcall(String donotcall) {
        this.donotcall = donotcall;
        return this;
    }

    public void setDonotcall(String donotcall) {
        this.donotcall = donotcall;
    }

    public String getDnsendDirectMail() {
        return dnsendDirectMail;
    }

    public ContactMasterHistory dnsendDirectMail(String dnsendDirectMail) {
        this.dnsendDirectMail = dnsendDirectMail;
        return this;
    }

    public void setDnsendDirectMail(String dnsendDirectMail) {
        this.dnsendDirectMail = dnsendDirectMail;
    }

    public String getDnshareOutsideMhp() {
        return dnshareOutsideMhp;
    }

    public ContactMasterHistory dnshareOutsideMhp(String dnshareOutsideMhp) {
        this.dnshareOutsideMhp = dnshareOutsideMhp;
        return this;
    }

    public void setDnshareOutsideMhp(String dnshareOutsideMhp) {
        this.dnshareOutsideMhp = dnshareOutsideMhp;
    }

    public String getOutsideOfContact() {
        return outsideOfContact;
    }

    public ContactMasterHistory outsideOfContact(String outsideOfContact) {
        this.outsideOfContact = outsideOfContact;
        return this;
    }

    public void setOutsideOfContact(String outsideOfContact) {
        this.outsideOfContact = outsideOfContact;
    }

    public String getDnshareOutsideSnp() {
        return dnshareOutsideSnp;
    }

    public ContactMasterHistory dnshareOutsideSnp(String dnshareOutsideSnp) {
        this.dnshareOutsideSnp = dnshareOutsideSnp;
        return this;
    }

    public void setDnshareOutsideSnp(String dnshareOutsideSnp) {
        this.dnshareOutsideSnp = dnshareOutsideSnp;
    }

    public String getEmailOptout() {
        return emailOptout;
    }

    public ContactMasterHistory emailOptout(String emailOptout) {
        this.emailOptout = emailOptout;
        return this;
    }

    public void setEmailOptout(String emailOptout) {
        this.emailOptout = emailOptout;
    }

    public String getSmsOptout() {
        return smsOptout;
    }

    public ContactMasterHistory smsOptout(String smsOptout) {
        this.smsOptout = smsOptout;
        return this;
    }

    public void setSmsOptout(String smsOptout) {
        this.smsOptout = smsOptout;
    }

    public String getIsSezContactFlag() {
        return isSezContactFlag;
    }

    public ContactMasterHistory isSezContactFlag(String isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
        return this;
    }

    public void setIsSezContactFlag(String isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
    }

    public String getIsExemptContactFlag() {
        return isExemptContactFlag;
    }

    public ContactMasterHistory isExemptContactFlag(String isExemptContactFlag) {
        this.isExemptContactFlag = isExemptContactFlag;
        return this;
    }

    public void setIsExemptContactFlag(String isExemptContactFlag) {
        this.isExemptContactFlag = isExemptContactFlag;
    }

    public String getGstNotApplicable() {
        return gstNotApplicable;
    }

    public ContactMasterHistory gstNotApplicable(String gstNotApplicable) {
        this.gstNotApplicable = gstNotApplicable;
        return this;
    }

    public void setGstNotApplicable(String gstNotApplicable) {
        this.gstNotApplicable = gstNotApplicable;
    }

    public String getBillingContact() {
        return billingContact;
    }

    public ContactMasterHistory billingContact(String billingContact) {
        this.billingContact = billingContact;
        return this;
    }

    public void setBillingContact(String billingContact) {
        this.billingContact = billingContact;
    }

    public String getGstPartyType() {
        return gstPartyType;
    }

    public ContactMasterHistory gstPartyType(String gstPartyType) {
        this.gstPartyType = gstPartyType;
        return this;
    }

    public void setGstPartyType(String gstPartyType) {
        this.gstPartyType = gstPartyType;
    }

    public String getTan() {
        return tan;
    }

    public ContactMasterHistory tan(String tan) {
        this.tan = tan;
        return this;
    }

    public void setTan(String tan) {
        this.tan = tan;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public ContactMasterHistory gstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
        return this;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getTdsNumber() {
        return tdsNumber;
    }

    public ContactMasterHistory tdsNumber(String tdsNumber) {
        this.tdsNumber = tdsNumber;
        return this;
    }

    public void setTdsNumber(String tdsNumber) {
        this.tdsNumber = tdsNumber;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public ContactMasterHistory createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public ContactMasterHistory createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public ContactMasterHistory lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public ContactMasterHistory lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public ContactMasterHistory isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactMasterHistory)) {
            return false;
        }
        return id != null && id.equals(((ContactMasterHistory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactMasterHistory{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", salutation='" + getSalutation() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", department='" + getDepartment() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", pin='" + getPin() + "'" +
            ", phoneNum='" + getPhoneNum() + "'" +
            ", faxNum='" + getFaxNum() + "'" +
            ", email='" + getEmail() + "'" +
            ", mobileNum='" + getMobileNum() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            ", comments='" + getComments() + "'" +
            ", keyPerson='" + getKeyPerson() + "'" +
            ", contactId='" + getContactId() + "'" +
            ", levelImportance='" + getLevelImportance() + "'" +
            ", status='" + getStatus() + "'" +
            ", removeReason='" + getRemoveReason() + "'" +
            ", cityId='" + getCityId() + "'" +
            ", oFlag='" + getoFlag() + "'" +
            ", optOut='" + getOptOut() + "'" +
            ", ofaContactId='" + getOfaContactId() + "'" +
            ", optOutDate='" + getOptOutDate() + "'" +
            ", faxOptout='" + getFaxOptout() + "'" +
            ", donotcall='" + getDonotcall() + "'" +
            ", dnsendDirectMail='" + getDnsendDirectMail() + "'" +
            ", dnshareOutsideMhp='" + getDnshareOutsideMhp() + "'" +
            ", outsideOfContact='" + getOutsideOfContact() + "'" +
            ", dnshareOutsideSnp='" + getDnshareOutsideSnp() + "'" +
            ", emailOptout='" + getEmailOptout() + "'" +
            ", smsOptout='" + getSmsOptout() + "'" +
            ", isSezContactFlag='" + getIsSezContactFlag() + "'" +
            ", isExemptContactFlag='" + getIsExemptContactFlag() + "'" +
            ", gstNotApplicable='" + getGstNotApplicable() + "'" +
            ", billingContact='" + getBillingContact() + "'" +
            ", gstPartyType='" + getGstPartyType() + "'" +
            ", tan='" + getTan() + "'" +
            ", gstNumber='" + getGstNumber() + "'" +
            ", tdsNumber='" + getTdsNumber() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
