package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Location_Master_Table
 */
@ApiModel(description = "Location_Master_Table")
@Entity
@Table(name = "loc_district_master")
public class LocDistrictMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "district_name", length = 100, nullable = false)
    private String districtName;

    @NotNull
    @Size(max = 250)
    @Column(name = "description", length = 250, nullable = false)
    private String description;

    @NotNull
    @Column(name = "district_id", nullable = false)
    private Integer districtId;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_approved", length = 1)
    private String isApproved;

    @Column(name = "seq_order")
    private Integer seqOrder;

    @OneToMany(mappedBy = "districtId")
    private Set<LocCityMaster> locCityMasters = new HashSet<>();

    @OneToMany(mappedBy = "districtId")
    private Set<LocPincodeMaster> locPincodeMasters = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "locDistrictMasters", allowSetters = true)
    private LocStateMaster stateId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public LocDistrictMaster districtName(String districtName) {
        this.districtName = districtName;
        return this;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDescription() {
        return description;
    }

    public LocDistrictMaster description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public LocDistrictMaster districtId(Integer districtId) {
        this.districtId = districtId;
        return this;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocDistrictMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocDistrictMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocDistrictMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocDistrictMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocDistrictMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public LocDistrictMaster isApproved(String isApproved) {
        this.isApproved = isApproved;
        return this;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public LocDistrictMaster seqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
        return this;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Set<LocCityMaster> getLocCityMasters() {
        return locCityMasters;
    }

    public LocDistrictMaster locCityMasters(Set<LocCityMaster> locCityMasters) {
        this.locCityMasters = locCityMasters;
        return this;
    }

    public LocDistrictMaster addLocCityMaster(LocCityMaster locCityMaster) {
        this.locCityMasters.add(locCityMaster);
        locCityMaster.setDistrictId(this);
        return this;
    }

    public LocDistrictMaster removeLocCityMaster(LocCityMaster locCityMaster) {
        this.locCityMasters.remove(locCityMaster);
        locCityMaster.setDistrictId(null);
        return this;
    }

    public void setLocCityMasters(Set<LocCityMaster> locCityMasters) {
        this.locCityMasters = locCityMasters;
    }

    public Set<LocPincodeMaster> getLocPincodeMasters() {
        return locPincodeMasters;
    }

    public LocDistrictMaster locPincodeMasters(Set<LocPincodeMaster> locPincodeMasters) {
        this.locPincodeMasters = locPincodeMasters;
        return this;
    }

    public LocDistrictMaster addLocPincodeMaster(LocPincodeMaster locPincodeMaster) {
        this.locPincodeMasters.add(locPincodeMaster);
        locPincodeMaster.setDistrictId(this);
        return this;
    }

    public LocDistrictMaster removeLocPincodeMaster(LocPincodeMaster locPincodeMaster) {
        this.locPincodeMasters.remove(locPincodeMaster);
        locPincodeMaster.setDistrictId(null);
        return this;
    }

    public void setLocPincodeMasters(Set<LocPincodeMaster> locPincodeMasters) {
        this.locPincodeMasters = locPincodeMasters;
    }

    public LocStateMaster getStateId() {
        return stateId;
    }

    public LocDistrictMaster stateId(LocStateMaster locStateMaster) {
        this.stateId = locStateMaster;
        return this;
    }

    public void setStateId(LocStateMaster locStateMaster) {
        this.stateId = locStateMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocDistrictMaster)) {
            return false;
        }
        return id != null && id.equals(((LocDistrictMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocDistrictMaster{" +
            "id=" + getId() +
            ", districtName='" + getDistrictName() + "'" +
            ", description='" + getDescription() + "'" +
            ", districtId=" + getDistrictId() +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            "}";
    }
}
