package com.crisil.onesource.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * VIEW
 */
@ApiModel(description = "VIEW")
@Entity
@Table(name = "vw_contact_gst_details")
public class VwContactGstDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "company_record_id", length = 100)
    private String companyRecordId;

    @Size(max = 100)
    @Column(name = "company_name", length = 100)
    private String companyName;

    @NotNull
    @Size(max = 50)
    @Column(name = "company_code", length = 50, nullable = false)
    private String companyCode;

    @Size(max = 25)
    @Column(name = "gst_number", length = 25)
    private String gstNumber;

    @Size(max = 1)
    @Column(name = "is_sez_contact_flag", length = 1)
    private String isSezContactFlag;

    @Size(max = 100)
    @Column(name = "state_name", length = 100)
    private String stateName;

    @Size(max = 0)
    @Column(name = "gst_state_code", length = 0)
    private String gstStateCode;

    @NotNull
    @Size(max = 150)
    @Column(name = "contact_id", length = 150, nullable = false)
    private String contactId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyRecordId() {
        return companyRecordId;
    }

    public VwContactGstDetails companyRecordId(String companyRecordId) {
        this.companyRecordId = companyRecordId;
        return this;
    }

    public void setCompanyRecordId(String companyRecordId) {
        this.companyRecordId = companyRecordId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public VwContactGstDetails companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public VwContactGstDetails companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public VwContactGstDetails gstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
        return this;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getIsSezContactFlag() {
        return isSezContactFlag;
    }

    public VwContactGstDetails isSezContactFlag(String isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
        return this;
    }

    public void setIsSezContactFlag(String isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
    }

    public String getStateName() {
        return stateName;
    }

    public VwContactGstDetails stateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getGstStateCode() {
        return gstStateCode;
    }

    public VwContactGstDetails gstStateCode(String gstStateCode) {
        this.gstStateCode = gstStateCode;
        return this;
    }

    public void setGstStateCode(String gstStateCode) {
        this.gstStateCode = gstStateCode;
    }

    public String getContactId() {
        return contactId;
    }

    public VwContactGstDetails contactId(String contactId) {
        this.contactId = contactId;
        return this;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VwContactGstDetails)) {
            return false;
        }
        return id != null && id.equals(((VwContactGstDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VwContactGstDetails{" +
            "id=" + getId() +
            ", companyRecordId='" + getCompanyRecordId() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            ", gstNumber='" + getGstNumber() + "'" +
            ", isSezContactFlag='" + getIsSezContactFlag() + "'" +
            ", stateName='" + getStateName() + "'" +
            ", gstStateCode='" + getGstStateCode() + "'" +
            ", contactId='" + getContactId() + "'" +
            "}";
    }
}
