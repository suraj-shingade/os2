package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A GroupMaster.
 */
@Entity
@Table(name = "group_master")
public class GroupMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "group_id", nullable = false)
    private Integer groupId;

    @Size(max = 100)
    @Column(name = "group_name", length = 100)
    private String groupName;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @OneToMany(mappedBy = "groupId")
    private Set<OnesourceCompanyMaster> onesourceCompanyMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public GroupMaster groupId(Integer groupId) {
        this.groupId = groupId;
        return this;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public GroupMaster groupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public GroupMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public GroupMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public GroupMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public GroupMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public GroupMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<OnesourceCompanyMaster> getOnesourceCompanyMasters() {
        return onesourceCompanyMasters;
    }

    public GroupMaster onesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
        return this;
    }

    public GroupMaster addOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.add(onesourceCompanyMaster);
        onesourceCompanyMaster.setGroupId(this);
        return this;
    }

    public GroupMaster removeOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.remove(onesourceCompanyMaster);
        onesourceCompanyMaster.setGroupId(null);
        return this;
    }

    public void setOnesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GroupMaster)) {
            return false;
        }
        return id != null && id.equals(((GroupMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GroupMaster{" +
            "id=" + getId() +
            ", groupId=" + getGroupId() +
            ", groupName='" + getGroupName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
