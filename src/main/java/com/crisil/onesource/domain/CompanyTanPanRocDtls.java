package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A CompanyTanPanRocDtls.
 */
@Entity
@Table(name = "company_tan_pan_roc_dtls")
public class CompanyTanPanRocDtls implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 20)
    @Column(name = "tan", length = 20)
    private String tan;

    @Size(max = 20)
    @Column(name = "pan", length = 20)
    private String pan;

    @Size(max = 25)
    @Column(name = "roc", length = 25)
    private String roc;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 150)
    @Column(name = "created_by", length = 150)
    private String createdBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 150)
    @Column(name = "last_modified_by", length = 150)
    private String lastModifiedBy;

    @Column(name = "company_risk_status_id")
    private Integer companyRiskStatusId;

    @Size(max = 500)
    @Column(name = "company_rist_comments", length = 500)
    private String companyRistComments;

    @Size(max = 4000)
    @Column(name = "company_background", length = 4000)
    private String companyBackground;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 20)
    @Column(name = "rpt_flag", length = 20)
    private String rptFlag;

    @Size(max = 1)
    @Column(name = "gst_exempt", length = 1)
    private String gstExempt;

    @Size(max = 1)
    @Column(name = "gst_number_not_applicable", length = 1)
    private String gstNumberNotApplicable;

    @Size(max = 50)
    @Column(name = "company_code", length = 50)
    private String companyCode;

    @ManyToOne
    @JsonIgnoreProperties(value = "companyTanPanRocDtls", allowSetters = true)
    private CompanyRequest record;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTan() {
        return tan;
    }

    public CompanyTanPanRocDtls tan(String tan) {
        this.tan = tan;
        return this;
    }

    public void setTan(String tan) {
        this.tan = tan;
    }

    public String getPan() {
        return pan;
    }

    public CompanyTanPanRocDtls pan(String pan) {
        this.pan = pan;
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getRoc() {
        return roc;
    }

    public CompanyTanPanRocDtls roc(String roc) {
        this.roc = roc;
        return this;
    }

    public void setRoc(String roc) {
        this.roc = roc;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public CompanyTanPanRocDtls createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyTanPanRocDtls createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyTanPanRocDtls lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyTanPanRocDtls lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getCompanyRiskStatusId() {
        return companyRiskStatusId;
    }

    public CompanyTanPanRocDtls companyRiskStatusId(Integer companyRiskStatusId) {
        this.companyRiskStatusId = companyRiskStatusId;
        return this;
    }

    public void setCompanyRiskStatusId(Integer companyRiskStatusId) {
        this.companyRiskStatusId = companyRiskStatusId;
    }

    public String getCompanyRistComments() {
        return companyRistComments;
    }

    public CompanyTanPanRocDtls companyRistComments(String companyRistComments) {
        this.companyRistComments = companyRistComments;
        return this;
    }

    public void setCompanyRistComments(String companyRistComments) {
        this.companyRistComments = companyRistComments;
    }

    public String getCompanyBackground() {
        return companyBackground;
    }

    public CompanyTanPanRocDtls companyBackground(String companyBackground) {
        this.companyBackground = companyBackground;
        return this;
    }

    public void setCompanyBackground(String companyBackground) {
        this.companyBackground = companyBackground;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyTanPanRocDtls isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRptFlag() {
        return rptFlag;
    }

    public CompanyTanPanRocDtls rptFlag(String rptFlag) {
        this.rptFlag = rptFlag;
        return this;
    }

    public void setRptFlag(String rptFlag) {
        this.rptFlag = rptFlag;
    }

    public String getGstExempt() {
        return gstExempt;
    }

    public CompanyTanPanRocDtls gstExempt(String gstExempt) {
        this.gstExempt = gstExempt;
        return this;
    }

    public void setGstExempt(String gstExempt) {
        this.gstExempt = gstExempt;
    }

    public String getGstNumberNotApplicable() {
        return gstNumberNotApplicable;
    }

    public CompanyTanPanRocDtls gstNumberNotApplicable(String gstNumberNotApplicable) {
        this.gstNumberNotApplicable = gstNumberNotApplicable;
        return this;
    }

    public void setGstNumberNotApplicable(String gstNumberNotApplicable) {
        this.gstNumberNotApplicable = gstNumberNotApplicable;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public CompanyTanPanRocDtls companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public CompanyRequest getRecord() {
        return record;
    }

    public CompanyTanPanRocDtls record(CompanyRequest companyRequest) {
        this.record = companyRequest;
        return this;
    }

    public void setRecord(CompanyRequest companyRequest) {
        this.record = companyRequest;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyTanPanRocDtls)) {
            return false;
        }
        return id != null && id.equals(((CompanyTanPanRocDtls) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyTanPanRocDtls{" +
            "id=" + getId() +
            ", tan='" + getTan() + "'" +
            ", pan='" + getPan() + "'" +
            ", roc='" + getRoc() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", companyRiskStatusId=" + getCompanyRiskStatusId() +
            ", companyRistComments='" + getCompanyRistComments() + "'" +
            ", companyBackground='" + getCompanyBackground() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", rptFlag='" + getRptFlag() + "'" +
            ", gstExempt='" + getGstExempt() + "'" +
            ", gstNumberNotApplicable='" + getGstNumberNotApplicable() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            "}";
    }
}
