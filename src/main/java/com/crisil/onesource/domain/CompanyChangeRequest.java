package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A CompanyChangeRequest.
 */
@Entity
@Table(name = "company_change_request")
public class CompanyChangeRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 200)
    @Column(name = "old_company_name", length = 200, nullable = false)
    private String oldCompanyName;

    @NotNull
    @Size(max = 200)
    @Column(name = "new_company_name", length = 200, nullable = false)
    private String newCompanyName;

    @NotNull
    @Size(max = 50)
    @Column(name = "new_company_code", length = 50, nullable = false)
    private String newCompanyCode;

    @NotNull
    @Size(max = 80)
    @Column(name = "status", length = 80, nullable = false)
    private String status;

    @NotNull
    @Column(name = "effective_date", nullable = false)
    private LocalDate effectiveDate;

    @NotNull
    @Size(max = 80)
    @Column(name = "requested_by", length = 80, nullable = false)
    private String requestedBy;

    @Size(max = 80)
    @Column(name = "company_change_id", length = 80)
    private String companyChangeId;

    @NotNull
    @Size(max = 80)
    @Column(name = "case_type", length = 80, nullable = false)
    private String caseType;

    @Size(max = 500)
    @Column(name = "reason", length = 500)
    private String reason;

    @Size(max = 500)
    @Column(name = "reason_description", length = 500)
    private String reasonDescription;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "companyChangeRequests", allowSetters = true)
    private OnesourceCompanyMaster oldCompanyCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOldCompanyName() {
        return oldCompanyName;
    }

    public CompanyChangeRequest oldCompanyName(String oldCompanyName) {
        this.oldCompanyName = oldCompanyName;
        return this;
    }

    public void setOldCompanyName(String oldCompanyName) {
        this.oldCompanyName = oldCompanyName;
    }

    public String getNewCompanyName() {
        return newCompanyName;
    }

    public CompanyChangeRequest newCompanyName(String newCompanyName) {
        this.newCompanyName = newCompanyName;
        return this;
    }

    public void setNewCompanyName(String newCompanyName) {
        this.newCompanyName = newCompanyName;
    }

    public String getNewCompanyCode() {
        return newCompanyCode;
    }

    public CompanyChangeRequest newCompanyCode(String newCompanyCode) {
        this.newCompanyCode = newCompanyCode;
        return this;
    }

    public void setNewCompanyCode(String newCompanyCode) {
        this.newCompanyCode = newCompanyCode;
    }

    public String getStatus() {
        return status;
    }

    public CompanyChangeRequest status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public CompanyChangeRequest effectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
        return this;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public CompanyChangeRequest requestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
        return this;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getCompanyChangeId() {
        return companyChangeId;
    }

    public CompanyChangeRequest companyChangeId(String companyChangeId) {
        this.companyChangeId = companyChangeId;
        return this;
    }

    public void setCompanyChangeId(String companyChangeId) {
        this.companyChangeId = companyChangeId;
    }

    public String getCaseType() {
        return caseType;
    }

    public CompanyChangeRequest caseType(String caseType) {
        this.caseType = caseType;
        return this;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getReason() {
        return reason;
    }

    public CompanyChangeRequest reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public CompanyChangeRequest reasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
        return this;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyChangeRequest createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public CompanyChangeRequest createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyChangeRequest lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyChangeRequest lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyChangeRequest isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public OnesourceCompanyMaster getOldCompanyCode() {
        return oldCompanyCode;
    }

    public CompanyChangeRequest oldCompanyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.oldCompanyCode = onesourceCompanyMaster;
        return this;
    }

    public void setOldCompanyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.oldCompanyCode = onesourceCompanyMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyChangeRequest)) {
            return false;
        }
        return id != null && id.equals(((CompanyChangeRequest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyChangeRequest{" +
            "id=" + getId() +
            ", oldCompanyName='" + getOldCompanyName() + "'" +
            ", newCompanyName='" + getNewCompanyName() + "'" +
            ", newCompanyCode='" + getNewCompanyCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", effectiveDate='" + getEffectiveDate() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", companyChangeId='" + getCompanyChangeId() + "'" +
            ", caseType='" + getCaseType() + "'" +
            ", reason='" + getReason() + "'" +
            ", reasonDescription='" + getReasonDescription() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
