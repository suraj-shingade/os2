package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A CompanyIsinMapping.
 */
@Entity
@Table(name = "company_isin_mapping")
public class CompanyIsinMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "isin_no", length = 50, nullable = false)
    private String isinNo;

    @Size(max = 10)
    @Column(name = "instrument_type", length = 10)
    private String instrumentType;

    @Size(max = 50)
    @Column(name = "instrument_sub_type", length = 50)
    private String instrumentSubType;

    @Column(name = "face_value")
    private Integer faceValue;

    @Size(max = 20)
    @Column(name = "status", length = 20)
    private String status;

    @Size(max = 10)
    @Column(name = "script_code", length = 10)
    private String scriptCode;

    @Size(max = 5)
    @Column(name = "script_source", length = 5)
    private String scriptSource;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = "companyIsinMappings", allowSetters = true)
    private OnesourceCompanyMaster companyCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsinNo() {
        return isinNo;
    }

    public CompanyIsinMapping isinNo(String isinNo) {
        this.isinNo = isinNo;
        return this;
    }

    public void setIsinNo(String isinNo) {
        this.isinNo = isinNo;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public CompanyIsinMapping instrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
        return this;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getInstrumentSubType() {
        return instrumentSubType;
    }

    public CompanyIsinMapping instrumentSubType(String instrumentSubType) {
        this.instrumentSubType = instrumentSubType;
        return this;
    }

    public void setInstrumentSubType(String instrumentSubType) {
        this.instrumentSubType = instrumentSubType;
    }

    public Integer getFaceValue() {
        return faceValue;
    }

    public CompanyIsinMapping faceValue(Integer faceValue) {
        this.faceValue = faceValue;
        return this;
    }

    public void setFaceValue(Integer faceValue) {
        this.faceValue = faceValue;
    }

    public String getStatus() {
        return status;
    }

    public CompanyIsinMapping status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScriptCode() {
        return scriptCode;
    }

    public CompanyIsinMapping scriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
        return this;
    }

    public void setScriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
    }

    public String getScriptSource() {
        return scriptSource;
    }

    public CompanyIsinMapping scriptSource(String scriptSource) {
        this.scriptSource = scriptSource;
        return this;
    }

    public void setScriptSource(String scriptSource) {
        this.scriptSource = scriptSource;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyIsinMapping createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public CompanyIsinMapping createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyIsinMapping lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyIsinMapping lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyIsinMapping isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public OnesourceCompanyMaster getCompanyCode() {
        return companyCode;
    }

    public CompanyIsinMapping companyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.companyCode = onesourceCompanyMaster;
        return this;
    }

    public void setCompanyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.companyCode = onesourceCompanyMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyIsinMapping)) {
            return false;
        }
        return id != null && id.equals(((CompanyIsinMapping) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyIsinMapping{" +
            "id=" + getId() +
            ", isinNo='" + getIsinNo() + "'" +
            ", instrumentType='" + getInstrumentType() + "'" +
            ", instrumentSubType='" + getInstrumentSubType() + "'" +
            ", faceValue=" + getFaceValue() +
            ", status='" + getStatus() + "'" +
            ", scriptCode='" + getScriptCode() + "'" +
            ", scriptSource='" + getScriptSource() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
