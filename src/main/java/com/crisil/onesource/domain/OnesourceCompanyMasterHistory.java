package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A OnesourceCompanyMasterHistory.
 */
@Entity
@Table(name = "onesource_company_master_history")
public class OnesourceCompanyMasterHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "company_code", length = 50, nullable = false)
    private String companyCode;

    @Size(max = 100)
    @Column(name = "company_name", length = 100)
    private String companyName;

    @Column(name = "company_type_id")
    private Integer companyTypeId;

    @Column(name = "sector_id")
    private Integer sectorId;

    @Column(name = "industry_id")
    private Integer industryId;

    @Column(name = "group_id")
    private Integer groupId;

    @Size(max = 100)
    @Column(name = "remarks", length = 100)
    private String remarks;

    @Size(max = 100)
    @Column(name = "record_id", length = 100)
    private String recordId;

    @Column(name = "status")
    private Integer status;

    @Size(max = 50)
    @Column(name = "pin", length = 50)
    private String pin;

    @Size(max = 50)
    @Column(name = "client_status", length = 50)
    private String clientStatus;

    @Size(max = 100)
    @Column(name = "display_company_name", length = 100)
    private String displayCompanyName;

    @Size(max = 1)
    @Column(name = "billing_type", length = 1)
    private String billingType;

    @Size(max = 1)
    @Column(name = "is_billable", length = 1)
    private String isBillable;

    @Size(max = 1)
    @Column(name = "listing_status", length = 1)
    private String listingStatus;

    @Column(name = "date_of_incooperation")
    private Instant dateOfIncooperation;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 50)
    @Column(name = "original_filename", length = 50)
    private String originalFilename;

    @Size(max = 500)
    @Column(name = "display_filename", length = 500)
    private String displayFilename;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public OnesourceCompanyMasterHistory companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public OnesourceCompanyMasterHistory companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyTypeId() {
        return companyTypeId;
    }

    public OnesourceCompanyMasterHistory companyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
        return this;
    }

    public void setCompanyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public OnesourceCompanyMasterHistory sectorId(Integer sectorId) {
        this.sectorId = sectorId;
        return this;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Integer getIndustryId() {
        return industryId;
    }

    public OnesourceCompanyMasterHistory industryId(Integer industryId) {
        this.industryId = industryId;
        return this;
    }

    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public OnesourceCompanyMasterHistory groupId(Integer groupId) {
        this.groupId = groupId;
        return this;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getRemarks() {
        return remarks;
    }

    public OnesourceCompanyMasterHistory remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRecordId() {
        return recordId;
    }

    public OnesourceCompanyMasterHistory recordId(String recordId) {
        this.recordId = recordId;
        return this;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Integer getStatus() {
        return status;
    }

    public OnesourceCompanyMasterHistory status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPin() {
        return pin;
    }

    public OnesourceCompanyMasterHistory pin(String pin) {
        this.pin = pin;
        return this;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getClientStatus() {
        return clientStatus;
    }

    public OnesourceCompanyMasterHistory clientStatus(String clientStatus) {
        this.clientStatus = clientStatus;
        return this;
    }

    public void setClientStatus(String clientStatus) {
        this.clientStatus = clientStatus;
    }

    public String getDisplayCompanyName() {
        return displayCompanyName;
    }

    public OnesourceCompanyMasterHistory displayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
        return this;
    }

    public void setDisplayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public String getBillingType() {
        return billingType;
    }

    public OnesourceCompanyMasterHistory billingType(String billingType) {
        this.billingType = billingType;
        return this;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getIsBillable() {
        return isBillable;
    }

    public OnesourceCompanyMasterHistory isBillable(String isBillable) {
        this.isBillable = isBillable;
        return this;
    }

    public void setIsBillable(String isBillable) {
        this.isBillable = isBillable;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public OnesourceCompanyMasterHistory listingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
        return this;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public Instant getDateOfIncooperation() {
        return dateOfIncooperation;
    }

    public OnesourceCompanyMasterHistory dateOfIncooperation(Instant dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
        return this;
    }

    public void setDateOfIncooperation(Instant dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public OnesourceCompanyMasterHistory createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public OnesourceCompanyMasterHistory lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public OnesourceCompanyMasterHistory createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public OnesourceCompanyMasterHistory lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public OnesourceCompanyMasterHistory isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public OnesourceCompanyMasterHistory originalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
        return this;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public OnesourceCompanyMasterHistory displayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
        return this;
    }

    public void setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnesourceCompanyMasterHistory)) {
            return false;
        }
        return id != null && id.equals(((OnesourceCompanyMasterHistory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OnesourceCompanyMasterHistory{" +
            "id=" + getId() +
            ", companyCode='" + getCompanyCode() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", companyTypeId=" + getCompanyTypeId() +
            ", sectorId=" + getSectorId() +
            ", industryId=" + getIndustryId() +
            ", groupId=" + getGroupId() +
            ", remarks='" + getRemarks() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", status=" + getStatus() +
            ", pin='" + getPin() + "'" +
            ", clientStatus='" + getClientStatus() + "'" +
            ", displayCompanyName='" + getDisplayCompanyName() + "'" +
            ", billingType='" + getBillingType() + "'" +
            ", isBillable='" + getIsBillable() + "'" +
            ", listingStatus='" + getListingStatus() + "'" +
            ", dateOfIncooperation='" + getDateOfIncooperation() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", originalFilename='" + getOriginalFilename() + "'" +
            ", displayFilename='" + getDisplayFilename() + "'" +
            "}";
    }
}
