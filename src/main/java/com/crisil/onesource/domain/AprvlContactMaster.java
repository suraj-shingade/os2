package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A AprvlContactMaster.
 */
@Entity
@Table(name = "aprvl_contact_master")
public class AprvlContactMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @Column(name = "lastname", length = 100)
    private String lastname;

    @Size(max = 100)
    @Column(name = "joinednewcompany", length = 100)
    private String joinednewcompany;

    @Column(name = "cityid")
    private Integer cityid;

    @Column(name = "hdeleted")
    private Integer hdeleted;

    @Size(max = 100)
    @Column(name = "recordid", length = 100)
    private String recordid;

    @Size(max = 100)
    @Column(name = "levelimportance", length = 100)
    private String levelimportance;

    @Size(max = 100)
    @Column(name = "department", length = 100)
    private String department;

    @Size(max = 100)
    @Column(name = "fax", length = 100)
    private String fax;

    @Size(max = 100)
    @Column(name = "email", length = 100)
    private String email;

    @Size(max = 5)
    @Column(name = "keyperson", length = 5)
    private String keyperson;

    @Size(max = 50)
    @Column(name = "companycode", length = 50)
    private String companycode;

    @Size(max = 100)
    @Column(name = "pincode", length = 100)
    private String pincode;

    @Lob
    @Column(name = "comments")
    private String comments;

    @Lob
    @Column(name = "address_3")
    private String address3;

    @Lob
    @Column(name = "address_2")
    private String address2;

    @Size(max = 100)
    @Column(name = "contact_full_name", length = 100)
    private String contactFullName;

    @Size(max = 100)
    @Column(name = "address_1", length = 100)
    private String address1;

    @Size(max = 50)
    @Column(name = "cityname", length = 50)
    private String cityname;

    @Size(max = 100)
    @Column(name = "countryname", length = 100)
    private String countryname;

    @Size(max = 100)
    @Column(name = "mobile", length = 100)
    private String mobile;

    @Size(max = 100)
    @Column(name = "statename", length = 100)
    private String statename;

    @Column(name = "modify_date")
    private LocalDate modifyDate;

    @Column(name = "ofaflag")
    private Integer ofaflag;

    @Size(max = 100)
    @Column(name = "firstname", length = 100)
    private String firstname;

    @Size(max = 100)
    @Column(name = "middlename", length = 100)
    private String middlename;

    @Size(max = 100)
    @Column(name = "removereason", length = 100)
    private String removereason;

    @Size(max = 5)
    @Column(name = "salutation", length = 5)
    private String salutation;

    @Size(max = 100)
    @Column(name = "designation", length = 100)
    private String designation;

    @Size(max = 15)
    @Column(name = "status", length = 15)
    private String status;

    @NotNull
    @Column(name = "ctlg_srno", nullable = false)
    private Integer ctlgSrno;

    @Size(max = 1)
    @Column(name = "ctlg_is_deleted", length = 1)
    private String ctlgIsDeleted;

    @Column(name = "json_store_id")
    private Long jsonStoreId;

    @Size(max = 1)
    @Column(name = "is_data_processed", length = 1)
    private String isDataProcessed;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 1)
    @Column(name = "operation", length = 1)
    private String operation;

    @Size(max = 150)
    @Column(name = "contact_id", length = 150)
    private String contactId;

    @Lob
    @Column(name = "phone")
    private String phone;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public AprvlContactMaster lastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getJoinednewcompany() {
        return joinednewcompany;
    }

    public AprvlContactMaster joinednewcompany(String joinednewcompany) {
        this.joinednewcompany = joinednewcompany;
        return this;
    }

    public void setJoinednewcompany(String joinednewcompany) {
        this.joinednewcompany = joinednewcompany;
    }

    public Integer getCityid() {
        return cityid;
    }

    public AprvlContactMaster cityid(Integer cityid) {
        this.cityid = cityid;
        return this;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public Integer getHdeleted() {
        return hdeleted;
    }

    public AprvlContactMaster hdeleted(Integer hdeleted) {
        this.hdeleted = hdeleted;
        return this;
    }

    public void setHdeleted(Integer hdeleted) {
        this.hdeleted = hdeleted;
    }

    public String getRecordid() {
        return recordid;
    }

    public AprvlContactMaster recordid(String recordid) {
        this.recordid = recordid;
        return this;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getLevelimportance() {
        return levelimportance;
    }

    public AprvlContactMaster levelimportance(String levelimportance) {
        this.levelimportance = levelimportance;
        return this;
    }

    public void setLevelimportance(String levelimportance) {
        this.levelimportance = levelimportance;
    }

    public String getDepartment() {
        return department;
    }

    public AprvlContactMaster department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getFax() {
        return fax;
    }

    public AprvlContactMaster fax(String fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public AprvlContactMaster email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKeyperson() {
        return keyperson;
    }

    public AprvlContactMaster keyperson(String keyperson) {
        this.keyperson = keyperson;
        return this;
    }

    public void setKeyperson(String keyperson) {
        this.keyperson = keyperson;
    }

    public String getCompanycode() {
        return companycode;
    }

    public AprvlContactMaster companycode(String companycode) {
        this.companycode = companycode;
        return this;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getPincode() {
        return pincode;
    }

    public AprvlContactMaster pincode(String pincode) {
        this.pincode = pincode;
        return this;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getComments() {
        return comments;
    }

    public AprvlContactMaster comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAddress3() {
        return address3;
    }

    public AprvlContactMaster address3(String address3) {
        this.address3 = address3;
        return this;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress2() {
        return address2;
    }

    public AprvlContactMaster address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getContactFullName() {
        return contactFullName;
    }

    public AprvlContactMaster contactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
        return this;
    }

    public void setContactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
    }

    public String getAddress1() {
        return address1;
    }

    public AprvlContactMaster address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCityname() {
        return cityname;
    }

    public AprvlContactMaster cityname(String cityname) {
        this.cityname = cityname;
        return this;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getCountryname() {
        return countryname;
    }

    public AprvlContactMaster countryname(String countryname) {
        this.countryname = countryname;
        return this;
    }

    public void setCountryname(String countryname) {
        this.countryname = countryname;
    }

    public String getMobile() {
        return mobile;
    }

    public AprvlContactMaster mobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatename() {
        return statename;
    }

    public AprvlContactMaster statename(String statename) {
        this.statename = statename;
        return this;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public LocalDate getModifyDate() {
        return modifyDate;
    }

    public AprvlContactMaster modifyDate(LocalDate modifyDate) {
        this.modifyDate = modifyDate;
        return this;
    }

    public void setModifyDate(LocalDate modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getOfaflag() {
        return ofaflag;
    }

    public AprvlContactMaster ofaflag(Integer ofaflag) {
        this.ofaflag = ofaflag;
        return this;
    }

    public void setOfaflag(Integer ofaflag) {
        this.ofaflag = ofaflag;
    }

    public String getFirstname() {
        return firstname;
    }

    public AprvlContactMaster firstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public AprvlContactMaster middlename(String middlename) {
        this.middlename = middlename;
        return this;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getRemovereason() {
        return removereason;
    }

    public AprvlContactMaster removereason(String removereason) {
        this.removereason = removereason;
        return this;
    }

    public void setRemovereason(String removereason) {
        this.removereason = removereason;
    }

    public String getSalutation() {
        return salutation;
    }

    public AprvlContactMaster salutation(String salutation) {
        this.salutation = salutation;
        return this;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getDesignation() {
        return designation;
    }

    public AprvlContactMaster designation(String designation) {
        this.designation = designation;
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getStatus() {
        return status;
    }

    public AprvlContactMaster status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public AprvlContactMaster ctlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
        return this;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public AprvlContactMaster ctlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
        return this;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public Long getJsonStoreId() {
        return jsonStoreId;
    }

    public AprvlContactMaster jsonStoreId(Long jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
        return this;
    }

    public void setJsonStoreId(Long jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public AprvlContactMaster isDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
        return this;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public AprvlContactMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public AprvlContactMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public AprvlContactMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public AprvlContactMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getOperation() {
        return operation;
    }

    public AprvlContactMaster operation(String operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getContactId() {
        return contactId;
    }

    public AprvlContactMaster contactId(String contactId) {
        this.contactId = contactId;
        return this;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getPhone() {
        return phone;
    }

    public AprvlContactMaster phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AprvlContactMaster)) {
            return false;
        }
        return id != null && id.equals(((AprvlContactMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AprvlContactMaster{" +
            "id=" + getId() +
            ", lastname='" + getLastname() + "'" +
            ", joinednewcompany='" + getJoinednewcompany() + "'" +
            ", cityid=" + getCityid() +
            ", hdeleted=" + getHdeleted() +
            ", recordid='" + getRecordid() + "'" +
            ", levelimportance='" + getLevelimportance() + "'" +
            ", department='" + getDepartment() + "'" +
            ", fax='" + getFax() + "'" +
            ", email='" + getEmail() + "'" +
            ", keyperson='" + getKeyperson() + "'" +
            ", companycode='" + getCompanycode() + "'" +
            ", pincode='" + getPincode() + "'" +
            ", comments='" + getComments() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", contactFullName='" + getContactFullName() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", cityname='" + getCityname() + "'" +
            ", countryname='" + getCountryname() + "'" +
            ", mobile='" + getMobile() + "'" +
            ", statename='" + getStatename() + "'" +
            ", modifyDate='" + getModifyDate() + "'" +
            ", ofaflag=" + getOfaflag() +
            ", firstname='" + getFirstname() + "'" +
            ", middlename='" + getMiddlename() + "'" +
            ", removereason='" + getRemovereason() + "'" +
            ", salutation='" + getSalutation() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", status='" + getStatus() + "'" +
            ", ctlgSrno=" + getCtlgSrno() +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", jsonStoreId=" + getJsonStoreId() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", operation='" + getOperation() + "'" +
            ", contactId='" + getContactId() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
