package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Company_Master_Table
 */
@ApiModel(description = "Company_Master_Table")
@Entity
@Table(name = "company_address_master")
public class CompanyAddressMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "company_code", length = 50)
    private String companyCode;

    @NotNull
    @Column(name = "address_type_id", nullable = false)
    private Integer addressTypeId;

    @Size(max = 1000)
    @Column(name = "address_1", length = 1000)
    private String address1;

    @Size(max = 1000)
    @Column(name = "address_2", length = 1000)
    private String address2;

    @Size(max = 1000)
    @Column(name = "address_3", length = 1000)
    private String address3;

    @Column(name = "city_id")
    private Integer cityId;

    @Size(max = 150)
    @Column(name = "phone_no", length = 150)
    private String phoneNo;

    @Size(max = 100)
    @Column(name = "mobile_no", length = 100)
    private String mobileNo;

    @Size(max = 100)
    @Column(name = "fax", length = 100)
    private String fax;

    @Size(max = 300)
    @Column(name = "email", length = 300)
    private String email;

    @Size(max = 200)
    @Column(name = "webpage", length = 200)
    private String webpage;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @NotNull
    @Column(name = "company_address_id", nullable = false)
    private Integer companyAddressId;

    @Size(max = 500)
    @Column(name = "address_source", length = 500)
    private String addressSource;

    @ManyToOne
    @JsonIgnoreProperties(value = "companyAddressMasters", allowSetters = true)
    private LocPincodeMaster pincode;

    @ManyToOne
    @JsonIgnoreProperties(value = "companyAddressMasters", allowSetters = true)
    private CompanyRequest record;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public CompanyAddressMaster companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Integer getAddressTypeId() {
        return addressTypeId;
    }

    public CompanyAddressMaster addressTypeId(Integer addressTypeId) {
        this.addressTypeId = addressTypeId;
        return this;
    }

    public void setAddressTypeId(Integer addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public String getAddress1() {
        return address1;
    }

    public CompanyAddressMaster address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public CompanyAddressMaster address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public CompanyAddressMaster address3(String address3) {
        this.address3 = address3;
        return this;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public Integer getCityId() {
        return cityId;
    }

    public CompanyAddressMaster cityId(Integer cityId) {
        this.cityId = cityId;
        return this;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public CompanyAddressMaster phoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
        return this;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public CompanyAddressMaster mobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFax() {
        return fax;
    }

    public CompanyAddressMaster fax(String fax) {
        this.fax = fax;
        return this;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public CompanyAddressMaster email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebpage() {
        return webpage;
    }

    public CompanyAddressMaster webpage(String webpage) {
        this.webpage = webpage;
        return this;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyAddressMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public CompanyAddressMaster createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyAddressMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyAddressMaster lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyAddressMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getCompanyAddressId() {
        return companyAddressId;
    }

    public CompanyAddressMaster companyAddressId(Integer companyAddressId) {
        this.companyAddressId = companyAddressId;
        return this;
    }

    public void setCompanyAddressId(Integer companyAddressId) {
        this.companyAddressId = companyAddressId;
    }

    public String getAddressSource() {
        return addressSource;
    }

    public CompanyAddressMaster addressSource(String addressSource) {
        this.addressSource = addressSource;
        return this;
    }

    public void setAddressSource(String addressSource) {
        this.addressSource = addressSource;
    }

    public LocPincodeMaster getPincode() {
        return pincode;
    }

    public CompanyAddressMaster pincode(LocPincodeMaster locPincodeMaster) {
        this.pincode = locPincodeMaster;
        return this;
    }

    public void setPincode(LocPincodeMaster locPincodeMaster) {
        this.pincode = locPincodeMaster;
    }

    public CompanyRequest getRecord() {
        return record;
    }

    public CompanyAddressMaster record(CompanyRequest companyRequest) {
        this.record = companyRequest;
        return this;
    }

    public void setRecord(CompanyRequest companyRequest) {
        this.record = companyRequest;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyAddressMaster)) {
            return false;
        }
        return id != null && id.equals(((CompanyAddressMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyAddressMaster{" +
            "id=" + getId() +
            ", companyCode='" + getCompanyCode() + "'" +
            ", addressTypeId=" + getAddressTypeId() +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", cityId=" + getCityId() +
            ", phoneNo='" + getPhoneNo() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            ", fax='" + getFax() + "'" +
            ", email='" + getEmail() + "'" +
            ", webpage='" + getWebpage() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", companyAddressId=" + getCompanyAddressId() +
            ", addressSource='" + getAddressSource() + "'" +
            "}";
    }
}
