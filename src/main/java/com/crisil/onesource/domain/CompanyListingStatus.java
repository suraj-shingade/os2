package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Approval Table for company_listing_status#SRC_OLD_ONESOURCE_5#EVT_COMPANY_LISTING_STATUS_151
 */
@ApiModel(description = "Approval Table for company_listing_status#SRC_OLD_ONESOURCE_5#EVT_COMPANY_LISTING_STATUS_151")
@Entity
@Table(name = "company_listing_status")
public class CompanyListingStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "listing_id")
    private Integer listingId;

    @Size(max = 50)
    @Column(name = "script_code", length = 50)
    private String scriptCode;

    @Size(max = 50)
    @Column(name = "exchange", length = 50)
    private String exchange;

    @Size(max = 50)
    @Column(name = "status", length = 50)
    private String status;

    @Column(name = "status_date")
    private LocalDate statusDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 50)
    @Column(name = "operation", length = 50)
    private String operation;

    @NotNull
    @Column(name = "ctlg_srno", nullable = false)
    private Integer ctlgSrno;

    @Column(name = "json_store_id")
    private Integer jsonStoreId;

    @Size(max = 1)
    @Column(name = "is_data_processed", length = 1)
    private String isDataProcessed;

    @NotNull
    @Size(max = 1)
    @Column(name = "ctlg_is_deleted", length = 1, nullable = false)
    private String ctlgIsDeleted;

    @ManyToOne
    @JsonIgnoreProperties(value = "companyListingStatuses", allowSetters = true)
    private OnesourceCompanyMaster companyCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getListingId() {
        return listingId;
    }

    public CompanyListingStatus listingId(Integer listingId) {
        this.listingId = listingId;
        return this;
    }

    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    public String getScriptCode() {
        return scriptCode;
    }

    public CompanyListingStatus scriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
        return this;
    }

    public void setScriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
    }

    public String getExchange() {
        return exchange;
    }

    public CompanyListingStatus exchange(String exchange) {
        this.exchange = exchange;
        return this;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getStatus() {
        return status;
    }

    public CompanyListingStatus status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getStatusDate() {
        return statusDate;
    }

    public CompanyListingStatus statusDate(LocalDate statusDate) {
        this.statusDate = statusDate;
        return this;
    }

    public void setStatusDate(LocalDate statusDate) {
        this.statusDate = statusDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanyListingStatus createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public CompanyListingStatus createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanyListingStatus lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanyListingStatus lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanyListingStatus isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOperation() {
        return operation;
    }

    public CompanyListingStatus operation(String operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public CompanyListingStatus ctlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
        return this;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public Integer getJsonStoreId() {
        return jsonStoreId;
    }

    public CompanyListingStatus jsonStoreId(Integer jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
        return this;
    }

    public void setJsonStoreId(Integer jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public CompanyListingStatus isDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
        return this;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public CompanyListingStatus ctlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
        return this;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public OnesourceCompanyMaster getCompanyCode() {
        return companyCode;
    }

    public CompanyListingStatus companyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.companyCode = onesourceCompanyMaster;
        return this;
    }

    public void setCompanyCode(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.companyCode = onesourceCompanyMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyListingStatus)) {
            return false;
        }
        return id != null && id.equals(((CompanyListingStatus) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyListingStatus{" +
            "id=" + getId() +
            ", listingId=" + getListingId() +
            ", scriptCode='" + getScriptCode() + "'" +
            ", exchange='" + getExchange() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusDate='" + getStatusDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", operation='" + getOperation() + "'" +
            ", ctlgSrno=" + getCtlgSrno() +
            ", jsonStoreId=" + getJsonStoreId() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            "}";
    }
}
