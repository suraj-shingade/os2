package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A OnesourceCompanyMaster.
 */
@Entity
@Table(name = "onesource_company_master")
public class OnesourceCompanyMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "company_code", length = 50, nullable = false)
    private String companyCode;

    @Size(max = 100)
    @Column(name = "company_name", length = 100)
    private String companyName;

    @Size(max = 100)
    @Column(name = "remarks", length = 100)
    private String remarks;

    @Size(max = 100)
    @Column(name = "record_id", length = 100)
    private String recordId;

    @Column(name = "status")
    private Integer status;

    @Size(max = 50)
    @Column(name = "client_status", length = 50)
    private String clientStatus;

    @Size(max = 100)
    @Column(name = "display_company_name", length = 100)
    private String displayCompanyName;

    @Size(max = 1)
    @Column(name = "billing_type", length = 1)
    private String billingType;

    @Size(max = 1)
    @Column(name = "is_billable", length = 1)
    private String isBillable;

    @Size(max = 1)
    @Column(name = "listing_status", length = 1)
    private String listingStatus;

    @Column(name = "date_of_incooperation")
    private Instant dateOfIncooperation;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 50)
    @Column(name = "original_filename", length = 50)
    private String originalFilename;

    @Size(max = 500)
    @Column(name = "display_filename", length = 500)
    private String displayFilename;

    @Size(max = 10)
    @Column(name = "company_status", length = 10)
    private String companyStatus;

    @OneToMany(mappedBy = "oldCompanyCode")
    private Set<CompanyChangeRequest> companyChangeRequests = new HashSet<>();

    @OneToMany(mappedBy = "companyCode")
    private Set<CompanyIsinMapping> companyIsinMappings = new HashSet<>();

    @OneToMany(mappedBy = "companyCode")
    private Set<CompanyListingStatus> companyListingStatuses = new HashSet<>();

    @OneToMany(mappedBy = "companyCode")
    private Set<ContactMaster> contactMasters = new HashSet<>();

    @OneToMany(mappedBy = "companyCode")
    private Set<KarzaCompInformation> karzaCompInformations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "onesourceCompanyMasters", allowSetters = true)
    private CompanyTypeMaster companyTypeId;

    @ManyToOne
    @JsonIgnoreProperties(value = "onesourceCompanyMasters", allowSetters = true)
    private CompanySectorMaster sectorId;

    @ManyToOne
    @JsonIgnoreProperties(value = "onesourceCompanyMasters", allowSetters = true)
    private IndustryMaster industryId;

    @ManyToOne
    @JsonIgnoreProperties(value = "onesourceCompanyMasters", allowSetters = true)
    private GroupMaster groupId;

    @ManyToOne
    @JsonIgnoreProperties(value = "onesourceCompanyMasters", allowSetters = true)
    private LocPincodeMaster pin;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public OnesourceCompanyMaster companyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public OnesourceCompanyMaster companyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRemarks() {
        return remarks;
    }

    public OnesourceCompanyMaster remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRecordId() {
        return recordId;
    }

    public OnesourceCompanyMaster recordId(String recordId) {
        this.recordId = recordId;
        return this;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Integer getStatus() {
        return status;
    }

    public OnesourceCompanyMaster status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getClientStatus() {
        return clientStatus;
    }

    public OnesourceCompanyMaster clientStatus(String clientStatus) {
        this.clientStatus = clientStatus;
        return this;
    }

    public void setClientStatus(String clientStatus) {
        this.clientStatus = clientStatus;
    }

    public String getDisplayCompanyName() {
        return displayCompanyName;
    }

    public OnesourceCompanyMaster displayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
        return this;
    }

    public void setDisplayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public String getBillingType() {
        return billingType;
    }

    public OnesourceCompanyMaster billingType(String billingType) {
        this.billingType = billingType;
        return this;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getIsBillable() {
        return isBillable;
    }

    public OnesourceCompanyMaster isBillable(String isBillable) {
        this.isBillable = isBillable;
        return this;
    }

    public void setIsBillable(String isBillable) {
        this.isBillable = isBillable;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public OnesourceCompanyMaster listingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
        return this;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public Instant getDateOfIncooperation() {
        return dateOfIncooperation;
    }

    public OnesourceCompanyMaster dateOfIncooperation(Instant dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
        return this;
    }

    public void setDateOfIncooperation(Instant dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public OnesourceCompanyMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public OnesourceCompanyMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public OnesourceCompanyMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public OnesourceCompanyMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public OnesourceCompanyMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public OnesourceCompanyMaster originalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
        return this;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public OnesourceCompanyMaster displayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
        return this;
    }

    public void setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public OnesourceCompanyMaster companyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
        return this;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public Set<CompanyChangeRequest> getCompanyChangeRequests() {
        return companyChangeRequests;
    }

    public OnesourceCompanyMaster companyChangeRequests(Set<CompanyChangeRequest> companyChangeRequests) {
        this.companyChangeRequests = companyChangeRequests;
        return this;
    }

    public OnesourceCompanyMaster addCompanyChangeRequest(CompanyChangeRequest companyChangeRequest) {
        this.companyChangeRequests.add(companyChangeRequest);
        companyChangeRequest.setOldCompanyCode(this);
        return this;
    }

    public OnesourceCompanyMaster removeCompanyChangeRequest(CompanyChangeRequest companyChangeRequest) {
        this.companyChangeRequests.remove(companyChangeRequest);
        companyChangeRequest.setOldCompanyCode(null);
        return this;
    }

    public void setCompanyChangeRequests(Set<CompanyChangeRequest> companyChangeRequests) {
        this.companyChangeRequests = companyChangeRequests;
    }

    public Set<CompanyIsinMapping> getCompanyIsinMappings() {
        return companyIsinMappings;
    }

    public OnesourceCompanyMaster companyIsinMappings(Set<CompanyIsinMapping> companyIsinMappings) {
        this.companyIsinMappings = companyIsinMappings;
        return this;
    }

    public OnesourceCompanyMaster addCompanyIsinMapping(CompanyIsinMapping companyIsinMapping) {
        this.companyIsinMappings.add(companyIsinMapping);
        companyIsinMapping.setCompanyCode(this);
        return this;
    }

    public OnesourceCompanyMaster removeCompanyIsinMapping(CompanyIsinMapping companyIsinMapping) {
        this.companyIsinMappings.remove(companyIsinMapping);
        companyIsinMapping.setCompanyCode(null);
        return this;
    }

    public void setCompanyIsinMappings(Set<CompanyIsinMapping> companyIsinMappings) {
        this.companyIsinMappings = companyIsinMappings;
    }

    public Set<CompanyListingStatus> getCompanyListingStatuses() {
        return companyListingStatuses;
    }

    public OnesourceCompanyMaster companyListingStatuses(Set<CompanyListingStatus> companyListingStatuses) {
        this.companyListingStatuses = companyListingStatuses;
        return this;
    }

    public OnesourceCompanyMaster addCompanyListingStatus(CompanyListingStatus companyListingStatus) {
        this.companyListingStatuses.add(companyListingStatus);
        companyListingStatus.setCompanyCode(this);
        return this;
    }

    public OnesourceCompanyMaster removeCompanyListingStatus(CompanyListingStatus companyListingStatus) {
        this.companyListingStatuses.remove(companyListingStatus);
        companyListingStatus.setCompanyCode(null);
        return this;
    }

    public void setCompanyListingStatuses(Set<CompanyListingStatus> companyListingStatuses) {
        this.companyListingStatuses = companyListingStatuses;
    }

    public Set<ContactMaster> getContactMasters() {
        return contactMasters;
    }

    public OnesourceCompanyMaster contactMasters(Set<ContactMaster> contactMasters) {
        this.contactMasters = contactMasters;
        return this;
    }

    public OnesourceCompanyMaster addContactMaster(ContactMaster contactMaster) {
        this.contactMasters.add(contactMaster);
        contactMaster.setCompanyCode(this);
        return this;
    }

    public OnesourceCompanyMaster removeContactMaster(ContactMaster contactMaster) {
        this.contactMasters.remove(contactMaster);
        contactMaster.setCompanyCode(null);
        return this;
    }

    public void setContactMasters(Set<ContactMaster> contactMasters) {
        this.contactMasters = contactMasters;
    }

    public Set<KarzaCompInformation> getKarzaCompInformations() {
        return karzaCompInformations;
    }

    public OnesourceCompanyMaster karzaCompInformations(Set<KarzaCompInformation> karzaCompInformations) {
        this.karzaCompInformations = karzaCompInformations;
        return this;
    }

    public OnesourceCompanyMaster addKarzaCompInformation(KarzaCompInformation karzaCompInformation) {
        this.karzaCompInformations.add(karzaCompInformation);
        karzaCompInformation.setCompanyCode(this);
        return this;
    }

    public OnesourceCompanyMaster removeKarzaCompInformation(KarzaCompInformation karzaCompInformation) {
        this.karzaCompInformations.remove(karzaCompInformation);
        karzaCompInformation.setCompanyCode(null);
        return this;
    }

    public void setKarzaCompInformations(Set<KarzaCompInformation> karzaCompInformations) {
        this.karzaCompInformations = karzaCompInformations;
    }

    public CompanyTypeMaster getCompanyTypeId() {
        return companyTypeId;
    }

    public OnesourceCompanyMaster companyTypeId(CompanyTypeMaster companyTypeMaster) {
        this.companyTypeId = companyTypeMaster;
        return this;
    }

    public void setCompanyTypeId(CompanyTypeMaster companyTypeMaster) {
        this.companyTypeId = companyTypeMaster;
    }

    public CompanySectorMaster getSectorId() {
        return sectorId;
    }

    public OnesourceCompanyMaster sectorId(CompanySectorMaster companySectorMaster) {
        this.sectorId = companySectorMaster;
        return this;
    }

    public void setSectorId(CompanySectorMaster companySectorMaster) {
        this.sectorId = companySectorMaster;
    }

    public IndustryMaster getIndustryId() {
        return industryId;
    }

    public OnesourceCompanyMaster industryId(IndustryMaster industryMaster) {
        this.industryId = industryMaster;
        return this;
    }

    public void setIndustryId(IndustryMaster industryMaster) {
        this.industryId = industryMaster;
    }

    public GroupMaster getGroupId() {
        return groupId;
    }

    public OnesourceCompanyMaster groupId(GroupMaster groupMaster) {
        this.groupId = groupMaster;
        return this;
    }

    public void setGroupId(GroupMaster groupMaster) {
        this.groupId = groupMaster;
    }

    public LocPincodeMaster getPin() {
        return pin;
    }

    public OnesourceCompanyMaster pin(LocPincodeMaster locPincodeMaster) {
        this.pin = locPincodeMaster;
        return this;
    }

    public void setPin(LocPincodeMaster locPincodeMaster) {
        this.pin = locPincodeMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnesourceCompanyMaster)) {
            return false;
        }
        return id != null && id.equals(((OnesourceCompanyMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OnesourceCompanyMaster{" +
            "id=" + getId() +
            ", companyCode='" + getCompanyCode() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", status=" + getStatus() +
            ", clientStatus='" + getClientStatus() + "'" +
            ", displayCompanyName='" + getDisplayCompanyName() + "'" +
            ", billingType='" + getBillingType() + "'" +
            ", isBillable='" + getIsBillable() + "'" +
            ", listingStatus='" + getListingStatus() + "'" +
            ", dateOfIncooperation='" + getDateOfIncooperation() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", originalFilename='" + getOriginalFilename() + "'" +
            ", displayFilename='" + getDisplayFilename() + "'" +
            ", companyStatus='" + getCompanyStatus() + "'" +
            "}";
    }
}
