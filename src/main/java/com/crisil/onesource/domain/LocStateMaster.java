package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Location_Master_Table
 */
@ApiModel(description = "Location_Master_Table")
@Entity
@Table(name = "loc_state_master")
public class LocStateMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "state_name", length = 100, nullable = false)
    private String stateName;

    @NotNull
    @Size(max = 250)
    @Column(name = "description", length = 250, nullable = false)
    private String description;

    @NotNull
    @Column(name = "state_id", nullable = false)
    private Integer stateId;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 25)
    @Column(name = "country_code", length = 25)
    private String countryCode;

    @Size(max = 30)
    @Column(name = "state_code", length = 30)
    private String stateCode;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_approved", length = 1)
    private String isApproved;

    @Column(name = "seq_order")
    private Integer seqOrder;

    @OneToMany(mappedBy = "stateId")
    private Set<LocCityMaster> locCityMasters = new HashSet<>();

    @OneToMany(mappedBy = "stateId")
    private Set<LocDistrictMaster> locDistrictMasters = new HashSet<>();

    @OneToMany(mappedBy = "stateId")
    private Set<LocPincodeMaster> locPincodeMasters = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "locStateMasters", allowSetters = true)
    private LocRegionMaster regionId;

    @ManyToOne
    @JsonIgnoreProperties(value = "locStateMasters", allowSetters = true)
    private LocCountryMaster country;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public LocStateMaster stateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDescription() {
        return description;
    }

    public LocStateMaster description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStateId() {
        return stateId;
    }

    public LocStateMaster stateId(Integer stateId) {
        this.stateId = stateId;
        return this;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocStateMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public LocStateMaster countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public LocStateMaster stateCode(String stateCode) {
        this.stateCode = stateCode;
        return this;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocStateMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocStateMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocStateMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocStateMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public LocStateMaster isApproved(String isApproved) {
        this.isApproved = isApproved;
        return this;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public LocStateMaster seqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
        return this;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Set<LocCityMaster> getLocCityMasters() {
        return locCityMasters;
    }

    public LocStateMaster locCityMasters(Set<LocCityMaster> locCityMasters) {
        this.locCityMasters = locCityMasters;
        return this;
    }

    public LocStateMaster addLocCityMaster(LocCityMaster locCityMaster) {
        this.locCityMasters.add(locCityMaster);
        locCityMaster.setStateId(this);
        return this;
    }

    public LocStateMaster removeLocCityMaster(LocCityMaster locCityMaster) {
        this.locCityMasters.remove(locCityMaster);
        locCityMaster.setStateId(null);
        return this;
    }

    public void setLocCityMasters(Set<LocCityMaster> locCityMasters) {
        this.locCityMasters = locCityMasters;
    }

    public Set<LocDistrictMaster> getLocDistrictMasters() {
        return locDistrictMasters;
    }

    public LocStateMaster locDistrictMasters(Set<LocDistrictMaster> locDistrictMasters) {
        this.locDistrictMasters = locDistrictMasters;
        return this;
    }

    public LocStateMaster addLocDistrictMaster(LocDistrictMaster locDistrictMaster) {
        this.locDistrictMasters.add(locDistrictMaster);
        locDistrictMaster.setStateId(this);
        return this;
    }

    public LocStateMaster removeLocDistrictMaster(LocDistrictMaster locDistrictMaster) {
        this.locDistrictMasters.remove(locDistrictMaster);
        locDistrictMaster.setStateId(null);
        return this;
    }

    public void setLocDistrictMasters(Set<LocDistrictMaster> locDistrictMasters) {
        this.locDistrictMasters = locDistrictMasters;
    }

    public Set<LocPincodeMaster> getLocPincodeMasters() {
        return locPincodeMasters;
    }

    public LocStateMaster locPincodeMasters(Set<LocPincodeMaster> locPincodeMasters) {
        this.locPincodeMasters = locPincodeMasters;
        return this;
    }

    public LocStateMaster addLocPincodeMaster(LocPincodeMaster locPincodeMaster) {
        this.locPincodeMasters.add(locPincodeMaster);
        locPincodeMaster.setStateId(this);
        return this;
    }

    public LocStateMaster removeLocPincodeMaster(LocPincodeMaster locPincodeMaster) {
        this.locPincodeMasters.remove(locPincodeMaster);
        locPincodeMaster.setStateId(null);
        return this;
    }

    public void setLocPincodeMasters(Set<LocPincodeMaster> locPincodeMasters) {
        this.locPincodeMasters = locPincodeMasters;
    }

    public LocRegionMaster getRegionId() {
        return regionId;
    }

    public LocStateMaster regionId(LocRegionMaster locRegionMaster) {
        this.regionId = locRegionMaster;
        return this;
    }

    public void setRegionId(LocRegionMaster locRegionMaster) {
        this.regionId = locRegionMaster;
    }

    public LocCountryMaster getCountry() {
        return country;
    }

    public LocStateMaster country(LocCountryMaster locCountryMaster) {
        this.country = locCountryMaster;
        return this;
    }

    public void setCountry(LocCountryMaster locCountryMaster) {
        this.country = locCountryMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocStateMaster)) {
            return false;
        }
        return id != null && id.equals(((LocStateMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocStateMaster{" +
            "id=" + getId() +
            ", stateName='" + getStateName() + "'" +
            ", description='" + getDescription() + "'" +
            ", stateId=" + getStateId() +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            ", stateCode='" + getStateCode() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            "}";
    }
}
