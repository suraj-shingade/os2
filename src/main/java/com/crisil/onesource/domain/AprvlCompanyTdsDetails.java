package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A AprvlCompanyTdsDetails.
 */
@Entity
@Table(name = "aprvl_company_tds_details")
public class AprvlCompanyTdsDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "ctlg_srno", nullable = false)
    private Integer ctlgSrno;

    @Size(max = 50)
    @Column(name = "recordid", length = 50)
    private String recordid;

    @Size(max = 50)
    @Column(name = "lineofbusiness", length = 50)
    private String lineofbusiness;

    @Size(max = 100)
    @Column(name = "filename", length = 100)
    private String filename;

    @Size(max = 50)
    @Column(name = "gstnumber", length = 50)
    private String gstnumber;

    @Lob
    @Column(name = "comments")
    private String comments;

    @Size(max = 10)
    @Column(name = "issezgst", length = 10)
    private String issezgst;

    @Size(max = 50)
    @Column(name = "previoustds", length = 50)
    private String previoustds;

    @Size(max = 50)
    @Column(name = "tdsnumber", length = 50)
    private String tdsnumber;

    @Column(name = "statecode")
    private Integer statecode;

    @Size(max = 1)
    @Column(name = "operation", length = 1)
    private String operation;

    @Size(max = 10)
    @Column(name = "hdeleted", length = 10)
    private String hdeleted;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "ctlg_is_deleted", length = 1)
    private String ctlgIsDeleted;

    @Column(name = "json_store_id")
    private Integer jsonStoreId;

    @Size(max = 1)
    @Column(name = "is_data_processed", length = 1)
    private String isDataProcessed;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public AprvlCompanyTdsDetails ctlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
        return this;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public String getRecordid() {
        return recordid;
    }

    public AprvlCompanyTdsDetails recordid(String recordid) {
        this.recordid = recordid;
        return this;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getLineofbusiness() {
        return lineofbusiness;
    }

    public AprvlCompanyTdsDetails lineofbusiness(String lineofbusiness) {
        this.lineofbusiness = lineofbusiness;
        return this;
    }

    public void setLineofbusiness(String lineofbusiness) {
        this.lineofbusiness = lineofbusiness;
    }

    public String getFilename() {
        return filename;
    }

    public AprvlCompanyTdsDetails filename(String filename) {
        this.filename = filename;
        return this;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getGstnumber() {
        return gstnumber;
    }

    public AprvlCompanyTdsDetails gstnumber(String gstnumber) {
        this.gstnumber = gstnumber;
        return this;
    }

    public void setGstnumber(String gstnumber) {
        this.gstnumber = gstnumber;
    }

    public String getComments() {
        return comments;
    }

    public AprvlCompanyTdsDetails comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIssezgst() {
        return issezgst;
    }

    public AprvlCompanyTdsDetails issezgst(String issezgst) {
        this.issezgst = issezgst;
        return this;
    }

    public void setIssezgst(String issezgst) {
        this.issezgst = issezgst;
    }

    public String getPrevioustds() {
        return previoustds;
    }

    public AprvlCompanyTdsDetails previoustds(String previoustds) {
        this.previoustds = previoustds;
        return this;
    }

    public void setPrevioustds(String previoustds) {
        this.previoustds = previoustds;
    }

    public String getTdsnumber() {
        return tdsnumber;
    }

    public AprvlCompanyTdsDetails tdsnumber(String tdsnumber) {
        this.tdsnumber = tdsnumber;
        return this;
    }

    public void setTdsnumber(String tdsnumber) {
        this.tdsnumber = tdsnumber;
    }

    public Integer getStatecode() {
        return statecode;
    }

    public AprvlCompanyTdsDetails statecode(Integer statecode) {
        this.statecode = statecode;
        return this;
    }

    public void setStatecode(Integer statecode) {
        this.statecode = statecode;
    }

    public String getOperation() {
        return operation;
    }

    public AprvlCompanyTdsDetails operation(String operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getHdeleted() {
        return hdeleted;
    }

    public AprvlCompanyTdsDetails hdeleted(String hdeleted) {
        this.hdeleted = hdeleted;
        return this;
    }

    public void setHdeleted(String hdeleted) {
        this.hdeleted = hdeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public AprvlCompanyTdsDetails createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public AprvlCompanyTdsDetails lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public AprvlCompanyTdsDetails createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public AprvlCompanyTdsDetails lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public AprvlCompanyTdsDetails ctlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
        return this;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public Integer getJsonStoreId() {
        return jsonStoreId;
    }

    public AprvlCompanyTdsDetails jsonStoreId(Integer jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
        return this;
    }

    public void setJsonStoreId(Integer jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public AprvlCompanyTdsDetails isDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
        return this;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AprvlCompanyTdsDetails)) {
            return false;
        }
        return id != null && id.equals(((AprvlCompanyTdsDetails) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AprvlCompanyTdsDetails{" +
            "id=" + getId() +
            ", ctlgSrno=" + getCtlgSrno() +
            ", recordid='" + getRecordid() + "'" +
            ", lineofbusiness='" + getLineofbusiness() + "'" +
            ", filename='" + getFilename() + "'" +
            ", gstnumber='" + getGstnumber() + "'" +
            ", comments='" + getComments() + "'" +
            ", issezgst='" + getIssezgst() + "'" +
            ", previoustds='" + getPrevioustds() + "'" +
            ", tdsnumber='" + getTdsnumber() + "'" +
            ", statecode=" + getStatecode() +
            ", operation='" + getOperation() + "'" +
            ", hdeleted='" + getHdeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", jsonStoreId=" + getJsonStoreId() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            "}";
    }
}
