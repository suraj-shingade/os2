package com.crisil.onesource.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * Location_Master_Table
 */
@ApiModel(description = "Location_Master_Table")
@Entity
@Table(name = "loc_country_master")
public class LocCountryMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "country_name", length = 100, nullable = false)
    private String countryName;

    @NotNull
    @Size(max = 25)
    @Column(name = "country_code", length = 25, nullable = false)
    private String countryCode;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Size(max = 100)
    @Column(name = "zip_code_format", length = 100)
    private String zipCodeFormat;

    @Size(max = 200)
    @Column(name = "continent_id", length = 200)
    private String continentId;

    @Size(max = 4000)
    @Column(name = "flag_image_path", length = 4000)
    private String flagImagePath;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @NotNull
    @Column(name = "country_id", nullable = false)
    private Integer countryId;

    @Size(max = 1)
    @Column(name = "is_approved", length = 1)
    private String isApproved;

    @Column(name = "seq_order")
    private Integer seqOrder;

    @OneToMany(mappedBy = "country")
    private Set<LocStateMaster> locStateMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public LocCountryMaster countryName(String countryName) {
        this.countryName = countryName;
        return this;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public LocCountryMaster countryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public LocCountryMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getZipCodeFormat() {
        return zipCodeFormat;
    }

    public LocCountryMaster zipCodeFormat(String zipCodeFormat) {
        this.zipCodeFormat = zipCodeFormat;
        return this;
    }

    public void setZipCodeFormat(String zipCodeFormat) {
        this.zipCodeFormat = zipCodeFormat;
    }

    public String getContinentId() {
        return continentId;
    }

    public LocCountryMaster continentId(String continentId) {
        this.continentId = continentId;
        return this;
    }

    public void setContinentId(String continentId) {
        this.continentId = continentId;
    }

    public String getFlagImagePath() {
        return flagImagePath;
    }

    public LocCountryMaster flagImagePath(String flagImagePath) {
        this.flagImagePath = flagImagePath;
        return this;
    }

    public void setFlagImagePath(String flagImagePath) {
        this.flagImagePath = flagImagePath;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public LocCountryMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public LocCountryMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public LocCountryMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public LocCountryMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public LocCountryMaster countryId(Integer countryId) {
        this.countryId = countryId;
        return this;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public LocCountryMaster isApproved(String isApproved) {
        this.isApproved = isApproved;
        return this;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public LocCountryMaster seqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
        return this;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Set<LocStateMaster> getLocStateMasters() {
        return locStateMasters;
    }

    public LocCountryMaster locStateMasters(Set<LocStateMaster> locStateMasters) {
        this.locStateMasters = locStateMasters;
        return this;
    }

    public LocCountryMaster addLocStateMaster(LocStateMaster locStateMaster) {
        this.locStateMasters.add(locStateMaster);
        locStateMaster.setCountry(this);
        return this;
    }

    public LocCountryMaster removeLocStateMaster(LocStateMaster locStateMaster) {
        this.locStateMasters.remove(locStateMaster);
        locStateMaster.setCountry(null);
        return this;
    }

    public void setLocStateMasters(Set<LocStateMaster> locStateMasters) {
        this.locStateMasters = locStateMasters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocCountryMaster)) {
            return false;
        }
        return id != null && id.equals(((LocCountryMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCountryMaster{" +
            "id=" + getId() +
            ", countryName='" + getCountryName() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", zipCodeFormat='" + getZipCodeFormat() + "'" +
            ", continentId='" + getContinentId() + "'" +
            ", flagImagePath='" + getFlagImagePath() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", countryId=" + getCountryId() +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            "}";
    }
}
