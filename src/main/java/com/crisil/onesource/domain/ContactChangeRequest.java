package com.crisil.onesource.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A ContactChangeRequest.
 */
@Entity
@Table(name = "contact_change_request")
public class ContactChangeRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 20)
    @Column(name = "request_status", length = 20)
    private String requestStatus;

    @Size(max = 50)
    @Column(name = "requester_comments", length = 50)
    private String requesterComments;

    @Size(max = 50)
    @Column(name = "requested_by", length = 50)
    private String requestedBy;

    @Column(name = "requested_date")
    private Instant requestedDate;

    @Size(max = 10)
    @Column(name = "approver_comments", length = 10)
    private String approverComments;

    @Size(max = 20)
    @Column(name = "approved_by", length = 20)
    private String approvedBy;

    @Column(name = "approved_date")
    private Instant approvedDate;

    @Size(max = 50)
    @Column(name = "file_name", length = 50)
    private String fileName;

    @Size(max = 1)
    @Column(name = "is_reverse_request", length = 1)
    private String isReverseRequest;

    @Size(max = 10)
    @Column(name = "request_type", length = 10)
    private String requestType;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactChangeRequests", allowSetters = true)
    private ContactMaster contactId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public ContactChangeRequest requestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
        return this;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequesterComments() {
        return requesterComments;
    }

    public ContactChangeRequest requesterComments(String requesterComments) {
        this.requesterComments = requesterComments;
        return this;
    }

    public void setRequesterComments(String requesterComments) {
        this.requesterComments = requesterComments;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public ContactChangeRequest requestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
        return this;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Instant getRequestedDate() {
        return requestedDate;
    }

    public ContactChangeRequest requestedDate(Instant requestedDate) {
        this.requestedDate = requestedDate;
        return this;
    }

    public void setRequestedDate(Instant requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getApproverComments() {
        return approverComments;
    }

    public ContactChangeRequest approverComments(String approverComments) {
        this.approverComments = approverComments;
        return this;
    }

    public void setApproverComments(String approverComments) {
        this.approverComments = approverComments;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public ContactChangeRequest approvedBy(String approvedBy) {
        this.approvedBy = approvedBy;
        return this;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Instant getApprovedDate() {
        return approvedDate;
    }

    public ContactChangeRequest approvedDate(Instant approvedDate) {
        this.approvedDate = approvedDate;
        return this;
    }

    public void setApprovedDate(Instant approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getFileName() {
        return fileName;
    }

    public ContactChangeRequest fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getIsReverseRequest() {
        return isReverseRequest;
    }

    public ContactChangeRequest isReverseRequest(String isReverseRequest) {
        this.isReverseRequest = isReverseRequest;
        return this;
    }

    public void setIsReverseRequest(String isReverseRequest) {
        this.isReverseRequest = isReverseRequest;
    }

    public String getRequestType() {
        return requestType;
    }

    public ContactChangeRequest requestType(String requestType) {
        this.requestType = requestType;
        return this;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public ContactChangeRequest createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public ContactChangeRequest lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public ContactChangeRequest lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public ContactChangeRequest isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public ContactMaster getContactId() {
        return contactId;
    }

    public ContactChangeRequest contactId(ContactMaster contactMaster) {
        this.contactId = contactMaster;
        return this;
    }

    public void setContactId(ContactMaster contactMaster) {
        this.contactId = contactMaster;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactChangeRequest)) {
            return false;
        }
        return id != null && id.equals(((ContactChangeRequest) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactChangeRequest{" +
            "id=" + getId() +
            ", requestStatus='" + getRequestStatus() + "'" +
            ", requesterComments='" + getRequesterComments() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", requestedDate='" + getRequestedDate() + "'" +
            ", approverComments='" + getApproverComments() + "'" +
            ", approvedBy='" + getApprovedBy() + "'" +
            ", approvedDate='" + getApprovedDate() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", isReverseRequest='" + getIsReverseRequest() + "'" +
            ", requestType='" + getRequestType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
