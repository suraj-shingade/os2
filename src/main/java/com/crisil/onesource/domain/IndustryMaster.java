package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A IndustryMaster.
 */
@Entity
@Table(name = "industry_master")
public class IndustryMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "industry_id", nullable = false)
    private Integer industryId;

    @Size(max = 100)
    @Column(name = "industry_name", length = 100)
    private String industryName;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @OneToMany(mappedBy = "industryId")
    private Set<OnesourceCompanyMaster> onesourceCompanyMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIndustryId() {
        return industryId;
    }

    public IndustryMaster industryId(Integer industryId) {
        this.industryId = industryId;
        return this;
    }

    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    public String getIndustryName() {
        return industryName;
    }

    public IndustryMaster industryName(String industryName) {
        this.industryName = industryName;
        return this;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public IndustryMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public IndustryMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public IndustryMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public IndustryMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public IndustryMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Set<OnesourceCompanyMaster> getOnesourceCompanyMasters() {
        return onesourceCompanyMasters;
    }

    public IndustryMaster onesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
        return this;
    }

    public IndustryMaster addOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.add(onesourceCompanyMaster);
        onesourceCompanyMaster.setIndustryId(this);
        return this;
    }

    public IndustryMaster removeOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.remove(onesourceCompanyMaster);
        onesourceCompanyMaster.setIndustryId(null);
        return this;
    }

    public void setOnesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IndustryMaster)) {
            return false;
        }
        return id != null && id.equals(((IndustryMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IndustryMaster{" +
            "id=" + getId() +
            ", industryId=" + getIndustryId() +
            ", industryName='" + getIndustryName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
