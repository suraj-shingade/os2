package com.crisil.onesource.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Company_Master_Table
 */
@ApiModel(description = "Company_Master_Table")
@Entity
@Table(name = "company_sector_master")
public class CompanySectorMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "sector_id", nullable = false)
    private Integer sectorId;

    @Size(max = 25)
    @Column(name = "sector_name", length = 25)
    private String sectorName;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    @Column(name = "last_modified_date")
    private LocalDate lastModifiedDate;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 100)
    @Column(name = "created_by", length = 100)
    private String createdBy;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Size(max = 50)
    @Column(name = "operation", length = 50)
    private String operation;

    @OneToMany(mappedBy = "sectorId")
    private Set<OnesourceCompanyMaster> onesourceCompanyMasters = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public CompanySectorMaster sectorId(Integer sectorId) {
        this.sectorId = sectorId;
        return this;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public String getSectorName() {
        return sectorName;
    }

    public CompanySectorMaster sectorName(String sectorName) {
        this.sectorName = sectorName;
        return this;
    }

    public void setSectorName(String sectorName) {
        this.sectorName = sectorName;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public CompanySectorMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public CompanySectorMaster lastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public CompanySectorMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public CompanySectorMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public CompanySectorMaster createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getOperation() {
        return operation;
    }

    public CompanySectorMaster operation(String operation) {
        this.operation = operation;
        return this;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Set<OnesourceCompanyMaster> getOnesourceCompanyMasters() {
        return onesourceCompanyMasters;
    }

    public CompanySectorMaster onesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
        return this;
    }

    public CompanySectorMaster addOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.add(onesourceCompanyMaster);
        onesourceCompanyMaster.setSectorId(this);
        return this;
    }

    public CompanySectorMaster removeOnesourceCompanyMaster(OnesourceCompanyMaster onesourceCompanyMaster) {
        this.onesourceCompanyMasters.remove(onesourceCompanyMaster);
        onesourceCompanyMaster.setSectorId(null);
        return this;
    }

    public void setOnesourceCompanyMasters(Set<OnesourceCompanyMaster> onesourceCompanyMasters) {
        this.onesourceCompanyMasters = onesourceCompanyMasters;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanySectorMaster)) {
            return false;
        }
        return id != null && id.equals(((CompanySectorMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanySectorMaster{" +
            "id=" + getId() +
            ", sectorId=" + getSectorId() +
            ", sectorName='" + getSectorName() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", operation='" + getOperation() + "'" +
            "}";
    }
}
