package com.crisil.onesource.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A BusinessAreaMaster.
 */
@Entity
@Table(name = "business_area_master")
public class BusinessAreaMaster implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "business_area_id", nullable = false)
    private Integer businessAreaId;

    @Size(max = 100)
    @Column(name = "business_area_name", length = 100)
    private String businessAreaName;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Size(max = 100)
    @Column(name = "last_modified_by", length = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    @Column(name = "is_deleted", length = 1)
    private String isDeleted;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    public BusinessAreaMaster businessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
        return this;
    }

    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public String getBusinessAreaName() {
        return businessAreaName;
    }

    public BusinessAreaMaster businessAreaName(String businessAreaName) {
        this.businessAreaName = businessAreaName;
        return this;
    }

    public void setBusinessAreaName(String businessAreaName) {
        this.businessAreaName = businessAreaName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public BusinessAreaMaster createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public BusinessAreaMaster createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public BusinessAreaMaster lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public BusinessAreaMaster lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public BusinessAreaMaster isDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
        return this;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BusinessAreaMaster)) {
            return false;
        }
        return id != null && id.equals(((BusinessAreaMaster) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BusinessAreaMaster{" +
            "id=" + getId() +
            ", businessAreaId=" + getBusinessAreaId() +
            ", businessAreaName='" + getBusinessAreaName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            "}";
    }
}
