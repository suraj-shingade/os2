package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocCityPincodeMapping;
import com.crisil.onesource.repository.LocCityPincodeMappingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocCityPincodeMapping}.
 */
@Service
@Transactional
public class LocCityPincodeMappingService {

    private final Logger log = LoggerFactory.getLogger(LocCityPincodeMappingService.class);

    private final LocCityPincodeMappingRepository locCityPincodeMappingRepository;

    public LocCityPincodeMappingService(LocCityPincodeMappingRepository locCityPincodeMappingRepository) {
        this.locCityPincodeMappingRepository = locCityPincodeMappingRepository;
    }

    /**
     * Save a locCityPincodeMapping.
     *
     * @param locCityPincodeMapping the entity to save.
     * @return the persisted entity.
     */
    public LocCityPincodeMapping save(LocCityPincodeMapping locCityPincodeMapping) {
        log.debug("Request to save LocCityPincodeMapping : {}", locCityPincodeMapping);
        return locCityPincodeMappingRepository.save(locCityPincodeMapping);
    }

    /**
     * Get all the locCityPincodeMappings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocCityPincodeMapping> findAll(Pageable pageable) {
        log.debug("Request to get all LocCityPincodeMappings");
        return locCityPincodeMappingRepository.findAll(pageable);
    }


    /**
     * Get one locCityPincodeMapping by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocCityPincodeMapping> findOne(Long id) {
        log.debug("Request to get LocCityPincodeMapping : {}", id);
        return locCityPincodeMappingRepository.findById(id);
    }

    /**
     * Delete the locCityPincodeMapping by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocCityPincodeMapping : {}", id);
        locCityPincodeMappingRepository.deleteById(id);
    }
}
