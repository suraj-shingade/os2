package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanySegmentMaster;
import com.crisil.onesource.repository.CompanySegmentMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanySegmentMaster}.
 */
@Service
@Transactional
public class CompanySegmentMasterService {

    private final Logger log = LoggerFactory.getLogger(CompanySegmentMasterService.class);

    private final CompanySegmentMasterRepository companySegmentMasterRepository;

    public CompanySegmentMasterService(CompanySegmentMasterRepository companySegmentMasterRepository) {
        this.companySegmentMasterRepository = companySegmentMasterRepository;
    }

    /**
     * Save a companySegmentMaster.
     *
     * @param companySegmentMaster the entity to save.
     * @return the persisted entity.
     */
    public CompanySegmentMaster save(CompanySegmentMaster companySegmentMaster) {
        log.debug("Request to save CompanySegmentMaster : {}", companySegmentMaster);
        return companySegmentMasterRepository.save(companySegmentMaster);
    }

    /**
     * Get all the companySegmentMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanySegmentMaster> findAll(Pageable pageable) {
        log.debug("Request to get all CompanySegmentMasters");
        return companySegmentMasterRepository.findAll(pageable);
    }


    /**
     * Get one companySegmentMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanySegmentMaster> findOne(Long id) {
        log.debug("Request to get CompanySegmentMaster : {}", id);
        return companySegmentMasterRepository.findById(id);
    }

    /**
     * Delete the companySegmentMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanySegmentMaster : {}", id);
        companySegmentMasterRepository.deleteById(id);
    }
}
