package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyTypeMaster;
import com.crisil.onesource.repository.CompanyTypeMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyTypeMaster}.
 */
@Service
@Transactional
public class CompanyTypeMasterService {

    private final Logger log = LoggerFactory.getLogger(CompanyTypeMasterService.class);

    private final CompanyTypeMasterRepository companyTypeMasterRepository;

    public CompanyTypeMasterService(CompanyTypeMasterRepository companyTypeMasterRepository) {
        this.companyTypeMasterRepository = companyTypeMasterRepository;
    }

    /**
     * Save a companyTypeMaster.
     *
     * @param companyTypeMaster the entity to save.
     * @return the persisted entity.
     */
    public CompanyTypeMaster save(CompanyTypeMaster companyTypeMaster) {
        log.debug("Request to save CompanyTypeMaster : {}", companyTypeMaster);
        return companyTypeMasterRepository.save(companyTypeMaster);
    }

    /**
     * Get all the companyTypeMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyTypeMaster> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyTypeMasters");
        return companyTypeMasterRepository.findAll(pageable);
    }


    /**
     * Get one companyTypeMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyTypeMaster> findOne(Long id) {
        log.debug("Request to get CompanyTypeMaster : {}", id);
        return companyTypeMasterRepository.findById(id);
    }

    /**
     * Delete the companyTypeMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyTypeMaster : {}", id);
        companyTypeMasterRepository.deleteById(id);
    }
}
