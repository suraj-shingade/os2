package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocRegionMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocRegionMasterRepository;
import com.crisil.onesource.service.dto.LocRegionMasterCriteria;

/**
 * Service for executing complex queries for {@link LocRegionMaster} entities in the database.
 * The main input is a {@link LocRegionMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocRegionMaster} or a {@link Page} of {@link LocRegionMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocRegionMasterQueryService extends QueryService<LocRegionMaster> {

    private final Logger log = LoggerFactory.getLogger(LocRegionMasterQueryService.class);

    private final LocRegionMasterRepository locRegionMasterRepository;

    public LocRegionMasterQueryService(LocRegionMasterRepository locRegionMasterRepository) {
        this.locRegionMasterRepository = locRegionMasterRepository;
    }

    /**
     * Return a {@link List} of {@link LocRegionMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocRegionMaster> findByCriteria(LocRegionMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocRegionMaster> specification = createSpecification(criteria);
        return locRegionMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocRegionMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocRegionMaster> findByCriteria(LocRegionMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocRegionMaster> specification = createSpecification(criteria);
        return locRegionMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocRegionMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocRegionMaster> specification = createSpecification(criteria);
        return locRegionMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link LocRegionMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocRegionMaster> createSpecification(LocRegionMasterCriteria criteria) {
        Specification<LocRegionMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocRegionMaster_.id));
            }
            if (criteria.getRegionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRegionId(), LocRegionMaster_.regionId));
            }
            if (criteria.getRegionName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegionName(), LocRegionMaster_.regionName));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), LocRegionMaster_.description));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocRegionMaster_.isDeleted));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocRegionMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocRegionMaster_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocRegionMaster_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocRegionMaster_.lastModifiedDate));
            }
            if (criteria.getIsApproved() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsApproved(), LocRegionMaster_.isApproved));
            }
            if (criteria.getSeqOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeqOrder(), LocRegionMaster_.seqOrder));
            }
            if (criteria.getLocStateMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocStateMasterId(),
                    root -> root.join(LocRegionMaster_.locStateMasters, JoinType.LEFT).get(LocStateMaster_.id)));
            }
        }
        return specification;
    }
}
