package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyAddressMaster;
import com.crisil.onesource.repository.CompanyAddressMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyAddressMaster}.
 */
@Service
@Transactional
public class CompanyAddressMasterService {

    private final Logger log = LoggerFactory.getLogger(CompanyAddressMasterService.class);

    private final CompanyAddressMasterRepository companyAddressMasterRepository;

    public CompanyAddressMasterService(CompanyAddressMasterRepository companyAddressMasterRepository) {
        this.companyAddressMasterRepository = companyAddressMasterRepository;
    }

    /**
     * Save a companyAddressMaster.
     *
     * @param companyAddressMaster the entity to save.
     * @return the persisted entity.
     */
    public CompanyAddressMaster save(CompanyAddressMaster companyAddressMaster) {
        log.debug("Request to save CompanyAddressMaster : {}", companyAddressMaster);
        return companyAddressMasterRepository.save(companyAddressMaster);
    }

    /**
     * Get all the companyAddressMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyAddressMaster> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyAddressMasters");
        return companyAddressMasterRepository.findAll(pageable);
    }


    /**
     * Get one companyAddressMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyAddressMaster> findOne(Long id) {
        log.debug("Request to get CompanyAddressMaster : {}", id);
        return companyAddressMasterRepository.findById(id);
    }

    /**
     * Delete the companyAddressMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyAddressMaster : {}", id);
        companyAddressMasterRepository.deleteById(id);
    }
}
