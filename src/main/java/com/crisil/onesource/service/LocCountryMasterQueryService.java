package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocCountryMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocCountryMasterRepository;
import com.crisil.onesource.service.dto.LocCountryMasterCriteria;

/**
 * Service for executing complex queries for {@link LocCountryMaster} entities in the database.
 * The main input is a {@link LocCountryMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocCountryMaster} or a {@link Page} of {@link LocCountryMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocCountryMasterQueryService extends QueryService<LocCountryMaster> {

    private final Logger log = LoggerFactory.getLogger(LocCountryMasterQueryService.class);

    private final LocCountryMasterRepository locCountryMasterRepository;

    public LocCountryMasterQueryService(LocCountryMasterRepository locCountryMasterRepository) {
        this.locCountryMasterRepository = locCountryMasterRepository;
    }

    /**
     * Return a {@link List} of {@link LocCountryMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocCountryMaster> findByCriteria(LocCountryMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocCountryMaster> specification = createSpecification(criteria);
        return locCountryMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocCountryMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocCountryMaster> findByCriteria(LocCountryMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocCountryMaster> specification = createSpecification(criteria);
        return locCountryMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocCountryMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocCountryMaster> specification = createSpecification(criteria);
        return locCountryMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link LocCountryMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocCountryMaster> createSpecification(LocCountryMasterCriteria criteria) {
        Specification<LocCountryMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocCountryMaster_.id));
            }
            if (criteria.getCountryName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryName(), LocCountryMaster_.countryName));
            }
            if (criteria.getCountryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryCode(), LocCountryMaster_.countryCode));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocCountryMaster_.isDeleted));
            }
            if (criteria.getZipCodeFormat() != null) {
                specification = specification.and(buildStringSpecification(criteria.getZipCodeFormat(), LocCountryMaster_.zipCodeFormat));
            }
            if (criteria.getContinentId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContinentId(), LocCountryMaster_.continentId));
            }
            if (criteria.getFlagImagePath() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFlagImagePath(), LocCountryMaster_.flagImagePath));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocCountryMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocCountryMaster_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocCountryMaster_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocCountryMaster_.lastModifiedDate));
            }
            if (criteria.getCountryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCountryId(), LocCountryMaster_.countryId));
            }
            if (criteria.getIsApproved() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsApproved(), LocCountryMaster_.isApproved));
            }
            if (criteria.getSeqOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeqOrder(), LocCountryMaster_.seqOrder));
            }
            if (criteria.getLocStateMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocStateMasterId(),
                    root -> root.join(LocCountryMaster_.locStateMasters, JoinType.LEFT).get(LocStateMaster_.id)));
            }
        }
        return specification;
    }
}
