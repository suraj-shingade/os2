package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyTypeMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyTypeMasterRepository;
import com.crisil.onesource.service.dto.CompanyTypeMasterCriteria;

/**
 * Service for executing complex queries for {@link CompanyTypeMaster} entities in the database.
 * The main input is a {@link CompanyTypeMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyTypeMaster} or a {@link Page} of {@link CompanyTypeMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyTypeMasterQueryService extends QueryService<CompanyTypeMaster> {

    private final Logger log = LoggerFactory.getLogger(CompanyTypeMasterQueryService.class);

    private final CompanyTypeMasterRepository companyTypeMasterRepository;

    public CompanyTypeMasterQueryService(CompanyTypeMasterRepository companyTypeMasterRepository) {
        this.companyTypeMasterRepository = companyTypeMasterRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyTypeMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyTypeMaster> findByCriteria(CompanyTypeMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyTypeMaster> specification = createSpecification(criteria);
        return companyTypeMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyTypeMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyTypeMaster> findByCriteria(CompanyTypeMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyTypeMaster> specification = createSpecification(criteria);
        return companyTypeMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyTypeMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyTypeMaster> specification = createSpecification(criteria);
        return companyTypeMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyTypeMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyTypeMaster> createSpecification(CompanyTypeMasterCriteria criteria) {
        Specification<CompanyTypeMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyTypeMaster_.id));
            }
            if (criteria.getCompanyTypeId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCompanyTypeId(), CompanyTypeMaster_.companyTypeId));
            }
            if (criteria.getCompanyType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyType(), CompanyTypeMaster_.companyType));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyTypeMaster_.isDeleted));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyTypeMaster_.lastModifiedDate));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyTypeMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyTypeMaster_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyTypeMaster_.lastModifiedBy));
            }
            if (criteria.getOnesourceCompanyMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getOnesourceCompanyMasterId(),
                    root -> root.join(CompanyTypeMaster_.onesourceCompanyMasters, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
