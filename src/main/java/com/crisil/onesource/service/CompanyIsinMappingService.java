package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyIsinMapping;
import com.crisil.onesource.repository.CompanyIsinMappingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyIsinMapping}.
 */
@Service
@Transactional
public class CompanyIsinMappingService {

    private final Logger log = LoggerFactory.getLogger(CompanyIsinMappingService.class);

    private final CompanyIsinMappingRepository companyIsinMappingRepository;

    public CompanyIsinMappingService(CompanyIsinMappingRepository companyIsinMappingRepository) {
        this.companyIsinMappingRepository = companyIsinMappingRepository;
    }

    /**
     * Save a companyIsinMapping.
     *
     * @param companyIsinMapping the entity to save.
     * @return the persisted entity.
     */
    public CompanyIsinMapping save(CompanyIsinMapping companyIsinMapping) {
        log.debug("Request to save CompanyIsinMapping : {}", companyIsinMapping);
        return companyIsinMappingRepository.save(companyIsinMapping);
    }

    /**
     * Get all the companyIsinMappings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyIsinMapping> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyIsinMappings");
        return companyIsinMappingRepository.findAll(pageable);
    }


    /**
     * Get one companyIsinMapping by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyIsinMapping> findOne(Long id) {
        log.debug("Request to get CompanyIsinMapping : {}", id);
        return companyIsinMappingRepository.findById(id);
    }

    /**
     * Delete the companyIsinMapping by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyIsinMapping : {}", id);
        companyIsinMappingRepository.deleteById(id);
    }
}
