package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyTanPanRocDtls;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyTanPanRocDtlsRepository;
import com.crisil.onesource.service.dto.CompanyTanPanRocDtlsCriteria;

/**
 * Service for executing complex queries for {@link CompanyTanPanRocDtls} entities in the database.
 * The main input is a {@link CompanyTanPanRocDtlsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyTanPanRocDtls} or a {@link Page} of {@link CompanyTanPanRocDtls} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyTanPanRocDtlsQueryService extends QueryService<CompanyTanPanRocDtls> {

    private final Logger log = LoggerFactory.getLogger(CompanyTanPanRocDtlsQueryService.class);

    private final CompanyTanPanRocDtlsRepository companyTanPanRocDtlsRepository;

    public CompanyTanPanRocDtlsQueryService(CompanyTanPanRocDtlsRepository companyTanPanRocDtlsRepository) {
        this.companyTanPanRocDtlsRepository = companyTanPanRocDtlsRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyTanPanRocDtls} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyTanPanRocDtls> findByCriteria(CompanyTanPanRocDtlsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyTanPanRocDtls> specification = createSpecification(criteria);
        return companyTanPanRocDtlsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyTanPanRocDtls} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyTanPanRocDtls> findByCriteria(CompanyTanPanRocDtlsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyTanPanRocDtls> specification = createSpecification(criteria);
        return companyTanPanRocDtlsRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyTanPanRocDtlsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyTanPanRocDtls> specification = createSpecification(criteria);
        return companyTanPanRocDtlsRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyTanPanRocDtlsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyTanPanRocDtls> createSpecification(CompanyTanPanRocDtlsCriteria criteria) {
        Specification<CompanyTanPanRocDtls> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyTanPanRocDtls_.id));
            }
            if (criteria.getTan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTan(), CompanyTanPanRocDtls_.tan));
            }
            if (criteria.getPan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPan(), CompanyTanPanRocDtls_.pan));
            }
            if (criteria.getRoc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRoc(), CompanyTanPanRocDtls_.roc));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyTanPanRocDtls_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyTanPanRocDtls_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyTanPanRocDtls_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyTanPanRocDtls_.lastModifiedBy));
            }
            if (criteria.getCompanyRiskStatusId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCompanyRiskStatusId(), CompanyTanPanRocDtls_.companyRiskStatusId));
            }
            if (criteria.getCompanyRistComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyRistComments(), CompanyTanPanRocDtls_.companyRistComments));
            }
            if (criteria.getCompanyBackground() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyBackground(), CompanyTanPanRocDtls_.companyBackground));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyTanPanRocDtls_.isDeleted));
            }
            if (criteria.getRptFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRptFlag(), CompanyTanPanRocDtls_.rptFlag));
            }
            if (criteria.getGstExempt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstExempt(), CompanyTanPanRocDtls_.gstExempt));
            }
            if (criteria.getGstNumberNotApplicable() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNumberNotApplicable(), CompanyTanPanRocDtls_.gstNumberNotApplicable));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), CompanyTanPanRocDtls_.companyCode));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecordId(),
                    root -> root.join(CompanyTanPanRocDtls_.record, JoinType.LEFT).get(CompanyRequest_.id)));
            }
        }
        return specification;
    }
}
