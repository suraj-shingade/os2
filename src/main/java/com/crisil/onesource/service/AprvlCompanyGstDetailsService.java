package com.crisil.onesource.service;

import com.crisil.onesource.domain.AprvlCompanyGstDetails;
import com.crisil.onesource.repository.AprvlCompanyGstDetailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AprvlCompanyGstDetails}.
 */
@Service
@Transactional
public class AprvlCompanyGstDetailsService {

    private final Logger log = LoggerFactory.getLogger(AprvlCompanyGstDetailsService.class);

    private final AprvlCompanyGstDetailsRepository aprvlCompanyGstDetailsRepository;

    public AprvlCompanyGstDetailsService(AprvlCompanyGstDetailsRepository aprvlCompanyGstDetailsRepository) {
        this.aprvlCompanyGstDetailsRepository = aprvlCompanyGstDetailsRepository;
    }

    /**
     * Save a aprvlCompanyGstDetails.
     *
     * @param aprvlCompanyGstDetails the entity to save.
     * @return the persisted entity.
     */
    public AprvlCompanyGstDetails save(AprvlCompanyGstDetails aprvlCompanyGstDetails) {
        log.debug("Request to save AprvlCompanyGstDetails : {}", aprvlCompanyGstDetails);
        return aprvlCompanyGstDetailsRepository.save(aprvlCompanyGstDetails);
    }

    /**
     * Get all the aprvlCompanyGstDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AprvlCompanyGstDetails> findAll(Pageable pageable) {
        log.debug("Request to get all AprvlCompanyGstDetails");
        return aprvlCompanyGstDetailsRepository.findAll(pageable);
    }


    /**
     * Get one aprvlCompanyGstDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AprvlCompanyGstDetails> findOne(Long id) {
        log.debug("Request to get AprvlCompanyGstDetails : {}", id);
        return aprvlCompanyGstDetailsRepository.findById(id);
    }

    /**
     * Delete the aprvlCompanyGstDetails by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AprvlCompanyGstDetails : {}", id);
        aprvlCompanyGstDetailsRepository.deleteById(id);
    }
}
