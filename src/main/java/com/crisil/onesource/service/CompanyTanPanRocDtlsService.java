package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyTanPanRocDtls;
import com.crisil.onesource.repository.CompanyTanPanRocDtlsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyTanPanRocDtls}.
 */
@Service
@Transactional
public class CompanyTanPanRocDtlsService {

    private final Logger log = LoggerFactory.getLogger(CompanyTanPanRocDtlsService.class);

    private final CompanyTanPanRocDtlsRepository companyTanPanRocDtlsRepository;

    public CompanyTanPanRocDtlsService(CompanyTanPanRocDtlsRepository companyTanPanRocDtlsRepository) {
        this.companyTanPanRocDtlsRepository = companyTanPanRocDtlsRepository;
    }

    /**
     * Save a companyTanPanRocDtls.
     *
     * @param companyTanPanRocDtls the entity to save.
     * @return the persisted entity.
     */
    public CompanyTanPanRocDtls save(CompanyTanPanRocDtls companyTanPanRocDtls) {
        log.debug("Request to save CompanyTanPanRocDtls : {}", companyTanPanRocDtls);
        return companyTanPanRocDtlsRepository.save(companyTanPanRocDtls);
    }

    /**
     * Get all the companyTanPanRocDtls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyTanPanRocDtls> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyTanPanRocDtls");
        return companyTanPanRocDtlsRepository.findAll(pageable);
    }


    /**
     * Get one companyTanPanRocDtls by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyTanPanRocDtls> findOne(Long id) {
        log.debug("Request to get CompanyTanPanRocDtls : {}", id);
        return companyTanPanRocDtlsRepository.findById(id);
    }

    /**
     * Delete the companyTanPanRocDtls by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyTanPanRocDtls : {}", id);
        companyTanPanRocDtlsRepository.deleteById(id);
    }
}
