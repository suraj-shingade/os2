package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanySectorMaster;
import com.crisil.onesource.repository.CompanySectorMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanySectorMaster}.
 */
@Service
@Transactional
public class CompanySectorMasterService {

    private final Logger log = LoggerFactory.getLogger(CompanySectorMasterService.class);

    private final CompanySectorMasterRepository companySectorMasterRepository;

    public CompanySectorMasterService(CompanySectorMasterRepository companySectorMasterRepository) {
        this.companySectorMasterRepository = companySectorMasterRepository;
    }

    /**
     * Save a companySectorMaster.
     *
     * @param companySectorMaster the entity to save.
     * @return the persisted entity.
     */
    public CompanySectorMaster save(CompanySectorMaster companySectorMaster) {
        log.debug("Request to save CompanySectorMaster : {}", companySectorMaster);
        return companySectorMasterRepository.save(companySectorMaster);
    }

    /**
     * Get all the companySectorMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanySectorMaster> findAll(Pageable pageable) {
        log.debug("Request to get all CompanySectorMasters");
        return companySectorMasterRepository.findAll(pageable);
    }


    /**
     * Get one companySectorMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanySectorMaster> findOne(Long id) {
        log.debug("Request to get CompanySectorMaster : {}", id);
        return companySectorMasterRepository.findById(id);
    }

    /**
     * Delete the companySectorMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanySectorMaster : {}", id);
        companySectorMasterRepository.deleteById(id);
    }
}
