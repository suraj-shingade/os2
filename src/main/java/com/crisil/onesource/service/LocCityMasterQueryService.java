package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocCityMasterRepository;
import com.crisil.onesource.service.dto.LocCityMasterCriteria;

/**
 * Service for executing complex queries for {@link LocCityMaster} entities in the database.
 * The main input is a {@link LocCityMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocCityMaster} or a {@link Page} of {@link LocCityMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocCityMasterQueryService extends QueryService<LocCityMaster> {

    private final Logger log = LoggerFactory.getLogger(LocCityMasterQueryService.class);

    private final LocCityMasterRepository locCityMasterRepository;

    public LocCityMasterQueryService(LocCityMasterRepository locCityMasterRepository) {
        this.locCityMasterRepository = locCityMasterRepository;
    }

    /**
     * Return a {@link List} of {@link LocCityMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocCityMaster> findByCriteria(LocCityMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocCityMaster> specification = createSpecification(criteria);
        return locCityMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocCityMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocCityMaster> findByCriteria(LocCityMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocCityMaster> specification = createSpecification(criteria);
        return locCityMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocCityMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocCityMaster> specification = createSpecification(criteria);
        return locCityMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link LocCityMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocCityMaster> createSpecification(LocCityMasterCriteria criteria) {
        Specification<LocCityMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocCityMaster_.id));
            }
            if (criteria.getCityName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCityName(), LocCityMaster_.cityName));
            }
            if (criteria.getCityId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCityId(), LocCityMaster_.cityId));
            }
            if (criteria.getStateName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateName(), LocCityMaster_.stateName));
            }
            if (criteria.getCountryName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryName(), LocCityMaster_.countryName));
            }
            if (criteria.getPinNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPinNum(), LocCityMaster_.pinNum));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocCityMaster_.isDeleted));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordId(), LocCityMaster_.recordId));
            }
            if (criteria.getCountryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryCode(), LocCityMaster_.countryCode));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocCityMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocCityMaster_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocCityMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocCityMaster_.lastModifiedBy));
            }
            if (criteria.getSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSrno(), LocCityMaster_.srno));
            }
            if (criteria.getIsApproved() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsApproved(), LocCityMaster_.isApproved));
            }
            if (criteria.getSeqOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeqOrder(), LocCityMaster_.seqOrder));
            }
            if (criteria.getLocCityPincodeMappingId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocCityPincodeMappingId(),
                    root -> root.join(LocCityMaster_.locCityPincodeMappings, JoinType.LEFT).get(LocCityPincodeMapping_.id)));
            }
            if (criteria.getStateIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getStateIdId(),
                    root -> root.join(LocCityMaster_.stateId, JoinType.LEFT).get(LocStateMaster_.id)));
            }
            if (criteria.getDistrictIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getDistrictIdId(),
                    root -> root.join(LocCityMaster_.districtId, JoinType.LEFT).get(LocDistrictMaster_.id)));
            }
        }
        return specification;
    }
}
