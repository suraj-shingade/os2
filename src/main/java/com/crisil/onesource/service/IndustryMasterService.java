package com.crisil.onesource.service;

import com.crisil.onesource.domain.IndustryMaster;
import com.crisil.onesource.repository.IndustryMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link IndustryMaster}.
 */
@Service
@Transactional
public class IndustryMasterService {

    private final Logger log = LoggerFactory.getLogger(IndustryMasterService.class);

    private final IndustryMasterRepository industryMasterRepository;

    public IndustryMasterService(IndustryMasterRepository industryMasterRepository) {
        this.industryMasterRepository = industryMasterRepository;
    }

    /**
     * Save a industryMaster.
     *
     * @param industryMaster the entity to save.
     * @return the persisted entity.
     */
    public IndustryMaster save(IndustryMaster industryMaster) {
        log.debug("Request to save IndustryMaster : {}", industryMaster);
        return industryMasterRepository.save(industryMaster);
    }

    /**
     * Get all the industryMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IndustryMaster> findAll(Pageable pageable) {
        log.debug("Request to get all IndustryMasters");
        return industryMasterRepository.findAll(pageable);
    }


    /**
     * Get one industryMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndustryMaster> findOne(Long id) {
        log.debug("Request to get IndustryMaster : {}", id);
        return industryMasterRepository.findById(id);
    }

    /**
     * Delete the industryMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IndustryMaster : {}", id);
        industryMasterRepository.deleteById(id);
    }
}
