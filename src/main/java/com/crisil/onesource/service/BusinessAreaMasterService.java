package com.crisil.onesource.service;

import com.crisil.onesource.domain.BusinessAreaMaster;
import com.crisil.onesource.repository.BusinessAreaMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BusinessAreaMaster}.
 */
@Service
@Transactional
public class BusinessAreaMasterService {

    private final Logger log = LoggerFactory.getLogger(BusinessAreaMasterService.class);

    private final BusinessAreaMasterRepository businessAreaMasterRepository;

    public BusinessAreaMasterService(BusinessAreaMasterRepository businessAreaMasterRepository) {
        this.businessAreaMasterRepository = businessAreaMasterRepository;
    }

    /**
     * Save a businessAreaMaster.
     *
     * @param businessAreaMaster the entity to save.
     * @return the persisted entity.
     */
    public BusinessAreaMaster save(BusinessAreaMaster businessAreaMaster) {
        log.debug("Request to save BusinessAreaMaster : {}", businessAreaMaster);
        return businessAreaMasterRepository.save(businessAreaMaster);
    }

    /**
     * Get all the businessAreaMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BusinessAreaMaster> findAll(Pageable pageable) {
        log.debug("Request to get all BusinessAreaMasters");
        return businessAreaMasterRepository.findAll(pageable);
    }


    /**
     * Get one businessAreaMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BusinessAreaMaster> findOne(Long id) {
        log.debug("Request to get BusinessAreaMaster : {}", id);
        return businessAreaMasterRepository.findById(id);
    }

    /**
     * Delete the businessAreaMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BusinessAreaMaster : {}", id);
        businessAreaMasterRepository.deleteById(id);
    }
}
