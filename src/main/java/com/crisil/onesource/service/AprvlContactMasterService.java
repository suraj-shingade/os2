package com.crisil.onesource.service;

import com.crisil.onesource.domain.AprvlContactMaster;
import com.crisil.onesource.repository.AprvlContactMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AprvlContactMaster}.
 */
@Service
@Transactional
public class AprvlContactMasterService {

    private final Logger log = LoggerFactory.getLogger(AprvlContactMasterService.class);

    private final AprvlContactMasterRepository aprvlContactMasterRepository;

    public AprvlContactMasterService(AprvlContactMasterRepository aprvlContactMasterRepository) {
        this.aprvlContactMasterRepository = aprvlContactMasterRepository;
    }

    /**
     * Save a aprvlContactMaster.
     *
     * @param aprvlContactMaster the entity to save.
     * @return the persisted entity.
     */
    public AprvlContactMaster save(AprvlContactMaster aprvlContactMaster) {
        log.debug("Request to save AprvlContactMaster : {}", aprvlContactMaster);
        return aprvlContactMasterRepository.save(aprvlContactMaster);
    }

    /**
     * Get all the aprvlContactMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AprvlContactMaster> findAll(Pageable pageable) {
        log.debug("Request to get all AprvlContactMasters");
        return aprvlContactMasterRepository.findAll(pageable);
    }


    /**
     * Get one aprvlContactMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AprvlContactMaster> findOne(Long id) {
        log.debug("Request to get AprvlContactMaster : {}", id);
        return aprvlContactMasterRepository.findById(id);
    }

    /**
     * Delete the aprvlContactMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AprvlContactMaster : {}", id);
        aprvlContactMasterRepository.deleteById(id);
    }
}
