package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyListingStatus;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyListingStatusRepository;
import com.crisil.onesource.service.dto.CompanyListingStatusCriteria;

/**
 * Service for executing complex queries for {@link CompanyListingStatus} entities in the database.
 * The main input is a {@link CompanyListingStatusCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyListingStatus} or a {@link Page} of {@link CompanyListingStatus} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyListingStatusQueryService extends QueryService<CompanyListingStatus> {

    private final Logger log = LoggerFactory.getLogger(CompanyListingStatusQueryService.class);

    private final CompanyListingStatusRepository companyListingStatusRepository;

    public CompanyListingStatusQueryService(CompanyListingStatusRepository companyListingStatusRepository) {
        this.companyListingStatusRepository = companyListingStatusRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyListingStatus} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyListingStatus> findByCriteria(CompanyListingStatusCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyListingStatus> specification = createSpecification(criteria);
        return companyListingStatusRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyListingStatus} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyListingStatus> findByCriteria(CompanyListingStatusCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyListingStatus> specification = createSpecification(criteria);
        return companyListingStatusRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyListingStatusCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyListingStatus> specification = createSpecification(criteria);
        return companyListingStatusRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyListingStatusCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyListingStatus> createSpecification(CompanyListingStatusCriteria criteria) {
        Specification<CompanyListingStatus> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyListingStatus_.id));
            }
            if (criteria.getListingId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getListingId(), CompanyListingStatus_.listingId));
            }
            if (criteria.getScriptCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScriptCode(), CompanyListingStatus_.scriptCode));
            }
            if (criteria.getExchange() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExchange(), CompanyListingStatus_.exchange));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), CompanyListingStatus_.status));
            }
            if (criteria.getStatusDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatusDate(), CompanyListingStatus_.statusDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyListingStatus_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyListingStatus_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyListingStatus_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyListingStatus_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyListingStatus_.isDeleted));
            }
            if (criteria.getOperation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperation(), CompanyListingStatus_.operation));
            }
            if (criteria.getCtlgSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCtlgSrno(), CompanyListingStatus_.ctlgSrno));
            }
            if (criteria.getJsonStoreId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJsonStoreId(), CompanyListingStatus_.jsonStoreId));
            }
            if (criteria.getIsDataProcessed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDataProcessed(), CompanyListingStatus_.isDataProcessed));
            }
            if (criteria.getCtlgIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtlgIsDeleted(), CompanyListingStatus_.ctlgIsDeleted));
            }
            if (criteria.getCompanyCodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyCodeId(),
                    root -> root.join(CompanyListingStatus_.companyCode, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
