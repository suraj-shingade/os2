package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.AprvlContactMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.AprvlContactMasterRepository;
import com.crisil.onesource.service.dto.AprvlContactMasterCriteria;

/**
 * Service for executing complex queries for {@link AprvlContactMaster} entities in the database.
 * The main input is a {@link AprvlContactMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AprvlContactMaster} or a {@link Page} of {@link AprvlContactMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AprvlContactMasterQueryService extends QueryService<AprvlContactMaster> {

    private final Logger log = LoggerFactory.getLogger(AprvlContactMasterQueryService.class);

    private final AprvlContactMasterRepository aprvlContactMasterRepository;

    public AprvlContactMasterQueryService(AprvlContactMasterRepository aprvlContactMasterRepository) {
        this.aprvlContactMasterRepository = aprvlContactMasterRepository;
    }

    /**
     * Return a {@link List} of {@link AprvlContactMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AprvlContactMaster> findByCriteria(AprvlContactMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AprvlContactMaster> specification = createSpecification(criteria);
        return aprvlContactMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AprvlContactMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AprvlContactMaster> findByCriteria(AprvlContactMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AprvlContactMaster> specification = createSpecification(criteria);
        return aprvlContactMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AprvlContactMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AprvlContactMaster> specification = createSpecification(criteria);
        return aprvlContactMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link AprvlContactMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AprvlContactMaster> createSpecification(AprvlContactMasterCriteria criteria) {
        Specification<AprvlContactMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), AprvlContactMaster_.id));
            }
            if (criteria.getLastname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastname(), AprvlContactMaster_.lastname));
            }
            if (criteria.getJoinednewcompany() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJoinednewcompany(), AprvlContactMaster_.joinednewcompany));
            }
            if (criteria.getCityid() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCityid(), AprvlContactMaster_.cityid));
            }
            if (criteria.getHdeleted() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHdeleted(), AprvlContactMaster_.hdeleted));
            }
            if (criteria.getRecordid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordid(), AprvlContactMaster_.recordid));
            }
            if (criteria.getLevelimportance() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLevelimportance(), AprvlContactMaster_.levelimportance));
            }
            if (criteria.getDepartment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDepartment(), AprvlContactMaster_.department));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), AprvlContactMaster_.fax));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), AprvlContactMaster_.email));
            }
            if (criteria.getKeyperson() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKeyperson(), AprvlContactMaster_.keyperson));
            }
            if (criteria.getCompanycode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanycode(), AprvlContactMaster_.companycode));
            }
            if (criteria.getPincode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPincode(), AprvlContactMaster_.pincode));
            }
            if (criteria.getContactFullName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactFullName(), AprvlContactMaster_.contactFullName));
            }
            if (criteria.getAddress1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress1(), AprvlContactMaster_.address1));
            }
            if (criteria.getCityname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCityname(), AprvlContactMaster_.cityname));
            }
            if (criteria.getCountryname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryname(), AprvlContactMaster_.countryname));
            }
            if (criteria.getMobile() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobile(), AprvlContactMaster_.mobile));
            }
            if (criteria.getStatename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatename(), AprvlContactMaster_.statename));
            }
            if (criteria.getModifyDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifyDate(), AprvlContactMaster_.modifyDate));
            }
            if (criteria.getOfaflag() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOfaflag(), AprvlContactMaster_.ofaflag));
            }
            if (criteria.getFirstname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstname(), AprvlContactMaster_.firstname));
            }
            if (criteria.getMiddlename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMiddlename(), AprvlContactMaster_.middlename));
            }
            if (criteria.getRemovereason() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemovereason(), AprvlContactMaster_.removereason));
            }
            if (criteria.getSalutation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSalutation(), AprvlContactMaster_.salutation));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), AprvlContactMaster_.designation));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), AprvlContactMaster_.status));
            }
            if (criteria.getCtlgSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCtlgSrno(), AprvlContactMaster_.ctlgSrno));
            }
            if (criteria.getCtlgIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtlgIsDeleted(), AprvlContactMaster_.ctlgIsDeleted));
            }
            if (criteria.getJsonStoreId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJsonStoreId(), AprvlContactMaster_.jsonStoreId));
            }
            if (criteria.getIsDataProcessed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDataProcessed(), AprvlContactMaster_.isDataProcessed));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), AprvlContactMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), AprvlContactMaster_.lastModifiedBy));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), AprvlContactMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), AprvlContactMaster_.createdDate));
            }
            if (criteria.getOperation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperation(), AprvlContactMaster_.operation));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactId(), AprvlContactMaster_.contactId));
            }
        }
        return specification;
    }
}
