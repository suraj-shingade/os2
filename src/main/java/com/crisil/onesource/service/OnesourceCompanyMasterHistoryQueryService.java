package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.OnesourceCompanyMasterHistory;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.OnesourceCompanyMasterHistoryRepository;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterHistoryCriteria;

/**
 * Service for executing complex queries for {@link OnesourceCompanyMasterHistory} entities in the database.
 * The main input is a {@link OnesourceCompanyMasterHistoryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OnesourceCompanyMasterHistory} or a {@link Page} of {@link OnesourceCompanyMasterHistory} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OnesourceCompanyMasterHistoryQueryService extends QueryService<OnesourceCompanyMasterHistory> {

    private final Logger log = LoggerFactory.getLogger(OnesourceCompanyMasterHistoryQueryService.class);

    private final OnesourceCompanyMasterHistoryRepository onesourceCompanyMasterHistoryRepository;

    public OnesourceCompanyMasterHistoryQueryService(OnesourceCompanyMasterHistoryRepository onesourceCompanyMasterHistoryRepository) {
        this.onesourceCompanyMasterHistoryRepository = onesourceCompanyMasterHistoryRepository;
    }

    /**
     * Return a {@link List} of {@link OnesourceCompanyMasterHistory} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OnesourceCompanyMasterHistory> findByCriteria(OnesourceCompanyMasterHistoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OnesourceCompanyMasterHistory> specification = createSpecification(criteria);
        return onesourceCompanyMasterHistoryRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OnesourceCompanyMasterHistory} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OnesourceCompanyMasterHistory> findByCriteria(OnesourceCompanyMasterHistoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OnesourceCompanyMasterHistory> specification = createSpecification(criteria);
        return onesourceCompanyMasterHistoryRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OnesourceCompanyMasterHistoryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OnesourceCompanyMasterHistory> specification = createSpecification(criteria);
        return onesourceCompanyMasterHistoryRepository.count(specification);
    }

    /**
     * Function to convert {@link OnesourceCompanyMasterHistoryCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OnesourceCompanyMasterHistory> createSpecification(OnesourceCompanyMasterHistoryCriteria criteria) {
        Specification<OnesourceCompanyMasterHistory> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), OnesourceCompanyMasterHistory_.id));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), OnesourceCompanyMasterHistory_.companyCode));
            }
            if (criteria.getCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyName(), OnesourceCompanyMasterHistory_.companyName));
            }
            if (criteria.getCompanyTypeId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCompanyTypeId(), OnesourceCompanyMasterHistory_.companyTypeId));
            }
            if (criteria.getSectorId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSectorId(), OnesourceCompanyMasterHistory_.sectorId));
            }
            if (criteria.getIndustryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIndustryId(), OnesourceCompanyMasterHistory_.industryId));
            }
            if (criteria.getGroupId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGroupId(), OnesourceCompanyMasterHistory_.groupId));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), OnesourceCompanyMasterHistory_.remarks));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordId(), OnesourceCompanyMasterHistory_.recordId));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), OnesourceCompanyMasterHistory_.status));
            }
            if (criteria.getPin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPin(), OnesourceCompanyMasterHistory_.pin));
            }
            if (criteria.getClientStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientStatus(), OnesourceCompanyMasterHistory_.clientStatus));
            }
            if (criteria.getDisplayCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayCompanyName(), OnesourceCompanyMasterHistory_.displayCompanyName));
            }
            if (criteria.getBillingType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBillingType(), OnesourceCompanyMasterHistory_.billingType));
            }
            if (criteria.getIsBillable() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsBillable(), OnesourceCompanyMasterHistory_.isBillable));
            }
            if (criteria.getListingStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getListingStatus(), OnesourceCompanyMasterHistory_.listingStatus));
            }
            if (criteria.getDateOfIncooperation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateOfIncooperation(), OnesourceCompanyMasterHistory_.dateOfIncooperation));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), OnesourceCompanyMasterHistory_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), OnesourceCompanyMasterHistory_.lastModifiedDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), OnesourceCompanyMasterHistory_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), OnesourceCompanyMasterHistory_.lastModifiedBy));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), OnesourceCompanyMasterHistory_.isDeleted));
            }
            if (criteria.getOriginalFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalFilename(), OnesourceCompanyMasterHistory_.originalFilename));
            }
            if (criteria.getDisplayFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayFilename(), OnesourceCompanyMasterHistory_.displayFilename));
            }
        }
        return specification;
    }
}
