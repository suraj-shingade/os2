package com.crisil.onesource.service;

import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.repository.ContactMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactMaster}.
 */
@Service
@Transactional
public class ContactMasterService {

    private final Logger log = LoggerFactory.getLogger(ContactMasterService.class);

    private final ContactMasterRepository contactMasterRepository;

    public ContactMasterService(ContactMasterRepository contactMasterRepository) {
        this.contactMasterRepository = contactMasterRepository;
    }

    /**
     * Save a contactMaster.
     *
     * @param contactMaster the entity to save.
     * @return the persisted entity.
     */
    public ContactMaster save(ContactMaster contactMaster) {
        log.debug("Request to save ContactMaster : {}", contactMaster);
        return contactMasterRepository.save(contactMaster);
    }

    /**
     * Get all the contactMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactMaster> findAll(Pageable pageable) {
        log.debug("Request to get all ContactMasters");
        return contactMasterRepository.findAll(pageable);
    }


    /**
     * Get one contactMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactMaster> findOne(Long id) {
        log.debug("Request to get ContactMaster : {}", id);
        return contactMasterRepository.findById(id);
    }

    /**
     * Delete the contactMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactMaster : {}", id);
        contactMasterRepository.deleteById(id);
    }
}
