package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocDistrictMasterRepository;
import com.crisil.onesource.service.dto.LocDistrictMasterCriteria;

/**
 * Service for executing complex queries for {@link LocDistrictMaster} entities in the database.
 * The main input is a {@link LocDistrictMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocDistrictMaster} or a {@link Page} of {@link LocDistrictMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocDistrictMasterQueryService extends QueryService<LocDistrictMaster> {

    private final Logger log = LoggerFactory.getLogger(LocDistrictMasterQueryService.class);

    private final LocDistrictMasterRepository locDistrictMasterRepository;

    public LocDistrictMasterQueryService(LocDistrictMasterRepository locDistrictMasterRepository) {
        this.locDistrictMasterRepository = locDistrictMasterRepository;
    }

    /**
     * Return a {@link List} of {@link LocDistrictMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocDistrictMaster> findByCriteria(LocDistrictMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocDistrictMaster> specification = createSpecification(criteria);
        return locDistrictMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocDistrictMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocDistrictMaster> findByCriteria(LocDistrictMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocDistrictMaster> specification = createSpecification(criteria);
        return locDistrictMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocDistrictMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocDistrictMaster> specification = createSpecification(criteria);
        return locDistrictMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link LocDistrictMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocDistrictMaster> createSpecification(LocDistrictMasterCriteria criteria) {
        Specification<LocDistrictMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocDistrictMaster_.id));
            }
            if (criteria.getDistrictName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDistrictName(), LocDistrictMaster_.districtName));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), LocDistrictMaster_.description));
            }
            if (criteria.getDistrictId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDistrictId(), LocDistrictMaster_.districtId));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocDistrictMaster_.isDeleted));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocDistrictMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocDistrictMaster_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocDistrictMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocDistrictMaster_.lastModifiedBy));
            }
            if (criteria.getIsApproved() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsApproved(), LocDistrictMaster_.isApproved));
            }
            if (criteria.getSeqOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeqOrder(), LocDistrictMaster_.seqOrder));
            }
            if (criteria.getLocCityMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocCityMasterId(),
                    root -> root.join(LocDistrictMaster_.locCityMasters, JoinType.LEFT).get(LocCityMaster_.id)));
            }
            if (criteria.getLocPincodeMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocPincodeMasterId(),
                    root -> root.join(LocDistrictMaster_.locPincodeMasters, JoinType.LEFT).get(LocPincodeMaster_.id)));
            }
            if (criteria.getStateIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getStateIdId(),
                    root -> root.join(LocDistrictMaster_.stateId, JoinType.LEFT).get(LocStateMaster_.id)));
            }
        }
        return specification;
    }
}
