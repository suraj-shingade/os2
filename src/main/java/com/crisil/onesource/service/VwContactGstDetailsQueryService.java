package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.VwContactGstDetails;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.VwContactGstDetailsRepository;
import com.crisil.onesource.service.dto.VwContactGstDetailsCriteria;

/**
 * Service for executing complex queries for {@link VwContactGstDetails} entities in the database.
 * The main input is a {@link VwContactGstDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link VwContactGstDetails} or a {@link Page} of {@link VwContactGstDetails} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class VwContactGstDetailsQueryService extends QueryService<VwContactGstDetails> {

    private final Logger log = LoggerFactory.getLogger(VwContactGstDetailsQueryService.class);

    private final VwContactGstDetailsRepository vwContactGstDetailsRepository;

    public VwContactGstDetailsQueryService(VwContactGstDetailsRepository vwContactGstDetailsRepository) {
        this.vwContactGstDetailsRepository = vwContactGstDetailsRepository;
    }

    /**
     * Return a {@link List} of {@link VwContactGstDetails} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VwContactGstDetails> findByCriteria(VwContactGstDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<VwContactGstDetails> specification = createSpecification(criteria);
        return vwContactGstDetailsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link VwContactGstDetails} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VwContactGstDetails> findByCriteria(VwContactGstDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<VwContactGstDetails> specification = createSpecification(criteria);
        return vwContactGstDetailsRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(VwContactGstDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<VwContactGstDetails> specification = createSpecification(criteria);
        return vwContactGstDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link VwContactGstDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<VwContactGstDetails> createSpecification(VwContactGstDetailsCriteria criteria) {
        Specification<VwContactGstDetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), VwContactGstDetails_.id));
            }
            if (criteria.getCompanyRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyRecordId(), VwContactGstDetails_.companyRecordId));
            }
            if (criteria.getCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyName(), VwContactGstDetails_.companyName));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), VwContactGstDetails_.companyCode));
            }
            if (criteria.getGstNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNumber(), VwContactGstDetails_.gstNumber));
            }
            if (criteria.getIsSezContactFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsSezContactFlag(), VwContactGstDetails_.isSezContactFlag));
            }
            if (criteria.getStateName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateName(), VwContactGstDetails_.stateName));
            }
            if (criteria.getGstStateCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstStateCode(), VwContactGstDetails_.gstStateCode));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactId(), VwContactGstDetails_.contactId));
            }
        }
        return specification;
    }
}
