package com.crisil.onesource.service;

import com.crisil.onesource.domain.ContactChangeRequest;
import com.crisil.onesource.repository.ContactChangeRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactChangeRequest}.
 */
@Service
@Transactional
public class ContactChangeRequestService {

    private final Logger log = LoggerFactory.getLogger(ContactChangeRequestService.class);

    private final ContactChangeRequestRepository contactChangeRequestRepository;

    public ContactChangeRequestService(ContactChangeRequestRepository contactChangeRequestRepository) {
        this.contactChangeRequestRepository = contactChangeRequestRepository;
    }

    /**
     * Save a contactChangeRequest.
     *
     * @param contactChangeRequest the entity to save.
     * @return the persisted entity.
     */
    public ContactChangeRequest save(ContactChangeRequest contactChangeRequest) {
        log.debug("Request to save ContactChangeRequest : {}", contactChangeRequest);
        return contactChangeRequestRepository.save(contactChangeRequest);
    }

    /**
     * Get all the contactChangeRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactChangeRequest> findAll(Pageable pageable) {
        log.debug("Request to get all ContactChangeRequests");
        return contactChangeRequestRepository.findAll(pageable);
    }


    /**
     * Get one contactChangeRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactChangeRequest> findOne(Long id) {
        log.debug("Request to get ContactChangeRequest : {}", id);
        return contactChangeRequestRepository.findById(id);
    }

    /**
     * Delete the contactChangeRequest by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactChangeRequest : {}", id);
        contactChangeRequestRepository.deleteById(id);
    }
}
