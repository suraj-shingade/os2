package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocStateMasterRepository;
import com.crisil.onesource.service.dto.LocStateMasterCriteria;

/**
 * Service for executing complex queries for {@link LocStateMaster} entities in the database.
 * The main input is a {@link LocStateMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocStateMaster} or a {@link Page} of {@link LocStateMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocStateMasterQueryService extends QueryService<LocStateMaster> {

    private final Logger log = LoggerFactory.getLogger(LocStateMasterQueryService.class);

    private final LocStateMasterRepository locStateMasterRepository;

    public LocStateMasterQueryService(LocStateMasterRepository locStateMasterRepository) {
        this.locStateMasterRepository = locStateMasterRepository;
    }

    /**
     * Return a {@link List} of {@link LocStateMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocStateMaster> findByCriteria(LocStateMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocStateMaster> specification = createSpecification(criteria);
        return locStateMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocStateMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocStateMaster> findByCriteria(LocStateMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocStateMaster> specification = createSpecification(criteria);
        return locStateMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocStateMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocStateMaster> specification = createSpecification(criteria);
        return locStateMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link LocStateMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocStateMaster> createSpecification(LocStateMasterCriteria criteria) {
        Specification<LocStateMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocStateMaster_.id));
            }
            if (criteria.getStateName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateName(), LocStateMaster_.stateName));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), LocStateMaster_.description));
            }
            if (criteria.getStateId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStateId(), LocStateMaster_.stateId));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocStateMaster_.isDeleted));
            }
            if (criteria.getCountryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryCode(), LocStateMaster_.countryCode));
            }
            if (criteria.getStateCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateCode(), LocStateMaster_.stateCode));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocStateMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocStateMaster_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocStateMaster_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocStateMaster_.lastModifiedDate));
            }
            if (criteria.getIsApproved() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsApproved(), LocStateMaster_.isApproved));
            }
            if (criteria.getSeqOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeqOrder(), LocStateMaster_.seqOrder));
            }
            if (criteria.getLocCityMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocCityMasterId(),
                    root -> root.join(LocStateMaster_.locCityMasters, JoinType.LEFT).get(LocCityMaster_.id)));
            }
            if (criteria.getLocDistrictMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocDistrictMasterId(),
                    root -> root.join(LocStateMaster_.locDistrictMasters, JoinType.LEFT).get(LocDistrictMaster_.id)));
            }
            if (criteria.getLocPincodeMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getLocPincodeMasterId(),
                    root -> root.join(LocStateMaster_.locPincodeMasters, JoinType.LEFT).get(LocPincodeMaster_.id)));
            }
            if (criteria.getRegionIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getRegionIdId(),
                    root -> root.join(LocStateMaster_.regionId, JoinType.LEFT).get(LocRegionMaster_.id)));
            }
            if (criteria.getCountryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCountryId(),
                    root -> root.join(LocStateMaster_.country, JoinType.LEFT).get(LocCountryMaster_.id)));
            }
        }
        return specification;
    }
}
