package com.crisil.onesource.service;

import com.crisil.onesource.domain.AprvlCompanyTdsDetails;
import com.crisil.onesource.repository.AprvlCompanyTdsDetailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link AprvlCompanyTdsDetails}.
 */
@Service
@Transactional
public class AprvlCompanyTdsDetailsService {

    private final Logger log = LoggerFactory.getLogger(AprvlCompanyTdsDetailsService.class);

    private final AprvlCompanyTdsDetailsRepository aprvlCompanyTdsDetailsRepository;

    public AprvlCompanyTdsDetailsService(AprvlCompanyTdsDetailsRepository aprvlCompanyTdsDetailsRepository) {
        this.aprvlCompanyTdsDetailsRepository = aprvlCompanyTdsDetailsRepository;
    }

    /**
     * Save a aprvlCompanyTdsDetails.
     *
     * @param aprvlCompanyTdsDetails the entity to save.
     * @return the persisted entity.
     */
    public AprvlCompanyTdsDetails save(AprvlCompanyTdsDetails aprvlCompanyTdsDetails) {
        log.debug("Request to save AprvlCompanyTdsDetails : {}", aprvlCompanyTdsDetails);
        return aprvlCompanyTdsDetailsRepository.save(aprvlCompanyTdsDetails);
    }

    /**
     * Get all the aprvlCompanyTdsDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<AprvlCompanyTdsDetails> findAll(Pageable pageable) {
        log.debug("Request to get all AprvlCompanyTdsDetails");
        return aprvlCompanyTdsDetailsRepository.findAll(pageable);
    }


    /**
     * Get one aprvlCompanyTdsDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AprvlCompanyTdsDetails> findOne(Long id) {
        log.debug("Request to get AprvlCompanyTdsDetails : {}", id);
        return aprvlCompanyTdsDetailsRepository.findById(id);
    }

    /**
     * Delete the aprvlCompanyTdsDetails by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AprvlCompanyTdsDetails : {}", id);
        aprvlCompanyTdsDetailsRepository.deleteById(id);
    }
}
