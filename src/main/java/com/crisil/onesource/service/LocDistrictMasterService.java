package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocDistrictMaster;
import com.crisil.onesource.repository.LocDistrictMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocDistrictMaster}.
 */
@Service
@Transactional
public class LocDistrictMasterService {

    private final Logger log = LoggerFactory.getLogger(LocDistrictMasterService.class);

    private final LocDistrictMasterRepository locDistrictMasterRepository;

    public LocDistrictMasterService(LocDistrictMasterRepository locDistrictMasterRepository) {
        this.locDistrictMasterRepository = locDistrictMasterRepository;
    }

    /**
     * Save a locDistrictMaster.
     *
     * @param locDistrictMaster the entity to save.
     * @return the persisted entity.
     */
    public LocDistrictMaster save(LocDistrictMaster locDistrictMaster) {
        log.debug("Request to save LocDistrictMaster : {}", locDistrictMaster);
        return locDistrictMasterRepository.save(locDistrictMaster);
    }

    /**
     * Get all the locDistrictMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocDistrictMaster> findAll(Pageable pageable) {
        log.debug("Request to get all LocDistrictMasters");
        return locDistrictMasterRepository.findAll(pageable);
    }


    /**
     * Get one locDistrictMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocDistrictMaster> findOne(Long id) {
        log.debug("Request to get LocDistrictMaster : {}", id);
        return locDistrictMasterRepository.findById(id);
    }

    /**
     * Delete the locDistrictMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocDistrictMaster : {}", id);
        locDistrictMasterRepository.deleteById(id);
    }
}
