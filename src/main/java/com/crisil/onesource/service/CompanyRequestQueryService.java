package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyRequest;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyRequestRepository;
import com.crisil.onesource.service.dto.CompanyRequestCriteria;

/**
 * Service for executing complex queries for {@link CompanyRequest} entities in the database.
 * The main input is a {@link CompanyRequestCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyRequest} or a {@link Page} of {@link CompanyRequest} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyRequestQueryService extends QueryService<CompanyRequest> {

    private final Logger log = LoggerFactory.getLogger(CompanyRequestQueryService.class);

    private final CompanyRequestRepository companyRequestRepository;

    public CompanyRequestQueryService(CompanyRequestRepository companyRequestRepository) {
        this.companyRequestRepository = companyRequestRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyRequest} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyRequest> findByCriteria(CompanyRequestCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyRequest> specification = createSpecification(criteria);
        return companyRequestRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyRequest} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyRequest> findByCriteria(CompanyRequestCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyRequest> specification = createSpecification(criteria);
        return companyRequestRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyRequestCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyRequest> specification = createSpecification(criteria);
        return companyRequestRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyRequestCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyRequest> createSpecification(CompanyRequestCriteria criteria) {
        Specification<CompanyRequest> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyRequest_.id));
            }
            if (criteria.getCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyName(), CompanyRequest_.companyName));
            }
            if (criteria.getCompanyTypeId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCompanyTypeId(), CompanyRequest_.companyTypeId));
            }
            if (criteria.getSectorId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSectorId(), CompanyRequest_.sectorId));
            }
            if (criteria.getIndustryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIndustryId(), CompanyRequest_.industryId));
            }
            if (criteria.getBusinessAreaId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBusinessAreaId(), CompanyRequest_.businessAreaId));
            }
            if (criteria.getClientGroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientGroupName(), CompanyRequest_.clientGroupName));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), CompanyRequest_.remarks));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordId(), CompanyRequest_.recordId));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), CompanyRequest_.companyCode));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), CompanyRequest_.status));
            }
            if (criteria.getReason() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReason(), CompanyRequest_.reason));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyRequest_.isDeleted));
            }
            if (criteria.getBillingType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBillingType(), CompanyRequest_.billingType));
            }
            if (criteria.getDisplayCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayCompanyName(), CompanyRequest_.displayCompanyName));
            }
            if (criteria.getRequestedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestedBy(), CompanyRequest_.requestedBy));
            }
            if (criteria.getApprovedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApprovedBy(), CompanyRequest_.approvedBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyRequest_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyRequest_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyRequest_.lastModifiedDate));
            }
            if (criteria.getRequestedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequestedDate(), CompanyRequest_.requestedDate));
            }
            if (criteria.getApprovedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getApprovedDate(), CompanyRequest_.approvedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyRequest_.lastModifiedBy));
            }
            if (criteria.getOriginalFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalFilename(), CompanyRequest_.originalFilename));
            }
            if (criteria.getDisplayFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayFilename(), CompanyRequest_.displayFilename));
            }
            if (criteria.getCompanyAddressMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyAddressMasterId(),
                    root -> root.join(CompanyRequest_.companyAddressMasters, JoinType.LEFT).get(CompanyAddressMaster_.id)));
            }
            if (criteria.getCompanyTanPanRocDtlsId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyTanPanRocDtlsId(),
                    root -> root.join(CompanyRequest_.companyTanPanRocDtls, JoinType.LEFT).get(CompanyTanPanRocDtls_.id)));
            }
        }
        return specification;
    }
}
