package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.ContactMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.ContactMasterRepository;
import com.crisil.onesource.service.dto.ContactMasterCriteria;

/**
 * Service for executing complex queries for {@link ContactMaster} entities in the database.
 * The main input is a {@link ContactMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactMaster} or a {@link Page} of {@link ContactMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactMasterQueryService extends QueryService<ContactMaster> {

    private final Logger log = LoggerFactory.getLogger(ContactMasterQueryService.class);

    private final ContactMasterRepository contactMasterRepository;

    public ContactMasterQueryService(ContactMasterRepository contactMasterRepository) {
        this.contactMasterRepository = contactMasterRepository;
    }

    /**
     * Return a {@link List} of {@link ContactMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactMaster> findByCriteria(ContactMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactMaster> specification = createSpecification(criteria);
        return contactMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ContactMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactMaster> findByCriteria(ContactMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactMaster> specification = createSpecification(criteria);
        return contactMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactMaster> specification = createSpecification(criteria);
        return contactMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactMaster> createSpecification(ContactMasterCriteria criteria) {
        Specification<ContactMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactMaster_.id));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), ContactMaster_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), ContactMaster_.lastName));
            }
            if (criteria.getMiddleName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMiddleName(), ContactMaster_.middleName));
            }
            if (criteria.getSalutation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSalutation(), ContactMaster_.salutation));
            }
            if (criteria.getAddress1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress1(), ContactMaster_.address1));
            }
            if (criteria.getAddress2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress2(), ContactMaster_.address2));
            }
            if (criteria.getAddress3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress3(), ContactMaster_.address3));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), ContactMaster_.designation));
            }
            if (criteria.getDepartment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDepartment(), ContactMaster_.department));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), ContactMaster_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), ContactMaster_.state));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), ContactMaster_.country));
            }
            if (criteria.getPin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPin(), ContactMaster_.pin));
            }
            if (criteria.getPhoneNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNum(), ContactMaster_.phoneNum));
            }
            if (criteria.getFaxNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFaxNum(), ContactMaster_.faxNum));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), ContactMaster_.email));
            }
            if (criteria.getMobileNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNum(), ContactMaster_.mobileNum));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordId(), ContactMaster_.recordId));
            }
            if (criteria.getComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComments(), ContactMaster_.comments));
            }
            if (criteria.getKeyPerson() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKeyPerson(), ContactMaster_.keyPerson));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactId(), ContactMaster_.contactId));
            }
            if (criteria.getLevelImportance() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLevelImportance(), ContactMaster_.levelImportance));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), ContactMaster_.status));
            }
            if (criteria.getRemoveReason() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemoveReason(), ContactMaster_.removeReason));
            }
            if (criteria.getCityId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCityId(), ContactMaster_.cityId));
            }
            if (criteria.getoFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getoFlag(), ContactMaster_.oFlag));
            }
            if (criteria.getOptOut() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOptOut(), ContactMaster_.optOut));
            }
            if (criteria.getOfaContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOfaContactId(), ContactMaster_.ofaContactId));
            }
            if (criteria.getOptOutDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOptOutDate(), ContactMaster_.optOutDate));
            }
            if (criteria.getFaxOptout() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFaxOptout(), ContactMaster_.faxOptout));
            }
            if (criteria.getDonotcall() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDonotcall(), ContactMaster_.donotcall));
            }
            if (criteria.getDnsendDirectMail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDnsendDirectMail(), ContactMaster_.dnsendDirectMail));
            }
            if (criteria.getDnshareOutsideMhp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDnshareOutsideMhp(), ContactMaster_.dnshareOutsideMhp));
            }
            if (criteria.getOutsideOfContact() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOutsideOfContact(), ContactMaster_.outsideOfContact));
            }
            if (criteria.getDnshareOutsideSnp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDnshareOutsideSnp(), ContactMaster_.dnshareOutsideSnp));
            }
            if (criteria.getEmailOptout() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailOptout(), ContactMaster_.emailOptout));
            }
            if (criteria.getSmsOptout() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSmsOptout(), ContactMaster_.smsOptout));
            }
            if (criteria.getIsSezContactFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsSezContactFlag(), ContactMaster_.isSezContactFlag));
            }
            if (criteria.getIsExemptContactFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsExemptContactFlag(), ContactMaster_.isExemptContactFlag));
            }
            if (criteria.getGstNotApplicable() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNotApplicable(), ContactMaster_.gstNotApplicable));
            }
            if (criteria.getBillingContact() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBillingContact(), ContactMaster_.billingContact));
            }
            if (criteria.getGstPartyType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstPartyType(), ContactMaster_.gstPartyType));
            }
            if (criteria.getTan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTan(), ContactMaster_.tan));
            }
            if (criteria.getGstNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNumber(), ContactMaster_.gstNumber));
            }
            if (criteria.getTdsNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTdsNumber(), ContactMaster_.tdsNumber));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), ContactMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ContactMaster_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), ContactMaster_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), ContactMaster_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), ContactMaster_.isDeleted));
            }
            if (criteria.getCompGstNotApplicableId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompGstNotApplicableId(),
                    root -> root.join(ContactMaster_.compGstNotApplicables, JoinType.LEFT).get(CompGstNotApplicable_.id)));
            }
            if (criteria.getContactChangeRequestId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactChangeRequestId(),
                    root -> root.join(ContactMaster_.contactChangeRequests, JoinType.LEFT).get(ContactChangeRequest_.id)));
            }
            if (criteria.getCompanyCodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyCodeId(),
                    root -> root.join(ContactMaster_.companyCode, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
