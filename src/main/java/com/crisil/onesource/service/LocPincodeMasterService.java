package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.repository.LocPincodeMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocPincodeMaster}.
 */
@Service
@Transactional
public class LocPincodeMasterService {

    private final Logger log = LoggerFactory.getLogger(LocPincodeMasterService.class);

    private final LocPincodeMasterRepository locPincodeMasterRepository;

    public LocPincodeMasterService(LocPincodeMasterRepository locPincodeMasterRepository) {
        this.locPincodeMasterRepository = locPincodeMasterRepository;
    }

    /**
     * Save a locPincodeMaster.
     *
     * @param locPincodeMaster the entity to save.
     * @return the persisted entity.
     */
    public LocPincodeMaster save(LocPincodeMaster locPincodeMaster) {
        log.debug("Request to save LocPincodeMaster : {}", locPincodeMaster);
        return locPincodeMasterRepository.save(locPincodeMaster);
    }

    /**
     * Get all the locPincodeMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocPincodeMaster> findAll(Pageable pageable) {
        log.debug("Request to get all LocPincodeMasters");
        return locPincodeMasterRepository.findAll(pageable);
    }


    /**
     * Get one locPincodeMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocPincodeMaster> findOne(Long id) {
        log.debug("Request to get LocPincodeMaster : {}", id);
        return locPincodeMasterRepository.findById(id);
    }

    /**
     * Delete the locPincodeMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocPincodeMaster : {}", id);
        locPincodeMasterRepository.deleteById(id);
    }
}
