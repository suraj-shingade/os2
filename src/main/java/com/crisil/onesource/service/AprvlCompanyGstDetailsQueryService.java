package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.AprvlCompanyGstDetails;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.AprvlCompanyGstDetailsRepository;
import com.crisil.onesource.service.dto.AprvlCompanyGstDetailsCriteria;

/**
 * Service for executing complex queries for {@link AprvlCompanyGstDetails} entities in the database.
 * The main input is a {@link AprvlCompanyGstDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AprvlCompanyGstDetails} or a {@link Page} of {@link AprvlCompanyGstDetails} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AprvlCompanyGstDetailsQueryService extends QueryService<AprvlCompanyGstDetails> {

    private final Logger log = LoggerFactory.getLogger(AprvlCompanyGstDetailsQueryService.class);

    private final AprvlCompanyGstDetailsRepository aprvlCompanyGstDetailsRepository;

    public AprvlCompanyGstDetailsQueryService(AprvlCompanyGstDetailsRepository aprvlCompanyGstDetailsRepository) {
        this.aprvlCompanyGstDetailsRepository = aprvlCompanyGstDetailsRepository;
    }

    /**
     * Return a {@link List} of {@link AprvlCompanyGstDetails} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AprvlCompanyGstDetails> findByCriteria(AprvlCompanyGstDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AprvlCompanyGstDetails> specification = createSpecification(criteria);
        return aprvlCompanyGstDetailsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AprvlCompanyGstDetails} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AprvlCompanyGstDetails> findByCriteria(AprvlCompanyGstDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AprvlCompanyGstDetails> specification = createSpecification(criteria);
        return aprvlCompanyGstDetailsRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AprvlCompanyGstDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AprvlCompanyGstDetails> specification = createSpecification(criteria);
        return aprvlCompanyGstDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link AprvlCompanyGstDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AprvlCompanyGstDetails> createSpecification(AprvlCompanyGstDetailsCriteria criteria) {
        Specification<AprvlCompanyGstDetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), AprvlCompanyGstDetails_.id));
            }
            if (criteria.getCtlgSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCtlgSrno(), AprvlCompanyGstDetails_.ctlgSrno));
            }
            if (criteria.getRecordid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordid(), AprvlCompanyGstDetails_.recordid));
            }
            if (criteria.getLineofbusiness() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLineofbusiness(), AprvlCompanyGstDetails_.lineofbusiness));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), AprvlCompanyGstDetails_.filename));
            }
            if (criteria.getGstnumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstnumber(), AprvlCompanyGstDetails_.gstnumber));
            }
            if (criteria.getIssezgst() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIssezgst(), AprvlCompanyGstDetails_.issezgst));
            }
            if (criteria.getPreviousgst() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPreviousgst(), AprvlCompanyGstDetails_.previousgst));
            }
            if (criteria.getStatecode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatecode(), AprvlCompanyGstDetails_.statecode));
            }
            if (criteria.getOperation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperation(), AprvlCompanyGstDetails_.operation));
            }
            if (criteria.getHdeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHdeleted(), AprvlCompanyGstDetails_.hdeleted));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), AprvlCompanyGstDetails_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), AprvlCompanyGstDetails_.lastModifiedDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), AprvlCompanyGstDetails_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), AprvlCompanyGstDetails_.lastModifiedBy));
            }
            if (criteria.getCtlgIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtlgIsDeleted(), AprvlCompanyGstDetails_.ctlgIsDeleted));
            }
            if (criteria.getJsonStoreId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJsonStoreId(), AprvlCompanyGstDetails_.jsonStoreId));
            }
            if (criteria.getIsDataProcessed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDataProcessed(), AprvlCompanyGstDetails_.isDataProcessed));
            }
        }
        return specification;
    }
}
