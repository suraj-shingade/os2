package com.crisil.onesource.service;

import com.crisil.onesource.domain.OnesourceCompanyMasterHistory;
import com.crisil.onesource.repository.OnesourceCompanyMasterHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OnesourceCompanyMasterHistory}.
 */
@Service
@Transactional
public class OnesourceCompanyMasterHistoryService {

    private final Logger log = LoggerFactory.getLogger(OnesourceCompanyMasterHistoryService.class);

    private final OnesourceCompanyMasterHistoryRepository onesourceCompanyMasterHistoryRepository;

    public OnesourceCompanyMasterHistoryService(OnesourceCompanyMasterHistoryRepository onesourceCompanyMasterHistoryRepository) {
        this.onesourceCompanyMasterHistoryRepository = onesourceCompanyMasterHistoryRepository;
    }

    /**
     * Save a onesourceCompanyMasterHistory.
     *
     * @param onesourceCompanyMasterHistory the entity to save.
     * @return the persisted entity.
     */
    public OnesourceCompanyMasterHistory save(OnesourceCompanyMasterHistory onesourceCompanyMasterHistory) {
        log.debug("Request to save OnesourceCompanyMasterHistory : {}", onesourceCompanyMasterHistory);
        return onesourceCompanyMasterHistoryRepository.save(onesourceCompanyMasterHistory);
    }

    /**
     * Get all the onesourceCompanyMasterHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OnesourceCompanyMasterHistory> findAll(Pageable pageable) {
        log.debug("Request to get all OnesourceCompanyMasterHistories");
        return onesourceCompanyMasterHistoryRepository.findAll(pageable);
    }


    /**
     * Get one onesourceCompanyMasterHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OnesourceCompanyMasterHistory> findOne(Long id) {
        log.debug("Request to get OnesourceCompanyMasterHistory : {}", id);
        return onesourceCompanyMasterHistoryRepository.findById(id);
    }

    /**
     * Delete the onesourceCompanyMasterHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OnesourceCompanyMasterHistory : {}", id);
        onesourceCompanyMasterHistoryRepository.deleteById(id);
    }
}
