package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.ContactMasterHistory;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.ContactMasterHistoryRepository;
import com.crisil.onesource.service.dto.ContactMasterHistoryCriteria;

/**
 * Service for executing complex queries for {@link ContactMasterHistory} entities in the database.
 * The main input is a {@link ContactMasterHistoryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactMasterHistory} or a {@link Page} of {@link ContactMasterHistory} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactMasterHistoryQueryService extends QueryService<ContactMasterHistory> {

    private final Logger log = LoggerFactory.getLogger(ContactMasterHistoryQueryService.class);

    private final ContactMasterHistoryRepository contactMasterHistoryRepository;

    public ContactMasterHistoryQueryService(ContactMasterHistoryRepository contactMasterHistoryRepository) {
        this.contactMasterHistoryRepository = contactMasterHistoryRepository;
    }

    /**
     * Return a {@link List} of {@link ContactMasterHistory} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactMasterHistory> findByCriteria(ContactMasterHistoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactMasterHistory> specification = createSpecification(criteria);
        return contactMasterHistoryRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ContactMasterHistory} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactMasterHistory> findByCriteria(ContactMasterHistoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactMasterHistory> specification = createSpecification(criteria);
        return contactMasterHistoryRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactMasterHistoryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactMasterHistory> specification = createSpecification(criteria);
        return contactMasterHistoryRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactMasterHistoryCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactMasterHistory> createSpecification(ContactMasterHistoryCriteria criteria) {
        Specification<ContactMasterHistory> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactMasterHistory_.id));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), ContactMasterHistory_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), ContactMasterHistory_.lastName));
            }
            if (criteria.getMiddleName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMiddleName(), ContactMasterHistory_.middleName));
            }
            if (criteria.getSalutation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSalutation(), ContactMasterHistory_.salutation));
            }
            if (criteria.getAddress1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress1(), ContactMasterHistory_.address1));
            }
            if (criteria.getAddress2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress2(), ContactMasterHistory_.address2));
            }
            if (criteria.getAddress3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress3(), ContactMasterHistory_.address3));
            }
            if (criteria.getDesignation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesignation(), ContactMasterHistory_.designation));
            }
            if (criteria.getDepartment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDepartment(), ContactMasterHistory_.department));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), ContactMasterHistory_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), ContactMasterHistory_.state));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), ContactMasterHistory_.country));
            }
            if (criteria.getPin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPin(), ContactMasterHistory_.pin));
            }
            if (criteria.getPhoneNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNum(), ContactMasterHistory_.phoneNum));
            }
            if (criteria.getFaxNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFaxNum(), ContactMasterHistory_.faxNum));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), ContactMasterHistory_.email));
            }
            if (criteria.getMobileNum() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNum(), ContactMasterHistory_.mobileNum));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordId(), ContactMasterHistory_.recordId));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), ContactMasterHistory_.companyCode));
            }
            if (criteria.getComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComments(), ContactMasterHistory_.comments));
            }
            if (criteria.getKeyPerson() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKeyPerson(), ContactMasterHistory_.keyPerson));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactId(), ContactMasterHistory_.contactId));
            }
            if (criteria.getLevelImportance() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLevelImportance(), ContactMasterHistory_.levelImportance));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), ContactMasterHistory_.status));
            }
            if (criteria.getRemoveReason() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemoveReason(), ContactMasterHistory_.removeReason));
            }
            if (criteria.getCityId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCityId(), ContactMasterHistory_.cityId));
            }
            if (criteria.getoFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getoFlag(), ContactMasterHistory_.oFlag));
            }
            if (criteria.getOptOut() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOptOut(), ContactMasterHistory_.optOut));
            }
            if (criteria.getOfaContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOfaContactId(), ContactMasterHistory_.ofaContactId));
            }
            if (criteria.getOptOutDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOptOutDate(), ContactMasterHistory_.optOutDate));
            }
            if (criteria.getFaxOptout() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFaxOptout(), ContactMasterHistory_.faxOptout));
            }
            if (criteria.getDonotcall() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDonotcall(), ContactMasterHistory_.donotcall));
            }
            if (criteria.getDnsendDirectMail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDnsendDirectMail(), ContactMasterHistory_.dnsendDirectMail));
            }
            if (criteria.getDnshareOutsideMhp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDnshareOutsideMhp(), ContactMasterHistory_.dnshareOutsideMhp));
            }
            if (criteria.getOutsideOfContact() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOutsideOfContact(), ContactMasterHistory_.outsideOfContact));
            }
            if (criteria.getDnshareOutsideSnp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDnshareOutsideSnp(), ContactMasterHistory_.dnshareOutsideSnp));
            }
            if (criteria.getEmailOptout() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailOptout(), ContactMasterHistory_.emailOptout));
            }
            if (criteria.getSmsOptout() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSmsOptout(), ContactMasterHistory_.smsOptout));
            }
            if (criteria.getIsSezContactFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsSezContactFlag(), ContactMasterHistory_.isSezContactFlag));
            }
            if (criteria.getIsExemptContactFlag() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsExemptContactFlag(), ContactMasterHistory_.isExemptContactFlag));
            }
            if (criteria.getGstNotApplicable() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNotApplicable(), ContactMasterHistory_.gstNotApplicable));
            }
            if (criteria.getBillingContact() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBillingContact(), ContactMasterHistory_.billingContact));
            }
            if (criteria.getGstPartyType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstPartyType(), ContactMasterHistory_.gstPartyType));
            }
            if (criteria.getTan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTan(), ContactMasterHistory_.tan));
            }
            if (criteria.getGstNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNumber(), ContactMasterHistory_.gstNumber));
            }
            if (criteria.getTdsNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTdsNumber(), ContactMasterHistory_.tdsNumber));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), ContactMasterHistory_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ContactMasterHistory_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), ContactMasterHistory_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), ContactMasterHistory_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), ContactMasterHistory_.isDeleted));
            }
        }
        return specification;
    }
}
