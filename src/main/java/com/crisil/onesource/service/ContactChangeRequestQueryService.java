package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.ContactChangeRequest;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.ContactChangeRequestRepository;
import com.crisil.onesource.service.dto.ContactChangeRequestCriteria;

/**
 * Service for executing complex queries for {@link ContactChangeRequest} entities in the database.
 * The main input is a {@link ContactChangeRequestCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactChangeRequest} or a {@link Page} of {@link ContactChangeRequest} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactChangeRequestQueryService extends QueryService<ContactChangeRequest> {

    private final Logger log = LoggerFactory.getLogger(ContactChangeRequestQueryService.class);

    private final ContactChangeRequestRepository contactChangeRequestRepository;

    public ContactChangeRequestQueryService(ContactChangeRequestRepository contactChangeRequestRepository) {
        this.contactChangeRequestRepository = contactChangeRequestRepository;
    }

    /**
     * Return a {@link List} of {@link ContactChangeRequest} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactChangeRequest> findByCriteria(ContactChangeRequestCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactChangeRequest> specification = createSpecification(criteria);
        return contactChangeRequestRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link ContactChangeRequest} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactChangeRequest> findByCriteria(ContactChangeRequestCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactChangeRequest> specification = createSpecification(criteria);
        return contactChangeRequestRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactChangeRequestCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactChangeRequest> specification = createSpecification(criteria);
        return contactChangeRequestRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactChangeRequestCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactChangeRequest> createSpecification(ContactChangeRequestCriteria criteria) {
        Specification<ContactChangeRequest> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactChangeRequest_.id));
            }
            if (criteria.getRequestStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestStatus(), ContactChangeRequest_.requestStatus));
            }
            if (criteria.getRequesterComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequesterComments(), ContactChangeRequest_.requesterComments));
            }
            if (criteria.getRequestedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestedBy(), ContactChangeRequest_.requestedBy));
            }
            if (criteria.getRequestedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequestedDate(), ContactChangeRequest_.requestedDate));
            }
            if (criteria.getApproverComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApproverComments(), ContactChangeRequest_.approverComments));
            }
            if (criteria.getApprovedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApprovedBy(), ContactChangeRequest_.approvedBy));
            }
            if (criteria.getApprovedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getApprovedDate(), ContactChangeRequest_.approvedDate));
            }
            if (criteria.getFileName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFileName(), ContactChangeRequest_.fileName));
            }
            if (criteria.getIsReverseRequest() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsReverseRequest(), ContactChangeRequest_.isReverseRequest));
            }
            if (criteria.getRequestType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestType(), ContactChangeRequest_.requestType));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ContactChangeRequest_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), ContactChangeRequest_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), ContactChangeRequest_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), ContactChangeRequest_.isDeleted));
            }
            if (criteria.getContactIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactIdId(),
                    root -> root.join(ContactChangeRequest_.contactId, JoinType.LEFT).get(ContactMaster_.id)));
            }
        }
        return specification;
    }
}
