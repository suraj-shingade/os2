package com.crisil.onesource.service;

import com.crisil.onesource.domain.GroupMaster;
import com.crisil.onesource.repository.GroupMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link GroupMaster}.
 */
@Service
@Transactional
public class GroupMasterService {

    private final Logger log = LoggerFactory.getLogger(GroupMasterService.class);

    private final GroupMasterRepository groupMasterRepository;

    public GroupMasterService(GroupMasterRepository groupMasterRepository) {
        this.groupMasterRepository = groupMasterRepository;
    }

    /**
     * Save a groupMaster.
     *
     * @param groupMaster the entity to save.
     * @return the persisted entity.
     */
    public GroupMaster save(GroupMaster groupMaster) {
        log.debug("Request to save GroupMaster : {}", groupMaster);
        return groupMasterRepository.save(groupMaster);
    }

    /**
     * Get all the groupMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<GroupMaster> findAll(Pageable pageable) {
        log.debug("Request to get all GroupMasters");
        return groupMasterRepository.findAll(pageable);
    }


    /**
     * Get one groupMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<GroupMaster> findOne(Long id) {
        log.debug("Request to get GroupMaster : {}", id);
        return groupMasterRepository.findById(id);
    }

    /**
     * Delete the groupMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete GroupMaster : {}", id);
        groupMasterRepository.deleteById(id);
    }
}
