package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.BusinessAreaMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.BusinessAreaMasterRepository;
import com.crisil.onesource.service.dto.BusinessAreaMasterCriteria;

/**
 * Service for executing complex queries for {@link BusinessAreaMaster} entities in the database.
 * The main input is a {@link BusinessAreaMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BusinessAreaMaster} or a {@link Page} of {@link BusinessAreaMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BusinessAreaMasterQueryService extends QueryService<BusinessAreaMaster> {

    private final Logger log = LoggerFactory.getLogger(BusinessAreaMasterQueryService.class);

    private final BusinessAreaMasterRepository businessAreaMasterRepository;

    public BusinessAreaMasterQueryService(BusinessAreaMasterRepository businessAreaMasterRepository) {
        this.businessAreaMasterRepository = businessAreaMasterRepository;
    }

    /**
     * Return a {@link List} of {@link BusinessAreaMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BusinessAreaMaster> findByCriteria(BusinessAreaMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BusinessAreaMaster> specification = createSpecification(criteria);
        return businessAreaMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link BusinessAreaMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BusinessAreaMaster> findByCriteria(BusinessAreaMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BusinessAreaMaster> specification = createSpecification(criteria);
        return businessAreaMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BusinessAreaMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BusinessAreaMaster> specification = createSpecification(criteria);
        return businessAreaMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link BusinessAreaMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BusinessAreaMaster> createSpecification(BusinessAreaMasterCriteria criteria) {
        Specification<BusinessAreaMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BusinessAreaMaster_.id));
            }
            if (criteria.getBusinessAreaId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBusinessAreaId(), BusinessAreaMaster_.businessAreaId));
            }
            if (criteria.getBusinessAreaName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBusinessAreaName(), BusinessAreaMaster_.businessAreaName));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), BusinessAreaMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), BusinessAreaMaster_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), BusinessAreaMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), BusinessAreaMaster_.lastModifiedBy));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), BusinessAreaMaster_.isDeleted));
            }
        }
        return specification;
    }
}
