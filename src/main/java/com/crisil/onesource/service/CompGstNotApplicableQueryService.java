package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompGstNotApplicable;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompGstNotApplicableRepository;
import com.crisil.onesource.service.dto.CompGstNotApplicableCriteria;

/**
 * Service for executing complex queries for {@link CompGstNotApplicable} entities in the database.
 * The main input is a {@link CompGstNotApplicableCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompGstNotApplicable} or a {@link Page} of {@link CompGstNotApplicable} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompGstNotApplicableQueryService extends QueryService<CompGstNotApplicable> {

    private final Logger log = LoggerFactory.getLogger(CompGstNotApplicableQueryService.class);

    private final CompGstNotApplicableRepository compGstNotApplicableRepository;

    public CompGstNotApplicableQueryService(CompGstNotApplicableRepository compGstNotApplicableRepository) {
        this.compGstNotApplicableRepository = compGstNotApplicableRepository;
    }

    /**
     * Return a {@link List} of {@link CompGstNotApplicable} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompGstNotApplicable> findByCriteria(CompGstNotApplicableCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompGstNotApplicable> specification = createSpecification(criteria);
        return compGstNotApplicableRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompGstNotApplicable} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompGstNotApplicable> findByCriteria(CompGstNotApplicableCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompGstNotApplicable> specification = createSpecification(criteria);
        return compGstNotApplicableRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompGstNotApplicableCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompGstNotApplicable> specification = createSpecification(criteria);
        return compGstNotApplicableRepository.count(specification);
    }

    /**
     * Function to convert {@link CompGstNotApplicableCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompGstNotApplicable> createSpecification(CompGstNotApplicableCriteria criteria) {
        Specification<CompGstNotApplicable> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompGstNotApplicable_.id));
            }
            if (criteria.getRequestStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestStatus(), CompGstNotApplicable_.requestStatus));
            }
            if (criteria.getRequesterComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequesterComments(), CompGstNotApplicable_.requesterComments));
            }
            if (criteria.getRequestedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestedBy(), CompGstNotApplicable_.requestedBy));
            }
            if (criteria.getRequestedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequestedDate(), CompGstNotApplicable_.requestedDate));
            }
            if (criteria.getApproverComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApproverComments(), CompGstNotApplicable_.approverComments));
            }
            if (criteria.getApprovedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getApprovedBy(), CompGstNotApplicable_.approvedBy));
            }
            if (criteria.getApprovedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getApprovedDate(), CompGstNotApplicable_.approvedDate));
            }
            if (criteria.getGstNaFile() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstNaFile(), CompGstNotApplicable_.gstNaFile));
            }
            if (criteria.getIsReverseRequest() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsReverseRequest(), CompGstNotApplicable_.isReverseRequest));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompGstNotApplicable_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompGstNotApplicable_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompGstNotApplicable_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompGstNotApplicable_.isDeleted));
            }
            if (criteria.getContactIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactIdId(),
                    root -> root.join(CompGstNotApplicable_.contactId, JoinType.LEFT).get(ContactMaster_.id)));
            }
        }
        return specification;
    }
}
