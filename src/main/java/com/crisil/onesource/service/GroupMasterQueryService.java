package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.GroupMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.GroupMasterRepository;
import com.crisil.onesource.service.dto.GroupMasterCriteria;

/**
 * Service for executing complex queries for {@link GroupMaster} entities in the database.
 * The main input is a {@link GroupMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link GroupMaster} or a {@link Page} of {@link GroupMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class GroupMasterQueryService extends QueryService<GroupMaster> {

    private final Logger log = LoggerFactory.getLogger(GroupMasterQueryService.class);

    private final GroupMasterRepository groupMasterRepository;

    public GroupMasterQueryService(GroupMasterRepository groupMasterRepository) {
        this.groupMasterRepository = groupMasterRepository;
    }

    /**
     * Return a {@link List} of {@link GroupMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<GroupMaster> findByCriteria(GroupMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<GroupMaster> specification = createSpecification(criteria);
        return groupMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link GroupMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<GroupMaster> findByCriteria(GroupMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<GroupMaster> specification = createSpecification(criteria);
        return groupMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(GroupMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<GroupMaster> specification = createSpecification(criteria);
        return groupMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link GroupMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<GroupMaster> createSpecification(GroupMasterCriteria criteria) {
        Specification<GroupMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), GroupMaster_.id));
            }
            if (criteria.getGroupId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGroupId(), GroupMaster_.groupId));
            }
            if (criteria.getGroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGroupName(), GroupMaster_.groupName));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), GroupMaster_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), GroupMaster_.lastModifiedDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), GroupMaster_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), GroupMaster_.lastModifiedBy));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), GroupMaster_.isDeleted));
            }
            if (criteria.getOnesourceCompanyMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getOnesourceCompanyMasterId(),
                    root -> root.join(GroupMaster_.onesourceCompanyMasters, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
