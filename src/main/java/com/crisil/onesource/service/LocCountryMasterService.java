package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocCountryMaster;
import com.crisil.onesource.repository.LocCountryMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocCountryMaster}.
 */
@Service
@Transactional
public class LocCountryMasterService {

    private final Logger log = LoggerFactory.getLogger(LocCountryMasterService.class);

    private final LocCountryMasterRepository locCountryMasterRepository;

    public LocCountryMasterService(LocCountryMasterRepository locCountryMasterRepository) {
        this.locCountryMasterRepository = locCountryMasterRepository;
    }

    /**
     * Save a locCountryMaster.
     *
     * @param locCountryMaster the entity to save.
     * @return the persisted entity.
     */
    public LocCountryMaster save(LocCountryMaster locCountryMaster) {
        log.debug("Request to save LocCountryMaster : {}", locCountryMaster);
        return locCountryMasterRepository.save(locCountryMaster);
    }

    /**
     * Get all the locCountryMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocCountryMaster> findAll(Pageable pageable) {
        log.debug("Request to get all LocCountryMasters");
        return locCountryMasterRepository.findAll(pageable);
    }


    /**
     * Get one locCountryMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocCountryMaster> findOne(Long id) {
        log.debug("Request to get LocCountryMaster : {}", id);
        return locCountryMasterRepository.findById(id);
    }

    /**
     * Delete the locCountryMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocCountryMaster : {}", id);
        locCountryMasterRepository.deleteById(id);
    }
}
