package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyChangeRequest;
import com.crisil.onesource.repository.CompanyChangeRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyChangeRequest}.
 */
@Service
@Transactional
public class CompanyChangeRequestService {

    private final Logger log = LoggerFactory.getLogger(CompanyChangeRequestService.class);

    private final CompanyChangeRequestRepository companyChangeRequestRepository;

    public CompanyChangeRequestService(CompanyChangeRequestRepository companyChangeRequestRepository) {
        this.companyChangeRequestRepository = companyChangeRequestRepository;
    }

    /**
     * Save a companyChangeRequest.
     *
     * @param companyChangeRequest the entity to save.
     * @return the persisted entity.
     */
    public CompanyChangeRequest save(CompanyChangeRequest companyChangeRequest) {
        log.debug("Request to save CompanyChangeRequest : {}", companyChangeRequest);
        return companyChangeRequestRepository.save(companyChangeRequest);
    }

    /**
     * Get all the companyChangeRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyChangeRequest> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyChangeRequests");
        return companyChangeRequestRepository.findAll(pageable);
    }


    /**
     * Get one companyChangeRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyChangeRequest> findOne(Long id) {
        log.debug("Request to get CompanyChangeRequest : {}", id);
        return companyChangeRequestRepository.findById(id);
    }

    /**
     * Delete the companyChangeRequest by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyChangeRequest : {}", id);
        companyChangeRequestRepository.deleteById(id);
    }
}
