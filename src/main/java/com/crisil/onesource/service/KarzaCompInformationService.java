package com.crisil.onesource.service;

import com.crisil.onesource.domain.KarzaCompInformation;
import com.crisil.onesource.repository.KarzaCompInformationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link KarzaCompInformation}.
 */
@Service
@Transactional
public class KarzaCompInformationService {

    private final Logger log = LoggerFactory.getLogger(KarzaCompInformationService.class);

    private final KarzaCompInformationRepository karzaCompInformationRepository;

    public KarzaCompInformationService(KarzaCompInformationRepository karzaCompInformationRepository) {
        this.karzaCompInformationRepository = karzaCompInformationRepository;
    }

    /**
     * Save a karzaCompInformation.
     *
     * @param karzaCompInformation the entity to save.
     * @return the persisted entity.
     */
    public KarzaCompInformation save(KarzaCompInformation karzaCompInformation) {
        log.debug("Request to save KarzaCompInformation : {}", karzaCompInformation);
        return karzaCompInformationRepository.save(karzaCompInformation);
    }

    /**
     * Get all the karzaCompInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<KarzaCompInformation> findAll(Pageable pageable) {
        log.debug("Request to get all KarzaCompInformations");
        return karzaCompInformationRepository.findAll(pageable);
    }


    /**
     * Get one karzaCompInformation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<KarzaCompInformation> findOne(Long id) {
        log.debug("Request to get KarzaCompInformation : {}", id);
        return karzaCompInformationRepository.findById(id);
    }

    /**
     * Delete the karzaCompInformation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete KarzaCompInformation : {}", id);
        karzaCompInformationRepository.deleteById(id);
    }
}
