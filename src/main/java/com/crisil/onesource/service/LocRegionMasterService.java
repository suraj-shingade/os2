package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocRegionMaster;
import com.crisil.onesource.repository.LocRegionMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocRegionMaster}.
 */
@Service
@Transactional
public class LocRegionMasterService {

    private final Logger log = LoggerFactory.getLogger(LocRegionMasterService.class);

    private final LocRegionMasterRepository locRegionMasterRepository;

    public LocRegionMasterService(LocRegionMasterRepository locRegionMasterRepository) {
        this.locRegionMasterRepository = locRegionMasterRepository;
    }

    /**
     * Save a locRegionMaster.
     *
     * @param locRegionMaster the entity to save.
     * @return the persisted entity.
     */
    public LocRegionMaster save(LocRegionMaster locRegionMaster) {
        log.debug("Request to save LocRegionMaster : {}", locRegionMaster);
        return locRegionMasterRepository.save(locRegionMaster);
    }

    /**
     * Get all the locRegionMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocRegionMaster> findAll(Pageable pageable) {
        log.debug("Request to get all LocRegionMasters");
        return locRegionMasterRepository.findAll(pageable);
    }


    /**
     * Get one locRegionMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocRegionMaster> findOne(Long id) {
        log.debug("Request to get LocRegionMaster : {}", id);
        return locRegionMasterRepository.findById(id);
    }

    /**
     * Delete the locRegionMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocRegionMaster : {}", id);
        locRegionMasterRepository.deleteById(id);
    }
}
