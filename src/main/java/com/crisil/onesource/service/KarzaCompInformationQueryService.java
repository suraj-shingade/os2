package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.KarzaCompInformation;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.KarzaCompInformationRepository;
import com.crisil.onesource.service.dto.KarzaCompInformationCriteria;

/**
 * Service for executing complex queries for {@link KarzaCompInformation} entities in the database.
 * The main input is a {@link KarzaCompInformationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link KarzaCompInformation} or a {@link Page} of {@link KarzaCompInformation} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class KarzaCompInformationQueryService extends QueryService<KarzaCompInformation> {

    private final Logger log = LoggerFactory.getLogger(KarzaCompInformationQueryService.class);

    private final KarzaCompInformationRepository karzaCompInformationRepository;

    public KarzaCompInformationQueryService(KarzaCompInformationRepository karzaCompInformationRepository) {
        this.karzaCompInformationRepository = karzaCompInformationRepository;
    }

    /**
     * Return a {@link List} of {@link KarzaCompInformation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<KarzaCompInformation> findByCriteria(KarzaCompInformationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<KarzaCompInformation> specification = createSpecification(criteria);
        return karzaCompInformationRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link KarzaCompInformation} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<KarzaCompInformation> findByCriteria(KarzaCompInformationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<KarzaCompInformation> specification = createSpecification(criteria);
        return karzaCompInformationRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(KarzaCompInformationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<KarzaCompInformation> specification = createSpecification(criteria);
        return karzaCompInformationRepository.count(specification);
    }

    /**
     * Function to convert {@link KarzaCompInformationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<KarzaCompInformation> createSpecification(KarzaCompInformationCriteria criteria) {
        Specification<KarzaCompInformation> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), KarzaCompInformation_.id));
            }
            if (criteria.getPan() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPan(), KarzaCompInformation_.pan));
            }
            if (criteria.getCompanyNameKarza() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyNameKarza(), KarzaCompInformation_.companyNameKarza));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), KarzaCompInformation_.address));
            }
            if (criteria.getTypeofapproval() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTypeofapproval(), KarzaCompInformation_.typeofapproval));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), KarzaCompInformation_.status));
            }
            if (criteria.getGstin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstin(), KarzaCompInformation_.gstin));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), KarzaCompInformation_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), KarzaCompInformation_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), KarzaCompInformation_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), KarzaCompInformation_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), KarzaCompInformation_.isDeleted));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), KarzaCompInformation_.state));
            }
            if (criteria.getIsValidGst() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsValidGst(), KarzaCompInformation_.isValidGst));
            }
            if (criteria.getSource() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSource(), KarzaCompInformation_.source));
            }
            if (criteria.getComments() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComments(), KarzaCompInformation_.comments));
            }
            if (criteria.getCheckdigit() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCheckdigit(), KarzaCompInformation_.checkdigit));
            }
            if (criteria.getTaxpayerName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTaxpayerName(), KarzaCompInformation_.taxpayerName));
            }
            if (criteria.getNatuteOfBuss() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNatuteOfBuss(), KarzaCompInformation_.natuteOfBuss));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), KarzaCompInformation_.mobileNo));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), KarzaCompInformation_.email));
            }
            if (criteria.getTradeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTradeName(), KarzaCompInformation_.tradeName));
            }
            if (criteria.getConstitBusiness() != null) {
                specification = specification.and(buildStringSpecification(criteria.getConstitBusiness(), KarzaCompInformation_.constitBusiness));
            }
            if (criteria.getCompanyCodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyCodeId(),
                    root -> root.join(KarzaCompInformation_.companyCode, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
