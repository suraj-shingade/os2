package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompGstNotApplicable;
import com.crisil.onesource.repository.CompGstNotApplicableRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompGstNotApplicable}.
 */
@Service
@Transactional
public class CompGstNotApplicableService {

    private final Logger log = LoggerFactory.getLogger(CompGstNotApplicableService.class);

    private final CompGstNotApplicableRepository compGstNotApplicableRepository;

    public CompGstNotApplicableService(CompGstNotApplicableRepository compGstNotApplicableRepository) {
        this.compGstNotApplicableRepository = compGstNotApplicableRepository;
    }

    /**
     * Save a compGstNotApplicable.
     *
     * @param compGstNotApplicable the entity to save.
     * @return the persisted entity.
     */
    public CompGstNotApplicable save(CompGstNotApplicable compGstNotApplicable) {
        log.debug("Request to save CompGstNotApplicable : {}", compGstNotApplicable);
        return compGstNotApplicableRepository.save(compGstNotApplicable);
    }

    /**
     * Get all the compGstNotApplicables.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompGstNotApplicable> findAll(Pageable pageable) {
        log.debug("Request to get all CompGstNotApplicables");
        return compGstNotApplicableRepository.findAll(pageable);
    }


    /**
     * Get one compGstNotApplicable by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompGstNotApplicable> findOne(Long id) {
        log.debug("Request to get CompGstNotApplicable : {}", id);
        return compGstNotApplicableRepository.findById(id);
    }

    /**
     * Delete the compGstNotApplicable by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompGstNotApplicable : {}", id);
        compGstNotApplicableRepository.deleteById(id);
    }
}
