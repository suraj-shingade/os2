package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.IndustryMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.IndustryMasterRepository;
import com.crisil.onesource.service.dto.IndustryMasterCriteria;

/**
 * Service for executing complex queries for {@link IndustryMaster} entities in the database.
 * The main input is a {@link IndustryMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link IndustryMaster} or a {@link Page} of {@link IndustryMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class IndustryMasterQueryService extends QueryService<IndustryMaster> {

    private final Logger log = LoggerFactory.getLogger(IndustryMasterQueryService.class);

    private final IndustryMasterRepository industryMasterRepository;

    public IndustryMasterQueryService(IndustryMasterRepository industryMasterRepository) {
        this.industryMasterRepository = industryMasterRepository;
    }

    /**
     * Return a {@link List} of {@link IndustryMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<IndustryMaster> findByCriteria(IndustryMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<IndustryMaster> specification = createSpecification(criteria);
        return industryMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link IndustryMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<IndustryMaster> findByCriteria(IndustryMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<IndustryMaster> specification = createSpecification(criteria);
        return industryMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(IndustryMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<IndustryMaster> specification = createSpecification(criteria);
        return industryMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link IndustryMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<IndustryMaster> createSpecification(IndustryMasterCriteria criteria) {
        Specification<IndustryMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), IndustryMaster_.id));
            }
            if (criteria.getIndustryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIndustryId(), IndustryMaster_.industryId));
            }
            if (criteria.getIndustryName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIndustryName(), IndustryMaster_.industryName));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), IndustryMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), IndustryMaster_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), IndustryMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), IndustryMaster_.lastModifiedBy));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), IndustryMaster_.isDeleted));
            }
            if (criteria.getOnesourceCompanyMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getOnesourceCompanyMasterId(),
                    root -> root.join(IndustryMaster_.onesourceCompanyMasters, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
