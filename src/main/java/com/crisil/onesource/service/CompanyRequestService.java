package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyRequest;
import com.crisil.onesource.repository.CompanyRequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyRequest}.
 */
@Service
@Transactional
public class CompanyRequestService {

    private final Logger log = LoggerFactory.getLogger(CompanyRequestService.class);

    private final CompanyRequestRepository companyRequestRepository;

    public CompanyRequestService(CompanyRequestRepository companyRequestRepository) {
        this.companyRequestRepository = companyRequestRepository;
    }

    /**
     * Save a companyRequest.
     *
     * @param companyRequest the entity to save.
     * @return the persisted entity.
     */
    public CompanyRequest save(CompanyRequest companyRequest) {
        log.debug("Request to save CompanyRequest : {}", companyRequest);
        return companyRequestRepository.save(companyRequest);
    }

    /**
     * Get all the companyRequests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyRequest> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyRequests");
        return companyRequestRepository.findAll(pageable);
    }


    /**
     * Get one companyRequest by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyRequest> findOne(Long id) {
        log.debug("Request to get CompanyRequest : {}", id);
        return companyRequestRepository.findById(id);
    }

    /**
     * Delete the companyRequest by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyRequest : {}", id);
        companyRequestRepository.deleteById(id);
    }
}
