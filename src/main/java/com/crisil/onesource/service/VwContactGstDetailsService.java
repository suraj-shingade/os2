package com.crisil.onesource.service;

import com.crisil.onesource.domain.VwContactGstDetails;
import com.crisil.onesource.repository.VwContactGstDetailsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link VwContactGstDetails}.
 */
@Service
@Transactional
public class VwContactGstDetailsService {

    private final Logger log = LoggerFactory.getLogger(VwContactGstDetailsService.class);

    private final VwContactGstDetailsRepository vwContactGstDetailsRepository;

    public VwContactGstDetailsService(VwContactGstDetailsRepository vwContactGstDetailsRepository) {
        this.vwContactGstDetailsRepository = vwContactGstDetailsRepository;
    }

    /**
     * Save a vwContactGstDetails.
     *
     * @param vwContactGstDetails the entity to save.
     * @return the persisted entity.
     */
    public VwContactGstDetails save(VwContactGstDetails vwContactGstDetails) {
        log.debug("Request to save VwContactGstDetails : {}", vwContactGstDetails);
        return vwContactGstDetailsRepository.save(vwContactGstDetails);
    }

    /**
     * Get all the vwContactGstDetails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<VwContactGstDetails> findAll(Pageable pageable) {
        log.debug("Request to get all VwContactGstDetails");
        return vwContactGstDetailsRepository.findAll(pageable);
    }


    /**
     * Get one vwContactGstDetails by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<VwContactGstDetails> findOne(Long id) {
        log.debug("Request to get VwContactGstDetails : {}", id);
        return vwContactGstDetailsRepository.findById(id);
    }

    /**
     * Delete the vwContactGstDetails by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete VwContactGstDetails : {}", id);
        vwContactGstDetailsRepository.deleteById(id);
    }
}
