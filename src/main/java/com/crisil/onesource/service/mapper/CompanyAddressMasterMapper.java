package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyAddressMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyAddressMaster} and its DTO {@link CompanyAddressMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {LocPincodeMasterMapper.class, CompanyRequestMapper.class})
public interface CompanyAddressMasterMapper extends EntityMapper<CompanyAddressMasterDTO, CompanyAddressMaster> {

    @Mapping(source = "pincode.id", target = "pincodeId")
    @Mapping(source = "record.id", target = "recordId")
    CompanyAddressMasterDTO toDto(CompanyAddressMaster companyAddressMaster);

    @Mapping(source = "pincodeId", target = "pincode")
    @Mapping(source = "recordId", target = "record")
    CompanyAddressMaster toEntity(CompanyAddressMasterDTO companyAddressMasterDTO);

    default CompanyAddressMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyAddressMaster companyAddressMaster = new CompanyAddressMaster();
        companyAddressMaster.setId(id);
        return companyAddressMaster;
    }
}
