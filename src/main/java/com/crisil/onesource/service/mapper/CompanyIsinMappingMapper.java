package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyIsinMappingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyIsinMapping} and its DTO {@link CompanyIsinMappingDTO}.
 */
@Mapper(componentModel = "spring", uses = {OnesourceCompanyMasterMapper.class})
public interface CompanyIsinMappingMapper extends EntityMapper<CompanyIsinMappingDTO, CompanyIsinMapping> {

    @Mapping(source = "companyCode.id", target = "companyCodeId")
    CompanyIsinMappingDTO toDto(CompanyIsinMapping companyIsinMapping);

    @Mapping(source = "companyCodeId", target = "companyCode")
    CompanyIsinMapping toEntity(CompanyIsinMappingDTO companyIsinMappingDTO);

    default CompanyIsinMapping fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyIsinMapping companyIsinMapping = new CompanyIsinMapping();
        companyIsinMapping.setId(id);
        return companyIsinMapping;
    }
}
