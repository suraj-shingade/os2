package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.GroupMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link GroupMaster} and its DTO {@link GroupMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GroupMasterMapper extends EntityMapper<GroupMasterDTO, GroupMaster> {


    @Mapping(target = "onesourceCompanyMasters", ignore = true)
    @Mapping(target = "removeOnesourceCompanyMaster", ignore = true)
    GroupMaster toEntity(GroupMasterDTO groupMasterDTO);

    default GroupMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        GroupMaster groupMaster = new GroupMaster();
        groupMaster.setId(id);
        return groupMaster;
    }
}
