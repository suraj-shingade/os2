package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.KarzaCompInformationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link KarzaCompInformation} and its DTO {@link KarzaCompInformationDTO}.
 */
@Mapper(componentModel = "spring", uses = {OnesourceCompanyMasterMapper.class})
public interface KarzaCompInformationMapper extends EntityMapper<KarzaCompInformationDTO, KarzaCompInformation> {

    @Mapping(source = "companyCode.id", target = "companyCodeId")
    KarzaCompInformationDTO toDto(KarzaCompInformation karzaCompInformation);

    @Mapping(source = "companyCodeId", target = "companyCode")
    KarzaCompInformation toEntity(KarzaCompInformationDTO karzaCompInformationDTO);

    default KarzaCompInformation fromId(Long id) {
        if (id == null) {
            return null;
        }
        KarzaCompInformation karzaCompInformation = new KarzaCompInformation();
        karzaCompInformation.setId(id);
        return karzaCompInformation;
    }
}
