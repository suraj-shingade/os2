package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.ContactMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactMaster} and its DTO {@link ContactMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {OnesourceCompanyMasterMapper.class})
public interface ContactMasterMapper extends EntityMapper<ContactMasterDTO, ContactMaster> {

    @Mapping(source = "companyCode.id", target = "companyCodeId")
    ContactMasterDTO toDto(ContactMaster contactMaster);

    @Mapping(target = "compGstNotApplicables", ignore = true)
    @Mapping(target = "removeCompGstNotApplicable", ignore = true)
    @Mapping(target = "contactChangeRequests", ignore = true)
    @Mapping(target = "removeContactChangeRequest", ignore = true)
    @Mapping(source = "companyCodeId", target = "companyCode")
    ContactMaster toEntity(ContactMasterDTO contactMasterDTO);

    default ContactMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactMaster contactMaster = new ContactMaster();
        contactMaster.setId(id);
        return contactMaster;
    }
}
