package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocRegionMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocRegionMaster} and its DTO {@link LocRegionMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LocRegionMasterMapper extends EntityMapper<LocRegionMasterDTO, LocRegionMaster> {


    @Mapping(target = "locStateMasters", ignore = true)
    @Mapping(target = "removeLocStateMaster", ignore = true)
    LocRegionMaster toEntity(LocRegionMasterDTO locRegionMasterDTO);

    default LocRegionMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocRegionMaster locRegionMaster = new LocRegionMaster();
        locRegionMaster.setId(id);
        return locRegionMaster;
    }
}
