package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyListingStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyListingStatus} and its DTO {@link CompanyListingStatusDTO}.
 */
@Mapper(componentModel = "spring", uses = {OnesourceCompanyMasterMapper.class})
public interface CompanyListingStatusMapper extends EntityMapper<CompanyListingStatusDTO, CompanyListingStatus> {

    @Mapping(source = "companyCode.id", target = "companyCodeId")
    CompanyListingStatusDTO toDto(CompanyListingStatus companyListingStatus);

    @Mapping(source = "companyCodeId", target = "companyCode")
    CompanyListingStatus toEntity(CompanyListingStatusDTO companyListingStatusDTO);

    default CompanyListingStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyListingStatus companyListingStatus = new CompanyListingStatus();
        companyListingStatus.setId(id);
        return companyListingStatus;
    }
}
