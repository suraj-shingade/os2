package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.ContactChangeRequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactChangeRequest} and its DTO {@link ContactChangeRequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMasterMapper.class})
public interface ContactChangeRequestMapper extends EntityMapper<ContactChangeRequestDTO, ContactChangeRequest> {

    @Mapping(source = "contactId.id", target = "contactIdId")
    ContactChangeRequestDTO toDto(ContactChangeRequest contactChangeRequest);

    @Mapping(source = "contactIdId", target = "contactId")
    ContactChangeRequest toEntity(ContactChangeRequestDTO contactChangeRequestDTO);

    default ContactChangeRequest fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactChangeRequest contactChangeRequest = new ContactChangeRequest();
        contactChangeRequest.setId(id);
        return contactChangeRequest;
    }
}
