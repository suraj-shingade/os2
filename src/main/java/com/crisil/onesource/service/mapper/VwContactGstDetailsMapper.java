package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.VwContactGstDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link VwContactGstDetails} and its DTO {@link VwContactGstDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VwContactGstDetailsMapper extends EntityMapper<VwContactGstDetailsDTO, VwContactGstDetails> {



    default VwContactGstDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        VwContactGstDetails vwContactGstDetails = new VwContactGstDetails();
        vwContactGstDetails.setId(id);
        return vwContactGstDetails;
    }
}
