package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocCountryMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocCountryMaster} and its DTO {@link LocCountryMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface LocCountryMasterMapper extends EntityMapper<LocCountryMasterDTO, LocCountryMaster> {


    @Mapping(target = "locStateMasters", ignore = true)
    @Mapping(target = "removeLocStateMaster", ignore = true)
    LocCountryMaster toEntity(LocCountryMasterDTO locCountryMasterDTO);

    default LocCountryMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocCountryMaster locCountryMaster = new LocCountryMaster();
        locCountryMaster.setId(id);
        return locCountryMaster;
    }
}
