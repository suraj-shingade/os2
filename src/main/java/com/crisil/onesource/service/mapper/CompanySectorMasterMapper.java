package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanySectorMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanySectorMaster} and its DTO {@link CompanySectorMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CompanySectorMasterMapper extends EntityMapper<CompanySectorMasterDTO, CompanySectorMaster> {


    @Mapping(target = "onesourceCompanyMasters", ignore = true)
    @Mapping(target = "removeOnesourceCompanyMaster", ignore = true)
    CompanySectorMaster toEntity(CompanySectorMasterDTO companySectorMasterDTO);

    default CompanySectorMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanySectorMaster companySectorMaster = new CompanySectorMaster();
        companySectorMaster.setId(id);
        return companySectorMaster;
    }
}
