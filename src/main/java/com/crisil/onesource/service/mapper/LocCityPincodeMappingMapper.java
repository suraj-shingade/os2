package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocCityPincodeMappingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocCityPincodeMapping} and its DTO {@link LocCityPincodeMappingDTO}.
 */
@Mapper(componentModel = "spring", uses = {LocCityMasterMapper.class})
public interface LocCityPincodeMappingMapper extends EntityMapper<LocCityPincodeMappingDTO, LocCityPincodeMapping> {

    @Mapping(source = "cityId.id", target = "cityIdId")
    LocCityPincodeMappingDTO toDto(LocCityPincodeMapping locCityPincodeMapping);

    @Mapping(source = "cityIdId", target = "cityId")
    LocCityPincodeMapping toEntity(LocCityPincodeMappingDTO locCityPincodeMappingDTO);

    default LocCityPincodeMapping fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocCityPincodeMapping locCityPincodeMapping = new LocCityPincodeMapping();
        locCityPincodeMapping.setId(id);
        return locCityPincodeMapping;
    }
}
