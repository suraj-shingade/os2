package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.AprvlContactMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AprvlContactMaster} and its DTO {@link AprvlContactMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AprvlContactMasterMapper extends EntityMapper<AprvlContactMasterDTO, AprvlContactMaster> {



    default AprvlContactMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        AprvlContactMaster aprvlContactMaster = new AprvlContactMaster();
        aprvlContactMaster.setId(id);
        return aprvlContactMaster;
    }
}
