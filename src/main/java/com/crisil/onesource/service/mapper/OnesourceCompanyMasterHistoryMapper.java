package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterHistoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OnesourceCompanyMasterHistory} and its DTO {@link OnesourceCompanyMasterHistoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OnesourceCompanyMasterHistoryMapper extends EntityMapper<OnesourceCompanyMasterHistoryDTO, OnesourceCompanyMasterHistory> {



    default OnesourceCompanyMasterHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        OnesourceCompanyMasterHistory onesourceCompanyMasterHistory = new OnesourceCompanyMasterHistory();
        onesourceCompanyMasterHistory.setId(id);
        return onesourceCompanyMasterHistory;
    }
}
