package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyTanPanRocDtlsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyTanPanRocDtls} and its DTO {@link CompanyTanPanRocDtlsDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompanyRequestMapper.class})
public interface CompanyTanPanRocDtlsMapper extends EntityMapper<CompanyTanPanRocDtlsDTO, CompanyTanPanRocDtls> {

    @Mapping(source = "record.id", target = "recordId")
    CompanyTanPanRocDtlsDTO toDto(CompanyTanPanRocDtls companyTanPanRocDtls);

    @Mapping(source = "recordId", target = "record")
    CompanyTanPanRocDtls toEntity(CompanyTanPanRocDtlsDTO companyTanPanRocDtlsDTO);

    default CompanyTanPanRocDtls fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyTanPanRocDtls companyTanPanRocDtls = new CompanyTanPanRocDtls();
        companyTanPanRocDtls.setId(id);
        return companyTanPanRocDtls;
    }
}
