package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompGstNotApplicableDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompGstNotApplicable} and its DTO {@link CompGstNotApplicableDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMasterMapper.class})
public interface CompGstNotApplicableMapper extends EntityMapper<CompGstNotApplicableDTO, CompGstNotApplicable> {

    @Mapping(source = "contactId.id", target = "contactIdId")
    CompGstNotApplicableDTO toDto(CompGstNotApplicable compGstNotApplicable);

    @Mapping(source = "contactIdId", target = "contactId")
    CompGstNotApplicable toEntity(CompGstNotApplicableDTO compGstNotApplicableDTO);

    default CompGstNotApplicable fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompGstNotApplicable compGstNotApplicable = new CompGstNotApplicable();
        compGstNotApplicable.setId(id);
        return compGstNotApplicable;
    }
}
