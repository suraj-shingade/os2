package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyChangeRequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyChangeRequest} and its DTO {@link CompanyChangeRequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {OnesourceCompanyMasterMapper.class})
public interface CompanyChangeRequestMapper extends EntityMapper<CompanyChangeRequestDTO, CompanyChangeRequest> {

    @Mapping(source = "oldCompanyCode.id", target = "oldCompanyCodeId")
    CompanyChangeRequestDTO toDto(CompanyChangeRequest companyChangeRequest);

    @Mapping(source = "oldCompanyCodeId", target = "oldCompanyCode")
    CompanyChangeRequest toEntity(CompanyChangeRequestDTO companyChangeRequestDTO);

    default CompanyChangeRequest fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyChangeRequest companyChangeRequest = new CompanyChangeRequest();
        companyChangeRequest.setId(id);
        return companyChangeRequest;
    }
}
