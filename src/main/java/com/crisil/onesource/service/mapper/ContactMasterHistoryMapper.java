package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.ContactMasterHistoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactMasterHistory} and its DTO {@link ContactMasterHistoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContactMasterHistoryMapper extends EntityMapper<ContactMasterHistoryDTO, ContactMasterHistory> {



    default ContactMasterHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactMasterHistory contactMasterHistory = new ContactMasterHistory();
        contactMasterHistory.setId(id);
        return contactMasterHistory;
    }
}
