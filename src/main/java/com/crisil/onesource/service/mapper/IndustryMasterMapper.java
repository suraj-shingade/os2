package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.IndustryMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link IndustryMaster} and its DTO {@link IndustryMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IndustryMasterMapper extends EntityMapper<IndustryMasterDTO, IndustryMaster> {


    @Mapping(target = "onesourceCompanyMasters", ignore = true)
    @Mapping(target = "removeOnesourceCompanyMaster", ignore = true)
    IndustryMaster toEntity(IndustryMasterDTO industryMasterDTO);

    default IndustryMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        IndustryMaster industryMaster = new IndustryMaster();
        industryMaster.setId(id);
        return industryMaster;
    }
}
