package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.BusinessAreaMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BusinessAreaMaster} and its DTO {@link BusinessAreaMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BusinessAreaMasterMapper extends EntityMapper<BusinessAreaMasterDTO, BusinessAreaMaster> {



    default BusinessAreaMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        BusinessAreaMaster businessAreaMaster = new BusinessAreaMaster();
        businessAreaMaster.setId(id);
        return businessAreaMaster;
    }
}
