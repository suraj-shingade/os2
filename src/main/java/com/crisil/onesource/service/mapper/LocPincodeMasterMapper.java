package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocPincodeMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocPincodeMaster} and its DTO {@link LocPincodeMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {LocStateMasterMapper.class, LocDistrictMasterMapper.class})
public interface LocPincodeMasterMapper extends EntityMapper<LocPincodeMasterDTO, LocPincodeMaster> {

    @Mapping(source = "stateId.id", target = "stateIdId")
    @Mapping(source = "districtId.id", target = "districtIdId")
    LocPincodeMasterDTO toDto(LocPincodeMaster locPincodeMaster);

    @Mapping(target = "companyAddressMasters", ignore = true)
    @Mapping(target = "removeCompanyAddressMaster", ignore = true)
    @Mapping(target = "onesourceCompanyMasters", ignore = true)
    @Mapping(target = "removeOnesourceCompanyMaster", ignore = true)
    @Mapping(source = "stateIdId", target = "stateId")
    @Mapping(source = "districtIdId", target = "districtId")
    LocPincodeMaster toEntity(LocPincodeMasterDTO locPincodeMasterDTO);

    default LocPincodeMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocPincodeMaster locPincodeMaster = new LocPincodeMaster();
        locPincodeMaster.setId(id);
        return locPincodeMaster;
    }
}
