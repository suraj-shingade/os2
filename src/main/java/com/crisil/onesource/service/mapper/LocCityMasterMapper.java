package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocCityMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocCityMaster} and its DTO {@link LocCityMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {LocStateMasterMapper.class, LocDistrictMasterMapper.class})
public interface LocCityMasterMapper extends EntityMapper<LocCityMasterDTO, LocCityMaster> {

    @Mapping(source = "stateId.id", target = "stateIdId")
    @Mapping(source = "districtId.id", target = "districtIdId")
    LocCityMasterDTO toDto(LocCityMaster locCityMaster);

    @Mapping(target = "locCityPincodeMappings", ignore = true)
    @Mapping(target = "removeLocCityPincodeMapping", ignore = true)
    @Mapping(source = "stateIdId", target = "stateId")
    @Mapping(source = "districtIdId", target = "districtId")
    LocCityMaster toEntity(LocCityMasterDTO locCityMasterDTO);

    default LocCityMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocCityMaster locCityMaster = new LocCityMaster();
        locCityMaster.setId(id);
        return locCityMaster;
    }
}
