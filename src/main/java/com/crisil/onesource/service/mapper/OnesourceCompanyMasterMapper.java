package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OnesourceCompanyMaster} and its DTO {@link OnesourceCompanyMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompanyTypeMasterMapper.class, CompanySectorMasterMapper.class, IndustryMasterMapper.class, GroupMasterMapper.class, LocPincodeMasterMapper.class})
public interface OnesourceCompanyMasterMapper extends EntityMapper<OnesourceCompanyMasterDTO, OnesourceCompanyMaster> {

    @Mapping(source = "companyTypeId.id", target = "companyTypeIdId")
    @Mapping(source = "sectorId.id", target = "sectorIdId")
    @Mapping(source = "industryId.id", target = "industryIdId")
    @Mapping(source = "groupId.id", target = "groupIdId")
    @Mapping(source = "pin.id", target = "pinId")
    OnesourceCompanyMasterDTO toDto(OnesourceCompanyMaster onesourceCompanyMaster);

    @Mapping(target = "companyChangeRequests", ignore = true)
    @Mapping(target = "removeCompanyChangeRequest", ignore = true)
    @Mapping(target = "companyIsinMappings", ignore = true)
    @Mapping(target = "removeCompanyIsinMapping", ignore = true)
    @Mapping(target = "companyListingStatuses", ignore = true)
    @Mapping(target = "removeCompanyListingStatus", ignore = true)
    @Mapping(target = "contactMasters", ignore = true)
    @Mapping(target = "removeContactMaster", ignore = true)
    @Mapping(target = "karzaCompInformations", ignore = true)
    @Mapping(target = "removeKarzaCompInformation", ignore = true)
    @Mapping(source = "companyTypeIdId", target = "companyTypeId")
    @Mapping(source = "sectorIdId", target = "sectorId")
    @Mapping(source = "industryIdId", target = "industryId")
    @Mapping(source = "groupIdId", target = "groupId")
    @Mapping(source = "pinId", target = "pin")
    OnesourceCompanyMaster toEntity(OnesourceCompanyMasterDTO onesourceCompanyMasterDTO);

    default OnesourceCompanyMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        OnesourceCompanyMaster onesourceCompanyMaster = new OnesourceCompanyMaster();
        onesourceCompanyMaster.setId(id);
        return onesourceCompanyMaster;
    }
}
