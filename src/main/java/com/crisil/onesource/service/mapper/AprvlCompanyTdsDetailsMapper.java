package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.AprvlCompanyTdsDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AprvlCompanyTdsDetails} and its DTO {@link AprvlCompanyTdsDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AprvlCompanyTdsDetailsMapper extends EntityMapper<AprvlCompanyTdsDetailsDTO, AprvlCompanyTdsDetails> {



    default AprvlCompanyTdsDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        AprvlCompanyTdsDetails aprvlCompanyTdsDetails = new AprvlCompanyTdsDetails();
        aprvlCompanyTdsDetails.setId(id);
        return aprvlCompanyTdsDetails;
    }
}
