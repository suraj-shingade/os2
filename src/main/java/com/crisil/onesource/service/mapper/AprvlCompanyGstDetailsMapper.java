package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.AprvlCompanyGstDetailsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AprvlCompanyGstDetails} and its DTO {@link AprvlCompanyGstDetailsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AprvlCompanyGstDetailsMapper extends EntityMapper<AprvlCompanyGstDetailsDTO, AprvlCompanyGstDetails> {



    default AprvlCompanyGstDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        AprvlCompanyGstDetails aprvlCompanyGstDetails = new AprvlCompanyGstDetails();
        aprvlCompanyGstDetails.setId(id);
        return aprvlCompanyGstDetails;
    }
}
