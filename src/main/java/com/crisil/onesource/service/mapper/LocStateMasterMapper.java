package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocStateMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocStateMaster} and its DTO {@link LocStateMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {LocRegionMasterMapper.class, LocCountryMasterMapper.class})
public interface LocStateMasterMapper extends EntityMapper<LocStateMasterDTO, LocStateMaster> {

    @Mapping(source = "regionId.id", target = "regionIdId")
    @Mapping(source = "country.id", target = "countryId")
    LocStateMasterDTO toDto(LocStateMaster locStateMaster);

    @Mapping(target = "locCityMasters", ignore = true)
    @Mapping(target = "removeLocCityMaster", ignore = true)
    @Mapping(target = "locDistrictMasters", ignore = true)
    @Mapping(target = "removeLocDistrictMaster", ignore = true)
    @Mapping(target = "locPincodeMasters", ignore = true)
    @Mapping(target = "removeLocPincodeMaster", ignore = true)
    @Mapping(source = "regionIdId", target = "regionId")
    @Mapping(source = "countryId", target = "country")
    LocStateMaster toEntity(LocStateMasterDTO locStateMasterDTO);

    default LocStateMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocStateMaster locStateMaster = new LocStateMaster();
        locStateMaster.setId(id);
        return locStateMaster;
    }
}
