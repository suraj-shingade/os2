package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanySegmentMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanySegmentMaster} and its DTO {@link CompanySegmentMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CompanySegmentMasterMapper extends EntityMapper<CompanySegmentMasterDTO, CompanySegmentMaster> {



    default CompanySegmentMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanySegmentMaster companySegmentMaster = new CompanySegmentMaster();
        companySegmentMaster.setId(id);
        return companySegmentMaster;
    }
}
