package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyRequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyRequest} and its DTO {@link CompanyRequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CompanyRequestMapper extends EntityMapper<CompanyRequestDTO, CompanyRequest> {


    @Mapping(target = "companyAddressMasters", ignore = true)
    @Mapping(target = "removeCompanyAddressMaster", ignore = true)
    @Mapping(target = "companyTanPanRocDtls", ignore = true)
    @Mapping(target = "removeCompanyTanPanRocDtls", ignore = true)
    CompanyRequest toEntity(CompanyRequestDTO companyRequestDTO);

    default CompanyRequest fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setId(id);
        return companyRequest;
    }
}
