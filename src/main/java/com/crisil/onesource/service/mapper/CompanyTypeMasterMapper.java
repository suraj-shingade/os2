package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.CompanyTypeMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CompanyTypeMaster} and its DTO {@link CompanyTypeMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CompanyTypeMasterMapper extends EntityMapper<CompanyTypeMasterDTO, CompanyTypeMaster> {


    @Mapping(target = "onesourceCompanyMasters", ignore = true)
    @Mapping(target = "removeOnesourceCompanyMaster", ignore = true)
    CompanyTypeMaster toEntity(CompanyTypeMasterDTO companyTypeMasterDTO);

    default CompanyTypeMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        CompanyTypeMaster companyTypeMaster = new CompanyTypeMaster();
        companyTypeMaster.setId(id);
        return companyTypeMaster;
    }
}
