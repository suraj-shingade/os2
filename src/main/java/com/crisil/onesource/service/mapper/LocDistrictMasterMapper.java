package com.crisil.onesource.service.mapper;


import com.crisil.onesource.domain.*;
import com.crisil.onesource.service.dto.LocDistrictMasterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link LocDistrictMaster} and its DTO {@link LocDistrictMasterDTO}.
 */
@Mapper(componentModel = "spring", uses = {LocStateMasterMapper.class})
public interface LocDistrictMasterMapper extends EntityMapper<LocDistrictMasterDTO, LocDistrictMaster> {

    @Mapping(source = "stateId.id", target = "stateIdId")
    LocDistrictMasterDTO toDto(LocDistrictMaster locDistrictMaster);

    @Mapping(target = "locCityMasters", ignore = true)
    @Mapping(target = "removeLocCityMaster", ignore = true)
    @Mapping(target = "locPincodeMasters", ignore = true)
    @Mapping(target = "removeLocPincodeMaster", ignore = true)
    @Mapping(source = "stateIdId", target = "stateId")
    LocDistrictMaster toEntity(LocDistrictMasterDTO locDistrictMasterDTO);

    default LocDistrictMaster fromId(Long id) {
        if (id == null) {
            return null;
        }
        LocDistrictMaster locDistrictMaster = new LocDistrictMaster();
        locDistrictMaster.setId(id);
        return locDistrictMaster;
    }
}
