package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.KarzaCompInformation} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.KarzaCompInformationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /karza-comp-informations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class KarzaCompInformationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter pan;

    private StringFilter companyNameKarza;

    private StringFilter address;

    private StringFilter typeofapproval;

    private StringFilter status;

    private StringFilter gstin;

    private LocalDateFilter createdDate;

    private StringFilter createdBy;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private StringFilter state;

    private StringFilter isValidGst;

    private StringFilter source;

    private StringFilter comments;

    private StringFilter checkdigit;

    private StringFilter taxpayerName;

    private StringFilter natuteOfBuss;

    private StringFilter mobileNo;

    private StringFilter email;

    private StringFilter tradeName;

    private StringFilter constitBusiness;

    private LongFilter companyCodeId;

    public KarzaCompInformationCriteria() {
    }

    public KarzaCompInformationCriteria(KarzaCompInformationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.pan = other.pan == null ? null : other.pan.copy();
        this.companyNameKarza = other.companyNameKarza == null ? null : other.companyNameKarza.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.typeofapproval = other.typeofapproval == null ? null : other.typeofapproval.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.gstin = other.gstin == null ? null : other.gstin.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.state = other.state == null ? null : other.state.copy();
        this.isValidGst = other.isValidGst == null ? null : other.isValidGst.copy();
        this.source = other.source == null ? null : other.source.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
        this.checkdigit = other.checkdigit == null ? null : other.checkdigit.copy();
        this.taxpayerName = other.taxpayerName == null ? null : other.taxpayerName.copy();
        this.natuteOfBuss = other.natuteOfBuss == null ? null : other.natuteOfBuss.copy();
        this.mobileNo = other.mobileNo == null ? null : other.mobileNo.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.tradeName = other.tradeName == null ? null : other.tradeName.copy();
        this.constitBusiness = other.constitBusiness == null ? null : other.constitBusiness.copy();
        this.companyCodeId = other.companyCodeId == null ? null : other.companyCodeId.copy();
    }

    @Override
    public KarzaCompInformationCriteria copy() {
        return new KarzaCompInformationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPan() {
        return pan;
    }

    public void setPan(StringFilter pan) {
        this.pan = pan;
    }

    public StringFilter getCompanyNameKarza() {
        return companyNameKarza;
    }

    public void setCompanyNameKarza(StringFilter companyNameKarza) {
        this.companyNameKarza = companyNameKarza;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getTypeofapproval() {
        return typeofapproval;
    }

    public void setTypeofapproval(StringFilter typeofapproval) {
        this.typeofapproval = typeofapproval;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getGstin() {
        return gstin;
    }

    public void setGstin(StringFilter gstin) {
        this.gstin = gstin;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getState() {
        return state;
    }

    public void setState(StringFilter state) {
        this.state = state;
    }

    public StringFilter getIsValidGst() {
        return isValidGst;
    }

    public void setIsValidGst(StringFilter isValidGst) {
        this.isValidGst = isValidGst;
    }

    public StringFilter getSource() {
        return source;
    }

    public void setSource(StringFilter source) {
        this.source = source;
    }

    public StringFilter getComments() {
        return comments;
    }

    public void setComments(StringFilter comments) {
        this.comments = comments;
    }

    public StringFilter getCheckdigit() {
        return checkdigit;
    }

    public void setCheckdigit(StringFilter checkdigit) {
        this.checkdigit = checkdigit;
    }

    public StringFilter getTaxpayerName() {
        return taxpayerName;
    }

    public void setTaxpayerName(StringFilter taxpayerName) {
        this.taxpayerName = taxpayerName;
    }

    public StringFilter getNatuteOfBuss() {
        return natuteOfBuss;
    }

    public void setNatuteOfBuss(StringFilter natuteOfBuss) {
        this.natuteOfBuss = natuteOfBuss;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getTradeName() {
        return tradeName;
    }

    public void setTradeName(StringFilter tradeName) {
        this.tradeName = tradeName;
    }

    public StringFilter getConstitBusiness() {
        return constitBusiness;
    }

    public void setConstitBusiness(StringFilter constitBusiness) {
        this.constitBusiness = constitBusiness;
    }

    public LongFilter getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(LongFilter companyCodeId) {
        this.companyCodeId = companyCodeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final KarzaCompInformationCriteria that = (KarzaCompInformationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(pan, that.pan) &&
            Objects.equals(companyNameKarza, that.companyNameKarza) &&
            Objects.equals(address, that.address) &&
            Objects.equals(typeofapproval, that.typeofapproval) &&
            Objects.equals(status, that.status) &&
            Objects.equals(gstin, that.gstin) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(state, that.state) &&
            Objects.equals(isValidGst, that.isValidGst) &&
            Objects.equals(source, that.source) &&
            Objects.equals(comments, that.comments) &&
            Objects.equals(checkdigit, that.checkdigit) &&
            Objects.equals(taxpayerName, that.taxpayerName) &&
            Objects.equals(natuteOfBuss, that.natuteOfBuss) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(email, that.email) &&
            Objects.equals(tradeName, that.tradeName) &&
            Objects.equals(constitBusiness, that.constitBusiness) &&
            Objects.equals(companyCodeId, that.companyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        pan,
        companyNameKarza,
        address,
        typeofapproval,
        status,
        gstin,
        createdDate,
        createdBy,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        state,
        isValidGst,
        source,
        comments,
        checkdigit,
        taxpayerName,
        natuteOfBuss,
        mobileNo,
        email,
        tradeName,
        constitBusiness,
        companyCodeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KarzaCompInformationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (pan != null ? "pan=" + pan + ", " : "") +
                (companyNameKarza != null ? "companyNameKarza=" + companyNameKarza + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (typeofapproval != null ? "typeofapproval=" + typeofapproval + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (gstin != null ? "gstin=" + gstin + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (state != null ? "state=" + state + ", " : "") +
                (isValidGst != null ? "isValidGst=" + isValidGst + ", " : "") +
                (source != null ? "source=" + source + ", " : "") +
                (comments != null ? "comments=" + comments + ", " : "") +
                (checkdigit != null ? "checkdigit=" + checkdigit + ", " : "") +
                (taxpayerName != null ? "taxpayerName=" + taxpayerName + ", " : "") +
                (natuteOfBuss != null ? "natuteOfBuss=" + natuteOfBuss + ", " : "") +
                (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (tradeName != null ? "tradeName=" + tradeName + ", " : "") +
                (constitBusiness != null ? "constitBusiness=" + constitBusiness + ", " : "") +
                (companyCodeId != null ? "companyCodeId=" + companyCodeId + ", " : "") +
            "}";
    }

}
