package com.crisil.onesource.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.crisil.onesource.domain.AprvlContactMaster} entity.
 */
public class AprvlContactMasterDTO implements Serializable {
    
    private Long id;

    @Size(max = 100)
    private String lastname;

    @Size(max = 100)
    private String joinednewcompany;

    private Integer cityid;

    private Integer hdeleted;

    @Size(max = 100)
    private String recordid;

    @Size(max = 100)
    private String levelimportance;

    @Size(max = 100)
    private String department;

    @Size(max = 100)
    private String fax;

    @Size(max = 100)
    private String email;

    @Size(max = 5)
    private String keyperson;

    @Size(max = 50)
    private String companycode;

    @Size(max = 100)
    private String pincode;

    @Lob
    private String comments;

    @Lob
    private String address3;

    @Lob
    private String address2;

    @Size(max = 100)
    private String contactFullName;

    @Size(max = 100)
    private String address1;

    @Size(max = 50)
    private String cityname;

    @Size(max = 100)
    private String countryname;

    @Size(max = 100)
    private String mobile;

    @Size(max = 100)
    private String statename;

    private LocalDate modifyDate;

    private Integer ofaflag;

    @Size(max = 100)
    private String firstname;

    @Size(max = 100)
    private String middlename;

    @Size(max = 100)
    private String removereason;

    @Size(max = 5)
    private String salutation;

    @Size(max = 100)
    private String designation;

    @Size(max = 15)
    private String status;

    @NotNull
    private Integer ctlgSrno;

    @Size(max = 1)
    private String ctlgIsDeleted;

    private Long jsonStoreId;

    @Size(max = 1)
    private String isDataProcessed;

    private Instant lastModifiedDate;

    @Size(max = 100)
    private String lastModifiedBy;

    @Size(max = 100)
    private String createdBy;

    private Instant createdDate;

    @Size(max = 1)
    private String operation;

    @Size(max = 150)
    private String contactId;

    @Lob
    private String phone;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getJoinednewcompany() {
        return joinednewcompany;
    }

    public void setJoinednewcompany(String joinednewcompany) {
        this.joinednewcompany = joinednewcompany;
    }

    public Integer getCityid() {
        return cityid;
    }

    public void setCityid(Integer cityid) {
        this.cityid = cityid;
    }

    public Integer getHdeleted() {
        return hdeleted;
    }

    public void setHdeleted(Integer hdeleted) {
        this.hdeleted = hdeleted;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getLevelimportance() {
        return levelimportance;
    }

    public void setLevelimportance(String levelimportance) {
        this.levelimportance = levelimportance;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKeyperson() {
        return keyperson;
    }

    public void setKeyperson(String keyperson) {
        this.keyperson = keyperson;
    }

    public String getCompanycode() {
        return companycode;
    }

    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getContactFullName() {
        return contactFullName;
    }

    public void setContactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getCountryname() {
        return countryname;
    }

    public void setCountryname(String countryname) {
        this.countryname = countryname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public LocalDate getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDate modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Integer getOfaflag() {
        return ofaflag;
    }

    public void setOfaflag(Integer ofaflag) {
        this.ofaflag = ofaflag;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getRemovereason() {
        return removereason;
    }

    public void setRemovereason(String removereason) {
        this.removereason = removereason;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public Long getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(Long jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AprvlContactMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((AprvlContactMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AprvlContactMasterDTO{" +
            "id=" + getId() +
            ", lastname='" + getLastname() + "'" +
            ", joinednewcompany='" + getJoinednewcompany() + "'" +
            ", cityid=" + getCityid() +
            ", hdeleted=" + getHdeleted() +
            ", recordid='" + getRecordid() + "'" +
            ", levelimportance='" + getLevelimportance() + "'" +
            ", department='" + getDepartment() + "'" +
            ", fax='" + getFax() + "'" +
            ", email='" + getEmail() + "'" +
            ", keyperson='" + getKeyperson() + "'" +
            ", companycode='" + getCompanycode() + "'" +
            ", pincode='" + getPincode() + "'" +
            ", comments='" + getComments() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", contactFullName='" + getContactFullName() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", cityname='" + getCityname() + "'" +
            ", countryname='" + getCountryname() + "'" +
            ", mobile='" + getMobile() + "'" +
            ", statename='" + getStatename() + "'" +
            ", modifyDate='" + getModifyDate() + "'" +
            ", ofaflag=" + getOfaflag() +
            ", firstname='" + getFirstname() + "'" +
            ", middlename='" + getMiddlename() + "'" +
            ", removereason='" + getRemovereason() + "'" +
            ", salutation='" + getSalutation() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", status='" + getStatus() + "'" +
            ", ctlgSrno=" + getCtlgSrno() +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", jsonStoreId=" + getJsonStoreId() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", operation='" + getOperation() + "'" +
            ", contactId='" + getContactId() + "'" +
            ", phone='" + getPhone() + "'" +
            "}";
    }
}
