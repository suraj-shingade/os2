package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyTanPanRocDtls} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyTanPanRocDtlsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-tan-pan-roc-dtls?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyTanPanRocDtlsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tan;

    private StringFilter pan;

    private StringFilter roc;

    private LocalDateFilter createdDate;

    private StringFilter createdBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private IntegerFilter companyRiskStatusId;

    private StringFilter companyRistComments;

    private StringFilter companyBackground;

    private StringFilter isDeleted;

    private StringFilter rptFlag;

    private StringFilter gstExempt;

    private StringFilter gstNumberNotApplicable;

    private StringFilter companyCode;

    private LongFilter recordId;

    public CompanyTanPanRocDtlsCriteria() {
    }

    public CompanyTanPanRocDtlsCriteria(CompanyTanPanRocDtlsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.tan = other.tan == null ? null : other.tan.copy();
        this.pan = other.pan == null ? null : other.pan.copy();
        this.roc = other.roc == null ? null : other.roc.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.companyRiskStatusId = other.companyRiskStatusId == null ? null : other.companyRiskStatusId.copy();
        this.companyRistComments = other.companyRistComments == null ? null : other.companyRistComments.copy();
        this.companyBackground = other.companyBackground == null ? null : other.companyBackground.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.rptFlag = other.rptFlag == null ? null : other.rptFlag.copy();
        this.gstExempt = other.gstExempt == null ? null : other.gstExempt.copy();
        this.gstNumberNotApplicable = other.gstNumberNotApplicable == null ? null : other.gstNumberNotApplicable.copy();
        this.companyCode = other.companyCode == null ? null : other.companyCode.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
    }

    @Override
    public CompanyTanPanRocDtlsCriteria copy() {
        return new CompanyTanPanRocDtlsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTan() {
        return tan;
    }

    public void setTan(StringFilter tan) {
        this.tan = tan;
    }

    public StringFilter getPan() {
        return pan;
    }

    public void setPan(StringFilter pan) {
        this.pan = pan;
    }

    public StringFilter getRoc() {
        return roc;
    }

    public void setRoc(StringFilter roc) {
        this.roc = roc;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public IntegerFilter getCompanyRiskStatusId() {
        return companyRiskStatusId;
    }

    public void setCompanyRiskStatusId(IntegerFilter companyRiskStatusId) {
        this.companyRiskStatusId = companyRiskStatusId;
    }

    public StringFilter getCompanyRistComments() {
        return companyRistComments;
    }

    public void setCompanyRistComments(StringFilter companyRistComments) {
        this.companyRistComments = companyRistComments;
    }

    public StringFilter getCompanyBackground() {
        return companyBackground;
    }

    public void setCompanyBackground(StringFilter companyBackground) {
        this.companyBackground = companyBackground;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getRptFlag() {
        return rptFlag;
    }

    public void setRptFlag(StringFilter rptFlag) {
        this.rptFlag = rptFlag;
    }

    public StringFilter getGstExempt() {
        return gstExempt;
    }

    public void setGstExempt(StringFilter gstExempt) {
        this.gstExempt = gstExempt;
    }

    public StringFilter getGstNumberNotApplicable() {
        return gstNumberNotApplicable;
    }

    public void setGstNumberNotApplicable(StringFilter gstNumberNotApplicable) {
        this.gstNumberNotApplicable = gstNumberNotApplicable;
    }

    public StringFilter getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(StringFilter companyCode) {
        this.companyCode = companyCode;
    }

    public LongFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(LongFilter recordId) {
        this.recordId = recordId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyTanPanRocDtlsCriteria that = (CompanyTanPanRocDtlsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tan, that.tan) &&
            Objects.equals(pan, that.pan) &&
            Objects.equals(roc, that.roc) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(companyRiskStatusId, that.companyRiskStatusId) &&
            Objects.equals(companyRistComments, that.companyRistComments) &&
            Objects.equals(companyBackground, that.companyBackground) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(rptFlag, that.rptFlag) &&
            Objects.equals(gstExempt, that.gstExempt) &&
            Objects.equals(gstNumberNotApplicable, that.gstNumberNotApplicable) &&
            Objects.equals(companyCode, that.companyCode) &&
            Objects.equals(recordId, that.recordId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tan,
        pan,
        roc,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        companyRiskStatusId,
        companyRistComments,
        companyBackground,
        isDeleted,
        rptFlag,
        gstExempt,
        gstNumberNotApplicable,
        companyCode,
        recordId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyTanPanRocDtlsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tan != null ? "tan=" + tan + ", " : "") +
                (pan != null ? "pan=" + pan + ", " : "") +
                (roc != null ? "roc=" + roc + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (companyRiskStatusId != null ? "companyRiskStatusId=" + companyRiskStatusId + ", " : "") +
                (companyRistComments != null ? "companyRistComments=" + companyRistComments + ", " : "") +
                (companyBackground != null ? "companyBackground=" + companyBackground + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (rptFlag != null ? "rptFlag=" + rptFlag + ", " : "") +
                (gstExempt != null ? "gstExempt=" + gstExempt + ", " : "") +
                (gstNumberNotApplicable != null ? "gstNumberNotApplicable=" + gstNumberNotApplicable + ", " : "") +
                (companyCode != null ? "companyCode=" + companyCode + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
            "}";
    }

}
