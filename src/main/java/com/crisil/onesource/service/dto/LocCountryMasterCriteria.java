package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocCountryMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocCountryMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-country-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocCountryMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter countryName;

    private StringFilter countryCode;

    private StringFilter isDeleted;

    private StringFilter zipCodeFormat;

    private StringFilter continentId;

    private StringFilter flagImagePath;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private IntegerFilter countryId;

    private StringFilter isApproved;

    private IntegerFilter seqOrder;

    private LongFilter locStateMasterId;

    public LocCountryMasterCriteria() {
    }

    public LocCountryMasterCriteria(LocCountryMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.countryName = other.countryName == null ? null : other.countryName.copy();
        this.countryCode = other.countryCode == null ? null : other.countryCode.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.zipCodeFormat = other.zipCodeFormat == null ? null : other.zipCodeFormat.copy();
        this.continentId = other.continentId == null ? null : other.continentId.copy();
        this.flagImagePath = other.flagImagePath == null ? null : other.flagImagePath.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.countryId = other.countryId == null ? null : other.countryId.copy();
        this.isApproved = other.isApproved == null ? null : other.isApproved.copy();
        this.seqOrder = other.seqOrder == null ? null : other.seqOrder.copy();
        this.locStateMasterId = other.locStateMasterId == null ? null : other.locStateMasterId.copy();
    }

    @Override
    public LocCountryMasterCriteria copy() {
        return new LocCountryMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCountryName() {
        return countryName;
    }

    public void setCountryName(StringFilter countryName) {
        this.countryName = countryName;
    }

    public StringFilter getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(StringFilter countryCode) {
        this.countryCode = countryCode;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getZipCodeFormat() {
        return zipCodeFormat;
    }

    public void setZipCodeFormat(StringFilter zipCodeFormat) {
        this.zipCodeFormat = zipCodeFormat;
    }

    public StringFilter getContinentId() {
        return continentId;
    }

    public void setContinentId(StringFilter continentId) {
        this.continentId = continentId;
    }

    public StringFilter getFlagImagePath() {
        return flagImagePath;
    }

    public void setFlagImagePath(StringFilter flagImagePath) {
        this.flagImagePath = flagImagePath;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public IntegerFilter getCountryId() {
        return countryId;
    }

    public void setCountryId(IntegerFilter countryId) {
        this.countryId = countryId;
    }

    public StringFilter getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(StringFilter isApproved) {
        this.isApproved = isApproved;
    }

    public IntegerFilter getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(IntegerFilter seqOrder) {
        this.seqOrder = seqOrder;
    }

    public LongFilter getLocStateMasterId() {
        return locStateMasterId;
    }

    public void setLocStateMasterId(LongFilter locStateMasterId) {
        this.locStateMasterId = locStateMasterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocCountryMasterCriteria that = (LocCountryMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(countryName, that.countryName) &&
            Objects.equals(countryCode, that.countryCode) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(zipCodeFormat, that.zipCodeFormat) &&
            Objects.equals(continentId, that.continentId) &&
            Objects.equals(flagImagePath, that.flagImagePath) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(countryId, that.countryId) &&
            Objects.equals(isApproved, that.isApproved) &&
            Objects.equals(seqOrder, that.seqOrder) &&
            Objects.equals(locStateMasterId, that.locStateMasterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        countryName,
        countryCode,
        isDeleted,
        zipCodeFormat,
        continentId,
        flagImagePath,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        countryId,
        isApproved,
        seqOrder,
        locStateMasterId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCountryMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (countryName != null ? "countryName=" + countryName + ", " : "") +
                (countryCode != null ? "countryCode=" + countryCode + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (zipCodeFormat != null ? "zipCodeFormat=" + zipCodeFormat + ", " : "") +
                (continentId != null ? "continentId=" + continentId + ", " : "") +
                (flagImagePath != null ? "flagImagePath=" + flagImagePath + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (countryId != null ? "countryId=" + countryId + ", " : "") +
                (isApproved != null ? "isApproved=" + isApproved + ", " : "") +
                (seqOrder != null ? "seqOrder=" + seqOrder + ", " : "") +
                (locStateMasterId != null ? "locStateMasterId=" + locStateMasterId + ", " : "") +
            "}";
    }

}
