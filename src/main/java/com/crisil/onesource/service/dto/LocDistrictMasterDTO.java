package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.LocDistrictMaster} entity.
 */
@ApiModel(description = "Location_Master_Table")
public class LocDistrictMasterDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 100)
    private String districtName;

    @NotNull
    @Size(max = 250)
    private String description;

    @NotNull
    private Integer districtId;

    @Size(max = 1)
    private String isDeleted;

    private Instant createdDate;

    @Size(max = 100)
    private String createdBy;

    private Instant lastModifiedDate;

    @Size(max = 50)
    private String lastModifiedBy;

    @Size(max = 1)
    private String isApproved;

    private Integer seqOrder;


    private Long stateIdId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Long getStateIdId() {
        return stateIdId;
    }

    public void setStateIdId(Long locStateMasterId) {
        this.stateIdId = locStateMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocDistrictMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((LocDistrictMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocDistrictMasterDTO{" +
            "id=" + getId() +
            ", districtName='" + getDistrictName() + "'" +
            ", description='" + getDescription() + "'" +
            ", districtId=" + getDistrictId() +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            ", stateIdId=" + getStateIdId() +
            "}";
    }
}
