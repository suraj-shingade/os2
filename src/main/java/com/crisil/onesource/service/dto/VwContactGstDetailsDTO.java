package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.VwContactGstDetails} entity.
 */
@ApiModel(description = "VIEW")
public class VwContactGstDetailsDTO implements Serializable {
    
    private Long id;

    @Size(max = 100)
    private String companyRecordId;

    @Size(max = 100)
    private String companyName;

    @NotNull
    @Size(max = 50)
    private String companyCode;

    @Size(max = 25)
    private String gstNumber;

    @Size(max = 1)
    private String isSezContactFlag;

    @Size(max = 100)
    private String stateName;

    @Size(max = 0)
    private String gstStateCode;

    @NotNull
    @Size(max = 150)
    private String contactId;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyRecordId() {
        return companyRecordId;
    }

    public void setCompanyRecordId(String companyRecordId) {
        this.companyRecordId = companyRecordId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getIsSezContactFlag() {
        return isSezContactFlag;
    }

    public void setIsSezContactFlag(String isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getGstStateCode() {
        return gstStateCode;
    }

    public void setGstStateCode(String gstStateCode) {
        this.gstStateCode = gstStateCode;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VwContactGstDetailsDTO)) {
            return false;
        }

        return id != null && id.equals(((VwContactGstDetailsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VwContactGstDetailsDTO{" +
            "id=" + getId() +
            ", companyRecordId='" + getCompanyRecordId() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            ", gstNumber='" + getGstNumber() + "'" +
            ", isSezContactFlag='" + getIsSezContactFlag() + "'" +
            ", stateName='" + getStateName() + "'" +
            ", gstStateCode='" + getGstStateCode() + "'" +
            ", contactId='" + getContactId() + "'" +
            "}";
    }
}
