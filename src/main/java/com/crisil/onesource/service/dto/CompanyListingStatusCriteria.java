package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyListingStatus} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyListingStatusResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-listing-statuses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyListingStatusCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter listingId;

    private StringFilter scriptCode;

    private StringFilter exchange;

    private StringFilter status;

    private LocalDateFilter statusDate;

    private StringFilter createdBy;

    private LocalDateFilter createdDate;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private StringFilter operation;

    private IntegerFilter ctlgSrno;

    private IntegerFilter jsonStoreId;

    private StringFilter isDataProcessed;

    private StringFilter ctlgIsDeleted;

    private LongFilter companyCodeId;

    public CompanyListingStatusCriteria() {
    }

    public CompanyListingStatusCriteria(CompanyListingStatusCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.listingId = other.listingId == null ? null : other.listingId.copy();
        this.scriptCode = other.scriptCode == null ? null : other.scriptCode.copy();
        this.exchange = other.exchange == null ? null : other.exchange.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.statusDate = other.statusDate == null ? null : other.statusDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.operation = other.operation == null ? null : other.operation.copy();
        this.ctlgSrno = other.ctlgSrno == null ? null : other.ctlgSrno.copy();
        this.jsonStoreId = other.jsonStoreId == null ? null : other.jsonStoreId.copy();
        this.isDataProcessed = other.isDataProcessed == null ? null : other.isDataProcessed.copy();
        this.ctlgIsDeleted = other.ctlgIsDeleted == null ? null : other.ctlgIsDeleted.copy();
        this.companyCodeId = other.companyCodeId == null ? null : other.companyCodeId.copy();
    }

    @Override
    public CompanyListingStatusCriteria copy() {
        return new CompanyListingStatusCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getListingId() {
        return listingId;
    }

    public void setListingId(IntegerFilter listingId) {
        this.listingId = listingId;
    }

    public StringFilter getScriptCode() {
        return scriptCode;
    }

    public void setScriptCode(StringFilter scriptCode) {
        this.scriptCode = scriptCode;
    }

    public StringFilter getExchange() {
        return exchange;
    }

    public void setExchange(StringFilter exchange) {
        this.exchange = exchange;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public LocalDateFilter getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(LocalDateFilter statusDate) {
        this.statusDate = statusDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getOperation() {
        return operation;
    }

    public void setOperation(StringFilter operation) {
        this.operation = operation;
    }

    public IntegerFilter getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(IntegerFilter ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public IntegerFilter getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(IntegerFilter jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public StringFilter getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(StringFilter isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public StringFilter getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(StringFilter ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public LongFilter getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(LongFilter companyCodeId) {
        this.companyCodeId = companyCodeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyListingStatusCriteria that = (CompanyListingStatusCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(listingId, that.listingId) &&
            Objects.equals(scriptCode, that.scriptCode) &&
            Objects.equals(exchange, that.exchange) &&
            Objects.equals(status, that.status) &&
            Objects.equals(statusDate, that.statusDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(operation, that.operation) &&
            Objects.equals(ctlgSrno, that.ctlgSrno) &&
            Objects.equals(jsonStoreId, that.jsonStoreId) &&
            Objects.equals(isDataProcessed, that.isDataProcessed) &&
            Objects.equals(ctlgIsDeleted, that.ctlgIsDeleted) &&
            Objects.equals(companyCodeId, that.companyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        listingId,
        scriptCode,
        exchange,
        status,
        statusDate,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        operation,
        ctlgSrno,
        jsonStoreId,
        isDataProcessed,
        ctlgIsDeleted,
        companyCodeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyListingStatusCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (listingId != null ? "listingId=" + listingId + ", " : "") +
                (scriptCode != null ? "scriptCode=" + scriptCode + ", " : "") +
                (exchange != null ? "exchange=" + exchange + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (statusDate != null ? "statusDate=" + statusDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (operation != null ? "operation=" + operation + ", " : "") +
                (ctlgSrno != null ? "ctlgSrno=" + ctlgSrno + ", " : "") +
                (jsonStoreId != null ? "jsonStoreId=" + jsonStoreId + ", " : "") +
                (isDataProcessed != null ? "isDataProcessed=" + isDataProcessed + ", " : "") +
                (ctlgIsDeleted != null ? "ctlgIsDeleted=" + ctlgIsDeleted + ", " : "") +
                (companyCodeId != null ? "companyCodeId=" + companyCodeId + ", " : "") +
            "}";
    }

}
