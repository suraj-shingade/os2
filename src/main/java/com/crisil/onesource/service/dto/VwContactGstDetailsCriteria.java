package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.VwContactGstDetails} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.VwContactGstDetailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /vw-contact-gst-details?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VwContactGstDetailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter companyRecordId;

    private StringFilter companyName;

    private StringFilter companyCode;

    private StringFilter gstNumber;

    private StringFilter isSezContactFlag;

    private StringFilter stateName;

    private StringFilter gstStateCode;

    private StringFilter contactId;

    public VwContactGstDetailsCriteria() {
    }

    public VwContactGstDetailsCriteria(VwContactGstDetailsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.companyRecordId = other.companyRecordId == null ? null : other.companyRecordId.copy();
        this.companyName = other.companyName == null ? null : other.companyName.copy();
        this.companyCode = other.companyCode == null ? null : other.companyCode.copy();
        this.gstNumber = other.gstNumber == null ? null : other.gstNumber.copy();
        this.isSezContactFlag = other.isSezContactFlag == null ? null : other.isSezContactFlag.copy();
        this.stateName = other.stateName == null ? null : other.stateName.copy();
        this.gstStateCode = other.gstStateCode == null ? null : other.gstStateCode.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public VwContactGstDetailsCriteria copy() {
        return new VwContactGstDetailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCompanyRecordId() {
        return companyRecordId;
    }

    public void setCompanyRecordId(StringFilter companyRecordId) {
        this.companyRecordId = companyRecordId;
    }

    public StringFilter getCompanyName() {
        return companyName;
    }

    public void setCompanyName(StringFilter companyName) {
        this.companyName = companyName;
    }

    public StringFilter getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(StringFilter companyCode) {
        this.companyCode = companyCode;
    }

    public StringFilter getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(StringFilter gstNumber) {
        this.gstNumber = gstNumber;
    }

    public StringFilter getIsSezContactFlag() {
        return isSezContactFlag;
    }

    public void setIsSezContactFlag(StringFilter isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
    }

    public StringFilter getStateName() {
        return stateName;
    }

    public void setStateName(StringFilter stateName) {
        this.stateName = stateName;
    }

    public StringFilter getGstStateCode() {
        return gstStateCode;
    }

    public void setGstStateCode(StringFilter gstStateCode) {
        this.gstStateCode = gstStateCode;
    }

    public StringFilter getContactId() {
        return contactId;
    }

    public void setContactId(StringFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final VwContactGstDetailsCriteria that = (VwContactGstDetailsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyRecordId, that.companyRecordId) &&
            Objects.equals(companyName, that.companyName) &&
            Objects.equals(companyCode, that.companyCode) &&
            Objects.equals(gstNumber, that.gstNumber) &&
            Objects.equals(isSezContactFlag, that.isSezContactFlag) &&
            Objects.equals(stateName, that.stateName) &&
            Objects.equals(gstStateCode, that.gstStateCode) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyRecordId,
        companyName,
        companyCode,
        gstNumber,
        isSezContactFlag,
        stateName,
        gstStateCode,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "VwContactGstDetailsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyRecordId != null ? "companyRecordId=" + companyRecordId + ", " : "") +
                (companyName != null ? "companyName=" + companyName + ", " : "") +
                (companyCode != null ? "companyCode=" + companyCode + ", " : "") +
                (gstNumber != null ? "gstNumber=" + gstNumber + ", " : "") +
                (isSezContactFlag != null ? "isSezContactFlag=" + isSezContactFlag + ", " : "") +
                (stateName != null ? "stateName=" + stateName + ", " : "") +
                (gstStateCode != null ? "gstStateCode=" + gstStateCode + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}
