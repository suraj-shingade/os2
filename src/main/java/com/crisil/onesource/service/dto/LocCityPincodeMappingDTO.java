package com.crisil.onesource.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.LocCityPincodeMapping} entity.
 */
public class LocCityPincodeMappingDTO implements Serializable {
    
    private Long id;

    @NotNull
    private Integer cityPinMappingId;

    @Size(max = 20)
    private String pincode;

    @Size(max = 1)
    private String isDeleted;

    private Instant createdDate;

    @Size(max = 100)
    private String createdBy;

    private Instant lastModifiedDate;

    @Size(max = 50)
    private String lastModifiedBy;


    private Long cityIdId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCityPinMappingId() {
        return cityPinMappingId;
    }

    public void setCityPinMappingId(Integer cityPinMappingId) {
        this.cityPinMappingId = cityPinMappingId;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Long getCityIdId() {
        return cityIdId;
    }

    public void setCityIdId(Long locCityMasterId) {
        this.cityIdId = locCityMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocCityPincodeMappingDTO)) {
            return false;
        }

        return id != null && id.equals(((LocCityPincodeMappingDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCityPincodeMappingDTO{" +
            "id=" + getId() +
            ", cityPinMappingId=" + getCityPinMappingId() +
            ", pincode='" + getPincode() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", cityIdId=" + getCityIdId() +
            "}";
    }
}
