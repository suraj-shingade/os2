package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.LocStateMaster} entity.
 */
@ApiModel(description = "Location_Master_Table")
public class LocStateMasterDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 100)
    private String stateName;

    @NotNull
    @Size(max = 250)
    private String description;

    @NotNull
    private Integer stateId;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 25)
    private String countryCode;

    @Size(max = 30)
    private String stateCode;

    @Size(max = 50)
    private String createdBy;

    private Instant createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private Instant lastModifiedDate;

    @Size(max = 1)
    private String isApproved;

    private Integer seqOrder;


    private Long regionIdId;

    private Long countryId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Long getRegionIdId() {
        return regionIdId;
    }

    public void setRegionIdId(Long locRegionMasterId) {
        this.regionIdId = locRegionMasterId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long locCountryMasterId) {
        this.countryId = locCountryMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocStateMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((LocStateMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocStateMasterDTO{" +
            "id=" + getId() +
            ", stateName='" + getStateName() + "'" +
            ", description='" + getDescription() + "'" +
            ", stateId=" + getStateId() +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            ", stateCode='" + getStateCode() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            ", regionIdId=" + getRegionIdId() +
            ", countryId=" + getCountryId() +
            "}";
    }
}
