package com.crisil.onesource.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.KarzaCompInformation} entity.
 */
public class KarzaCompInformationDTO implements Serializable {
    
    private Long id;

    @Size(max = 15)
    private String pan;

    @Size(max = 100)
    private String companyNameKarza;

    @Size(max = 500)
    private String address;

    @Size(max = 100)
    private String typeofapproval;

    @Size(max = 100)
    private String status;

    @Size(max = 100)
    private String gstin;

    private LocalDate createdDate;

    @Size(max = 100)
    private String createdBy;

    @Size(max = 100)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 50)
    private String state;

    @Size(max = 1)
    private String isValidGst;

    @Size(max = 20)
    private String source;

    @Size(max = 500)
    private String comments;

    @Size(max = 100)
    private String checkdigit;

    @Size(max = 100)
    private String taxpayerName;

    @Size(max = 200)
    private String natuteOfBuss;

    @Size(max = 15)
    private String mobileNo;

    @Size(max = 50)
    private String email;

    @Size(max = 100)
    private String tradeName;

    @Size(max = 50)
    private String constitBusiness;


    private Long companyCodeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCompanyNameKarza() {
        return companyNameKarza;
    }

    public void setCompanyNameKarza(String companyNameKarza) {
        this.companyNameKarza = companyNameKarza;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeofapproval() {
        return typeofapproval;
    }

    public void setTypeofapproval(String typeofapproval) {
        this.typeofapproval = typeofapproval;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGstin() {
        return gstin;
    }

    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsValidGst() {
        return isValidGst;
    }

    public void setIsValidGst(String isValidGst) {
        this.isValidGst = isValidGst;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCheckdigit() {
        return checkdigit;
    }

    public void setCheckdigit(String checkdigit) {
        this.checkdigit = checkdigit;
    }

    public String getTaxpayerName() {
        return taxpayerName;
    }

    public void setTaxpayerName(String taxpayerName) {
        this.taxpayerName = taxpayerName;
    }

    public String getNatuteOfBuss() {
        return natuteOfBuss;
    }

    public void setNatuteOfBuss(String natuteOfBuss) {
        this.natuteOfBuss = natuteOfBuss;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getConstitBusiness() {
        return constitBusiness;
    }

    public void setConstitBusiness(String constitBusiness) {
        this.constitBusiness = constitBusiness;
    }

    public Long getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(Long onesourceCompanyMasterId) {
        this.companyCodeId = onesourceCompanyMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KarzaCompInformationDTO)) {
            return false;
        }

        return id != null && id.equals(((KarzaCompInformationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "KarzaCompInformationDTO{" +
            "id=" + getId() +
            ", pan='" + getPan() + "'" +
            ", companyNameKarza='" + getCompanyNameKarza() + "'" +
            ", address='" + getAddress() + "'" +
            ", typeofapproval='" + getTypeofapproval() + "'" +
            ", status='" + getStatus() + "'" +
            ", gstin='" + getGstin() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", state='" + getState() + "'" +
            ", isValidGst='" + getIsValidGst() + "'" +
            ", source='" + getSource() + "'" +
            ", comments='" + getComments() + "'" +
            ", checkdigit='" + getCheckdigit() + "'" +
            ", taxpayerName='" + getTaxpayerName() + "'" +
            ", natuteOfBuss='" + getNatuteOfBuss() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            ", email='" + getEmail() + "'" +
            ", tradeName='" + getTradeName() + "'" +
            ", constitBusiness='" + getConstitBusiness() + "'" +
            ", companyCodeId=" + getCompanyCodeId() +
            "}";
    }
}
