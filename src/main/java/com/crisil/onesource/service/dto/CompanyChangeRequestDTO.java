package com.crisil.onesource.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompanyChangeRequest} entity.
 */
public class CompanyChangeRequestDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 200)
    private String oldCompanyName;

    @NotNull
    @Size(max = 200)
    private String newCompanyName;

    @NotNull
    @Size(max = 50)
    private String newCompanyCode;

    @NotNull
    @Size(max = 80)
    private String status;

    @NotNull
    private LocalDate effectiveDate;

    @NotNull
    @Size(max = 80)
    private String requestedBy;

    @Size(max = 80)
    private String companyChangeId;

    @NotNull
    @Size(max = 80)
    private String caseType;

    @Size(max = 500)
    private String reason;

    @Size(max = 500)
    private String reasonDescription;

    @Size(max = 50)
    private String createdBy;

    private LocalDate createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;


    private Long oldCompanyCodeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOldCompanyName() {
        return oldCompanyName;
    }

    public void setOldCompanyName(String oldCompanyName) {
        this.oldCompanyName = oldCompanyName;
    }

    public String getNewCompanyName() {
        return newCompanyName;
    }

    public void setNewCompanyName(String newCompanyName) {
        this.newCompanyName = newCompanyName;
    }

    public String getNewCompanyCode() {
        return newCompanyCode;
    }

    public void setNewCompanyCode(String newCompanyCode) {
        this.newCompanyCode = newCompanyCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getCompanyChangeId() {
        return companyChangeId;
    }

    public void setCompanyChangeId(String companyChangeId) {
        this.companyChangeId = companyChangeId;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getOldCompanyCodeId() {
        return oldCompanyCodeId;
    }

    public void setOldCompanyCodeId(Long onesourceCompanyMasterId) {
        this.oldCompanyCodeId = onesourceCompanyMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyChangeRequestDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyChangeRequestDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyChangeRequestDTO{" +
            "id=" + getId() +
            ", oldCompanyName='" + getOldCompanyName() + "'" +
            ", newCompanyName='" + getNewCompanyName() + "'" +
            ", newCompanyCode='" + getNewCompanyCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", effectiveDate='" + getEffectiveDate() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", companyChangeId='" + getCompanyChangeId() + "'" +
            ", caseType='" + getCaseType() + "'" +
            ", reason='" + getReason() + "'" +
            ", reasonDescription='" + getReasonDescription() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", oldCompanyCodeId=" + getOldCompanyCodeId() +
            "}";
    }
}
