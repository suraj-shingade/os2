package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyRequest} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyRequestResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-requests?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyRequestCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter companyName;

    private IntegerFilter companyTypeId;

    private IntegerFilter sectorId;

    private IntegerFilter industryId;

    private IntegerFilter businessAreaId;

    private StringFilter clientGroupName;

    private StringFilter remarks;

    private StringFilter recordId;

    private StringFilter companyCode;

    private StringFilter status;

    private StringFilter reason;

    private StringFilter isDeleted;

    private StringFilter billingType;

    private StringFilter displayCompanyName;

    private StringFilter requestedBy;

    private StringFilter approvedBy;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private InstantFilter requestedDate;

    private InstantFilter approvedDate;

    private StringFilter lastModifiedBy;

    private StringFilter originalFilename;

    private StringFilter displayFilename;

    private LongFilter companyAddressMasterId;

    private LongFilter companyTanPanRocDtlsId;

    public CompanyRequestCriteria() {
    }

    public CompanyRequestCriteria(CompanyRequestCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.companyName = other.companyName == null ? null : other.companyName.copy();
        this.companyTypeId = other.companyTypeId == null ? null : other.companyTypeId.copy();
        this.sectorId = other.sectorId == null ? null : other.sectorId.copy();
        this.industryId = other.industryId == null ? null : other.industryId.copy();
        this.businessAreaId = other.businessAreaId == null ? null : other.businessAreaId.copy();
        this.clientGroupName = other.clientGroupName == null ? null : other.clientGroupName.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
        this.companyCode = other.companyCode == null ? null : other.companyCode.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.reason = other.reason == null ? null : other.reason.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.billingType = other.billingType == null ? null : other.billingType.copy();
        this.displayCompanyName = other.displayCompanyName == null ? null : other.displayCompanyName.copy();
        this.requestedBy = other.requestedBy == null ? null : other.requestedBy.copy();
        this.approvedBy = other.approvedBy == null ? null : other.approvedBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.requestedDate = other.requestedDate == null ? null : other.requestedDate.copy();
        this.approvedDate = other.approvedDate == null ? null : other.approvedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.originalFilename = other.originalFilename == null ? null : other.originalFilename.copy();
        this.displayFilename = other.displayFilename == null ? null : other.displayFilename.copy();
        this.companyAddressMasterId = other.companyAddressMasterId == null ? null : other.companyAddressMasterId.copy();
        this.companyTanPanRocDtlsId = other.companyTanPanRocDtlsId == null ? null : other.companyTanPanRocDtlsId.copy();
    }

    @Override
    public CompanyRequestCriteria copy() {
        return new CompanyRequestCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCompanyName() {
        return companyName;
    }

    public void setCompanyName(StringFilter companyName) {
        this.companyName = companyName;
    }

    public IntegerFilter getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(IntegerFilter companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public IntegerFilter getSectorId() {
        return sectorId;
    }

    public void setSectorId(IntegerFilter sectorId) {
        this.sectorId = sectorId;
    }

    public IntegerFilter getIndustryId() {
        return industryId;
    }

    public void setIndustryId(IntegerFilter industryId) {
        this.industryId = industryId;
    }

    public IntegerFilter getBusinessAreaId() {
        return businessAreaId;
    }

    public void setBusinessAreaId(IntegerFilter businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public StringFilter getClientGroupName() {
        return clientGroupName;
    }

    public void setClientGroupName(StringFilter clientGroupName) {
        this.clientGroupName = clientGroupName;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public StringFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(StringFilter recordId) {
        this.recordId = recordId;
    }

    public StringFilter getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(StringFilter companyCode) {
        this.companyCode = companyCode;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getReason() {
        return reason;
    }

    public void setReason(StringFilter reason) {
        this.reason = reason;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getBillingType() {
        return billingType;
    }

    public void setBillingType(StringFilter billingType) {
        this.billingType = billingType;
    }

    public StringFilter getDisplayCompanyName() {
        return displayCompanyName;
    }

    public void setDisplayCompanyName(StringFilter displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public StringFilter getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(StringFilter requestedBy) {
        this.requestedBy = requestedBy;
    }

    public StringFilter getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(StringFilter approvedBy) {
        this.approvedBy = approvedBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public InstantFilter getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(InstantFilter requestedDate) {
        this.requestedDate = requestedDate;
    }

    public InstantFilter getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(InstantFilter approvedDate) {
        this.approvedDate = approvedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(StringFilter originalFilename) {
        this.originalFilename = originalFilename;
    }

    public StringFilter getDisplayFilename() {
        return displayFilename;
    }

    public void setDisplayFilename(StringFilter displayFilename) {
        this.displayFilename = displayFilename;
    }

    public LongFilter getCompanyAddressMasterId() {
        return companyAddressMasterId;
    }

    public void setCompanyAddressMasterId(LongFilter companyAddressMasterId) {
        this.companyAddressMasterId = companyAddressMasterId;
    }

    public LongFilter getCompanyTanPanRocDtlsId() {
        return companyTanPanRocDtlsId;
    }

    public void setCompanyTanPanRocDtlsId(LongFilter companyTanPanRocDtlsId) {
        this.companyTanPanRocDtlsId = companyTanPanRocDtlsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyRequestCriteria that = (CompanyRequestCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyName, that.companyName) &&
            Objects.equals(companyTypeId, that.companyTypeId) &&
            Objects.equals(sectorId, that.sectorId) &&
            Objects.equals(industryId, that.industryId) &&
            Objects.equals(businessAreaId, that.businessAreaId) &&
            Objects.equals(clientGroupName, that.clientGroupName) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(recordId, that.recordId) &&
            Objects.equals(companyCode, that.companyCode) &&
            Objects.equals(status, that.status) &&
            Objects.equals(reason, that.reason) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(billingType, that.billingType) &&
            Objects.equals(displayCompanyName, that.displayCompanyName) &&
            Objects.equals(requestedBy, that.requestedBy) &&
            Objects.equals(approvedBy, that.approvedBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(requestedDate, that.requestedDate) &&
            Objects.equals(approvedDate, that.approvedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(originalFilename, that.originalFilename) &&
            Objects.equals(displayFilename, that.displayFilename) &&
            Objects.equals(companyAddressMasterId, that.companyAddressMasterId) &&
            Objects.equals(companyTanPanRocDtlsId, that.companyTanPanRocDtlsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyName,
        companyTypeId,
        sectorId,
        industryId,
        businessAreaId,
        clientGroupName,
        remarks,
        recordId,
        companyCode,
        status,
        reason,
        isDeleted,
        billingType,
        displayCompanyName,
        requestedBy,
        approvedBy,
        createdDate,
        createdBy,
        lastModifiedDate,
        requestedDate,
        approvedDate,
        lastModifiedBy,
        originalFilename,
        displayFilename,
        companyAddressMasterId,
        companyTanPanRocDtlsId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyRequestCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyName != null ? "companyName=" + companyName + ", " : "") +
                (companyTypeId != null ? "companyTypeId=" + companyTypeId + ", " : "") +
                (sectorId != null ? "sectorId=" + sectorId + ", " : "") +
                (industryId != null ? "industryId=" + industryId + ", " : "") +
                (businessAreaId != null ? "businessAreaId=" + businessAreaId + ", " : "") +
                (clientGroupName != null ? "clientGroupName=" + clientGroupName + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
                (companyCode != null ? "companyCode=" + companyCode + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (reason != null ? "reason=" + reason + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (billingType != null ? "billingType=" + billingType + ", " : "") +
                (displayCompanyName != null ? "displayCompanyName=" + displayCompanyName + ", " : "") +
                (requestedBy != null ? "requestedBy=" + requestedBy + ", " : "") +
                (approvedBy != null ? "approvedBy=" + approvedBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (requestedDate != null ? "requestedDate=" + requestedDate + ", " : "") +
                (approvedDate != null ? "approvedDate=" + approvedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (originalFilename != null ? "originalFilename=" + originalFilename + ", " : "") +
                (displayFilename != null ? "displayFilename=" + displayFilename + ", " : "") +
                (companyAddressMasterId != null ? "companyAddressMasterId=" + companyAddressMasterId + ", " : "") +
                (companyTanPanRocDtlsId != null ? "companyTanPanRocDtlsId=" + companyTanPanRocDtlsId + ", " : "") +
            "}";
    }

}
