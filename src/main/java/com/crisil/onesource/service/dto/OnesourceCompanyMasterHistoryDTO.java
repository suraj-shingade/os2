package com.crisil.onesource.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.OnesourceCompanyMasterHistory} entity.
 */
public class OnesourceCompanyMasterHistoryDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 50)
    private String companyCode;

    @Size(max = 100)
    private String companyName;

    private Integer companyTypeId;

    private Integer sectorId;

    private Integer industryId;

    private Integer groupId;

    @Size(max = 100)
    private String remarks;

    @Size(max = 100)
    private String recordId;

    private Integer status;

    @Size(max = 50)
    private String pin;

    @Size(max = 50)
    private String clientStatus;

    @Size(max = 100)
    private String displayCompanyName;

    @Size(max = 1)
    private String billingType;

    @Size(max = 1)
    private String isBillable;

    @Size(max = 1)
    private String listingStatus;

    private Instant dateOfIncooperation;

    private Instant createdDate;

    private Instant lastModifiedDate;

    @Size(max = 100)
    private String createdBy;

    @Size(max = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 50)
    private String originalFilename;

    @Size(max = 500)
    private String displayFilename;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Integer getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(String clientStatus) {
        this.clientStatus = clientStatus;
    }

    public String getDisplayCompanyName() {
        return displayCompanyName;
    }

    public void setDisplayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getIsBillable() {
        return isBillable;
    }

    public void setIsBillable(String isBillable) {
        this.isBillable = isBillable;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public Instant getDateOfIncooperation() {
        return dateOfIncooperation;
    }

    public void setDateOfIncooperation(Instant dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public void setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnesourceCompanyMasterHistoryDTO)) {
            return false;
        }

        return id != null && id.equals(((OnesourceCompanyMasterHistoryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OnesourceCompanyMasterHistoryDTO{" +
            "id=" + getId() +
            ", companyCode='" + getCompanyCode() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", companyTypeId=" + getCompanyTypeId() +
            ", sectorId=" + getSectorId() +
            ", industryId=" + getIndustryId() +
            ", groupId=" + getGroupId() +
            ", remarks='" + getRemarks() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", status=" + getStatus() +
            ", pin='" + getPin() + "'" +
            ", clientStatus='" + getClientStatus() + "'" +
            ", displayCompanyName='" + getDisplayCompanyName() + "'" +
            ", billingType='" + getBillingType() + "'" +
            ", isBillable='" + getIsBillable() + "'" +
            ", listingStatus='" + getListingStatus() + "'" +
            ", dateOfIncooperation='" + getDateOfIncooperation() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", originalFilename='" + getOriginalFilename() + "'" +
            ", displayFilename='" + getDisplayFilename() + "'" +
            "}";
    }
}
