package com.crisil.onesource.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.ContactMaster} entity.
 */
public class ContactMasterDTO implements Serializable {
    
    private Long id;

    @Size(max = 75)
    private String firstName;

    @NotNull
    @Size(max = 75)
    private String lastName;

    @Size(max = 75)
    private String middleName;

    @NotNull
    @Size(max = 15)
    private String salutation;

    @NotNull
    @Size(max = 500)
    private String address1;

    @Size(max = 500)
    private String address2;

    @Size(max = 500)
    private String address3;

    @NotNull
    @Size(max = 200)
    private String designation;

    @Size(max = 200)
    private String department;

    @Size(max = 100)
    private String city;

    @Size(max = 100)
    private String state;

    @Size(max = 100)
    private String country;

    @Size(max = 50)
    private String pin;

    @Size(max = 200)
    private String phoneNum;

    @Size(max = 200)
    private String faxNum;

    @Size(max = 200)
    private String email;

    @Size(max = 200)
    private String mobileNum;

    @NotNull
    @Size(max = 25)
    private String recordId;

    @Size(max = 100)
    private String comments;

    @Size(max = 25)
    private String keyPerson;

    @NotNull
    @Size(max = 150)
    private String contactId;

    @Size(max = 10)
    private String levelImportance;

    @Size(max = 50)
    private String status;

    @Size(max = 100)
    private String removeReason;

    @Size(max = 15)
    private String cityId;

    @Size(max = 3)
    private String oFlag;

    @Size(max = 20)
    private String optOut;

    @Size(max = 150)
    private String ofaContactId;

    private LocalDate optOutDate;

    @Size(max = 10)
    private String faxOptout;

    @Size(max = 10)
    private String donotcall;

    @Size(max = 10)
    private String dnsendDirectMail;

    @Size(max = 10)
    private String dnshareOutsideMhp;

    @Size(max = 10)
    private String outsideOfContact;

    @Size(max = 10)
    private String dnshareOutsideSnp;

    @Size(max = 10)
    private String emailOptout;

    @Size(max = 1)
    private String smsOptout;

    @Size(max = 1)
    private String isSezContactFlag;

    @Size(max = 10)
    private String isExemptContactFlag;

    @Size(max = 1)
    private String gstNotApplicable;

    @Size(max = 1)
    private String billingContact;

    @Size(max = 25)
    private String gstPartyType;

    @Size(max = 20)
    private String tan;

    @Size(max = 25)
    private String gstNumber;

    @Size(max = 25)
    private String tdsNumber;

    @Size(max = 50)
    private String createdBy;

    private LocalDate createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;


    private Long companyCodeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getFaxNum() {
        return faxNum;
    }

    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getKeyPerson() {
        return keyPerson;
    }

    public void setKeyPerson(String keyPerson) {
        this.keyPerson = keyPerson;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getLevelImportance() {
        return levelImportance;
    }

    public void setLevelImportance(String levelImportance) {
        this.levelImportance = levelImportance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemoveReason() {
        return removeReason;
    }

    public void setRemoveReason(String removeReason) {
        this.removeReason = removeReason;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getoFlag() {
        return oFlag;
    }

    public void setoFlag(String oFlag) {
        this.oFlag = oFlag;
    }

    public String getOptOut() {
        return optOut;
    }

    public void setOptOut(String optOut) {
        this.optOut = optOut;
    }

    public String getOfaContactId() {
        return ofaContactId;
    }

    public void setOfaContactId(String ofaContactId) {
        this.ofaContactId = ofaContactId;
    }

    public LocalDate getOptOutDate() {
        return optOutDate;
    }

    public void setOptOutDate(LocalDate optOutDate) {
        this.optOutDate = optOutDate;
    }

    public String getFaxOptout() {
        return faxOptout;
    }

    public void setFaxOptout(String faxOptout) {
        this.faxOptout = faxOptout;
    }

    public String getDonotcall() {
        return donotcall;
    }

    public void setDonotcall(String donotcall) {
        this.donotcall = donotcall;
    }

    public String getDnsendDirectMail() {
        return dnsendDirectMail;
    }

    public void setDnsendDirectMail(String dnsendDirectMail) {
        this.dnsendDirectMail = dnsendDirectMail;
    }

    public String getDnshareOutsideMhp() {
        return dnshareOutsideMhp;
    }

    public void setDnshareOutsideMhp(String dnshareOutsideMhp) {
        this.dnshareOutsideMhp = dnshareOutsideMhp;
    }

    public String getOutsideOfContact() {
        return outsideOfContact;
    }

    public void setOutsideOfContact(String outsideOfContact) {
        this.outsideOfContact = outsideOfContact;
    }

    public String getDnshareOutsideSnp() {
        return dnshareOutsideSnp;
    }

    public void setDnshareOutsideSnp(String dnshareOutsideSnp) {
        this.dnshareOutsideSnp = dnshareOutsideSnp;
    }

    public String getEmailOptout() {
        return emailOptout;
    }

    public void setEmailOptout(String emailOptout) {
        this.emailOptout = emailOptout;
    }

    public String getSmsOptout() {
        return smsOptout;
    }

    public void setSmsOptout(String smsOptout) {
        this.smsOptout = smsOptout;
    }

    public String getIsSezContactFlag() {
        return isSezContactFlag;
    }

    public void setIsSezContactFlag(String isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
    }

    public String getIsExemptContactFlag() {
        return isExemptContactFlag;
    }

    public void setIsExemptContactFlag(String isExemptContactFlag) {
        this.isExemptContactFlag = isExemptContactFlag;
    }

    public String getGstNotApplicable() {
        return gstNotApplicable;
    }

    public void setGstNotApplicable(String gstNotApplicable) {
        this.gstNotApplicable = gstNotApplicable;
    }

    public String getBillingContact() {
        return billingContact;
    }

    public void setBillingContact(String billingContact) {
        this.billingContact = billingContact;
    }

    public String getGstPartyType() {
        return gstPartyType;
    }

    public void setGstPartyType(String gstPartyType) {
        this.gstPartyType = gstPartyType;
    }

    public String getTan() {
        return tan;
    }

    public void setTan(String tan) {
        this.tan = tan;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getTdsNumber() {
        return tdsNumber;
    }

    public void setTdsNumber(String tdsNumber) {
        this.tdsNumber = tdsNumber;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(Long onesourceCompanyMasterId) {
        this.companyCodeId = onesourceCompanyMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactMasterDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", salutation='" + getSalutation() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", designation='" + getDesignation() + "'" +
            ", department='" + getDepartment() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", pin='" + getPin() + "'" +
            ", phoneNum='" + getPhoneNum() + "'" +
            ", faxNum='" + getFaxNum() + "'" +
            ", email='" + getEmail() + "'" +
            ", mobileNum='" + getMobileNum() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", comments='" + getComments() + "'" +
            ", keyPerson='" + getKeyPerson() + "'" +
            ", contactId='" + getContactId() + "'" +
            ", levelImportance='" + getLevelImportance() + "'" +
            ", status='" + getStatus() + "'" +
            ", removeReason='" + getRemoveReason() + "'" +
            ", cityId='" + getCityId() + "'" +
            ", oFlag='" + getoFlag() + "'" +
            ", optOut='" + getOptOut() + "'" +
            ", ofaContactId='" + getOfaContactId() + "'" +
            ", optOutDate='" + getOptOutDate() + "'" +
            ", faxOptout='" + getFaxOptout() + "'" +
            ", donotcall='" + getDonotcall() + "'" +
            ", dnsendDirectMail='" + getDnsendDirectMail() + "'" +
            ", dnshareOutsideMhp='" + getDnshareOutsideMhp() + "'" +
            ", outsideOfContact='" + getOutsideOfContact() + "'" +
            ", dnshareOutsideSnp='" + getDnshareOutsideSnp() + "'" +
            ", emailOptout='" + getEmailOptout() + "'" +
            ", smsOptout='" + getSmsOptout() + "'" +
            ", isSezContactFlag='" + getIsSezContactFlag() + "'" +
            ", isExemptContactFlag='" + getIsExemptContactFlag() + "'" +
            ", gstNotApplicable='" + getGstNotApplicable() + "'" +
            ", billingContact='" + getBillingContact() + "'" +
            ", gstPartyType='" + getGstPartyType() + "'" +
            ", tan='" + getTan() + "'" +
            ", gstNumber='" + getGstNumber() + "'" +
            ", tdsNumber='" + getTdsNumber() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", companyCodeId=" + getCompanyCodeId() +
            "}";
    }
}
