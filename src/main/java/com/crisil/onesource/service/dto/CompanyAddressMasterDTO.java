package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompanyAddressMaster} entity.
 */
@ApiModel(description = "Company_Master_Table")
public class CompanyAddressMasterDTO implements Serializable {
    
    private Long id;

    @Size(max = 50)
    private String companyCode;

    @NotNull
    private Integer addressTypeId;

    @Size(max = 1000)
    private String address1;

    @Size(max = 1000)
    private String address2;

    @Size(max = 1000)
    private String address3;

    private Integer cityId;

    @Size(max = 150)
    private String phoneNo;

    @Size(max = 100)
    private String mobileNo;

    @Size(max = 100)
    private String fax;

    @Size(max = 300)
    private String email;

    @Size(max = 200)
    private String webpage;

    @Size(max = 50)
    private String createdBy;

    private LocalDate createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;

    @NotNull
    private Integer companyAddressId;

    @Size(max = 500)
    private String addressSource;


    private Long pincodeId;

    private Long recordId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Integer getAddressTypeId() {
        return addressTypeId;
    }

    public void setAddressTypeId(Integer addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getCompanyAddressId() {
        return companyAddressId;
    }

    public void setCompanyAddressId(Integer companyAddressId) {
        this.companyAddressId = companyAddressId;
    }

    public String getAddressSource() {
        return addressSource;
    }

    public void setAddressSource(String addressSource) {
        this.addressSource = addressSource;
    }

    public Long getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(Long locPincodeMasterId) {
        this.pincodeId = locPincodeMasterId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long companyRequestId) {
        this.recordId = companyRequestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyAddressMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyAddressMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyAddressMasterDTO{" +
            "id=" + getId() +
            ", companyCode='" + getCompanyCode() + "'" +
            ", addressTypeId=" + getAddressTypeId() +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", cityId=" + getCityId() +
            ", phoneNo='" + getPhoneNo() + "'" +
            ", mobileNo='" + getMobileNo() + "'" +
            ", fax='" + getFax() + "'" +
            ", email='" + getEmail() + "'" +
            ", webpage='" + getWebpage() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", companyAddressId=" + getCompanyAddressId() +
            ", addressSource='" + getAddressSource() + "'" +
            ", pincodeId=" + getPincodeId() +
            ", recordId=" + getRecordId() +
            "}";
    }
}
