package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocRegionMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocRegionMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-region-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocRegionMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter regionId;

    private StringFilter regionName;

    private StringFilter description;

    private StringFilter isDeleted;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private StringFilter isApproved;

    private IntegerFilter seqOrder;

    private LongFilter locStateMasterId;

    public LocRegionMasterCriteria() {
    }

    public LocRegionMasterCriteria(LocRegionMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.regionId = other.regionId == null ? null : other.regionId.copy();
        this.regionName = other.regionName == null ? null : other.regionName.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isApproved = other.isApproved == null ? null : other.isApproved.copy();
        this.seqOrder = other.seqOrder == null ? null : other.seqOrder.copy();
        this.locStateMasterId = other.locStateMasterId == null ? null : other.locStateMasterId.copy();
    }

    @Override
    public LocRegionMasterCriteria copy() {
        return new LocRegionMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRegionId() {
        return regionId;
    }

    public void setRegionId(IntegerFilter regionId) {
        this.regionId = regionId;
    }

    public StringFilter getRegionName() {
        return regionName;
    }

    public void setRegionName(StringFilter regionName) {
        this.regionName = regionName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(StringFilter isApproved) {
        this.isApproved = isApproved;
    }

    public IntegerFilter getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(IntegerFilter seqOrder) {
        this.seqOrder = seqOrder;
    }

    public LongFilter getLocStateMasterId() {
        return locStateMasterId;
    }

    public void setLocStateMasterId(LongFilter locStateMasterId) {
        this.locStateMasterId = locStateMasterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocRegionMasterCriteria that = (LocRegionMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(regionId, that.regionId) &&
            Objects.equals(regionName, that.regionName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isApproved, that.isApproved) &&
            Objects.equals(seqOrder, that.seqOrder) &&
            Objects.equals(locStateMasterId, that.locStateMasterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        regionId,
        regionName,
        description,
        isDeleted,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isApproved,
        seqOrder,
        locStateMasterId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocRegionMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (regionId != null ? "regionId=" + regionId + ", " : "") +
                (regionName != null ? "regionName=" + regionName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isApproved != null ? "isApproved=" + isApproved + ", " : "") +
                (seqOrder != null ? "seqOrder=" + seqOrder + ", " : "") +
                (locStateMasterId != null ? "locStateMasterId=" + locStateMasterId + ", " : "") +
            "}";
    }

}
