package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanySegmentMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanySegmentMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-segment-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanySegmentMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter segmentId;

    private StringFilter segmentName;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private StringFilter isDeleted;

    public CompanySegmentMasterCriteria() {
    }

    public CompanySegmentMasterCriteria(CompanySegmentMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.segmentId = other.segmentId == null ? null : other.segmentId.copy();
        this.segmentName = other.segmentName == null ? null : other.segmentName.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
    }

    @Override
    public CompanySegmentMasterCriteria copy() {
        return new CompanySegmentMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(IntegerFilter segmentId) {
        this.segmentId = segmentId;
    }

    public StringFilter getSegmentName() {
        return segmentName;
    }

    public void setSegmentName(StringFilter segmentName) {
        this.segmentName = segmentName;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanySegmentMasterCriteria that = (CompanySegmentMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(segmentId, that.segmentId) &&
            Objects.equals(segmentName, that.segmentName) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(isDeleted, that.isDeleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        segmentId,
        segmentName,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        isDeleted
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanySegmentMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (segmentId != null ? "segmentId=" + segmentId + ", " : "") +
                (segmentName != null ? "segmentName=" + segmentName + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
            "}";
    }

}
