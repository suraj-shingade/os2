package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.OnesourceCompanyMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.OnesourceCompanyMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /onesource-company-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OnesourceCompanyMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter companyCode;

    private StringFilter companyName;

    private StringFilter remarks;

    private StringFilter recordId;

    private IntegerFilter status;

    private StringFilter clientStatus;

    private StringFilter displayCompanyName;

    private StringFilter billingType;

    private StringFilter isBillable;

    private StringFilter listingStatus;

    private InstantFilter dateOfIncooperation;

    private InstantFilter createdDate;

    private InstantFilter lastModifiedDate;

    private StringFilter createdBy;

    private StringFilter lastModifiedBy;

    private StringFilter isDeleted;

    private StringFilter originalFilename;

    private StringFilter displayFilename;

    private StringFilter companyStatus;

    private LongFilter companyChangeRequestId;

    private LongFilter companyIsinMappingId;

    private LongFilter companyListingStatusId;

    private LongFilter contactMasterId;

    private LongFilter karzaCompInformationId;

    private LongFilter companyTypeIdId;

    private LongFilter sectorIdId;

    private LongFilter industryIdId;

    private LongFilter groupIdId;

    private LongFilter pinId;

    public OnesourceCompanyMasterCriteria() {
    }

    public OnesourceCompanyMasterCriteria(OnesourceCompanyMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.companyCode = other.companyCode == null ? null : other.companyCode.copy();
        this.companyName = other.companyName == null ? null : other.companyName.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.clientStatus = other.clientStatus == null ? null : other.clientStatus.copy();
        this.displayCompanyName = other.displayCompanyName == null ? null : other.displayCompanyName.copy();
        this.billingType = other.billingType == null ? null : other.billingType.copy();
        this.isBillable = other.isBillable == null ? null : other.isBillable.copy();
        this.listingStatus = other.listingStatus == null ? null : other.listingStatus.copy();
        this.dateOfIncooperation = other.dateOfIncooperation == null ? null : other.dateOfIncooperation.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.originalFilename = other.originalFilename == null ? null : other.originalFilename.copy();
        this.displayFilename = other.displayFilename == null ? null : other.displayFilename.copy();
        this.companyStatus = other.companyStatus == null ? null : other.companyStatus.copy();
        this.companyChangeRequestId = other.companyChangeRequestId == null ? null : other.companyChangeRequestId.copy();
        this.companyIsinMappingId = other.companyIsinMappingId == null ? null : other.companyIsinMappingId.copy();
        this.companyListingStatusId = other.companyListingStatusId == null ? null : other.companyListingStatusId.copy();
        this.contactMasterId = other.contactMasterId == null ? null : other.contactMasterId.copy();
        this.karzaCompInformationId = other.karzaCompInformationId == null ? null : other.karzaCompInformationId.copy();
        this.companyTypeIdId = other.companyTypeIdId == null ? null : other.companyTypeIdId.copy();
        this.sectorIdId = other.sectorIdId == null ? null : other.sectorIdId.copy();
        this.industryIdId = other.industryIdId == null ? null : other.industryIdId.copy();
        this.groupIdId = other.groupIdId == null ? null : other.groupIdId.copy();
        this.pinId = other.pinId == null ? null : other.pinId.copy();
    }

    @Override
    public OnesourceCompanyMasterCriteria copy() {
        return new OnesourceCompanyMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(StringFilter companyCode) {
        this.companyCode = companyCode;
    }

    public StringFilter getCompanyName() {
        return companyName;
    }

    public void setCompanyName(StringFilter companyName) {
        this.companyName = companyName;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public StringFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(StringFilter recordId) {
        this.recordId = recordId;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public StringFilter getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(StringFilter clientStatus) {
        this.clientStatus = clientStatus;
    }

    public StringFilter getDisplayCompanyName() {
        return displayCompanyName;
    }

    public void setDisplayCompanyName(StringFilter displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public StringFilter getBillingType() {
        return billingType;
    }

    public void setBillingType(StringFilter billingType) {
        this.billingType = billingType;
    }

    public StringFilter getIsBillable() {
        return isBillable;
    }

    public void setIsBillable(StringFilter isBillable) {
        this.isBillable = isBillable;
    }

    public StringFilter getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(StringFilter listingStatus) {
        this.listingStatus = listingStatus;
    }

    public InstantFilter getDateOfIncooperation() {
        return dateOfIncooperation;
    }

    public void setDateOfIncooperation(InstantFilter dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(StringFilter originalFilename) {
        this.originalFilename = originalFilename;
    }

    public StringFilter getDisplayFilename() {
        return displayFilename;
    }

    public void setDisplayFilename(StringFilter displayFilename) {
        this.displayFilename = displayFilename;
    }

    public StringFilter getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(StringFilter companyStatus) {
        this.companyStatus = companyStatus;
    }

    public LongFilter getCompanyChangeRequestId() {
        return companyChangeRequestId;
    }

    public void setCompanyChangeRequestId(LongFilter companyChangeRequestId) {
        this.companyChangeRequestId = companyChangeRequestId;
    }

    public LongFilter getCompanyIsinMappingId() {
        return companyIsinMappingId;
    }

    public void setCompanyIsinMappingId(LongFilter companyIsinMappingId) {
        this.companyIsinMappingId = companyIsinMappingId;
    }

    public LongFilter getCompanyListingStatusId() {
        return companyListingStatusId;
    }

    public void setCompanyListingStatusId(LongFilter companyListingStatusId) {
        this.companyListingStatusId = companyListingStatusId;
    }

    public LongFilter getContactMasterId() {
        return contactMasterId;
    }

    public void setContactMasterId(LongFilter contactMasterId) {
        this.contactMasterId = contactMasterId;
    }

    public LongFilter getKarzaCompInformationId() {
        return karzaCompInformationId;
    }

    public void setKarzaCompInformationId(LongFilter karzaCompInformationId) {
        this.karzaCompInformationId = karzaCompInformationId;
    }

    public LongFilter getCompanyTypeIdId() {
        return companyTypeIdId;
    }

    public void setCompanyTypeIdId(LongFilter companyTypeIdId) {
        this.companyTypeIdId = companyTypeIdId;
    }

    public LongFilter getSectorIdId() {
        return sectorIdId;
    }

    public void setSectorIdId(LongFilter sectorIdId) {
        this.sectorIdId = sectorIdId;
    }

    public LongFilter getIndustryIdId() {
        return industryIdId;
    }

    public void setIndustryIdId(LongFilter industryIdId) {
        this.industryIdId = industryIdId;
    }

    public LongFilter getGroupIdId() {
        return groupIdId;
    }

    public void setGroupIdId(LongFilter groupIdId) {
        this.groupIdId = groupIdId;
    }

    public LongFilter getPinId() {
        return pinId;
    }

    public void setPinId(LongFilter pinId) {
        this.pinId = pinId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OnesourceCompanyMasterCriteria that = (OnesourceCompanyMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyCode, that.companyCode) &&
            Objects.equals(companyName, that.companyName) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(recordId, that.recordId) &&
            Objects.equals(status, that.status) &&
            Objects.equals(clientStatus, that.clientStatus) &&
            Objects.equals(displayCompanyName, that.displayCompanyName) &&
            Objects.equals(billingType, that.billingType) &&
            Objects.equals(isBillable, that.isBillable) &&
            Objects.equals(listingStatus, that.listingStatus) &&
            Objects.equals(dateOfIncooperation, that.dateOfIncooperation) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(originalFilename, that.originalFilename) &&
            Objects.equals(displayFilename, that.displayFilename) &&
            Objects.equals(companyStatus, that.companyStatus) &&
            Objects.equals(companyChangeRequestId, that.companyChangeRequestId) &&
            Objects.equals(companyIsinMappingId, that.companyIsinMappingId) &&
            Objects.equals(companyListingStatusId, that.companyListingStatusId) &&
            Objects.equals(contactMasterId, that.contactMasterId) &&
            Objects.equals(karzaCompInformationId, that.karzaCompInformationId) &&
            Objects.equals(companyTypeIdId, that.companyTypeIdId) &&
            Objects.equals(sectorIdId, that.sectorIdId) &&
            Objects.equals(industryIdId, that.industryIdId) &&
            Objects.equals(groupIdId, that.groupIdId) &&
            Objects.equals(pinId, that.pinId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyCode,
        companyName,
        remarks,
        recordId,
        status,
        clientStatus,
        displayCompanyName,
        billingType,
        isBillable,
        listingStatus,
        dateOfIncooperation,
        createdDate,
        lastModifiedDate,
        createdBy,
        lastModifiedBy,
        isDeleted,
        originalFilename,
        displayFilename,
        companyStatus,
        companyChangeRequestId,
        companyIsinMappingId,
        companyListingStatusId,
        contactMasterId,
        karzaCompInformationId,
        companyTypeIdId,
        sectorIdId,
        industryIdId,
        groupIdId,
        pinId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OnesourceCompanyMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyCode != null ? "companyCode=" + companyCode + ", " : "") +
                (companyName != null ? "companyName=" + companyName + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (clientStatus != null ? "clientStatus=" + clientStatus + ", " : "") +
                (displayCompanyName != null ? "displayCompanyName=" + displayCompanyName + ", " : "") +
                (billingType != null ? "billingType=" + billingType + ", " : "") +
                (isBillable != null ? "isBillable=" + isBillable + ", " : "") +
                (listingStatus != null ? "listingStatus=" + listingStatus + ", " : "") +
                (dateOfIncooperation != null ? "dateOfIncooperation=" + dateOfIncooperation + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (originalFilename != null ? "originalFilename=" + originalFilename + ", " : "") +
                (displayFilename != null ? "displayFilename=" + displayFilename + ", " : "") +
                (companyStatus != null ? "companyStatus=" + companyStatus + ", " : "") +
                (companyChangeRequestId != null ? "companyChangeRequestId=" + companyChangeRequestId + ", " : "") +
                (companyIsinMappingId != null ? "companyIsinMappingId=" + companyIsinMappingId + ", " : "") +
                (companyListingStatusId != null ? "companyListingStatusId=" + companyListingStatusId + ", " : "") +
                (contactMasterId != null ? "contactMasterId=" + contactMasterId + ", " : "") +
                (karzaCompInformationId != null ? "karzaCompInformationId=" + karzaCompInformationId + ", " : "") +
                (companyTypeIdId != null ? "companyTypeIdId=" + companyTypeIdId + ", " : "") +
                (sectorIdId != null ? "sectorIdId=" + sectorIdId + ", " : "") +
                (industryIdId != null ? "industryIdId=" + industryIdId + ", " : "") +
                (groupIdId != null ? "groupIdId=" + groupIdId + ", " : "") +
                (pinId != null ? "pinId=" + pinId + ", " : "") +
            "}";
    }

}
