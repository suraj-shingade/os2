package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocStateMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocStateMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-state-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocStateMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter stateName;

    private StringFilter description;

    private IntegerFilter stateId;

    private StringFilter isDeleted;

    private StringFilter countryCode;

    private StringFilter stateCode;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private StringFilter isApproved;

    private IntegerFilter seqOrder;

    private LongFilter locCityMasterId;

    private LongFilter locDistrictMasterId;

    private LongFilter locPincodeMasterId;

    private LongFilter regionIdId;

    private LongFilter countryId;

    public LocStateMasterCriteria() {
    }

    public LocStateMasterCriteria(LocStateMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.stateName = other.stateName == null ? null : other.stateName.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.stateId = other.stateId == null ? null : other.stateId.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.countryCode = other.countryCode == null ? null : other.countryCode.copy();
        this.stateCode = other.stateCode == null ? null : other.stateCode.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isApproved = other.isApproved == null ? null : other.isApproved.copy();
        this.seqOrder = other.seqOrder == null ? null : other.seqOrder.copy();
        this.locCityMasterId = other.locCityMasterId == null ? null : other.locCityMasterId.copy();
        this.locDistrictMasterId = other.locDistrictMasterId == null ? null : other.locDistrictMasterId.copy();
        this.locPincodeMasterId = other.locPincodeMasterId == null ? null : other.locPincodeMasterId.copy();
        this.regionIdId = other.regionIdId == null ? null : other.regionIdId.copy();
        this.countryId = other.countryId == null ? null : other.countryId.copy();
    }

    @Override
    public LocStateMasterCriteria copy() {
        return new LocStateMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getStateName() {
        return stateName;
    }

    public void setStateName(StringFilter stateName) {
        this.stateName = stateName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getStateId() {
        return stateId;
    }

    public void setStateId(IntegerFilter stateId) {
        this.stateId = stateId;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(StringFilter countryCode) {
        this.countryCode = countryCode;
    }

    public StringFilter getStateCode() {
        return stateCode;
    }

    public void setStateCode(StringFilter stateCode) {
        this.stateCode = stateCode;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(StringFilter isApproved) {
        this.isApproved = isApproved;
    }

    public IntegerFilter getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(IntegerFilter seqOrder) {
        this.seqOrder = seqOrder;
    }

    public LongFilter getLocCityMasterId() {
        return locCityMasterId;
    }

    public void setLocCityMasterId(LongFilter locCityMasterId) {
        this.locCityMasterId = locCityMasterId;
    }

    public LongFilter getLocDistrictMasterId() {
        return locDistrictMasterId;
    }

    public void setLocDistrictMasterId(LongFilter locDistrictMasterId) {
        this.locDistrictMasterId = locDistrictMasterId;
    }

    public LongFilter getLocPincodeMasterId() {
        return locPincodeMasterId;
    }

    public void setLocPincodeMasterId(LongFilter locPincodeMasterId) {
        this.locPincodeMasterId = locPincodeMasterId;
    }

    public LongFilter getRegionIdId() {
        return regionIdId;
    }

    public void setRegionIdId(LongFilter regionIdId) {
        this.regionIdId = regionIdId;
    }

    public LongFilter getCountryId() {
        return countryId;
    }

    public void setCountryId(LongFilter countryId) {
        this.countryId = countryId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocStateMasterCriteria that = (LocStateMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(stateName, that.stateName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(stateId, that.stateId) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(countryCode, that.countryCode) &&
            Objects.equals(stateCode, that.stateCode) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isApproved, that.isApproved) &&
            Objects.equals(seqOrder, that.seqOrder) &&
            Objects.equals(locCityMasterId, that.locCityMasterId) &&
            Objects.equals(locDistrictMasterId, that.locDistrictMasterId) &&
            Objects.equals(locPincodeMasterId, that.locPincodeMasterId) &&
            Objects.equals(regionIdId, that.regionIdId) &&
            Objects.equals(countryId, that.countryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        stateName,
        description,
        stateId,
        isDeleted,
        countryCode,
        stateCode,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isApproved,
        seqOrder,
        locCityMasterId,
        locDistrictMasterId,
        locPincodeMasterId,
        regionIdId,
        countryId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocStateMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (stateName != null ? "stateName=" + stateName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (stateId != null ? "stateId=" + stateId + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (countryCode != null ? "countryCode=" + countryCode + ", " : "") +
                (stateCode != null ? "stateCode=" + stateCode + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isApproved != null ? "isApproved=" + isApproved + ", " : "") +
                (seqOrder != null ? "seqOrder=" + seqOrder + ", " : "") +
                (locCityMasterId != null ? "locCityMasterId=" + locCityMasterId + ", " : "") +
                (locDistrictMasterId != null ? "locDistrictMasterId=" + locDistrictMasterId + ", " : "") +
                (locPincodeMasterId != null ? "locPincodeMasterId=" + locPincodeMasterId + ", " : "") +
                (regionIdId != null ? "regionIdId=" + regionIdId + ", " : "") +
                (countryId != null ? "countryId=" + countryId + ", " : "") +
            "}";
    }

}
