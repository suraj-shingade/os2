package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyTypeMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyTypeMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-type-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyTypeMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter companyTypeId;

    private StringFilter companyType;

    private StringFilter isDeleted;

    private InstantFilter lastModifiedDate;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private StringFilter lastModifiedBy;

    private LongFilter onesourceCompanyMasterId;

    public CompanyTypeMasterCriteria() {
    }

    public CompanyTypeMasterCriteria(CompanyTypeMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.companyTypeId = other.companyTypeId == null ? null : other.companyTypeId.copy();
        this.companyType = other.companyType == null ? null : other.companyType.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.onesourceCompanyMasterId = other.onesourceCompanyMasterId == null ? null : other.onesourceCompanyMasterId.copy();
    }

    @Override
    public CompanyTypeMasterCriteria copy() {
        return new CompanyTypeMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(IntegerFilter companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public StringFilter getCompanyType() {
        return companyType;
    }

    public void setCompanyType(StringFilter companyType) {
        this.companyType = companyType;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LongFilter getOnesourceCompanyMasterId() {
        return onesourceCompanyMasterId;
    }

    public void setOnesourceCompanyMasterId(LongFilter onesourceCompanyMasterId) {
        this.onesourceCompanyMasterId = onesourceCompanyMasterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyTypeMasterCriteria that = (CompanyTypeMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyTypeId, that.companyTypeId) &&
            Objects.equals(companyType, that.companyType) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(onesourceCompanyMasterId, that.onesourceCompanyMasterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyTypeId,
        companyType,
        isDeleted,
        lastModifiedDate,
        createdDate,
        createdBy,
        lastModifiedBy,
        onesourceCompanyMasterId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyTypeMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyTypeId != null ? "companyTypeId=" + companyTypeId + ", " : "") +
                (companyType != null ? "companyType=" + companyType + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (onesourceCompanyMasterId != null ? "onesourceCompanyMasterId=" + onesourceCompanyMasterId + ", " : "") +
            "}";
    }

}
