package com.crisil.onesource.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompGstNotApplicable} entity.
 */
public class CompGstNotApplicableDTO implements Serializable {
    
    private Long id;

    @Size(max = 20)
    private String requestStatus;

    @Size(max = 50)
    private String requesterComments;

    @Size(max = 50)
    private String requestedBy;

    private Instant requestedDate;

    @Size(max = 10)
    private String approverComments;

    @Size(max = 20)
    private String approvedBy;

    private Instant approvedDate;

    @Size(max = 50)
    private String gstNaFile;

    @Size(max = 1)
    private String isReverseRequest;

    private LocalDate createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;


    private Long contactIdId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequesterComments() {
        return requesterComments;
    }

    public void setRequesterComments(String requesterComments) {
        this.requesterComments = requesterComments;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public Instant getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Instant requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getApproverComments() {
        return approverComments;
    }

    public void setApproverComments(String approverComments) {
        this.approverComments = approverComments;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Instant getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Instant approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getGstNaFile() {
        return gstNaFile;
    }

    public void setGstNaFile(String gstNaFile) {
        this.gstNaFile = gstNaFile;
    }

    public String getIsReverseRequest() {
        return isReverseRequest;
    }

    public void setIsReverseRequest(String isReverseRequest) {
        this.isReverseRequest = isReverseRequest;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getContactIdId() {
        return contactIdId;
    }

    public void setContactIdId(Long contactMasterId) {
        this.contactIdId = contactMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompGstNotApplicableDTO)) {
            return false;
        }

        return id != null && id.equals(((CompGstNotApplicableDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompGstNotApplicableDTO{" +
            "id=" + getId() +
            ", requestStatus='" + getRequestStatus() + "'" +
            ", requesterComments='" + getRequesterComments() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", requestedDate='" + getRequestedDate() + "'" +
            ", approverComments='" + getApproverComments() + "'" +
            ", approvedBy='" + getApprovedBy() + "'" +
            ", approvedDate='" + getApprovedDate() + "'" +
            ", gstNaFile='" + getGstNaFile() + "'" +
            ", isReverseRequest='" + getIsReverseRequest() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", contactIdId=" + getContactIdId() +
            "}";
    }
}
