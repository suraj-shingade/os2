package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocCityPincodeMapping} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocCityPincodeMappingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-city-pincode-mappings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocCityPincodeMappingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter cityPinMappingId;

    private StringFilter pincode;

    private StringFilter isDeleted;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private LongFilter cityIdId;

    public LocCityPincodeMappingCriteria() {
    }

    public LocCityPincodeMappingCriteria(LocCityPincodeMappingCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cityPinMappingId = other.cityPinMappingId == null ? null : other.cityPinMappingId.copy();
        this.pincode = other.pincode == null ? null : other.pincode.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.cityIdId = other.cityIdId == null ? null : other.cityIdId.copy();
    }

    @Override
    public LocCityPincodeMappingCriteria copy() {
        return new LocCityPincodeMappingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCityPinMappingId() {
        return cityPinMappingId;
    }

    public void setCityPinMappingId(IntegerFilter cityPinMappingId) {
        this.cityPinMappingId = cityPinMappingId;
    }

    public StringFilter getPincode() {
        return pincode;
    }

    public void setPincode(StringFilter pincode) {
        this.pincode = pincode;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LongFilter getCityIdId() {
        return cityIdId;
    }

    public void setCityIdId(LongFilter cityIdId) {
        this.cityIdId = cityIdId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocCityPincodeMappingCriteria that = (LocCityPincodeMappingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cityPinMappingId, that.cityPinMappingId) &&
            Objects.equals(pincode, that.pincode) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(cityIdId, that.cityIdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cityPinMappingId,
        pincode,
        isDeleted,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        cityIdId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCityPincodeMappingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cityPinMappingId != null ? "cityPinMappingId=" + cityPinMappingId + ", " : "") +
                (pincode != null ? "pincode=" + pincode + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (cityIdId != null ? "cityIdId=" + cityIdId + ", " : "") +
            "}";
    }

}
