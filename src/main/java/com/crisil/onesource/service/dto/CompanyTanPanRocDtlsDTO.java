package com.crisil.onesource.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompanyTanPanRocDtls} entity.
 */
public class CompanyTanPanRocDtlsDTO implements Serializable {
    
    private Long id;

    @Size(max = 20)
    private String tan;

    @Size(max = 20)
    private String pan;

    @Size(max = 25)
    private String roc;

    private LocalDate createdDate;

    @Size(max = 150)
    private String createdBy;

    private LocalDate lastModifiedDate;

    @Size(max = 150)
    private String lastModifiedBy;

    private Integer companyRiskStatusId;

    @Size(max = 500)
    private String companyRistComments;

    @Size(max = 4000)
    private String companyBackground;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 20)
    private String rptFlag;

    @Size(max = 1)
    private String gstExempt;

    @Size(max = 1)
    private String gstNumberNotApplicable;

    @Size(max = 50)
    private String companyCode;


    private Long recordId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTan() {
        return tan;
    }

    public void setTan(String tan) {
        this.tan = tan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getRoc() {
        return roc;
    }

    public void setRoc(String roc) {
        this.roc = roc;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getCompanyRiskStatusId() {
        return companyRiskStatusId;
    }

    public void setCompanyRiskStatusId(Integer companyRiskStatusId) {
        this.companyRiskStatusId = companyRiskStatusId;
    }

    public String getCompanyRistComments() {
        return companyRistComments;
    }

    public void setCompanyRistComments(String companyRistComments) {
        this.companyRistComments = companyRistComments;
    }

    public String getCompanyBackground() {
        return companyBackground;
    }

    public void setCompanyBackground(String companyBackground) {
        this.companyBackground = companyBackground;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRptFlag() {
        return rptFlag;
    }

    public void setRptFlag(String rptFlag) {
        this.rptFlag = rptFlag;
    }

    public String getGstExempt() {
        return gstExempt;
    }

    public void setGstExempt(String gstExempt) {
        this.gstExempt = gstExempt;
    }

    public String getGstNumberNotApplicable() {
        return gstNumberNotApplicable;
    }

    public void setGstNumberNotApplicable(String gstNumberNotApplicable) {
        this.gstNumberNotApplicable = gstNumberNotApplicable;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long companyRequestId) {
        this.recordId = companyRequestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyTanPanRocDtlsDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyTanPanRocDtlsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyTanPanRocDtlsDTO{" +
            "id=" + getId() +
            ", tan='" + getTan() + "'" +
            ", pan='" + getPan() + "'" +
            ", roc='" + getRoc() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", companyRiskStatusId=" + getCompanyRiskStatusId() +
            ", companyRistComments='" + getCompanyRistComments() + "'" +
            ", companyBackground='" + getCompanyBackground() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", rptFlag='" + getRptFlag() + "'" +
            ", gstExempt='" + getGstExempt() + "'" +
            ", gstNumberNotApplicable='" + getGstNumberNotApplicable() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            ", recordId=" + getRecordId() +
            "}";
    }
}
