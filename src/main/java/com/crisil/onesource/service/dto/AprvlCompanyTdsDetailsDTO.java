package com.crisil.onesource.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.crisil.onesource.domain.AprvlCompanyTdsDetails} entity.
 */
public class AprvlCompanyTdsDetailsDTO implements Serializable {
    
    private Long id;

    @NotNull
    private Integer ctlgSrno;

    @Size(max = 50)
    private String recordid;

    @Size(max = 50)
    private String lineofbusiness;

    @Size(max = 100)
    private String filename;

    @Size(max = 50)
    private String gstnumber;

    @Lob
    private String comments;

    @Size(max = 10)
    private String issezgst;

    @Size(max = 50)
    private String previoustds;

    @Size(max = 50)
    private String tdsnumber;

    private Integer statecode;

    @Size(max = 1)
    private String operation;

    @Size(max = 10)
    private String hdeleted;

    private Instant createdDate;

    private Instant lastModifiedDate;

    @Size(max = 100)
    private String createdBy;

    @Size(max = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    private String ctlgIsDeleted;

    private Integer jsonStoreId;

    @Size(max = 1)
    private String isDataProcessed;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getLineofbusiness() {
        return lineofbusiness;
    }

    public void setLineofbusiness(String lineofbusiness) {
        this.lineofbusiness = lineofbusiness;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getGstnumber() {
        return gstnumber;
    }

    public void setGstnumber(String gstnumber) {
        this.gstnumber = gstnumber;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIssezgst() {
        return issezgst;
    }

    public void setIssezgst(String issezgst) {
        this.issezgst = issezgst;
    }

    public String getPrevioustds() {
        return previoustds;
    }

    public void setPrevioustds(String previoustds) {
        this.previoustds = previoustds;
    }

    public String getTdsnumber() {
        return tdsnumber;
    }

    public void setTdsnumber(String tdsnumber) {
        this.tdsnumber = tdsnumber;
    }

    public Integer getStatecode() {
        return statecode;
    }

    public void setStatecode(Integer statecode) {
        this.statecode = statecode;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getHdeleted() {
        return hdeleted;
    }

    public void setHdeleted(String hdeleted) {
        this.hdeleted = hdeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public Integer getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(Integer jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AprvlCompanyTdsDetailsDTO)) {
            return false;
        }

        return id != null && id.equals(((AprvlCompanyTdsDetailsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AprvlCompanyTdsDetailsDTO{" +
            "id=" + getId() +
            ", ctlgSrno=" + getCtlgSrno() +
            ", recordid='" + getRecordid() + "'" +
            ", lineofbusiness='" + getLineofbusiness() + "'" +
            ", filename='" + getFilename() + "'" +
            ", gstnumber='" + getGstnumber() + "'" +
            ", comments='" + getComments() + "'" +
            ", issezgst='" + getIssezgst() + "'" +
            ", previoustds='" + getPrevioustds() + "'" +
            ", tdsnumber='" + getTdsnumber() + "'" +
            ", statecode=" + getStatecode() +
            ", operation='" + getOperation() + "'" +
            ", hdeleted='" + getHdeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", jsonStoreId=" + getJsonStoreId() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            "}";
    }
}
