package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocCityMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocCityMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-city-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocCityMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cityName;

    private IntegerFilter cityId;

    private StringFilter stateName;

    private StringFilter countryName;

    private StringFilter pinNum;

    private StringFilter isDeleted;

    private StringFilter recordId;

    private StringFilter countryCode;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private IntegerFilter srno;

    private StringFilter isApproved;

    private IntegerFilter seqOrder;

    private LongFilter locCityPincodeMappingId;

    private LongFilter stateIdId;

    private LongFilter districtIdId;

    public LocCityMasterCriteria() {
    }

    public LocCityMasterCriteria(LocCityMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.cityName = other.cityName == null ? null : other.cityName.copy();
        this.cityId = other.cityId == null ? null : other.cityId.copy();
        this.stateName = other.stateName == null ? null : other.stateName.copy();
        this.countryName = other.countryName == null ? null : other.countryName.copy();
        this.pinNum = other.pinNum == null ? null : other.pinNum.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
        this.countryCode = other.countryCode == null ? null : other.countryCode.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.srno = other.srno == null ? null : other.srno.copy();
        this.isApproved = other.isApproved == null ? null : other.isApproved.copy();
        this.seqOrder = other.seqOrder == null ? null : other.seqOrder.copy();
        this.locCityPincodeMappingId = other.locCityPincodeMappingId == null ? null : other.locCityPincodeMappingId.copy();
        this.stateIdId = other.stateIdId == null ? null : other.stateIdId.copy();
        this.districtIdId = other.districtIdId == null ? null : other.districtIdId.copy();
    }

    @Override
    public LocCityMasterCriteria copy() {
        return new LocCityMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCityName() {
        return cityName;
    }

    public void setCityName(StringFilter cityName) {
        this.cityName = cityName;
    }

    public IntegerFilter getCityId() {
        return cityId;
    }

    public void setCityId(IntegerFilter cityId) {
        this.cityId = cityId;
    }

    public StringFilter getStateName() {
        return stateName;
    }

    public void setStateName(StringFilter stateName) {
        this.stateName = stateName;
    }

    public StringFilter getCountryName() {
        return countryName;
    }

    public void setCountryName(StringFilter countryName) {
        this.countryName = countryName;
    }

    public StringFilter getPinNum() {
        return pinNum;
    }

    public void setPinNum(StringFilter pinNum) {
        this.pinNum = pinNum;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(StringFilter recordId) {
        this.recordId = recordId;
    }

    public StringFilter getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(StringFilter countryCode) {
        this.countryCode = countryCode;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public IntegerFilter getSrno() {
        return srno;
    }

    public void setSrno(IntegerFilter srno) {
        this.srno = srno;
    }

    public StringFilter getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(StringFilter isApproved) {
        this.isApproved = isApproved;
    }

    public IntegerFilter getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(IntegerFilter seqOrder) {
        this.seqOrder = seqOrder;
    }

    public LongFilter getLocCityPincodeMappingId() {
        return locCityPincodeMappingId;
    }

    public void setLocCityPincodeMappingId(LongFilter locCityPincodeMappingId) {
        this.locCityPincodeMappingId = locCityPincodeMappingId;
    }

    public LongFilter getStateIdId() {
        return stateIdId;
    }

    public void setStateIdId(LongFilter stateIdId) {
        this.stateIdId = stateIdId;
    }

    public LongFilter getDistrictIdId() {
        return districtIdId;
    }

    public void setDistrictIdId(LongFilter districtIdId) {
        this.districtIdId = districtIdId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocCityMasterCriteria that = (LocCityMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cityName, that.cityName) &&
            Objects.equals(cityId, that.cityId) &&
            Objects.equals(stateName, that.stateName) &&
            Objects.equals(countryName, that.countryName) &&
            Objects.equals(pinNum, that.pinNum) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(recordId, that.recordId) &&
            Objects.equals(countryCode, that.countryCode) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(srno, that.srno) &&
            Objects.equals(isApproved, that.isApproved) &&
            Objects.equals(seqOrder, that.seqOrder) &&
            Objects.equals(locCityPincodeMappingId, that.locCityPincodeMappingId) &&
            Objects.equals(stateIdId, that.stateIdId) &&
            Objects.equals(districtIdId, that.districtIdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cityName,
        cityId,
        stateName,
        countryName,
        pinNum,
        isDeleted,
        recordId,
        countryCode,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        srno,
        isApproved,
        seqOrder,
        locCityPincodeMappingId,
        stateIdId,
        districtIdId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCityMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cityName != null ? "cityName=" + cityName + ", " : "") +
                (cityId != null ? "cityId=" + cityId + ", " : "") +
                (stateName != null ? "stateName=" + stateName + ", " : "") +
                (countryName != null ? "countryName=" + countryName + ", " : "") +
                (pinNum != null ? "pinNum=" + pinNum + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
                (countryCode != null ? "countryCode=" + countryCode + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (srno != null ? "srno=" + srno + ", " : "") +
                (isApproved != null ? "isApproved=" + isApproved + ", " : "") +
                (seqOrder != null ? "seqOrder=" + seqOrder + ", " : "") +
                (locCityPincodeMappingId != null ? "locCityPincodeMappingId=" + locCityPincodeMappingId + ", " : "") +
                (stateIdId != null ? "stateIdId=" + stateIdId + ", " : "") +
                (districtIdId != null ? "districtIdId=" + districtIdId + ", " : "") +
            "}";
    }

}
