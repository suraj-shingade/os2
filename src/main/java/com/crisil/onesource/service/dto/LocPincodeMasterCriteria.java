package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocPincodeMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocPincodeMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-pincode-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocPincodeMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter pincodeSrno;

    private StringFilter officeName;

    private StringFilter pincode;

    private StringFilter officeType;

    private StringFilter deliveryStatus;

    private StringFilter divisionName;

    private StringFilter regionName;

    private StringFilter circleName;

    private StringFilter taluka;

    private StringFilter districtName;

    private StringFilter stateName;

    private StringFilter country;

    private StringFilter isDeleted;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private StringFilter isApproved;

    private IntegerFilter seqOrder;

    private StringFilter isDataProcessed;

    private LongFilter jsonStoreId;

    private StringFilter ctlgIsDeleted;

    private StringFilter operation;

    private IntegerFilter ctlgSrno;

    private LongFilter companyAddressMasterId;

    private LongFilter onesourceCompanyMasterId;

    private LongFilter stateIdId;

    private LongFilter districtIdId;

    public LocPincodeMasterCriteria() {
    }

    public LocPincodeMasterCriteria(LocPincodeMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.pincodeSrno = other.pincodeSrno == null ? null : other.pincodeSrno.copy();
        this.officeName = other.officeName == null ? null : other.officeName.copy();
        this.pincode = other.pincode == null ? null : other.pincode.copy();
        this.officeType = other.officeType == null ? null : other.officeType.copy();
        this.deliveryStatus = other.deliveryStatus == null ? null : other.deliveryStatus.copy();
        this.divisionName = other.divisionName == null ? null : other.divisionName.copy();
        this.regionName = other.regionName == null ? null : other.regionName.copy();
        this.circleName = other.circleName == null ? null : other.circleName.copy();
        this.taluka = other.taluka == null ? null : other.taluka.copy();
        this.districtName = other.districtName == null ? null : other.districtName.copy();
        this.stateName = other.stateName == null ? null : other.stateName.copy();
        this.country = other.country == null ? null : other.country.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.isApproved = other.isApproved == null ? null : other.isApproved.copy();
        this.seqOrder = other.seqOrder == null ? null : other.seqOrder.copy();
        this.isDataProcessed = other.isDataProcessed == null ? null : other.isDataProcessed.copy();
        this.jsonStoreId = other.jsonStoreId == null ? null : other.jsonStoreId.copy();
        this.ctlgIsDeleted = other.ctlgIsDeleted == null ? null : other.ctlgIsDeleted.copy();
        this.operation = other.operation == null ? null : other.operation.copy();
        this.ctlgSrno = other.ctlgSrno == null ? null : other.ctlgSrno.copy();
        this.companyAddressMasterId = other.companyAddressMasterId == null ? null : other.companyAddressMasterId.copy();
        this.onesourceCompanyMasterId = other.onesourceCompanyMasterId == null ? null : other.onesourceCompanyMasterId.copy();
        this.stateIdId = other.stateIdId == null ? null : other.stateIdId.copy();
        this.districtIdId = other.districtIdId == null ? null : other.districtIdId.copy();
    }

    @Override
    public LocPincodeMasterCriteria copy() {
        return new LocPincodeMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getPincodeSrno() {
        return pincodeSrno;
    }

    public void setPincodeSrno(IntegerFilter pincodeSrno) {
        this.pincodeSrno = pincodeSrno;
    }

    public StringFilter getOfficeName() {
        return officeName;
    }

    public void setOfficeName(StringFilter officeName) {
        this.officeName = officeName;
    }

    public StringFilter getPincode() {
        return pincode;
    }

    public void setPincode(StringFilter pincode) {
        this.pincode = pincode;
    }

    public StringFilter getOfficeType() {
        return officeType;
    }

    public void setOfficeType(StringFilter officeType) {
        this.officeType = officeType;
    }

    public StringFilter getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(StringFilter deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public StringFilter getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(StringFilter divisionName) {
        this.divisionName = divisionName;
    }

    public StringFilter getRegionName() {
        return regionName;
    }

    public void setRegionName(StringFilter regionName) {
        this.regionName = regionName;
    }

    public StringFilter getCircleName() {
        return circleName;
    }

    public void setCircleName(StringFilter circleName) {
        this.circleName = circleName;
    }

    public StringFilter getTaluka() {
        return taluka;
    }

    public void setTaluka(StringFilter taluka) {
        this.taluka = taluka;
    }

    public StringFilter getDistrictName() {
        return districtName;
    }

    public void setDistrictName(StringFilter districtName) {
        this.districtName = districtName;
    }

    public StringFilter getStateName() {
        return stateName;
    }

    public void setStateName(StringFilter stateName) {
        this.stateName = stateName;
    }

    public StringFilter getCountry() {
        return country;
    }

    public void setCountry(StringFilter country) {
        this.country = country;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(StringFilter isApproved) {
        this.isApproved = isApproved;
    }

    public IntegerFilter getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(IntegerFilter seqOrder) {
        this.seqOrder = seqOrder;
    }

    public StringFilter getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(StringFilter isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public LongFilter getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(LongFilter jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public StringFilter getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(StringFilter ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public StringFilter getOperation() {
        return operation;
    }

    public void setOperation(StringFilter operation) {
        this.operation = operation;
    }

    public IntegerFilter getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(IntegerFilter ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public LongFilter getCompanyAddressMasterId() {
        return companyAddressMasterId;
    }

    public void setCompanyAddressMasterId(LongFilter companyAddressMasterId) {
        this.companyAddressMasterId = companyAddressMasterId;
    }

    public LongFilter getOnesourceCompanyMasterId() {
        return onesourceCompanyMasterId;
    }

    public void setOnesourceCompanyMasterId(LongFilter onesourceCompanyMasterId) {
        this.onesourceCompanyMasterId = onesourceCompanyMasterId;
    }

    public LongFilter getStateIdId() {
        return stateIdId;
    }

    public void setStateIdId(LongFilter stateIdId) {
        this.stateIdId = stateIdId;
    }

    public LongFilter getDistrictIdId() {
        return districtIdId;
    }

    public void setDistrictIdId(LongFilter districtIdId) {
        this.districtIdId = districtIdId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocPincodeMasterCriteria that = (LocPincodeMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(pincodeSrno, that.pincodeSrno) &&
            Objects.equals(officeName, that.officeName) &&
            Objects.equals(pincode, that.pincode) &&
            Objects.equals(officeType, that.officeType) &&
            Objects.equals(deliveryStatus, that.deliveryStatus) &&
            Objects.equals(divisionName, that.divisionName) &&
            Objects.equals(regionName, that.regionName) &&
            Objects.equals(circleName, that.circleName) &&
            Objects.equals(taluka, that.taluka) &&
            Objects.equals(districtName, that.districtName) &&
            Objects.equals(stateName, that.stateName) &&
            Objects.equals(country, that.country) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(isApproved, that.isApproved) &&
            Objects.equals(seqOrder, that.seqOrder) &&
            Objects.equals(isDataProcessed, that.isDataProcessed) &&
            Objects.equals(jsonStoreId, that.jsonStoreId) &&
            Objects.equals(ctlgIsDeleted, that.ctlgIsDeleted) &&
            Objects.equals(operation, that.operation) &&
            Objects.equals(ctlgSrno, that.ctlgSrno) &&
            Objects.equals(companyAddressMasterId, that.companyAddressMasterId) &&
            Objects.equals(onesourceCompanyMasterId, that.onesourceCompanyMasterId) &&
            Objects.equals(stateIdId, that.stateIdId) &&
            Objects.equals(districtIdId, that.districtIdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        pincodeSrno,
        officeName,
        pincode,
        officeType,
        deliveryStatus,
        divisionName,
        regionName,
        circleName,
        taluka,
        districtName,
        stateName,
        country,
        isDeleted,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        isApproved,
        seqOrder,
        isDataProcessed,
        jsonStoreId,
        ctlgIsDeleted,
        operation,
        ctlgSrno,
        companyAddressMasterId,
        onesourceCompanyMasterId,
        stateIdId,
        districtIdId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocPincodeMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (pincodeSrno != null ? "pincodeSrno=" + pincodeSrno + ", " : "") +
                (officeName != null ? "officeName=" + officeName + ", " : "") +
                (pincode != null ? "pincode=" + pincode + ", " : "") +
                (officeType != null ? "officeType=" + officeType + ", " : "") +
                (deliveryStatus != null ? "deliveryStatus=" + deliveryStatus + ", " : "") +
                (divisionName != null ? "divisionName=" + divisionName + ", " : "") +
                (regionName != null ? "regionName=" + regionName + ", " : "") +
                (circleName != null ? "circleName=" + circleName + ", " : "") +
                (taluka != null ? "taluka=" + taluka + ", " : "") +
                (districtName != null ? "districtName=" + districtName + ", " : "") +
                (stateName != null ? "stateName=" + stateName + ", " : "") +
                (country != null ? "country=" + country + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (isApproved != null ? "isApproved=" + isApproved + ", " : "") +
                (seqOrder != null ? "seqOrder=" + seqOrder + ", " : "") +
                (isDataProcessed != null ? "isDataProcessed=" + isDataProcessed + ", " : "") +
                (jsonStoreId != null ? "jsonStoreId=" + jsonStoreId + ", " : "") +
                (ctlgIsDeleted != null ? "ctlgIsDeleted=" + ctlgIsDeleted + ", " : "") +
                (operation != null ? "operation=" + operation + ", " : "") +
                (ctlgSrno != null ? "ctlgSrno=" + ctlgSrno + ", " : "") +
                (companyAddressMasterId != null ? "companyAddressMasterId=" + companyAddressMasterId + ", " : "") +
                (onesourceCompanyMasterId != null ? "onesourceCompanyMasterId=" + onesourceCompanyMasterId + ", " : "") +
                (stateIdId != null ? "stateIdId=" + stateIdId + ", " : "") +
                (districtIdId != null ? "districtIdId=" + districtIdId + ", " : "") +
            "}";
    }

}
