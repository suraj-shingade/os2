package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanySectorMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanySectorMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-sector-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanySectorMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter sectorId;

    private StringFilter sectorName;

    private StringFilter isDeleted;

    private LocalDateFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private StringFilter createdBy;

    private LocalDateFilter createdDate;

    private StringFilter operation;

    private LongFilter onesourceCompanyMasterId;

    public CompanySectorMasterCriteria() {
    }

    public CompanySectorMasterCriteria(CompanySectorMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.sectorId = other.sectorId == null ? null : other.sectorId.copy();
        this.sectorName = other.sectorName == null ? null : other.sectorName.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.operation = other.operation == null ? null : other.operation.copy();
        this.onesourceCompanyMasterId = other.onesourceCompanyMasterId == null ? null : other.onesourceCompanyMasterId.copy();
    }

    @Override
    public CompanySectorMasterCriteria copy() {
        return new CompanySectorMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getSectorId() {
        return sectorId;
    }

    public void setSectorId(IntegerFilter sectorId) {
        this.sectorId = sectorId;
    }

    public StringFilter getSectorName() {
        return sectorName;
    }

    public void setSectorName(StringFilter sectorName) {
        this.sectorName = sectorName;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getOperation() {
        return operation;
    }

    public void setOperation(StringFilter operation) {
        this.operation = operation;
    }

    public LongFilter getOnesourceCompanyMasterId() {
        return onesourceCompanyMasterId;
    }

    public void setOnesourceCompanyMasterId(LongFilter onesourceCompanyMasterId) {
        this.onesourceCompanyMasterId = onesourceCompanyMasterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanySectorMasterCriteria that = (CompanySectorMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(sectorId, that.sectorId) &&
            Objects.equals(sectorName, that.sectorName) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(operation, that.operation) &&
            Objects.equals(onesourceCompanyMasterId, that.onesourceCompanyMasterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        sectorId,
        sectorName,
        isDeleted,
        lastModifiedDate,
        lastModifiedBy,
        createdBy,
        createdDate,
        operation,
        onesourceCompanyMasterId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanySectorMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (sectorId != null ? "sectorId=" + sectorId + ", " : "") +
                (sectorName != null ? "sectorName=" + sectorName + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (operation != null ? "operation=" + operation + ", " : "") +
                (onesourceCompanyMasterId != null ? "onesourceCompanyMasterId=" + onesourceCompanyMasterId + ", " : "") +
            "}";
    }

}
