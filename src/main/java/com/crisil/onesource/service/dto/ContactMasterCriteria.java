package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.ContactMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.ContactMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter firstName;

    private StringFilter lastName;

    private StringFilter middleName;

    private StringFilter salutation;

    private StringFilter address1;

    private StringFilter address2;

    private StringFilter address3;

    private StringFilter designation;

    private StringFilter department;

    private StringFilter city;

    private StringFilter state;

    private StringFilter country;

    private StringFilter pin;

    private StringFilter phoneNum;

    private StringFilter faxNum;

    private StringFilter email;

    private StringFilter mobileNum;

    private StringFilter recordId;

    private StringFilter comments;

    private StringFilter keyPerson;

    private StringFilter contactId;

    private StringFilter levelImportance;

    private StringFilter status;

    private StringFilter removeReason;

    private StringFilter cityId;

    private StringFilter oFlag;

    private StringFilter optOut;

    private StringFilter ofaContactId;

    private LocalDateFilter optOutDate;

    private StringFilter faxOptout;

    private StringFilter donotcall;

    private StringFilter dnsendDirectMail;

    private StringFilter dnshareOutsideMhp;

    private StringFilter outsideOfContact;

    private StringFilter dnshareOutsideSnp;

    private StringFilter emailOptout;

    private StringFilter smsOptout;

    private StringFilter isSezContactFlag;

    private StringFilter isExemptContactFlag;

    private StringFilter gstNotApplicable;

    private StringFilter billingContact;

    private StringFilter gstPartyType;

    private StringFilter tan;

    private StringFilter gstNumber;

    private StringFilter tdsNumber;

    private StringFilter createdBy;

    private LocalDateFilter createdDate;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private LongFilter compGstNotApplicableId;

    private LongFilter contactChangeRequestId;

    private LongFilter companyCodeId;

    public ContactMasterCriteria() {
    }

    public ContactMasterCriteria(ContactMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.firstName = other.firstName == null ? null : other.firstName.copy();
        this.lastName = other.lastName == null ? null : other.lastName.copy();
        this.middleName = other.middleName == null ? null : other.middleName.copy();
        this.salutation = other.salutation == null ? null : other.salutation.copy();
        this.address1 = other.address1 == null ? null : other.address1.copy();
        this.address2 = other.address2 == null ? null : other.address2.copy();
        this.address3 = other.address3 == null ? null : other.address3.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.department = other.department == null ? null : other.department.copy();
        this.city = other.city == null ? null : other.city.copy();
        this.state = other.state == null ? null : other.state.copy();
        this.country = other.country == null ? null : other.country.copy();
        this.pin = other.pin == null ? null : other.pin.copy();
        this.phoneNum = other.phoneNum == null ? null : other.phoneNum.copy();
        this.faxNum = other.faxNum == null ? null : other.faxNum.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.mobileNum = other.mobileNum == null ? null : other.mobileNum.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
        this.comments = other.comments == null ? null : other.comments.copy();
        this.keyPerson = other.keyPerson == null ? null : other.keyPerson.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
        this.levelImportance = other.levelImportance == null ? null : other.levelImportance.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.removeReason = other.removeReason == null ? null : other.removeReason.copy();
        this.cityId = other.cityId == null ? null : other.cityId.copy();
        this.oFlag = other.oFlag == null ? null : other.oFlag.copy();
        this.optOut = other.optOut == null ? null : other.optOut.copy();
        this.ofaContactId = other.ofaContactId == null ? null : other.ofaContactId.copy();
        this.optOutDate = other.optOutDate == null ? null : other.optOutDate.copy();
        this.faxOptout = other.faxOptout == null ? null : other.faxOptout.copy();
        this.donotcall = other.donotcall == null ? null : other.donotcall.copy();
        this.dnsendDirectMail = other.dnsendDirectMail == null ? null : other.dnsendDirectMail.copy();
        this.dnshareOutsideMhp = other.dnshareOutsideMhp == null ? null : other.dnshareOutsideMhp.copy();
        this.outsideOfContact = other.outsideOfContact == null ? null : other.outsideOfContact.copy();
        this.dnshareOutsideSnp = other.dnshareOutsideSnp == null ? null : other.dnshareOutsideSnp.copy();
        this.emailOptout = other.emailOptout == null ? null : other.emailOptout.copy();
        this.smsOptout = other.smsOptout == null ? null : other.smsOptout.copy();
        this.isSezContactFlag = other.isSezContactFlag == null ? null : other.isSezContactFlag.copy();
        this.isExemptContactFlag = other.isExemptContactFlag == null ? null : other.isExemptContactFlag.copy();
        this.gstNotApplicable = other.gstNotApplicable == null ? null : other.gstNotApplicable.copy();
        this.billingContact = other.billingContact == null ? null : other.billingContact.copy();
        this.gstPartyType = other.gstPartyType == null ? null : other.gstPartyType.copy();
        this.tan = other.tan == null ? null : other.tan.copy();
        this.gstNumber = other.gstNumber == null ? null : other.gstNumber.copy();
        this.tdsNumber = other.tdsNumber == null ? null : other.tdsNumber.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.compGstNotApplicableId = other.compGstNotApplicableId == null ? null : other.compGstNotApplicableId.copy();
        this.contactChangeRequestId = other.contactChangeRequestId == null ? null : other.contactChangeRequestId.copy();
        this.companyCodeId = other.companyCodeId == null ? null : other.companyCodeId.copy();
    }

    @Override
    public ContactMasterCriteria copy() {
        return new ContactMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getFirstName() {
        return firstName;
    }

    public void setFirstName(StringFilter firstName) {
        this.firstName = firstName;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getMiddleName() {
        return middleName;
    }

    public void setMiddleName(StringFilter middleName) {
        this.middleName = middleName;
    }

    public StringFilter getSalutation() {
        return salutation;
    }

    public void setSalutation(StringFilter salutation) {
        this.salutation = salutation;
    }

    public StringFilter getAddress1() {
        return address1;
    }

    public void setAddress1(StringFilter address1) {
        this.address1 = address1;
    }

    public StringFilter getAddress2() {
        return address2;
    }

    public void setAddress2(StringFilter address2) {
        this.address2 = address2;
    }

    public StringFilter getAddress3() {
        return address3;
    }

    public void setAddress3(StringFilter address3) {
        this.address3 = address3;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public StringFilter getDepartment() {
        return department;
    }

    public void setDepartment(StringFilter department) {
        this.department = department;
    }

    public StringFilter getCity() {
        return city;
    }

    public void setCity(StringFilter city) {
        this.city = city;
    }

    public StringFilter getState() {
        return state;
    }

    public void setState(StringFilter state) {
        this.state = state;
    }

    public StringFilter getCountry() {
        return country;
    }

    public void setCountry(StringFilter country) {
        this.country = country;
    }

    public StringFilter getPin() {
        return pin;
    }

    public void setPin(StringFilter pin) {
        this.pin = pin;
    }

    public StringFilter getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(StringFilter phoneNum) {
        this.phoneNum = phoneNum;
    }

    public StringFilter getFaxNum() {
        return faxNum;
    }

    public void setFaxNum(StringFilter faxNum) {
        this.faxNum = faxNum;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(StringFilter mobileNum) {
        this.mobileNum = mobileNum;
    }

    public StringFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(StringFilter recordId) {
        this.recordId = recordId;
    }

    public StringFilter getComments() {
        return comments;
    }

    public void setComments(StringFilter comments) {
        this.comments = comments;
    }

    public StringFilter getKeyPerson() {
        return keyPerson;
    }

    public void setKeyPerson(StringFilter keyPerson) {
        this.keyPerson = keyPerson;
    }

    public StringFilter getContactId() {
        return contactId;
    }

    public void setContactId(StringFilter contactId) {
        this.contactId = contactId;
    }

    public StringFilter getLevelImportance() {
        return levelImportance;
    }

    public void setLevelImportance(StringFilter levelImportance) {
        this.levelImportance = levelImportance;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getRemoveReason() {
        return removeReason;
    }

    public void setRemoveReason(StringFilter removeReason) {
        this.removeReason = removeReason;
    }

    public StringFilter getCityId() {
        return cityId;
    }

    public void setCityId(StringFilter cityId) {
        this.cityId = cityId;
    }

    public StringFilter getoFlag() {
        return oFlag;
    }

    public void setoFlag(StringFilter oFlag) {
        this.oFlag = oFlag;
    }

    public StringFilter getOptOut() {
        return optOut;
    }

    public void setOptOut(StringFilter optOut) {
        this.optOut = optOut;
    }

    public StringFilter getOfaContactId() {
        return ofaContactId;
    }

    public void setOfaContactId(StringFilter ofaContactId) {
        this.ofaContactId = ofaContactId;
    }

    public LocalDateFilter getOptOutDate() {
        return optOutDate;
    }

    public void setOptOutDate(LocalDateFilter optOutDate) {
        this.optOutDate = optOutDate;
    }

    public StringFilter getFaxOptout() {
        return faxOptout;
    }

    public void setFaxOptout(StringFilter faxOptout) {
        this.faxOptout = faxOptout;
    }

    public StringFilter getDonotcall() {
        return donotcall;
    }

    public void setDonotcall(StringFilter donotcall) {
        this.donotcall = donotcall;
    }

    public StringFilter getDnsendDirectMail() {
        return dnsendDirectMail;
    }

    public void setDnsendDirectMail(StringFilter dnsendDirectMail) {
        this.dnsendDirectMail = dnsendDirectMail;
    }

    public StringFilter getDnshareOutsideMhp() {
        return dnshareOutsideMhp;
    }

    public void setDnshareOutsideMhp(StringFilter dnshareOutsideMhp) {
        this.dnshareOutsideMhp = dnshareOutsideMhp;
    }

    public StringFilter getOutsideOfContact() {
        return outsideOfContact;
    }

    public void setOutsideOfContact(StringFilter outsideOfContact) {
        this.outsideOfContact = outsideOfContact;
    }

    public StringFilter getDnshareOutsideSnp() {
        return dnshareOutsideSnp;
    }

    public void setDnshareOutsideSnp(StringFilter dnshareOutsideSnp) {
        this.dnshareOutsideSnp = dnshareOutsideSnp;
    }

    public StringFilter getEmailOptout() {
        return emailOptout;
    }

    public void setEmailOptout(StringFilter emailOptout) {
        this.emailOptout = emailOptout;
    }

    public StringFilter getSmsOptout() {
        return smsOptout;
    }

    public void setSmsOptout(StringFilter smsOptout) {
        this.smsOptout = smsOptout;
    }

    public StringFilter getIsSezContactFlag() {
        return isSezContactFlag;
    }

    public void setIsSezContactFlag(StringFilter isSezContactFlag) {
        this.isSezContactFlag = isSezContactFlag;
    }

    public StringFilter getIsExemptContactFlag() {
        return isExemptContactFlag;
    }

    public void setIsExemptContactFlag(StringFilter isExemptContactFlag) {
        this.isExemptContactFlag = isExemptContactFlag;
    }

    public StringFilter getGstNotApplicable() {
        return gstNotApplicable;
    }

    public void setGstNotApplicable(StringFilter gstNotApplicable) {
        this.gstNotApplicable = gstNotApplicable;
    }

    public StringFilter getBillingContact() {
        return billingContact;
    }

    public void setBillingContact(StringFilter billingContact) {
        this.billingContact = billingContact;
    }

    public StringFilter getGstPartyType() {
        return gstPartyType;
    }

    public void setGstPartyType(StringFilter gstPartyType) {
        this.gstPartyType = gstPartyType;
    }

    public StringFilter getTan() {
        return tan;
    }

    public void setTan(StringFilter tan) {
        this.tan = tan;
    }

    public StringFilter getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(StringFilter gstNumber) {
        this.gstNumber = gstNumber;
    }

    public StringFilter getTdsNumber() {
        return tdsNumber;
    }

    public void setTdsNumber(StringFilter tdsNumber) {
        this.tdsNumber = tdsNumber;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getCompGstNotApplicableId() {
        return compGstNotApplicableId;
    }

    public void setCompGstNotApplicableId(LongFilter compGstNotApplicableId) {
        this.compGstNotApplicableId = compGstNotApplicableId;
    }

    public LongFilter getContactChangeRequestId() {
        return contactChangeRequestId;
    }

    public void setContactChangeRequestId(LongFilter contactChangeRequestId) {
        this.contactChangeRequestId = contactChangeRequestId;
    }

    public LongFilter getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(LongFilter companyCodeId) {
        this.companyCodeId = companyCodeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactMasterCriteria that = (ContactMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(middleName, that.middleName) &&
            Objects.equals(salutation, that.salutation) &&
            Objects.equals(address1, that.address1) &&
            Objects.equals(address2, that.address2) &&
            Objects.equals(address3, that.address3) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(department, that.department) &&
            Objects.equals(city, that.city) &&
            Objects.equals(state, that.state) &&
            Objects.equals(country, that.country) &&
            Objects.equals(pin, that.pin) &&
            Objects.equals(phoneNum, that.phoneNum) &&
            Objects.equals(faxNum, that.faxNum) &&
            Objects.equals(email, that.email) &&
            Objects.equals(mobileNum, that.mobileNum) &&
            Objects.equals(recordId, that.recordId) &&
            Objects.equals(comments, that.comments) &&
            Objects.equals(keyPerson, that.keyPerson) &&
            Objects.equals(contactId, that.contactId) &&
            Objects.equals(levelImportance, that.levelImportance) &&
            Objects.equals(status, that.status) &&
            Objects.equals(removeReason, that.removeReason) &&
            Objects.equals(cityId, that.cityId) &&
            Objects.equals(oFlag, that.oFlag) &&
            Objects.equals(optOut, that.optOut) &&
            Objects.equals(ofaContactId, that.ofaContactId) &&
            Objects.equals(optOutDate, that.optOutDate) &&
            Objects.equals(faxOptout, that.faxOptout) &&
            Objects.equals(donotcall, that.donotcall) &&
            Objects.equals(dnsendDirectMail, that.dnsendDirectMail) &&
            Objects.equals(dnshareOutsideMhp, that.dnshareOutsideMhp) &&
            Objects.equals(outsideOfContact, that.outsideOfContact) &&
            Objects.equals(dnshareOutsideSnp, that.dnshareOutsideSnp) &&
            Objects.equals(emailOptout, that.emailOptout) &&
            Objects.equals(smsOptout, that.smsOptout) &&
            Objects.equals(isSezContactFlag, that.isSezContactFlag) &&
            Objects.equals(isExemptContactFlag, that.isExemptContactFlag) &&
            Objects.equals(gstNotApplicable, that.gstNotApplicable) &&
            Objects.equals(billingContact, that.billingContact) &&
            Objects.equals(gstPartyType, that.gstPartyType) &&
            Objects.equals(tan, that.tan) &&
            Objects.equals(gstNumber, that.gstNumber) &&
            Objects.equals(tdsNumber, that.tdsNumber) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(compGstNotApplicableId, that.compGstNotApplicableId) &&
            Objects.equals(contactChangeRequestId, that.contactChangeRequestId) &&
            Objects.equals(companyCodeId, that.companyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        firstName,
        lastName,
        middleName,
        salutation,
        address1,
        address2,
        address3,
        designation,
        department,
        city,
        state,
        country,
        pin,
        phoneNum,
        faxNum,
        email,
        mobileNum,
        recordId,
        comments,
        keyPerson,
        contactId,
        levelImportance,
        status,
        removeReason,
        cityId,
        oFlag,
        optOut,
        ofaContactId,
        optOutDate,
        faxOptout,
        donotcall,
        dnsendDirectMail,
        dnshareOutsideMhp,
        outsideOfContact,
        dnshareOutsideSnp,
        emailOptout,
        smsOptout,
        isSezContactFlag,
        isExemptContactFlag,
        gstNotApplicable,
        billingContact,
        gstPartyType,
        tan,
        gstNumber,
        tdsNumber,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        compGstNotApplicableId,
        contactChangeRequestId,
        companyCodeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (firstName != null ? "firstName=" + firstName + ", " : "") +
                (lastName != null ? "lastName=" + lastName + ", " : "") +
                (middleName != null ? "middleName=" + middleName + ", " : "") +
                (salutation != null ? "salutation=" + salutation + ", " : "") +
                (address1 != null ? "address1=" + address1 + ", " : "") +
                (address2 != null ? "address2=" + address2 + ", " : "") +
                (address3 != null ? "address3=" + address3 + ", " : "") +
                (designation != null ? "designation=" + designation + ", " : "") +
                (department != null ? "department=" + department + ", " : "") +
                (city != null ? "city=" + city + ", " : "") +
                (state != null ? "state=" + state + ", " : "") +
                (country != null ? "country=" + country + ", " : "") +
                (pin != null ? "pin=" + pin + ", " : "") +
                (phoneNum != null ? "phoneNum=" + phoneNum + ", " : "") +
                (faxNum != null ? "faxNum=" + faxNum + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (mobileNum != null ? "mobileNum=" + mobileNum + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
                (comments != null ? "comments=" + comments + ", " : "") +
                (keyPerson != null ? "keyPerson=" + keyPerson + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
                (levelImportance != null ? "levelImportance=" + levelImportance + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (removeReason != null ? "removeReason=" + removeReason + ", " : "") +
                (cityId != null ? "cityId=" + cityId + ", " : "") +
                (oFlag != null ? "oFlag=" + oFlag + ", " : "") +
                (optOut != null ? "optOut=" + optOut + ", " : "") +
                (ofaContactId != null ? "ofaContactId=" + ofaContactId + ", " : "") +
                (optOutDate != null ? "optOutDate=" + optOutDate + ", " : "") +
                (faxOptout != null ? "faxOptout=" + faxOptout + ", " : "") +
                (donotcall != null ? "donotcall=" + donotcall + ", " : "") +
                (dnsendDirectMail != null ? "dnsendDirectMail=" + dnsendDirectMail + ", " : "") +
                (dnshareOutsideMhp != null ? "dnshareOutsideMhp=" + dnshareOutsideMhp + ", " : "") +
                (outsideOfContact != null ? "outsideOfContact=" + outsideOfContact + ", " : "") +
                (dnshareOutsideSnp != null ? "dnshareOutsideSnp=" + dnshareOutsideSnp + ", " : "") +
                (emailOptout != null ? "emailOptout=" + emailOptout + ", " : "") +
                (smsOptout != null ? "smsOptout=" + smsOptout + ", " : "") +
                (isSezContactFlag != null ? "isSezContactFlag=" + isSezContactFlag + ", " : "") +
                (isExemptContactFlag != null ? "isExemptContactFlag=" + isExemptContactFlag + ", " : "") +
                (gstNotApplicable != null ? "gstNotApplicable=" + gstNotApplicable + ", " : "") +
                (billingContact != null ? "billingContact=" + billingContact + ", " : "") +
                (gstPartyType != null ? "gstPartyType=" + gstPartyType + ", " : "") +
                (tan != null ? "tan=" + tan + ", " : "") +
                (gstNumber != null ? "gstNumber=" + gstNumber + ", " : "") +
                (tdsNumber != null ? "tdsNumber=" + tdsNumber + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (compGstNotApplicableId != null ? "compGstNotApplicableId=" + compGstNotApplicableId + ", " : "") +
                (contactChangeRequestId != null ? "contactChangeRequestId=" + contactChangeRequestId + ", " : "") +
                (companyCodeId != null ? "companyCodeId=" + companyCodeId + ", " : "") +
            "}";
    }

}
