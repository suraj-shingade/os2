package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.LocCityMaster} entity.
 */
@ApiModel(description = "Location_Master_Table")
public class LocCityMasterDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 100)
    private String cityName;

    @NotNull
    private Integer cityId;

    @Size(max = 100)
    private String stateName;

    @Size(max = 100)
    private String countryName;

    @Size(max = 20)
    private String pinNum;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 25)
    private String recordId;

    @Size(max = 25)
    private String countryCode;

    private Instant createdDate;

    @Size(max = 100)
    private String createdBy;

    private Instant lastModifiedDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private Integer srno;

    @Size(max = 1)
    private String isApproved;

    private Integer seqOrder;


    private Long stateIdId;

    private Long districtIdId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPinNum() {
        return pinNum;
    }

    public void setPinNum(String pinNum) {
        this.pinNum = pinNum;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getSrno() {
        return srno;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public Long getStateIdId() {
        return stateIdId;
    }

    public void setStateIdId(Long locStateMasterId) {
        this.stateIdId = locStateMasterId;
    }

    public Long getDistrictIdId() {
        return districtIdId;
    }

    public void setDistrictIdId(Long locDistrictMasterId) {
        this.districtIdId = locDistrictMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocCityMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((LocCityMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocCityMasterDTO{" +
            "id=" + getId() +
            ", cityName='" + getCityName() + "'" +
            ", cityId=" + getCityId() +
            ", stateName='" + getStateName() + "'" +
            ", countryName='" + getCountryName() + "'" +
            ", pinNum='" + getPinNum() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", countryCode='" + getCountryCode() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", srno=" + getSrno() +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            ", stateIdId=" + getStateIdId() +
            ", districtIdId=" + getDistrictIdId() +
            "}";
    }
}
