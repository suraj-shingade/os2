package com.crisil.onesource.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompanyRequest} entity.
 */
public class CompanyRequestDTO implements Serializable {
    
    private Long id;

    @Size(max = 200)
    private String companyName;

    private Integer companyTypeId;

    private Integer sectorId;

    private Integer industryId;

    private Integer businessAreaId;

    @Size(max = 50)
    private String clientGroupName;

    @Size(max = 100)
    private String remarks;

    @NotNull
    @Size(max = 50)
    private String recordId;

    @Size(max = 10)
    private String companyCode;

    @Size(max = 50)
    private String status;

    @Size(max = 50)
    private String reason;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 50)
    private String billingType;

    @Size(max = 200)
    private String displayCompanyName;

    @Size(max = 20)
    private String requestedBy;

    @Size(max = 20)
    private String approvedBy;

    private Instant createdDate;

    @Size(max = 50)
    private String createdBy;

    private Instant lastModifiedDate;

    private Instant requestedDate;

    private Instant approvedDate;

    @Size(max = 100)
    private String lastModifiedBy;

    @Size(max = 50)
    private String originalFilename;

    @Size(max = 500)
    private String displayFilename;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(Integer companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public Integer getSectorId() {
        return sectorId;
    }

    public void setSectorId(Integer sectorId) {
        this.sectorId = sectorId;
    }

    public Integer getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Integer industryId) {
        this.industryId = industryId;
    }

    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public String getClientGroupName() {
        return clientGroupName;
    }

    public void setClientGroupName(String clientGroupName) {
        this.clientGroupName = clientGroupName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getDisplayCompanyName() {
        return displayCompanyName;
    }

    public void setDisplayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Instant requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Instant getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Instant approvedDate) {
        this.approvedDate = approvedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public void setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyRequestDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyRequestDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyRequestDTO{" +
            "id=" + getId() +
            ", companyName='" + getCompanyName() + "'" +
            ", companyTypeId=" + getCompanyTypeId() +
            ", sectorId=" + getSectorId() +
            ", industryId=" + getIndustryId() +
            ", businessAreaId=" + getBusinessAreaId() +
            ", clientGroupName='" + getClientGroupName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", companyCode='" + getCompanyCode() + "'" +
            ", status='" + getStatus() + "'" +
            ", reason='" + getReason() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", billingType='" + getBillingType() + "'" +
            ", displayCompanyName='" + getDisplayCompanyName() + "'" +
            ", requestedBy='" + getRequestedBy() + "'" +
            ", approvedBy='" + getApprovedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", requestedDate='" + getRequestedDate() + "'" +
            ", approvedDate='" + getApprovedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", originalFilename='" + getOriginalFilename() + "'" +
            ", displayFilename='" + getDisplayFilename() + "'" +
            "}";
    }
}
