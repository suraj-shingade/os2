package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.AprvlCompanyGstDetails} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.AprvlCompanyGstDetailsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /aprvl-company-gst-details?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AprvlCompanyGstDetailsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter ctlgSrno;

    private StringFilter recordid;

    private StringFilter lineofbusiness;

    private StringFilter filename;

    private StringFilter gstnumber;

    private StringFilter issezgst;

    private StringFilter previousgst;

    private IntegerFilter statecode;

    private StringFilter operation;

    private StringFilter hdeleted;

    private InstantFilter createdDate;

    private InstantFilter lastModifiedDate;

    private StringFilter createdBy;

    private StringFilter lastModifiedBy;

    private StringFilter ctlgIsDeleted;

    private IntegerFilter jsonStoreId;

    private StringFilter isDataProcessed;

    public AprvlCompanyGstDetailsCriteria() {
    }

    public AprvlCompanyGstDetailsCriteria(AprvlCompanyGstDetailsCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.ctlgSrno = other.ctlgSrno == null ? null : other.ctlgSrno.copy();
        this.recordid = other.recordid == null ? null : other.recordid.copy();
        this.lineofbusiness = other.lineofbusiness == null ? null : other.lineofbusiness.copy();
        this.filename = other.filename == null ? null : other.filename.copy();
        this.gstnumber = other.gstnumber == null ? null : other.gstnumber.copy();
        this.issezgst = other.issezgst == null ? null : other.issezgst.copy();
        this.previousgst = other.previousgst == null ? null : other.previousgst.copy();
        this.statecode = other.statecode == null ? null : other.statecode.copy();
        this.operation = other.operation == null ? null : other.operation.copy();
        this.hdeleted = other.hdeleted == null ? null : other.hdeleted.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.ctlgIsDeleted = other.ctlgIsDeleted == null ? null : other.ctlgIsDeleted.copy();
        this.jsonStoreId = other.jsonStoreId == null ? null : other.jsonStoreId.copy();
        this.isDataProcessed = other.isDataProcessed == null ? null : other.isDataProcessed.copy();
    }

    @Override
    public AprvlCompanyGstDetailsCriteria copy() {
        return new AprvlCompanyGstDetailsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(IntegerFilter ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public StringFilter getRecordid() {
        return recordid;
    }

    public void setRecordid(StringFilter recordid) {
        this.recordid = recordid;
    }

    public StringFilter getLineofbusiness() {
        return lineofbusiness;
    }

    public void setLineofbusiness(StringFilter lineofbusiness) {
        this.lineofbusiness = lineofbusiness;
    }

    public StringFilter getFilename() {
        return filename;
    }

    public void setFilename(StringFilter filename) {
        this.filename = filename;
    }

    public StringFilter getGstnumber() {
        return gstnumber;
    }

    public void setGstnumber(StringFilter gstnumber) {
        this.gstnumber = gstnumber;
    }

    public StringFilter getIssezgst() {
        return issezgst;
    }

    public void setIssezgst(StringFilter issezgst) {
        this.issezgst = issezgst;
    }

    public StringFilter getPreviousgst() {
        return previousgst;
    }

    public void setPreviousgst(StringFilter previousgst) {
        this.previousgst = previousgst;
    }

    public IntegerFilter getStatecode() {
        return statecode;
    }

    public void setStatecode(IntegerFilter statecode) {
        this.statecode = statecode;
    }

    public StringFilter getOperation() {
        return operation;
    }

    public void setOperation(StringFilter operation) {
        this.operation = operation;
    }

    public StringFilter getHdeleted() {
        return hdeleted;
    }

    public void setHdeleted(StringFilter hdeleted) {
        this.hdeleted = hdeleted;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(StringFilter ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public IntegerFilter getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(IntegerFilter jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public StringFilter getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(StringFilter isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AprvlCompanyGstDetailsCriteria that = (AprvlCompanyGstDetailsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(ctlgSrno, that.ctlgSrno) &&
            Objects.equals(recordid, that.recordid) &&
            Objects.equals(lineofbusiness, that.lineofbusiness) &&
            Objects.equals(filename, that.filename) &&
            Objects.equals(gstnumber, that.gstnumber) &&
            Objects.equals(issezgst, that.issezgst) &&
            Objects.equals(previousgst, that.previousgst) &&
            Objects.equals(statecode, that.statecode) &&
            Objects.equals(operation, that.operation) &&
            Objects.equals(hdeleted, that.hdeleted) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(ctlgIsDeleted, that.ctlgIsDeleted) &&
            Objects.equals(jsonStoreId, that.jsonStoreId) &&
            Objects.equals(isDataProcessed, that.isDataProcessed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        ctlgSrno,
        recordid,
        lineofbusiness,
        filename,
        gstnumber,
        issezgst,
        previousgst,
        statecode,
        operation,
        hdeleted,
        createdDate,
        lastModifiedDate,
        createdBy,
        lastModifiedBy,
        ctlgIsDeleted,
        jsonStoreId,
        isDataProcessed
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AprvlCompanyGstDetailsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (ctlgSrno != null ? "ctlgSrno=" + ctlgSrno + ", " : "") +
                (recordid != null ? "recordid=" + recordid + ", " : "") +
                (lineofbusiness != null ? "lineofbusiness=" + lineofbusiness + ", " : "") +
                (filename != null ? "filename=" + filename + ", " : "") +
                (gstnumber != null ? "gstnumber=" + gstnumber + ", " : "") +
                (issezgst != null ? "issezgst=" + issezgst + ", " : "") +
                (previousgst != null ? "previousgst=" + previousgst + ", " : "") +
                (statecode != null ? "statecode=" + statecode + ", " : "") +
                (operation != null ? "operation=" + operation + ", " : "") +
                (hdeleted != null ? "hdeleted=" + hdeleted + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (ctlgIsDeleted != null ? "ctlgIsDeleted=" + ctlgIsDeleted + ", " : "") +
                (jsonStoreId != null ? "jsonStoreId=" + jsonStoreId + ", " : "") +
                (isDataProcessed != null ? "isDataProcessed=" + isDataProcessed + ", " : "") +
            "}";
    }

}
