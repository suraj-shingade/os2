package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.OnesourceCompanyMasterHistory} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.OnesourceCompanyMasterHistoryResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /onesource-company-master-histories?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OnesourceCompanyMasterHistoryCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter companyCode;

    private StringFilter companyName;

    private IntegerFilter companyTypeId;

    private IntegerFilter sectorId;

    private IntegerFilter industryId;

    private IntegerFilter groupId;

    private StringFilter remarks;

    private StringFilter recordId;

    private IntegerFilter status;

    private StringFilter pin;

    private StringFilter clientStatus;

    private StringFilter displayCompanyName;

    private StringFilter billingType;

    private StringFilter isBillable;

    private StringFilter listingStatus;

    private InstantFilter dateOfIncooperation;

    private InstantFilter createdDate;

    private InstantFilter lastModifiedDate;

    private StringFilter createdBy;

    private StringFilter lastModifiedBy;

    private StringFilter isDeleted;

    private StringFilter originalFilename;

    private StringFilter displayFilename;

    public OnesourceCompanyMasterHistoryCriteria() {
    }

    public OnesourceCompanyMasterHistoryCriteria(OnesourceCompanyMasterHistoryCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.companyCode = other.companyCode == null ? null : other.companyCode.copy();
        this.companyName = other.companyName == null ? null : other.companyName.copy();
        this.companyTypeId = other.companyTypeId == null ? null : other.companyTypeId.copy();
        this.sectorId = other.sectorId == null ? null : other.sectorId.copy();
        this.industryId = other.industryId == null ? null : other.industryId.copy();
        this.groupId = other.groupId == null ? null : other.groupId.copy();
        this.remarks = other.remarks == null ? null : other.remarks.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.pin = other.pin == null ? null : other.pin.copy();
        this.clientStatus = other.clientStatus == null ? null : other.clientStatus.copy();
        this.displayCompanyName = other.displayCompanyName == null ? null : other.displayCompanyName.copy();
        this.billingType = other.billingType == null ? null : other.billingType.copy();
        this.isBillable = other.isBillable == null ? null : other.isBillable.copy();
        this.listingStatus = other.listingStatus == null ? null : other.listingStatus.copy();
        this.dateOfIncooperation = other.dateOfIncooperation == null ? null : other.dateOfIncooperation.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.originalFilename = other.originalFilename == null ? null : other.originalFilename.copy();
        this.displayFilename = other.displayFilename == null ? null : other.displayFilename.copy();
    }

    @Override
    public OnesourceCompanyMasterHistoryCriteria copy() {
        return new OnesourceCompanyMasterHistoryCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(StringFilter companyCode) {
        this.companyCode = companyCode;
    }

    public StringFilter getCompanyName() {
        return companyName;
    }

    public void setCompanyName(StringFilter companyName) {
        this.companyName = companyName;
    }

    public IntegerFilter getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(IntegerFilter companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public IntegerFilter getSectorId() {
        return sectorId;
    }

    public void setSectorId(IntegerFilter sectorId) {
        this.sectorId = sectorId;
    }

    public IntegerFilter getIndustryId() {
        return industryId;
    }

    public void setIndustryId(IntegerFilter industryId) {
        this.industryId = industryId;
    }

    public IntegerFilter getGroupId() {
        return groupId;
    }

    public void setGroupId(IntegerFilter groupId) {
        this.groupId = groupId;
    }

    public StringFilter getRemarks() {
        return remarks;
    }

    public void setRemarks(StringFilter remarks) {
        this.remarks = remarks;
    }

    public StringFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(StringFilter recordId) {
        this.recordId = recordId;
    }

    public IntegerFilter getStatus() {
        return status;
    }

    public void setStatus(IntegerFilter status) {
        this.status = status;
    }

    public StringFilter getPin() {
        return pin;
    }

    public void setPin(StringFilter pin) {
        this.pin = pin;
    }

    public StringFilter getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(StringFilter clientStatus) {
        this.clientStatus = clientStatus;
    }

    public StringFilter getDisplayCompanyName() {
        return displayCompanyName;
    }

    public void setDisplayCompanyName(StringFilter displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public StringFilter getBillingType() {
        return billingType;
    }

    public void setBillingType(StringFilter billingType) {
        this.billingType = billingType;
    }

    public StringFilter getIsBillable() {
        return isBillable;
    }

    public void setIsBillable(StringFilter isBillable) {
        this.isBillable = isBillable;
    }

    public StringFilter getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(StringFilter listingStatus) {
        this.listingStatus = listingStatus;
    }

    public InstantFilter getDateOfIncooperation() {
        return dateOfIncooperation;
    }

    public void setDateOfIncooperation(InstantFilter dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StringFilter getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(StringFilter originalFilename) {
        this.originalFilename = originalFilename;
    }

    public StringFilter getDisplayFilename() {
        return displayFilename;
    }

    public void setDisplayFilename(StringFilter displayFilename) {
        this.displayFilename = displayFilename;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OnesourceCompanyMasterHistoryCriteria that = (OnesourceCompanyMasterHistoryCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyCode, that.companyCode) &&
            Objects.equals(companyName, that.companyName) &&
            Objects.equals(companyTypeId, that.companyTypeId) &&
            Objects.equals(sectorId, that.sectorId) &&
            Objects.equals(industryId, that.industryId) &&
            Objects.equals(groupId, that.groupId) &&
            Objects.equals(remarks, that.remarks) &&
            Objects.equals(recordId, that.recordId) &&
            Objects.equals(status, that.status) &&
            Objects.equals(pin, that.pin) &&
            Objects.equals(clientStatus, that.clientStatus) &&
            Objects.equals(displayCompanyName, that.displayCompanyName) &&
            Objects.equals(billingType, that.billingType) &&
            Objects.equals(isBillable, that.isBillable) &&
            Objects.equals(listingStatus, that.listingStatus) &&
            Objects.equals(dateOfIncooperation, that.dateOfIncooperation) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(originalFilename, that.originalFilename) &&
            Objects.equals(displayFilename, that.displayFilename);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyCode,
        companyName,
        companyTypeId,
        sectorId,
        industryId,
        groupId,
        remarks,
        recordId,
        status,
        pin,
        clientStatus,
        displayCompanyName,
        billingType,
        isBillable,
        listingStatus,
        dateOfIncooperation,
        createdDate,
        lastModifiedDate,
        createdBy,
        lastModifiedBy,
        isDeleted,
        originalFilename,
        displayFilename
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OnesourceCompanyMasterHistoryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyCode != null ? "companyCode=" + companyCode + ", " : "") +
                (companyName != null ? "companyName=" + companyName + ", " : "") +
                (companyTypeId != null ? "companyTypeId=" + companyTypeId + ", " : "") +
                (sectorId != null ? "sectorId=" + sectorId + ", " : "") +
                (industryId != null ? "industryId=" + industryId + ", " : "") +
                (groupId != null ? "groupId=" + groupId + ", " : "") +
                (remarks != null ? "remarks=" + remarks + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (pin != null ? "pin=" + pin + ", " : "") +
                (clientStatus != null ? "clientStatus=" + clientStatus + ", " : "") +
                (displayCompanyName != null ? "displayCompanyName=" + displayCompanyName + ", " : "") +
                (billingType != null ? "billingType=" + billingType + ", " : "") +
                (isBillable != null ? "isBillable=" + isBillable + ", " : "") +
                (listingStatus != null ? "listingStatus=" + listingStatus + ", " : "") +
                (dateOfIncooperation != null ? "dateOfIncooperation=" + dateOfIncooperation + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (originalFilename != null ? "originalFilename=" + originalFilename + ", " : "") +
                (displayFilename != null ? "displayFilename=" + displayFilename + ", " : "") +
            "}";
    }

}
