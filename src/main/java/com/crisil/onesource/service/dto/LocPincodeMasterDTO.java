package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.LocPincodeMaster} entity.
 */
@ApiModel(description = "Location_Master_Table")
public class LocPincodeMasterDTO implements Serializable {
    
    private Long id;

    private Integer pincodeSrno;

    @Size(max = 255)
    private String officeName;

    @Size(max = 25)
    private String pincode;

    @Size(max = 255)
    private String officeType;

    @Size(max = 255)
    private String deliveryStatus;

    @Size(max = 255)
    private String divisionName;

    @Size(max = 255)
    private String regionName;

    @Size(max = 255)
    private String circleName;

    @Size(max = 255)
    private String taluka;

    @Size(max = 255)
    private String districtName;

    @Size(max = 255)
    private String stateName;

    @Size(max = 255)
    private String country;

    @Size(max = 1)
    private String isDeleted;

    private Instant createdDate;

    @Size(max = 100)
    private String createdBy;

    private Instant lastModifiedDate;

    @Size(max = 50)
    private String lastModifiedBy;

    @Size(max = 1)
    private String isApproved;

    private Integer seqOrder;

    @Size(max = 1)
    private String isDataProcessed;

    private Long jsonStoreId;

    @Size(max = 1)
    private String ctlgIsDeleted;

    @Size(max = 1)
    private String operation;

    @NotNull
    private Integer ctlgSrno;


    private Long stateIdId;

    private Long districtIdId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPincodeSrno() {
        return pincodeSrno;
    }

    public void setPincodeSrno(Integer pincodeSrno) {
        this.pincodeSrno = pincodeSrno;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getOfficeType() {
        return officeType;
    }

    public void setOfficeType(String officeType) {
        this.officeType = officeType;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getTaluka() {
        return taluka;
    }

    public void setTaluka(String taluka) {
        this.taluka = taluka;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(Integer seqOrder) {
        this.seqOrder = seqOrder;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public Long getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(Long jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public Long getStateIdId() {
        return stateIdId;
    }

    public void setStateIdId(Long locStateMasterId) {
        this.stateIdId = locStateMasterId;
    }

    public Long getDistrictIdId() {
        return districtIdId;
    }

    public void setDistrictIdId(Long locDistrictMasterId) {
        this.districtIdId = locDistrictMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LocPincodeMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((LocPincodeMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocPincodeMasterDTO{" +
            "id=" + getId() +
            ", pincodeSrno=" + getPincodeSrno() +
            ", officeName='" + getOfficeName() + "'" +
            ", pincode='" + getPincode() + "'" +
            ", officeType='" + getOfficeType() + "'" +
            ", deliveryStatus='" + getDeliveryStatus() + "'" +
            ", divisionName='" + getDivisionName() + "'" +
            ", regionName='" + getRegionName() + "'" +
            ", circleName='" + getCircleName() + "'" +
            ", taluka='" + getTaluka() + "'" +
            ", districtName='" + getDistrictName() + "'" +
            ", stateName='" + getStateName() + "'" +
            ", country='" + getCountry() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isApproved='" + getIsApproved() + "'" +
            ", seqOrder=" + getSeqOrder() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            ", jsonStoreId=" + getJsonStoreId() +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", operation='" + getOperation() + "'" +
            ", ctlgSrno=" + getCtlgSrno() +
            ", stateIdId=" + getStateIdId() +
            ", districtIdId=" + getDistrictIdId() +
            "}";
    }
}
