package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.IndustryMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.IndustryMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /industry-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class IndustryMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter industryId;

    private StringFilter industryName;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private StringFilter isDeleted;

    private LongFilter onesourceCompanyMasterId;

    public IndustryMasterCriteria() {
    }

    public IndustryMasterCriteria(IndustryMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.industryId = other.industryId == null ? null : other.industryId.copy();
        this.industryName = other.industryName == null ? null : other.industryName.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.onesourceCompanyMasterId = other.onesourceCompanyMasterId == null ? null : other.onesourceCompanyMasterId.copy();
    }

    @Override
    public IndustryMasterCriteria copy() {
        return new IndustryMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getIndustryId() {
        return industryId;
    }

    public void setIndustryId(IntegerFilter industryId) {
        this.industryId = industryId;
    }

    public StringFilter getIndustryName() {
        return industryName;
    }

    public void setIndustryName(StringFilter industryName) {
        this.industryName = industryName;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getOnesourceCompanyMasterId() {
        return onesourceCompanyMasterId;
    }

    public void setOnesourceCompanyMasterId(LongFilter onesourceCompanyMasterId) {
        this.onesourceCompanyMasterId = onesourceCompanyMasterId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final IndustryMasterCriteria that = (IndustryMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(industryId, that.industryId) &&
            Objects.equals(industryName, that.industryName) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(onesourceCompanyMasterId, that.onesourceCompanyMasterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        industryId,
        industryName,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        isDeleted,
        onesourceCompanyMasterId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IndustryMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (industryId != null ? "industryId=" + industryId + ", " : "") +
                (industryName != null ? "industryName=" + industryName + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (onesourceCompanyMasterId != null ? "onesourceCompanyMasterId=" + onesourceCompanyMasterId + ", " : "") +
            "}";
    }

}
