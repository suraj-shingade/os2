package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.AprvlContactMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.AprvlContactMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /aprvl-contact-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class AprvlContactMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter lastname;

    private StringFilter joinednewcompany;

    private IntegerFilter cityid;

    private IntegerFilter hdeleted;

    private StringFilter recordid;

    private StringFilter levelimportance;

    private StringFilter department;

    private StringFilter fax;

    private StringFilter email;

    private StringFilter keyperson;

    private StringFilter companycode;

    private StringFilter pincode;

    private StringFilter contactFullName;

    private StringFilter address1;

    private StringFilter cityname;

    private StringFilter countryname;

    private StringFilter mobile;

    private StringFilter statename;

    private LocalDateFilter modifyDate;

    private IntegerFilter ofaflag;

    private StringFilter firstname;

    private StringFilter middlename;

    private StringFilter removereason;

    private StringFilter salutation;

    private StringFilter designation;

    private StringFilter status;

    private IntegerFilter ctlgSrno;

    private StringFilter ctlgIsDeleted;

    private LongFilter jsonStoreId;

    private StringFilter isDataProcessed;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter operation;

    private StringFilter contactId;

    public AprvlContactMasterCriteria() {
    }

    public AprvlContactMasterCriteria(AprvlContactMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.lastname = other.lastname == null ? null : other.lastname.copy();
        this.joinednewcompany = other.joinednewcompany == null ? null : other.joinednewcompany.copy();
        this.cityid = other.cityid == null ? null : other.cityid.copy();
        this.hdeleted = other.hdeleted == null ? null : other.hdeleted.copy();
        this.recordid = other.recordid == null ? null : other.recordid.copy();
        this.levelimportance = other.levelimportance == null ? null : other.levelimportance.copy();
        this.department = other.department == null ? null : other.department.copy();
        this.fax = other.fax == null ? null : other.fax.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.keyperson = other.keyperson == null ? null : other.keyperson.copy();
        this.companycode = other.companycode == null ? null : other.companycode.copy();
        this.pincode = other.pincode == null ? null : other.pincode.copy();
        this.contactFullName = other.contactFullName == null ? null : other.contactFullName.copy();
        this.address1 = other.address1 == null ? null : other.address1.copy();
        this.cityname = other.cityname == null ? null : other.cityname.copy();
        this.countryname = other.countryname == null ? null : other.countryname.copy();
        this.mobile = other.mobile == null ? null : other.mobile.copy();
        this.statename = other.statename == null ? null : other.statename.copy();
        this.modifyDate = other.modifyDate == null ? null : other.modifyDate.copy();
        this.ofaflag = other.ofaflag == null ? null : other.ofaflag.copy();
        this.firstname = other.firstname == null ? null : other.firstname.copy();
        this.middlename = other.middlename == null ? null : other.middlename.copy();
        this.removereason = other.removereason == null ? null : other.removereason.copy();
        this.salutation = other.salutation == null ? null : other.salutation.copy();
        this.designation = other.designation == null ? null : other.designation.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.ctlgSrno = other.ctlgSrno == null ? null : other.ctlgSrno.copy();
        this.ctlgIsDeleted = other.ctlgIsDeleted == null ? null : other.ctlgIsDeleted.copy();
        this.jsonStoreId = other.jsonStoreId == null ? null : other.jsonStoreId.copy();
        this.isDataProcessed = other.isDataProcessed == null ? null : other.isDataProcessed.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.operation = other.operation == null ? null : other.operation.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public AprvlContactMasterCriteria copy() {
        return new AprvlContactMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLastname() {
        return lastname;
    }

    public void setLastname(StringFilter lastname) {
        this.lastname = lastname;
    }

    public StringFilter getJoinednewcompany() {
        return joinednewcompany;
    }

    public void setJoinednewcompany(StringFilter joinednewcompany) {
        this.joinednewcompany = joinednewcompany;
    }

    public IntegerFilter getCityid() {
        return cityid;
    }

    public void setCityid(IntegerFilter cityid) {
        this.cityid = cityid;
    }

    public IntegerFilter getHdeleted() {
        return hdeleted;
    }

    public void setHdeleted(IntegerFilter hdeleted) {
        this.hdeleted = hdeleted;
    }

    public StringFilter getRecordid() {
        return recordid;
    }

    public void setRecordid(StringFilter recordid) {
        this.recordid = recordid;
    }

    public StringFilter getLevelimportance() {
        return levelimportance;
    }

    public void setLevelimportance(StringFilter levelimportance) {
        this.levelimportance = levelimportance;
    }

    public StringFilter getDepartment() {
        return department;
    }

    public void setDepartment(StringFilter department) {
        this.department = department;
    }

    public StringFilter getFax() {
        return fax;
    }

    public void setFax(StringFilter fax) {
        this.fax = fax;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getKeyperson() {
        return keyperson;
    }

    public void setKeyperson(StringFilter keyperson) {
        this.keyperson = keyperson;
    }

    public StringFilter getCompanycode() {
        return companycode;
    }

    public void setCompanycode(StringFilter companycode) {
        this.companycode = companycode;
    }

    public StringFilter getPincode() {
        return pincode;
    }

    public void setPincode(StringFilter pincode) {
        this.pincode = pincode;
    }

    public StringFilter getContactFullName() {
        return contactFullName;
    }

    public void setContactFullName(StringFilter contactFullName) {
        this.contactFullName = contactFullName;
    }

    public StringFilter getAddress1() {
        return address1;
    }

    public void setAddress1(StringFilter address1) {
        this.address1 = address1;
    }

    public StringFilter getCityname() {
        return cityname;
    }

    public void setCityname(StringFilter cityname) {
        this.cityname = cityname;
    }

    public StringFilter getCountryname() {
        return countryname;
    }

    public void setCountryname(StringFilter countryname) {
        this.countryname = countryname;
    }

    public StringFilter getMobile() {
        return mobile;
    }

    public void setMobile(StringFilter mobile) {
        this.mobile = mobile;
    }

    public StringFilter getStatename() {
        return statename;
    }

    public void setStatename(StringFilter statename) {
        this.statename = statename;
    }

    public LocalDateFilter getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDateFilter modifyDate) {
        this.modifyDate = modifyDate;
    }

    public IntegerFilter getOfaflag() {
        return ofaflag;
    }

    public void setOfaflag(IntegerFilter ofaflag) {
        this.ofaflag = ofaflag;
    }

    public StringFilter getFirstname() {
        return firstname;
    }

    public void setFirstname(StringFilter firstname) {
        this.firstname = firstname;
    }

    public StringFilter getMiddlename() {
        return middlename;
    }

    public void setMiddlename(StringFilter middlename) {
        this.middlename = middlename;
    }

    public StringFilter getRemovereason() {
        return removereason;
    }

    public void setRemovereason(StringFilter removereason) {
        this.removereason = removereason;
    }

    public StringFilter getSalutation() {
        return salutation;
    }

    public void setSalutation(StringFilter salutation) {
        this.salutation = salutation;
    }

    public StringFilter getDesignation() {
        return designation;
    }

    public void setDesignation(StringFilter designation) {
        this.designation = designation;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public IntegerFilter getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(IntegerFilter ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public StringFilter getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(StringFilter ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public LongFilter getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(LongFilter jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public StringFilter getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(StringFilter isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getOperation() {
        return operation;
    }

    public void setOperation(StringFilter operation) {
        this.operation = operation;
    }

    public StringFilter getContactId() {
        return contactId;
    }

    public void setContactId(StringFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AprvlContactMasterCriteria that = (AprvlContactMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(lastname, that.lastname) &&
            Objects.equals(joinednewcompany, that.joinednewcompany) &&
            Objects.equals(cityid, that.cityid) &&
            Objects.equals(hdeleted, that.hdeleted) &&
            Objects.equals(recordid, that.recordid) &&
            Objects.equals(levelimportance, that.levelimportance) &&
            Objects.equals(department, that.department) &&
            Objects.equals(fax, that.fax) &&
            Objects.equals(email, that.email) &&
            Objects.equals(keyperson, that.keyperson) &&
            Objects.equals(companycode, that.companycode) &&
            Objects.equals(pincode, that.pincode) &&
            Objects.equals(contactFullName, that.contactFullName) &&
            Objects.equals(address1, that.address1) &&
            Objects.equals(cityname, that.cityname) &&
            Objects.equals(countryname, that.countryname) &&
            Objects.equals(mobile, that.mobile) &&
            Objects.equals(statename, that.statename) &&
            Objects.equals(modifyDate, that.modifyDate) &&
            Objects.equals(ofaflag, that.ofaflag) &&
            Objects.equals(firstname, that.firstname) &&
            Objects.equals(middlename, that.middlename) &&
            Objects.equals(removereason, that.removereason) &&
            Objects.equals(salutation, that.salutation) &&
            Objects.equals(designation, that.designation) &&
            Objects.equals(status, that.status) &&
            Objects.equals(ctlgSrno, that.ctlgSrno) &&
            Objects.equals(ctlgIsDeleted, that.ctlgIsDeleted) &&
            Objects.equals(jsonStoreId, that.jsonStoreId) &&
            Objects.equals(isDataProcessed, that.isDataProcessed) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(operation, that.operation) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        lastname,
        joinednewcompany,
        cityid,
        hdeleted,
        recordid,
        levelimportance,
        department,
        fax,
        email,
        keyperson,
        companycode,
        pincode,
        contactFullName,
        address1,
        cityname,
        countryname,
        mobile,
        statename,
        modifyDate,
        ofaflag,
        firstname,
        middlename,
        removereason,
        salutation,
        designation,
        status,
        ctlgSrno,
        ctlgIsDeleted,
        jsonStoreId,
        isDataProcessed,
        lastModifiedDate,
        lastModifiedBy,
        createdBy,
        createdDate,
        operation,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AprvlContactMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (lastname != null ? "lastname=" + lastname + ", " : "") +
                (joinednewcompany != null ? "joinednewcompany=" + joinednewcompany + ", " : "") +
                (cityid != null ? "cityid=" + cityid + ", " : "") +
                (hdeleted != null ? "hdeleted=" + hdeleted + ", " : "") +
                (recordid != null ? "recordid=" + recordid + ", " : "") +
                (levelimportance != null ? "levelimportance=" + levelimportance + ", " : "") +
                (department != null ? "department=" + department + ", " : "") +
                (fax != null ? "fax=" + fax + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (keyperson != null ? "keyperson=" + keyperson + ", " : "") +
                (companycode != null ? "companycode=" + companycode + ", " : "") +
                (pincode != null ? "pincode=" + pincode + ", " : "") +
                (contactFullName != null ? "contactFullName=" + contactFullName + ", " : "") +
                (address1 != null ? "address1=" + address1 + ", " : "") +
                (cityname != null ? "cityname=" + cityname + ", " : "") +
                (countryname != null ? "countryname=" + countryname + ", " : "") +
                (mobile != null ? "mobile=" + mobile + ", " : "") +
                (statename != null ? "statename=" + statename + ", " : "") +
                (modifyDate != null ? "modifyDate=" + modifyDate + ", " : "") +
                (ofaflag != null ? "ofaflag=" + ofaflag + ", " : "") +
                (firstname != null ? "firstname=" + firstname + ", " : "") +
                (middlename != null ? "middlename=" + middlename + ", " : "") +
                (removereason != null ? "removereason=" + removereason + ", " : "") +
                (salutation != null ? "salutation=" + salutation + ", " : "") +
                (designation != null ? "designation=" + designation + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (ctlgSrno != null ? "ctlgSrno=" + ctlgSrno + ", " : "") +
                (ctlgIsDeleted != null ? "ctlgIsDeleted=" + ctlgIsDeleted + ", " : "") +
                (jsonStoreId != null ? "jsonStoreId=" + jsonStoreId + ", " : "") +
                (isDataProcessed != null ? "isDataProcessed=" + isDataProcessed + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (operation != null ? "operation=" + operation + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}
