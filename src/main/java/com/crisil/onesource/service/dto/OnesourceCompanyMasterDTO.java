package com.crisil.onesource.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.OnesourceCompanyMaster} entity.
 */
public class OnesourceCompanyMasterDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 50)
    private String companyCode;

    @Size(max = 100)
    private String companyName;

    @Size(max = 100)
    private String remarks;

    @Size(max = 100)
    private String recordId;

    private Integer status;

    @Size(max = 50)
    private String clientStatus;

    @Size(max = 100)
    private String displayCompanyName;

    @Size(max = 1)
    private String billingType;

    @Size(max = 1)
    private String isBillable;

    @Size(max = 1)
    private String listingStatus;

    private Instant dateOfIncooperation;

    private Instant createdDate;

    private Instant lastModifiedDate;

    @Size(max = 100)
    private String createdBy;

    @Size(max = 100)
    private String lastModifiedBy;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 50)
    private String originalFilename;

    @Size(max = 500)
    private String displayFilename;

    @Size(max = 10)
    private String companyStatus;


    private Long companyTypeIdId;

    private Long sectorIdId;

    private Long industryIdId;

    private Long groupIdId;

    private Long pinId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(String clientStatus) {
        this.clientStatus = clientStatus;
    }

    public String getDisplayCompanyName() {
        return displayCompanyName;
    }

    public void setDisplayCompanyName(String displayCompanyName) {
        this.displayCompanyName = displayCompanyName;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getIsBillable() {
        return isBillable;
    }

    public void setIsBillable(String isBillable) {
        this.isBillable = isBillable;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public Instant getDateOfIncooperation() {
        return dateOfIncooperation;
    }

    public void setDateOfIncooperation(Instant dateOfIncooperation) {
        this.dateOfIncooperation = dateOfIncooperation;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getDisplayFilename() {
        return displayFilename;
    }

    public void setDisplayFilename(String displayFilename) {
        this.displayFilename = displayFilename;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public Long getCompanyTypeIdId() {
        return companyTypeIdId;
    }

    public void setCompanyTypeIdId(Long companyTypeMasterId) {
        this.companyTypeIdId = companyTypeMasterId;
    }

    public Long getSectorIdId() {
        return sectorIdId;
    }

    public void setSectorIdId(Long companySectorMasterId) {
        this.sectorIdId = companySectorMasterId;
    }

    public Long getIndustryIdId() {
        return industryIdId;
    }

    public void setIndustryIdId(Long industryMasterId) {
        this.industryIdId = industryMasterId;
    }

    public Long getGroupIdId() {
        return groupIdId;
    }

    public void setGroupIdId(Long groupMasterId) {
        this.groupIdId = groupMasterId;
    }

    public Long getPinId() {
        return pinId;
    }

    public void setPinId(Long locPincodeMasterId) {
        this.pinId = locPincodeMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OnesourceCompanyMasterDTO)) {
            return false;
        }

        return id != null && id.equals(((OnesourceCompanyMasterDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OnesourceCompanyMasterDTO{" +
            "id=" + getId() +
            ", companyCode='" + getCompanyCode() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            ", remarks='" + getRemarks() + "'" +
            ", recordId='" + getRecordId() + "'" +
            ", status=" + getStatus() +
            ", clientStatus='" + getClientStatus() + "'" +
            ", displayCompanyName='" + getDisplayCompanyName() + "'" +
            ", billingType='" + getBillingType() + "'" +
            ", isBillable='" + getIsBillable() + "'" +
            ", listingStatus='" + getListingStatus() + "'" +
            ", dateOfIncooperation='" + getDateOfIncooperation() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", originalFilename='" + getOriginalFilename() + "'" +
            ", displayFilename='" + getDisplayFilename() + "'" +
            ", companyStatus='" + getCompanyStatus() + "'" +
            ", companyTypeIdId=" + getCompanyTypeIdId() +
            ", sectorIdId=" + getSectorIdId() +
            ", industryIdId=" + getIndustryIdId() +
            ", groupIdId=" + getGroupIdId() +
            ", pinId=" + getPinId() +
            "}";
    }
}
