package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyChangeRequest} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyChangeRequestResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-change-requests?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyChangeRequestCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter oldCompanyName;

    private StringFilter newCompanyName;

    private StringFilter newCompanyCode;

    private StringFilter status;

    private LocalDateFilter effectiveDate;

    private StringFilter requestedBy;

    private StringFilter companyChangeId;

    private StringFilter caseType;

    private StringFilter reason;

    private StringFilter reasonDescription;

    private StringFilter createdBy;

    private LocalDateFilter createdDate;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private LongFilter oldCompanyCodeId;

    public CompanyChangeRequestCriteria() {
    }

    public CompanyChangeRequestCriteria(CompanyChangeRequestCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.oldCompanyName = other.oldCompanyName == null ? null : other.oldCompanyName.copy();
        this.newCompanyName = other.newCompanyName == null ? null : other.newCompanyName.copy();
        this.newCompanyCode = other.newCompanyCode == null ? null : other.newCompanyCode.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.effectiveDate = other.effectiveDate == null ? null : other.effectiveDate.copy();
        this.requestedBy = other.requestedBy == null ? null : other.requestedBy.copy();
        this.companyChangeId = other.companyChangeId == null ? null : other.companyChangeId.copy();
        this.caseType = other.caseType == null ? null : other.caseType.copy();
        this.reason = other.reason == null ? null : other.reason.copy();
        this.reasonDescription = other.reasonDescription == null ? null : other.reasonDescription.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.oldCompanyCodeId = other.oldCompanyCodeId == null ? null : other.oldCompanyCodeId.copy();
    }

    @Override
    public CompanyChangeRequestCriteria copy() {
        return new CompanyChangeRequestCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getOldCompanyName() {
        return oldCompanyName;
    }

    public void setOldCompanyName(StringFilter oldCompanyName) {
        this.oldCompanyName = oldCompanyName;
    }

    public StringFilter getNewCompanyName() {
        return newCompanyName;
    }

    public void setNewCompanyName(StringFilter newCompanyName) {
        this.newCompanyName = newCompanyName;
    }

    public StringFilter getNewCompanyCode() {
        return newCompanyCode;
    }

    public void setNewCompanyCode(StringFilter newCompanyCode) {
        this.newCompanyCode = newCompanyCode;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public LocalDateFilter getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(LocalDateFilter effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public StringFilter getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(StringFilter requestedBy) {
        this.requestedBy = requestedBy;
    }

    public StringFilter getCompanyChangeId() {
        return companyChangeId;
    }

    public void setCompanyChangeId(StringFilter companyChangeId) {
        this.companyChangeId = companyChangeId;
    }

    public StringFilter getCaseType() {
        return caseType;
    }

    public void setCaseType(StringFilter caseType) {
        this.caseType = caseType;
    }

    public StringFilter getReason() {
        return reason;
    }

    public void setReason(StringFilter reason) {
        this.reason = reason;
    }

    public StringFilter getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(StringFilter reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getOldCompanyCodeId() {
        return oldCompanyCodeId;
    }

    public void setOldCompanyCodeId(LongFilter oldCompanyCodeId) {
        this.oldCompanyCodeId = oldCompanyCodeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyChangeRequestCriteria that = (CompanyChangeRequestCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(oldCompanyName, that.oldCompanyName) &&
            Objects.equals(newCompanyName, that.newCompanyName) &&
            Objects.equals(newCompanyCode, that.newCompanyCode) &&
            Objects.equals(status, that.status) &&
            Objects.equals(effectiveDate, that.effectiveDate) &&
            Objects.equals(requestedBy, that.requestedBy) &&
            Objects.equals(companyChangeId, that.companyChangeId) &&
            Objects.equals(caseType, that.caseType) &&
            Objects.equals(reason, that.reason) &&
            Objects.equals(reasonDescription, that.reasonDescription) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(oldCompanyCodeId, that.oldCompanyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        oldCompanyName,
        newCompanyName,
        newCompanyCode,
        status,
        effectiveDate,
        requestedBy,
        companyChangeId,
        caseType,
        reason,
        reasonDescription,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        oldCompanyCodeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyChangeRequestCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (oldCompanyName != null ? "oldCompanyName=" + oldCompanyName + ", " : "") +
                (newCompanyName != null ? "newCompanyName=" + newCompanyName + ", " : "") +
                (newCompanyCode != null ? "newCompanyCode=" + newCompanyCode + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (effectiveDate != null ? "effectiveDate=" + effectiveDate + ", " : "") +
                (requestedBy != null ? "requestedBy=" + requestedBy + ", " : "") +
                (companyChangeId != null ? "companyChangeId=" + companyChangeId + ", " : "") +
                (caseType != null ? "caseType=" + caseType + ", " : "") +
                (reason != null ? "reason=" + reason + ", " : "") +
                (reasonDescription != null ? "reasonDescription=" + reasonDescription + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (oldCompanyCodeId != null ? "oldCompanyCodeId=" + oldCompanyCodeId + ", " : "") +
            "}";
    }

}
