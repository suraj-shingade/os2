package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.LocDistrictMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.LocDistrictMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /loc-district-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LocDistrictMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter districtName;

    private StringFilter description;

    private IntegerFilter districtId;

    private StringFilter isDeleted;

    private InstantFilter createdDate;

    private StringFilter createdBy;

    private InstantFilter lastModifiedDate;

    private StringFilter lastModifiedBy;

    private StringFilter isApproved;

    private IntegerFilter seqOrder;

    private LongFilter locCityMasterId;

    private LongFilter locPincodeMasterId;

    private LongFilter stateIdId;

    public LocDistrictMasterCriteria() {
    }

    public LocDistrictMasterCriteria(LocDistrictMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.districtName = other.districtName == null ? null : other.districtName.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.districtId = other.districtId == null ? null : other.districtId.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.isApproved = other.isApproved == null ? null : other.isApproved.copy();
        this.seqOrder = other.seqOrder == null ? null : other.seqOrder.copy();
        this.locCityMasterId = other.locCityMasterId == null ? null : other.locCityMasterId.copy();
        this.locPincodeMasterId = other.locPincodeMasterId == null ? null : other.locPincodeMasterId.copy();
        this.stateIdId = other.stateIdId == null ? null : other.stateIdId.copy();
    }

    @Override
    public LocDistrictMasterCriteria copy() {
        return new LocDistrictMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDistrictName() {
        return districtName;
    }

    public void setDistrictName(StringFilter districtName) {
        this.districtName = districtName;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getDistrictId() {
        return districtId;
    }

    public void setDistrictId(IntegerFilter districtId) {
        this.districtId = districtId;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(StringFilter isApproved) {
        this.isApproved = isApproved;
    }

    public IntegerFilter getSeqOrder() {
        return seqOrder;
    }

    public void setSeqOrder(IntegerFilter seqOrder) {
        this.seqOrder = seqOrder;
    }

    public LongFilter getLocCityMasterId() {
        return locCityMasterId;
    }

    public void setLocCityMasterId(LongFilter locCityMasterId) {
        this.locCityMasterId = locCityMasterId;
    }

    public LongFilter getLocPincodeMasterId() {
        return locPincodeMasterId;
    }

    public void setLocPincodeMasterId(LongFilter locPincodeMasterId) {
        this.locPincodeMasterId = locPincodeMasterId;
    }

    public LongFilter getStateIdId() {
        return stateIdId;
    }

    public void setStateIdId(LongFilter stateIdId) {
        this.stateIdId = stateIdId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LocDistrictMasterCriteria that = (LocDistrictMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(districtName, that.districtName) &&
            Objects.equals(description, that.description) &&
            Objects.equals(districtId, that.districtId) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(isApproved, that.isApproved) &&
            Objects.equals(seqOrder, that.seqOrder) &&
            Objects.equals(locCityMasterId, that.locCityMasterId) &&
            Objects.equals(locPincodeMasterId, that.locPincodeMasterId) &&
            Objects.equals(stateIdId, that.stateIdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        districtName,
        description,
        districtId,
        isDeleted,
        createdDate,
        createdBy,
        lastModifiedDate,
        lastModifiedBy,
        isApproved,
        seqOrder,
        locCityMasterId,
        locPincodeMasterId,
        stateIdId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LocDistrictMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (districtName != null ? "districtName=" + districtName + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (districtId != null ? "districtId=" + districtId + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (isApproved != null ? "isApproved=" + isApproved + ", " : "") +
                (seqOrder != null ? "seqOrder=" + seqOrder + ", " : "") +
                (locCityMasterId != null ? "locCityMasterId=" + locCityMasterId + ", " : "") +
                (locPincodeMasterId != null ? "locPincodeMasterId=" + locPincodeMasterId + ", " : "") +
                (stateIdId != null ? "stateIdId=" + stateIdId + ", " : "") +
            "}";
    }

}
