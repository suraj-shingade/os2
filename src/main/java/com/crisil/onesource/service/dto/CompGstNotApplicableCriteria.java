package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompGstNotApplicable} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompGstNotApplicableResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /comp-gst-not-applicables?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompGstNotApplicableCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter requestStatus;

    private StringFilter requesterComments;

    private StringFilter requestedBy;

    private InstantFilter requestedDate;

    private StringFilter approverComments;

    private StringFilter approvedBy;

    private InstantFilter approvedDate;

    private StringFilter gstNaFile;

    private StringFilter isReverseRequest;

    private LocalDateFilter createdDate;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private LongFilter contactIdId;

    public CompGstNotApplicableCriteria() {
    }

    public CompGstNotApplicableCriteria(CompGstNotApplicableCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.requestStatus = other.requestStatus == null ? null : other.requestStatus.copy();
        this.requesterComments = other.requesterComments == null ? null : other.requesterComments.copy();
        this.requestedBy = other.requestedBy == null ? null : other.requestedBy.copy();
        this.requestedDate = other.requestedDate == null ? null : other.requestedDate.copy();
        this.approverComments = other.approverComments == null ? null : other.approverComments.copy();
        this.approvedBy = other.approvedBy == null ? null : other.approvedBy.copy();
        this.approvedDate = other.approvedDate == null ? null : other.approvedDate.copy();
        this.gstNaFile = other.gstNaFile == null ? null : other.gstNaFile.copy();
        this.isReverseRequest = other.isReverseRequest == null ? null : other.isReverseRequest.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.contactIdId = other.contactIdId == null ? null : other.contactIdId.copy();
    }

    @Override
    public CompGstNotApplicableCriteria copy() {
        return new CompGstNotApplicableCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(StringFilter requestStatus) {
        this.requestStatus = requestStatus;
    }

    public StringFilter getRequesterComments() {
        return requesterComments;
    }

    public void setRequesterComments(StringFilter requesterComments) {
        this.requesterComments = requesterComments;
    }

    public StringFilter getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(StringFilter requestedBy) {
        this.requestedBy = requestedBy;
    }

    public InstantFilter getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(InstantFilter requestedDate) {
        this.requestedDate = requestedDate;
    }

    public StringFilter getApproverComments() {
        return approverComments;
    }

    public void setApproverComments(StringFilter approverComments) {
        this.approverComments = approverComments;
    }

    public StringFilter getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(StringFilter approvedBy) {
        this.approvedBy = approvedBy;
    }

    public InstantFilter getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(InstantFilter approvedDate) {
        this.approvedDate = approvedDate;
    }

    public StringFilter getGstNaFile() {
        return gstNaFile;
    }

    public void setGstNaFile(StringFilter gstNaFile) {
        this.gstNaFile = gstNaFile;
    }

    public StringFilter getIsReverseRequest() {
        return isReverseRequest;
    }

    public void setIsReverseRequest(StringFilter isReverseRequest) {
        this.isReverseRequest = isReverseRequest;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getContactIdId() {
        return contactIdId;
    }

    public void setContactIdId(LongFilter contactIdId) {
        this.contactIdId = contactIdId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompGstNotApplicableCriteria that = (CompGstNotApplicableCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(requestStatus, that.requestStatus) &&
            Objects.equals(requesterComments, that.requesterComments) &&
            Objects.equals(requestedBy, that.requestedBy) &&
            Objects.equals(requestedDate, that.requestedDate) &&
            Objects.equals(approverComments, that.approverComments) &&
            Objects.equals(approvedBy, that.approvedBy) &&
            Objects.equals(approvedDate, that.approvedDate) &&
            Objects.equals(gstNaFile, that.gstNaFile) &&
            Objects.equals(isReverseRequest, that.isReverseRequest) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(contactIdId, that.contactIdId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        requestStatus,
        requesterComments,
        requestedBy,
        requestedDate,
        approverComments,
        approvedBy,
        approvedDate,
        gstNaFile,
        isReverseRequest,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        contactIdId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompGstNotApplicableCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (requestStatus != null ? "requestStatus=" + requestStatus + ", " : "") +
                (requesterComments != null ? "requesterComments=" + requesterComments + ", " : "") +
                (requestedBy != null ? "requestedBy=" + requestedBy + ", " : "") +
                (requestedDate != null ? "requestedDate=" + requestedDate + ", " : "") +
                (approverComments != null ? "approverComments=" + approverComments + ", " : "") +
                (approvedBy != null ? "approvedBy=" + approvedBy + ", " : "") +
                (approvedDate != null ? "approvedDate=" + approvedDate + ", " : "") +
                (gstNaFile != null ? "gstNaFile=" + gstNaFile + ", " : "") +
                (isReverseRequest != null ? "isReverseRequest=" + isReverseRequest + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (contactIdId != null ? "contactIdId=" + contactIdId + ", " : "") +
            "}";
    }

}
