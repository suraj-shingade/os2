package com.crisil.onesource.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompanyListingStatus} entity.
 */
@ApiModel(description = "Approval Table for company_listing_status#SRC_OLD_ONESOURCE_5#EVT_COMPANY_LISTING_STATUS_151")
public class CompanyListingStatusDTO implements Serializable {
    
    private Long id;

    private Integer listingId;

    @Size(max = 50)
    private String scriptCode;

    @Size(max = 50)
    private String exchange;

    @Size(max = 50)
    private String status;

    private LocalDate statusDate;

    @Size(max = 50)
    private String createdBy;

    private LocalDate createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;

    @Size(max = 50)
    private String operation;

    @NotNull
    private Integer ctlgSrno;

    private Integer jsonStoreId;

    @Size(max = 1)
    private String isDataProcessed;

    @NotNull
    @Size(max = 1)
    private String ctlgIsDeleted;


    private Long companyCodeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getListingId() {
        return listingId;
    }

    public void setListingId(Integer listingId) {
        this.listingId = listingId;
    }

    public String getScriptCode() {
        return scriptCode;
    }

    public void setScriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDate getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(LocalDate statusDate) {
        this.statusDate = statusDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Integer getCtlgSrno() {
        return ctlgSrno;
    }

    public void setCtlgSrno(Integer ctlgSrno) {
        this.ctlgSrno = ctlgSrno;
    }

    public Integer getJsonStoreId() {
        return jsonStoreId;
    }

    public void setJsonStoreId(Integer jsonStoreId) {
        this.jsonStoreId = jsonStoreId;
    }

    public String getIsDataProcessed() {
        return isDataProcessed;
    }

    public void setIsDataProcessed(String isDataProcessed) {
        this.isDataProcessed = isDataProcessed;
    }

    public String getCtlgIsDeleted() {
        return ctlgIsDeleted;
    }

    public void setCtlgIsDeleted(String ctlgIsDeleted) {
        this.ctlgIsDeleted = ctlgIsDeleted;
    }

    public Long getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(Long onesourceCompanyMasterId) {
        this.companyCodeId = onesourceCompanyMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyListingStatusDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyListingStatusDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyListingStatusDTO{" +
            "id=" + getId() +
            ", listingId=" + getListingId() +
            ", scriptCode='" + getScriptCode() + "'" +
            ", exchange='" + getExchange() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusDate='" + getStatusDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", operation='" + getOperation() + "'" +
            ", ctlgSrno=" + getCtlgSrno() +
            ", jsonStoreId=" + getJsonStoreId() +
            ", isDataProcessed='" + getIsDataProcessed() + "'" +
            ", ctlgIsDeleted='" + getCtlgIsDeleted() + "'" +
            ", companyCodeId=" + getCompanyCodeId() +
            "}";
    }
}
