package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyIsinMapping} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyIsinMappingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-isin-mappings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyIsinMappingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter isinNo;

    private StringFilter instrumentType;

    private StringFilter instrumentSubType;

    private IntegerFilter faceValue;

    private StringFilter status;

    private StringFilter scriptCode;

    private StringFilter scriptSource;

    private StringFilter createdBy;

    private LocalDateFilter createdDate;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private LongFilter companyCodeId;

    public CompanyIsinMappingCriteria() {
    }

    public CompanyIsinMappingCriteria(CompanyIsinMappingCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.isinNo = other.isinNo == null ? null : other.isinNo.copy();
        this.instrumentType = other.instrumentType == null ? null : other.instrumentType.copy();
        this.instrumentSubType = other.instrumentSubType == null ? null : other.instrumentSubType.copy();
        this.faceValue = other.faceValue == null ? null : other.faceValue.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.scriptCode = other.scriptCode == null ? null : other.scriptCode.copy();
        this.scriptSource = other.scriptSource == null ? null : other.scriptSource.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.companyCodeId = other.companyCodeId == null ? null : other.companyCodeId.copy();
    }

    @Override
    public CompanyIsinMappingCriteria copy() {
        return new CompanyIsinMappingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getIsinNo() {
        return isinNo;
    }

    public void setIsinNo(StringFilter isinNo) {
        this.isinNo = isinNo;
    }

    public StringFilter getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(StringFilter instrumentType) {
        this.instrumentType = instrumentType;
    }

    public StringFilter getInstrumentSubType() {
        return instrumentSubType;
    }

    public void setInstrumentSubType(StringFilter instrumentSubType) {
        this.instrumentSubType = instrumentSubType;
    }

    public IntegerFilter getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(IntegerFilter faceValue) {
        this.faceValue = faceValue;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getScriptCode() {
        return scriptCode;
    }

    public void setScriptCode(StringFilter scriptCode) {
        this.scriptCode = scriptCode;
    }

    public StringFilter getScriptSource() {
        return scriptSource;
    }

    public void setScriptSource(StringFilter scriptSource) {
        this.scriptSource = scriptSource;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public LongFilter getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(LongFilter companyCodeId) {
        this.companyCodeId = companyCodeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyIsinMappingCriteria that = (CompanyIsinMappingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(isinNo, that.isinNo) &&
            Objects.equals(instrumentType, that.instrumentType) &&
            Objects.equals(instrumentSubType, that.instrumentSubType) &&
            Objects.equals(faceValue, that.faceValue) &&
            Objects.equals(status, that.status) &&
            Objects.equals(scriptCode, that.scriptCode) &&
            Objects.equals(scriptSource, that.scriptSource) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(companyCodeId, that.companyCodeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        isinNo,
        instrumentType,
        instrumentSubType,
        faceValue,
        status,
        scriptCode,
        scriptSource,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        companyCodeId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyIsinMappingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (isinNo != null ? "isinNo=" + isinNo + ", " : "") +
                (instrumentType != null ? "instrumentType=" + instrumentType + ", " : "") +
                (instrumentSubType != null ? "instrumentSubType=" + instrumentSubType + ", " : "") +
                (faceValue != null ? "faceValue=" + faceValue + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (scriptCode != null ? "scriptCode=" + scriptCode + ", " : "") +
                (scriptSource != null ? "scriptSource=" + scriptSource + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (companyCodeId != null ? "companyCodeId=" + companyCodeId + ", " : "") +
            "}";
    }

}
