package com.crisil.onesource.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.crisil.onesource.domain.CompanyIsinMapping} entity.
 */
public class CompanyIsinMappingDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 50)
    private String isinNo;

    @Size(max = 10)
    private String instrumentType;

    @Size(max = 50)
    private String instrumentSubType;

    private Integer faceValue;

    @Size(max = 20)
    private String status;

    @Size(max = 10)
    private String scriptCode;

    @Size(max = 5)
    private String scriptSource;

    @Size(max = 50)
    private String createdBy;

    private LocalDate createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private LocalDate lastModifiedDate;

    @Size(max = 1)
    private String isDeleted;


    private Long companyCodeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsinNo() {
        return isinNo;
    }

    public void setIsinNo(String isinNo) {
        this.isinNo = isinNo;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getInstrumentSubType() {
        return instrumentSubType;
    }

    public void setInstrumentSubType(String instrumentSubType) {
        this.instrumentSubType = instrumentSubType;
    }

    public Integer getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(Integer faceValue) {
        this.faceValue = faceValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getScriptCode() {
        return scriptCode;
    }

    public void setScriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
    }

    public String getScriptSource() {
        return scriptSource;
    }

    public void setScriptSource(String scriptSource) {
        this.scriptSource = scriptSource;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDate getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDate lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getCompanyCodeId() {
        return companyCodeId;
    }

    public void setCompanyCodeId(Long onesourceCompanyMasterId) {
        this.companyCodeId = onesourceCompanyMasterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompanyIsinMappingDTO)) {
            return false;
        }

        return id != null && id.equals(((CompanyIsinMappingDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyIsinMappingDTO{" +
            "id=" + getId() +
            ", isinNo='" + getIsinNo() + "'" +
            ", instrumentType='" + getInstrumentType() + "'" +
            ", instrumentSubType='" + getInstrumentSubType() + "'" +
            ", faceValue=" + getFaceValue() +
            ", status='" + getStatus() + "'" +
            ", scriptCode='" + getScriptCode() + "'" +
            ", scriptSource='" + getScriptSource() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", isDeleted='" + getIsDeleted() + "'" +
            ", companyCodeId=" + getCompanyCodeId() +
            "}";
    }
}
