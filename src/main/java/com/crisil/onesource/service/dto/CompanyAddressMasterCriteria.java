package com.crisil.onesource.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link com.crisil.onesource.domain.CompanyAddressMaster} entity. This class is used
 * in {@link com.crisil.onesource.web.rest.CompanyAddressMasterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /company-address-masters?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyAddressMasterCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter companyCode;

    private IntegerFilter addressTypeId;

    private StringFilter address1;

    private StringFilter address2;

    private StringFilter address3;

    private IntegerFilter cityId;

    private StringFilter phoneNo;

    private StringFilter mobileNo;

    private StringFilter fax;

    private StringFilter email;

    private StringFilter webpage;

    private StringFilter createdBy;

    private LocalDateFilter createdDate;

    private StringFilter lastModifiedBy;

    private LocalDateFilter lastModifiedDate;

    private StringFilter isDeleted;

    private IntegerFilter companyAddressId;

    private StringFilter addressSource;

    private LongFilter pincodeId;

    private LongFilter recordId;

    public CompanyAddressMasterCriteria() {
    }

    public CompanyAddressMasterCriteria(CompanyAddressMasterCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.companyCode = other.companyCode == null ? null : other.companyCode.copy();
        this.addressTypeId = other.addressTypeId == null ? null : other.addressTypeId.copy();
        this.address1 = other.address1 == null ? null : other.address1.copy();
        this.address2 = other.address2 == null ? null : other.address2.copy();
        this.address3 = other.address3 == null ? null : other.address3.copy();
        this.cityId = other.cityId == null ? null : other.cityId.copy();
        this.phoneNo = other.phoneNo == null ? null : other.phoneNo.copy();
        this.mobileNo = other.mobileNo == null ? null : other.mobileNo.copy();
        this.fax = other.fax == null ? null : other.fax.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.webpage = other.webpage == null ? null : other.webpage.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.isDeleted = other.isDeleted == null ? null : other.isDeleted.copy();
        this.companyAddressId = other.companyAddressId == null ? null : other.companyAddressId.copy();
        this.addressSource = other.addressSource == null ? null : other.addressSource.copy();
        this.pincodeId = other.pincodeId == null ? null : other.pincodeId.copy();
        this.recordId = other.recordId == null ? null : other.recordId.copy();
    }

    @Override
    public CompanyAddressMasterCriteria copy() {
        return new CompanyAddressMasterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(StringFilter companyCode) {
        this.companyCode = companyCode;
    }

    public IntegerFilter getAddressTypeId() {
        return addressTypeId;
    }

    public void setAddressTypeId(IntegerFilter addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public StringFilter getAddress1() {
        return address1;
    }

    public void setAddress1(StringFilter address1) {
        this.address1 = address1;
    }

    public StringFilter getAddress2() {
        return address2;
    }

    public void setAddress2(StringFilter address2) {
        this.address2 = address2;
    }

    public StringFilter getAddress3() {
        return address3;
    }

    public void setAddress3(StringFilter address3) {
        this.address3 = address3;
    }

    public IntegerFilter getCityId() {
        return cityId;
    }

    public void setCityId(IntegerFilter cityId) {
        this.cityId = cityId;
    }

    public StringFilter getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(StringFilter phoneNo) {
        this.phoneNo = phoneNo;
    }

    public StringFilter getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(StringFilter mobileNo) {
        this.mobileNo = mobileNo;
    }

    public StringFilter getFax() {
        return fax;
    }

    public void setFax(StringFilter fax) {
        this.fax = fax;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getWebpage() {
        return webpage;
    }

    public void setWebpage(StringFilter webpage) {
        this.webpage = webpage;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(StringFilter isDeleted) {
        this.isDeleted = isDeleted;
    }

    public IntegerFilter getCompanyAddressId() {
        return companyAddressId;
    }

    public void setCompanyAddressId(IntegerFilter companyAddressId) {
        this.companyAddressId = companyAddressId;
    }

    public StringFilter getAddressSource() {
        return addressSource;
    }

    public void setAddressSource(StringFilter addressSource) {
        this.addressSource = addressSource;
    }

    public LongFilter getPincodeId() {
        return pincodeId;
    }

    public void setPincodeId(LongFilter pincodeId) {
        this.pincodeId = pincodeId;
    }

    public LongFilter getRecordId() {
        return recordId;
    }

    public void setRecordId(LongFilter recordId) {
        this.recordId = recordId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyAddressMasterCriteria that = (CompanyAddressMasterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(companyCode, that.companyCode) &&
            Objects.equals(addressTypeId, that.addressTypeId) &&
            Objects.equals(address1, that.address1) &&
            Objects.equals(address2, that.address2) &&
            Objects.equals(address3, that.address3) &&
            Objects.equals(cityId, that.cityId) &&
            Objects.equals(phoneNo, that.phoneNo) &&
            Objects.equals(mobileNo, that.mobileNo) &&
            Objects.equals(fax, that.fax) &&
            Objects.equals(email, that.email) &&
            Objects.equals(webpage, that.webpage) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(isDeleted, that.isDeleted) &&
            Objects.equals(companyAddressId, that.companyAddressId) &&
            Objects.equals(addressSource, that.addressSource) &&
            Objects.equals(pincodeId, that.pincodeId) &&
            Objects.equals(recordId, that.recordId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        companyCode,
        addressTypeId,
        address1,
        address2,
        address3,
        cityId,
        phoneNo,
        mobileNo,
        fax,
        email,
        webpage,
        createdBy,
        createdDate,
        lastModifiedBy,
        lastModifiedDate,
        isDeleted,
        companyAddressId,
        addressSource,
        pincodeId,
        recordId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompanyAddressMasterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (companyCode != null ? "companyCode=" + companyCode + ", " : "") +
                (addressTypeId != null ? "addressTypeId=" + addressTypeId + ", " : "") +
                (address1 != null ? "address1=" + address1 + ", " : "") +
                (address2 != null ? "address2=" + address2 + ", " : "") +
                (address3 != null ? "address3=" + address3 + ", " : "") +
                (cityId != null ? "cityId=" + cityId + ", " : "") +
                (phoneNo != null ? "phoneNo=" + phoneNo + ", " : "") +
                (mobileNo != null ? "mobileNo=" + mobileNo + ", " : "") +
                (fax != null ? "fax=" + fax + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (webpage != null ? "webpage=" + webpage + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (isDeleted != null ? "isDeleted=" + isDeleted + ", " : "") +
                (companyAddressId != null ? "companyAddressId=" + companyAddressId + ", " : "") +
                (addressSource != null ? "addressSource=" + addressSource + ", " : "") +
                (pincodeId != null ? "pincodeId=" + pincodeId + ", " : "") +
                (recordId != null ? "recordId=" + recordId + ", " : "") +
            "}";
    }

}
