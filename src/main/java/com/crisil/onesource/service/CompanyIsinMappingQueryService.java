package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyIsinMapping;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyIsinMappingRepository;
import com.crisil.onesource.service.dto.CompanyIsinMappingCriteria;

/**
 * Service for executing complex queries for {@link CompanyIsinMapping} entities in the database.
 * The main input is a {@link CompanyIsinMappingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyIsinMapping} or a {@link Page} of {@link CompanyIsinMapping} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyIsinMappingQueryService extends QueryService<CompanyIsinMapping> {

    private final Logger log = LoggerFactory.getLogger(CompanyIsinMappingQueryService.class);

    private final CompanyIsinMappingRepository companyIsinMappingRepository;

    public CompanyIsinMappingQueryService(CompanyIsinMappingRepository companyIsinMappingRepository) {
        this.companyIsinMappingRepository = companyIsinMappingRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyIsinMapping} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyIsinMapping> findByCriteria(CompanyIsinMappingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyIsinMapping> specification = createSpecification(criteria);
        return companyIsinMappingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyIsinMapping} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyIsinMapping> findByCriteria(CompanyIsinMappingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyIsinMapping> specification = createSpecification(criteria);
        return companyIsinMappingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyIsinMappingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyIsinMapping> specification = createSpecification(criteria);
        return companyIsinMappingRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyIsinMappingCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyIsinMapping> createSpecification(CompanyIsinMappingCriteria criteria) {
        Specification<CompanyIsinMapping> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyIsinMapping_.id));
            }
            if (criteria.getIsinNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsinNo(), CompanyIsinMapping_.isinNo));
            }
            if (criteria.getInstrumentType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInstrumentType(), CompanyIsinMapping_.instrumentType));
            }
            if (criteria.getInstrumentSubType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInstrumentSubType(), CompanyIsinMapping_.instrumentSubType));
            }
            if (criteria.getFaceValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFaceValue(), CompanyIsinMapping_.faceValue));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), CompanyIsinMapping_.status));
            }
            if (criteria.getScriptCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScriptCode(), CompanyIsinMapping_.scriptCode));
            }
            if (criteria.getScriptSource() != null) {
                specification = specification.and(buildStringSpecification(criteria.getScriptSource(), CompanyIsinMapping_.scriptSource));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyIsinMapping_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyIsinMapping_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyIsinMapping_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyIsinMapping_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyIsinMapping_.isDeleted));
            }
            if (criteria.getCompanyCodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyCodeId(),
                    root -> root.join(CompanyIsinMapping_.companyCode, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
