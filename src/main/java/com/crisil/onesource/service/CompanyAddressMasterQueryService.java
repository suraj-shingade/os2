package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyAddressMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyAddressMasterRepository;
import com.crisil.onesource.service.dto.CompanyAddressMasterCriteria;

/**
 * Service for executing complex queries for {@link CompanyAddressMaster} entities in the database.
 * The main input is a {@link CompanyAddressMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyAddressMaster} or a {@link Page} of {@link CompanyAddressMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyAddressMasterQueryService extends QueryService<CompanyAddressMaster> {

    private final Logger log = LoggerFactory.getLogger(CompanyAddressMasterQueryService.class);

    private final CompanyAddressMasterRepository companyAddressMasterRepository;

    public CompanyAddressMasterQueryService(CompanyAddressMasterRepository companyAddressMasterRepository) {
        this.companyAddressMasterRepository = companyAddressMasterRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyAddressMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyAddressMaster> findByCriteria(CompanyAddressMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyAddressMaster> specification = createSpecification(criteria);
        return companyAddressMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyAddressMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyAddressMaster> findByCriteria(CompanyAddressMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyAddressMaster> specification = createSpecification(criteria);
        return companyAddressMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyAddressMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyAddressMaster> specification = createSpecification(criteria);
        return companyAddressMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyAddressMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyAddressMaster> createSpecification(CompanyAddressMasterCriteria criteria) {
        Specification<CompanyAddressMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyAddressMaster_.id));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), CompanyAddressMaster_.companyCode));
            }
            if (criteria.getAddressTypeId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAddressTypeId(), CompanyAddressMaster_.addressTypeId));
            }
            if (criteria.getAddress1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress1(), CompanyAddressMaster_.address1));
            }
            if (criteria.getAddress2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress2(), CompanyAddressMaster_.address2));
            }
            if (criteria.getAddress3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress3(), CompanyAddressMaster_.address3));
            }
            if (criteria.getCityId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCityId(), CompanyAddressMaster_.cityId));
            }
            if (criteria.getPhoneNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNo(), CompanyAddressMaster_.phoneNo));
            }
            if (criteria.getMobileNo() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMobileNo(), CompanyAddressMaster_.mobileNo));
            }
            if (criteria.getFax() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFax(), CompanyAddressMaster_.fax));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), CompanyAddressMaster_.email));
            }
            if (criteria.getWebpage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWebpage(), CompanyAddressMaster_.webpage));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyAddressMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyAddressMaster_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyAddressMaster_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyAddressMaster_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyAddressMaster_.isDeleted));
            }
            if (criteria.getCompanyAddressId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCompanyAddressId(), CompanyAddressMaster_.companyAddressId));
            }
            if (criteria.getAddressSource() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddressSource(), CompanyAddressMaster_.addressSource));
            }
            if (criteria.getPincodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getPincodeId(),
                    root -> root.join(CompanyAddressMaster_.pincode, JoinType.LEFT).get(LocPincodeMaster_.id)));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildSpecification(criteria.getRecordId(),
                    root -> root.join(CompanyAddressMaster_.record, JoinType.LEFT).get(CompanyRequest_.id)));
            }
        }
        return specification;
    }
}
