package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocStateMaster;
import com.crisil.onesource.repository.LocStateMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocStateMaster}.
 */
@Service
@Transactional
public class LocStateMasterService {

    private final Logger log = LoggerFactory.getLogger(LocStateMasterService.class);

    private final LocStateMasterRepository locStateMasterRepository;

    public LocStateMasterService(LocStateMasterRepository locStateMasterRepository) {
        this.locStateMasterRepository = locStateMasterRepository;
    }

    /**
     * Save a locStateMaster.
     *
     * @param locStateMaster the entity to save.
     * @return the persisted entity.
     */
    public LocStateMaster save(LocStateMaster locStateMaster) {
        log.debug("Request to save LocStateMaster : {}", locStateMaster);
        return locStateMasterRepository.save(locStateMaster);
    }

    /**
     * Get all the locStateMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocStateMaster> findAll(Pageable pageable) {
        log.debug("Request to get all LocStateMasters");
        return locStateMasterRepository.findAll(pageable);
    }


    /**
     * Get one locStateMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocStateMaster> findOne(Long id) {
        log.debug("Request to get LocStateMaster : {}", id);
        return locStateMasterRepository.findById(id);
    }

    /**
     * Delete the locStateMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocStateMaster : {}", id);
        locStateMasterRepository.deleteById(id);
    }
}
