package com.crisil.onesource.service;

import com.crisil.onesource.domain.CompanyListingStatus;
import com.crisil.onesource.repository.CompanyListingStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CompanyListingStatus}.
 */
@Service
@Transactional
public class CompanyListingStatusService {

    private final Logger log = LoggerFactory.getLogger(CompanyListingStatusService.class);

    private final CompanyListingStatusRepository companyListingStatusRepository;

    public CompanyListingStatusService(CompanyListingStatusRepository companyListingStatusRepository) {
        this.companyListingStatusRepository = companyListingStatusRepository;
    }

    /**
     * Save a companyListingStatus.
     *
     * @param companyListingStatus the entity to save.
     * @return the persisted entity.
     */
    public CompanyListingStatus save(CompanyListingStatus companyListingStatus) {
        log.debug("Request to save CompanyListingStatus : {}", companyListingStatus);
        return companyListingStatusRepository.save(companyListingStatus);
    }

    /**
     * Get all the companyListingStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyListingStatus> findAll(Pageable pageable) {
        log.debug("Request to get all CompanyListingStatuses");
        return companyListingStatusRepository.findAll(pageable);
    }


    /**
     * Get one companyListingStatus by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyListingStatus> findOne(Long id) {
        log.debug("Request to get CompanyListingStatus : {}", id);
        return companyListingStatusRepository.findById(id);
    }

    /**
     * Delete the companyListingStatus by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CompanyListingStatus : {}", id);
        companyListingStatusRepository.deleteById(id);
    }
}
