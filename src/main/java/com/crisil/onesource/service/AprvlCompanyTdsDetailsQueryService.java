package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.AprvlCompanyTdsDetails;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.AprvlCompanyTdsDetailsRepository;
import com.crisil.onesource.service.dto.AprvlCompanyTdsDetailsCriteria;

/**
 * Service for executing complex queries for {@link AprvlCompanyTdsDetails} entities in the database.
 * The main input is a {@link AprvlCompanyTdsDetailsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link AprvlCompanyTdsDetails} or a {@link Page} of {@link AprvlCompanyTdsDetails} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class AprvlCompanyTdsDetailsQueryService extends QueryService<AprvlCompanyTdsDetails> {

    private final Logger log = LoggerFactory.getLogger(AprvlCompanyTdsDetailsQueryService.class);

    private final AprvlCompanyTdsDetailsRepository aprvlCompanyTdsDetailsRepository;

    public AprvlCompanyTdsDetailsQueryService(AprvlCompanyTdsDetailsRepository aprvlCompanyTdsDetailsRepository) {
        this.aprvlCompanyTdsDetailsRepository = aprvlCompanyTdsDetailsRepository;
    }

    /**
     * Return a {@link List} of {@link AprvlCompanyTdsDetails} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<AprvlCompanyTdsDetails> findByCriteria(AprvlCompanyTdsDetailsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<AprvlCompanyTdsDetails> specification = createSpecification(criteria);
        return aprvlCompanyTdsDetailsRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link AprvlCompanyTdsDetails} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<AprvlCompanyTdsDetails> findByCriteria(AprvlCompanyTdsDetailsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<AprvlCompanyTdsDetails> specification = createSpecification(criteria);
        return aprvlCompanyTdsDetailsRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(AprvlCompanyTdsDetailsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<AprvlCompanyTdsDetails> specification = createSpecification(criteria);
        return aprvlCompanyTdsDetailsRepository.count(specification);
    }

    /**
     * Function to convert {@link AprvlCompanyTdsDetailsCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<AprvlCompanyTdsDetails> createSpecification(AprvlCompanyTdsDetailsCriteria criteria) {
        Specification<AprvlCompanyTdsDetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), AprvlCompanyTdsDetails_.id));
            }
            if (criteria.getCtlgSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCtlgSrno(), AprvlCompanyTdsDetails_.ctlgSrno));
            }
            if (criteria.getRecordid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordid(), AprvlCompanyTdsDetails_.recordid));
            }
            if (criteria.getLineofbusiness() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLineofbusiness(), AprvlCompanyTdsDetails_.lineofbusiness));
            }
            if (criteria.getFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFilename(), AprvlCompanyTdsDetails_.filename));
            }
            if (criteria.getGstnumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGstnumber(), AprvlCompanyTdsDetails_.gstnumber));
            }
            if (criteria.getIssezgst() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIssezgst(), AprvlCompanyTdsDetails_.issezgst));
            }
            if (criteria.getPrevioustds() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrevioustds(), AprvlCompanyTdsDetails_.previoustds));
            }
            if (criteria.getTdsnumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTdsnumber(), AprvlCompanyTdsDetails_.tdsnumber));
            }
            if (criteria.getStatecode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatecode(), AprvlCompanyTdsDetails_.statecode));
            }
            if (criteria.getOperation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperation(), AprvlCompanyTdsDetails_.operation));
            }
            if (criteria.getHdeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getHdeleted(), AprvlCompanyTdsDetails_.hdeleted));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), AprvlCompanyTdsDetails_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), AprvlCompanyTdsDetails_.lastModifiedDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), AprvlCompanyTdsDetails_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), AprvlCompanyTdsDetails_.lastModifiedBy));
            }
            if (criteria.getCtlgIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtlgIsDeleted(), AprvlCompanyTdsDetails_.ctlgIsDeleted));
            }
            if (criteria.getJsonStoreId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJsonStoreId(), AprvlCompanyTdsDetails_.jsonStoreId));
            }
            if (criteria.getIsDataProcessed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDataProcessed(), AprvlCompanyTdsDetails_.isDataProcessed));
            }
        }
        return specification;
    }
}
