package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanySectorMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanySectorMasterRepository;
import com.crisil.onesource.service.dto.CompanySectorMasterCriteria;

/**
 * Service for executing complex queries for {@link CompanySectorMaster} entities in the database.
 * The main input is a {@link CompanySectorMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanySectorMaster} or a {@link Page} of {@link CompanySectorMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanySectorMasterQueryService extends QueryService<CompanySectorMaster> {

    private final Logger log = LoggerFactory.getLogger(CompanySectorMasterQueryService.class);

    private final CompanySectorMasterRepository companySectorMasterRepository;

    public CompanySectorMasterQueryService(CompanySectorMasterRepository companySectorMasterRepository) {
        this.companySectorMasterRepository = companySectorMasterRepository;
    }

    /**
     * Return a {@link List} of {@link CompanySectorMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanySectorMaster> findByCriteria(CompanySectorMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanySectorMaster> specification = createSpecification(criteria);
        return companySectorMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanySectorMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanySectorMaster> findByCriteria(CompanySectorMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanySectorMaster> specification = createSpecification(criteria);
        return companySectorMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanySectorMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanySectorMaster> specification = createSpecification(criteria);
        return companySectorMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanySectorMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanySectorMaster> createSpecification(CompanySectorMasterCriteria criteria) {
        Specification<CompanySectorMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanySectorMaster_.id));
            }
            if (criteria.getSectorId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSectorId(), CompanySectorMaster_.sectorId));
            }
            if (criteria.getSectorName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSectorName(), CompanySectorMaster_.sectorName));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanySectorMaster_.isDeleted));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanySectorMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanySectorMaster_.lastModifiedBy));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanySectorMaster_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanySectorMaster_.createdDate));
            }
            if (criteria.getOperation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperation(), CompanySectorMaster_.operation));
            }
            if (criteria.getOnesourceCompanyMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getOnesourceCompanyMasterId(),
                    root -> root.join(CompanySectorMaster_.onesourceCompanyMasters, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
