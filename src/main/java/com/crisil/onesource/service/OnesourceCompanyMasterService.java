package com.crisil.onesource.service;

import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.repository.OnesourceCompanyMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OnesourceCompanyMaster}.
 */
@Service
@Transactional
public class OnesourceCompanyMasterService {

    private final Logger log = LoggerFactory.getLogger(OnesourceCompanyMasterService.class);

    private final OnesourceCompanyMasterRepository onesourceCompanyMasterRepository;

    public OnesourceCompanyMasterService(OnesourceCompanyMasterRepository onesourceCompanyMasterRepository) {
        this.onesourceCompanyMasterRepository = onesourceCompanyMasterRepository;
    }

    /**
     * Save a onesourceCompanyMaster.
     *
     * @param onesourceCompanyMaster the entity to save.
     * @return the persisted entity.
     */
    public OnesourceCompanyMaster save(OnesourceCompanyMaster onesourceCompanyMaster) {
        log.debug("Request to save OnesourceCompanyMaster : {}", onesourceCompanyMaster);
        return onesourceCompanyMasterRepository.save(onesourceCompanyMaster);
    }

    /**
     * Get all the onesourceCompanyMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OnesourceCompanyMaster> findAll(Pageable pageable) {
        log.debug("Request to get all OnesourceCompanyMasters");
        return onesourceCompanyMasterRepository.findAll(pageable);
    }


    /**
     * Get one onesourceCompanyMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OnesourceCompanyMaster> findOne(Long id) {
        log.debug("Request to get OnesourceCompanyMaster : {}", id);
        return onesourceCompanyMasterRepository.findById(id);
    }

    /**
     * Delete the onesourceCompanyMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OnesourceCompanyMaster : {}", id);
        onesourceCompanyMasterRepository.deleteById(id);
    }
}
