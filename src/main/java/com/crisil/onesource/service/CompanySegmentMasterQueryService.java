package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanySegmentMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanySegmentMasterRepository;
import com.crisil.onesource.service.dto.CompanySegmentMasterCriteria;

/**
 * Service for executing complex queries for {@link CompanySegmentMaster} entities in the database.
 * The main input is a {@link CompanySegmentMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanySegmentMaster} or a {@link Page} of {@link CompanySegmentMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanySegmentMasterQueryService extends QueryService<CompanySegmentMaster> {

    private final Logger log = LoggerFactory.getLogger(CompanySegmentMasterQueryService.class);

    private final CompanySegmentMasterRepository companySegmentMasterRepository;

    public CompanySegmentMasterQueryService(CompanySegmentMasterRepository companySegmentMasterRepository) {
        this.companySegmentMasterRepository = companySegmentMasterRepository;
    }

    /**
     * Return a {@link List} of {@link CompanySegmentMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanySegmentMaster> findByCriteria(CompanySegmentMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanySegmentMaster> specification = createSpecification(criteria);
        return companySegmentMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanySegmentMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanySegmentMaster> findByCriteria(CompanySegmentMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanySegmentMaster> specification = createSpecification(criteria);
        return companySegmentMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanySegmentMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanySegmentMaster> specification = createSpecification(criteria);
        return companySegmentMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanySegmentMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanySegmentMaster> createSpecification(CompanySegmentMasterCriteria criteria) {
        Specification<CompanySegmentMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanySegmentMaster_.id));
            }
            if (criteria.getSegmentId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSegmentId(), CompanySegmentMaster_.segmentId));
            }
            if (criteria.getSegmentName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSegmentName(), CompanySegmentMaster_.segmentName));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanySegmentMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanySegmentMaster_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanySegmentMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanySegmentMaster_.lastModifiedBy));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanySegmentMaster_.isDeleted));
            }
        }
        return specification;
    }
}
