package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocPincodeMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocPincodeMasterRepository;
import com.crisil.onesource.service.dto.LocPincodeMasterCriteria;

/**
 * Service for executing complex queries for {@link LocPincodeMaster} entities in the database.
 * The main input is a {@link LocPincodeMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocPincodeMaster} or a {@link Page} of {@link LocPincodeMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocPincodeMasterQueryService extends QueryService<LocPincodeMaster> {

    private final Logger log = LoggerFactory.getLogger(LocPincodeMasterQueryService.class);

    private final LocPincodeMasterRepository locPincodeMasterRepository;

    public LocPincodeMasterQueryService(LocPincodeMasterRepository locPincodeMasterRepository) {
        this.locPincodeMasterRepository = locPincodeMasterRepository;
    }

    /**
     * Return a {@link List} of {@link LocPincodeMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocPincodeMaster> findByCriteria(LocPincodeMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocPincodeMaster> specification = createSpecification(criteria);
        return locPincodeMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocPincodeMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocPincodeMaster> findByCriteria(LocPincodeMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocPincodeMaster> specification = createSpecification(criteria);
        return locPincodeMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocPincodeMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocPincodeMaster> specification = createSpecification(criteria);
        return locPincodeMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link LocPincodeMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocPincodeMaster> createSpecification(LocPincodeMasterCriteria criteria) {
        Specification<LocPincodeMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocPincodeMaster_.id));
            }
            if (criteria.getPincodeSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPincodeSrno(), LocPincodeMaster_.pincodeSrno));
            }
            if (criteria.getOfficeName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOfficeName(), LocPincodeMaster_.officeName));
            }
            if (criteria.getPincode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPincode(), LocPincodeMaster_.pincode));
            }
            if (criteria.getOfficeType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOfficeType(), LocPincodeMaster_.officeType));
            }
            if (criteria.getDeliveryStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDeliveryStatus(), LocPincodeMaster_.deliveryStatus));
            }
            if (criteria.getDivisionName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDivisionName(), LocPincodeMaster_.divisionName));
            }
            if (criteria.getRegionName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegionName(), LocPincodeMaster_.regionName));
            }
            if (criteria.getCircleName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCircleName(), LocPincodeMaster_.circleName));
            }
            if (criteria.getTaluka() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTaluka(), LocPincodeMaster_.taluka));
            }
            if (criteria.getDistrictName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDistrictName(), LocPincodeMaster_.districtName));
            }
            if (criteria.getStateName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStateName(), LocPincodeMaster_.stateName));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), LocPincodeMaster_.country));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocPincodeMaster_.isDeleted));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocPincodeMaster_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocPincodeMaster_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocPincodeMaster_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocPincodeMaster_.lastModifiedBy));
            }
            if (criteria.getIsApproved() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsApproved(), LocPincodeMaster_.isApproved));
            }
            if (criteria.getSeqOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSeqOrder(), LocPincodeMaster_.seqOrder));
            }
            if (criteria.getIsDataProcessed() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDataProcessed(), LocPincodeMaster_.isDataProcessed));
            }
            if (criteria.getJsonStoreId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getJsonStoreId(), LocPincodeMaster_.jsonStoreId));
            }
            if (criteria.getCtlgIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCtlgIsDeleted(), LocPincodeMaster_.ctlgIsDeleted));
            }
            if (criteria.getOperation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOperation(), LocPincodeMaster_.operation));
            }
            if (criteria.getCtlgSrno() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCtlgSrno(), LocPincodeMaster_.ctlgSrno));
            }
            if (criteria.getCompanyAddressMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyAddressMasterId(),
                    root -> root.join(LocPincodeMaster_.companyAddressMasters, JoinType.LEFT).get(CompanyAddressMaster_.id)));
            }
            if (criteria.getOnesourceCompanyMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getOnesourceCompanyMasterId(),
                    root -> root.join(LocPincodeMaster_.onesourceCompanyMasters, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
            if (criteria.getStateIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getStateIdId(),
                    root -> root.join(LocPincodeMaster_.stateId, JoinType.LEFT).get(LocStateMaster_.id)));
            }
            if (criteria.getDistrictIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getDistrictIdId(),
                    root -> root.join(LocPincodeMaster_.districtId, JoinType.LEFT).get(LocDistrictMaster_.id)));
            }
        }
        return specification;
    }
}
