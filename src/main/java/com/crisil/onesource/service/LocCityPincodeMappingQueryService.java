package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.LocCityPincodeMapping;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.LocCityPincodeMappingRepository;
import com.crisil.onesource.service.dto.LocCityPincodeMappingCriteria;

/**
 * Service for executing complex queries for {@link LocCityPincodeMapping} entities in the database.
 * The main input is a {@link LocCityPincodeMappingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link LocCityPincodeMapping} or a {@link Page} of {@link LocCityPincodeMapping} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class LocCityPincodeMappingQueryService extends QueryService<LocCityPincodeMapping> {

    private final Logger log = LoggerFactory.getLogger(LocCityPincodeMappingQueryService.class);

    private final LocCityPincodeMappingRepository locCityPincodeMappingRepository;

    public LocCityPincodeMappingQueryService(LocCityPincodeMappingRepository locCityPincodeMappingRepository) {
        this.locCityPincodeMappingRepository = locCityPincodeMappingRepository;
    }

    /**
     * Return a {@link List} of {@link LocCityPincodeMapping} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<LocCityPincodeMapping> findByCriteria(LocCityPincodeMappingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<LocCityPincodeMapping> specification = createSpecification(criteria);
        return locCityPincodeMappingRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link LocCityPincodeMapping} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<LocCityPincodeMapping> findByCriteria(LocCityPincodeMappingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<LocCityPincodeMapping> specification = createSpecification(criteria);
        return locCityPincodeMappingRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(LocCityPincodeMappingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<LocCityPincodeMapping> specification = createSpecification(criteria);
        return locCityPincodeMappingRepository.count(specification);
    }

    /**
     * Function to convert {@link LocCityPincodeMappingCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<LocCityPincodeMapping> createSpecification(LocCityPincodeMappingCriteria criteria) {
        Specification<LocCityPincodeMapping> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), LocCityPincodeMapping_.id));
            }
            if (criteria.getCityPinMappingId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCityPinMappingId(), LocCityPincodeMapping_.cityPinMappingId));
            }
            if (criteria.getPincode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPincode(), LocCityPincodeMapping_.pincode));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), LocCityPincodeMapping_.isDeleted));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), LocCityPincodeMapping_.createdDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), LocCityPincodeMapping_.createdBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), LocCityPincodeMapping_.lastModifiedDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), LocCityPincodeMapping_.lastModifiedBy));
            }
            if (criteria.getCityIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getCityIdId(),
                    root -> root.join(LocCityPincodeMapping_.cityId, JoinType.LEFT).get(LocCityMaster_.id)));
            }
        }
        return specification;
    }
}
