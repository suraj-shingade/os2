package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.OnesourceCompanyMaster;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.OnesourceCompanyMasterRepository;
import com.crisil.onesource.service.dto.OnesourceCompanyMasterCriteria;

/**
 * Service for executing complex queries for {@link OnesourceCompanyMaster} entities in the database.
 * The main input is a {@link OnesourceCompanyMasterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OnesourceCompanyMaster} or a {@link Page} of {@link OnesourceCompanyMaster} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OnesourceCompanyMasterQueryService extends QueryService<OnesourceCompanyMaster> {

    private final Logger log = LoggerFactory.getLogger(OnesourceCompanyMasterQueryService.class);

    private final OnesourceCompanyMasterRepository onesourceCompanyMasterRepository;

    public OnesourceCompanyMasterQueryService(OnesourceCompanyMasterRepository onesourceCompanyMasterRepository) {
        this.onesourceCompanyMasterRepository = onesourceCompanyMasterRepository;
    }

    /**
     * Return a {@link List} of {@link OnesourceCompanyMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OnesourceCompanyMaster> findByCriteria(OnesourceCompanyMasterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OnesourceCompanyMaster> specification = createSpecification(criteria);
        return onesourceCompanyMasterRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link OnesourceCompanyMaster} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OnesourceCompanyMaster> findByCriteria(OnesourceCompanyMasterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OnesourceCompanyMaster> specification = createSpecification(criteria);
        return onesourceCompanyMasterRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OnesourceCompanyMasterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OnesourceCompanyMaster> specification = createSpecification(criteria);
        return onesourceCompanyMasterRepository.count(specification);
    }

    /**
     * Function to convert {@link OnesourceCompanyMasterCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OnesourceCompanyMaster> createSpecification(OnesourceCompanyMasterCriteria criteria) {
        Specification<OnesourceCompanyMaster> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), OnesourceCompanyMaster_.id));
            }
            if (criteria.getCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyCode(), OnesourceCompanyMaster_.companyCode));
            }
            if (criteria.getCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyName(), OnesourceCompanyMaster_.companyName));
            }
            if (criteria.getRemarks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRemarks(), OnesourceCompanyMaster_.remarks));
            }
            if (criteria.getRecordId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRecordId(), OnesourceCompanyMaster_.recordId));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStatus(), OnesourceCompanyMaster_.status));
            }
            if (criteria.getClientStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientStatus(), OnesourceCompanyMaster_.clientStatus));
            }
            if (criteria.getDisplayCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayCompanyName(), OnesourceCompanyMaster_.displayCompanyName));
            }
            if (criteria.getBillingType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBillingType(), OnesourceCompanyMaster_.billingType));
            }
            if (criteria.getIsBillable() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsBillable(), OnesourceCompanyMaster_.isBillable));
            }
            if (criteria.getListingStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getListingStatus(), OnesourceCompanyMaster_.listingStatus));
            }
            if (criteria.getDateOfIncooperation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDateOfIncooperation(), OnesourceCompanyMaster_.dateOfIncooperation));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), OnesourceCompanyMaster_.createdDate));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), OnesourceCompanyMaster_.lastModifiedDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), OnesourceCompanyMaster_.createdBy));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), OnesourceCompanyMaster_.lastModifiedBy));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), OnesourceCompanyMaster_.isDeleted));
            }
            if (criteria.getOriginalFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOriginalFilename(), OnesourceCompanyMaster_.originalFilename));
            }
            if (criteria.getDisplayFilename() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayFilename(), OnesourceCompanyMaster_.displayFilename));
            }
            if (criteria.getCompanyStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyStatus(), OnesourceCompanyMaster_.companyStatus));
            }
            if (criteria.getCompanyChangeRequestId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyChangeRequestId(),
                    root -> root.join(OnesourceCompanyMaster_.companyChangeRequests, JoinType.LEFT).get(CompanyChangeRequest_.id)));
            }
            if (criteria.getCompanyIsinMappingId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyIsinMappingId(),
                    root -> root.join(OnesourceCompanyMaster_.companyIsinMappings, JoinType.LEFT).get(CompanyIsinMapping_.id)));
            }
            if (criteria.getCompanyListingStatusId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyListingStatusId(),
                    root -> root.join(OnesourceCompanyMaster_.companyListingStatuses, JoinType.LEFT).get(CompanyListingStatus_.id)));
            }
            if (criteria.getContactMasterId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactMasterId(),
                    root -> root.join(OnesourceCompanyMaster_.contactMasters, JoinType.LEFT).get(ContactMaster_.id)));
            }
            if (criteria.getKarzaCompInformationId() != null) {
                specification = specification.and(buildSpecification(criteria.getKarzaCompInformationId(),
                    root -> root.join(OnesourceCompanyMaster_.karzaCompInformations, JoinType.LEFT).get(KarzaCompInformation_.id)));
            }
            if (criteria.getCompanyTypeIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getCompanyTypeIdId(),
                    root -> root.join(OnesourceCompanyMaster_.companyTypeId, JoinType.LEFT).get(CompanyTypeMaster_.id)));
            }
            if (criteria.getSectorIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getSectorIdId(),
                    root -> root.join(OnesourceCompanyMaster_.sectorId, JoinType.LEFT).get(CompanySectorMaster_.id)));
            }
            if (criteria.getIndustryIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getIndustryIdId(),
                    root -> root.join(OnesourceCompanyMaster_.industryId, JoinType.LEFT).get(IndustryMaster_.id)));
            }
            if (criteria.getGroupIdId() != null) {
                specification = specification.and(buildSpecification(criteria.getGroupIdId(),
                    root -> root.join(OnesourceCompanyMaster_.groupId, JoinType.LEFT).get(GroupMaster_.id)));
            }
            if (criteria.getPinId() != null) {
                specification = specification.and(buildSpecification(criteria.getPinId(),
                    root -> root.join(OnesourceCompanyMaster_.pin, JoinType.LEFT).get(LocPincodeMaster_.id)));
            }
        }
        return specification;
    }
}
