package com.crisil.onesource.service;

import com.crisil.onesource.domain.ContactMasterHistory;
import com.crisil.onesource.repository.ContactMasterHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactMasterHistory}.
 */
@Service
@Transactional
public class ContactMasterHistoryService {

    private final Logger log = LoggerFactory.getLogger(ContactMasterHistoryService.class);

    private final ContactMasterHistoryRepository contactMasterHistoryRepository;

    public ContactMasterHistoryService(ContactMasterHistoryRepository contactMasterHistoryRepository) {
        this.contactMasterHistoryRepository = contactMasterHistoryRepository;
    }

    /**
     * Save a contactMasterHistory.
     *
     * @param contactMasterHistory the entity to save.
     * @return the persisted entity.
     */
    public ContactMasterHistory save(ContactMasterHistory contactMasterHistory) {
        log.debug("Request to save ContactMasterHistory : {}", contactMasterHistory);
        return contactMasterHistoryRepository.save(contactMasterHistory);
    }

    /**
     * Get all the contactMasterHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactMasterHistory> findAll(Pageable pageable) {
        log.debug("Request to get all ContactMasterHistories");
        return contactMasterHistoryRepository.findAll(pageable);
    }


    /**
     * Get one contactMasterHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactMasterHistory> findOne(Long id) {
        log.debug("Request to get ContactMasterHistory : {}", id);
        return contactMasterHistoryRepository.findById(id);
    }

    /**
     * Delete the contactMasterHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactMasterHistory : {}", id);
        contactMasterHistoryRepository.deleteById(id);
    }
}
