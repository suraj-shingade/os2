package com.crisil.onesource.service;

import com.crisil.onesource.domain.LocCityMaster;
import com.crisil.onesource.repository.LocCityMasterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link LocCityMaster}.
 */
@Service
@Transactional
public class LocCityMasterService {

    private final Logger log = LoggerFactory.getLogger(LocCityMasterService.class);

    private final LocCityMasterRepository locCityMasterRepository;

    public LocCityMasterService(LocCityMasterRepository locCityMasterRepository) {
        this.locCityMasterRepository = locCityMasterRepository;
    }

    /**
     * Save a locCityMaster.
     *
     * @param locCityMaster the entity to save.
     * @return the persisted entity.
     */
    public LocCityMaster save(LocCityMaster locCityMaster) {
        log.debug("Request to save LocCityMaster : {}", locCityMaster);
        return locCityMasterRepository.save(locCityMaster);
    }

    /**
     * Get all the locCityMasters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<LocCityMaster> findAll(Pageable pageable) {
        log.debug("Request to get all LocCityMasters");
        return locCityMasterRepository.findAll(pageable);
    }


    /**
     * Get one locCityMaster by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<LocCityMaster> findOne(Long id) {
        log.debug("Request to get LocCityMaster : {}", id);
        return locCityMasterRepository.findById(id);
    }

    /**
     * Delete the locCityMaster by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete LocCityMaster : {}", id);
        locCityMasterRepository.deleteById(id);
    }
}
