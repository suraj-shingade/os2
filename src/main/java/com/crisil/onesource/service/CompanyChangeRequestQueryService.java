package com.crisil.onesource.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.crisil.onesource.domain.CompanyChangeRequest;
import com.crisil.onesource.domain.*; // for static metamodels
import com.crisil.onesource.repository.CompanyChangeRequestRepository;
import com.crisil.onesource.service.dto.CompanyChangeRequestCriteria;

/**
 * Service for executing complex queries for {@link CompanyChangeRequest} entities in the database.
 * The main input is a {@link CompanyChangeRequestCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyChangeRequest} or a {@link Page} of {@link CompanyChangeRequest} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyChangeRequestQueryService extends QueryService<CompanyChangeRequest> {

    private final Logger log = LoggerFactory.getLogger(CompanyChangeRequestQueryService.class);

    private final CompanyChangeRequestRepository companyChangeRequestRepository;

    public CompanyChangeRequestQueryService(CompanyChangeRequestRepository companyChangeRequestRepository) {
        this.companyChangeRequestRepository = companyChangeRequestRepository;
    }

    /**
     * Return a {@link List} of {@link CompanyChangeRequest} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyChangeRequest> findByCriteria(CompanyChangeRequestCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CompanyChangeRequest> specification = createSpecification(criteria);
        return companyChangeRequestRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CompanyChangeRequest} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyChangeRequest> findByCriteria(CompanyChangeRequestCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CompanyChangeRequest> specification = createSpecification(criteria);
        return companyChangeRequestRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyChangeRequestCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CompanyChangeRequest> specification = createSpecification(criteria);
        return companyChangeRequestRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyChangeRequestCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CompanyChangeRequest> createSpecification(CompanyChangeRequestCriteria criteria) {
        Specification<CompanyChangeRequest> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CompanyChangeRequest_.id));
            }
            if (criteria.getOldCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOldCompanyName(), CompanyChangeRequest_.oldCompanyName));
            }
            if (criteria.getNewCompanyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNewCompanyName(), CompanyChangeRequest_.newCompanyName));
            }
            if (criteria.getNewCompanyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNewCompanyCode(), CompanyChangeRequest_.newCompanyCode));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), CompanyChangeRequest_.status));
            }
            if (criteria.getEffectiveDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEffectiveDate(), CompanyChangeRequest_.effectiveDate));
            }
            if (criteria.getRequestedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRequestedBy(), CompanyChangeRequest_.requestedBy));
            }
            if (criteria.getCompanyChangeId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCompanyChangeId(), CompanyChangeRequest_.companyChangeId));
            }
            if (criteria.getCaseType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCaseType(), CompanyChangeRequest_.caseType));
            }
            if (criteria.getReason() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReason(), CompanyChangeRequest_.reason));
            }
            if (criteria.getReasonDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReasonDescription(), CompanyChangeRequest_.reasonDescription));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CompanyChangeRequest_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), CompanyChangeRequest_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), CompanyChangeRequest_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), CompanyChangeRequest_.lastModifiedDate));
            }
            if (criteria.getIsDeleted() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIsDeleted(), CompanyChangeRequest_.isDeleted));
            }
            if (criteria.getOldCompanyCodeId() != null) {
                specification = specification.and(buildSpecification(criteria.getOldCompanyCodeId(),
                    root -> root.join(CompanyChangeRequest_.oldCompanyCode, JoinType.LEFT).get(OnesourceCompanyMaster_.id)));
            }
        }
        return specification;
    }
}
